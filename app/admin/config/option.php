<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/13 0013
 * Time: 16:51
 */
$config['article_option']=array(
    1=>"我的QA",
    2=>"通知",
    3=>"免责条款",
    4=>"其他公告",
    5=>"用户注册协议",
    6=>"技师注册协议",
    7=>"用户帮助中心",
    8=>"技师帮助中心"
);

$config['cash_option']=array(
    1=>"支付宝",
    2=>"微信"
);

$config['area_list']=array(
    0=>"",
    1=>"武汉",
    2=>"襄阳",
    3=>"湖北省武汉市"
);

$config['sex_option']=array(
    0=>"",
    1=>"男",
    2=>"女"
);

$config['cash_status']=array(
    0=>"审核中",
    1=>"审核通过",
    2=>"驳回"
);

$config['card_success']=array(
  0=>"未验证",
  1=>"已通过",
  2=>"未通过"
);

$config['techer_status']=array(
    0=>"审核中",
    1=>"审核通过",
    2=>"未通过审核"
);

$config['refund_status']=array(
    0=>'已确认',
    1=>"未确认",
    2=>"已拒绝",
    3=>"已完成退款"
);

$config['success_status']=array(
    0=>'未审核',
    1=>"已通过",
    2=>"未通过"
);
