<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {
    public function __construct() {
        parent::__construct();
    }

    public function user_list() {
        $now_page = $this->input->get('per_page', TRUE);
        $kwd = $this->input->get('kwd');
        $all_user = $this->adminuser_model->all_user($kwd);
        //获取分页数据
        $page_num = 8;
        $page_msg = $this->adminuser_model->page_msg($page_num, $now_page, $kwd);
        $view['page_list'] = help_pages($all_user, $page_num);
        $view['main_msg'] = $page_msg;
        $view['kwd'] = $kwd;
        $this->load->view('admin/user_list', $view);
    }

    public function edit_user($uid = '') {
        if ($_POST) {
            $data = $this->input->post();
            if (empty($data['login_name']) || empty($data['menu_leval']) || ($data['status'] != 1 && $data['status'] != 0) || empty($data['area'])) {
                $this->showmessage('操作失败');
            }
            $user_id = $data['uid'];
            if (!preg_match("/^[a-zA-Z][a-zA-Z0-9_]{5,19}$/", $data['login_name'])) {
                $this->showmessage('账号格式有误！');
            }
            if ($data ['pwd']) {
                if (!preg_match("/^[a-zA-Z]\w{5,19}$/", $data['pwd'])) {
                    $this->showmessage('密码格式有误！');
                }
                $save['pwd'] = sha1(md5(md5($data ['pwd'])));
            }
            $save['login_name'] = $data['login_name'];
            $save['menu_leval'] = $data['menu_leval'];
            $save['status'] = $data['status'];
            $save['area'] = implode(',', $data['area']);
            $rs = $this->adminuser_model->edit_user($user_id, $save);
            if ($rs) {
                $this->showmessage('修改成功！', HTTP_REFERER);
            } else {
                $this->showmessage('修改失败！');
            }
        } else {
            if (empty($uid)) {
                $this->showmessage('缺少重要参数！');
            }
            $this->load->helper('form');
            $user = $this->adminuser_model->user_msg($uid);
            $menu_level = $this->adminuser_model->menu_level();
            $area = $this->adminuser_model->area_list();
            //所有分类
            $user['area'] = explode(',', $user['area']);
            $data['user'] = $user;
            $data['menu_level'] = $menu_level;
            $data['area'] = $area;
            $this->load->view('admin/edit_user', $data);
        }
    }

    public function add_user() {
        if ($_POST) {
            $data = $this->input->post();
            if (empty($data['login_name']) || empty($data['pwd']) || empty($data['menu_leval']) || ($data['status'] != 1 && $data['status'] != 0)) {
                $this->showmessage('缺少重要参数！');
            }
            if (!preg_match("/^[a-zA-Z][a-zA-Z0-9_]{5,19}$/", $data['login_name'])) {
                $this->showmessage('账号格式有误！');
            }
            if (!preg_match("/^[a-zA-Z]\w{5,19}$/", $data['pwd'])) {
                $this->showmessage('密码格式有误！');
            }
            $add['login_name'] = $data['login_name'];
            $add['menu_leval'] = $data['menu_leval'];
            $add['status'] = $data['status'];
            $add['pwd'] = sha1(md5(md5($data ['pwd'])));
            $add['area'] = implode(',', $data['area']);
            $add['time'] = time();
            $rs = $this->adminuser_model->add_user($add);
            if ($rs) {
                $this->showmessage('添加成功', HTTP_REFERER);
            } else {
                $this->showmessage('账号已存在，请换一个账号！');
            }
        } else {
            $this->load->helper('form');
            $menu_level = $this->adminuser_model->menu_level();
            $area = $this->adminuser_model->area_list();
            $data['menu_level'] = $menu_level;
            $data['area'] = $area;
            $this->load->view('admin/add_user', $data);
        }
    }

    public function del_user($uid) {
        $rs = $this->adminuser_model->del_user($uid);
        if ($rs) {
            $this->showmessage('删除成功', HTTP_REFERER);
        } else {
            $this->showmessage('删除失败');
        }
    }


    //权限组列表
    public function aul_config()
    {
        $param=$this->input->post();
        if(!empty($param))
        {
            $data=array(
                "name"=>$param['name'],
                "menuIds"=>"1"
            );
            $result=$this->db->insert("xjm_admin_menu_level",$data);
            if($result)
            {
                $arr=array('code'=>0,'msg'=>'新增成功');
            }
            else
            {
                $arr=array('code'=>-1,'msg'=>'新增失败');
            }
            echo json_encode($arr);die;
        }
        else
        {
            $aul_query=$this->db->select('id,name')->from('xjm_admin_menu_level')->order_by('id')->get();
            $view['aul_result']=$aul_query->result_array();

            $this->load->view('admin/aul_list',$view);
        }


    }


    //权限设置
    public function aul_option()
    {
        $param=$this->input->post();
        if(!empty($param))
        {
            $config['menuIds']=@implode(',',$param['auls']);
            $arr=array('id'=>$param['aul_name']);
            $str = $this->db->update("xjm_admin_menu_level",$config,$arr);
            if($str==1)
            {
                $this->showmessage('设置成功', HTTP_REFERER);
            }
            else
            {
                $this->showmessage('设置失败');
            }
        }
        else
        {
            $id=$this->input->get('id');

            $query=$this->db->select('menuIds')->from('xjm_admin_menu_level')->where('id='.intval($id))->get();

            $row=$query->row_array();

            $all=$this->db->select('id,`name`')->from('xjm_admin_menu')->get();
            $all_result=$all->result_array();

            $new_all=array();
            foreach($all_result as $k=>$v)
            {
                $new_all[$v['id']]['name']=$v['name'];
                $new_all[$v['id']]['id']=$v['id'];
            }


            $qu=$this->db->query('SELECT id,`name` FROM xjm_admin_menu WHERE id IN ( '.$row["menuIds"].' )');
            $re=$qu->result_array();


            foreach($re as $k=>$v)
            {

                $new_all[$v['id']]['true']=1;

            }
            $view['option_list']=$new_all;


            $this->load->view('admin/aul_config',$view);
        }
    }

    //省份列表
    public function city_list()
    {
        $param=$this->input->post();
        if(!empty($param))
        {

        }
        else
        {
            $now_page = $this->input->get('per_page', TRUE);
            $kwd=$this->input->get('kwd');
            $all_user = $this->adminuser_model->all_pro($kwd);
            //获取分页数据
            $page_num = 15;
            $page_msg = $this->adminuser_model->page_pro($page_num, $now_page, $kwd);
            $view['page_list'] = help_pages($all_user, $page_num);
            $view['pro_result']=$page_msg;

            $this->load->view('admin/pro_list',$view);
        }
    }


    //城市选择
    public function city_config()
    {
        $param=$this->input->post();
        if(!empty($param))
        {
            if(empty($param['status']))
            {
                $arr['status']=0;
                $where='aid='.$param['aid'];
                $str=$this->db->update('xjm_city',$arr,$where);
                if($str>=1)
                {
                    $this->showmessage('设置成功', HTTP_REFERER);
                }
                else
                {
                    $this->showmessage('设置失败');
                }
            }
            else
            {
                $arr_one['status']=0;
                $where_one='aid='.$param['aid'];
                $this->db->update('xjm_city',$arr_one,$where_one);

                foreach($param['status'] as $k=>$v)
                {
                    $where='id='.$v;
                    $arr['status']=1;
                    $this->db->update('xjm_city',$arr,$where);

                }

                $this->showmessage('设置成功', HTTP_REFERER);
            }
        }
        else
        {
            $aid=$this->input->get('aid');
            $query=$this->db->select('id,name,status')->from('xjm_city')->where('aid='.intval($aid))->order_by('id')->get();
            $view['option_list']=$query->result_array();
            $this->load->view('admin/city_config',$view);
        }


    }
}
