<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Adminlogin extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('m_admin_model');
    }

    public function index() {
        $this->load->view('main/login');
    }

    public function submit() {
        $name = $this->input->post('name');
        $pwd = $this->input->post('pwd');
        if ($this->m_admin_model->login($name, $pwd)) {
            header('location: /admin.php/Main/index');
        } else {
            header('location: /admin.php/adminlogin');
        }
    }

    public function logout() {
        $this->load->helper('url');
        $this->session->sess_destroy();
        header('location: /admin.php');
    }

}
