<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends MY_Controller {
    
    public function __construct() {
        parent::__construct();

        $this->load->model('article_model');
        $this->config->load('option');
        $this->load->driver('cache');

    }
    
    public function index() {
        $now_page = $this->input->get('per_page', TRUE);
        $kwd = $this->input->get('kwd');
        $all_user = $this->article_model->all_article($kwd);
        //获取分页数据
        $page_data = $this->article_model->page_data(10, $now_page, $kwd);
        $view['page_list'] = help_pages($all_user, 10);
        $view['page_data'] = $page_data;
        $view['kwd'] = $kwd;
        $article_option=$this->config->item('article_option');
        $view['article_option']=$article_option;
        $this->load->view('Article/index', $view);
    }
    
    public function add(){
        if($this->input->post('formhash')){
            $data = $this->input->post();
            $addArticle['type'] = $data['type'];
            $addArticle['title'] = $data['title'];
            $addArticle['description'] = $data['description'];
            $addArticle['sort_order'] = $data['sort_order'];
            $addArticle['content'] = $data['content'];
            $addArticle['status'] = $data['is_show'];
            $addArticle['add_time'] = time();
            $addArticle['set_time'] = time();
            if($this->input->post('id')){
                if($this->article_model->saveArticle($this->input->post('id'),$addArticle)){
                    $this->showmessage('修改成功','/admin.php/Article/index');
                }
            }else{
                if($this->article_model->addArticle($addArticle)){
                    $this->showmessage('添加成功','/admin.php/Article/index');
                }
            }
        }else{
            $this->load->library('qiniu');
    	    //初始化七牛信息
    	    header('Access-Control-Allow-Origin:*');
    	    $bucket = '20170914';
    	    $accessKey = 'ofogHIzoeYZbU_HEhd6THyA3ZJDCgB_nONhLNmZS';
    	    $secretKey = 'w-bAU5rWT46C8heMe-wmWaedurY_9r1J0ZEaoOvx';
    	    $auth = new Qiniu\Auth($accessKey, $secretKey);
    	    $upToken = $auth->uploadToken($bucket, null, 3600);//获取上传所需的token
    	    $qiniu_url = 'http://up-z0.qiniu.com/putb64/-1';
    	    $read_url = 'http://ow91q34mo.bkt.clouddn.com/';
    	    $data =array();
    	    $data['uptoken'] = $upToken;
    	    $data['bucket'] = $bucket;
    	    $goodsOptionList = $this->config->item('article_option');
    	    $data['goodsOptionList'] = $goodsOptionList;
    	    if($this->input->get('id')){
    	        $goodsInfo = $this->article_model->getArticleById($this->input->get('id'));
    	        $data['goodsInfo'] = $goodsInfo;
    	    }
            $this->load->view('Article/add', $data);
        }
    }
    public function del(){
        if($this->input->get('id')){
             if($this->article_model->delArticleById($this->input->get('id'))){
                 echo json_encode(array('status'=>'success','msg'=>'删除成功！','url'=>'/admin.php/Article/index'));exit;
                 
             }
        }
        
    }

    //清空静态缓存
    public function flush_cache()
    {
        $myperson_qa=$this->cache->redis->delete('myperson_qa');
        $teacher_register_agreement=$this->cache->redis->delete('teacher_register_agreement');
        echo "<script type='text/javascript'>alert('清除成功');window.location.href='/admin.php/Article/index';</script>";

    }
    
}
