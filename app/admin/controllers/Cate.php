<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cate extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('cate_model');
        $this->load->model('goods_model');
    }
    
    public function index() {
        $now_page = $this->input->get('per_page', TRUE);
        $kwd = $this->input->get('kwd');
        $all_user = $this->cate_model->all_cate($kwd);
        //获取分页数据
        $page_data = $this->cate_model->page_data(20, $now_page, $kwd);
        $view['page_list'] = help_pages($all_user, 20);
        $view['page_data'] = $page_data;
        $view['kwd'] = $kwd;
        $this->load->view('cate/index', $view);
    }
    
    public function add(){
        if($this->input->post('formhash')){
             $data = $this->input->post();
             $addData['name'] = $data['name'];
             $addData['parent_id'] = $data['cate_id'];
             $addData['sort_order']= $data['sort_order'];
//            $addData['images']= $data['fm_img'];
            $addData['publicity_img']= $data['goods_img'];
            $addData['profile']= $data['content'];

            (empty($data['good_tags'])) ? $addData['good_tag']='' : $addData['good_tag']=$data['good_tags'];
            (empty($data['bad_tags'])) ? $addData['bad_tag']='' : $addData['bad_tag']=$data['bad_tags'];


             if($this->cate_model->addData($addData)){
                 $this->showmessage('添加成功','/admin.php/Cate/index');
             }
        }else{
            $this->load->library('qiniu');
            //初始化七牛信息
            header('Access-Control-Allow-Origin:*');
            $bucket = 'xijiaomo';
            $accessKey = 'b2wTxQXjJKx4igvZfSNfp_ZWV5Flg6cHrzJAswaB';
            $secretKey = '5N6gxBSfvKLZRSAuv3yKMTJkiWMfzIIMeCjQsNRW';
            $auth = new Qiniu\Auth($accessKey, $secretKey);
            $upToken = $auth->uploadToken($bucket, null, 3600);//获取上传所需的token
            $qiniu_url = 'http://up-z0.qiniu.com/putb64/-1';
            $read_url = 'http://pic.xijiaomo.com/';
            $data =array();
            $data['uptoken'] = $upToken;
            $data['bucket'] = $bucket;
            $goodsOptionList = $this->goods_model->getGoodsOption();
            $data['goodsOptionList'] = $goodsOptionList;
            if($this->input->get('id')){
                $goodsInfo = $this->goods_model->getGoodsById($this->input->get('id'));
                $data['goodsInfo'] = $goodsInfo;
            }
             $goodsOptionList = $this->cate_model->getGoodsOption();
             $data['goodsOptionList'] = $goodsOptionList;

             $data['tag_list']=$this->db->select('id,tag_name')->from('xjm_comment_tag_set')->order_by('id')->get()->result_array();
             $this->load->view('cate/add',$data);
        }
    }


    public function del()
    {
        if($this->input->post('id')){
            if($this->cate_model->delCateById($this->input->post('id'))){
                echo json_encode(array('status'=>'success','msg'=>'删除成功！','url'=>'/admin.php/Cate/index'));exit;

            }
        }
    }


    public function edit()
    {
        if($this->input->post('formhash')){
            $data = $this->input->post();
//            var_dump($data);die;
            $addData['name'] = $data['name'];
            $addData['parent_id'] = $data['cate_id'];
            $addData['sort_order']= $data['sort_order'];
//            $addData['images']= $data['fm_img'];
            $addData['publicity_img']= $data['goods_img'];
            $addData['profile']= $data['content'];

            (empty($data['good_tags'])) ? $addData['good_tag']='' : $addData['good_tag']=$data['good_tags'];
            (empty($data['bad_tags'])) ? $addData['bad_tag']='' : $addData['bad_tag']=$data['bad_tags'];


            if($this->cate_model->updateData($addData,$data['id'])){
                $this->showmessage('修改成功','/admin.php/Cate/index');
            }
        }else{
            $this->load->library('qiniu');
            //初始化七牛信息
            header('Access-Control-Allow-Origin:*');
            $bucket = 'xijiaomo';
            $accessKey = 'b2wTxQXjJKx4igvZfSNfp_ZWV5Flg6cHrzJAswaB';
            $secretKey = '5N6gxBSfvKLZRSAuv3yKMTJkiWMfzIIMeCjQsNRW';
            $auth = new Qiniu\Auth($accessKey, $secretKey);
            $upToken = $auth->uploadToken($bucket, null, 3600);//获取上传所需的token
            $qiniu_url = 'http://up-z0.qiniu.com/putb64/-1';
            $read_url = 'http://pic.xijiaomo.com/';
            $data =array();

            $data['uptoken'] = $upToken;
            $data['bucket'] = $bucket;
            $goodsOptionList = $this->goods_model->getGoodsOption();
            $data['goodsOptionList'] = $goodsOptionList;
            if($this->input->get('id')){
                $goodsInfo = $this->db->select('*')->from('xjm_goods_option')->where('cate_id='.$this->input->get('id'))->limit(1)->get()->row_array();

                $goodsInfo['good']=$this->db->query('SELECT id,tag_name FROM xjm_comment_tag_set WHERE id IN ('.$goodsInfo["good_tag"].') ORDER BY id')->result_array();

                $goodsInfo['bad']=$this->db->query('SELECT id,tag_name FROM xjm_comment_tag_set WHERE id IN ('.$goodsInfo["bad_tag"].') ORDER BY id')->result_array();

                $data['goodsInfo'] = $goodsInfo;
            }
            $goodsOptionList = $this->cate_model->getGoodsOption();
            $data['goodsOptionList'] = $goodsOptionList;

            $data['tag_list']=$this->db->select('id,tag_name')->from('xjm_comment_tag_set')->order_by('id')->get()->result_array();


            foreach($goodsInfo['good'] as $k=>$v)
            {
                $data['tag_list'][$v['id']-1]['good']=true;
            }

            foreach($goodsInfo['bad'] as $ks=>$vs)
            {
                $data['tag_list'][$vs['id']-1]['bad']=true;
            }

            $this->load->view('cate/edit',$data);
        }
    }
}
