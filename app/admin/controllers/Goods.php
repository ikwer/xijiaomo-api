<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Goods extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('goods_model');
    }
    
    public function index() {
        $now_page = $this->input->get('per_page', TRUE);
        $kwd = $this->input->get('kwd');
        $all_user = $this->goods_model->all_goods($kwd);
        //获取分页数据
        $page_data = $this->goods_model->page_data(15, $now_page, $kwd);
        $view['page_list'] = help_pages($all_user, 15);
        $view['page_data'] = $page_data;
        $view['kwd'] = $kwd;
        $this->load->view('goods/index', $view);
    }
    
    public function add(){
        if($this->input->post('formhash')){
            $data = $this->input->post();
            $addGoods['goods_type'] = $data['cate_id'];
            $addGoods['goods_name'] = $data['name'];
            $addGoods['goods_content'] = $data['content'];
            $addGoods['goods_price'] = $data['price'];
            $addGoods['server_time'] = $data['server_time'];
            $addGoods['goods_image'] = $data['goods_img'];
            $addGoods['add_time'] = time();
            if($this->input->post('id')){
                if($this->goods_model->saveGoods($this->input->post('id'),$addGoods)){
                    $this->showmessage('修改成功','/admin.php/Goods/index');
                }
            }else{
                if($this->goods_model->addGoods($addGoods)){
                    $this->showmessage('添加成功','/admin.php/Goods/index');
                }
            }
        }else{
            $this->load->library('qiniu');
    	    //初始化七牛信息
    	    header('Access-Control-Allow-Origin:*');
    	    $bucket = 'xijiaomo';
    	    $accessKey = 'b2wTxQXjJKx4igvZfSNfp_ZWV5Flg6cHrzJAswaB';
	        $secretKey = '5N6gxBSfvKLZRSAuv3yKMTJkiWMfzIIMeCjQsNRW';
    	    $auth = new Qiniu\Auth($accessKey, $secretKey);
    	    $upToken = $auth->uploadToken($bucket, null, 3600);//获取上传所需的token
    	    $qiniu_url = 'http://up-z0.qiniu.com/putb64/-1';
    	    $read_url = 'http://pic.xijiaomo.com/';
    	    $data =array();
    	    $data['uptoken'] = $upToken;
    	    $data['bucket'] = $bucket;
    	    $goodsOptionList = $this->goods_model->getGoodsOption();
    	    $data['goodsOptionList'] = $goodsOptionList;
    	    if($this->input->get('id')){
    	        $goodsInfo = $this->goods_model->getGoodsById($this->input->get('id'));
    	        $data['goodsInfo'] = $goodsInfo;
    	    }
//    	    var_dump($data);die;
            $this->load->view('goods/add', $data);
        }
    }
    public function del(){
        if($this->input->post('id')){
             if($this->goods_model->delGoodsById($this->input->post('id'))){
                 echo json_encode(array('status'=>'success','msg'=>'删除成功！','url'=>'/admin.php/Goods/index'));exit;
                 
             }
        }
        
    }
    
}
