<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller {
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        if(empty($this->input->post()))
        {
            $query=$this->db->select('option')->from('xjm_option')->where('id=1')->get();
            $view['result']=$query->row_array();
            $view['result']=json_decode($view['result']['option'],true);
            $this->load->view('main/index',$view);
        }
        else
        {
            $data=$this->input->post();
            if(empty($data['slogan']) || empty($data['website']) || empty($data['kf_email']) || empty($data['weixin_code']) || empty($data['kf_qq']) || empty($data['company_phone']) || empty($data['ps']) || empty($data['version']) )
            {
                $arr=array('code'=>-1,'msg'=>'参数错误');
                echo json_encode($arr);die;
            }
            else
            {
                $json['option']=json_encode($data);
                $this->db->where('id=1')->update('xjm_option',$json);
                $arr=array('code'=>0,'msg'=>'修改成功');
                echo json_encode($arr);die;
            }

        }


    }



    public function upload()
    {
        $dir=time();
        $year=date("Y");
        $mon=date("m");
        $targetFolder='/uploads/';

        $tar=@explode("/",$targetFolder);
        unset($tar[count($tar)-1]);
        $targetFolder=@implode("/",$tar);

        //$mkdir=mkdir($targetFolder,0777,true);

        if (!empty($_FILES)) {
            $tempFile = $_FILES['file']['tmp_name'];
            $targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;

            $name_a=@explode(".", $_FILES['file']['name']);
            $count=count($name_a)-1;
            $new_name=$dir.".".$name_a[$count];

            $targetFile = rtrim($targetPath,'/') . '/' . $new_name;
//            var_dump($targetPath,$targetFile);die;
            //判断文件类型
            $fileTypes = array('jpg','png','gif','jpeg'); // File extensions
            $fileParts = pathinfo($_FILES['file']['name']);

            if (in_array($fileParts['extension'],$fileTypes)) {
                echo 'https://api.xijiaomo.com'.$targetFolder.'/'.$new_name;
                move_uploaded_file($tempFile,$targetFile);
            } else {
                echo 'Invalid file type.';
            }
        }
    }



    
    public function attrlist(){
        $attr_url = 'data/attachment/';
        $ext_arr = array('gif', 'jpg', 'jpeg', 'png', 'bmp');
        $dir_name = $this->input->get('dir');
        $path_name = $this->input->get('path');
        $order = $this->input->get('order');
        if(!in_array($dir_name,array('image','flash','media','file'))){
            echo "Invalid Directory name.";
            exit;
        }
        if($dir_name!=''){
            $attr_url .= $dir_name.'/';
            if (!file_exists($attr_url)) {
                mkdir($attr_url);
            }
        }
        $current_path = $attr_url.($path_name!=''?$path_name.'/':'');
        if ($path_name=='') {
            $current_path =$attr_url;
            $current_dir_path = '';
            $moveup_dir_path = '';
        } else {
            $current_path = $attr_url.$path_name.'/';
            $current_dir_path = $path_name;
            $moveup_dir_path = preg_replace('/(.*?)[^\/]+\/$/', '$1', $current_dir_path);
        }
        $order = $order==''?'name':strtolower($order);
        if (preg_match('/\.\./', $current_path)) {
            echo 'Access is not allowed.';
            exit;
        }
        if (!preg_match('/\/$/', $current_path)) {
            echo 'Parameter is not valid.';
            exit;
        }
        if (!file_exists($current_path) || !is_dir($current_path)) {
            echo 'Directory does not exist.';
            exit;
        }
        $this->load->helper('directory');
        $this->load->helper('file');
        $this->load->helper('number');
        $listarr = directory_map($current_path,1);
        $i = 0;
        foreach($listarr as $filename){
            $filepath = $current_path.$filename;
            $file = get_file_info($filepath);
            if(is_dir($filepath)){
                $tmpDir =directory_map($file['server_path'],1);
                $file_list[$i]['is_dir'] = true;
                $file_list[$i]['has_file'] = (count($tmpDir) > 0);
                $file_list[$i]['filesize'] = 0;
                $file_list[$i]['is_photo'] = false;
                $file_list[$i]['filetype'] = '';
                unset($tmpDir);
            }else{
                $file_list[$i]['is_dir'] = false;
                $file_list[$i]['has_file'] = false;
                $file_list[$i]['filesize'] = $file['size'];
                $file_list[$i]['dir_path'] = '';
                $file_ext = strtolower(get_suffix($file['server_path']));
                $file_list[$i]['is_photo'] = in_array($file_ext, $ext_arr);
                $file_list[$i]['filetype'] = $file_ext;
            }
            $file_list[$i]['filename'] = $filename; //文件名，包含扩展名
            $file_list[$i]['datetime'] = date('Y-m-d H:i:s', $file['date']); //文件最后修改时间
            $i++;
        }
        usort($file_list, 'cmp_func');
        $result = array();
        $result['moveup_dir_path'] = $moveup_dir_path;
        $result['current_dir_path'] = $current_dir_path;
        $result['current_url'] = $current_path;
        $result['total_count'] = count($file_list);
        $result['file_list'] = $file_list;
        echo json_encode($result);
    }
    
    function attrupload(){
        $usergroupid=$this->session->userdata('usergroup');
        if(!$usergroupid){
            $result = array('error'=>1,'message'=>lang('nopur'));
            echo json_encode($result);exit;
        }
        $save_path = 'data/attachment/';
        $ext_arr = array(
            'image' => array('gif', 'jpg', 'jpeg', 'png', 'bmp'),
            'flash' => array('swf', 'flv'),
            'media' => array('swf', 'flv', 'mp3', 'wav', 'wma', 'wmv', 'mid', 'avi', 'mpg', 'rm', 'rmvb'),
            'file' => array('gif', 'jpg', 'jpeg', 'png', 'bmp','doc', 'docx', 'xls', 'xlsx', 'ppt', 'htm', 'html', 'txt', 'zip', 'rar', 'gz', 'bz2'),
        );
        $attrconfig = $this->Cache_model->loadConfig('attr');
        $dir_name = $this->input->get('dir');
        if(!in_array($dir_name,array('image','flash','media','file'))){
            $result = array('error'=>1,'message'=>"Invalid Directory name.");
            echo json_encode($result);
        }
        $save_path .= $dir_name.'/';
        if (!file_exists($save_path)) {
            mkdir($save_path);
        }
        $save_path .= date('Ymd').'/';
        if (!file_exists($save_path)) {
            mkdir($save_path);
        }
        $uploadconfig['upload_path'] = $save_path;
        $uploadconfig['allowed_types'] = implode('|',$ext_arr[$dir_name]);
        $uploadconfig['max_size'] = $attrconfig['attr_maxsize']?$attrconfig['attr_maxsize']:0;
        $uploadconfig['encrypt_name']  = TRUE;
        $uploadconfig['remove_spaces']  = TRUE;
        $this->load->library('upload', $uploadconfig);
        if(!$this->upload->do_upload('imgFile')){
            $result = array('error'=>1,'message'=>$this->upload->display_errors('',''));
        }else{
            $data = $this->upload->data();
            if($this->input->post('iswater')==1&&$dir_name=='image'&&$attrconfig['water_type']>0){
                $this->load->library('image_lib');
                $waterconfig['source_image'] = $save_path.$data['file_name'];
                $waterconfig['quality'] = $attrconfig['water_quality'];
                $waterconfig['wm_padding'] = $attrconfig['water_padding'];
    
                switch($attrconfig['water_position']){
                    case 'topleft':
                        $waterconfig['wm_vrt_alignment'] = 'top';
                        $waterconfig['wm_hor_alignment'] = 'left';
                        break;
                    case 'topcenter':
                        $waterconfig['wm_vrt_alignment'] = 'top';
                        $waterconfig['wm_hor_alignment'] = 'center';
                        break;
                    case 'topright':
                        $waterconfig['wm_vrt_alignment'] = 'top';
                        $waterconfig['wm_hor_alignment'] = 'right';
                        break;
                    case 'middleleft':
                        $waterconfig['wm_vrt_alignment'] = 'middle';
                        $waterconfig['wm_hor_alignment'] = 'left';
                        break;
                    case 'middlecenter':
                        $waterconfig['wm_vrt_alignment'] = 'middle';
                        $waterconfig['wm_hor_alignment'] = 'center';
                        break;
                    case 'middleright':
                        $waterconfig['wm_vrt_alignment'] = 'middle';
                        $waterconfig['wm_hor_alignment'] = 'right';
                        break;
                    case 'bottomleft':
                        $waterconfig['wm_vrt_alignment'] = 'bottom';
                        $waterconfig['wm_hor_alignment'] = 'left';
                        break;
                    case 'bottomcenter':
                        $waterconfig['wm_vrt_alignment'] = 'bottom';
                        $waterconfig['wm_hor_alignment'] = 'center';
                        break;
                    case 'bottomright':
                        $waterconfig['wm_vrt_alignment'] = 'bottom';
                        $waterconfig['wm_hor_alignment'] = 'right';
                        break;
                    default:
                        $waterconfig['wm_vrt_alignment'] = 'bottom';
                        $waterconfig['wm_hor_alignment'] = 'right';
                        break;
                }
                if($attrconfig['water_type']==1){
                    $waterconfig['wm_type'] = 'overlay';
                    $waterconfig['wm_overlay_path'] = $attrconfig['water_image_path'];
                    $waterconfig['wm_opacity'] = $attrconfig['water_opacity'];
                }elseif($attrconfig['water_type']==2){
                    $waterconfig['wm_type'] = 'text';
                    $waterconfig['wm_text'] = $attrconfig['water_text_value'];
                    $waterconfig['wm_font_path'] = $attrconfig['water_text_font'];
                    $waterconfig['wm_font_size'] = $attrconfig['water_text_size'];
                    $waterconfig['wm_font_color'] = $attrconfig['water_text_color'];
                }
                $this->image_lib->initialize($waterconfig);
                $this->image_lib->watermark();
            }
            $result = array('error'=>0,'url'=>base_url($save_path.$data['file_name']));
        }
        echo json_encode($result);
    }
}
