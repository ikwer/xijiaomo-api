<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('order_model');
        $this->load->model('user_model');

        $this->orderStatus = array('正常状态','待支付','已完成','已失效','退款','技师开始服务','服务中','用户发起退款','技师同意退款','技师不同意退款');
        
        $this->config->load('option');
    }
    
   public function index() {

        $param=$this->input->post();
        if(!empty($param))
        {
            $r=$this->order_model->tuidan($param['order_id']);
            if($r['status']){
                $json=array('code'=>0,'msg'=>$r['msg']);
            }else{
                $json=array('code'=>-1,'msg'=>$r['msg']);
            }
            echo json_encode($json);die;
        }
        else
        {
            $now_page = $this->input->get('per_page', TRUE);
            $kwd = $this->input->get('kwd');
            $user = $this->input->get('kwd2');
            $all_order = $this->order_model->all_order($kwd,$user);
            //获取分页数据
            $orderStatus = $this->orderStatus;
            $page_data = $this->order_model->page_data(15, $now_page, $kwd,$user);
            $view['page_list'] = help_pages($all_order, 15);
            $view['page_data'] = $page_data;
            $view['kwd'] = $kwd;
            $view['order_status'] = $orderStatus;
            $this->load->view('order/index', $view);
        }


    }

    public function cash_list()
    {
        $now_page = $this->input->get('per_page', TRUE);
        $kwd = $this->input->get('kwd');
        $all_order = $this->order_model->all_techer_cash($kwd);
        //获取分页数据
        $page_data = $this->order_model->page_techer_data(10, $now_page, $kwd);
        $view['page_list'] = help_pages($all_order, 10);
        $view['page_data'] = $page_data;
        $view['kwd'] = $kwd;
        $view['cash_option']=$this->config->item('cash_option');
        $view['cash_status']=$this->config->item('cash_status');
        $this->load->view('order/cash_list', $view);
    }


    public function cash_info()
    {
        $id=$this->input->get('id', TRUE);
        $view['cash_info']=$this->order_model->get_techercash_one($id);
        $view['area_list']=$this->config->item("area_list");
        $view['cash_option']=$this->config->item('cash_option');
        $view['sex_option']=$this->config->item('sex_option');
        $view['cash_status']=$this->config->item('cash_status');
//        print_r($view);die;
        $this->load->view('order/cash_info', $view);
    }


    //退单列表
    public function tuidan_list()
    {
        $now_page = $this->input->get('per_page', TRUE);
        $kwd = $this->input->get('kwd');
        $all_order = $this->order_model->all_tuidan($kwd);
        $page_data = $this->order_model->tuidan_data(15, $now_page, $kwd);
        $view['page_list'] = help_pages($all_order, 15);
        $view['page_data'] = $page_data;
        $view['kwd'] = $kwd;
        $this->load->view('order/tuidan_list', $view);
    }

    //提现审核通过
    public function cash_success()
    {
        $id=$this->input->get('id');
        if(empty($id))
        {
            $array=array("code"=>"-1","msg"=>"非法请求");
        }
        else
        {
            $result = $this->order_model->cash_success($id);
            if($result['status'])
            {
                $array=array("code"=>"0","msg"=>$result['msg']);
            }
            else
            {
                $array=array("code"=>"-1","msg"=>$result['msg']);
            }
        }

        echo json_encode($array);die;
    }

    //提现审核驳回
    public function cash_fail()
    {
        $id=$this->input->get("id");
        if(empty($id) || !isset($id))
        {
            $array=array("code"=>"-1","msg"=>"非法请求");
        }
        else
        {
            $result = $this->order_model->cash_fail($id);
            if($result)
            {
                $array=array("code"=>"0","msg"=>"审核驳回");
            }
            else
            {
                $array=array("code"=>"-1","msg"=>"审核异常");
            }
        }

        echo json_encode($array);die;
    }


    //退款列表
    public function refund_list()
    {
        $param=$this->input->post();
        if(!empty($param)){
            $arr=$this->order_model->refund_order($param);
            echo json_encode($arr);die;
        }
        else
        {
            $now_page = $this->input->get('per_page', TRUE);
            $kwd = $this->input->get('kwd');
            $all_order = $this->order_model->all_refund($kwd);
            //获取分页数据
            $orderStatus = $this->orderStatus;
            $page_data = $this->order_model->refund_data(15, $now_page, $kwd);
            $view['page_list'] = help_pages($all_order, 15);
            $view['page_data'] = $page_data;
            $view['kwd'] = $kwd;
            $view['refund_status']=$this->config->item('refund_status');
            $this->load->view('order/refund_list', $view);
        }

    }


    //订单走势
    public function order_trend()
    {
        $sdate=$this->input->get('sdate');
        $edate=$this->input->get('edate');

        $view['charts']=$this->user_model->order_trend_charts($sdate,$edate);
        $view['table_data']=$this->user_model->order_trend_data($sdate,$edate);

        $this->load->view('user/order_trend',$view);
    }




}
