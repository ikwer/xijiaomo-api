<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Techer extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('techer_model');
        $this->config->load('option');
    }
/*
 * 技师列表
 */
    public function techer_list() {
        $param=$this->input->get('tid');
        if(!empty($param)){
            $type=$this->input->get('type');
            if(!empty($type)){
                $now_page = $this->input->get('per_page', TRUE);
                $kwd = $this->input->get('kwd');
                $all_user = $this->techer_model->all_income($kwd,$param);
                //获取分页数据
                $page_num = 10;
                $page_data = $this->techer_model->income_list($page_num, $now_page, $kwd,$param);
                $view['page_list'] = help_pages($all_user, $page_num);
                $view['page_data'] = $page_data;
                $view['kwd'] = $kwd;
                $view['sex'] = $this->config->item('sex_option');
                $this->load->view('techer/techer_income', $view);
            }else{
                $now_page = $this->input->get('per_page', TRUE);
                $kwd = $this->input->get('kwd');
                $all_user = $this->techer_model->all_share($kwd,$param);
                //获取分页数据
                $page_num = 10;
                $page_data = $this->techer_model->share_list($page_num, $now_page, $kwd,$param);
                $view['page_list'] = help_pages($all_user, $page_num);
                $view['page_data'] = $page_data;
                $view['kwd'] = $kwd;
                $view['sex'] = $this->config->item('sex_option');
                $this->load->view('techer/share_index', $view);
            }

        }else{
            $now_page = $this->input->get('per_page', TRUE);
            $kwd = $this->input->get('kwd');
            $all_user = $this->techer_model->all_techer($kwd);
            //获取分页数据
            $page_num = 10;
            $page_data = $this->techer_model->page_data($page_num, $now_page, $kwd);
            $view['page_list'] = help_pages($all_user, $page_num);
            $view['page_data'] = $page_data;
            $view['kwd'] = $kwd;
            $view['sex'] = $this->config->item('sex_option');
            $this->load->view('techer/index', $view);
        }
    }
/*
* 技师详情
*/  
   public function techer_info(){
        $uid = $this->uri->segment(4);
        $techerInfoArr = $this->techer_model->getTecherInfoByUid($uid);
        $view['techerInfo'] = $techerInfoArr;
        $view['card_success'] = $this->config->item('card_success');
        $view['techer_status'] = $this->config->item('techer_status');
        $view['sex'] = $this->config->item('sex_option');
        $view['success_status']=$this->config->item('success_status');
        $this->load->view('techer/techer_info',$view);
   } 
 /*
  * 删除技师
  */  
   public function techer_del(){
       $tid = $this->input->get('id');
       if($tid){
           $rs = $this->techer_model->techer_dele($tid);
           if($rs){
               echo json_encode(array('status'=>'success','删除成功','url'=>'/Techer/techer_list'));exit;
           }
       }
   }
   
/*
 * 技师审核
 */    
   public function passfn(){
       $tid = $this->input->get('tid');
       $type = $this->input->get('type');
       $per_page = $this->input->get('per_page');
       if($tid && $type ){
            if($this->techer_model->saveStatus($tid,$type)){
                //$this->showmessage('操作成功', HTTP_REFERER);
                //echo json_encode(array('succes'))
                  $url =   $per_page?'admin.php/Techer/techer_list?per_page='.$per_page:'/admin.php/Techer/techer_list';
                  echo json_encode(array('status'=>'success','操作成功','url'=>$url));exit;
            }
       }
   } 
   /*
    * 添加技师
    */
   public function techer_add(){
       $this->load->helper('form');
       if($this->input->post('formhash')){
            $data = $this->input->post();
            $techerArr['nickname'] = $data['loginname'];
            $techerArr['mobile'] = $data['mobile'];
            $techerArr['skill_level'] = $data['skill_level'];
            $techerArr['money'] = $data['money'];
            $techerInfoArr['card'] = $data['card'];
            $techerInfoArr['sex'] = $data['sex'];
            $lastId = $this->techer_model->addTecher($techerArr,'xjm_techer');
            if($lastId){
                 $techerInfoArr['tid'] = $lastId;
                 $lastId =  $this->techer_model->addTecher($techerInfoArr,'xjm_techer_checkinfo');
            }
            if($lastId){
                echo json_encode(array('status'=>'success','添加成功','url'=>'/Techer/techer_list'));exit;
            }
       }
       $this->load->view('techer/techer_add');
   }


    //技师评论列表
    public function feedback()
    {
        $now_page = $this->input->get('per_page', TRUE);
        $kwd = $this->input->get('kwd');
        $all_user_feedback = $this->techer_model->all_techer_feedback($kwd);
        //获取分页数据
        $page_num = 8;
        $page_data = $this->techer_model->page_techer_feedback_data($page_num, $now_page, $kwd);
        $view['page_list'] = help_pages($all_user_feedback, $page_num);
        $view['page_data'] = $page_data;
        $view['kwd'] = $kwd;
        $this->load->view('techer/feedback', $view);
    }

    //技师评论详情
    public function feedback_info()
    {
        $id=$this->input->get('id');
        $view['cash_info']=$this->techer_model->techer_feedback_info($id);
        $view['sex_option']=$this->config->item("sex_option");
        $this->load->view('techer/feedback_info', $view);
    }



    //添加投诉
    public function add_tousu()
    {
        $get=$this->input->get();
        $param=$this->input->post();
        if(empty($get) && empty($param))
        {
            $this->load->view('techer/add_tousu');
        }
        elseif(!empty($get)){
                $query=$this->db->select('o.tid,o.order_id,o.order_sn,u.nickname uname,t.nickname tname,o.goods_name,o.goods_price,o.add_time,o.date_time,o.server_time,o.order_amount,o.xiaofei')->
                from('xjm_order_list o')->
                join('xjm_user u','o.uid=u.uid','left')->
                join('xjm_techer t','o.tid=t.tid','left')->
                where(array('o.order_sn'=>$get['order_sn']))->
                get()->row_array();

                if(!empty($query)){
                    $arr=array('code'=>0,'msg'=>'数据获取成功','data'=>$query);
                    echo json_encode($arr);die;
                }else{
                    $arr=array('code'=>-1,'msg'=>'不存在的订单');
                    echo json_encode($arr);die;
                }
        }else{
            $nums=$this->db->select('id')->from('xjm_tousu_log')->where(array('order_id'=>$param['order_id']))->get()->num_rows();
            if($nums>0){
                $arr=array('code'=>-2,'msg'=>'同一笔订单无法添加两次投诉');
            }else{
                $data=array(
                    'order_id'=>$param['order_id'],
                    'tid'=>$param['tid'],
                    'content'=>$param['content'],
                    'add_time'=>time(),
                    'admin_user'=>$_SESSION['admin']['uid']
                );
                $str=$this->db->insert('xjm_tousu_log',$data);
                if($str){
                    $arr=array('code'=>0,'msg'=>'投诉添加成功');
                }else{
                    $arr=array('code'=>-1,'msg'=>'投诉添加失败');
                }
            }

            echo json_encode($arr);die;
        }
    }


    //投诉列表
    public function tousu_list()
    {
        $now_page = $this->input->get('per_page', TRUE);
        $kwd = $this->input->get('kwd');
        $all_user = $this->techer_model->all_tousu($kwd);
        //获取分页数据
        $page_num = 10;
        $page_data = $this->techer_model->tousu_list($page_num, $now_page, $kwd);
        $view['page_list'] = help_pages($all_user, $page_num);
        $view['page_data'] = $page_data;
        $view['kwd'] = $kwd;
        $this->load->view('techer/tousu_list', $view);
    }

    


    //调用京东万象 身份证一致性核验 接口
    public function check_idcard()
    {
        $id=$this->input->get("id");
        $c_from=$this->input->get('c_from');

        if($c_from=='id_card')
        {
            $r=$this->techer_model->check_idcard($id);
            if($r['status'])
            {
                $arr=array("code"=>0,"msg"=>$r['msg']);
            }
            else
            {
                $arr=array("code"=>-1,"msg"=>$r['msg']);
            }
            echo json_encode($arr);
        }
        else
        {
            $opt=$this->input->get('opt');
            //头像审核
            if($c_from=='head')
            {
                //头像审核通过将通过的图片路径存入keyong_headurl中
                $infos=$this->db->select('headurl')->from('xjm_techer_checkinfo')->where(array('tid'=>$id))->get()->row_array();
                $str=$this->db->where(array('tid'=>$id))->update('xjm_techer_checkinfo',array('headurl_status'=>$opt));
                if($str)
                {
                    if($opt==1)
                    {
                        $array=array(
                          'tid'=>$id,
                          'type'=>0,
                          'common_id'=>3,
                          'title'=>'您的头像已经审核通过',
                            'content'=>'您的头像已经审核通过',
                            'add_time'=>time(),
                            'is_read'=>0
                        );

                        $new_info=array('keyong_headurl'=>$infos['headurl']);
                        $this->db->where(array('tid'=>$id))->update('xjm_techer_checkinfo',$new_info);
                    }
                    else
                    {
                        $array=array(
                            'tid'=>$id,
                            'type'=>0,
                            'common_id'=>3,
                            'title'=>'您的头像没有通过审核',
                            'content'=>'您的头像没有通过审核',
                            'add_time'=>time(),
                            'is_read'=>0
                        );
                    }
                    $this->db->insert('xjm_techer_notice',$array);
                    $arr=array("code"=>0,"msg"=>'操作成功');
                }
                else
                {
                    $arr=array("code"=>-1,"msg"=>'数据异常');
                }
            }
            //相册审核
            elseif($c_from=='pic_arr')
            {
                $str=$this->db->where(array('id'=>$id))->update('xjm_techer_photo',array('pic_status'=>$opt));
                if($str)
                {
                    $arr=array("code"=>0,"msg"=>'操作成功');
                }
                else
                {
                    $arr=array("code"=>-1,"msg"=>'数据异常');
                }

            }
            //视频审核
            elseif($c_from=='video_arr')
            {
                $str=$this->db->where(array('id'=>$id))->update('xjm_techer_photo',array('video_status'=>$opt));

                if($str)
                {
                    $tid=$this->db->select('tid')->from('xjm_techer_photo')->where(array('id'=>$id))->get()->row_array();
                    if($opt==1)
                    {
                        $array=array(
                            'tid'=>$tid['tid'],
                            'type'=>0,
                            'common_id'=>3,
                            'title'=>'您的视频已经审核通过',
                            'content'=>'您的视频已经审核通过',
                            'add_time'=>time(),
                            'is_read'=>0
                        );
                    }
                    else
                    {
                        $array=array(
                            'tid'=>$tid['tid'],
                            'type'=>0,
                            'common_id'=>3,
                            'title'=>'您的视频没有通过审核',
                            'content'=>'您的视频没有通过审核',
                            'add_time'=>time(),
                            'is_read'=>0
                        );
                    }
                    $this->db->insert('xjm_techer_notice',$array);
                    $arr=array("code"=>0,"msg"=>'操作成功');
                }
                else
                {
                    $arr=array("code"=>-1,"msg"=>'数据异常');
                }
            }
            else
            {
                $arr=array("code"=>-1,"msg"=>'非法请求');
            }
            echo json_encode($arr);die;
        }

    }
   
   
}
