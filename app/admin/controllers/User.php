<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('user_model');
    }
    
    public function index() {
        

        $param=$this->input->get('uid');
        if(!empty($param)){
            $now_page = $this->input->get('per_page', TRUE);
            $kwd = $this->input->get('kwd');
            $all_user = $this->user_model->all_share($kwd,$param);
            //获取分页数据
            $page_num = 8;
            $page_data = $this->user_model->page_share($page_num, $now_page, $kwd,$param);
            $view['page_list'] = help_pages($all_user, $page_num);
            $view['page_data'] = $page_data;
            $view['kwd'] = $kwd;
            $this->load->view('user/share_index', $view);
        }else{
            $now_page = $this->input->get('per_page', TRUE);
            $kwd = $this->input->get('kwd');
            $all_user = $this->user_model->all_user($kwd);
            //获取分页数据
            $page_num = 8;
            $page_data = $this->user_model->page_data($page_num, $now_page, $kwd);
            $view['page_list'] = help_pages($all_user, $page_num);
            $view['page_data'] = $page_data;
            $view['kwd'] = $kwd;
            $this->load->view('user/index', $view);
        }
    }

    //用户评论列表
    public function feedback()
    {
        $now_page = $this->input->get('per_page', TRUE);
        $kwd = $this->input->get('kwd');
        $all_user_feedback = $this->user_model->all_user_feedback($kwd);
        //获取分页数据
        $page_num = 8;
        $page_data = $this->user_model->page_user_feedback_data($page_num, $now_page, $kwd);
        $view['page_list'] = help_pages($all_user_feedback, $page_num);
        $view['page_data'] = $page_data;
        $view['kwd'] = $kwd;
        $this->load->view('user/feedback', $view);
    }

    //用户评论详情
    public function feedback_info()
    {
        $id=$this->input->get('id');
        $view['cash_info']=$this->user_model->user_feedback_info($id);
        $this->load->view('user/feedback_info', $view);
    }

    //每日新增用户&技师
    public function everyday_user()
    {
        $sdate=$this->input->get('sdate');
        $edate=$this->input->get('edate');
        $view['charts_data']=$this->user_model->everyday_user_charts($sdate,$edate);
        $view['table_data']=$this->user_model->everyday_user_data($sdate,$edate);
        $this->load->view('user/everyday_user',$view);
    }


    //用户分布
    public function fenbu_user()
    {
        $view['charts_data']=$this->user_model->fenbu_user_charts();
        $this->load->view('user/fenbu_user',$view);
    }


    //违禁词管理
    public function warning_msg()
    {
        $param=$this->input->post();
        if(!empty($param)){
            $where=array('id'=>2);
            $this->db->where($where)->update('xjm_option',$param);
            $this->showmessage('修改成功！', '/admin.php/User/warning_msg');
        }else{
            $view['data']=$this->db->select('option')->from('xjm_option')->where('id=2')->get()->row_array();
            $this->load->view('user/warning_msg',$view);
        }

    }


}
