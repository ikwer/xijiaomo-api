<?php

//分页助手
function help_pages($all_num, $per_page = 19) {
    if ($all_num <= $per_page) {
        return '';
    }
    $CI = & get_instance();
    $index = SITE_URL;
    $class = $CI->router->fetch_class();
    $fun = $CI->router->fetch_method();
    $config['base_url'] = '/admin.php/'.$index . $class . '/' . $fun;
    $config['total_rows'] = $all_num;
    $config['per_page'] = $per_page;
    //样式定制
    $config['num_links'] = 3;
    $config['num_links'] = 3;
    $config['use_page_numbers'] = TRUE;
    $config['page_query_string'] = TRUE;
    $config['full_tag_open'] = '';
    $config['full_tag_close'] = '';
    $config['first_link'] = '首页';
    $config['last_link'] = '尾页';
    $config['reuse_query_string'] = TRUE;
    $CI->load->library('pagination');
    $CI->pagination->initialize($config);
    return $CI->pagination->create_links();
}

//页面跳转
function help_success($msg = '', $url = '') {
    if (!$msg) {
        $msg = '操作成功，';
    } else {
        $msg = $msg . ',';
    }
    if ($url) {
        $host = 'http://' . $_SERVER['HTTP_HOST'];
        $back_url = $host . '/' . $url;
    } else {
        $back_url = current_url();
    }
    echo '<html><head><meta http-equiv="refresh" content="3;url=' . $back_url . '"></head>
    <body> ' . $msg . '页面将在三秒后跳转,清稍等！<a href="' . $back_url . '">点击直接跳转</a></body></html>';
    exit;
}

function help_error($msg = '', $url = '') {
    if (!$msg) {
        $msg = '操作失败，';
    } else {
        $msg = $msg . ',';
    }
    if ($url) {
        $host = 'http://' . $_SERVER['HTTP_HOST'];
        $back_url = $host . '/' . $url;
    } else {
        $back_url = current_url();
    }
    echo '<html><head><meta http-equiv="refresh" content="3;url=' . $back_url . '"></head>
    <body style="color: red;"> ' . $msg . '页面将在三秒后跳转,清稍等！<a href="' . $back_url . '">点击直接跳转</a></body></html>';
    exit;
}
