<?php

/**
 * Description of m_user
 *
 * @author acer
 */
class Adminuser_model extends CI_Model {

//    public function __construct() {
//        parent::__construct();
//        // Your own constructor code
//    }
    //所有用户
    public function all_user($kwd) {
        $where = array('login_name' => $kwd);
        return $rs = $this->db
                ->select('uid')
                ->from('xjm_admin_user')
                ->like($where)
                ->count_all_results();
    }

    //获取分页数据
    public function page_msg($page_num, $now_page = 1, $kwd) {
        --$now_page;
        $rs = $this->db->select('uid,login_name,menu_leval,status,name')
                ->from('xjm_admin_user')
                ->join('xjm_admin_menu_level', 'xjm_admin_menu_level.id = xjm_admin_user.menu_leval', 'left')
                ->where(array())
                ->like('login_name', $kwd)
                ->order_by('time')
                ->limit($page_num, $now_page * $page_num)
                ->get();
        return $rs->result_array();
    }



    public function all_pro($kwd)
    {
        $where = array('name' => $kwd);
        return $rs = $this->db
            ->select('id')
            ->from('xjm_city')
            ->where('aid=0')
            ->like($where)
            ->count_all_results();
    }



    public function page_pro($page_num, $now_page = 1, $kwd)
    {
        --$now_page;
        $where = array('name' => $kwd);
        $rs = $this->db->select('*')
            ->from('xjm_city')
            ->where('aid=0')
            ->like($where)
            ->order_by('id')
            ->limit($page_num, $now_page * $page_num)
            ->get();
        return $rs->result_array();
    }




    //管理员信息
    public function user_msg($uid) {
        $where['uid'] = $uid;
        $rs = $this->db->select('uid,login_name,menu_leval,status,area')
                ->from('xjm_admin_user')
                ->where($where)
                ->get();
        return $rs->row_array();
    }

    //管理员等级
    public function menu_level() {
        $livel = "SELECT id,name FROM xjm_admin_menu_level";
        return $this->db->query($livel)->result_array();
    }

    //城区列表
    public function area_list() {
        $sql = "SELECT id,area_name FROM xjm_area_list GROUP BY time";
        return $this->db->query($sql)->result_array();
    }

    //修改信息
    public function edit_user($uid, $save_msg) {
        if (!$uid || !is_array($save_msg)) {
            return false;
        }
        return $this->db->where('uid', $uid)->update('xjm_admin_user', $save_msg);
    }

    //添加管理员
    public function add_user($add) {
        //检测是否已存在
        $where['login_name'] = $add['login_name'];
        $rs = $this->db->select('uid')->from('xjm_admin_user')->where($where)->get();
        if ($rs->row_array()) {
            return false;
        }
        return $this->db->insert('xjm_admin_user', $add);
    }

    public function del_user($uid) {
        if (empty($uid)) {
            return false;
        }
        return $this->db->where('uid', $uid)->delete('xjm_admin_user');
    }

}
