<?php
/**
 * Description of m_user
 *
 * @author acer
 */
class Article_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    //所有文章
    public function all_article($kwd) {
        $where = array('goods_name' => $kwd);
        return $rs = $this->db
        ->select('id')
        ->from('xjm_article')
        ->count_all_results();
    }

    //添加文章
    public function addArticle($data){
       return  $this->db->insert('xjm_article',$data);
        
    }
    
    public function saveArticle($article_id,$data){
        unset($data['add_time']);
        return  $this->db->where(['id'=>$article_id])
                ->update('xjm_article',$data);
    
    }
    
    //获取分页数据
    public function page_data($page_num=10, $now_page = 1, $kwd) {
        --$now_page;

        $rs = $this->db->select('id,title,type,status,description,FROM_UNIXTIME(set_time,"%Y-%m-%d %H:%i:%s") set_times,sort_order')
        ->from('xjm_article')
        ->like('title',$kwd)
        ->order_by('id desc')
        ->limit($page_num, $now_page * $page_num)
        ->get();
        return $rs->result_array();
    }
    //获取单篇文章详情
    public function getArticleById($article_id){
        $rs = $this->db->select("*")
            ->from('xjm_article')
            ->where(['id'=>$article_id])
            ->limit(1)
            ->get();
        return $rs->row_array();
    }
    //删除文章
    public function delArticleById($id){
       return $this->db->delete('xjm_article',array('id'=>$id));
    }
  
    
}

?>
