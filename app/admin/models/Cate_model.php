<?php
/**
 * Description of m_user
 *
 * @author acer
 */
class Cate_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    //所有分类
    public function all_cate($kwd) {
        $where = array('name' => $kwd);
        return $rs = $this->db
        ->select('cate_id')
        ->from('xjm_goods_option')
        ->like($where)
        ->count_all_results();
    }
    
    //获取分页数据
    public function page_data($page_num=10, $now_page = 1, $kwd) {
        --$now_page;
        $rs = $this->db->select('a.*,b.name parent_name')
        ->from('xjm_goods_option a')
        ->join('xjm_goods_option b','b.cate_id=a.parent_id','left') 
        ->where(array())
        ->like('a.name', $kwd)
        ->order_by('a.cate_id')
        ->limit($page_num, $now_page * $page_num)
        ->get();
        return $rs->result_array();
    }
    //获取产品一级分类
    public function getGoodsOption(){
        $rs = $this->db->select('cate_id,name')
        ->from('xjm_goods_option')
        ->order_by('cate_id desc')
        ->where(['parent_id'=>0])
        ->get();
        return $rs->result_array();
    }
    //添加产品分类
    public function addData($data){
       if(is_array($data)){
            if(is_array($data['good_tag']))
            {
                $data['good_tag']=@implode(',',$data['good_tag']);
            }
           if(is_array($data['bad_tag']))
           {
               $data['bad_tag']=@implode(',',$data['bad_tag']);
           }
            return $this->db->insert('xjm_goods_option',$data);
       }
    }



    public function updateData($data,$id)
    {
//        var_dump($data);die;
        if(is_array($data)){
            if(is_array($data['good_tag']))
            {
                $data['good_tag']=@implode(',',$data['good_tag']);
            }
            if(is_array($data['bad_tag']))
            {
                $data['bad_tag']=@implode(',',$data['bad_tag']);
            }
            return $this->db->update('xjm_goods_option',$data,array('cate_id'=>$id));
        }
    }

    //删除产品
    public function delCateById($id){
        return $this->db->delete('xjm_goods_option',array('cate_id'=>$id));
    }
}

?>
