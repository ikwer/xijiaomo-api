<?php
/**
 * Description of m_user
 *
 * @author acer
 */
class Goods_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    //所有产品
    public function all_goods($kwd) {
        $where = array('goods_name' => $kwd);
        return $rs = $this->db
        ->select('goods_id')
        ->from('xjm_goods')
        ->like($where)
        ->count_all_results();
    }
    //获取产品分类
    public function getGoodsOption(){
        $rs = $this->db->select('cate_id,name')
        ->from('xjm_goods_option')
        ->order_by('cate_id desc')
        ->get();
        return $rs->result_array();
    }
    //添加产品
    public function addGoods($data){
       return  $this->db->insert('xjm_goods',$data);
        
    }
    
    public function saveGoods($goods_id,$data){
        return  $this->db->where(['goods_id'=>$goods_id])
                ->update('xjm_goods',$data);
    
    }
    
    //获取分页数据
    public function page_data($page_num, $now_page = 1, $kwd) {
        --$now_page;
        $rs = $this->db->select('a.*,b.name')
        ->from('xjm_goods a')
        ->join('xjm_goods_option b','a.goods_type=b.cate_id')
        ->where(array())
        ->like('a.goods_name', $kwd)
        ->order_by('a.goods_id')
        ->limit($page_num, $now_page * $page_num)
        ->get();
        return $rs->result_array();
    }
    public function getGoodsById($goods_id){
        $rs = $this->db->select("*")
            ->from('xjm_goods')
            ->where(['goods_id'=>$goods_id])
            ->get();
        return $rs->row_array();
    }
    //删除产品
    public function delGoodsById($id){
       return $this->db->delete('xjm_goods',array('goods_id'=>$id));
    }
  
    
}

?>
