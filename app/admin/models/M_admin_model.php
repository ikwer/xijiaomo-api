<?php

/**
 * Description of m_user
 *
 * @author acer
 */
class M_admin_model extends CI_Model {

    const TABLE_ADMIN_ACCOUNT = 'xjm_admin_user';

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->library('session');
    }

    protected function is_login() {
        if (isset($_SESSION['admin'])) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function login($login_name, $pwd) {
        $query = 'select uid,login_name,pwd,menu_leval,status from ' . self::TABLE_ADMIN_ACCOUNT . ' where login_name="' . $login_name . '" AND status = 1 AND pwd="' . sha1(md5(md5($pwd))) . '"';
        $admin_info = $this->db->query($query)->row_array();
        if ($admin_info && $admin_info['status'] == 1) {
            //获取权限列表
//            $auth_list = $this->menu_list($admin_info['menu_leval']);
            $_SESSION['admin'] = $admin_info;
//            $_SESSION['auth_list'] = $auth_list;
            $expire = time() + 3600 * 24 * 7;
            setcookie('M_login_name', $admin_info['login_name'], $expire, '/');
            setcookie('M_login_time', time(), $expire, '/');
            return true;
        } else {
            return false;
        }
    }

    //获取权限列表
    private function menu_list($menu_leval = '') {
        $livel = "SELECT id,name,menuIds FROM xjm_admin_menu_level WHERE id = $menu_leval";
        $admin_info = $this->db->query($livel)->row_array();
        $auth_sql = "SELECT id,cate_id,level,name,class,function FROM xjm_admin_menu WHERE  id IN (" . $admin_info['menuIds'] . ") AND status=1 ORDER BY cate_id,sort";
        $menu_list = $this->db->query($auth_sql)->result_array();
        $main_menu_list = array();
        $child_menu_list = array();
        foreach ($menu_list as $ls) {
            if ($ls['level'] == 1) {
                $main_menu_list[] = $ls;
            }
            if ($ls['level'] == 2) {
                $child_menu_list[$ls['cate_id']][$ls['id']] = $ls;
            }
        }
        foreach ($main_menu_list as $key => $value) {
            if (isset($child_menu_list[$value['id']])) {
                $main_menu_list[$key]['child'] = $child_menu_list[$value['id']];
            } else {
                $main_menu_list[$key]['child'] = array();
            }
        }
        return array('left_list' => $main_menu_list, 'menu_list' => $admin_info['menuIds']);
    }

    //检测是否有当前操作权限$class = '', $fun = ''
    public function check_behavior() {
        //检测登陆
        $login_rs = $this->is_login();
        if (empty($login_rs)) {
            $this->session->sess_destroy();
            header('Location: /admin.php');
            exit;
        }
        $class = $this->router->fetch_class();
        $fun = $this->router->fetch_method();
        $check_sql = "SELECT id,cate_id FROM xjm_admin_menu WHERE class='$class' AND FUNCTION ='$fun'";
        $check_id = $this->db->query($check_sql)->row_array();
        if (empty($check_id)) {
            return FALSE;
        }
        //左侧权限列表
        $user_level = $_SESSION['admin']['menu_leval'];
        $auth_list = $this->menu_list($user_level);
        $user_auth = $auth_list['menu_list'];
        $user_auth = explode(',', $user_auth);
        if (in_array($check_id['id'], $user_auth)) {
            $now_id = $check_id['cate_id'];
            return array('user_auth' => $auth_list, 'now_id' => $now_id, 'check_id'=>$check_id['id']);
        } else {
            return FALSE;
        }
    }

    public function log_out() {
        unset($_SESSION['admin']);
        setcookie('M_login_time', null, time() - 3600, '/');
        header('location: /admin.php/adminlogin');
    }

    /* public function reset_pwd($login_name, $pwd, $check_owner = true) {
      //todo:记录日志
      if ($check_owner && $_SESSION['admin']['login_name'] != $login_name) {
      return false;
      } else {
      return $this->db->update(self::TABLE_ADMIN_ACCOUNT, array(
      'pwd' => sha1(md5(md5($pwd))),
      ), array('login_name' => $login_name));
      }
      } */
}

?>
