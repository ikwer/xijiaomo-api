<?php
/**
 * Description of m_user
 *
 * @author acer
 */
class Order_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        define('alipay_rsa_public','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2JxZD6CSTYkGWjUC54BA6t9VTLvTaTQHngPoEwV/sHZDT65r3GtMGNL5H/QkwSYhb7MzaGVECC6FdeQKDaxNxguSjZpC3NU8mHLLx1L4WLptjbbJeWbTE4ZhZOD7jSJ5C9UKvTTiKpbzSGRr0m3D2Ad04yUPBA2vHihGKozjrISFkDzzOplNQU2F7W/ZL2NPczD2hCJ6oCwjtIUAQJNfM2CAZR7U2j6v3eN5O4WD4cuvDlc8PkUlo+lFvqHMr1jrjsqd4eRIZg97cD/Vr/3WxmBwo0AH8BIoDTYB3N4JhIB7IONNrAp4NssRGPse1cT4hTVUV11SxgejEPryjGA7KQIDAQAB');
        define('alipay_rsa_private','MIIEpQIBAAKCAQEA09fNeZc74rE7s+LG07YvO1KzisDPAQLG3oJhYstviXhe3UU8egRNnc+XFwrmQozlXffO2dOnXh/TQWJy2HaCO71tndK32rBH2qzA6+Hvv6xiQaDw8xv6E3GYj/6awYFhyciRd0fmv5oQUfNvvCUIqkbycK5dT4fPyAGLIGpfZCylP7qIG/6duhZ+/8LV4eoTfqxSaGa/KMwDqe5N7cFw3ftO769mK3APO+F37v+hKWPaQ4eYksPANfiN7bPy8n+kB6d+FbPqyojdyWodD8hYRg8TR7jcUPkWsC64iTQL5Lcw/pe7ojR9eNR/DADUzQAwWXnv3zhe2xfRfRQQnm4rkwIDAQABAoIBADt+HoloLHxEc9TX+iiYwHjzh9KOxVOuWPVCPmFFaqR7toCDojFOJhIMq4zgFbxY301de6z24MjxPR3erQwXdAmc8DzjYZi76uIBpAhaoj79BXcS4LgVgkyVmVfPE90GHdVSS2/pNGdYt/6b1CRNbjqLxCha/3+HT5YGwPCwjuuNj2mX3zLCWtLgedf1lnas/lu2RDxElkuRhOsIIo5YXJdZhWIY/2zwqIBndgTDwptfWVSLEnvYo4hyDvQFkFnZGfvwtI2c12lmY+ExM400uDXFJIN5kIIGBgcaPVQ/n8wJykBkVyeyqNm5Ffhn5zjilx5ZdFGB9zvldq4iVckpU7ECgYEA6Y9jX1Ndyh9gClXGqCp4P9g0D8XMluZjZEeCmtwksUjvaLNCAB/yqB09habUNKjxgo1TBQuLEZPvEmUGGNwYN3BQ9aJGbznmI+ILoq2GR2jZXM0PDMW5ER5qZSVob/d6FLZbVZRm4EqASfFoCsgcHi9BzhlEQ1H1nzvghFLBFpkCgYEA6DJDUE4YBVc09uBg0fQYuToNYdqhk7TQoRn+krkb2iOWSU2ZmFBW7HP5abe2CdPcclXn7AUj6Qz6JiTRO1z5KvPoksuCcY+U0CL4GHT55hmnkfHg59JDbcaeTkOxg8HR2iQehyEubcJL6/nevnakumX3KpeeKnLFjXac2y6aqwsCgYEAzT4DDkbdOXw+0nY0H51KlJgyj9W7BrQojXvFyr4/xEcak4BLNH7ep1sisCs9eZUovhfg56MQL383bIu4QupOoZEio+hZSu6vTcMbhHZGdMQvlrxgSFIMYn2+82lfEF2CO2dQdbD2go0VlWT9j9Pv2ZqfkjRj52DDno1oq/9ozKkCgYEA1S5gkvhAIZchb5AuFFUx5c2gv7jFJCGccmy1R+xf3/VQY9i1LhyE0e7gjOk4Xul+uhKZLh7CC5P4jtC6sO/5bDAn1a63AqA6lqWkdn/feB0RtnMGdJCdi8oRSfXoovluPANxa8tRH0CGCA+PK/st3l0Dgr1VX8+kBO9jr/Cn3GUCgYEAzlgaLfaaopFXyPbKmnzMA0R7eTGMIgTJwhWwVlr5OBYAI2z3MQu8kj0ZFCFqJWavTlxbxGOPC3uwJZxXhjf1GviJYTzlsvXlyeZeQzy3bdi+1an2QsBHQZZRPU4u+A7tdy1Rg91MONZmTkthZdVR+Q2KHLZkzhcYNn178kghVGk=');
        define('gatewayUrl','https://openapi.alipay.com/gateway.do');
        define('format','json');
        define('charset','UTF-8');
        define('signType','RSA2');
        define('appId','2017101809366484');

    }
    

    //技师退单流程
    public function tuidan($order_id)
    {
        $where=array('order_id'=>$order_id);
        $order=$this->db->
        select('tid,order_status,pay_status,order_id,order_sn,goods_id,goods_name,order_amount,trade_no,server_time,goods_price,uid')->
        from('xjm_order_list')->
        where($where)->
        get()->
        row_array();
        if($order['order_status']==0){

            //查询一个自然月退单日志表(tuidan_log)
            $start=date('Y-m-01 00:00:00');
            $end=date('Y-m-d 00:00:00',strtotime("$start +1 month"));
            $wh=" tid=".$order['tid']." AND add_time>=".strtotime($start)." AND add_time<".strtotime($end);
            $nums=$this->db->select('id')->from('xjm_tuidan_log')->where($wh)->get()->num_rows();

            //超过三次无法进行退单操作
            if($nums>3){
                $arr=array('status'=>false,'msg'=>'该技师本月已经退单了三次，无法进行退单操作');
            }else{

                //支付宝退款
                $alipay_array=array(
                    'order_sn'=>$order['order_sn'],
                    'trade_no'=>$order['trade_no'],
                    'refund_reason'=> '技师联系客服申请退单',
                    'refund_money'=>$order['order_amount'],
                    'goods_price'=>$order['goods_price']
                );

                //调用支付宝退款接口
                $result=$this->alipay_refund($alipay_array);
                $json=json_decode($result,true);
                //退款成功
                if($json['code']==10000){

                    //事务开始
                    $this->db->trans_begin();

                    //插入退款表（完成退款状态3）
                    $refund_array=array(
                        'order_id'=>$order['order_id'],
                        'order_sn'=>$order['order_sn'],
                        'goods_name'=>$order['goods_name'],
                        'goods_id'=>$order['goods_id'],
                        'uid'=>$order['uid'],
                        'tid'=>$order['tid'],
                        'refund_reason'=>'技师申请退单',
                        'refund_money'=>$order['order_amount'],
                        'refund_note'=>'技师联系客服申请退单',
                        'status'=>3,
                        'refund_time'=>time(),
                        'server_time'=>$order['server_time'],
                        'goods_price'=>$order['goods_price'],
                        'add_time'=>time()
                    );
                    $this->db->insert('xjm_refund',$refund_array);

                    //修改订单表订单状态为4
                    $order_array=array(
                        'order_status'=>4
                    );
                    $this->db->where($where)->update('xjm_order_list',$order_array);

                    //插入退单log表
                    $tuidan_array=array(
                        'order_id'=>$order['order_id'],
                        'tid'=>$order['tid'],
                        'add_time'=>time(),
                        'admin_user'=>$_SESSION['admin']['uid']
                    );
                    $this->db->insert('xjm_tuidan_log',$tuidan_array);


                    //更新技师退单次数
                    $tuidan_nums=$this->db->select('tuidan_nums')->from('xjm_techer')->where(array('tid'=>$tuidan_array['tid']))->get()->row_array();
                    $techer_array=array(
                        'tuidan_nums'=>$tuidan_nums['tuidan_nums']+1
                    );
                    $this->db->where(array('tid'=>$tuidan_array['tid']))->update('xjm_techer',$techer_array);

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                        $arr=array('status'=>false,'msg'=>'退单失败'.$alipay_array['order_sn']);
                    }
                    else
                    {
                        $this->db->trans_commit();
                        $arr=array('status'=>true,'msg'=>'退单成功'.$alipay_array['order_sn']);
                    }




                }else{
                    $arr=array('status'=>false,'msg'=>'退单失败'.$alipay_array['order_sn']);
                }


            }

        }else{
            $arr=array('status'=>false,'msg'=>'订单状态已更改，无法退单');
        }

        return $arr;

    }



    //所有退单log
    public function all_tuidan($kwd)
    {
        $where = array('o.order_sn' => $kwd);
        return $this->db->select('l.id')->
        from('xjm_tuidan_log l')->
        join('xjm_order_list o','l.order_id=o.order_id','left')->
        join('xjm_admin_user a','l.admin_user=a.uid','left')->
        join('xjm_techer t','l.tid=t.tid','left')->
        join('xjm_user u','o.uid=u.uid','left')->
        where($where)->get()->num_rows();
    }

    //退单列表数据
    public function tuidan_data($page_num, $now_page = 1, $kwd)
    {
        --$now_page;
        $where = array('o.order_sn' => $kwd);
        $rs = $this->db->select('l.id,o.order_sn,o.goods_name,t.nickname tname,u.nickname uname,a.login_name,l.add_time')->
        from('xjm_tuidan_log l')->
        join('xjm_order_list o','l.order_id=o.order_id','left')->
        join('xjm_admin_user a','l.admin_user=a.uid','left')->
        join('xjm_techer t','l.tid=t.tid','left')->
        join('xjm_user u','o.uid=u.uid','left')->
        like($where)->
        order_by('l.id','desc')->
        limit($page_num, $now_page * $page_num)->
        get()->
        result_array();

        return $rs;
    }




    //所有用户
    public function all_order($kwd,$user) {
        $where = array('o.order_sn' => $kwd,'u.mobile'=>$user);
        return $rs = $this->db
        ->select('order_id')
        ->from('xjm_order_list o')
        ->join('xjm_user u','o.uid=u.uid','left')
        ->like($where)
        ->count_all_results();
    }
    
    //获取分页数据
    public function page_data($page_num, $now_page = 1, $kwd,$user) {
        --$now_page;
        $where = array('o.order_sn' => $kwd,'u.mobile'=>$user);
        $rs = $this->db->select('o.*,u.nickname,u.mobile mobiles')
        ->from('xjm_order_list o')
            ->join('xjm_user u','o.uid=u.uid','left')
        ->like($where)
        ->order_by('o.order_id')
        ->limit($page_num, $now_page * $page_num)
        ->get();
//        echo $this->db->last_query();die;
        return $rs->result_array();
    }


    public function all_techer_cash($kwd)
    {
        $where = array('tid' => $kwd);
        return $rs = $this->db
            ->select('id')
            ->from('xjm_techer_cash')
            ->like($where)
            ->count_all_results();
    }


    public function page_techer_data($page_num, $now_page = 1, $kwd)
    {
        --$now_page;
        $conds=array();
        if ($kwd)   $conds[] = 'u.nickname like "%'.$kwd.'%"';
        $where = !empty ( $conds ) ? ' where ' . @implode ( ' and ', $conds ) : '';
        $limit=$now_page * $page_num;
        $sql="SELECT c.*,u.nickname FROM xjm_techer_cash c LEFT JOIN xjm_techer u ON c.tid=u.tid $where ORDER BY c.id LIMIT $limit,$page_num ";
        $rs=$this->db->query($sql);
        return $rs->result_array();
    }


    public function get_techercash_one($id)
    {
        if ($id)   $conds[] =  'c.id ='.$id;
        $where = !empty ( $conds ) ? ' where ' . @implode ( ' and ', $conds ) : '';

        $sql="SELECT c.*,u.nickname,u.mobile,u.area_id,u.money,i.truename,i.card,i.sex,a.login_name FROM xjm_techer_cash c LEFT JOIN xjm_techer u ON c.tid=u.tid LEFT JOIN xjm_techer_checkinfo i ON c.tid=i.tid LEFT JOIN xjm_admin_user a ON c.admin_user=a.uid $where ORDER BY c.id LIMIT 1";

        $rs=$this->db->query($sql);
        return $rs->row_array();
    }



    //提现到支付宝/微信(没有微信)
    public function cash_success($id)
    {

        $query=$this->db->select('c.tid,c.type,c.cash_account,c.cash_money,c.status,t.money,t.nickname,c.add_time set_time')->from('xjm_techer_cash c')->join('xjm_techer t','c.tid=t.tid')->where('id='.intval($id))->get();
        $result=$query->row_array();
        $msg=array();
        // if($result['money']<$result['cash_account'])
        // {
        //     $msg['status']=false;
        //     $msg['msg']="提现金额大于技师余额";

        // }
        // else
        // {
            //支付宝提现
            if($result['type']==1)
            {
                $resultMsg=$this->alipay_transfer($result,'提现');
                if($resultMsg=="success")
                {
                    $data['status']=1;
                    $data['admin_user']=$_SESSION['admin']['uid'];
                    $data['set_time']=time();

                    $where="id=".intval($id);
                    $str = $this->db->update("xjm_techer_cash",$data,$where);
                    if($str==1)
                    {
                        $arr=array(
                            'tid'=>$result['tid'],
                            'type'=>0,
                            'common_id'=>3,
                            'title'=>'提现成功',
                            'content'=>'已经将'.$result['cash_money'].'打入您的支付宝账户（'.$result['cash_account'].'）',
                            'add_time'=>time(),
                            'is_read'=>0,
                            'read_time'=>0
                        );
                        $this->db->insert('xjm_techer_notice',$arr);
                        $msg['status']=true;
                        $msg['msg']="提现成功";

                    }
                    else
                    {
                        $msg['status']=false;
                        $msg['msg']="提现异常1";
                    }
                }
                else
                {
                    $msg['status']=false;
                    $msg['msg']="支付宝转账失败";
                }
            }
            else
            {
                $msg['status']=false;
                $msg['msg']="微信提现通道未开通";
            }

        // }

        return $msg;

    }


    //单笔转账到支付宝账户
    private function alipay_transfer($order,$type){
        require_once('/usr/share/nginx/html/api/system/libraries/Pay/Alipay/aop/AopClient.php');
        require_once('/usr/share/nginx/html/api/system/libraries/Pay/Alipay/aop/request/AlipayFundTransToaccountTransferRequest.php');
        $aop = new AopClient ();
        $aop->gatewayUrl = gatewayUrl;
        $aop->appId = appId;
        $aop->rsaPrivateKey = alipay_rsa_private;
        $aop->format = format;
        $aop->charset = charset;
        $aop->signType = signType;
        $aop->alipayrsaPublicKey = alipay_rsa_public;

        $request = new AlipayFundTransToaccountTransferRequest ();
        $json=json_encode([
            'out_biz_no'=>$order['set_time'].time(),
            'payee_type'=>'ALIPAY_LOGONID',
            'payee_account'=> $order['cash_account'],
            'amount'=>$order['cash_money'],
            'remark'=>'技师'.$order['nickname'].$type
        ]);

        $request->setBizContent($json);
        $result = $aop->execute ($request);

        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";

        $resultCode = $result->$responseNode->code;
        $resultMsg=$result->$responseNode->msg;
        if(!empty($resultCode)&&$resultCode == 10000){
            return "success";
        } else {
            return "fail";
        }

    }

    //订单退款
    public function refund_order($param)
    {
        switch ($param['status']) {
            case 3:
                $order_status=4;
                break;
            case 1:
                $order_status=7;
                break;
            case 0:
                $order_status=8;
                break;
            default:
                $order_status=9;
        }
        
        $data['status']=$param['status'];
        $order_data['order_status']=$order_status;

        


        $query=$this->db->select('f.order_id,f.order_sn,o.trade_no,f.refund_money,f.refund_reason,f.goods_price,o.uid')->from('xjm_refund f')->join('xjm_order_list o','f.order_id=o.order_id','left')->where('f.id='.intval($param['id']))->get();
        $result=$query->row_array();
        
        //已完成退款进行支付宝调用
        if($data['status']==3)
        {
            //调用支付宝退款接口
            $alipay_refund=$this->alipay_refund($result);
            $refund_json=json_decode($alipay_refund,true);
            if($refund_json['code']==10000)
            {
                $str1=$this->db->where('id='.$param['id'])->update('xjm_refund',$data);

                $str2=$this->db->where('order_id='.$result['order_id'])->update('xjm_order_list',array('order_status'=>4));

                if($str1==1 && $str2==1)
                {
                    $ars=array(
                        'uid'=>$result['uid'],
                        'type'=>0,
                        'common_id'=>3,
                        'title'=>'退款成功',
                        'content'=>'已经将订单：'.$result['order_sn'].'退款'.$result['refund_money'].'元，打入您的付款账户',
                        'add_time'=>time(),
                        'is_read'=>0,
                        'read_time'=>0
                    );
                    $this->db->insert('xjm_notice',$ars);
                    $arr=array('code'=>0,'msg'=>$result['order_sn'].'修改成功');
                }
                else
                {
                    $arr=array('code'=>-1,'msg'=>$result['order_sn'].'修改失败');
                }
                return $arr;
            }
            else
            {
                return $alipay_refund;
            }
        }
        else
        {
            $str1=$this->db->where('id='.$param['id'])->update('xjm_refund',$data);

            $str2=$this->db->where('order_id='.$result['order_id'])->update('xjm_order_list',$order_data);

            if($str1==1 && $str2==1)
            {
                $arr=array('code'=>0,'msg'=>$result['order_sn'].'修改成功');
            }
            else
            {
                $arr=array('code'=>-1,'msg'=>$result['order_sn'].'修改失败');
            }
            return $arr;
        }



    }


    //支付宝退款
    private function alipay_refund($order)
    {
        require_once('/usr/share/nginx/html/api/system/libraries/Pay/Alipay/aop/AopClient.php');
        require_once('/usr/share/nginx/html/api/system/libraries/Pay/Alipay/aop/request/AlipayTradeRefundRequest.php');
        $aop = new AopClient ();
        $aop->gatewayUrl = gatewayUrl;
        $aop->appId = appId;
        $aop->rsaPrivateKey = alipay_rsa_private;
        $aop->format = format;
        $aop->charset = charset;
        $aop->signType = signType;
        $aop->alipayrsaPublicKey = alipay_rsa_public;

        $request = new AlipayTradeRefundRequest ();


        //部分退款
        if($order['refund_money']<$order['goods_price'])
        {
            $json=json_encode([
                'out_trade_no'=>$order['order_sn'],
                'trade_no'=>$order['trade_no'],
                'refund_amount'=> $order['refund_money'],
                'refund_reason'=>$order['refund_reason'],
                'out_request_no'=>$order['order_sn'].time(),
            ]);
        }
        else
        {
            $json=json_encode([
                'out_trade_no'=>$order['order_sn'],
                'trade_no'=>$order['trade_no'],
                'refund_amount'=> $order['refund_money'],
                'refund_reason'=>$order['refund_reason'],
            ]);
        }

        $request->setBizContent($json);
        $result = $aop->execute ( $request);
        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $resultCode = $result->$responseNode->code;
        $resultMsg = $result->$responseNode->msg;
        $arr=array('code'=>$resultCode,'msg'=>$resultMsg);
        return json_encode($arr);
    }



    //提现申请驳回
    public function cash_fail($id)
    {
        $data['status']=2;
        $data['admin_user']=$_SESSION['admin']['uid'];
        $data['set_time']=time();

        $where="id=".intval($id);
        $str = $this->db->update("xjm_techer_cash",$data,$where);
        if($str==1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //退款全部
    public function all_refund($kwd)
    {
        $where = array('order_sn' => $kwd);
        return $rs = $this->db
            ->select('id')
            ->from('xjm_refund')
            ->like($where)
            ->count_all_results();
    }


    //获取分页数据
    public function refund_data($page_num, $now_page = 1, $kwd) {
        --$now_page;
        $rs = $this->db->select('r.*,u.nickname unick,t.nickname tnick')
            ->from('xjm_refund r')
            ->join('xjm_user u','r.uid=u.uid')
            ->join('xjm_techer t','r.tid=t.tid')
            ->where(array())
            ->like('r.order_sn', $kwd)
            ->order_by('r.id')
            ->limit($page_num, $now_page * $page_num)
            ->get();
        return $rs->result_array();
    }



}

?>
