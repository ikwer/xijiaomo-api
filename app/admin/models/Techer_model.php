<?php
/**
 * Description of m_user
 *
 * @author acer
 */
class Techer_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    //所有用户
    public function all_techer($kwd) {
       // $where = array('mobile' => $kwd);
        return $rs = $this->db
        ->select('*')
        ->from('xjm_techer a')
        ->join('xjm_techer_checkinfo b','a.tid=b.tid','left')
        //->like($where)
        ->count_all_results();
    }


    //所有用户
    public function all_share($kwd,$param) {
        $where=array('pid'=>$param);
        $rs = $this->db
            ->select('*')
            ->from('xjm_techer')
            ->where($where)
            ->like(array('mobile'=>$kwd))
            ->count_all_results();
//        echo $this->db->last_query();die;
        return $rs;
    }
    
    //技师收入明细
    public function all_income($kwd,$param)
    {
        $where=array('tid'=>$param);
        $rs=$this->db->select('id')->from('xjm_techer_income')->where($where)->get()->result_array();
        return count($rs);
    }

    public function income_list($page_num, $now_page = 1, $kwd,$param)
    {
        --$now_page;
        $where=array('tid'=>$param);
        $rs = $this->db->select('id,goods_name,change_money,from,add_time,order_id')->from('xjm_techer_income')->where($where)
            ->order_by('id')
            ->limit($page_num, $now_page * $page_num)
            ->get();
        $result=$rs->result_array();
        return $result;
    }



    //所有投诉
    public function all_tousu($kwd)
    {
        $where = array('o.order_sn' => $kwd);
        $rs=$this->db->select('g.id')->
        from('xjm_tousu_log g')->
        join('xjm_order_list o','g.order_id=o.order_id','left')->
        join('xjm_techer_checkinfo c','g.tid=c.tid','left')->
        join('xjm_user u','o.uid=u.uid','left')->like($where)->get()->num_rows();
        
        return $rs;
    }


    public function tousu_list($page_num, $now_page = 1, $kwd)
    {
        --$now_page;
        $where = array('o.order_sn' => $kwd);
        $rs = $this->db->select('g.id,o.order_sn,u.nickname uname,c.truename tname,g.content,g.add_time,a.login_name')->
        from('xjm_tousu_log g')->
        join('xjm_order_list o','g.order_id=o.order_id','left')->
        join('xjm_techer_checkinfo c','g.tid=c.tid','left')->
        join('xjm_admin_user a','g.admin_user=a.uid','left')->
        join('xjm_user u','o.uid=u.uid','left')->like($where)->
        limit($page_num, $now_page * $page_num)->
        order_by('g.id','desc')->
        get()->result_array();

        return $rs;
    }
    

    public function share_list($page_num, $now_page = 1, $kwd,$param)
    {
        --$now_page;
        $where=array('pid'=>$param);
        $rs = $this->db->select('*')
            ->from('xjm_techer')
            ->where($where)
            ->like(array('mobile'=>$kwd))
            ->order_by('tid')
            ->limit($page_num, $now_page * $page_num)
            ->get();
        $result=$rs->result_array();
//        var_dump($result);die;
        return $result;
    }
    
    //获取分页数据
    public function page_data($page_num, $now_page = 1, $kwd) {
        --$now_page;
        $rs = $this->db->select('a.tid,a.nickname,a.skill_level,a.money,a.status,a.mobile,b.place,b.sex')
            ->from('xjm_techer a')
            ->join('xjm_techer_checkinfo b','a.tid=b.tid','left')
            ->where(array())
            ->like('a.mobile', $kwd)
            ->order_by('a.tid')
            ->limit($page_num, $now_page * $page_num)
            ->get();
        $result=$rs->result_array();
        foreach($result as $k=>$v)
        {
            $where=array('pid'=>$v['tid']);
            $r=$this->db->select('count(tid) tnum')->from('xjm_techer')->where($where)->get()->row_array();
            $result[$k]['tnum']=$r['tnum'];
        }
        return $result;
    }
    public function getTecherInfoByUid($uid){
        $rs = $this->db
        ->select('*')
        ->from('xjm_techer a')
        ->join('xjm_techer_checkinfo b','a.tid=b.tid','left')
        ->where(['a.tid'=>$uid])
        ->get();
        $r=$rs->row_array();
        $r['pic_arr']=$this->db->select('*')->from('xjm_techer_photo')->where(array('tid'=>$r['tid'],'type'=>1))->get()->result_array();
        $r['video_arr']=$this->db->select('*')->from('xjm_techer_photo')->where(array('tid'=>$r['tid'],'type'=>2))->get()->row_array();
        return $r;
    }
    public function techer_dele($tid){
        $where['tid'] = $tid;
       if($this->db->delete('xjm_techer',$where)){
           $this->db->delete('xjm_techer_cash',$where);
           $this->db->delete('xjm_techer_checkinfo',$where);
           $this->db->delete('xjm_techer_photo',$where);
          return  $this->db->delete('xjm_techer_checkinfo',array('tid'=>$tid));
       }
       
       
    }
    public function saveStatus($tid,$status){
        $data['status'] = $status;
        $query=$this->db
            ->where('tid',$tid)
            ->update('xjm_techer',$data);
        if($query){
            if($status==1)
            {
                $content='恭喜您审核成功，可以接单了';
                $title='恭喜您审核成功';
            }else{
                $title='技师审核未通过';
                $content='技师审核未通过';
            }
            $arr=array(
              'tid'=>$tid,
              'type'=>0,
                'common_id'=>3,
                'title'=>$title,
                'content'=>$content,
                'add_time'=>time(),
                'is_read'=>0,
                'read_time'=>0
            );
            $this->add_techer_notice($arr);
            return $query;
        }

    }
    
    public function add_techer_notice($data)
    {
        return $this->db->insert('xjm_techer_notice',$data);
    }
    
    public function addTecher($data,$table){
         $this->db->insert($table,$data);
         return $this->db->insert_id();
    }



    //所有技师评论
    public function all_techer_feedback($kwd)
    {
        $where = array('u.mobile' => $kwd,"uid"=>0);
        return $rs = $this->db
            ->select('f.id')
            ->from('xjm_feedback f')
            ->join('xjm_techer u','f.tid=u.tid')
            ->like($where)
            ->count_all_results();
    }

    //获取技师评论分页数据
    public function page_techer_feedback_data($page_num, $now_page = 1, $kwd)
    {
        --$now_page;
        if ($kwd)   $conds[] = 'u.mobile like "%'.$kwd.'%"';
        $conds[] = " f.uid=0 ";
        $where = !empty ( $conds ) ? ' where ' . @implode ( ' and ', $conds ) : '';

        $limit=$now_page * $page_num;
        $sql=" SELECT f.id,f.uid,f.content,f.add_time,u.nickname,u.mobile,f.type FROM xjm_feedback f JOIN xjm_techer u ON f.tid=u.tid $where ORDER BY f.id DESC LIMIT $limit,$page_num";
        return $this->db->query($sql)->result_array();
    }


    //用户技师详情
    public function techer_feedback_info($id)
    {
        if ($id)   $conds[] = 'f.id = '.$id.'';
        $conds[] = " f.uid=0 ";
        $where = !empty ( $conds ) ? ' where ' . @implode ( ' and ', $conds ) : '';
        $sql="SELECT f.id,f.uid,f.content,f.add_time,u.nickname,u.mobile,u.status,c.truename,c.card,c.sex,c.card_success FROM xjm_feedback f JOIN xjm_techer u ON f.tid=u.tid JOIN xjm_techer_checkinfo c ON f.tid=c.tid $where LIMIT 1";
        return $this->db->query($sql)->row_array();
    }


    //检测身份证信息
    public function check_idcard($id)
    {
        header("Content-type: text/html; charset=utf-8"); 
        $id=intval($id);

        $where=array("id"=>$id);
        $query=$this->db->select('truename,card')->from('xjm_techer_checkinfo')->where($where)->get();
        $result=$query->row_array();
        $update=array();
        if(!empty($result['truename']) || !empty($result['card']))
        {
            $name=$result['truename'];
            $id_card=$result['card'];
            
            $appkey="4bfdddfa590291d53c7f329f9fc0aec9";    
            
           //京东万象APPKEY
            
            $url="http://way.jd.com/freedt/api_rest_police_identity?name=$name&idCard=$id_card&appkey=$appkey";
            ob_start();
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $stream = curl_exec($ch);
            curl_close($ch);
            ob_end_clean();
            //$res=mb_convert_encoding($stream,'UTF-8','GBK');
            
            $array=json_decode($stream,true);
            // var_dump($array);die;
            if($array['code']=='10000')
            {
                if(!empty($array['result']['success']) && $array['result']['data']['compareStatus']=='SAME')
                {
                    $update['card_success']=1;
                    $str = $this->db->update("xjm_techer_checkinfo",$update,$where);
                    $data['status']=true;
                    $data['msg']=$array['result']['data']['compareStatusDesc'];
                }
                else
                {
                    $update['card_success']=2;
                    $str = $this->db->update("xjm_techer_checkinfo",$update,$where);
                    $data['status']=false;
                    $data['msg']='身份信息异常';
                }
            }
            else
            {
                $update['card_success']=2;
                $str = $this->db->update("xjm_techer_checkinfo",$update,$where);
                $data['status']=false;
                $data['msg']=$array['msg'];
            }
        }
        else
        {
            $update['card_success']=2;
            $str = $this->db->update("xjm_techer_checkinfo",$update,$where);
            $data['status']=false;
            $data['msg']="信息异常";
        }

        return $data;

    }


}

?>
