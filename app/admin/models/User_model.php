<?php
/**
 * Description of m_user
 *
 * @author acer
 */
class User_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    //所有用户
    public function all_user($kwd) {
        $where = array('mobile' => $kwd);
        return $rs = $this->db
        ->select('uid')
        ->from('xjm_user')
        ->like($where)
        ->count_all_results();
    }

    public function all_share($kwd,$param)
    {
        $where=array('uid'=>$param);
        $rs = $this->db
            ->select('s_uid')
            ->from('xjm_user')
            ->where($where)
            ->like(array('mobile'=>$kwd))
            ->get()
            ->row_array();
        if(!empty($rs['s_uid'])){
            $ex=@explode(',',$rs['s_uid']);
            return count($ex);
        }else{
            return 0;
        }
    }
    
    //获取分页数据
    public function page_data($page_num, $now_page = 1, $kwd) {
        --$now_page;
        $rs = $this->db->select('uid,mobile,money,status,point,s_uid')
        ->from('xjm_user')
        ->where(array())
        ->like('mobile', $kwd)
        ->order_by('uid')
        ->limit($page_num, $now_page * $page_num)
        ->get();
        $result=$rs->result_array();
        foreach($result as $k=>$v){
            if(empty($v['s_uid'])){
                $result[$k]['s_num']=0;
            }else{
                $ex=@explode(',',$v['s_uid']);
                $result[$k]['s_num']=count($ex);
            }
        }
        return $result;
    }


    public function page_share($page_num, $now_page, $kwd,$param)
    {
        --$now_page;
        $where=array('uid'=>$param);
        $rs = $this->db->select('s_uid')
            ->from('xjm_user')
            ->where($where)
            ->like('mobile', $kwd)
            ->order_by('uid')
            ->limit($page_num, $now_page * $page_num)
            ->get()->row_array();
        (!empty($rs['s_uid']))?$s_uid=$rs['s_uid']:$s_uid=0;
        $sql="SELECT * FROM xjm_user WHERE uid IN($s_uid)";
        $query=$this->db->query($sql)->result_array();
        return $query;
    }

    //所有用户评论
    public function all_user_feedback($kwd)
    {
        $where = array('u.mobile' => $kwd,"tid"=>0);
        return $rs = $this->db
            ->select('f.id')
            ->from('xjm_feedback f')
            ->join('xjm_user u','f.uid=u.uid')
            ->like($where)
            ->count_all_results();
    }

    //获取用户评论分页数据
    public function page_user_feedback_data($page_num, $now_page = 1, $kwd)
    {
        --$now_page;
        if ($kwd)   $conds[] = 'u.mobile like "%'.$kwd.'%"';
        $conds[] = " f.tid=0 ";
        $where = !empty ( $conds ) ? ' where ' . @implode ( ' and ', $conds ) : '';

        $limit=$now_page * $page_num;
        $sql=" SELECT f.id,f.uid,f.content,f.add_time,u.nickname,u.mobile FROM xjm_feedback f JOIN xjm_user u ON f.uid=u.uid $where ORDER BY f.id DESC LIMIT $limit,$page_num";
        return $this->db->query($sql)->result_array();
    }


    //用户评论详情
    public function user_feedback_info($id)
    {
        if ($id)   $conds[] = 'f.id = '.$id.'';
        $conds[] = " f.tid=0 ";
        $where = !empty ( $conds ) ? ' where ' . @implode ( ' and ', $conds ) : '';
        $sql="SELECT f.id,f.uid,f.content,f.add_time,u.nickname,u.mobile FROM xjm_feedback f JOIN xjm_user u ON f.uid=u.uid $where LIMIT 1";
        return $this->db->query($sql)->row_array();
    }


    /**
     * 日新增用户图表数据
     * @return mixed
     */
    public function everyday_user_charts($sdate,$edate)
    {
        if(!empty($sdate) || !empty($edate))
        {
            $s_date=strtotime($sdate.' 00:00:00');
            $e_date=strtotime($edate.' 23:59:59');


            $date_arr=$this->getDateFromRange($sdate,$edate);
        }
        else
        {
            $s_date=date("Y-m-d",strtotime("-7 day"));
            $s_date=strtotime($s_date);

            $e_date=date("Y-m-d 00:00:00",strtotime("+1 day"));
            $e_date=strtotime($e_date);

            $date_arr=$this->getDateFromRange(date("Y-m-d",strtotime("-7 day")),date("Y-m-d"));
        }


        
        $where=" WHERE add_time>=$s_date AND add_time<$e_date ";

        $user_query=$this->db->query('SELECT COUNT(uid) usernum,FROM_UNIXTIME(add_time,\'%Y-%m-%d\') dates FROM xjm_user '.$where.' GROUP BY dates ORDER BY dates');
        $user_result=$user_query->result_array();


        $tech_query=$this->db->query('SELECT COUNT(tid) techernum,FROM_UNIXTIME(add_time,\'%Y-%m-%d\') dates FROM xjm_techer '.$where.'  GROUP BY dates ORDER BY dates');
        $tech_result=$tech_query->result_array();

        foreach($user_result as $k=>$v)
        {
            $new_user[$v['dates']]=$v['usernum'];
        }
        (empty($new_user)) ? $new_users=$date_arr : $new_users=array_merge($date_arr,$new_user);


        foreach($new_users as $ks=>$vs)
        {
            (empty($vs)) ? $zhi=0 :$zhi=$vs;
            $newuser[]='["'.$ks.'",'.$zhi.']';
        }

        $data['new_users']='['.@implode(',',$newuser).']';

        foreach($tech_result as $k=>$v)
        {
            $new_tech[$v['dates']]=$v['techernum'];
        }

        (empty($new_tech)) ? $new_techs=$date_arr : $new_techs=array_merge($date_arr,$new_tech);


        foreach($new_techs as $ks=>$vs)
        {
            (empty($vs)) ? $zhi=0 :$zhi=$vs;
            $newtechs[]='["'.$ks.'",'.$zhi.']';
        }

        $data['new_techs']='['.@implode(',',$newtechs).']';
        return $data;

    }


    /**
     * 每日新增统计列表
     * @return array
     */
    public function everyday_user_data($sdate,$edate)
    {

        if(!empty($sdate) || !empty($edate))
        {
            $s_date=strtotime($sdate.' 00:00:00');
            $e_date=strtotime($edate.' 23:59:59');


            $date_arr=$this->getDateFromRange($sdate,$edate);
        }
        else
        {
            $s_date=date("Y-m-d",strtotime("-7 day"));
            $s_date=strtotime($s_date);

            $e_date=date("Y-m-d 00:00:00",strtotime("+1 day"));
            $e_date=strtotime($e_date);

            $date_arr=$this->getDateFromRange(date("Y-m-d",strtotime("-7 day")),date("Y-m-d"));
        }


        $where=" WHERE add_time>=$s_date AND add_time<$e_date ";

        $count_query=$this->db->query('SELECT c.dates FROM (( SELECT COUNT(uid) usernum,FROM_UNIXTIME(add_time,\'%Y-%m-%d\') dates FROM xjm_user '.$where.' GROUP BY dates ORDER BY dates ) UNION ALL ( SELECT COUNT(tid) techernum,FROM_UNIXTIME(add_time,\'%Y-%m-%d\') dates FROM xjm_techer '.$where.'  GROUP BY dates ORDER BY dates )
) c GROUP BY c.dates ORDER BY c.dates DESC ');

        $data['count']=$count_query->num_rows();

        $date_list=$count_query->result_array();


        foreach($date_arr as $k=>$v)
        {
            $start=strtotime($k." 00:00:00");
            $end=strtotime($k." 23:59:59");

            $where = " WHERE add_time>=$start AND add_time<=$end ";
            $user_query=$this->db->query('SELECT COUNT(uid) usernum FROM xjm_user '.$where.' ');
            $user_result[$k]=$user_query->row_array();


            $tech_query=$this->db->query('SELECT COUNT(tid) techernum FROM xjm_techer '.$where.' ');
            $tech_result[$k]=$tech_query->row_array();


        }

        $arr=array();

        foreach($user_result as $ks=>$vs)
        {
            $arr[$ks]['usernum']=$vs['usernum'];
        }

        foreach($tech_result as $ks=>$vs)
        {
            $arr[$ks]['techernum']=$vs['techernum'];
        }

        return $arr;

    }


    /**
     * 用户比例图表数据
     * @return mixed
     */
    public function fenbu_user_charts()
    {
        //技师来源分布
        $query=$this->db->query('SELECT count(tid) value,source name FROM xjm_techer GROUP BY source');
        $result=$query->result_array();

        //用户来源分布
        $u_query=$this->db->query('SELECT count(uid) value,source name FROM xjm_user GROUP BY source');
        $u_result=$u_query->result_array();


        $data['table_list']=$result;

        $data['u_table_list']=$u_result;

        //技师图表
        foreach($result as $k=>$v)
        {
            $result[$k]['value']=(int)$v['value'];
        }

        $data['result']=$result;

        //用户图表
        foreach($u_result as $key=>$val)
        {
            $u_result[$key]['value']=(int)$val['value'];
        }

        $data['u_result']=$u_result;


        $source_query=$this->db->query('SELECT source FROM xjm_techer GROUP BY source');
        $source_result=$source_query->result_array();
        foreach($source_result as $ks=>$vs)
        {
            $news[]=@implode(',',$vs);
        }
        $data['source_result']=$news;

//        var_dump($news);die;




        return $data;
    }


    /**
     * 订单走势图表
     * @param $sdate
     * @param $edate
     * @return mixed
     */
    public function order_trend_charts($sdate,$edate)
    {
        if(!empty($sdate) || !empty($edate))
        {
            $s_date=strtotime($sdate.' 00:00:00');
            $e_date=strtotime($edate.' 23:59:59');


            $date_arr=$this->getDateFromRange($sdate,$edate);
        }
        else
        {
            $s_date=date("Y-m-d",strtotime("-7 day"));
            $s_date=strtotime($s_date);

            $e_date=date("Y-m-d 00:00:00",strtotime("+1 day"));
            $e_date=strtotime($e_date);

            $date_arr=$this->getDateFromRange(date("Y-m-d",strtotime("-7 day")),date("Y-m-d"));
        }



        foreach($date_arr as $k=>$v)
        {
            $start=strtotime($k." 00:00:00");
            $end=strtotime($k." 23:59:59");

            $where = " WHERE add_time>=$start AND add_time<=$end AND order_status=2 AND pay_status=1 ";

            $query=$this->db->query('SELECT FROM_UNIXTIME(add_time,\'%Y-%m-%d\') dates,COUNT(add_time) bishu,SUM(order_amount) money FROM xjm_order_list '.$where.'  GROUP BY dates ORDER BY dates DESC');

            $date_list[$k]=$query->row_array();
        }

        $arr=array();

        $m_arr=array();

        foreach($date_list as $ks=>$vs)
        {
            $arr[$ks]['value']=intval($vs['bishu']);
            ($vs['money']==null) ? $m_arr[$ks]['value']=0 : $m_arr[$ks]['value']=$vs['money'];
        }

        foreach($arr as $ks=>$vs)
        {
            $news[$ks]=@implode(',',$vs);
        }

        foreach($m_arr as $ks=>$vs)
        {
            $m_news[$ks]=@implode(',',$vs);
        }

        foreach($news as $key=>$val)
        {
            $ss[]='["'.$key.'",'.$val.']';
        }

        foreach($m_news as $key=>$val)
        {
            $m_ss[]='["'.$key.'",'.$val.']';
        }

        $data['charts']='['.@implode(',',$ss).']';

        $data['m_charts']='['.@implode(',',$m_ss).']';

        return $data;
    }


    /**
     * 订单走势列表数据
     * @param $sdate
     * @param $edate
     * @return array
     */
    public function order_trend_data($sdate,$edate)
    {
        if(!empty($sdate) || !empty($edate))
        {
            $s_date=strtotime($sdate.' 00:00:00');
            $e_date=strtotime($edate.' 23:59:59');


            $date_arr=$this->getDateFromRange($sdate,$edate);
        }
        else
        {
            $s_date=date("Y-m-d",strtotime("-7 day"));
            $s_date=strtotime($s_date);

            $e_date=date("Y-m-d 00:00:00",strtotime("+1 day"));
            $e_date=strtotime($e_date);

            $date_arr=$this->getDateFromRange(date("Y-m-d",strtotime("-7 day")),date("Y-m-d"));
        }

        foreach($date_arr as $k=>$v)
        {
            $start=strtotime($k." 00:00:00");
            $end=strtotime($k." 23:59:59");

            $where = " WHERE add_time>=$start AND add_time<=$end AND order_status=2 AND pay_status=1 ";

            $query=$this->db->query('SELECT FROM_UNIXTIME(add_time,\'%Y-%m-%d\') dates,COUNT(add_time) bishu,SUM(order_amount) money FROM xjm_order_list '.$where.'  GROUP BY dates ORDER BY dates DESC');

            $date_list[$k]=$query->row_array();
        }


        $arr=array();

        foreach($date_list as $ks=>$vs)
        {
            $arr[$ks]['value']=intval($vs['bishu']);
            $arr[$ks]['money']=$vs['money'];
        }
        return $arr;
    }



    /**
     * 获取指定日期段内每一天的日期
     * @param  Date  $startdate 开始日期
     * @param  Date  $enddate   结束日期
     * @return Array
     */
    public function getDateFromRange($startdate, $enddate){

        $stimestamp = strtotime($startdate);
        $etimestamp = strtotime($enddate);

        // 计算日期段内有多少天
        $days = ($etimestamp-$stimestamp)/86400+1;

        // 保存每天日期
        $date = array();

        for($i=0; $i<$days; $i++){
            $date[date('Y-m-d', $stimestamp+(86400*$i))] = '';
        }

        return $date;
    }


}

?>
