<?php include_once APPPATH . 'views/public/header.php'; ?>
<script src="<?php echo JS_PATH ?>formdata.js"></script>
  <script>
                var domain = "20170914"; // you bucket domain  eg: http://xxx.bkt.clouddn.com
              </script>
<div class="main-content">
	<div class="page-header">
        <h1><i class="icon-hand-right icon-animated-hand-pointer blue"></i>文章管理
            <small>
                <i class="icon-double-angle-right"></i>&nbsp;添加文章
            </small>
        </h1>
    </div>

    <div class="page-content">
        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                <form action="/admin.php/Article/add" class="form-horizontal" id="myform" method="post" enctype="multipart/form-data">
               <input type="hidden" name="formhash" value="1"/>
                    <input type="hidden" name="id" value="<?php if($this->input->get('id')){echo $goodsInfo['id'];}?>"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> 文章所属分类 </label>
                    <div class="col-sm-9">
                        <select class="col-sm-5" id="form-field-select-1" name="type" required="required">
                            <?php if($_GET['id']){ ?>
                                <option value="<?php echo $goodsInfo['type']; ?>"><?php echo $goodsOptionList[$goodsInfo['type']]; ?></option>
                                <?php unset($goodsOptionList[$goodsInfo['type']]);  ?>
                                <?php foreach($goodsOptionList as $k=>$v){?>
                                    <option value="<?php echo $k; ?>"><?php echo $v; ?></option>
                                <?php }?>
                            <?php }else{ ?>
                                <?php foreach($goodsOptionList as $k=>$v){?>
							        <option value="<?php echo $k; ?>"><?php echo $v; ?></option>
							    <?php }?>
                            <?php } ?>
						</select>
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> 文章标题 </label>
                    <div class="col-sm-9">
                        <input type="text" name="title" id="form-field-2" required="required" placeholder="必填：文章标题"  value="<?php if($this->input->get('id')){ echo $goodsInfo['title'];}?>" class="col-xs-10 col-sm-5" />
                        <span class="help-inline col-xs-12 col-sm-7">
                        </span>
                    </div>
                </div>
                 <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> 文章简介</label>
                    <div class="col-sm-9">
                        <input type="text" name="description" id="form-field-2" required="required" value="<?php if($this->input->get('id')){ echo $goodsInfo['description'];}?>"   placeholder="必填：文章描述" class="col-xs-10 col-sm-5" />
                        <span class="help-inline col-xs-12 col-sm-7">
                        </span>
                    </div>
                </div>



				<div class="space-4"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-4"> 排序（从小到大）： </label>
                    <div class="col-sm-9">
                        <input type="text" name="sort_order" id="form-field-4" value="<?php if($this->input->get('id')){ echo $goodsInfo['sort_order']; } ?>" class="col-xs-10 col-sm-5" />
                        <span class="help-inline col-xs-12 col-sm-7">
                        </span>
                    </div>
                </div>
                <div class="space-4"></div>
                
                <div class="form-group">
					<label class="col-sm-3 control-label no-padding-right" for="form-field-5"> 文章主内容 </label>
					<div class="col-sm-7">
						<link href="<?php echo DATA_PATH ?>kindeditor/themes/default/default.css" rel="stylesheet" type="text/css"/>
						<script src="<?php echo DATA_PATH ?>kindeditor/kindeditor-min.js" type="text/javascript" ></script>
						<textarea rows="100%" style="width:100%" name="content" id="editor_id" class="editor">
							<?php if($this->input->get('id')){ echo $goodsInfo['content'];}?>
						</textarea>
					</div>
				</div>
                <div class="space-4"></div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-5"> 文章是否显示 </label>
                        <div class="col-sm-9" style="width: 10%;">
                            <input type="radio" name="is_show" value="0" <?php if($this->input->get('id')){ if($goodsInfo['status']==0){ ?> checked="checked"  <?php } }  ?>   />显示
                            <input type="radio" name="is_show" value="1"  <?php if($this->input->get('id')){ if($goodsInfo['status']==1){ ?> checked="checked"  <?php } }  ?>  />隐藏
                            <span class="help-inline col-xs-12 col-sm-7">
                        </span>
                        </div>
                    </div>
                    <div class="space-4"></div>
                
                <!--<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right" for="form-field-5"> 文章主内容 </label>
					<div class="col-sm-7">
						<script src="<?php echo DATA_PATH ?>ueditor/ueditor.config.js" type="text/javascript"></script>
						<script src="<?php echo DATA_PATH ?>ueditor/ueditor.all.js" type="text/javascript"></script>
						<textarea name="news_content" rows="100%" style="width:100%" id="myEditor"></textarea>
						<script type="text/javascript">
							var editor = new UE.ui.Editor();
							editor.render("myEditor");
						</script>
					</div>
				</div>
                <div class="space-4"></div>-->
                
                <div class="clearfix form-actions">
                    <div class="col-md-offset-3 col-md-9">
                        <input class="btn btn-info" type="submit" name="ok" value="提交">
                        &nbsp; &nbsp; &nbsp;
                        <button id="back_url" class="btn" type="reset">
                            <i class="icon-undo bigger-110"></i>
                            	返回
                        </button>
                    </div>
                </div>
                </form>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.page-content -->
</div><!-- /.main-content -->

<?php include_once APPPATH . 'views/public/footer.php'; ?>
<script>
    $("#back_url").click(function () {
        location.href = '/admin.php/Article/index';
    });
</script>
<script type="text/javascript">
    $(function(){
    	KindEditor.ready(function (K) {
            window.editor = K.create('#editor_id', {items: [
                'source','paste', 'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
                'justifyfull', '|', 'forecolor', 'hilitecolor', '|', 'insertorderedlist', '|', '-', 'image', 'multiimage', '|', 'bold'], height: '200px'
            });
        });
    })
	
</script>
<script src="http://apps.bdimg.com/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
