<?php include_once APPPATH . 'views/public/header.php'; ?>
<div class="main-content">
	<div class="page-header">
        <h1>
           	<i class="icon-hand-right icon-animated-hand-pointer blue"></i>文章管理
            <small>
                <i class="icon-double-angle-right"></i>
            	文章列表
            </small>
        </h1>
    </div><!-- /.page-header -->
	
	<div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try {
                ace.settings.check('breadcrumbs', 'fixed')
            } catch (e) {
            }
        </script>

        <ul class="breadcrumb">
            <li> 
                <a href="/admin.php/Article/add">
                    <button class="btn btn-sm btn-success">
                        <i class="icon-ok bigger-110"></i>
                        	添加文章
                    </button>
                </a>
            </li>
            <li>
                <a href="/admin.php/Article/flush_cache">
                    <button class="btn btn-sm btn-error">
                        <i class="icon-ok bigger-110"></i>
                        清空缓存
                    </button>
                </a>
            </li>
        </ul><!-- .breadcrumb -->
        <div class="nav-search" id="nav-search">
            <form class="form-search">
                <span class="input-icon">
                    <input type="text" placeholder="Search ..." name="kwd"  class="nav-search-input" value="<?php if($this->input->get('kwd')){ echo $this->input->get('kwd'); } ?>" id="nav-search-input" autocomplete="off" />
                    <i class="icon-search nav-search-icon"></i>
                </span>
                <input type="submit" >
            </form>
        </div><!-- #nav-search -->
    </div>
    <div class="page-content">
        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="center">ID</th>
                                        <th>文章标题</th>
                                        <th>文章类型</th>
                                        <th class="hidden-480">状态</th>
                                        <th>更新时间</th>
                                        <th>排序</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php foreach ($page_data as $key => $value) { ?>
                                        <tr>
                                            <td class="center"><?php echo $value['id']; ?></td>
                                            <td><?php echo $value['title']; ?></td>
                                            <td><?php echo $article_option[$value['type']]; ?></td>

                                            <td class="hidden-480">
                                            	<?php if ($value['status'] == 0) { ?>
                                                    <span class="label label-sm label-success">显示</span>
                                                <?php } else { ?>
                                                    <span class = "label label-sm label-warning">隐藏</span>
                                                <?php } ?>
                                            </td>
                                            <td><?php echo $value['set_times']; ?></td>
                                            <td><?php echo $value['sort_order']; ?></td>
                                            <td>
                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                    <a class="green" href="/admin.php/Article/add?id=<?php echo $value['id']; ?>" title="修改">
                                                        <i class="icon-pencil bigger-130"></i> 
                                                    </a>
                                                    
                                                    <!-- <a class="red" onclick="confirmDels('<?php /*echo $value['id']; */?>')" title="删除">
                                                        <i class="icon-trash bigger-130"></i> 
                                                    </a>-->
                                                </div>
                                            </td>
                                        </tr>
                                        <?php }?>
                                </tbody>
                            </table>
                        </div><!-- /.table-responsive -->
                    </div><!-- /span -->
                </div><!-- /row -->
                <div class="row">
                	<div class="col-sm-6">
                    	<div class="dataTables_info" id="sample-table-2_info"></div>
                    </div>
                    <div class="col-sm-6">
                        <div class="dataTables_paginate paging_bootstrap">
                            <ul class="pagination">
                            	<?php echo $page_list ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.page-content -->
</div><!-- /.main-content -->

    <script type="text/javascript">
        function confirmDels(id)
        {
            window.location.href="/admin.php/Article/del?id="+id;
            layer.confirm('是否删除这篇文章', {icon: 3, title:'提示'}, function(index){
                console.log(id);
                //do something
                if(index)
                {
                    var loading=layer.load(1, {shade:0.7});
                    window.location.href="/admin.php/Article/del?id="+index;
                }
                else
                {
                    layer.close(index);
                }
            });
        }
    </script>
<?php include_once APPPATH . 'views/public/footer.php'; ?>