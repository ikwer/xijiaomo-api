<?php include_once APPPATH . 'views/public/header.php'; ?>
<div class="main-content">
    <div class="page-header">
        <h1>管理员
            <small>
                <i class="icon-double-angle-right"></i>&nbsp;添加管理员
            </small>
        </h1>
    </div>

    <div class="page-content">
        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                <form action="/admin.php/Admin/add_user" class="form-horizontal" id="myform" method="post">
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> 账号 </label>
                    <div class="col-sm-9">
                        <input type="text" name="login_name" id="form-field-1" required="required"  placeholder="字母开头，允许6-20字节之间，只能包含字母、数字下划线" value=""  class="col-xs-10 col-sm-5" />
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> 密码 </label>
                    <div class="col-sm-9">
                        <input type="password" name="pwd" id="form-field-2" required="required" placeholder="字母开头，长度在6~20之间，只能包含字母、数字和下划线" class="col-xs-10 col-sm-5" />
                        <span class="help-inline col-xs-12 col-sm-7">
                        </span>
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-input-readonly"> 权限分类 </label>
                    <div class="col-sm-9">
                        <select class="col-sm-3" name="menu_leval" required="required" class="form-control " id="form-field-select-1">
                            <?php foreach ($menu_level as $key => $value) { ?>
                                <option value="<?php echo $value['id'] ?>"><?php echo $value['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="space-4"></div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-4">状态</label>
                    <div class="col-sm-9">
                        <select class="col-sm-3" name="status" class="form-control " id="form-field-select-1">
                            <option value="1">正常</option>
                            <option value="0">封号</option>
                        </select>
                    </div>
                </div>
                <div class="space-4"></div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-4">辖区</label>
                    <div class="col-sm-9">
                        <?php foreach ($area as $key => $value) { ?>
                            <div class="checkbox">
                                <label>
                                    <input name="area[]" checked="checked" type="checkbox" value="<?php echo $value['id'] ?>" class="ace ace-checkbox-2">
                                    <span class="lbl"> <?php echo $value['area_name'] ?></span>
                                </label>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="space-4"></div>
                <div class="clearfix form-actions">
                    <div class="col-md-offset-3 col-md-9">
                        <!--                            <button class="btn btn-info" type="submit">
                                                        <i class="icon-ok bigger-110"></i>
                                                        Submit
                                                    </button>-->
                        <input class="btn btn-info" type="submit" name="ok" value="提交">
                        &nbsp; &nbsp; &nbsp;
                        <button id="back_url" class="btn" type="reset">
                            <i class="icon-undo bigger-110"></i>
                            返回
                        </button>
                    </div>
                </div>

                </form>

            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.page-content -->
</div><!-- /.main-content -->
<?php include_once APPPATH . 'views/public/footer.php'; ?>
<script>
    $("#back_url").click(function () {
        location.href = '<?php echo site_url('Admin/user_list'); ?>';
    });
</script>