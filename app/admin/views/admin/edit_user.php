<?php include_once APPPATH . 'views/public/header.php'; ?>
<div class="main-content">
    <div class="page-header">
        <h1>管理员
            <small>
                <i class="icon-double-angle-right"></i>&nbsp;修改管理员
            </small>
        </h1>
    </div>
    
    <div class="page-content">
        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->

                <!--<form class="form-horizontal" name="form" method="post" action="<?php echo site_url('Admin/edit_user'); ?>" role="form">-->

                <form action="/admin.php/Admin/edit_user" class="form-horizontal" id="myform" method="post">
                <?php echo form_hidden('uid', $user['uid']); ?>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> 账号 </label>

                    <div class="col-sm-9">
                        <input type="text" name="login_name" id="form-field-1" placeholder="" value="<?php echo $user['login_name'] ?>"  class="col-xs-10 col-sm-5" />
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> 修改密码 </label>

                    <div class="col-sm-9">
                        <input type="password" name="pwd" id="form-field-2" placeholder="请输入新密码" class="col-xs-10 col-sm-5" />
                        <span class="help-inline col-xs-12 col-sm-7">
                            <!--<span class="middle">Inline help text</span>-->
                        </span>
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-input-readonly"> 权限分类 </label>
                    <div class="col-sm-9">
                        <select class="col-sm-3" name="menu_leval" class="form-control " id="form-field-select-1">
                            <?php foreach ($menu_level as $key => $value) { ?>
                                <option  <?php echo $value['id'] == $user['menu_leval'] ? "selected='selected'" : ''; ?>  value="<?php echo $value['id'] ?>"><?php echo $value['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="space-4"></div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-4">状态</label>

                    <div class="col-sm-9">
                        <select class="col-sm-3" name="status" class="form-control " id="form-field-select-1">
                            <option  <?php echo $user['status'] == 1 ? "selected='selected'" : ''; ?>  value="1">正常</option>
                            <option  <?php echo $user['status'] == 0 ? "selected='selected'" : ''; ?>  value="0">封号</option>
                        </select>
                    </div>
                </div>
                <div class="space-4"></div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-4">辖区</label>
                    <div class="col-sm-9">
                        <?php foreach ($area as $key => $value) { ?>
                            <div class="checkbox">
                                <label>
                                    <input name="area[]"  <?php echo in_array($value['id'], $user['area']) ? 'checked="checked"' : ''; ?>    type="checkbox" value="<?php echo $value['id'] ?>" class="ace ace-checkbox-2">
                                    <span class="lbl"> <?php echo $value['area_name'] ?></span>
                                </label>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="space-4"></div>
                <div class="clearfix form-actions">
                    <div class="col-md-offset-3 col-md-9">
                        <input class="btn btn-info" type="submit" name="ok" value="提交">
                        &nbsp; &nbsp; &nbsp;
                        <button id="back_url" class="btn" type="reset">
                            <i class="icon-undo bigger-110"></i>
                           	 返回
                        </button>
                    </div>
                </div>

                </form>

            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.page-content -->
</div><!-- /.main-content -->
<?php include_once APPPATH . 'views/public/footer.php'; ?>
<script>
    $("#back_url").click(function () {
        location.href = '<?php echo site_url('Admin/user_list'); ?>';
    });
</script>