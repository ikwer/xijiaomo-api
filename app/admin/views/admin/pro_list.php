<?php include_once APPPATH . 'views/public/header.php'; ?>
<div class="main-content">
    <div class="page-header">
        <h1>管理员
            <small>
                <i class="icon-double-angle-right"></i> 城市设置
            </small>
        </h1>
    </div><!-- /.page-header -->
    
    <div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try {
                ace.settings.check('breadcrumbs', 'fixed')
            } catch (e) {
            }
        </script>

        <ul class="breadcrumb" style="display: none;">
            <li>
                <a href="javascript:;" onclick="open_prompt()">
                    <button class="btn btn-sm btn-success">
                        <i class="icon-ok bigger-110"></i>
                        添加权限组
                    </button>
                </a>
            </li>
        </ul><!-- .breadcrumb -->
        <div class="nav-search" id="nav-search">
            <form class="form-search">
                <span class="input-icon">
                    <input type="text" placeholder="Search ..." name="kwd"  class="nav-search-input" value="<?php if(!empty($_GET['kwd'])){ echo $_GET['kwd']; } ?>" id="nav-search-input" autocomplete="off" />
                    <i class="icon-search nav-search-icon"></i>
                </span>
                <input type="submit" >
            </form>
        </div><!-- #nav-search -->
    </div>

    <div class="page-content">
        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->

                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="center">ID</th>
                                        <th>省份</th>

                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($pro_result as $key => $value) { ?>
                                        <tr>
                                            <td class="center"><?php echo $value['id'] ?></td>
                                            <td><?php echo $value['name'] ?></a></td>

                                            <td>
                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                    <a class="green" href='javascript:;' onclick="opens(<?php echo $value['id'] ?>)">
                                                        <i class="icon-pencil bigger-130"></i> 
                                                    </a>

                                                </div>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <div class="row"><div class="col-sm-6">
                                <div class="dataTables_info" id="sample-table-2_info"></div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="dataTables_paginate paging_bootstrap">
                                        <ul class="pagination">
                                            <?php echo $page_list; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.table-responsive -->
                    </div><!-- /span -->
                </div><!-- /row -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.page-content -->
</div><!-- /.main-content -->
<script>
function opens(id)
{
    var index = layer.open({
        type: 2,
        content: '/admin.php/Admin/city_config?aid='+id,
        area: ['750px', '500px'],
        maxmin: true
    });
}

function open_prompt()
{
    //prompt层
    layer.prompt({title: '新增权限组名称', formType: 1}, function(test, index){
        layer.close(index);
        $.post("/admin.php/Admin/aul_config",{name:test},function(data){
            layer.msg(data.msg);
            window.location.reload();
        },'json');
    });
    $(".layui-layer-input").attr('type','text');
}

    </script>
<?php include_once APPPATH . 'views/public/footer.php'; ?>