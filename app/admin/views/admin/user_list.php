<?php include_once APPPATH . 'views/public/header.php'; ?>
<div class="main-content">
    <div class="page-header">
        <h1>管理员
            <small>
                <i class="icon-double-angle-right"></i> 管理员列表
            </small>
        </h1>
    </div><!-- /.page-header -->
    
    <div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try {
                ace.settings.check('breadcrumbs', 'fixed')
            } catch (e) {
            }
        </script>

        <ul class="breadcrumb">
            <li> 
                <a href="<?php echo site_url('Admin/add_user'); ?>">
                    <button class="btn btn-sm btn-success">
                        <i class="icon-ok bigger-110"></i>
                        添加管理员
                    </button>
                </a>
            </li>
        </ul><!-- .breadcrumb -->
        <div class="nav-search" id="nav-search">
            <form class="form-search">
                <span class="input-icon">
                    <input type="text" placeholder="Search ..." name="kwd"  class="nav-search-input" value="<?php echo $kwd ?>" id="nav-search-input" autocomplete="off" />
                    <i class="icon-search nav-search-icon"></i>
                </span>
                <input type="submit" >
            </form>
        </div><!-- #nav-search -->
    </div>

    <div class="page-content">
        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->

                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="center">ID</th>
                                        <th>账号</th>
                                        <th>权限等级</th>
                                        <th class="hidden-480">状态</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($main_msg as $key => $value) { ?>
                                        <tr>
                                            <td class="center"><?php echo $value['uid'] ?></td>
                                            <td><a href="<?php $uid = $value['uid']; ?><?php echo site_url("Admin/edit_user/$uid") ?>"><?php echo $value['login_name'] ?></a></td>
                                            <td><?php echo $value['name'] ?></td>
                                            <td class="hidden-480">
                                                <?php if ($value['status'] == 1) { ?>
                                                    <span class="label label-sm label-success">正常</span>
                                                <?php } else { ?>
                                                    <span class = "label label-sm label-warning">封号</span>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                    <a class="green" href='/admin.php/Admin/edit_user/<?php echo $uid;?>'>
                                                        <i class="icon-pencil bigger-130"></i> 
                                                    </a>
                                                    <a class="red" onclick="return confirmDel()" href="/admin.php/Admin/del_user/<?php echo $uid;?>">
                                                        <i class="icon-trash bigger-130"></i> 
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <div class="row"><div class="col-sm-6">
                                <div class="dataTables_info" id="sample-table-2_info"></div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="dataTables_paginate paging_bootstrap">
                                        <ul class="pagination">
                                            <?php echo $page_list ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.table-responsive -->
                    </div><!-- /span -->
                </div><!-- /row -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.page-content -->
</div><!-- /.main-content -->
<?php include_once APPPATH . 'views/public/footer.php'; ?>