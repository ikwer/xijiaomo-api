<?php include_once APPPATH . 'views/public/header.php'; ?>
<script src="<?php echo JS_PATH ?>formdata.js"></script>
<div class="main-content">
    <div class="page-header">
        <h1><i class="icon-hand-right icon-animated-hand-pointer blue"></i>分类管理
            <small>
                <i class="icon-double-angle-right"></i>&nbsp;添加分类
            </small>
        </h1>
    </div>

    <div class="page-content">
        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                <form action="<?php echo '/admin.php/Cate/add'?>" class="form-horizontal" id="myform" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="formhash" value="1"/>
                    <input type="hidden" name="id" value="<?php if($this->input->get('id')){echo $goodsInfo['goods_id'];}?>"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 上级分类 </label>
                        <div class="col-sm-9">
                            <select class="col-sm-5" id="form-field-select-1" name="cate_id" required="required">
                                <option value="0">作为一级分类</option>
                                <?php foreach($goodsOptionList as $k=>$v){?>
                                    <option value="<?php echo $v['cate_id']?>"><?php echo $v['name']?></option>
                                <?php }?>
                            </select>
                        </div>
                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> 分类名称 </label>
                        <div class="col-sm-9">
                            <input type="text" name="name" id="form-field-2" required="required" placeholder="必填：分类名称" class="col-xs-10 col-sm-5" />
                            <span class="help-inline col-xs-12 col-sm-7">
                        </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-3"> （从小到大）： </label>
                        <div class="col-sm-9">
                            <input type="text" name="sort_order" id="form-field-3" value="" class="col-xs-10 col-sm-5" />
                            <span class="help-inline col-xs-12 col-sm-7">
                        </span>
                        </div>
                    </div>





                    <div class="space-4"></div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-3"> banner图片上传： </label>
                        <div class="col-sm-9">
                            <input name="key" id="key" type="hidden" value="">
                            <input name="token" type="hidden" value="<?php echo $uptoken?>">
                            <input id="userfile" name="file" type="file" />
                            <input name="accept" type="hidden" />
                            <input type="hidden" name="goods_img" id="goods_img"  value="<?php if($this->input->get('id')){ echo $goodsInfo['goods_image'];}?>" />
                            <!-- upload info -->

                            <div>

                    <span class="lbl">&nbsp;&nbsp;
                    <img<?php if($this->input->get('id')){ ?> src="<?php echo  $goodsInfo['goods_image']?>"<?php }else{?> src="<?php echo ASSETS_PATH ?>images/no_img.jpg"<?php }?>width="100" height="70" id="img0">
                    </span>&nbsp;&nbsp;
                            </div>
                            <!--
                            <div class="selected-file"></div>
                            <div class="uploaded-result"></div>-->
                        </div>
                    </div>
                    <div class="space-4"></div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-4"> 排序（从小到大）： </label>
                        <div class="col-sm-9">
                            <input type="text" name="sort_order" id="form-field-4" value="50" class="col-xs-10 col-sm-5" />
                            <span class="help-inline col-xs-12 col-sm-7">
                        </span>
                        </div>
                    </div>
                    <div class="space-4"></div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-5"> 详情介绍 </label>
                        <div class="col-sm-7">
                            <link href="<?php echo DATA_PATH ?>kindeditor/themes/default/default.css" rel="stylesheet" type="text/css"/>
                            <script src="<?php echo DATA_PATH ?>kindeditor/kindeditor-min.js" type="text/javascript" ></script>
                            <textarea rows="100%" style="width:100%" name="content" id="editor_id" class="editor">
							<?php if($this->input->get('id')){ echo $goodsInfo['goods_content'];}?>
						</textarea>
                        </div>
                    </div>
                    <div class="space-4"></div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-5"> 好评标签 </label>
                        <div class="col-sm-7">
                            <?php foreach($tag_list as $k=>$v): ?>
                                <label>
                                    <input type="checkbox" name="good_tags[]" value="<?php echo $v['id']; ?>" <?php if($v['id']==1){ echo 'checked'; } ?> ><?php echo $v['tag_name']; ?>
                                </label>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="space-4"></div>


                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-5"> 差评标签 </label>
                        <div class="col-sm-7">
                            <?php foreach($tag_list as $k=>$v): ?>
                                <label>
                                    <input type="checkbox" name="bad_tags[]" value="<?php echo $v['id']; ?>" <?php if($v['id']==1){ echo 'checked'; } ?> ><?php echo $v['tag_name']; ?>
                                </label>
                            <?php endforeach; ?>

                        </div>
                    </div>
                    <div class="space-4"></div>

                    <div class="clearfix form-actions">
                        <div class="col-md-offset-3 col-md-9">
                            <input class="btn btn-info" type="submit" name="ok" value="提交">
                            &nbsp; &nbsp; &nbsp;
                            <button id="back_url" class="btn" type="reset">
                                <i class="icon-undo bigger-110"></i>
                                返回
                            </button>
                        </div>
                    </div>
                </form>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.page-content -->
</div><!-- /.main-content -->

<?php include_once APPPATH . 'views/public/footer.php'; ?>
<script>
    $("#back_url").click(function () {
        location.href = '/admin.php/Cate/index';
    });
</script>
<script type="text/javascript">
    $(function(){
        KindEditor.ready(function (K) {
            window.editor = K.create('#editor_id', {items: [
                'paste', 'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
                'justifyfull', '|', 'forecolor', 'hilitecolor', '|', 'insertorderedlist', '|', '-', 'image', 'multiimage', '|', 'bold'], height: '200px'
            });
        });
    })

</script>
<script src="http://apps.bdimg.com/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
