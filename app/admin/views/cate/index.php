<?php include_once APPPATH . 'views/public/header.php'; ?>
<div class="main-content">
	<div class="page-header">
        <h1>
           	<i class="icon-hand-right icon-animated-hand-pointer blue"></i>分类管理
            <small>
                <i class="icon-double-angle-right"></i>
            	分类列表
            </small>
        </h1>
    </div><!-- /.page-header -->
	
	<div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try {
                ace.settings.check('breadcrumbs', 'fixed')
            } catch (e) {
            }
        </script>

        <ul class="breadcrumb">
            <li> 
                <a href="<?php echo '/admin.php/Cate/add' ?>">
                    <button class="btn btn-sm btn-success">
                        <i class="icon-ok bigger-110"></i>
                        	添加分类
                    </button>
                </a>
            </li>
        </ul><!-- .breadcrumb -->
        <div class="nav-search" id="nav-search">
            <form class="form-search">
                <span class="input-icon">
                    <input type="text" placeholder="Search ..." name="kwd"  class="nav-search-input" value="" id="nav-search-input" autocomplete="off" />
                    <i class="icon-search nav-search-icon"></i>
                </span>
                <input type="submit" >
            </form>
        </div><!-- #nav-search -->
    </div>
    <div class="page-content">
        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="center">ID</th>
                                        <th>分类名称</th>
                                        <th>上级分类</th>
                                        <th class="hidden-480">状态</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php foreach ($page_data as $key => $value) { ?>
                                        <tr>
                                            <td class="center"><?php echo $value['cate_id'] ?></td>
                                            <td><?php echo $value['name'] ?></td>
                                             <td><?php if($value['parent_name']){echo $value['parent_name'];}else{echo '顶级分类';} ?></td>
                                            <td class="hidden-480">
                                            	<?php if ($value['is_show'] == 0) { ?>
                                                    <span class="label label-sm label-success">显示</span>
                                                <?php } else { ?>
                                                    <span class = "label label-sm label-warning">隐藏</span>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                    <a class="green" href='javascript:;' title="添加子类"> 
                                                        <i class="ace-icon fa fa-plus-circle bigger-130"></i> 
                                                    </a>
                                                    <a class="green" href='/admin.php/Cate/edit?id=<?php echo $value['cate_id'] ?>' title="修改">
                                                        <i class="icon-pencil bigger-130"></i> 
                                                    </a>
                                                    <a class="red" onclick="return confirmDel('/admin.php/Cate/del','<?php echo $value['cate_id']?>')" href="" title="删除">
                                                        <i class="icon-trash bigger-130"></i> 
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php }?>
                                </tbody>
                            </table>
                        </div><!-- /.table-responsive -->
                    </div><!-- /span -->
                </div><!-- /row -->
                <div class="row">
                	<div class="col-sm-6">
                    	<div class="dataTables_info" id="sample-table-2_info"></div>
                    </div>
                    <div class="col-sm-6">
                        <div class="dataTables_paginate paging_bootstrap">
                            <ul class="pagination">
                            	<?php echo $page_list ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.page-content -->
</div><!-- /.main-content -->
<?php include_once APPPATH . 'views/public/footer.php'; ?>