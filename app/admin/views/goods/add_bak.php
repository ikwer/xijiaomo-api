<?php include_once APPPATH . 'views/public/header.php'; ?>
<div class="main-content">
	<div class="page-header">
        <h1><i class="icon-hand-right icon-animated-hand-pointer blue"></i>产品管理
            <small>
                <i class="icon-double-angle-right"></i>&nbsp;添加产品
            </small>
        </h1>
    </div>

    <div class="page-content">
        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                <form action="<?php echo site_url('Goods/add')?>>" class="form-horizontal" id="myform" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> 产品所属分类 </label>
                    <div class="col-sm-9">
                        <select class="col-sm-5" id="form-field-select-1" name="cate_id" required="required">
							<option value="">请选择分类</option>
							<option value="spa">spa</option>
						</select>
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> 产品名称 </label>
                    <div class="col-sm-9">
                        <input type="text" name="name" id="form-field-2" required="required" placeholder="必填：分类名称" class="col-xs-10 col-sm-5" />
                        <span class="help-inline col-xs-12 col-sm-7">
                        </span>
                    </div>
                </div>
                
                <div class="space-4"></div>
                
                <div class="form-group">
					<label class="col-sm-3 control-label no-padding-right" for="form-field-3"> 封面图片上传： </label>
					<div class="col-sm-7">
						<input type="hidden" id="token" name="uptoken" value="<?php echo $uptoken?>">
						<input type="hidden" id="key" name="bucket" value="<?php echo $bucket?>">
						<input type="hidden" id="btn_upload"/>
						<input type="hidden" id="goods_image" name="goods_image">
						<a href="javascript:;" class="file">
							<input type="file" id="file"  required="required">
							选择上传文件
						</a>
						<span class="lbl">&nbsp;&nbsp;<img src="<?php echo ASSETS_PATH ?>images/no_img.jpg" width="100" height="70" id="img0"></span>&nbsp;&nbsp;
						<div id="progressbar"><div class="progress-label"></div></div>
					</div>
				</div>
				<div class="space-4"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-4"> 排序（从小到大）： </label>
                    <div class="col-sm-9">
                        <input type="text" name="sort_order" id="form-field-4" value="50" class="col-xs-10 col-sm-5" />
                        <span class="help-inline col-xs-12 col-sm-7">
                        </span>
                    </div>
                </div>
                <div class="space-4"></div>
                
                <div class="form-group">
					<label class="col-sm-3 control-label no-padding-right" for="form-field-5"> 文章主内容 </label>
					<div class="col-sm-7">
						<link href="<?php echo DATA_PATH ?>kindeditor/themes/default/default.css" rel="stylesheet" type="text/css"/>
						<script src="<?php echo DATA_PATH ?>kindeditor/kindeditor-min.js" type="text/javascript" ></script>
						<textarea rows="100%" style="width:100%" name="content" id="editor_id" class="editor">
							
						</textarea>
					</div>
				</div>
                <div class="space-4"></div>
                
                <!--<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right" for="form-field-5"> 文章主内容 </label>
					<div class="col-sm-7">
						<script src="<?php echo DATA_PATH ?>ueditor/ueditor.config.js" type="text/javascript"></script>
						<script src="<?php echo DATA_PATH ?>ueditor/ueditor.all.js" type="text/javascript"></script>
						<textarea name="news_content" rows="100%" style="width:100%" id="myEditor"></textarea>
						<script type="text/javascript">
							var editor = new UE.ui.Editor();
							editor.render("myEditor");
						</script>
					</div>
				</div>
                <div class="space-4"></div>-->
                
                <div class="clearfix form-actions">
                    <div class="col-md-offset-3 col-md-9">
                        <input class="btn btn-info" type="submit" name="ok" value="提交">
                        &nbsp; &nbsp; &nbsp;
                        <button id="back_url" class="btn" type="reset">
                            <i class="icon-undo bigger-110"></i>
                            	返回
                        </button>
                    </div>
                </div>
                </form>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.page-content -->
</div><!-- /.main-content -->

<?php include_once APPPATH . 'views/public/footer.php'; ?>
<script>
    $("#back_url").click(function () {
        location.href = '<?php echo site_url('Goods/index'); ?>';
    });
</script>
<script type="text/javascript">
    $(function(){
    	KindEditor.ready(function (K) {
            window.editor = K.create('#editor_id', {items: [
                'paste', 'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
                'justifyfull', '|', 'forecolor', 'hilitecolor', '|', 'insertorderedlist', '|', '-', 'image', 'multiimage', '|', 'bold'], height: '200px'
            });
        });
    })
	
</script>
<script src="http://apps.bdimg.com/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
