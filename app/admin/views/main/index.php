<?php include_once APPPATH . 'views/public/header.php'; ?>
    <link rel="stylesheet" type="text/css" href="<?php echo JS_PATH ?>webuploader/webuploader.css">
    <script type="text/javascript" src="<?php echo JS_PATH ?>webuploader/webuploader.js"></script>
    <div class="main-content">
        <div class="page-header">
            <h1> 控制台
                <small>
                    <i class="icon-double-angle-right"></i>&nbsp;查看
                </small>
            </h1>
        </div>
        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    <div class="alert alert-block alert-success">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="icon-remove"></i>
                        </button>

                        <i class="icon-ok green"></i>欢迎使用
                        <strong class="green">喜脚么后台管理系统
                        </strong>
                    </div>

                    <div class="row">
                        <div class="space-6"></div>

                        <form action="" class="form-horizontal" id="myform" method="post">


                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-2">Slogan： </label>
                                <div class="col-sm-9">
                                    <input type="text" name="slogan" id="form-field-2" required="required" placeholder="必填：Slogan" value="<?php echo $result['slogan']; ?>" class="col-xs-10 col-sm-5">
                                    <span class="help-inline col-xs-12 col-sm-7">
                        </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-2">网站域名： </label>
                                <div class="col-sm-9">
                                    <input type="text" name="website" id="form-field-2" required="required" value="<?php echo $result['website']; ?>" placeholder="必填：网站域名" class="col-xs-10 col-sm-5">
                                    <span class="help-inline col-xs-12 col-sm-7">
                        </span>
                                </div>
                            </div>

                            <div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-2">客服邮箱： </label>
                                <div class="col-sm-9">
                                    <input type="text" name="kf_email" id="form-field-2" required="required" value="<?php echo $result['kf_email']; ?>" placeholder="必填：客服邮箱" class="col-xs-10 col-sm-5">
                                    <span class="help-inline col-xs-12 col-sm-7">
                        </span>
                                </div>
                            </div>


                            <div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-2">微信公众号： </label>
                                <div class="col-sm-9">
                                    <input type="text" name="weixin_code" id="form-field-2" required="required" value="<?php echo $result['weixin_code']; ?>" placeholder="必填：微信公众号" class="col-xs-10 col-sm-5">
                                    <span class="help-inline col-xs-12 col-sm-7">
                        </span>
                                </div>
                            </div>


                            <div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-2">客服QQ： </label>
                                <div class="col-sm-9">
                                    <input type="text" name="kf_qq" id="form-field-2" required="required" value="<?php echo $result['kf_qq']; ?>" placeholder="必填：客服QQ" class="col-xs-10 col-sm-5">
                                    <span class="help-inline col-xs-12 col-sm-7">
                        </span>
                                </div>
                            </div>


                            <div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-2">公司电话： </label>
                                <div class="col-sm-9">
                                    <input type="text" name="company_phone" id="form-field-2" required="required" value="<?php echo $result['company_phone']; ?>" placeholder="必填：公司电话" class="col-xs-10 col-sm-5">
                                    <span class="help-inline col-xs-12 col-sm-7">
                        </span>
                                </div>
                            </div>

                            <div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-2">备注： </label>
                                <div class="col-sm-9">
                                    <textarea name="ps" style="width:50%;height:90px;" id="form-field-2" required="required" value="" placeholder="必填：备注" class="col-xs-10 col-sm-5"><?php echo $result['ps']; ?></textarea>
                                    <span class="help-inline col-xs-12 col-sm-7">
                        </span>
                                </div>
                            </div>


                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-2">安卓-用户端版本号： </label>
                                <div class="col-sm-9">
                                    <input type="text" name="au_version" id="form-field-2" required="required" value="<?php echo $result['au_version']; ?>" placeholder="必填：安卓-用户端版本号" class="col-xs-10 col-sm-5">
                                    <span class="help-inline col-xs-12 col-sm-7">
                        </span>
                                </div>
                            </div>
                            
                            
                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-2">安卓-技师端版本号： </label>
                                <div class="col-sm-9">
                                    <input type="text" name="at_version" id="form-field-2" required="required" value="<?php echo $result['at_version']; ?>" placeholder="必填：安卓-技师端版本号" class="col-xs-10 col-sm-5">
                                    <span class="help-inline col-xs-12 col-sm-7">
                        </span>
                                </div>
                            </div>
                            
                            
                            
                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-2">苹果-用户端版本号： </label>
                                <div class="col-sm-9">
                                    <input type="text" name="iu_version" id="form-field-2" required="required" value="<?php echo $result['iu_version']; ?>" placeholder="必填：苹果-用户端版本号" class="col-xs-10 col-sm-5">
                                    <span class="help-inline col-xs-12 col-sm-7">
                        </span>
                                </div>
                            </div>
                            
                            
                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-2">苹果-技师端版本号： </label>
                                <div class="col-sm-9">
                                    <input type="text" name="it_version" id="form-field-2" required="required" value="<?php echo $result['it_version']; ?>" placeholder="必填：安卓-用户端版本号" class="col-xs-10 col-sm-5">
                                    <span class="help-inline col-xs-12 col-sm-7">
                        </span>
                                </div>
                            </div>
                            
                            <!--<div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-5"> 文章主内容 </label>
                                <div class="col-sm-7">
                                    <script src="/data/ueditor/ueditor.config.js" type="text/javascript"></script>
                                    <script src="/data/ueditor/ueditor.all.js" type="text/javascript"></script>
                                    <textarea name="news_content" rows="100%" style="width:100%" id="myEditor"></textarea>
                                    <script type="text/javascript">
                                        var editor = new UE.ui.Editor();
                                        editor.render("myEditor");
                                    </script>
                                </div>
                            </div>
                            <div class="space-4"></div>-->

                            <div id="uploader-demo" style="margin-left:20%">
                                <!--用来存放item-->
                                <div id="fileList" class="uploader-list"></div>
                                <div id="filePicker">客服banner</div>
                                <p class="progress"><span></span></p>
                                <input type="hidden" name="kf_banner">
                                <img src="<?php echo $result['kf_banner']; ?>" id="kf_bans">
                            </div>

                            <div class="space-4"></div>



                            <div class="space-4"></div>


                            <div class="clearfix form-actions">
                                <div class="col-md-offset-3 col-md-9">
                                    <input class="btn btn-info" type="button" name="ok" value="提交">
                                    &nbsp; &nbsp; &nbsp;

                                </div>
                            </div>
                        </form>



                    </div><!-- /row -->
                    <!-- PAGE CONTENT ENDS -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div><!-- /.main-content -->

    <style>
        .progress{background: #fff;}
    </style>

    <script type="text/javascript">

        $(".btn-info").click(function(){
            var loading = layer.load(1, {shade:0.7});
            $.post("/admin.php/Main/index",$("#myform").serialize(),function(data){
                layer.close(loading);
                layer.msg(data.msg);
                //window.location.reload();
            },"json");
        });

        // 初始化Web Uploader
        var uploader = WebUploader.create({

            // 选完文件后，是否自动上传。
            auto: true,

            // swf文件路径
            swf: '<?php echo JS_PATH; ?>' + '/webuploader/Uploader.swf',

            // 文件接收服务端。
            server: '/admin.php/Main/upload',

            // 选择文件的按钮。可选。
            // 内部根据当前运行是创建，可能是input元素，也可能是flash.
            pick: '#filePicker',

            // 只允许选择图片文件。
            accept: {
                title: 'Images',
                extensions: 'gif,jpg,jpeg,bmp,png',
                mimeTypes: 'image/*'
            }
        });

        // 文件上传过程中创建进度条实时显示。
        uploader.on( 'uploadProgress', function( file, percentage ) {
            $(".progress span").html("正在上传中...");
        });

        // 文件上传成功，给item添加成功class, 用样式标记上传成功。
        uploader.on( 'uploadSuccess', function( file,response ) {
            $(".progress span").html("上传成功");
            $("input[name='kf_banner']").val(response._raw);
            $("#kf_bans").attr('src',response._raw);
            $( '#'+file.id ).addClass('upload-state-done');
        });

        // 文件上传失败，显示上传出错。
        uploader.on( 'uploadError', function( file ) {
            $(".progress span").html("上传失败");
            var $li = $( '#'+file.id ),
                $error = $li.find('div.error');

            // 避免重复创建
            if ( !$error.length ) {
                $error = $('<div class="error"></div>').appendTo( $li );
            }

            $error.text('上传失败');
        });

        // 完成上传完了，成功或者失败，先删除进度条。
        uploader.on( 'uploadComplete', function( file ) {
            $( '#'+file.id ).find('.progress').remove();
        });

    </script>
<?php include_once APPPATH . 'views/public/footer.php'; ?>