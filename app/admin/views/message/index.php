<?php include_once APPPATH . 'views/public/header.php'; ?>
<div class="main-content">
    <div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try {
                ace.settings.check('breadcrumbs', 'fixed')
            } catch (e) {
            }
        </script>

        <ul class="breadcrumb">
            <li>
                <i class="icon-home home-icon"></i>
                <a href="#">首页</a>
            </li>
            <li class="active">控制台</li>
        </ul><!-- .breadcrumb -->

        <div class="nav-search" id="nav-search">
            <form class="form-search">
                <span class="input-icon">
                    <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                    <i class="icon-search nav-search-icon"></i>
                </span>
            </form>
        </div><!-- #nav-search -->
    </div>

    <div class="page-content">
        <div class="row">
            <div class="col-xs-12">
				<!-- PAGE CONTENT BEGINS -->
				<div class="well center">
					<h1 class="grey lighter smaller">
						<span class="blue bigger-125">
							<i class="icon-sitemap"></i>
							系统提示!
						</span>
					</h1>
					<hr>
					<h3 class="lighter smaller"><i class="icon-hand-right blue"></i> <?php echo $msg; ?></h3>
                    <hr>
                    <div>
                        <?php if($url_forward=='goback' || $url_forward=='') {?>
                        	<a href="javascript:history.back();" class="btn btn-grey"><i class="icon-arrow-left">[返回上一步]</i></a>
                        <?php } elseif($url_forward=="close") {?>
                        	<input type="button" name="close" value="关闭" onClick="window.close();">
                        <?php } elseif($url_forward=="blank") {?>
                        	
                        <?php } elseif($url_forward && !$returnjs) {
                        ?>
                        <a href="<?php echo $url_forward?>" class="btn btn-grey"><i class="icon-arrow-left">点击这里</i></a>
                        <script language="javascript">setTimeout(function(){window.location.href='<?php echo $url_forward?>';},<?php echo $ms?>);</script>
                        <?php }?>
                        <?php if($returnjs) { ?> <script style="text/javascript"><?php echo $returnjs;?></script><?php } ?>
                        <?php if ($dialog):?><script style="text/javascript">window.top.right.location.reload();window.top.art.dialog({id:"<?php echo $dialog?>"}).close();</script><?php endif;?>
                    </div>
				</div>
				<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.page-content -->
</div><!-- /.main-content -->
<?php include_once APPPATH . 'views/public/footer.php'; ?>