<?php include_once APPPATH . 'views/public/header.php'; ?>
<div class="main-content">
	<div class="page-header">
        <h1>
           	<i class="icon-hand-right icon-animated-hand-pointer blue"></i>订单管理
            <small>
                <i class="icon-double-angle-right"></i>
            	提现列表
            </small>
        </h1>
    </div><!-- /.page-header -->
	
	<div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try {
                ace.settings.check('breadcrumbs', 'fixed')
            } catch (e) {
            }
        </script>

        <ul class="breadcrumb">
            <li> 
                <a href="javascript:;">

                </a>
            </li>
        </ul><!-- .breadcrumb -->
        <div class="nav-search" id="nav-search">
            <form class="form-search">
                <span class="input-icon">

                    <input type="text" placeholder="Search ..." name="kwd"  class="nav-search-input" value="<?php if($this->input->get('kwd')){ echo $this->input->get('kwd');  } ?>" id="nav-search-input" autocomplete="off" />
                    <i class="icon-search nav-search-icon"></i>
                </span>
                <input type="submit" >
            </form>
        </div><!-- #nav-search -->
    </div>
    <div class="page-content">
        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="center">ID</th>
                                        <th>技师名称</th>
                                        <th>提现渠道</th>
                                        <th>提现账号</th>
                                        <th>提现金额</th>
                                        <th>审核状态</th>
                                        <th class="hidden-480">添加时间</th>
                                        <th class="hidden-480">审核时间</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php foreach ($page_data as $key => $value) { ?>
                                        <tr>
                                            <td class="center"><?php echo $value['id'] ?></td>
                                            <td><?php echo $value['nickname'] ?></td>
                                            <td><?php echo $cash_option[$value['type']]; ?></td>
                                            <td><?php echo $value['cash_account']; ?></td>
                                            <td><?php echo $value['cash_money']; ?></td>
                                            <td><?php echo $cash_status[$value['status']]; ?></td>
                                            <td class="hidden-480">
                                            	<span class="label label-sm label-success"><?php echo date("Y-m-d H:i:s",$value['add_time']); ?></span>
                                            </td>
                                            <td class="hidden-480">
                                                <span class="label label-sm label-success"><?php echo date("Y-m-d H:i:s",$value['set_time']); ?></span>
                                            </td>
                                            <td>
                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                    <a class="green" href='javascript:;' title="查看" onclick="showbox(<?php echo $value['id'] ?>)" >
                                                        查看详情
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php }?>
                                </tbody>
                            </table>
                        </div><!-- /.table-responsive -->
                    </div><!-- /span -->
                </div><!-- /row -->
                <div class="row">
                	<div class="col-sm-6">
                    	<div class="dataTables_info" id="sample-table-2_info"></div>
                    </div>
                    <div class="col-sm-6">
                        <div class="dataTables_paginate paging_bootstrap">
                            <ul class="pagination">
                            	<?php echo $page_list ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.page-content -->
</div><!-- /.main-content -->

    <script type="text/javascript">
        function showbox(id)
        {
            var index = layer.open({
                type: 2,
                content: '/admin.php/order/cash_info?id='+id,
                area: ['750px', '500px'],
                maxmin: true
            });
        }
    </script>

<?php include_once APPPATH . 'views/public/footer.php'; ?>