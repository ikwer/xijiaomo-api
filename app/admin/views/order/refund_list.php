<?php include_once APPPATH . 'views/public/header.php'; ?>
<div class="main-content">
	<div class="page-header">
        <h1>
           	<i class="icon-hand-right icon-animated-hand-pointer blue"></i>订单管理
            <small>
                <i class="icon-double-angle-right"></i>
            	退款列表
            </small>
        </h1>
    </div><!-- /.page-header -->
	
	<div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try {
                ace.settings.check('breadcrumbs', 'fixed')
            } catch (e) {
            }
        </script>

        <ul class="breadcrumb">
            <li> 
                <a href="javascript:;">

                </a>
            </li>
        </ul><!-- .breadcrumb -->
        <div class="nav-search" id="nav-search">
            <form class="form-search">
                <span class="input-icon">
                    <input type="text" placeholder="Search ..." name="kwd"  class="nav-search-input" value="" id="nav-search-input" autocomplete="off" />
                    <i class="icon-search nav-search-icon"></i>
                </span>
                <input type="submit" >
            </form>
        </div><!-- #nav-search -->
    </div>
    <div class="page-content">
        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="center">ID</th>
                                        <th>订单编号</th>
                                        <th>商品名称</th>
                                        <th>退款原因</th>
                                        <th>实退款金额</th>
                                        <th>退款备注</th>
                                        <th>退款用户</th>
                                        <th>退款技师</th>
                                        <th>申请时间</th>
                                        <th>状态</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($page_data as $k=>$v): ?>
                                        <tr>
                                            <td class="center"><?php echo $v['id']; ?></td>
                                            <td><?php echo $v['order_sn']; ?></td>
                                            <td><?php echo $v['goods_name']; ?></td>
                                            <td><?php echo $v['refund_reason']; ?></td>
                                            <td><?php echo $v['refund_money']; ?></td>
                                            <td><?php echo $v['refund_note']; ?></td>
                                            <td><?php echo $v['unick']; ?></td>
                                            <td><?php echo $v['tnick']; ?></td>
                                            <td><?php echo date('Y-m-d H:i:s',$v['add_time']); ?></td>
                                            <td>
                                                <select name="status" class="status" id="<?php echo $v['id']; ?>">
                                                    <option value="<?php echo $v['status']; ?>" selected><?php echo $refund_status[$v['status']]; ?></option>
                                                    <?php foreach($refund_status as $ks=>$vs): ?>
                                                        <?php if($ks!=$v['status']): ?>
                                                        <option value="<?php echo $ks; ?>"><?php echo $vs; ?></option>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                            </td>
                                            <td>
                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                    <a class="green" href='javascript:;' title="修改"> 
                                                        <i class="icon-pencil bigger-130"></i> 
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div><!-- /.table-responsive -->
                    </div><!-- /span -->
                </div><!-- /row -->
                <div class="row">
                	<div class="col-sm-6">
                    	<div class="dataTables_info" id="sample-table-2_info"></div>
                    </div>
                    <div class="col-sm-6">
                        <div class="dataTables_paginate paging_bootstrap">
                            <ul class="pagination">
                            	<?php echo $page_list ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.page-content -->
</div><!-- /.main-content -->
<script>
    $(".status").change(function(){
        var zhi = $(this).children('option:selected').val();
        var id = $(this).attr('id');

        var loading=layer.load(1, {shade:0.7});
        $.post("/admin.php/order/refund_list",{status:zhi,id:id},function(data){
            layer.close(loading);
            layer.msg(data.msg);
            // window.location.reload();
        },'json');
    });
</script>

<?php include_once APPPATH . 'views/public/footer.php'; ?>