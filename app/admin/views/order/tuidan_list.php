<?php include_once APPPATH . 'views/public/header.php'; ?>
<div class="main-content">
	<div class="page-header">
        <h1>
           	<i class="icon-hand-right icon-animated-hand-pointer blue"></i>订单管理
            <small>
                <i class="icon-double-angle-right"></i>
            	退单列表
            </small>
        </h1>
    </div><!-- /.page-header -->
	
	<div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try {
                ace.settings.check('breadcrumbs', 'fixed')
            } catch (e) {
            }
        </script>

        <ul class="breadcrumb">
            <li> 
                <a href="javascript:;">

                </a>
            </li>
        </ul><!-- .breadcrumb -->
        <div class="nav-search" id="nav-search">
            <form class="form-search">
                <span class="input-icon">
                    <input type="text" placeholder="Search ..." name="kwd"  class="nav-search-input" value="" id="nav-search-input" autocomplete="off" />
                    <i class="icon-search nav-search-icon"></i>
                </span>
                <input type="submit" >
            </form>
        </div><!-- #nav-search -->
    </div>
    <div class="page-content">
        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="center">ID</th>
                                        <th>订单编号</th>
                                        <th>商品名称</th>
                                        <th>退款用户</th>
                                        <th>退款技师</th>
                                        <th>申请时间</th>
                                        <th>操作员</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($page_data as $k=>$v): ?>
                                        <tr>
                                            <td class="center"><?php echo $v['id']; ?></td>
                                            <td><?php echo $v['order_sn']; ?></td>
                                            <td><?php echo $v['goods_name']; ?></td>
                                            <td><?php echo $v['tname']; ?></td>
                                            <td><?php echo $v['uname']; ?></td>
                                            <td><?php echo date('Y-m-d H:i:s',$v['add_time']); ?></td>

                                            <td><?php echo $v['login_name']; ?></td>

                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div><!-- /.table-responsive -->
                    </div><!-- /span -->
                </div><!-- /row -->
                <div class="row">
                	<div class="col-sm-6">
                    	<div class="dataTables_info" id="sample-table-2_info"></div>
                    </div>
                    <div class="col-sm-6">
                        <div class="dataTables_paginate paging_bootstrap">
                            <ul class="pagination">
                            	<?php echo $page_list ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.page-content -->
</div><!-- /.main-content -->


<?php include_once APPPATH . 'views/public/footer.php'; ?>