</div><!-- /.main-container-inner -->

<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="icon-double-angle-up icon-only bigger-110"></i>
</a>
</div><!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->



<!-- <![endif]-->

<!--[if IE]>
<script src="<?php echo JS_PATH ?>jquery-2.0.3.min.js"></script>
<![endif]-->

<!--[if !IE]> -->

<script type="text/javascript">
    window.jQuery || document.write("<script src='<?php echo JS_PATH ?>jquery-2.0.3.min.js'>" + "<" + "script>");
</script>

<!-- <![endif]-->

<!--[if IE]>
<script type="text/javascript">
window.jQuery || document.write("<script src='<?php echo JS_PATH ?>jquery-1.10.2.min.js'>"+"<"+"script>");
</script>
<![endif]-->

<script type="text/javascript">
    if ("ontouchend" in document)
        document.write("<script src='<?php echo JS_PATH ?>jquery.mobile.custom.min.js'>" + "<" + "script>");
</script>
<script src="<?php echo JS_PATH ?>bootstrap.min.js"></script>
<script src="<?php echo JS_PATH ?>typeahead-bs2.min.js"></script>

<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
  <script src="<?php echo JS_PATH ?>excanvas.min.js"></script>
<![endif]-->

<script src="<?php echo JS_PATH ?>jquery-ui-1.10.3.custom.min.js"></script>
<script src="<?php echo JS_PATH ?>jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo JS_PATH ?>jquery.slimscroll.min.js"></script>
<script src="<?php echo JS_PATH ?>jquery.easy-pie-chart.min.js"></script>
<script src="<?php echo JS_PATH ?>jquery.sparkline.min.js"></script>
<!--<script src="<?php echo JS_PATH ?>flot/jquery.flot.min.js"></script>
<script src="<?php echo JS_PATH ?>flot/jquery.flot.pie.min.js"></script>
<script src="<?php echo JS_PATH ?>flot/jquery.flot.resize.min.js"></script>-->

<!-- ace scripts -->

<script src="<?php echo JS_PATH ?>ace-elements.min.js"></script>
<script src="<?php echo JS_PATH ?>ace.min.js"></script>
<script src="<?php echo JS_PATH ?>xjm.js"></script>
<!-- inline scripts related to this page -->
</body>
</html>

<script>
//     function confirmDel() {
//         var str = '确定要删除吗?,此过程不可逆,请谨慎操作！';
//         return confirm(str);
//     }
    function confirmDel(url,id){
		if(confirm('确定要删除吗?,此过程不可逆,请谨慎操作！')){
			$.post(url,{id:id},function(data){
				if(data.status){
					window.location.href=data.url;
				}
			},'json')	
		}
    }
</script>