<div class="sidebar" id="sidebar">
    <script type="text/javascript">
        try {
            ace.settings.check('sidebar', 'fixed')
        } catch (e) {
        }
    </script>

    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <button class="btn btn-success">
                <i class="icon-signal"></i>
            </button>

            <button class="btn btn-info">
                <i class="icon-pencil"></i>
            </button>

            <button class="btn btn-warning">
                <i class="icon-group"></i>
            </button>

            <button class="btn btn-danger">
                <i class="icon-cogs"></i>
            </button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>

            <span class="btn btn-info"></span>

            <span class="btn btn-warning"></span>

            <span class="btn btn-danger"></span>
        </div>
    </div><!-- #sidebar-shortcuts -->

    <ul class="nav nav-list">
        <?php foreach ($this->left_list as $key => $value) { ?>
            <?php if (count($value['child']) > 0) { ?>
                <li <?php echo $value['id'] == $this->now_show ? 'class="active"' : ''; ?>>
                    <a href="" class="dropdown-toggle">
                        <!--<i class="icon-user"></i>-->
                        <i class="icon-dashboard"></i>
                        <span class="menu-text"> <?php echo $value['name'] ?> </span>
                        <b class="arrow icon-angle-down"></b>
                    </a>
                    <ul class="submenu">
                        <?php foreach ($value['child'] as $ck => $cv) {?>
                            <li <?php echo $cv['id'] == $this->check_id ? 'class="active"' : ''; ?>>
                                <a href="/admin.php/<?php echo $cv['class'] . '/' . $cv['function'] ?>">
                                    <i class="icon-double-angle-right"></i>
                                    <?php echo $cv['name']; ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } else { ?>
                <li <?php echo $this->now_show == 0 ? 'class="active"' : ''; ?>>
                    <a href="/admin.php/<?php echo $value['class'] . '/' . $value['function'] ?>">
                        <i class="icon-text-width"></i>
                        <span class="menu-text"> <?php echo $value['name'] ?> </span>
                    </a>
                </li>
            <?php } ?>
        <?php } ?>
    </ul><!-- /.nav-list -->

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>

    <script type="text/javascript">
        try {
            ace.settings.check('sidebar', 'collapsed')
        } catch (e) {
        }
    </script>
</div>