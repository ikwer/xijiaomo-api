<?php include_once APPPATH . 'views/public/header.php'; ?>

    <script type="text/javascript" src="<?php echo JS_PATH ?>echarts.js"></script>
    <script type="text/javascript" src="<?php echo JS_PATH ?>laydate/laydate.js"></script>

    <div class="main-content">
        <div class="page-header">
            <h1>
                <i class="icon-hand-right icon-animated-hand-pointer blue"></i>技师管理
                <small>
                    <i class="icon-double-angle-right"></i>
                    添加投诉
                </small>
            </h1>
        </div><!-- /.page-header -->

        <div style="margin-left: 10%;">
        <input type="text" name="order_sn" value="" placeholder="输入订单编号搜索订单"><input type="button" value="搜索" id="search">
        <form action="" method="post">
            <p>订单编号：<span id="order_sn"></span></p>
            <p>技师姓名：<span id="uname"></span></p>
            <p>用户姓名：<span id="tname"></span></p>
            <p>服务项目：<span id="goods_name"></span></p>
            <p>服务时长：<span id="server_time"></span></p>
            <p>项目金额：<span id="goods_price"></span></p>
            <p>订单金额：<span id="order_amount"></span></p>
            <p>补贴金额：<span id="xiaofei"></span></p>
            <p>投诉内容：<textarea id="content" style="width: 50%;height:200px;"></textarea></p>
            <p>
                <input type="hidden" name="order_id" id="order_id">
                <input type="hidden" name="tid" id="tid">
            <input type="button" id="tijiao" value="提交" style="width:100px;height:50px;line-height:50px;margin-left: 10%;margin-top:3%;border: 0;background: #ff00ff;color: #fff;font-size: 14px;">
            </p>
        </form>
        </div>

    </div>
    </div>




<script type="text/javascript">
    $("#search").click(function(){
        var order_sn=$("input[name='order_sn']").val();
        order_sn=$.trim(order_sn);
        var loading = layer.load(1, {shade:0.7});
        $.get('/admin.php/Techer/add_tousu',{order_sn:order_sn},function (data) {
            layer.close(loading);
            if(data.code==0){
                $('#order_sn').html(data.data.order_sn);
                $('#uname').html(data.data.uname);
                $('#tname').html(data.data.tname);
                $('#goods_name').html(data.data.goods_name);
                $('#server_time').html(data.data.server_time);
                $('#goods_price').html(data.data.goods_price);
                $('#order_amount').html(data.data.order_amount);
                $('#xiaofei').html(data.data.xiaofei);

                $("#order_id").val(data.data.order_id);
                $("#tid").val(data.data.tid);
            }else{
                layer.msg(data.msg);
                $('#order_sn').html('');
                $('#uname').html('');
                $('#tname').html('');
                $('#goods_name').html('');
                $('#server_time').html('');
                $('#goods_price').html('');
                $('#order_amount').html('');
                $('#xiaofei').html('');

                $("#order_id").val('');
                $("#tid").val('');
             }
        },'json');
    });


    $("#tijiao").click(function(){
        var order_id=$("input[name='order_id']").val();
        order_id=$.trim(order_id);

        var tid=$("input[name='tid']").val();
        tid=$.trim(tid);

        if(order_id=='' || tid==''){
            return false;
        }else{
            var load = layer.load(1, {shade:0.7});
            $.post('/admin.php/Techer/add_tousu',{order_id:order_id,tid:tid,content:$("#content").val()},function (data) {
                layer.close(load);

                 alert(data.msg);
                 window.location.reload();

            },'json');
        }

    });

</script>

<?php include_once APPPATH . 'views/public/footer.php'; ?>