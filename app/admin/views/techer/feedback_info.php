<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>控制台 - 喜脚么后台管理系统</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!-- basic styles -->
        <link href="<?php echo CSS_PATH ?>bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo CSS_PATH ?>font-awesome.min.css" />

        <!--[if IE 7]>
          <link rel="stylesheet" href="<?php echo CSS_PATH ?>font-awesome-ie7.min.css" />
        <![endif]-->

        <!-- page specific plugin styles -->

        <!-- fonts -->
        <script src="<?php echo JS_PATH ?>jquery-2.0.3.min.js"></script>

        <!-- ace styles -->

        <link rel="stylesheet" href="<?php echo CSS_PATH ?>ace.min.css" />
        <link rel="stylesheet" href="<?php echo CSS_PATH ?>ace-rtl.min.css" />
        <link rel="stylesheet" href="<?php echo CSS_PATH ?>ace-skins.min.css" />

        <!--[if lte IE 8]>
          <link rel="stylesheet" href="<?php echo CSS_PATH ?>ace-ie.min.css" />
        <![endif]-->

        <!-- inline styles related to this page -->

        <!-- ace settings handler -->

        <script src="<?php echo JS_PATH ?>ace-extra.min.js"></script>
        <!-- layer -->
        <link rel="stylesheet" href="<?php echo JS_PATH ?>layer/theme/default/layer.css">
        <script src="<?php echo JS_PATH ?>layer/layer.js"></script>
        <!-- layer -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

        <!--[if lt IE 9]>
        <script src="<?php echo JS_PATH ?>html5shiv.js"></script>
        <script src="<?php echo JS_PATH ?>respond.min.js"></script>
        <![endif]-->
        
        <link rel="stylesheet" href="<?php echo CSS_PATH ?>xjm.css" />
    </head>
 <script src="<?php echo JS_PATH ?>dialog.js"></script>
 <link rel="stylesheet" href="<?php echo CSS_PATH ?>dialog_simp.css" />
  <link rel="stylesheet" href="<?php echo CSS_PATH ?>dialog.css" />

<div class="main-content">
	<div class="page-header">
        <h1>
           	<i class="icon-hand-right icon-animated-hand-pointer blue"></i>技师管理
            <small>
                <i class="icon-double-angle-right"></i>
            	<?php echo $cash_info['nickname']?>技师评论详情
            </small>
        </h1>
    </div><!-- /.page-header -->
	
	<div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try {
                ace.settings.check('breadcrumbs', 'fixed')
            } catch (e) {
            }
        </script>
    </div>
    <div class="page-content">
        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                <tbody>
                                    <tr>
                                        <td>用户名</td>
                                         <td><?php echo $cash_info['nickname']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>联系电话</td>
                                        <td><?php echo $cash_info['mobile']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>反馈内容</td>
                                         <td><?php echo $cash_info['content']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>真实姓名</td>
                                        <td><?php echo $cash_info['truename']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>性别</td>
                                        <td><?php echo $sex_option[$cash_info['sex']]; ?></td>
                                    </tr>
                                    <tr>
                                        <td>身份证号</td>
                                        <td><?php echo $cash_info['card']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>反馈时间</td>
                                         <td><?php echo date("Y-m-d H:i:s",$cash_info['add_time']); ?></td>
                                    </tr>



                                </tbody>
                            </table>
                            </table>
                        </div><!-- /.table-responsive -->
                    </div><!-- /span -->
                </div><!-- /row -->
                <div class="row">
                	<div class="col-sm-6">
                    	<div class="dataTables_info" id="sample-table-2_info"></div>
                    </div>
                  
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.page-content -->
</div><!-- /.main-content -->
<script>
    function select_step(id, name,type) {
    	window.top.art.dialog({id:'edit'}).close();
    	window.top.art.dialog({title:name,id:'edit',iframe:'/Techer/techer_info',width:'550',height:'540'});
    }

    //审核通过
    function success(id)
    {
        var confirm =  layer.confirm('确认审核通过？', {icon: 3, title:'提示'}, function(index){
            if(index==1)
            {
                var loading = layer.load(1, {shade:0.7});
                $.get("/admin.php/order/cash_success",{id:id},function(data){
                    layer.close(loading);
                })
            }

        });
    }



    //驳回申请
    function fail(id)
    {
        var confirm =  layer.confirm('确认驳回审核？', {icon: 3, title:'提示'}, function(index){
            if(index==1)
            {
                var loading = layer.load(1, {shade:0.7});
                $.get("/admin.php/order/cash_fail",{id:id},function(data){
                    layer.close(loading);
                    layer.msg(data.msg);
                    window.location.reload();

                },"json")
            }

        });
    }



//     function select_step(id, name,type) {
//     	window.top.art.dialog({id:'edit'}).close();
//     	window.top.art.dialog({title:name,id:'edit',iframe:'/Techer/techer_info',width:'550',height:'540'});
//     }
    
</script>
    <style type="text/css">
        .btn0
        {
            border:0;
            background: #009f95;
            border-color: #009f95;
            width:80px;
            height:31px;
            line-height:31px;
            color:#fff;
        }
        .btn1
        {
            border:0;
            background: #ff00ff;
            border-color: #ff00ff;
            width:80px;
            height:31px;
            line-height:31px;
            color:#fff;
        }
    </style>
<?php include_once APPPATH . 'views/public/footer.php'; ?>