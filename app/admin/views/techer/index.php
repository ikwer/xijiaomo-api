<?php include_once APPPATH . 'views/public/header.php'; ?>
 <script src="<?php echo JS_PATH ?>dialog.js"></script>
  <link rel="stylesheet" href="<?php echo CSS_PATH ?>dialog.css" />

<div class="main-content">
	<div class="page-header">
        <h1>
           	<i class="icon-hand-right icon-animated-hand-pointer blue"></i>技师管理
            <small>
                <i class="icon-double-angle-right"></i>
            	技师列表
            </small>
        </h1>
    </div><!-- /.page-header -->
	
	<div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try {
                ace.settings.check('breadcrumbs', 'fixed')
            } catch (e) {
            }
        </script>

        <ul class="breadcrumb">
           <li> 
                <!-- <a href="javascript:;">
                    <a href="<?php echo site_url('Techer/techer_add')?>"><button class="btn btn-sm btn-success" >
                        <i class="icon-ok bigger-110"></i>
                        	添加技师
                    </button></a>
                </a>-->
            </li>
        </ul><!-- .breadcrumb -->
        <div class="nav-search" id="nav-search">
            <form class="form-search">
                <span class="input-icon">
                    <input type="text" placeholder="Search ..." name="kwd"  class="nav-search-input" value="" id="nav-search-input" autocomplete="off" />
                    <i class="icon-search nav-search-icon"></i>
                </span>
                <input type="submit" >
            </form>
        </div><!-- #nav-search -->
    </div>
    <div class="page-content">
        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="center">ID</th>
                                        <th>用户名</th>
                                        <th>手机号</th>
                                         <th>技师等级</th>
                                         <th>技师性别</th>
                                        <th>地区</th>
                                        <th>余额</th>
                                        <th>分享人数</th>
                                        <th>收入明细</th>
                                        <th>操作</th>
                                        
                                        <th class="hidden-480">状态</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php foreach ($page_data as $key => $value) { ?>
                                        <tr>
                                            <td class="center"><?php echo $value['tid'] ?></td>
                                             <td><?php echo $value['nickname'] ?></td>
                                            <td><?php echo $value['mobile'] ?></td>
                                            
                                            <td><?php echo $value['skill_level'] ?></td>
                                            <td><?php if(empty($value['sex'])){ echo ''; }else{ echo $sex[$value['sex']]; } ?></td>
                                            
                                              <td><?php echo $value['place'] ?></td>
                                            <td><?php echo $value['money'] ?></td>
                                            <td><a href="javascript:;" onclick="info(<?php echo $value['tid']?>)"><?php echo $value['tnum'] ?></a></td>
                                            <td><a href="javascript:;" onclick="income(<?php echo $value['tid']?>)">收入明细</a></td>
                                            <td><a href="javascript:select_step(<?php echo $value['tid']?>, '查看技师详细信息','invoice')">查看</a> <!--<a href="">修改</a>-->
                                             <a href="javascript:del('<?php echo $value['tid'];?>')">删除</a></td>
                                            <td class="hidden-480">
                                            	<?php if ($value['status'] == 0) { ?>
                                                   <!-- <span class="label label-sm label-success">等待审核</span>-->
                                                     <!--  <span class = "label label-sm label-warning"  onclick="passFn(1)">审核通过</span>
                                                      <span class = "label label-sm label-warning"  onclick="passFn(2)">审核不通过</span>-->
                                                     
                                                      <span class = "label label-sm label-warning"  onclick="passFn(<?php echo $value['tid']?>,1)">审核通过</span>
                                                     <span class = "label label-sm label-warning"  onclick="passFn(<?php echo $value['tid']?>,2)">审核失败</span>
                                                      
                                                      
                                                <?php } elseif(($value['status'] == 1)) { ?>
                                                    <span class = "label label-sm label-success">已通过审核</span>
                                                   <?php
                                                    } else{?>
                                                    <span class = "label label-sm label-warning">未通过审核</span>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php }?>
                                </tbody>
                            </table>
                        </div><!-- /.table-responsive -->
                    </div><!-- /span -->
                </div><!-- /row -->
                <div class="row">
                	<div class="col-sm-6">
                    	<div class="dataTables_info" id="sample-table-2_info"></div>
                    </div>
                    <div class="col-sm-6">
                        <div class="dataTables_paginate paging_bootstrap">
                            <ul class="pagination">
                            	<?php echo $page_list ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.page-content -->
</div><!-- /.main-content -->
<script>
    function select_step(id, name,type) {
        //window.location.href="/Techer/techer_info/uid/"+id;

        var index = layer.open({
            type: 2,
            content: '/admin.php/Techer/techer_info/uid/'+id,
            area: ['600px', '400px'],
            maxmin: true
        });
        layer.full(index);

    }

    function passFn(id, type) {
		layer.confirm('此操作不可逆，确定要执行此操作吗？',{},function(index){
			 layer.close(index);
			  $.post('/admin.php/Techer/passfn?tid='+id+'&type='+type,{id:id,type:type},function(data){
					if(data.status=='success'){
						window.location.href=data.url;
					}
			  },'json');
        });
    }


    function info(id)
    {
        var indexs = layer.open({
            type: 2,
            content: "/admin.php/Techer/techer_list?tid="+id,
            area: ['600px', '400px'],
            maxmin: true
        });
    }


    function income(id)
    {
        var indexss = layer.open({
            type: 2,
            content: "/admin.php/Techer/techer_list?type=income&tid="+id,
            area: ['600px', '400px'],
            maxmin: true
        });
    }


    function del(id) {

        layer.confirm('是否删除这个技师', {icon: 3, title:'提示'}, function(index){
            //do something
            if(index==1)
            {
                        //var loading=layer.load(1, {shade:0.7});
                        $.post("/admin.php/Techer/techer_del",{id:id},function(data){
                            //layer.close(loading);
			                if(data.status=='success'){
				                window.location.href=data.url;
				            }
                        },'json');
            }
            else
            {
                layer.close(index);
            }
            layer.close(index);
        });



    }
</script>

<?php include_once APPPATH . 'views/public/footer.php'; ?>