<?php include_once APPPATH . 'views/public/header.php'; ?>    
                  <script src="<?php echo JS_PATH ?>jquery.validate.js"></script>
        <script src="<?php echo JS_PATH ?>jQuery.Form.js"></script>   
<div class="main-content">
	<div class="page-header">
        <h1>
           	<i class="icon-hand-right icon-animated-hand-pointer blue"></i>技师管理
            <small>
                <i class="icon-double-angle-right"></i>
            	添加技师
            </small>
        </h1>
    </div><!-- /.page-header -->
	
	<div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
            }
        </script>
    </div>
    <div class="page-content">
        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                              <div class="page-content">
        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->

                <!--<form class="form-horizontal" name="form" method="post" action="<?php echo site_url('Admin/edit_user'); ?>" role="form">-->
               <!-- <?php echo form_open('Techer/techer_add', 'class="form-horizontal" id="myform"'); ?>-->
               <form class="form-horizontal" name="form" method="post" action="<?php echo site_url('techer/techer_add'); ?>" role="form" id="techerFormAdd">
                <input type="hidden" name="formhash" value="1"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">用户名：</label>

                    <div class="col-sm-9">
                        <input type="text" name="loginname" id="loginname" placeholder="" value=""  class="col-xs-10 col-sm-5" />
                    </div>
                </div>
                <div class="space-4"></div>
                 <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">技师等级：</label>

                    <div class="col-sm-9">
                        <input type="text" name="skill_level" id="skill_level" placeholder="" value=""  class="col-xs-10 col-sm-5" />
                    </div>
                </div>
                <div class="space-4"></div>
                 <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> 联系方式： </label>

                    <div class="col-sm-9">
                        <input type="text" name="mobile" id="mobile" placeholder="" value=""  class="col-xs-10 col-sm-5" />
                    </div>
                </div>
                <div class="space-4"></div>
                 <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">价格：</label>

                    <div class="col-sm-9">
                        <input type="text" name="money" id="money" placeholder="" value=""  class="col-xs-10 col-sm-5" />
                    </div>
                </div>
                <div class="space-4"></div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">身份证号码：</label>

                    <div class="col-sm-9">
                        <input type="text" name="card" id="card" placeholder="" value=""  class="col-xs-10 col-sm-5" />
                    </div>
                </div>
                <div class="space-4"></div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">性别：</label>
                    <input type="radio" name="sex" value="1" checked="checked"/>女
                    <input type="radio" name="sex" value="2"/>男
                </div>
                <div class="space-4"></div>
                
                
                <div class="col-md-offset-3 col-md-9">
                        <!--                            <button class="btn btn-info" type="submit">
                                                        <i class="icon-ok bigger-110"></i>
                                                        Submit
                                                    </button>-->
                        <input class="btn btn-info" type="submit" name="ok" value="提交">
                        &nbsp; &nbsp; &nbsp;
                        <button id="back_url" class="btn" type="reset">
                            <i class="icon-undo bigger-110"></i>
                            返回
                        </button>
                    </div>
                </form>

            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.page-content -->
                        </div><!-- /.table-responsive -->
                    </div><!-- /span -->
                </div><!-- /row -->
                <div class="row">
                	<div class="col-sm-6">
                    	<div class="dataTables_info" id="sample-table-2_info"></div>
                    </div>
                  
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.page-content -->
</div><!-- /.main-content -->
  
<script>
$(function(){
        




	$("#techerFormAdd").validate({
		  focusInvalid : true, 
		  rules: {
			 loginname:{
				required:true
				 },
	 		skill_level:{
				required:true
			 } 
		},
		messages:{
			loginname:{
				required:'用户名不能为空'
				},
		skill_level:{
			required:'技师等级不能为空'
		 }
		},
		 errorElement : "span",
	        errorClass : "error_info",
	        highlight : function(element, errorClass,
	            validClass) {
	          $(element).closest('.form-control').addClass(
	              'highlight_red');
	        },
	        success : function(element) {
	          $(element).siblings('.form-control')
	              .removeClass('highlight_red');
	          $(element).siblings('.form-control').addClass(
	              'highlight_green');
	          $(element).remove();
	        },
	        submitHandler : function(form) {  //验证通过后的执行方法
	            //当前的form通过ajax方式提交（用到jQuery.Form文件）
	            $("#techerFormAdd").ajaxSubmit({
	                dataType:"json",
	                success:function( jsondata ){
						if( jsondata.status == 'success' ){
		                	    //$(".tips").html(jsondata.msg);
			                	//setTimeout(function(){
// 								alert('信息保存成功！');
			                		window.location.href=jsondata.url;
			                	//},1000);
		                   }else{
		                       //alert("fail");
		                	   $(".tips").html("<lable style='color:#F42706'>"+jsondata.msg+"</lable>")
		                   }
	                  }
	                }); 
	        }
	});




	
	

});      

</script>

<?php include_once APPPATH . 'views/public/footer.php'; ?>