<?php include_once APPPATH . 'views/public/header.php'; ?>
 <script src="<?php echo JS_PATH ?>dialog.js"></script>
  <link rel="stylesheet" href="<?php echo CSS_PATH ?>dialog.css" />

<div class="main-content">
	<div class="page-header">
        <h1>
           	<i class="icon-hand-right icon-animated-hand-pointer blue"></i>技师管理
            <small>
                <i class="icon-double-angle-right"></i>
            	技师收入明细
            </small>
        </h1>
    </div><!-- /.page-header -->
	
	<div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try {
                ace.settings.check('breadcrumbs', 'fixed')
            } catch (e) {
            }
        </script>

        <ul class="breadcrumb">
           <li> 
                <!-- <a href="javascript:;">
                    <a href="<?php echo site_url('Techer/techer_add')?>"><button class="btn btn-sm btn-success" >
                        <i class="icon-ok bigger-110"></i>
                        	添加技师
                    </button></a>
                </a>-->
            </li>
        </ul><!-- .breadcrumb -->
        <div class="nav-search" id="nav-search" style="display: none;">
            <form class="form-search">
                <span class="input-icon">
                    <input type="text" placeholder="Search ..." name="kwd"  class="nav-search-input" value="" id="nav-search-input" autocomplete="off" />
                    <i class="icon-search nav-search-icon"></i>
                </span>
                <input type="submit" >
            </form>
        </div><!-- #nav-search -->
    </div>
    <div class="page-content">
        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="center">ID</th>
                                        <th>名称</th>
                                        <th>金额</th>
                                        <th>类型</th>
                                        <th>添加时间</th>
                                        <th>订单号</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php foreach ($page_data as $key => $value) { ?>
                                        <tr>
                                            <td class="center"><?php echo $value['id'] ?></td>
                                             <td><?php echo $value['goods_name'] ?></td>
                                            <td><?php echo $value['change_money'] ?></td>
                                            <td><?php if($value['from']==1){ echo '收入'; }elseif($value['from']==2){ echo '提现'; } ?></td>
                                            <td><?php echo date('Y-m-d H:i:s',$value['add_time']) ?></td>
                                            <td><?php echo $value['order_id'] ?></td>
                                        <?php } ?>

                                </tbody>
                            </table>
                        </div><!-- /.table-responsive -->
                    </div><!-- /span -->
                </div><!-- /row -->
                <div class="row">
                	<div class="col-sm-6">
                    	<div class="dataTables_info" id="sample-table-2_info"></div>
                    </div>
                    <div class="col-sm-6">
                        <div class="dataTables_paginate paging_bootstrap">
                            <ul class="pagination">
                            	<?php echo $page_list ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.page-content -->
</div><!-- /.main-content -->
<script>
    $(".menu-toggler").hide();
    $(".navbar-default").hide();
    function select_step(id, name,type) {
        //window.location.href="/Techer/techer_info/uid/"+id;

        var index = layer.open({
            type: 2,
            content: '/admin.php/Techer/techer_info/uid/'+id,
            area: ['600px', '400px'],
            maxmin: true
        });


    }

    function passFn(id, type) {
		layer.confirm('此操作不可逆，确定要执行此操作吗？',{},function(index){
			 layer.close(index);
			  $.post('/admin.php/Techer/passfn?tid='+id+'&type='+type,{id:id,type:type},function(data){
					if(data.status=='success'){
						window.location.href=data.url;
					}
			  },'json');
        });
    }


    function info(id)
    {
        $.post('/admin.php/Techer/techer_list',{tid:id},function(data){
            console.log(data);
        },'json');
    }


    function del(id) {

        layer.confirm('是否删除这个技师', {icon: 3, title:'提示'}, function(index){
            //do something
            if(index==1)
            {
                        //var loading=layer.load(1, {shade:0.7});
                        $.post("/admin.php/Techer/techer_del",{id:id},function(data){
                            //layer.close(loading);
			                if(data.status=='success'){
				                window.location.href=data.url;
				            }
                        },'json');
            }
            else
            {
                layer.close(index);
            }
            layer.close(index);
        });



    }
</script>

<?php include_once APPPATH . 'views/public/footer.php'; ?>