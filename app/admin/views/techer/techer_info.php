<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>控制台 - 喜脚么后台管理系统</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!-- basic styles -->
        <link href="<?php echo CSS_PATH ?>bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo CSS_PATH ?>font-awesome.min.css" />

        <!--[if IE 7]>
          <link rel="stylesheet" href="<?php echo CSS_PATH ?>font-awesome-ie7.min.css" />
        <![endif]-->

        <!-- page specific plugin styles -->

        <!-- fonts -->



        <!-- ace styles -->

        <link rel="stylesheet" href="<?php echo CSS_PATH ?>ace.min.css" />
        <link rel="stylesheet" href="<?php echo CSS_PATH ?>ace-rtl.min.css" />
        <link rel="stylesheet" href="<?php echo CSS_PATH ?>ace-skins.min.css" />

        <!--[if lte IE 8]>
          <link rel="stylesheet" href="<?php echo CSS_PATH ?>ace-ie.min.css" />
        <![endif]-->

        <!-- inline styles related to this page -->

        <!-- ace settings handler -->
        <script src="/data/assets/js/jquery-2.0.3.min.js"></script>

        <!-- layer -->
        <link rel="stylesheet" href="/data/assets/js/layer/theme/default/layer.css">
        <script src="/data/assets/js/layer/layer.js"></script>
        <!-- layer -->

        <script src="<?php echo JS_PATH ?>ace-extra.min.js"></script>

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

        <!--[if lt IE 9]>
        <script src="<?php echo JS_PATH ?>html5shiv.js"></script>
        <script src="<?php echo JS_PATH ?>respond.min.js"></script>
        <![endif]-->
        
        <link rel="stylesheet" href="<?php echo CSS_PATH ?>xjm.css" />
    </head>
 <script src="<?php echo JS_PATH ?>dialog.js"></script>
  <link rel="stylesheet" href="<?php echo CSS_PATH ?>dialog.css" />

<div class="main-content">
    <div class="page-header">
        <h1>
            <i class="icon-hand-right icon-animated-hand-pointer blue"></i>技师管理
            <small>
                <i class="icon-double-angle-right"></i>
                <?php echo $techerInfo['nickname']?>个人详情
            </small>
        </h1>
    </div><!-- /.page-header -->
    
    <div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try {
                ace.settings.check('breadcrumbs', 'fixed')
            } catch (e) {
            }
        </script>
    </div>
    <div class="page-content">
        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                <tbody>
                                    <tr>
                                        <td>昵称</td>
                                         <td><?php echo $techerInfo['nickname']?></td>
                                    </tr>
                                    <tr>
                                        <td>真实姓名</td>
                                        <td><?php echo $techerInfo['truename']?></td>
                                    </tr>
                                    <tr>
                                        <td>联系方式</td>
                                         <td><?php echo $techerInfo['mobile']?></td>
                                    </tr>
                                    <tr>
                                        <td>技师等级</td>
                                         <td><?php echo $techerInfo['skill_level']?></td>
                                    </tr>
                                    <tr>
                                        <td>地区</td>
                                         <td><?php echo $techerInfo['place']?></td>
                                    </tr>
                                    <tr>
                                        <td>账户余额</td>
                                         <td><?php echo $techerInfo['money']?></td>
                                    </tr>
                                    <tr>
                                        <td>生日</td>
                                        <td><?php echo date('Y-m-d',$techerInfo['birthday']); ?></td>
                                    </tr>
                                     <tr>
                                        <td>身份证号</td>
                                         <td><?php echo $techerInfo['card']?>
                                             <?php if($techerInfo['card_success']!=1 && $techerInfo['status']==0): ?>
                                             <input type="button" id="check" onclick="check(<?php echo $techerInfo['id']; ?>)" value="检测">
                                             <?php endif; ?>
                                         </td>
                                    </tr>
                                    <tr>
                                        <td>身份检测状态</td>
                                        <td><font color="red">
                                                <?php if(empty($techerInfo['card_success'])){ echo '';  }else{ echo $card_success[$techerInfo['card_success']]; } ?>
                                                </font></td>
                                    </tr>

                                    <tr>
                                        <td>技师性别</td>
                                        <td><?php if(empty($techerInfo['sex'])){ echo '';  }else{ echo $sex[$techerInfo['sex']]; } ?></td>
                                    </tr>

                                    <tr>
                                        <td>技师审核状态</td>
                                        <td><font color="red">
                                                <?php if(empty($techerInfo['status'])){ echo '';  }else{ echo $techer_status[$techerInfo['status']]; } ?>
                                                </font></td>
                                    </tr>

                                    <tr>
                                        <td>技师头像</td>
                                        <td>
                                            <?php if(!empty($techerInfo['headurl'])){ ?>
                                            <img src="<?php echo $techerInfo['headurl']; ?>" >
                                                <strong><?php echo $success_status[$techerInfo['headurl_status']]; ?></strong>
                                            <?php }else{
                                                echo "暂无头像";
                                            } ?>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td>头像审核</td>
                                        <td>
                                            <input type="button" id="check" value="ok" onclick="success_kpi('head',<?php echo $techerInfo['tid']; ?>)">
                                            <input type="button" id="error"  value="no" onclick="error_kpi('head',<?php echo $techerInfo['tid']; ?>)">
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>技师相册</td>
                                        <td>
                                            <?php if(!empty($techerInfo['pic_arr'])){ ?>
                                                <?php foreach($techerInfo['pic_arr'] as $k=>$v): ?>
                                                    <img src="<?php echo $v['pic_url']; ?>" style="width:70%;">
                                                    <strong><?php echo $success_status[$v['pic_status']]; ?></strong>
                                                    <input type="button" id="check"  value="ok" onclick="success_kpi('pic_arr',<?php echo $v['id']; ?>)">
                                                    <input type="button" id="error"  value="no" onclick="error_kpi('pic_arr',<?php echo $v['id']; ?>)">
                                                <?php endforeach; ?>
                                            <?php }else{ echo "暂未上传图片"; } ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>技师视频</td>
                                        <td>
                                            <?php if(!empty($techerInfo['video_arr'])){ ?>
                                                <video src="<?php echo $techerInfo['video_arr']['video_url']; ?>" controls="controls"></video>
                                                <strong><?php echo $success_status[$techerInfo['video_arr']['video_status']]; ?></strong>
                                                <input type="button" id="check"  value="ok" onclick="success_kpi('video_arr',<?php echo $techerInfo['video_arr']['id']; ?>)">
                                                <input type="button" id="error"  value="no" onclick="error_kpi('video_arr',<?php echo $techerInfo['video_arr']['id']; ?>)">
                                            <?php }else{ echo "暂未上传视频"; } ?>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td>身份证正面照</td>
                                        <td>
                                            <?php if(!empty($techerInfo['idcard_zheng'])){ ?>
                                            <img src="<?php echo $techerInfo['idcard_zheng']; ?>" style="width:50%;">
                                            <?php }else{ echo '暂无'; } ?>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td>身份证反面照</td>
                                        <td>
                                            <?php if(!empty($techerInfo['idcard_fan'])){ ?>
                                            <img src="<?php echo $techerInfo['idcard_fan']; ?>" style="width:50%;">
                                            <?php }else{ echo '暂无'; } ?>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                            </table>
                        </div><!-- /.table-responsive -->
                    </div><!-- /span -->
                </div><!-- /row -->
                <div class="row">
                    <div class="col-sm-6">
                        <div class="dataTables_info" id="sample-table-2_info"></div>
                    </div>
                  
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.page-content -->
</div><!-- /.main-content -->
<script>



        function check(id)
        {
            var loading = layer.load(1, {shade:0.7});
            $.get("/admin.php/techer/check_idcard",{id:id,c_from:'id_card'},function(data){
                layer.close(loading);
               if(data.code!=-1)
               {
                   layer.msg("身份证信息检测属实");
                   window.location.reload();
               }
               else
               {
                   layer.msg("身份证信息异常");
                   return false;
               }
            },"json");
        }


        function success_kpi(c_from,id)
        {

            var loading = layer.load(1, {shade:0.7});
            $.get("/admin.php/techer/check_idcard",{id:id,c_from:c_from,opt:1},function(data){
                layer.close(loading);
                if(data.code==0)
                {
                    layer.msg(data.msg);
                    window.location.reload();
                }
                else
                {
                    layer.msg(data.msg);
                    return false;
                }
            },"json");
        }



        function error_kpi(c_from,id)
        {
            var loading = layer.load(1, {shade:0.7});
            $.get("/admin.php/techer/check_idcard",{id:id,c_from:c_from,opt:2},function(data){
                layer.close(loading);
                if(data.code==0)
                {
                    layer.msg(data.msg);
                    window.location.reload();
                }
                else
                {
                    layer.msg(data.msg);
                    return false;
                }
            },"json");
        }

</script>
    <style type="text/css">
        #check{
            border:0;
            background: #0b6cbc;
            color:#fff;
            font-size: 14px;
            width:50px;
            height:25px;
            margin-left:5px;
        }
        #error{
            border:0;
            background: #bc0a10;
            color:#fff;
            font-size: 14px;
            width:50px;
            height:25px;
            margin-left:5px;
        }
    </style>
<?php include_once APPPATH . 'views/public/footer.php'; ?>