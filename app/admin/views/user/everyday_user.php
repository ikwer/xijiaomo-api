<?php include_once APPPATH . 'views/public/header.php'; ?>

    <script type="text/javascript" src="<?php echo JS_PATH ?>echarts.js"></script>
    <script type="text/javascript" src="<?php echo JS_PATH ?>laydate/laydate.js"></script>
    <script>
        //执行一个laydate实例
        laydate.render({
            elem: '#test1', //指定元素
            max: '<?php echo date("Y-m-d"); ?>'
        });
        //执行一个laydate实例
        laydate.render({
            elem: '#test2', //指定元素
            max: '<?php echo date("Y-m-d"); ?>'
        });

        //时间搜索按钮点击
        function gets()
        {
            var sdate = $("#test1").val();
            var edate = $("#test2").val();

            if(sdate=='' || edate=='')
            {
                layer.load('请选择日期');
            }
            else
            {

                if(!ckdate())
                {
                    layer.msg('开始日期不能大于结束日期');
                }
                else
                {
                    layer.load(1, {shade:0.7});
                    window.location.href="/admin.php/User/everyday_user?sdate="+sdate+"&edate="+edate;
                }

            }
        }

        //检测两个时间大小
        function ckdate() {
            var endtime = $('#test2').val();
            var starttime = $('#test1').val();
            var start = new Date(starttime.replace("-", "/").replace("-", "/"));
            var end = new Date(endtime.replace("-", "/").replace("-", "/"));
            if (end < start) {

                return false;
            }
            else {
                return true;
            }
        }
    </script>
    <div class="main-content">
        <div class="page-header">
            <h1>
                <i class="icon-hand-right icon-animated-hand-pointer blue"></i>运营行为
                <small>
                    <i class="icon-double-angle-right"></i>
                    日新增用户+技师
                </small>
            </h1>
        </div><!-- /.page-header -->
    <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
        <div style="margin-left:69px;">
        <form action="" method="get">
        开始日期：<input type="text" name="sdate" readonly="readonly" id="test1" value="<?php if(empty($this->input->get('sdate'))){ echo date("Y-m-d",strtotime("-7 day")); }else{ echo $this->input->get('sdate'); } ?>">
        结束日期：<input type="text" name="edate" readonly="readonly" id="test2" value="<?php if(empty($this->input->get('edate'))){ echo date('Y-m-d'); }else{ echo $this->input->get('edate'); } ?>">
        <input type="button" onclick="gets()" value="确定" style="border: 0;width: 75px;height: 29px;background: #2c8aff;color:#fff;font-size:14px;line-height: 14px;">
        </form>
        </div>
    <div class="page-content">
        <div id="main" style="width: 100%;height:400px;"></div>
    </div>

        <div class="page-content">
            <div id="under" style="width: 100%;height:400px;"></div>

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="center">时间</th>
                                        <th>新增用户</th>
                                        <th>新增技师</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                        <?php foreach ($table_data as $k=>$v): ?>
                                        <tr>
                                            <td class="center"><?php echo $k; ?></td>
                                            <td><?php echo $v['usernum']; ?></td>
                                            <td><?php echo $v['techernum']; ?></td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div><!-- /.table-responsive -->
                        </div><!-- /span -->
                    </div><!-- /row -->
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="dataTables_info" id="sample-table-2_info"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_bootstrap">
                                <ul class="pagination">

                                </ul>
                            </div>
                        </div>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
        </div>
    </div>





    <script type="text/javascript">
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('main'));

        var underChart = echarts.init(document.getElementById('under'));

        // 指定图表的配置项和数据
        data =  <?php echo $charts_data['new_users']; ?>;

        under_data = <?php echo $charts_data['new_techs']; ?>

        var dateList = data.map(function (item) {
            return item[0];
        });
        var valueList = data.map(function (item) {
            return item[1];
        });

        var date_List = under_data.map(function (item) {
            return item[0];
        });
        var value_List = under_data.map(function (item) {
            return item[1];
        });


        option = {

            // Make gradient line here
            visualMap: [{
                show: false,
                type: 'piecewise',
                seriesIndex: 0,
                min: 0,
                max: 400
            }],


            title: [{
                left: 'center',
                text: '日新增用户'
            }],
            tooltip: {
                trigger: 'axis'
            },
            xAxis: [{
                data: dateList
            }],
            yAxis: [{
                splitLine: {show: false}
            }],
            grid: [{
                bottom: '60%'
            }, {
                top: '60%'
            }],
            series: [{
                type: 'line',
                showSymbol: false,
                data: valueList
            }]
        };


        option2 = {

            // Make gradient line here
            visualMap: [{
                show: false,
                type: 'piecewise',
                seriesIndex: 0,
                min: 0,
                max: 400
            }],


            title: [{
                left: 'center',
                text: '日新增技师'
            }],
            tooltip: {
                trigger: 'axis'
            },
            xAxis: [{
                data: date_List
            }],
            yAxis: [{
                splitLine: {show: false}
            }],
            grid: [{
                bottom: '60%'
            }, {
                top: '60%'
            }],
            series: [{
                type: 'line',
                showSymbol: false,
                data: value_List
            }]
        };

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);

        underChart.setOption(option2);
    </script>

<?php include_once APPPATH . 'views/public/footer.php'; ?>