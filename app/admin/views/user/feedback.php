<?php include_once APPPATH . 'views/public/header.php'; ?>
    <div class="main-content">
        <div class="page-header">
            <h1>
                <i class="icon-hand-right icon-animated-hand-pointer blue"></i>用户管理
                <small>
                    <i class="icon-double-angle-right"></i>
                    用户反馈
                </small>
            </h1>
        </div><!-- /.page-header -->

        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

            </ul><!-- .breadcrumb -->
            <div class="nav-search" id="nav-search">
                <form class="form-search">
                <span class="input-icon">
                    <input type="text" placeholder="Search ..." name="kwd"  class="nav-search-input" value="<?php if(!empty($_GET['kwd'])){ echo $_GET['kwd']; } ?>" id="nav-search-input" autocomplete="off" />
                    <i class="icon-search nav-search-icon"></i>
                </span>
                    <input type="submit" >
                </form>
            </div><!-- #nav-search -->
        </div>
        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="center">ID</th>
                                        <th>用户名</th>
                                        <th>添加时间</th>
                                        <th>反馈内容</th>
                                        <th class="hidden-480">联系电话</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($page_data as $key => $value) { ?>
                                        <tr>
                                            <td class="center"><?php echo $value['id']; ?></td>
                                            <td><?php echo $value['nickname']; ?></td>
                                            <td><?php echo date("Y-m-d H:i:s",$value['add_time']); ?></td>
                                            <td title="<?php echo $value['content']; ?>"><?php echo mb_substr($value['content'],0,15,"utf-8"); ?>......</td>
                                            <td class="hidden-480">
                                               <?php echo $value['mobile']; ?>
                                            </td>
                                            <td>
                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                    <a class="green" href='javascript:;' title="查看详情" onclick="feedback_info(<?php echo $value['id']; ?>)">
                                                        <i class="icon-pencil bigger-130"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php }?>
                                    </tbody>
                                </table>
                            </div><!-- /.table-responsive -->
                        </div><!-- /span -->
                    </div><!-- /row -->
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="dataTables_info" id="sample-table-2_info"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_bootstrap">
                                <ul class="pagination">
                                    <?php echo $page_list ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div><!-- /.main-content -->
    <script type="text/javascript">
        function feedback_info(id)
        {
            var index = layer.open({
                type: 2,
                content: '/admin.php/user/feedback_info?id='+id,
                area: ['750px', '500px'],
                maxmin: true
            });
        }
    </script>
<?php include_once APPPATH . 'views/public/footer.php'; ?>