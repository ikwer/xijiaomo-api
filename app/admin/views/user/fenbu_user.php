<?php include_once APPPATH . 'views/public/header.php'; ?>

    <script type="text/javascript" src="<?php echo JS_PATH ?>echarts.js"></script>
    <script type="text/javascript" src="<?php echo JS_PATH ?>laydate/laydate.js"></script>

    <div class="main-content">
        <div class="page-header">
            <h1>
                <i class="icon-hand-right icon-animated-hand-pointer blue"></i>运营行为
                <small>
                    <i class="icon-double-angle-right"></i>
                    用户比例
                </small>
            </h1>
        </div><!-- /.page-header -->
        <!-- 为ECharts准备一个具备大小（宽高）的Dom -->


        <div style="width: 100%;">
        <div id="u_main" style="width: 40%;height:400px;"></div>
        <div id="main" style="width: 40%;height:400px;float: left;display: block;margin-left: 50%;margin-top:-400px;"></div>
        </div>

        <div class="page-content">


            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="center">来源</th>
                                        <th>用户</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($charts_data['u_table_list'] as $k=>$v): ?>
                                        <tr>
                                            <td><?php echo $v['name']; ?></td>
                                            <td><?php echo $v['value']; ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>


                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="center">来源</th>
                                        <th>技师</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($charts_data['table_list'] as $k=>$v): ?>
                                        <tr>
                                            <td><?php echo $v['name']; ?></td>
                                            <td><?php echo $v['value']; ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>


                            </div><!-- /.table-responsive -->
                        </div><!-- /span -->
                    </div><!-- /row -->
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="dataTables_info" id="sample-table-2_info"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_bootstrap">
                                <ul class="pagination">

                                </ul>
                            </div>
                        </div>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
    </div>




    <script type="text/javascript">
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('main'));




        option = {
            title : {
                text: '技师注册来源',
                subtext: '',
                x:'center'
            },
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                left: 'left',
                data: <?php echo json_encode($charts_data['source_result']); ?>
            },
            series : [
                {
                    name: '注册来源',
                    type: 'pie',
                    radius : '60%',
                    center: ['50%', '60%'],
                    data:<?php echo json_encode($charts_data['result']); ?>,
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };





        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);


    </script>

    <script type="text/javascript">
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('u_main'));




        option = {
            title : {
                text: '用户注册来源',
                subtext: '',
                x:'center'
            },
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                left: 'left',
                data: <?php echo json_encode($charts_data['source_result']); ?>
            },
            series : [
                {
                    name: '注册来源',
                    type: 'pie',
                    radius : '60%',
                    center: ['50%', '60%'],
                    data:<?php echo json_encode($charts_data['u_result']); ?>,
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };





        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);


    </script>

<?php include_once APPPATH . 'views/public/footer.php'; ?>