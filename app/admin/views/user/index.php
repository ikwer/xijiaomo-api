<?php include_once APPPATH . 'views/public/header.php'; ?>
    <div class="main-content">
        <div class="page-header">
            <h1>
                <i class="icon-hand-right icon-animated-hand-pointer blue"></i>用户管理
                <small>
                    <i class="icon-double-angle-right"></i>
                    用户列表
                </small>
            </h1>
        </div><!-- /.page-header -->

        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">
                <li>
                    <a href="javascript:;">
                        <button class="btn btn-sm btn-success">
                            <i class="icon-ok bigger-110"></i>
                            添加用户
                        </button>
                    </a>
                </li>
            </ul><!-- .breadcrumb -->
            <div class="nav-search" id="nav-search">
                <form class="form-search">
                <span class="input-icon">
                    <input type="text" placeholder="Search ..." name="kwd"  class="nav-search-input" value="" id="nav-search-input" autocomplete="off" />
                    <i class="icon-search nav-search-icon"></i>
                </span>
                    <input type="submit" >
                </form>
            </div><!-- #nav-search -->
        </div>
        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="center">ID</th>
                                        <th>手机号</th>
                                        <th>余额</th>
                                        <th>积分</th>
                                        <th>分享人数</th>
                                        <th class="hidden-480">状态</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($page_data as $key => $value) { ?>
                                        <tr>
                                            <td class="center"><?php echo $value['uid'] ?></td>
                                            <td><?php echo $value['mobile'] ?></td>
                                            <td><?php echo $value['money'] ?></td>
                                            <td><?php echo $value['point'] ?></td>
                                            <td><a href="javascript:;" onclick="shareinfo(<?php echo $value['uid'] ?>)"><?php echo $value['s_num'] ?></a></td>
                                            <td class="hidden-480">
                                                <?php if ($value['status'] == 0) { ?>
                                                    <span class="label label-sm label-success">正常</span>
                                                <?php } else { ?>
                                                    <span class = "label label-sm label-warning">封号</span>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                    <a class="green" href='javascript:;' title="修改">
                                                        <i class="icon-pencil bigger-130"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php }?>
                                    </tbody>
                                </table>
                            </div><!-- /.table-responsive -->
                        </div><!-- /span -->
                    </div><!-- /row -->
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="dataTables_info" id="sample-table-2_info"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_bootstrap">
                                <ul class="pagination">
                                    <?php echo $page_list ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div><!-- /.main-content -->
<script type="text/javascript">
    function shareinfo(id){
        var index = layer.open({
            type: 2,
            content: '/admin.php/User/index?uid='+id,
            area: ['600px', '400px'],
            maxmin: true
        });
    }
</script>
<?php include_once APPPATH . 'views/public/footer.php'; ?>

