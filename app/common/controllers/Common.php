<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {
    const TABLE_USER_ACCOUNT = 'xj_user';

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    
    public function __construct()
    {
        parent::__construct();
    }
    
	public function index()
	{
		
	}
	
	/**
	 * 发送短信公共函数
	 * @param mobile 手机号
	 * @param type 类型 1：注册 2：找回密码 
	 * @param tpl_id 短信模板id
	 * @param content 内容
	 */
	public function sendsms($loginname, $type, $tpl_id, $content){
	    switch ($type){
	        case 1:
	            //验证当前手机号是否被注册
	            $r = $this->checkloginname($loginname);
	            if($r !== false){
	                ajax_return(1, '当前手机已存在');
	            }
	            break;
	        case 2:
	            //验证当前手机号是否存在
	            $r = $this->checkloginname($loginname);
	            if($r === false){
	                ajax_return(1, '当前手机不存在');
	            }
	            break;
	    }
	    
	    $this->load->library('juhesms');
	    $code   = rand(100000, 999999);
	    $r = $this->juhesms->send($loginname, $tpl_id, $code, $content);
	    if($r !== false){
	        ajax_return(0, '发送成功');
	    }else{
	        ajax_return(1, '发送失败');
	    }
	}
	
	/*
	 * 检测当前用户是否存在
	 */
	public function checkloginname($loginname){
	    $query = 'select uid from ' . self::TABLE_USER_ACCOUNT . ' where loginname="' . $loginname.'"';
	    $user_info = $this->db->query($query)->row_array();
	    if($user_info){
	        return true;
	    }else{
	        return false;
	    }
	}

	//api安全性—Token签名sign的设计与实现
	public function signTokent($timestamp, $token, $sign){
	    $this->load->library('redis');
	    
        //1.判断是否包含timestamp，token，sign参数，如果不含有返回错误码
        if(empty($timestamp) || empty($token) || empty($sign)){
            ajax_return(1, '参数错误');
        }
        //2.判断服务器接到请求的时间和参数中的时间戳是否相差很长一段时间（时间自定义如半个小时），如果超过则说明该 url已经过期（如果url被盗，他改变了时间戳，但是会导致sign签名不相等）
        $time = time();
        if($timestamp-$timestamp>1800){
            ajax_return(1, 'url已过期');
        }
        //3.判断token是否有效，根据请求过来的token，查询redis缓存中的uid，如果获取不到这说明该token已过期。
        if(empty($this->redis->get($token))){
            ajax_return(1, 'token已过期');
        }
        //4.根据用户请求的url参数，服务器端按照同样的规则生成sign签名，对比签名看是否相等，相等则放行
        $server_sign = '';
        if($sign == $server_sign){
            ajax_return(1, '签名不正确');
        }
        //对于敏感的api接口，需使用https协议
         
        //以上url拦截除了只需对获取身份认证的url放行（如登陆url），剩余所有的url都需拦截
    }
}
