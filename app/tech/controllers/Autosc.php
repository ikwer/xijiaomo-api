<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Autosc extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('auto_model');
    }


    //两天自动退款
	public function auto_refund()
    {
        //两天前的时间戳
        $date=time()-172800;
        $this->auto_model->auto_refund($date);
    }

    //七天自动好评
    public function auto_goodPraise()
    {
        //七天前的时间戳
        $date=time()-604800;
        $this->auto_model->auto_goodPraise($date);
    }
    
    //十分钟下线状态
    public function auto_offline()
    {
        $this->auto_model->auto_offline();
    }

    //订单自动失效
    public function auto_quxiao()
    {
        $this->auto_model->auto_quxiao();
    }

    //生成二维码图片
    public function qr_code()
    {
        include "data/techer/phpqrcode/phpqrcode.php";
        $errorLevel = "L";
        $size = "6";
        $url=$_GET['url'];
        $url.="\r\n";
        QRcode::png($url, false, $errorLevel, $size);
    }

    //技师端15天自动奖励
    public function auto_reward()
    {
        $this->auto_model->auto_reward();
    }

    //女技师八点自动下线
    public function auto_woman_offline()
    {
        $woman=$this->db->select('tid')->from('xjm_techer_checkinfo')->where(array('sex'=>2))->get()->result_array();
        if(!empty($woman)){
            foreach($woman as $k=>$v)
            {
                $techer['is_online']=1;
                echo $this->db->where(array('tid'=>$v['tid']))->update('xjm_techer',$techer);
            }
        }
    }

}
