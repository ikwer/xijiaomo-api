<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Help extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function wap()
    {
        $source=$this->input->get('source');
        (empty($source)) ? $source='user' : $source=$source;

        if($source=='user')
        {
            $query=$this->db->select('title,content')->from('xjm_article')->where('type=7 AND status=0')->order_by('sort_order')->get();
            $view['result']=$query->result_array();
        }
        else
        {
            $query=$this->db->select('title,content')->from('xjm_article')->where('type=8 AND status=0')->order_by('sort_order')->get();
            $view['result']=$query->result_array();
        }
//        var_dump($view['result']);die;
        $this->load->view('help/my_help',$view);

    }

    public function custom()
    {
        $query=$this->db->select('option')->from('xjm_option')->where("id=1")->limit(1)->get();
        $result=$query->row_array();
        $view['result']=json_decode($result['option'],true);

        $this->load->view('help/custom_service',$view);
    }


    public function user_agreement()
    {
        $source=$this->input->get('source');
        (empty($source)) ? $source='user' : $source=$source;

        if($source=='user')
        {
            $query=$this->db->select('title,content')->from('xjm_article')->where('type=5 AND status=0')->order_by('sort_order')->get();
            $view['result']=$query->row_array();
        }
        elseif($source=='techer_share_rules'){
            $query=$this->db->select('title,content')->from('xjm_article')->where('id=27 AND status=0')->order_by('sort_order')->get();
            $view['result']=$query->row_array();
        }elseif($source=='user_share_rules'){
            $query=$this->db->select('title,content')->from('xjm_article')->where('id=28 AND status=0')->order_by('sort_order')->get();
            $view['result']=$query->row_array();
        }
        else
        {
            $query=$this->db->select('title,content')->from('xjm_article')->where('type=6 AND status=0')->order_by('sort_order')->get();
            $view['result']=$query->row_array();
        }
//        var_dump($view);die;
        $this->load->view('help/user_agreement',$view);
    }
    
    //赚钱攻略
    public function money()
    {
        $this->load->view('help/index');
    }
    
    
    //服务流程
    public function o_some()
    {
        $source=$this->input->get('goods_type');
        switch ($source){
            case 1:
                $id=18;
                break;
            case 2:
                $id=19;
                break;
            case 3:
                $id=20;
                break;
            case 4:
                $id=21;
                break;
            case 5:
                $id=22;
                break;
            case 6:
                $id=23;
                break;
            case 7:
                $id=24;
                break;
            case 8:
                $id=25;
                break;
            default:
                $id=26;
        }

        $view['result']=$this->db->select('title,content')->from('xjm_article')->where('id='.$id)->get()->row_array();
        $this->load->view('help/o_some',$view);


    }
    
    


}
