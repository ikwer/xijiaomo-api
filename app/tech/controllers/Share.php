<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Share extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function __construct()
    {
        parent::__construct();
        
        define('web_url','https://api.xijiaomo.com/');
        define('send_code_url','tech.php/v1/Login/send_code');
        define('check_phone','tech.php/v1/Login/reg_step1');
        define('set_pwd','tech.php/v1/Techer/set_pwd');
    }

    
	public function wap()
	{
        $param=$this->input->get();
//        var_dump($param);die;
        if(empty($param))
        {
            echo "非法请求";
        }
        else
        {
            $tid=intval($param['tid']);
            $techer_query=$this->db->query("SELECT t.mobile,t.nickname,c.headurl,c.`desc`,t.`skill` FROM ( SELECT mobile,tid,nickname,skill FROM xjm_techer WHERE tid = $tid ) t JOIN ( SELECT tid,headurl,`desc` FROM xjm_techer_checkinfo WHERE tid = $tid ) c ON t.tid = c.tid");
            $user_info=$techer_query->row_array();

            (empty($user_info['skill'])) ? $skill=0 : $skill=$user_info['skill'];
            $re=$this->db->query('SELECT `name` FROM xjm_goods_option WHERE cate_id IN('.$skill.')')->result_array();
            $skill=array();
            if(!empty($re))
            {
                foreach ($re as $v)
                {
                    $skill[]=@implode(',',$v);

                }
                $skill=@implode(',',$skill);
            }
            else
            {
                $skill="";
            }
            unset($user_info['skill']);
            $user_info['desc']=$skill;


            $data['techer_row']=$user_info;

            $techer_photo_query=$this->db->query("SELECT pic_url,video_url,type FROM xjm_techer_photo WHERE tid=$tid AND type=1 ORDER BY add_time DESC LIMIT 4");
            $data['techer_photo_result']=$techer_photo_query->result_array();

            $techer_photo=$this->db->query("SELECT pic_url,video_url,type FROM xjm_techer_photo WHERE tid=$tid AND type=1  ORDER BY add_time");
            $data['techer_photo_nums']=$techer_photo->num_rows();
            $data['photo_video']=$techer_photo->result_array();



            $commemt_query=$this->db->query("SELECT nick_name,FROM_UNIXTIME(add_time,'%Y-%m-%d') dates,headurl,content FROM xjm_user_comment WHERE tid=$tid ORDER BY add_time DESC LIMIT 0,4");
            $data['comment_result']=$commemt_query->result_array();

            $commemt_querys=$this->db->query("SELECT nick_name,FROM_UNIXTIME(add_time,'%Y-%m-%d') dates,headurl,content FROM xjm_user_comment WHERE tid=$tid ORDER BY add_time DESC");
            $data['commemt_num']=$commemt_querys->num_rows();

            if($data['commemt_num']==0)
            {
                $data['great_bili']=0;
            }
            else
            {
                $great_query=$this->db->select('id')->from('xjm_user_comment')->where('tid='.$tid." and type=0")->get();
                $great_num=$great_query->num_rows();

                $cc=$this->db->select('id,comment_tag')->from('xjm_user_comment')->where('tid='.$tid." and comment_tag IS NOT NULL")->get();
                $cc_result=$cc->result_array();
                if(!empty($cc_result))
                {
                    foreach($cc_result as $k=>$v)
                    {
                        (empty($v['comment_tag'])) ? $tag=0 : $tag=$v['comment_tag'];
                        $tag_query=$this->db->query("SELECT tag_name FROM xjm_comment_tag_set WHERE id IN($tag)");
                        $re=$tag_query->result_array();
                        $tag_result[$k]=$re;
                    }
                }
                else
                {
                    $tag_result='';
                }


                $data['tag']=$tag_result;
                $data['great_bili']=ceil(($great_num/$data['commemt_num'])*100);
            }
            $this->load->view('share/wap',$data);
        }
	}

    //分页
    public function nextpage()
    {
        $data=$this->input->get('page');
        $tid=$this->input->get('tid');
        $limit=4;
        $page=$data*$limit;
        $commemt_query=$this->db->query("SELECT nick_name,FROM_UNIXTIME(add_time,'%Y-%m-%d') dates,headurl,content FROM xjm_user_comment WHERE tid=$tid ORDER BY add_time DESC LIMIT $page,$limit");
        $comment_result=$commemt_query->result_array();

        if(empty($comment_result))
        {
            $arr=array('code'=>-1,'data'=>'');
        }
        else
        {
            $arr=array('code'=>0,'data'=>$comment_result,'count'=>count($comment_result));
        }
        echo json_encode($arr);die;

    }


    public function reg_step1()
    {
        $param=$this->input->post();
        if(!empty($param)) {
            $url = web_url . set_pwd;
            $post = array(
                '0-account' => $param['account'],
                '1-pwd' => md5(md5($param['pwd'])),
                '2-re_pwd' => md5(md5($param['re_pwd'])),
                '3-code_id' => $_SESSION['code_id'],
                '4-code' => $param['code'],
                '5-version' => '1.0',
                '6-source' => 'share',
                '7-pid'=>$param['pid'],
                '8-icode'=>$param['icode'],
                'signature' => API_KEYS
            );
            ksort($post);

            $a = http_build_query($post);
            $signature = sha1(md5(urldecode($a)));

            $array = array(
                '0-account' => $param['account'],
                '1-pwd' => md5(md5($param['pwd'])),
                '2-re_pwd' => md5(md5($param['re_pwd'])),
                '3-code_id' => $_SESSION['code_id'],
                '4-code' => $param['code'],
                '5-version' => '1.0',
                '6-source' => 'share',
                '7-pid'=>$param['pid'],
                '8-icode'=>$param['icode'],
                'signature' => $signature
            );
            $steam = $this->curl_info($url, 'post', $array);
            $json = json_decode($steam, true);

            echo $steam;die;
        }else{
            $this->load->view('share/reg_step1');
        }


    }



    //发送短信
    public function send_code()
    {
        $param=$this->input->post();
        $c_url=web_url.check_phone;
        $url=web_url.send_code_url;


        //检测手机号是否被注册
        $return=$this->check_phone($param,$c_url);
        if($param['type']==1 && $return['code']!=0) {
            echo json_encode($return);
            die;
        }elseif($param['type']==2 && $return['code']==0){
            echo json_encode(array('code'=>-2,'message'=>'用户不存在'));
            die;
        }else{
            //发送短信
            $post=array('0-account'=>$param['account'],'1-type'=>$param['type'],'signature'=>API_KEYS);
            ksort($post);

            $a= http_build_query($post);
            $signature=sha1(md5(urldecode($a)));

            $array=array('0-account'=>$param['account'],'1-type'=>$param['type'],'signature'=>$signature);
            $steam=$this->curl_info($url,'post',$array);
            $json=json_decode($steam,true);
            if($json['code']==0){
                $_SESSION['code_id']=$json['data']['code_id'];
            }
            echo $steam;die;
        }

    }


    //检测是否已被注册
    function check_phone($param,$c_url)
    {

        $c_post=array('0-account'=>$param['account'],'signature'=>API_KEYS);
        ksort($c_post);
        $c_a=http_build_query($c_post);
        $c_signature=sha1(md5(urldecode($c_a)));
        $c_array=array('0-account'=>$param['account'],'signature'=>$c_signature);
        $c_steam=$this->curl_info($c_url,'post',$c_array);
        $c_json=json_decode($c_steam,true);
        if($c_json['code']!=0){
            return $c_json;
        }
    }


    //curl
    function curl_info($url,$method,$data)
    {
        if($method=="post"){
            ob_start();
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_exec($ch);
            curl_close($ch);
            $stream = ob_get_contents();
            ob_end_clean();

        }else{
            ob_start();
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_exec($ch);
            curl_close($ch);
            $stream = ob_get_contents();
            ob_end_clean();
        }

        return $stream;
    }
    
  


}
