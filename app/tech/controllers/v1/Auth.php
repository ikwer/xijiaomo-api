<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('authtoken');
    }

    //颁发token令牌
    public function issue_token() {
        $tid = $this->input->post('tid');
        $mobile = $this->input->post('account');
        if (!preg_match('/^1\d{10}/', $mobile) || empty(intval($tid))) {
            ajax_return(-1, '参数格式有误');
        }
        //检测请求用户是否存在
        $this->load->model('techer_v1_model');
        $tech_info = $this->techer_v1_model->tech_info($tid, $mobile);
        if (!$tech_info['status']) {
            ajax_return(-1, '令牌发放失败,不存在的用户');
        }
        $rs = $this->authtoken->auth_encode($tid);
        if ($rs['status']) {
            ajax_return(0, '请求成功', $rs['data']);
        } else {
            ajax_return(-1, '请求失败,令牌生成失败');
        }
    }

    //验证令牌
    public function check_token() {
        $token = $this->input->post('token');
        $tid = $this->input->post('tid');
        if (empty($tid) || empty($token)) {
            ajax_return(-1, '缺少验证数据');
        }
        $rs = $this->authtoken->autn_decode($tid, $token);
        if ($rs['status']) {
            ajax_return(0, '令牌验证成功');
        } else {
            ajax_return(-1, '令牌验证失败');
        }
    }
    
    //七牛使用demo
    public function qiniut(){
        $this->load->library('qiniu');
        
        //初始化七牛信息
        header('Access-Control-Allow-Origin:*');
        $bucket = 'xijiaomo';
        $accessKey = 'b2wTxQXjJKx4igvZfSNfp_ZWV5Flg6cHrzJAswaB';
        $secretKey = '5N6gxBSfvKLZRSAuv3yKMTJkiWMfzIIMeCjQsNRW';
        $auth = new Qiniu\Auth($accessKey, $secretKey);
        $upToken = $auth->uploadToken($bucket, null, 3600);//获取上传所需的token
        
        ajax_return(0, 'upToken获取成功', array('upToken'=>$upToken));
    }
}
