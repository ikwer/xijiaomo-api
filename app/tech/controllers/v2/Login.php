<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    public $code = '';
    
    public function __construct(){
        parent::__construct();
        
        $this->load->model('public_v2_model');
        $this->load->model('techer_v2_model');
        $this->load->library('authtoken');

        //随机短信验证码
        $this->code = rand(1000, 9999);
//        $this->code = 1234;
        
    }




   
    //账号密码登录
    public function account_login() {

        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);

        (!empty($param['account'])) ? $mobile = $param['account'] : $mobile = 0 ;
        (!empty($param['pwd'])) ? $pwd = $param['pwd'] : $pwd = 0 ;

        if(empty($mobile) || empty($pwd))
        {
            ajax_return('-1','参数错误');
        }

        $pwd = sha1($pwd);
        $rs = $this->techer_v2_model->check_login($mobile, $pwd);
        if ($rs['status']==true) {
           ajax_return(0, $rs['msg'], $rs['data']);
        }elseif($rs['msg']=='请补全个人身份信息'){
            ajax_return(10008, $rs['msg'], $rs['data']);
        }
        else {
           ajax_return(-1, $rs['msg']);
        }
    }
    
    //注册判断当前手机号是否已注册
    public function reg_step1(){

        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);

        (!empty($param['account'])) ? $mobile = $param['account'] : $mobile = 0 ;


        if (!preg_match('/^1\d{10}/', $mobile)) {
            ajax_return(-1, '手机号码格式有误');
        }
        $r = $this->techer_v2_model->check_mobile($mobile);
        if($r['status'] === false){
            //发送短信验证码
            /* $data['code_id'] = 0;
            $smsinfo = $this->sednd_code_auto($mobile);
            if($smsinfo['status']){
                $data['code_id'] = $smsinfo['data']['code_id'];
            }
            ajax_return(0, $r['msg'], $data); */
            
            ajax_return(0, $r['msg']);
        }else{
            ajax_return(-1, $r['msg']);
        }
    }
    
    //发送注册短信--自动发送
    public function sednd_code_auto($mobile){
        //入库保存
        $inset_sms = $this->public_v2_model->inset_sms_code($mobile, $this->code);
        if (empty($inset_sms['status'])) {
            return array('status'=>false);
        }
        //聚合短信发送demo
        $this->load->library('juhesms');
        $send_rs = $this->juhesms->send($mobile, 35216, $this->code);
        if ($send_rs !== false) {
            $data['code_id'] = $inset_sms['data']['send_id'];
            $data['phone'] = $mobile;
            $data['code'] = $this->code;
            return array('status'=>true, 'data'=>$data);
        } else {
            return array('status'=>false);
        }
    }

    //发送注册短信--手动发送
    public function send_code() {

        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);

        (!empty($param['account'])) ? $mobile = $param['account'] : $mobile = 0 ;
        (!empty($param['type'])) ? $type = $param['type'] : $type = 0 ;


        if (!preg_match('/^1\d{10}/', $mobile)) {
            ajax_return(1002);
        }
        //入库保存
        $types=$type;
        $inset_sms = $this->public_v2_model->inset_sms_code($mobile, $this->code,$types);
        if (empty($inset_sms['status'])) {
            ajax_return(-1, $inset_sms['msg']);
        }
        //聚合短信发送demo
        $this->load->library('juhesms');
        $content = "【喜脚么】您的验证码是$this->code";
        $send_rs = $this->juhesms->send_phone($mobile, $content);
        if ($send_rs['code'] === 0 ) {
            $data['code_id'] = $inset_sms['data']['send_id'];
            $data['mobile'] = $mobile;
            //$data['code'] = $this->code;
            ajax_return(0, '短信发送成功请注意查收', $data);
        } else {
            ajax_return(-1, '验证码发送失败');
        }
    }
    
    //验证短信验证码是否有效
    public function valid_code(){

        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);

        (!empty($param['account'])) ? $mobile = $param['account'] : $mobile = 0 ;
        (!empty($param['code_id'])) ? $code_id = $param['code_id'] : $code_id = 0 ;
        (!empty($param['code'])) ? $code = $param['code'] : $code = 0 ;

        if($mobile==0 || $code_id==0 || $code==0)
        {
            ajax_return("-1","参数错误");
        }

        //验证新手机验证码
        $check_phone = $this->public_v2_model->check_phone_code($mobile, $code_id, $code);
        if (!$check_phone['status']) {
            ajax_return(-1, '验证码输入有误');
        }else{
            ajax_return(0, '验证码正确');
        }
    }
    
    //用户(注册）
    public function doregister() {
        $baseinfo = array();
        $addinfo  = array();
        
        //基本信息
        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);
        (!empty($param['tid'])) ? $addinfo['tid'] = $param['tid'] : $addinfo['tid'] = '' ;
        (!empty($param['account'])) ? $baseinfo['mobile'] = $param['account'] : $baseinfo['mobile'] = '' ;
        (!empty($param['city'])) ? $baseinfo['city'] = $param['city'] : $baseinfo['city'] = '武汉' ;



        //$pwd = $this->input->post('pwd');
        //$baseinfo['pwd'] = sha1($pwd);//密码传输两次md5加密md5(md5($pwd))

        //附加信息
        (!empty($param['truename'])) ? $addinfo['truename'] = $param['truename'] : $addinfo['truename'] = "" ;
        (!empty($param['card'])) ? $addinfo['card'] = $param['card'] : $addinfo['card'] = "" ;
        (!empty($param['sex'])) ? $addinfo['sex'] = $param['sex'] : $addinfo['sex'] = "" ;
        (!empty($param['birthday'])) ? $addinfo['birthday'] = $param['birthday'] : $addinfo['birthday'] = "" ;
        (!empty($param['desc'])) ? $addinfo['desc'] = $param['desc'] : $addinfo['desc'] = "" ;
        (!empty($param['idcard_zheng'])) ? $addinfo['idcard_zheng'] = $param['idcard_zheng'] : $addinfo['idcard_zheng'] = "" ;
        (!empty($param['idcard_fan'])) ? $addinfo['idcard_fan'] = $param['idcard_fan'] : $addinfo['idcard_fan'] = "" ;
        (!empty($param['headurl'])) ? $addinfo['headurl'] = $param['headurl'] : $addinfo['headurl'] = "" ;
        //(!empty($param['skill'])) ? $addinfo['skill'] = $param['skill'] : $addinfo['skill'] = "" ;


        
        if(empty($addinfo['tid']) || empty($baseinfo['mobile'])  || empty($baseinfo['city']) || empty($addinfo['truename']) || empty($addinfo['card']) || empty($addinfo['sex']) || empty($addinfo['birthday']) || empty($addinfo['idcard_fan']) || empty($addinfo['idcard_zheng'])){
            ajax_return(-1, '参数错误');
        }
        if (!preg_match('/^1\d{10}/', $baseinfo['mobile'])) {
            ajax_return(-1, '手机号码格式有误');
        }

        $dang=date('Ymd');
        $birth=date('Ymd',$addinfo['birthday']);

        $age=$dang-$birth;
        if($age<180000 || $age>550000)
        {
            ajax_return(-1, '注册年龄必须在18周岁以上，55周岁以下');
        }
        
        /*$r = $this->techer_v2_model->check_mobile($baseinfo['mobile']);
        if($r['status'] !== false){
            ajax_return(-1, $r['msg']);
        }*/
        $check_idcard=$this->is_idcard(trim($addinfo['card']));
        if(!$check_idcard)
        {
            ajax_return(-1, '身份证号码格式错误');
        }

        $inset = $this->techer_v2_model->insert_techer($baseinfo, $addinfo);
        if ($inset['status']) {
            ajax_return(0, $inset['msg']);
        } else {
            ajax_return(-1, $inset['msg']);
        }
    }

    //更换手机号第一步  验证当前手机号码
    public function change_mobile_first() {
        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);

        (!empty($param['account'])) ? $mobile = $param['account'] : $mobile = '' ;
        (!empty($param['code_id'])) ? $code_id = $param['code_id'] : $code_id = '' ;
        (!empty($param['code'])) ? $code = $param['code'] : $code = '' ;


        if (!preg_match('/^1\d{10}/', $mobile)) {
            ajax_return(-1, '手机号码格式有误');
        }
        if (!preg_match('/^\d{4}$/i', $code) || empty(intval($code_id))) {
            ajax_return(-1, '验证码格式有误');
        }
        $check_phone = $this->public_v2_model->check_phone_code($mobile, $code_id, $code);
        if (!$check_phone['status']) {
            ajax_return(-1, '验证码输入有误');
        } else {
            ajax_return(0, '验证成功');
        }
    }

    //更换手机号第二步  验证新手机号码  修改手机号码
    public function change_mobile_second() {
        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);

        (!empty($param['tid'])) ? $tid = $param['tid'] : $tid = '' ;
        (!empty($param['account'])) ? $mobile = $param['account'] : $mobile = '' ;
        (!empty($param['new_account'])) ? $new_mobile = $param['new_account'] : $new_mobile = '' ;
        (!empty($param['code_id'])) ? $code_id = $param['code_id'] : $code_id = '' ;
        (!empty($param['code'])) ? $code = $param['code'] : $code = '' ;




        if (!preg_match('/^1\d{10}/', $mobile) || !preg_match('/^1\d{10}/', $new_mobile)) {
            ajax_return(-1, '手机号码格式有误');
        }
        if (!preg_match('/^\d{4}$/i', $code) || empty(intval($code_id))) {
            ajax_return(-1, '验证码格式有误');
        }
        //验证新手机是否有人使用
        $phone_check = $this->techer_v2_model->check_mobile($new_mobile);
        if ($phone_check['status']) {
            ajax_return(-1, '新手机号码已被使用');
        }
        //验证新手机验证码
        $check_phone = $this->public_v2_model->check_phone_code($new_mobile, $code_id, $code);
        if (!$check_phone['status']) {
            ajax_return(-1, '验证码输入有误');
        }
        //更换新手机号码
        $change_phone = $this->techer_v2_model->change_mobile($tid, $mobile, $new_mobile);
        if ($change_phone['status']) {
            ajax_return(0, '更换成功');
        } else {
            ajax_return(-1, '更换失败');
        }
    }
    
    //忘记密码
    public function doforgetpwd() {


        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);

        (!empty($param['account'])) ? $mobile = $param['account'] : $mobile = '' ;
        (!empty($param['new_pwd'])) ? $pwd = $param['new_pwd'] : $pwd = '' ;
        (!empty($param['code_id'])) ? $code_id = $param['code_id'] : $code_id = '' ;
        (!empty($param['code'])) ? $code = $param['code'] : $code = '' ;


        if (!preg_match('/^1\d{10}/', $mobile)) {
            ajax_return(-1, '手机号码格式有误');
        }
        if (!preg_match('/^\d{4}$/i', $code) || empty(intval($code_id))) {
            ajax_return(-1, '验证码格式有误');
        }
        $check_phone = $this->public_v2_model->check_phone_code($mobile, $code_id, $code);
        if (!$check_phone['status']) {
            ajax_return(-1, '验证码输入有误');
        }
        //开始重置密码
        $pwd = sha1($pwd);
        $rechange = $this->techer_v2_model->change_pwd($mobile, $pwd);
        if ($rechange['status']) {
            ajax_return(0, '密码修改成功');
        } else {
            ajax_return(-1, '密码修改失败');
        }
    }


    //验证身份证号方法
    public function is_idcard($id)
    {
        $id = strtoupper($id);
        $regx = "/(^\d{15}$)|(^\d{17}([0-9]|X)$)/";
        $arr_split = array();
        if(!preg_match($regx, $id))
        {
            return FALSE;
        }
        if(15==strlen($id)) //检查15位
        {
            $regx = "/^(\d{6})+(\d{2})+(\d{2})+(\d{2})+(\d{3})$/";

            @preg_match($regx, $id, $arr_split);
            //检查生日日期是否正确
            $dtm_birth = "19".$arr_split[2] . '/' . $arr_split[3]. '/' .$arr_split[4];
            if(!strtotime($dtm_birth))
            {
                return FALSE;
            } else {
                return TRUE;
            }
        }
        else      //检查18位
        {
            $regx = "/^(\d{6})+(\d{4})+(\d{2})+(\d{2})+(\d{3})([0-9]|X)$/";
            @preg_match($regx, $id, $arr_split);
            $dtm_birth = $arr_split[2] . '/' . $arr_split[3]. '/' .$arr_split[4];
            if(!strtotime($dtm_birth)) //检查生日日期是否正确
            {
                return FALSE;
            }
            else
            {
                //检验18位身份证的校验码是否正确。
                //校验位按照ISO 7064:1983.MOD 11-2的规定生成，X可以认为是数字10。
                $arr_int = array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2);
                $arr_ch = array('1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2');
                $sign = 0;
                for ( $i = 0; $i < 17; $i++ )
                {
                    $b = (int) $id{$i};
                    $w = $arr_int[$i];
                    $sign += $b * $w;
                }
                $n = $sign % 11;
                $val_num = $arr_ch[$n];
                if ($val_num != substr($id,17, 1))
                {
                    return FALSE;
                } //phpfensi.com
                else
                {
                    return TRUE;
                }
            }
        }

    }
    
    
    //技能列表展示
    public function skill_list()
    {
        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);

        (!empty($param['tid'])) ? $tid = $param['tid'] : $tid = 0 ;
        $one=$this->db->select('cate_id,name')->from('xjm_goods_option')->order_by('cate_id')->get()->result_array();

        $te_skill=$this->db->select('skill')->from('xjm_techer')->where(array('tid'=>intval($tid)))->limit(1)->get()->row_array();

        $new=array();
        foreach($te_skill as $k=>$v){
            (empty($v)) ? $v=0 : $v=$v;
            $sql=" SELECT * FROM ( ( SELECT cate_id,name,\"1\" trues FROM xjm_goods_option WHERE cate_id IN (".$v.") ) UNION ALL ( SELECT cate_id,name,\"0\" trues FROM xjm_goods_option WHERE cate_id NOT IN (".$v.") ) ) a GROUP BY a.cate_id";
            $new=$this->db->query($sql)->result_array();
        }

        ajax_return(0,'技能列表展示成功',$new);

    }

}
