<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notice extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->model('public_v2_model');
    }


    
    
    function index(){

        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);

        (!empty($param['tid'])) ? $tid = $param['tid'] : $tid = '' ;

        if ($tid) {
            //获取站内信息
            $statistics = $this->public_v2_model->notice($tid);
            if ($statistics['status']) {
                $all_num = $statistics['data'];
            }
        } else {
            $all_num = 0;
        }
        ajax_return(0, '获取成功', array('all_num' => $all_num));
    }
   
    //所有公告信息
    function msg() {

        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);

        (!empty($param['tid'])) ? $tid = $param['tid'] : $tid = '' ;
        (!empty($param['page'])) ? $page = $param['page'] : $page = '' ;

        $page ? $page : $page = 1;
        if (empty(intval($tid))) {
            ajax_return(-1, '缺少重要数据');
        }
        $rs = $this->public_v2_model->unread($tid, $page);
        if ($rs['status']) {
            ajax_return(0, '成功', $rs['data']);
        } else {
            ajax_return(-2, '暂无数据');
        }
    }


    //技师免责条款
    public function techer_mianze()
    {
        $result=$this->db->select('title,content')->from('xjm_article')->where('id=30')->get()->row_array();
        if(!empty($result)){
            ajax_return(0,'数据获取成功',$result);
        }else{
            ajax_return(-1,'数据获取失败',$result);
        }
    }

    //更改公告状态
    function read_msg() {

        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);

        (!empty($param['tid'])) ? $tid = $param['tid'] : $tid = '' ;
        (!empty($param['msg_id'])) ? $msg_id = $param['msg_id'] : $msg_id = '' ;

        if (empty(intval($tid)) || empty($msg_id)) {
            ajax_return(-1, '缺少重要数据');
        }
        $rs = $this->public_v2_model->change_msg($tid, $msg_id);
        if ($rs['status']) {
            ajax_return(0, '成功');
        } else {
            ajax_return(-1, '失败');
        }
    }
    
    
    //技师关于我们
    public function techer_about()
    {
        $query=$this->db->select('option')->from('xjm_option')->where("id=1")->limit(1)->get();
        $result=$query->row_array();

        if(empty($result))
        {
            ajax_return(-1, '失败','');
        }
        else
        {
            $arr=json_decode($result['option'],'true');
            unset($arr['au_version']);
            unset($arr['at_version']);
            unset($arr['iu_version']);
            unset($arr['it_version']);
            $arr['version']='1.1.0';
            ajax_return(0,'成功',$arr);
        }
    }
    
    
    public function check_update()
    {

        $query=$this->db->select('option')->from('xjm_option')->where("id=1")->limit(1)->get();
        $result=$query->row_array();

        if(empty($result))
        {
            ajax_return(-1, '失败','');
        }
        else
        {
            $arr=json_decode($result['option'],'true');
            $ar=array(
                'android_new_version'=>$arr['at_version'],
                'android_download_url'=>'http://download.pargo24.com/android/beta/techer/Technician-release.apk',
                'android_is_update'=>0,
            );
            ajax_return(1, '获取成功',$ar);

        }
    }
    
    
    public function check_ios_update()
    {
        $query=$this->db->select('option')->from('xjm_option')->where("id=1")->limit(1)->get();
        $result=$query->row_array();

        if(empty($result))
        {
            ajax_return(-1, '失败','');
        }
        else
        {
            $arr=json_decode($result['option'],'true');
            $ar=array(
                'ios_new_version'=>$arr['it_version'],
                'ios_download_url'=>'xxx',
                'ios_is_update'=>1,
            );
            ajax_return(1, '获取成功',$ar);

        }
        
    }
}
