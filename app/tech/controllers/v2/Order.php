<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {
    public $endtime = 0;
    public $raidus  = 0;
    
    public function __construct(){
        parent::__construct();
       
        $this->load->model('public_v2_model');
        $this->load->model('order_v2_model');
        $this->load->library('authtoken');
        

        //初始化5分钟倒计时
        $this->endtime = 300;
        //初始化范围半径
        $this->raidus = 3000;

    }
    
    //获取5分钟内3公里范围内的所有能抢的订单
	public function get_area_order(){

        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);

        (!empty($param['tid'])) ? $tid = $param['tid'] : $tid = '' ;
        (!empty($param['token'])) ? $token = $param['token'] : $token = '' ;
        (!empty($param['t_lat'])) ? $t_lat = $param['t_lat'] : $t_lat = '' ;
        (!empty($param['t_lon'])) ? $t_lon = $param['t_lon'] : $t_lon = '' ;


	    $difftime = time()-$this->endtime;
	    $page=$param['page'];
	    if(empty(intval($tid)) || empty($t_lat) || empty($t_lon) ){
	        ajax_return(-1, '非法请求');
	    }

	    $this->check_token($tid,$token);
	    $this->check_techer($tid);
	    $date_start=strtotime(date("Y-m-d 00:00:00"));
	    $date_end=$date_start+86400;
        $tid=intval($tid);
        $limit=5;
        if(empty($page)){
            $start=0;
        }else{
            if($page==1){
                $start=0;
            }
            else{
                $start=$page*$limit-$limit;
            }
            
            
        }



        //更新技师经纬度
        $t_data=array(
            't_lat'=>$t_lat,
            't_lon'=>$t_lon
        );
        $this->db->where(array('tid'=>$tid))->update('xjm_techer',$t_data);

	    $sql="SELECT order_id,uid,option_type,order_sn,order_status,pay_status,address,goods_name,goods_price,u_lon,u_lat,FROM_UNIXTIME(add_time,'%Y-%m-%d %H:%i:%s') addtime FROM xjm_order_list WHERE add_time>=$date_start AND add_time<$date_end AND pay_status=0 AND order_status=1  LIMIT $start,$limit";
	    $query=$this->db->query($sql)->result_array();
	    
	    
	    $all="SELECT order_id,uid,option_type,order_sn,order_status,pay_status,address,goods_name,goods_price,u_lon,u_lat,FROM_UNIXTIME(add_time,'%Y-%m-%d %H:%i:%s') addtime FROM xjm_order_list WHERE add_time>=$date_start AND add_time<$date_end AND pay_status=0 AND order_status=1";
	    $all_q=$this->db->query($all)->num_rows();

        if(empty($query))
        {
            ajax_return(-2, '暂无数据','');
        }
        else
        {

                $result = GetRange($t_lat, $t_lon, $this->raidus);
                $data=array();
                foreach($query as $ke=>$va)
                {
                    $va['distance'] = sphere_distance($t_lat, $t_lon,$va['u_lat'], $va['u_lon']);
                    $va['distance'] = abs($va['distance']).'KM';
                    $data['list'][]=$va;
                }
                $data['count']=$all_q;
                ajax_return(0, '返回成功', $data);


        }

	}


    //订单详情
    public function order_info()
    {
        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);
        (!empty($param['tid'])) ? $tid = $param['tid'] : $tid = '' ;
        (!empty($param['order_id'])) ? $order_id = $param['order_id'] : $order_id = '' ;
        (!empty($param['token'])) ? $token = $param['token'] : $token = '' ;
        if(empty($tid))
        {
            ajax_return(-1, '非法请求');
        }

        $this->check_token($tid,$token);
        $this->check_techer($tid);
        $tid=intval($tid);
        $r = $this->order_v2_model->order_info($tid,$order_id);
        if($r['status'] !== false){
            ajax_return(0, $r['msg'], $r['data']);
        }else{
            ajax_return(-2, $r['msg'],'');
        }
    }


    //未完成的订单
    public function no_finish_order()
    {
        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);
        (!empty($param['tid'])) ? $tid = $param['tid'] : $tid = '' ;
        (!empty($param['token'])) ? $token = $param['token'] : $token = '' ;
        if(empty($tid))
        {
            ajax_return(-1, '非法请求');
        }

        //$this->check_token($tid,$token);
        $this->check_techer($tid);

        $tid=intval($tid);
        $r = $this->order_v2_model->no_finish_order($tid);
        if($r['status'] !== false){
            ajax_return(0, $r['msg'], $r['data']);
        }else{
            ajax_return(-2, $r['msg'],$r['data']);
        }

    }



    //检测技师是否可以开始服务
    public function location_range()
    {

        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);

        (!empty($param['tid'])) ? $tid = $param['tid'] : $tid = '' ;
        (!empty($param['order_id'])) ? $order_id = $param['order_id'] : $order_id = '' ;
        (!empty($param['u_lon'])) ? $u_lon = $param['u_lon'] : $u_lon = '' ;
        (!empty($param['u_lat'])) ? $u_lat = $param['u_lat'] : $u_lat = '' ;
        (!empty($param['token'])) ? $token = $param['token'] : $token = '' ;




        if(empty($tid) || empty($order_id) || empty($u_lon) || empty($u_lat) )
        {
            ajax_return(-1, '非法请求');
        }

        $tid=intval($tid);

        $this->check_token($tid,$token);
        $this->check_techer($tid);

        $r = $this->order_v2_model->location_range($tid,$order_id,$u_lon,$u_lat);
        if($r['status'] !== false){
            ajax_return(0, $r['msg'], '');
        }else{
            ajax_return(-1, $r['msg'],'');
        }
    }







    //技师开始服务
    public function start_work()
    {
        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);
        (!empty($param['tid'])) ? $tid = $param['tid'] : $tid = '' ;
        (!empty($param['order_id'])) ? $order_id = $param['order_id'] : $order_id = '' ;
        (!empty($param['token'])) ? $token = $param['token'] : $token = '' ;

        if(empty($tid) || empty($order_id))
        {
            ajax_return(-1, '非法请求');
        }


        $tid=intval($tid);

        $this->check_token($tid,$token);
        $this->check_techer($tid);

        $r = $this->order_v2_model->start_work($tid,$order_id);
        if($r['status'] !== false){
            ajax_return(0, $r['msg'], '');
        }else{
            ajax_return(-1, $r['msg'],'');
        }
    }



    //刷新订单，返回订单状态
    public function reflush_order()
    {
        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);
        (!empty($param['tid'])) ? $tid = $param['tid'] : $tid = '' ;
        (!empty($param['order_id'])) ? $order_id = $param['order_id'] : $order_id = '' ;
        (!empty($param['token'])) ? $token = $param['token'] : $token = '' ;
        if(empty($order_id) || empty($tid)){
            ajax_return(-1, '非法请求');
        }
        $this->check_token($tid,$token);
        $this->check_techer($tid);

        $r = $this->order_v2_model->reflush_order($tid,$order_id);
        if($r['status'] !== false){
            ajax_return(0, $r['msg'], '');
        }else{
            ajax_return(-1, $r['msg'],'');
        }
    }





	

	//获取订单列表
	public function all_order(){
        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);

        (!empty($param['tid'])) ? $tid = $param['tid'] : $tid = '' ;
        (!empty($param['token'])) ? $token = $param['token'] : $token = '' ;
        (!empty($param['order_status'])) ? $order_status = $param['order_status'] : $order_status = '' ;

	    if(empty(intval($tid))){
	        ajax_return(-1, '非法请求');
	    }

	    $this->check_token($tid,$token);
	    $this->check_techer($tid);

	    //获取当前待服务，已完成的订单
	    $r = $this->order_v2_model->getAllOrder($tid, $order_status);
	    if($r['status'] !== false){
	        ajax_return(0, $r['msg'], $r['data']);
	    }else{
	        ajax_return(-1, $r['msg'],'');
	    }
	}
	
	//获取订单详情
	public function order_detail(){

        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);

        (!empty($param['tid'])) ? $tid = $param['tid'] : $tid = '' ;
        (!empty($param['token'])) ? $token = $param['token'] : $token = '' ;
        (!empty($param['order_id'])) ? $order_id = $param['order_id'] : $order_id = '' ;


	    if(empty(intval($tid)) && empty($order_id)){
	        ajax_return(-1, '非法请求');
	    }

	    $this->check_token($tid,$token);
	    $this->check_techer($tid);

	    $r = $this->order_v2_model->getOrderDetail($tid, $order_id);
	    if($r['status'] !== false){
	        ajax_return(0, $r['msg'], $r['data']);
	    }else{
	        ajax_return(-1, $r['msg'],'');
	    }
	}
	
	//技师确认订单到达
	public function confirm_arrival(){

        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);

        (!empty($param['tid'])) ? $tid = $param['tid'] : $tid = '' ;
        (!empty($param['token'])) ? $token = $param['token'] : $token = '' ;
        (!empty($param['order_id'])) ? $order_id = $param['order_id'] : $order_id = '' ;



	    if(empty(intval($tid)) && empty($order_id)){
	        ajax_return(-1, '非法请求');
	    }

	    $this->check_token($tid,$token);
	    $this->check_techer($tid);

	    $r = $this->order_v2_model->do_confirm_arrival($tid, $order_id);
	    if($r['status'] !== false){
	        ajax_return(0, $r['msg'], $r['data']);
	    }else{
	        ajax_return(-1, $r['msg'],'');
	    }
	}
	
	//技师确认订单完成
	public function confirm_finish(){

        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);
        (!empty($param['tid'])) ? $tid = $param['tid'] : $tid = '' ;
        (!empty($param['order_id'])) ? $order_id = $param['order_id'] : $order_id = '' ;
        (!empty($param['order_sn'])) ? $order_sn = $param['order_sn'] : $order_sn = '' ;
        (!empty($param['token'])) ? $token = $param['token'] : $token = '' ;

        if(empty(intval($tid)) || empty($order_id) || empty($order_sn)){
	        ajax_return(-1, '非法请求');
	    }

	    $this->check_token($tid,$token);
	    $this->check_techer($tid);

        $r = $this->order_v2_model->do_confirm_finish($tid, $order_id,$order_sn);
	    if($r['status'] !== false){
	        ajax_return(0, $r['msg'], '');
	    }else{
	        ajax_return(-1, $r['msg'],'');
	    }
	}


    //技师同意/不同意退款操作
	public function teacher_refund()
    {
        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);
        (!empty($param['tid'])) ? $tid = $param['tid'] : $tid = '' ;
        (!empty($param['refund_id'])) ? $refund_id = $param['refund_id'] : $refund_id = '' ;
        (!empty($param['token'])) ? $token = $param['token'] : $token = '' ;
        (!empty($param['is_success'])) ? $is_success = $param['is_success'] : $is_success = 0 ;

        if(empty(intval($tid)) || empty($refund_id)){
            ajax_return(-1, '非法请求');
        }

        $this->check_token($tid,$token);
        $this->check_techer($tid);

        $r = $this->order_v2_model->teacher_refund($tid,$refund_id,$is_success);
        if($r['status'] !== false){
            ajax_return(0, $r['msg'], '');
        }else{
            ajax_return(-1, $r['msg'],'');
        }
    }


    /**
     * 订单列表
     */
    public function order_list()
    {
        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);
        (!empty($param['tid'])) ? $tid = $param['tid'] : $tid = '' ;
        (!empty($param['token'])) ? $token = $param['token'] : $token = '' ;
        if(empty(intval($tid))){
            ajax_return(-1, '非法请求');
        }

        $this->check_token($tid,$token);
        $this->check_techer($tid);

        $r = $this->order_v2_model->order_list($tid);
        if($r['status'] !== false){
            ajax_return(0, $r['msg'], $r['data']);
        }else{
            ajax_return(-2, $r['msg'],'');
        }


    }

    //退款列表
    public function refund_list()
    {
         $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);
        (!empty($param['tid'])) ? $tid = $param['tid'] : $tid = '' ;
        (!empty($param['token'])) ? $token = $param['token'] : $token = '' ;
        (!empty($param['page'])) ? $page = $param['page'] : $page = 0 ;
        if(empty(intval($tid))){
            ajax_return(-1, '非法请求');
        }
//
        $this->check_token($tid,$token);
        $this->check_techer($tid);

        $r = $this->order_v2_model->refund_list($tid,$page);
        if($r['status'] !== false){
            ajax_return(0, $r['msg'], $r['data']);
        }else{
            ajax_return(-2, $r['msg'],'');
        }
    }


    //退款详情
    public function refund_info()
    {
        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);
        (!empty($param['tid'])) ? $tid = $param['tid'] : $tid = '' ;
        (!empty($param['token'])) ? $token = $param['token'] : $token = '' ;
        (!empty($param['refund_id'])) ? $refund_id=$param['refund_id'] : $refund_id='';
        if(empty(intval($tid)) || empty($refund_id)){
            ajax_return(-1, '非法请求');
        }
//
        $this->check_token($tid,$token);
        $this->check_techer($tid);

        $r = $this->order_v2_model->refund_info($tid,$refund_id);
        if($r['status'] !== false){
            ajax_return(0, $r['msg'], $r['data']);
        }else{
            ajax_return(-2, $r['msg'],'');
        }

    }
    
    //我的订单列表
    public function my_order_list()
    {
        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);
        (!empty($param['tid'])) ? $tid = $param['tid'] : $tid = 0 ;
        (!empty($param['token'])) ? $token = $param['token'] : $token = '' ;
        (!empty($param['type'])) ? $type = $param['type'] : $type = 0 ;
        (!empty($param['page'])) ? $page = $param['page'] : $page = 0 ;
        if(empty($tid))
        {
            ajax_return(-1, '非法请求');
        }
        $tid=intval($tid);
        $this->check_token($tid,$token);
        $this->check_techer($tid);
        
        $type=intval($type);
        $r = $this->order_v2_model->my_order_list($tid,$type,$page);
        if($r['status'] !== false){
            ajax_return(0, $r['msg'], $r['data']);
        }else{
            ajax_return(-2, $r['msg'],'');
        }
    }
    


    //检测token
    public function check_token($tid,$token)
    {
        $checkauth = $this->authtoken->autn_decode($tid, $token);
        if(!$checkauth['status']){
            ajax_return(-1, '令牌验证失败，请重新登录');
        }else{
            return true;
        }
    }
    
    
    public function check_techer_status(){
        $param=$this->input->post();
        check_sign($param);
        $param=decodeParam($param);
        (!empty($param['tid'])) ? $tid = $param['tid'] : $tid = 0 ;
        (!empty($param['token'])) ? $token = $param['token'] : $token = 0 ;
        $this->check_token($tid,$token);
        $check = $this->db->select('t.tid,t.status,c.sex')->
        from('xjm_techer t')->
        join('xjm_techer_checkinfo c','t.tid=c.tid','left')->
        where(array('t.tid'=>$tid))->
        get();
        $check_row=$check->row_array();
        $h=date('G');
        if($check_row['status']==0)
        {
            ajax_return(-1,'账号审核中');
        }
        elseif($check_row['status']==2)
        {
            ajax_return(-2,'账号已封号');
        }
        else
        {
            if($check_row['status']==1) {
                if ($check_row['sex'] == 2) {
                    if($h<8 || $h>20){
                        ajax_return(-3, '女性技师早八点到晚八点才能接单');
                    }
                    else
                    {
                        ajax_return(0, '账号可以接单');
                    }
                } else {
                    ajax_return(0, '账号可以接单');
                }
            }

        }
    }
    
    
    //检测技师审核状态
    public function check_techer($tid)
    {
        $check = $this->db->select('tid,status')->from('xjm_techer')->where(array('tid'=>$tid))->get();
        $check_row=$check->row_array();
        if($check_row['status']==0)
        {
            ajax_return('-1','账号审核中');
        }
        elseif($check_row['status']==2)
        {
            ajax_return('-1','账号已封号');
        }
        else
        {
            return true;
        }
    }
}
