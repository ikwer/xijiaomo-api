<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
	    echo 'this is a techer page';
	    exit;
		$this->load->view('welcome_message');
	}
	
	public function test2(){
	    $file = fopen("test3.txt","r+");
	    
	    if (flock($file,LOCK_EX | LOCK_NB))
	    {
	        fwrite($file,"Write something");
	        sleep(10);
	        flock($file,LOCK_UN);
	    }
	    else
	    {
	        echo "Error locking file!";
	    }
	    
	    fclose($file);
	}
}
