<?php
/**
 * Description of m_user
 *
 * @author acer
 */
class Auto_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        define('alipay_rsa_public','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2JxZD6CSTYkGWjUC54BA6t9VTLvTaTQHngPoEwV/sHZDT65r3GtMGNL5H/QkwSYhb7MzaGVECC6FdeQKDaxNxguSjZpC3NU8mHLLx1L4WLptjbbJeWbTE4ZhZOD7jSJ5C9UKvTTiKpbzSGRr0m3D2Ad04yUPBA2vHihGKozjrISFkDzzOplNQU2F7W/ZL2NPczD2hCJ6oCwjtIUAQJNfM2CAZR7U2j6v3eN5O4WD4cuvDlc8PkUlo+lFvqHMr1jrjsqd4eRIZg97cD/Vr/3WxmBwo0AH8BIoDTYB3N4JhIB7IONNrAp4NssRGPse1cT4hTVUV11SxgejEPryjGA7KQIDAQAB');
        define('alipay_rsa_private','MIIEpQIBAAKCAQEA09fNeZc74rE7s+LG07YvO1KzisDPAQLG3oJhYstviXhe3UU8egRNnc+XFwrmQozlXffO2dOnXh/TQWJy2HaCO71tndK32rBH2qzA6+Hvv6xiQaDw8xv6E3GYj/6awYFhyciRd0fmv5oQUfNvvCUIqkbycK5dT4fPyAGLIGpfZCylP7qIG/6duhZ+/8LV4eoTfqxSaGa/KMwDqe5N7cFw3ftO769mK3APO+F37v+hKWPaQ4eYksPANfiN7bPy8n+kB6d+FbPqyojdyWodD8hYRg8TR7jcUPkWsC64iTQL5Lcw/pe7ojR9eNR/DADUzQAwWXnv3zhe2xfRfRQQnm4rkwIDAQABAoIBADt+HoloLHxEc9TX+iiYwHjzh9KOxVOuWPVCPmFFaqR7toCDojFOJhIMq4zgFbxY301de6z24MjxPR3erQwXdAmc8DzjYZi76uIBpAhaoj79BXcS4LgVgkyVmVfPE90GHdVSS2/pNGdYt/6b1CRNbjqLxCha/3+HT5YGwPCwjuuNj2mX3zLCWtLgedf1lnas/lu2RDxElkuRhOsIIo5YXJdZhWIY/2zwqIBndgTDwptfWVSLEnvYo4hyDvQFkFnZGfvwtI2c12lmY+ExM400uDXFJIN5kIIGBgcaPVQ/n8wJykBkVyeyqNm5Ffhn5zjilx5ZdFGB9zvldq4iVckpU7ECgYEA6Y9jX1Ndyh9gClXGqCp4P9g0D8XMluZjZEeCmtwksUjvaLNCAB/yqB09habUNKjxgo1TBQuLEZPvEmUGGNwYN3BQ9aJGbznmI+ILoq2GR2jZXM0PDMW5ER5qZSVob/d6FLZbVZRm4EqASfFoCsgcHi9BzhlEQ1H1nzvghFLBFpkCgYEA6DJDUE4YBVc09uBg0fQYuToNYdqhk7TQoRn+krkb2iOWSU2ZmFBW7HP5abe2CdPcclXn7AUj6Qz6JiTRO1z5KvPoksuCcY+U0CL4GHT55hmnkfHg59JDbcaeTkOxg8HR2iQehyEubcJL6/nevnakumX3KpeeKnLFjXac2y6aqwsCgYEAzT4DDkbdOXw+0nY0H51KlJgyj9W7BrQojXvFyr4/xEcak4BLNH7ep1sisCs9eZUovhfg56MQL383bIu4QupOoZEio+hZSu6vTcMbhHZGdMQvlrxgSFIMYn2+82lfEF2CO2dQdbD2go0VlWT9j9Pv2ZqfkjRj52DDno1oq/9ozKkCgYEA1S5gkvhAIZchb5AuFFUx5c2gv7jFJCGccmy1R+xf3/VQY9i1LhyE0e7gjOk4Xul+uhKZLh7CC5P4jtC6sO/5bDAn1a63AqA6lqWkdn/feB0RtnMGdJCdi8oRSfXoovluPANxa8tRH0CGCA+PK/st3l0Dgr1VX8+kBO9jr/Cn3GUCgYEAzlgaLfaaopFXyPbKmnzMA0R7eTGMIgTJwhWwVlr5OBYAI2z3MQu8kj0ZFCFqJWavTlxbxGOPC3uwJZxXhjf1GviJYTzlsvXlyeZeQzy3bdi+1an2QsBHQZZRPU4u+A7tdy1Rg91MONZmTkthZdVR+Q2KHLZkzhcYNn178kghVGk=');
        define('gatewayUrl','https://openapi.alipay.com/gateway.do');
        define('format','json');
        define('charset','UTF-8');
        define('signType','RSA2');
        define('appId','2017101809366484');

    }

    //自动退款
    public function auto_refund($time)
    {
        $query=$this->db->select('f.id,f.order_id,f.order_sn,o.trade_no,f.refund_money,f.refund_reason,f.goods_price')->from('xjm_refund f')->join('xjm_order_list o','f.order_id=o.order_id','left')->where('f.status>=0 AND f.status<2 AND f.add_time<='.$time)->get();
        $result=$query->result_array();
//        echo $this->db->last_query();die;
        $data['status']=3;
        $order_data['order_status']=4;




        if(empty($result))
        {
            $arr=array('code'=>-1,'msg'=>'无可用订单');
        }
        else
        {
            foreach ($result as $k=>$v)
            {
                //调用支付宝退款接口
                $alipay_refund=$this->alipay_refund($v);
                $refund_json=json_decode($alipay_refund,true);
                if($refund_json['code']==10000)
                {
                    $this->db->trans_begin();

                    $str1=$this->db->where('id='.$v['id'])->update('xjm_refund',$data);

                    $str2=$this->db->where('order_id='.$v['order_id'])->update('xjm_order_list',$order_data);



                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                        $arr=array('code'=>-1,'msg'=>$v['order_sn'].'修改失败');
                    }
                    else
                    {
                        $this->db->trans_commit();
                        $arr=array('code'=>0,'msg'=>$v['order_sn'].'修改成功');
                    }


                }
                else
                {
                    $arr=array('code'=>-1,'msg'=>$v['order_sn'].'退款失败');
                }
            }
        }

        echo json_encode($arr);


    }


    //支付宝退款
    private function alipay_refund($order)
    {
        require_once('/usr/share/nginx/html/api/system/libraries/Pay/Alipay/aop/AopClient.php');
        require_once('/usr/share/nginx/html/api/system/libraries/Pay/Alipay/aop/request/AlipayTradeRefundRequest.php');
        $aop = new AopClient ();
        $aop->gatewayUrl = gatewayUrl;
        $aop->appId = appId;
        $aop->rsaPrivateKey = alipay_rsa_private;
        $aop->format = format;
        $aop->charset = charset;
        $aop->signType = signType;
        $aop->alipayrsaPublicKey = alipay_rsa_public;

        $request = new AlipayTradeRefundRequest ();


        //部分退款
        if($order['refund_money']<$order['goods_price'])
        {
            $json=json_encode([
                'out_trade_no'=>$order['order_sn'],
                'trade_no'=>$order['trade_no'],
                'refund_amount'=> $order['refund_money'],
                'refund_reason'=>$order['refund_reason'],
                'out_request_no'=>$order['order_sn'].time(),
            ]);
        }
        else
        {
            $json=json_encode([
                'out_trade_no'=>$order['order_sn'],
                'trade_no'=>$order['trade_no'],
                'refund_amount'=> $order['refund_money'],
                'refund_reason'=>$order['refund_reason'],
            ]);
        }

        $request->setBizContent($json);
        $result = $aop->execute ( $request);
        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $resultCode = $result->$responseNode->code;
        $resultMsg = $result->$responseNode->msg;
        $arr=array('code'=>$resultCode,'msg'=>$resultMsg);
        return json_encode($arr);
    }
    
    
    //七天自动好评
    public function auto_goodPraise($date)
    {
        $result=$this->db->select('o.uid,o.tid,o.order_sn,o.goods_name,u.nickname,u.headurl,o.end_time,u.mobile')->from('xjm_order_list o')->join('xjm_user u','o.uid=u.uid','left')->where('o.order_status=2')->get()->result_array();
        if(empty($result))
        {
            echo "无可用订单";die;
        }
        else
        {
            foreach ($result as $k=>$v)
            {
                //如果结束时间为空
                if(empty($v['end_time']))
                {
                    echo "无效订单\n";
                }
                else
                {
                    $search=$this->db->select('id')->from('xjm_user_comment')->where(array('order_id'=>$v['order_sn']))->limit(1)->get()->row_array();

                    if(!empty($search))
                    {
                        echo "该订单已经自动好评";
                    }
                    else
                    {
                        if($v['end_time']<=$date)
                        {
                            $data=array(
                                'type'=>0,
                                'anonymity'=>0,
                                'content'=>'系统自动好评',
                                'uid'=>$v['uid'],
                                'tid'=>$v['tid'],
                                'order_id'=>$v['order_sn'],
                                'goods_name'=>$v['goods_name'],
                                'nick_name'=>$v['nickname'],
                                'headurl'=>$v['headurl'],
                                'mobile'=>$v['mobile'],
                                'add_time'=>time()
                            );
                            $this->db->insert('xjm_user_comment',$data);
                            echo '评论ID：'.$this->db->insert_id().' ';
                        }
                    }

                }
            }
        }
    }
    
    
    
    //十分钟改变接单状态
    public function auto_offline()
    {
        $result=$this->db->select('tid,last_time')->from('xjm_techer')->where(array('is_online'=>0))->get()->result_array();
        if(!empty($result)){
            foreach ($result as $k=>$v)
            {
                $time_cha=time()-$v['last_time'];
                if($time_cha>180)
                {
                    $data['is_online']=1;
                    $r=$this->db->where(array('tid'=>$v['tid']))->update('xjm_techer',$data);
                    echo $r;
                }
            }
        }
        else{
            echo "暂无处理";
        }

    }
    
    
    //自动取消
    public function auto_quxiao()
    {
        //即时上门
        $type1_sql='SELECT * FROM ( SELECT o.tid,o.order_sn,o.order_amount,(o.server_time*60+o.add_time+1800) daoqi,o.uid 
FROM xjm_order_list o
LEFT JOIN xjm_user u
ON o.uid=u.uid
WHERE pay_status=1 and order_status IN (0,5) and date_type=0
ORDER BY order_id DESC ) a 
WHERE a.daoqi<'.time();
        $type1=$this->db->query($type1_sql)->result_array();

      //预约订单
        $type2_sql=' SELECT * FROM ( SELECT o.tid,o.order_sn,o.order_amount,(o.server_time*60+o.add_time+1800) daoqi,o.uid 
FROM xjm_order_list o
LEFT JOIN xjm_user u
ON o.uid=u.uid
WHERE pay_status=1 and order_status IN (0,5) and date_type!=0
ORDER BY order_id DESC ) a 
WHERE a.daoqi<'.time();

        $type2=$this->db->query($type2_sql)->result_array();

        if(!empty($type1)){
            foreach ($type1 as $k=>$v){
                $array=array(
                    'user_id'=>$v['uid'],
                    'order_sn'=>$v['order_sn'],
                    'refund_desc'=>'技师未按时到达',
                    'refund_note'=>'系统自动申请退款',
                    'refund_money'=>$v['order_amount']
                );
                $this->db->where(['tid'=>$v['tid']])->update('techer',array('is_lock'=>0));
                $steam=$this->refundAdd($array);
                echo $steam;
            }
        }elseif(!empty($type2)){
            foreach ($type2 as $k=>$v){
                $array=array(
                    'user_id'=>$v['uid'],
                    'order_sn'=>$v['order_sn'],
                    'refund_desc'=>'技师未按时到达',
                    'refund_note'=>'系统自动申请退款',
                    'refund_money'=>$v['order_amount']
                );

                $steam=$this->refundAdd($array);
                echo $steam;
            }
        }else{
            echo "无可用订单";
        }

    }


    //提交退款
    function refundAdd($param){
        if(empty($param['order_sn']) || empty($param['refund_money']) || empty($param['refund_desc'])){
            ajax_return(0,'缺少重要参数');
        }
        $order_id = $param['order_sn'];
        $uid = $param['user_id'];
        $refund_money = $param['refund_money'];
        $refund_desc =  $param['refund_desc'];
        $data['refund_note'] =  isset($param['refund_note'])?$param['refund_note']:'好评';
        //$data['refund_note'] =  $this->input->post('refund_note');
        $rs = $this->db->select('order_id,goods_name,goods_price,tid,goods_id,server_time')->from('order_list')->where(['order_sn'=>$order_id])->get();
        $orderInfo = $rs->row_array();
        $data['refund_money'] =  $refund_money;
        $data['goods_name'] =  $orderInfo['goods_name'];
        $data['order_id'] =  $orderInfo['order_id'];
        $data['order_sn'] =  $order_id;
        $data['tid'] =  $orderInfo['tid'];
        $data['uid'] =  $uid;
        $data['goods_id'] =  $orderInfo['goods_id'];
        $data['goods_price'] =  $orderInfo['goods_price'];
        $data['server_time'] =  $orderInfo['server_time'];
        $data['refund_reason'] = $refund_desc;
        $data['add_time'] = time();
        $update['order_status'] = 7;
          $this->db->where(['order_sn'=>$order_id])->update('order_list',$update);
        if($this->db->from('refund')->where(['uid'=>$uid,'order_id'=>$orderInfo['order_id']])->count_all_results() <3){
            $data['status'] =  1;
            if($this->db->insert('refund',$data)){
              
                ajax_return(0,'提交成功');
            }else{
                ajax_return(-1,'提交失败');
            }
        }else{
            ajax_return(-1,'您已经提交过三次了');
        }
    }



    //15天自动分享奖励
    public function auto_reward()
    {
        //当前15天前
        $time=time()-1296000;
        $where=array('t.status'=>1,'i.check_time>='=>$time);
        $query=$this->db->select('t.pid,t.money')->from('techer t')->join('techer_checkinfo i','t.tid=i.tid','left')->where($where)->group_by('t.pid')->get()->result_array();
//        echo $this->db->last_query();die;
        if(!empty($query)){
            if(count($query)>20){
                echo "超过了限额";
            }else{

                foreach ($query as $k=>$v)
                {
                    $array=array(
                        'tid'=>$v['pid'],
                        'goods_name'=>'分享奖励',
                        'change_money'=>5,
                        'from'=>1,
                        'add_time'=>time(),
                        'money'=>$v['money']+5,
                    );

                    //插入技师收入表
                    $in1=$this->db->insert('techer_income',$array);

                    //修改技师余额
                    $arr=array('money'=>$v['money']+5);
                    $wh=array('tid'=>$v['pid']);
                    $in2=$this->db->where($wh)->update('techer',$arr);

                    //增加消息通知
                    $ar=array(
                        'tid'=>$v['pid'],
                        'type'=>0,
                        'common_id'=>3,
                        'title'=>'您的分享奖励已到帐',
                        'content'=>'您的分享5元奖励已到帐',
                        'add_time'=>time(),
                        'is_read'=>0,
                        'read_time'=>0
                    );
                    $in3=$this->db->insert('techer_notice',$ar);
                    echo $in1.'<br />'.$in2.'<br />'.$in3;
                }




            }
        }else{
            echo "无可用技师";
        }
    }

}

?>
