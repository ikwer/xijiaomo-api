<?php
/**
 * Description of t_order
 *
 * @author acer
 */
class Order_v2_model extends CI_Model {
    public function __construct(){
        parent::__construct();
    }
    
    /**
     * 获取所有订单列表
     * @param unknown $tid
     * @param unknown $order_status
     */
    public function getAllOrder($tid, $order_status){
        $orders_list = $this->mongodb->select("*")->where(array('tid'=>$tid))->limit(21)->get('order_list');
        $data = array();
        $rss = array();
        $rs = array();
        if ($orders_list) {
            foreach ($orders_list as $k=>$v){
                $rs['order_sn']     = $v['order_sn'];
                $rs['order_status'] = $v['order_status'];
                $rs['pay_status']   = $v['pay_status'];
                
                $rss[] = $rs;
            }
            $data['status'] = true;
            $data['msg'] = '返回成功';
            $data['data'] = $rss;
        } else {
            $data['status'] = false;
            $data['msg'] = '暂无数据';
        }
        return $data;
    }
    
    /**
     * 获取当前订单详情
     * @param unknown $tid
     * @param unknown $order_id
     */
    public function getOrderDetail($tid, $order_id){
        $orders_list = $this->mongodb->select("*")->where(array('tid'=>$tid,'order_sn'=>$order_id))->limit(21)->get('order_list');
        $data=array();
        $rss = array();
        $rs = array();
        if ($orders_list) {
            foreach ($orders_list as $k=>$v){
                $rs['order_sn']     = $v['order_sn'];
                $rs['order_status'] = $v['order_status'];
                $rs['pay_status']   = $v['pay_status'];
                $rs['mobile'] = $v['mobile'];
                $rs['goods_name'] = $v['goods_name'];
                $rs['goods_price'] = $v['goods_price'];
                $rs['goods_image'] = $v['goods_image'];
                $rs['server_time'] = $v['server_time'];
                $rs['add_time'] = date('Y-m-d H:i:s',$v['add_time']);
                $rss[] = $rs;
            }
            $data['status'] = true;
            $data['msg'] = '返回成功';
            $data['data'] = $rss;
        } else {
            $data['status'] = false;
            $data['msg'] = '暂无数据';
        }
        return $data;
    }
    
    
    
    //订单详情
    public function order_info($tid,$order_id)
    {
        $where=array('o.order_id'=>$order_id);
        $result=$this->db->select('u.nickname,u.mobile,o.address,o.user_note,goods_name,server_time,o.u_lon,o.u_lat')->from('xjm_order_list o')->join('xjm_user u','o.uid=u.uid','left')->where($where)->limit(1)->get()->row_array();
        if(empty($result))
        {
            $data['status']=false;
            $data['msg']='无订单记录';
        }
        else
        {
            $data['status']=true;
            $data['msg']='数据获取成功';
            $data['data']=$result;
        }
        return $data;
    }
    
    
    /**
     * 确认服务已到达,技师开始服务
     * @param unknown $tid
     * @param unknown $order_id
     */
    public function start_work($tid,$order_id){


        $query=$this->db->select('order_status,pay_status')->from('order_list')->where(array('order_id'=>$order_id,'tid'=>$tid))->get();
        $result=$query->row_array();
        //当订单状态为0和付款状态为1时可以操作订单
        if( empty($result) || $result['pay_status']!=1 || $result['order_status']!=0)
        {
            $data['status'] = false;
            $data['msg'] = '订单异常';
        }

        else
        {
            $update=array();
            $update['order_status']=5;
            $change_order = $this->db->where(array('order_id'=>$order_id))->update('order_list', $update);

            if($change_order)
            {
                $data['status'] = true;
                $data['msg'] = '开始服务';
            }
            else
            {
                $data['status'] = false;
                $data['msg'] = '操作异常';
            }

        }
        return $data;

    }


    /**
     * 未完成订单
     * @param $tid
     * @return mixed
     */
    public function no_finish_order($tid)
    {
        
        $query=$this->db->query('SELECT a.tid,a.address,a.order_id,a.order_sn,a.mobile,a.user_note,a.u_lat,a.u_lon,a.nickname FROM 
( SELECT o.tid,o.address,o.order_id,o.order_sn,o.mobile,o.user_note,o.u_lat,o.u_lon,u.nickname,(o.server_time+o.add_time) daoqi,o.add_time
FROM xjm_order_list o
LEFT JOIN xjm_user u
ON o.uid=u.uid
WHERE tid='.$tid.' and pay_status=1 and order_status=0 and date_type=0
ORDER BY order_id DESC ) a WHERE a.daoqi>'.time());
        
        // $query=$this->db->select('o.tid,o.address,o.order_id,o.order_sn,o.mobile,o.user_note,o.u_lat,o.u_lon,u.nickname')->
        // from('order_list o')->
        // join('xjm_user u','o.uid=u.uid','left')->
        // where("tid=$tid and pay_status=1 and order_status=0 and date_type=0")->
        // order_by('order_id DESC')->
        // get();
        $result=$query->row_array();

        $sex=$this->db->select('sex')->from('techer_checkinfo')->where(array('tid'=>$tid))->get()->row_array();
        $h=date('G');

        if(empty($result))
        {
            $re=$this->db->select('status')->from('techer')->where(array('tid'=>$tid))->get()->row_array();
            if($sex['sex']==2){
                if($h<8 || $h>20){
                    $re['status']='3';
                }
            }
            $data['status'] = false;
            $data['msg'] = '无未完成订单';
            $data['data']=$re;
        }
        else
        {
            $data['status'] = true;
            $data['msg'] = '您还有未完成的订单';
            if($sex['sex']==2){
                if($h<8 || $h>20){
                    $result['status']='3';
                }
            }
            else
            {
                $result['status']='1';
            }
            $data['data']=$result;
        }

        return $data;
    }




    /**
     * 检测技师是否可以开始服务
     * @param $tid
     * @param $order_id
     * @param $order_sn
     * @return mixed
     */
    public function location_range($tid,$order_id,$u_lon,$u_lat)
    {
        $query=$this->db->select('pay_status,order_status,u_lon,u_lat')->from('order_list')->where(array('tid' => $tid,'order_id'=>$order_id))->get();
        $result=$query->row_array();
        if(empty($result) || $result['pay_status']!=1 || $result['order_status']!=0)
        {
            $data['status'] = false;
            $data['msg'] = '订单异常';
        }
        else
        {

            $distance = sphere_distance($result['u_lat'], $result['u_lon'], $u_lat, $u_lon);
            $juli=abs($distance);
            if($juli<0.2){
                $data['status'] = true;
                $data['msg'] = '技师可以开始服务';
            }else{
                $data['status'] = false;
                $data['msg'] = '还未到达订单指定范围';
            }
        }
        return $data;
    }


    /**
     * 确认服务已完成
     * @param unknown $tid
     * @param unknown $order_id
     */
    public function do_confirm_finish($tid, $order_id,$order_sn){
        $query=$this->db->select('pay_status,order_status')->from('order_list')->where(array('tid' => $tid,'order_sn'=>$order_sn,'order_id'=>$order_id))->get();
        $result=$query->row_array();
        if(empty($result) || $result['pay_status']!=1 || $result['order_status']!=5)
        {
            $data['status'] = false;
            $data['msg'] = '订单异常';
        }
        else
        {
            $update=array();
            $update['order_status']=2;
            $change_order = $this->db->where(array('tid' => $tid,'order_sn'=>$order_sn,'order_id'=>$order_id))->update('order_list', $update);
            
            //即时上门解锁
            if($result['date_type']==0){
                $lock=array('is_lock'=>0);
                $is_lock=$this->db->where(array('tid' => $tid))->update('techer', $lock);
            }

            if($change_order)
            {
                $data['status'] = true;
                $data['msg'] = '服务已完成';
            }
            else
            {
                $data['status'] = false;
                $data['msg'] = '操作异常';
            }
        }
        return $data;
    }

    //技师同意/不同意退款操作
    public function teacher_refund($tid,$refund_id,$is_success)
    {
        //事务开始
        $this->db->trans_begin();

        //查找order——id
        $refund_query=$this->db->select('order_id,tid')->from('refund')->where('id='.$refund_id)->limit(1)->get();
        $refund_row=$refund_query->row_array();

        if(empty($refund_row))
        {
            $data['status']=false;
            $data['msg']='数据异常';
        }
        else
        {
            //如果申请退款次数大于3次就不能再申请
            $search=$this->db->select('order_id')->from('refund')->where('order_id='.$refund_row['order_id'])->get();
            $check_num=$search->num_rows();

            if($check_num>2)
            {
                $data['status']=false;
                $data['msg']='申请退款次数超额';
            }
            else
            {
                $update=array();
                //0已确认，2已拒绝
                ($is_success==0) ? $update['status']=0 : $update['status']=2;
                $this->db->where(array('id' => $refund_id))->update('refund', $update);

                 if($is_success==0){
                    $order['order_status']=8;
                    $te=array('is_lock'=>0);
                    $this->db->where(array('tid'=>$refund_row['tid']))->update('techer',$te);
                 }elseif ($is_success==1){
                     $order['order_status']=9;
                 }

                 $this->db->where(array('order_id'=>$refund_row['order_id']))->update('order_list',$order);
                $data['status']=true;
                ($is_success==0)?$data['msg']="已确认":$data['msg']="已拒绝";

            }
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }

        return $data;

    }


    /**
     * 订单列表
     * @param $tid
     * @return mixed
     */
    public function order_list($tid)
    {
        $tid=intval($tid);
        $query=$this->db->query('SELECT
            o.order_id,
            o.order_sn,
            o.goods_name,
            o.order_amount,
            g.goods_image,
            o.order_status,
            o.pay_status,
            u.nickname,
            u.mobile,
            FROM_UNIXTIME(o.add_time,\'%Y-%m-%d %H:%i:%s\') addtime
        FROM
            xjm_order_list o
        JOIN xjm_goods g ON o.goods_id = g.goods_id
        JOIN xjm_user u ON o.uid = u.uid
        WHERE
            o.tid = '.$tid.'
        AND pay_status = 1');

        if(empty($query->result_array()))
        {
            $data['status']=false;
            $data['msg']='无订单记录';
        }
        else
        {
            $data['status']=true;
            $data['msg']='数据获取成功';
            $data['data']=$query->result_array();

        }
        return $data;

    }
    
    
     /**
     * 退款列表
     * @param $tid
     * @return mixed
     */
    public function refund_list($tid,$page)
    {
        $limit=8;
        ($page==0) ? $start=0 : $start=$page*$limit-$limit;
        //$where=array('f.tid'=>intval($tid),'f.status!='=>'3','f.status!='=>'1');
        $query=$this->db->select('f.id,f.order_id,f.order_sn,f.goods_name,o.mobile,f.refund_reason,f.`status`,f.refund_money,c.headurl,o.xiaofei')->
        from('refund f')->
        join('order_list o','f.order_id=o.order_id','left')->join('user c','f.uid=c.uid','left')->
        where('f.tid='.intval($tid).' AND f.status=0 AND f.status=2')->limit($limit,$start)->
        order_by('f.`status` ,f.add_time DESC')->get();
//        echo $this->db->last_query();die;
        $result=$query->result_array();


        $where2=array('f.tid'=>intval($tid),'f.status'=>'1');
        $query2=$this->db->select('f.id,f.order_id,f.order_sn,f.goods_name,c.mobile,f.refund_reason,f.`status`,f.refund_money,c.headurl,o.xiaofei')->
        from('refund f')->
        join('order_list o','f.order_id=o.order_id','left')->join('user c','f.uid=c.uid','left')->
        where($where2)->limit($limit,$start)->
        order_by('f.`status` ,f.add_time DESC')->get();
        $result2=$query2->result_array();

        $merge=array_merge($result2,$result);

        if(empty($merge))
        {
            $data['status']=false;
            $data['msg']='无订单记录';
        }
        else
        {
            foreach($merge as $k=>$v)
            {
                (empty($v['headurl'])) ? $merge[$k]['headurl']='http://oxll5q098.bkt.clouddn.com/wo.png' : $merge[$k]['headurl']=$v['headurl'].'?imageMogr2/thumbnail/!640x300r/gravity/Center/crop/300x300/format/jpg/blur/1x0/quality/75|imageslim';
                //技师看到的金额是实际金额的80%
                $merge[$k]['refund_money']=$v['refund_money']*0.8+$v['xiaofei'];
            }
            $data['status']=true;
            $data['msg']='数据获取成功';
            $data['data']=$merge;
        }
        return $data;
    }
    
    
    //刷新订单，返回订单状态
    public function reflush_order($tid,$order_id)
    {
        $query=$this->db->select('order_status')->from('order_list')->where(array('tid'=>$tid,'order_id'=>$order_id))->get()->row_array();
        if(!empty($query)){
            if($query['order_status']==2){
                $data['status']=true;
                $data['msg']='订单已完成';
            }else{
                $data['status']=false;
                $data['msg']='订单未完成';
            }
        }else{
            $data['status']=false;
            $data['msg']='订单不存在';
        }

        return $data;

    }
    
    
    
    //我的订单列表
    public function my_order_list($tid,$type,$page)
    {

        $limit=8;
        ($page==0) ? $start=0 : $start=$page*$limit-$limit;

        if($type==0)
        {
            $where='( o.`order_status` = 5 OR o.`order_status` = 6 OR o.`order_status` = 0 ) AND o.tid='.$tid.' AND o.pay_status=1';
        }
        elseif($type==2)
        {
            $where=' o.`order_status` = 2   AND o.tid='.$tid.' AND o.pay_status=1';
        }
        elseif($type==3)
        {
            $where='( o.`order_status` = 3 OR o.`order_status` = 4 ) AND o.tid='.$tid.' AND o.pay_status=1';
        }
        else
        {
            $where=array('o.order_status'=>$type,'o.tid'=>$tid,'o.pay_status'=>1);
        }



        $result=$this->db->select('o.order_id,o.order_sn,address,o.u_lon,o.u_lat,o.uid,o.user_note,o.mobile,o.goods_name,o.order_amount,o.date_type,o.date_time yytime ,FROM_UNIXTIME(o.add_time,"%Y-%m-%d %H:%i") addtime,o.order_status,g.goods_image,c.content,o.xiaofei')->
        from('order_list o')->
        join('goods g','o.goods_id=g.goods_id','left')->
        join('user_comment c','o.order_sn=c.order_id','left')->
        where($where)->
        limit($limit,$start)->
        order_by('o.date_type','ASC')->
        order_by('o.add_time','DESC')->
        get()->
        result_array();
//        echo $this->db->last_query();die;
        if(empty($result))
        {
            $data['status']=false;
            $data['msg']='无订单记录';
        }
        else
        {
//            var_dump($result);die;
            foreach($result as $k=>$v)
            {

                  

                //$result[$k]['address']=trim($v['address']);
                ($v['date_type']!=0 && $v['yytime']!=0 )? $result[$k]['yytime']=date('Y-m-d H:i',$v['yytime']) : $result[$k]['yytime']=$v['addtime'];
                ($v['content']==null)? $result[$k]['content']="" : $result[$k]['content']=$v['content'];

                $yy=strtotime($result[$k]['yytime'])-1800;
                $yy=date('Y-m-d H:i',$yy);    



                if($v['order_status']==5 || $v['order_status']==6){
                    $result[$k]['order_status']=0;
                    $result[$k]['date_type']=3;
                }
                elseif($v['order_status']==9 ){
                    $result[$k]['order_status']=2;
                }
                elseif($v['order_status']==4){
                    $result[$k]['order_status']=3;
                }elseif($type==0  && date('Y-m-d H:i')>=$yy){
                    $result[$k]['date_type']=0;
                }
                $result[$k]['goods_image']=$v['goods_image'].'?imageMogr2/thumbnail/!640x300r/gravity/Center/crop/300x300/format/jpg/blur/1x0/quality/75|imageslim';
                //计算小费
                $result[$k]['order_amount']=$v['order_amount']*0.8+$v['xiaofei'];
            }
//            var_dump($result);die;
            $data['status']=true;
            $data['msg']='数据获取成功';
            $data['data']=$result;
        }
        return $data;
    }
    
    
    /**
     * 退款详情
     * @param $tid
     * @param $order_id
     * @return mixed
     */
    public function refund_info($tid,$refund_id)
    {
        $where=array('r.id'=>$refund_id);
        $query=$this->db->select('r.id,r.order_id,r.order_sn,r.goods_name,r.goods_price,r.refund_money,r.refund_reason,r.refund_note,r.add_time,FROM_UNIXTIME(r.add_time,\'%Y-%m-%d %h:%i:%s\') refundTime,r.server_time,r.`status`,o.xiaofei')->
            from('refund r')->
            join('order_list o','o.order_id=r.order_id','left')->
            where($where)->
            limit(1)->get();
        $result=$query->row_array();
        if(empty($result))
        {
            $data['status']=false;
            $data['msg']='无订单记录';
        }
        else
        {

            $daoqi=$result['add_time']+86400+86400;
            if(time()>$daoqi)
            {
                $time="0天0小时0分";
            }
            else
            {
                $time_chuo=$daoqi-time();
                $days = intval($time_chuo/86400);
                //计算小时数
                $remain = $time_chuo%86400;
                $hours = intval($remain/3600);
                //计算分钟数
                $remain = $remain%3600;
                $mins = intval($remain/60);

                $time=$days.'天'.$hours.'小时'.$mins.'分';




            }
            $result['exprise']=$time;
            $result['add_time']=date('Y-m-d H:i:s',$result['add_time']);

            //商品单价和退款金额只显示80%
            $result['goods_price']=$result['goods_price']*0.8;
            $result['refund_money']=$result['refund_money']*0.8+$result['xiaofei'];

            $data['status']=true;
            $data['msg']='数据获取成功';
            $data['data']=$result;


        }
        return $data;


    }

}
