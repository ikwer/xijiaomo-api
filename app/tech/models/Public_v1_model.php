<?php

/**
 * Description of m_user
 *
 * @author acer
 */
class Public_v1_model extends CI_Model {

    //true成功  false 失败
    private $results = array(
        'status' => false,
        'msg' => '',
        'data' => array()
    );

    //短信发送存档
    function inset_sms_code($mobile, $code,$type) {
        //120秒发一次
        $check_sql = "SELECT * FROM xjm_sms_code WHERE mobile = '$mobile' ORDER BY sendtime DESC LIMIT 1";
        $check_rs = $this->db->query($check_sql)->row_array();
        $cool_time = time() - $check_rs['sendtime'];
        if ($cool_time < 10) {
            $this->results['msg'] = '短信发送过快,请稍后再试';
            return $this->results;
        }
        $inset['mobile'] = $mobile;
        $inset['code'] = $code;
        $inset['sendtime'] = time();
        $inset['type']=$type;
        $rs = $this->db->insert('sms_code', $inset);
        if ($rs) {
            $this->results['status'] = true;
            $this->results['data'] = array('send_id' => $this->db->insert_id());
        } else {
            $this->results['msg'] = '短信发送失败';
        }
        return $this->results;
    }

    /**
     * 返回短信检验结果
     * @param type $mobile 
     * @param type $id 短信验证id
     * @param type $code  验证码
     */
    function check_phone_code($mobile, $code_id, $code) {
        $where['mobile'] = $mobile;
        $where['id'] = $code_id;
        $where['code'] = $code;
        //2分钟以前的无效
        $send_time = time() - 5000000;
        $check_send = $this->db->select('id')->from('sms_code')->where($where)->where('sendtime >', $send_time)->get();
        if ($check_send->row_array()) {
            $this->results['status'] = true;
        } else {
            $this->results['status'] = false;
        }
        return $this->results;
    }
    
    //公告分页
    function unread($tid, $now_page) {
          --$now_page;
        $page_num = 12;
        $notice = $this->db->select('id as m_id,title,is_read,content')->from('techer_notice')
                ->where('tid', $tid)
                ->limit($page_num, $now_page * $page_num)
                ->order_by('is_read, add_time DESC')
                ->get();
        $all_notice = $notice->result_array();
        $all_count=$this->db->select('id')->from('techer_notice')
            ->where('tid', $tid)->get()->num_rows();
        $arr=array('tid'=>$tid,'is_read'=>0);
        $weidu=$this->db->select('id')->from('techer_notice')
            ->where($arr)->get()->num_rows();
        if(empty($all_notice)){
            $this->results['status'] = FALSE;
        }else{
            $this->results['status'] = TRUE;
            $this->results['data'] = array('list'=>$all_notice,'count'=>$all_count,'page_num'=>$page_num,'weidu'=>$weidu);
        }    
            
        
        return $this->results;
    }

    //读信息
    function change_msg($tid, $msg_id) {
        $where['id'] = $msg_id;
        $where['tid'] = $tid;
        $save['is_read'] = 1;
        $save['read_time'] = time();
        $this->db->where($where);
        $this->db->update('techer_notice', $save);
        $rs = $this->db->affected_rows();
        if ($rs) {
            $this->results['status'] = TRUE;
        } else {
            $this->results['status'] = FALSE;
        }
        return $this->results;
    }

    //插入公共信息
    function notice($tid) {
        //获取1个月内所有以插入公告信息
        $time = time() - 2592000;
        $notice = $this->db->select('id,common_id')->from('techer_notice')
                ->where('tid', $tid)
                ->where('add_time >', $time)
                ->where('type', 1)
                ->get();
        $all_notice = $notice->result_array();
        $where = '';
        $all_id = '';
        if (count($all_notice) > 0) {
            foreach ($all_notice as $key => $value) {
                $all_id .= "'" . $value['common_id'] . "'" . ',';
            }
            $all_id = trim($all_id, ',');
            $where = "AND `id` NOT IN($all_id)";
        }
        //将1个月内所有未插入的公告通告找出
        $sql = "SELECT `id`, `title`, `content`, `add_time` FROM `xjm_notice_pubilc` WHERE `add_time` > $time $where";
        $check_send = $this->db->query($sql);
        $uninset_notice = $check_send->result_array();
        $inset_rs = TRUE;
        if (count($uninset_notice) > 0) {
            //开始插入未插入公告
            $inset_data = array();
            foreach ($uninset_notice as $key => $value) {
                $inset_data[$key]['tid'] = $tid;
                $inset_data[$key]['type'] = 1;
                $inset_data[$key]['common_id'] = $value['id'];
                $inset_data[$key]['title'] = $value['title'];
                $inset_data[$key]['content'] = $value['content'];
                $inset_data[$key]['add_time'] = $value['add_time'];
            }
            $inset_rs = $this->db->insert_batch('techer_notice', $inset_data);
            if (!$inset_rs) {
                //保存错误日志
                $error_sql = $this->db->last_query();
                $time = date('Y-m-d H:i;s');
                log_message('error', '更新公用公告到技师公告表失败时间：' . $time . '用户id为：' . $tid . '执行sql为：' . $error_sql);
                $this->results['status'] = FALSE;
                return $this->results;
            }
        }
        $all_num = $this->db->where("tid = $tid AND is_read = 0")->count_all_results('techer_notice');
        $this->results['status'] = TRUE;
        $this->results['data'] = $all_num;
        return $this->results;
    }

}
