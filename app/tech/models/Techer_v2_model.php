<?php

/**
 * Description of m_user
 *
 * @author acer
 */
class Techer_v2_model extends CI_Model {

    //true成功  false 失败
    private $results = array(
        'status' => false,
        'msg' => '',
        'data' => array()
    );
    
    function __construct() {
        parent::__construct();
    }
    


    public function techer_Invitation_list($tid)
    {
        $sql='SELECT IFNULL(SUM(change_money),0) money_sum FROM xjm_techer_income WHERE tid='.intval($tid).' AND goods_name="分享奖励"';
//        echo $sql;die;
        $query=$this->db->query($sql)->row_array();

        $result=$this->db->select('mobile,FROM_UNIXTIME(add_time,"%Y-%m-%d") addtime')->from('techer')->where(array('pid'=>$tid,'status'=>1,'source'=>"share"))->order_by('add_time')->get()->result_array();
        if(!empty($result)){
            foreach ($result as $k=>$v){
                $result[$k]['mobile']=substr_replace($v['mobile'],'****',3,4);
            }
            $all_list=array('money_sum'=>$query['money_sum'],'list'=>$result);
            $res['status'] = true;
            $res['msg'] = '返回成功';
            $res['data'] = $all_list;
        }else{
            $res['status'] = false;
            $res['msg'] = '暂无数据';
        }
        return $res;
    }
    
    
    
    //技师信息
    public function tech_msg($tid) {
        $user = $this->db->select('techer.tid,mobile,truename,nickname,headurl,desc,skill,headurl_status')->from('techer_checkinfo')
            ->join('techer', 'techer.tid = techer_checkinfo.tid', 'left')
            ->where('xjm_techer.tid', $tid)
            ->get();
        $user_info = $user->row_array();
        
        (empty($user_info['skill'])) ? $skill=0 : $skill=$user_info['skill'];
        $re=$this->db->query('SELECT `name` FROM xjm_goods_option WHERE cate_id IN('.$skill.')')->result_array();
        $skill=array();
        if(!empty($re))
        {
            foreach ($re as $v)
            {
                $skill[]=@implode(',',$v);

            }
            $skill=@implode(',',$skill);
        }
        else
        {
            $skill="";
        }
        unset($user_info['skill']);
        $user_info['desc']=$skill;
        
        if (!$user_info) {
            return $this->results;
        }
        //照片视频
        $photo = $this->db->select('type,pic_url,video_url,pic_status,video_status')->from('techer_photo')
            ->where('tid', $tid)
            ->get();
        $photo_info = $photo->result_array();
        $hear_data = array();
        $video = '';
        foreach ($photo_info as $key => $value) {
            if ($value['type'] == 1) {
                //$hear_data[] = $value['pic_url'];
                if($value['pic_status']!=2){
                    $hear_data[] = array(
                        'thumb_pic'=>$value['pic_url'].'?imageMogr2/thumbnail/!640x300r/gravity/Center/crop/300x300/format/jpg/blur/1x0/quality/75|imageslim',
                        'pic_url'=>$value['pic_url']
                    );
                }

            }
            if ($value['type'] == 2) {
                // $video = $value['video_url'];
                if($value['video_status']!=2) {
                    $video[] = array('video_pic' => $value['video_url'] . '?vframe/png/offset/2/w/750/h/600', 'video_url' => $value['video_url']);
                }

            }
        }
        //标注
        $comment_tag = $this->db->select('tag_name,count(tag_id)as all_num')->from('user_comment_tag')
            ->where('tid', $tid)
            ->join('comment_tag_set', 'user_comment_tag.tag_id = comment_tag_set.id', 'left')
            ->group_by('tag_id')
            ->get();
        $comment_tag = $comment_tag->result_array();
//         if (empty($comment_tag)) {
//             $comment_tag = NULL;
//         }


        $user_msg['tid'] = $user_info['tid'];
        $user_msg['headurl'] = (empty($user_info['keyong_headurl'])) ? (empty($user_info['headurl']) ? 'http://pic.xijiaomo.com/wo.png' :  $user_info['headurl']) : $user_info['keyong_headurl'];
        //$user_msg['headurl'] = $user_info['headurl'];
        $user_msg['truename'] = $user_info['nickname'] ? $user_info['nickname'] : $user_info['truename'];
        $user_msg['mobile'] = substr_replace($user_info['mobile'], '****', 3, 4);
        $user_msg['desc'] = $user_info['desc'];
        //$user_msg['photos_url'] = $hear_data;
        (!empty($hear_data)) ? $user_msg['photos_url'] = $hear_data : $user_msg['photos_url'] = array();
        (!empty($video)) ? $user_msg['video_url'] = $video : $user_msg['video_url'] =array();
        $user_msg['comment_tag'] = $comment_tag;
        $this->results['status'] = TRUE;
        $this->results['data'] = $user_msg;
        return $this->results;
    }
    /*
     * 获取技师评论信息
     */
    public function getTechComment($tid,$now_page){
        --$now_page;
        $page_num = 5;
        $appraise = $this->db->select('c.nick_name,c.goods_name,c.headurl,c.mobile,c.type,c.anonymity,c.content,FROM_UNIXTIME(c.add_time,"%Y-%m-%d") add_time')
            ->from('user_comment c')
            ->join('user u','c.uid=u.uid','left')
            ->where('c.tid', $tid)
            ->limit($page_num,$now_page*$page_num)
            ->order_by('c.add_time desc')
            ->get();

        

        $appraiseList = $appraise->result_array();
        if(!empty($appraiseList)){
             foreach($appraiseList as $k=>$v){
                (empty($v['headurl'])) ? $appraiseList[$k]['headurl']='http://oxll5q098.bkt.clouddn.com/wo.png' : $appraiseList[$k]['headurl']=$v['headurl'];
            }
        }
       
        $good_praise = 0;
        $all_appraise = 0;
        $rs = $this->db->select('id,type')
            ->from('user_comment')
            ->where('tid', $tid)
            ->get();
        $appraise_rs = $rs->result_array();
        if ($appraise_rs) {
            $all_appraise = count($appraise_rs);
            $good_appraise = 0;
            foreach ($appraise_rs as $key => $value) {
                if ($value['type'] == 0) {
                    $good_appraise ++;
                }
//                 $appraise_rs[$key]['add_time'] = date('Y-m-d', $value['add_time']);
            }
            $good_praise = ceil(($good_appraise / $all_appraise) * 100);
        }
        $data['all_appraise'] = $all_appraise;
        $data['good_praise'] = $good_praise;
        $data['appraise_list'] = $appraiseList;
        return $data;

    }
    
    
    
    /**
     * 帐号密码 登录
     * @param type $mobile
     * @param type $pwd
     * @return type
     */
    function check_login($mobile, $pwd) {
        $where = array();
        $where['mobile'] = $mobile;
        $where['pwd'] = $pwd;
        $check = $this->db->select('tid,status')->from('techer')->where(array('mobile'=>$mobile))->get();
        $check_row=$check->row_array();


        
        
            if($check_row){
                $info=$this->db->select('id,card')->from('techer_checkinfo')->where(array('tid'=>$check_row['tid']))->limit(1)->get()->row_array();
                //检测账号状态
                if($check_row['status']==2)
                {
                    $this->results['status'] = false;
                    $this->results['msg'] = '账号已封号';
                }
                elseif(empty($info) || empty($info['card']))
                {
                    $this->results['status']=false;
                    $this->results['msg']='请补全个人身份信息';
                    $this->results['data']=$check_row;
                }
                else
                {
                    $where2=array('techer.mobile'=>$mobile,'techer.pwd'=>$pwd);
                    $tech_query = $this->db->select('*,techer.tid tids')->from('techer')->join('techer_checkinfo', 'techer.tid = techer_checkinfo.tid','left')->where($where2)->get();
                    $tech_info  = $tech_query->row_array();

                    $where2=array('tid'=>$check_row['tid'],'pay_status'=>'1');
                    $my_income=$this->db->select('order_id')->from('order_list')->where($where2)->get()->num_rows();

                    
                    $tid=$check_row['tid'];
                    $query=$this->db->select('c.id,c.tid,c.truename,c.card,c.sex,FROM_UNIXTIME(c.birthday,\'%Y-%m-%d\') birthday,c.`desc`,t.skill,c.idcard_zheng,c.idcard_fan,c.headurl')->from('techer_checkinfo c')->join('techer t','c.tid=t.tid','left')->where('c.tid='.intval($tid))->limit(1)->get();
                    $result=$query->row_array();

                    (empty($tech_info['skill'])) ? $skill=0 : $skill=$tech_info['skill'];
                    $re=$this->db->query('SELECT `name` FROM xjm_goods_option WHERE cate_id IN('.$skill.')')->result_array();
                    $skill=array();
                    if(!empty($re))
                    {
                        foreach ($re as $v)
                        {
                            $skill[]=@implode(',',$v);

                        }
                        $skill=@implode(',',$skill);
                    }
                    else
                    {
                        $skill="";
                    }
                    
                    if ($tech_info) {
                        $ds = array();

                        //登录时更新ip地址
                        $client_ip=$this->input->ip_address();
                        $save['last_login_ip']=$client_ip;
                        $this->db->where('tid='.$tech_info['tid'])->update('techer', $save);

                        $ds['tid'] = $tech_info['tids'];
                        $ds['pwd'] = $tech_info['pwd'];
                        $ds['is_online'] = $tech_info['is_online'];
                        //$ds['age'] = $tech_info['birthday']>0 ? getAge($tech_info['birthday']) : 0;
                        //$ds['skill'] = '高级按摩师';
                        //$ds['rate']     = !empty($tech_info['rate']) ? $tech_info['rate'] : 5;
                        $ds['mobile']   = substr_replace($tech_info['mobile'], '****', 3, 4);
                        $ds['desc']     = $skill;
                        $ds['nickname'] = $tech_info['nickname'];
                        $ds['truename'] = $tech_info['truename'];
                        (empty($tech_info['headurl'])) ? $ds['headurl']  = 'http://oxll5q098.bkt.clouddn.com/wo.png' : $ds['headurl']  = $tech_info['headurl'];
                        
                        $ds['my_income'] = isset($tech_info['money']) ? $tech_info['money'] : '0.00';
                        $ds['order_number'] = $my_income;
                        $tokeninfo = $this->authtoken->auth_encode($tech_info['tids']);
                        $ds['token'] = $tokeninfo['data'];

                        $this->results['status'] = true;
                        $this->results['msg'] = '登录成功';
                        $this->results['data'] = $ds;
                    } else {
                        $this->results['status'] = false;
                        $this->results['msg'] = '密码输入不正确';
                    }
                }

            }else {
                $this->results['status'] = false;
                $this->results['msg'] = '用户不存在';
            }
        



        return $this->results;
    }

    /**
     * 添加技师信息 注册
     * @param type $mobile
     * @param type $pwd
     * @return type
     */
    function insert_techer($baseinfo=array(), $addinfo=array()) {
        $inset = array();
        $time = time();
        $inset['mobile'] = $baseinfo['mobile'];
        //$inset['pwd']    = $baseinfo['pwd'];
        $inset['add_time'] = $time;

        //$rs = $this->db->insert('techer', $inset);

        //验证身份证不可重复
        $search_card=$this->db->select('id')->from('techer_checkinfo')->where(array('card'=>$addinfo['card']))->limit(1)->get()->row_array();
        if(!empty($search_card))
        {
            $this->results['status'] = false;
            $this->results['msg'] = '该身份证已被注册';
        }
        else
        {
            //查找技师
            $rs = $this->db->select('tid')->from('techer')->where(array("mobile"=>$inset['mobile']))->limit(1)->get();
            $rs_arr = $rs->row_array();
            if ($rs_arr['tid']) {
                $addinfo['tid'] = $rs_arr['tid'];

                //更新当前城市地址
                $city = $this->db->select('id')->from('area_list')->where(array('area_name'=>$baseinfo['city']))->get();
                $cityinfo = $city->row_array();
                if(empty($cityinfo)){
                    $this->db->insert('area_list', array('area_name'=>$baseinfo['city']));
                    $area_id = $this->db->insert_id();
                    if($area_id){
                        $this->db->update('techer', array('area_id'=>$area_id,'nickname'=>$addinfo['truename']), array('tid' => $addinfo['tid']));
                    }
                }

                //验证是否有数据
                $rowsa=$this->db->select('id')->from('techer_checkinfo')->where(array('tid'=>$addinfo['tid']))->get()->num_rows();
                if($rowsa>0){
                    $tids=$addinfo['tid'];
                    //unset($addinfo['tid']);
                    $ts = $this->db->where(array('tid'=>$tids))->update('techer_checkinfo', $addinfo);
                }else{
                    //插入技师信息审核表
                    $ts = $this->db->insert('techer_checkinfo', $addinfo);
                }

                $this->db->where(array('tid' => $addinfo['tid']))->update('techer', array('nickname'=>$addinfo['truename']));
                if($ts){
                    $this->results['status'] = true;
                    $this->results['msg']  = '注册成功';
                }else{
                    $this->results['status'] = false;
                    $this->results['msg'] = '注册失败,请稍后再试';
                }
            } else {
                $this->results['status'] = false;
                $this->results['msg'] = '注册失败,请稍后再试';
            }
        }



        return $this->results;
    }



    //设置登录密码
    public function set_pwd($account,$pwd,$code_id,$code,$source,$version,$pid,$icode)
    {
        $insert=array();
        $time=time();
        
            
                $data['mobile']=$account;
                $search=$this->db->select('pwd')->from('techer')->where($data)->get();
                $query=$search->row_array();

                //如果没有设置过密码或者没有这个电话
                if(!empty($query))
                {
                    $this->results['msg'] = '不能重复注册';
                }
                else
                {
                    //检测短信时效性
                    $array=array("mobile"=>$account,"type"=>"1","id"=>$code_id,"code"=>$code);
                    $search_code=$this->db->select('sendtime')->from('sms_code')->where($array)->limit(1)->get();
                    $search_code_array=$search_code->row_array();
                    if(empty($search_code_array))
                    {
                        $this->results['msg']  = '验证码错误';
                    }
                    else
                    {
                        $exp=$time-$search_code_array['sendtime'];
                        $exp_time=strtotime("i",$exp);

                        if($exp_time>30)
                        {
                            $this->results['msg']  = '验证码已过期';
                        }
                        else
                        {
                            $param['mobile']=$data['mobile'];
                            $param['pwd']=sha1($pwd);
                            $param['add_time']=$time;
                            $param['version']=$version;
                            $param['source']=$source;
                            $param['pid']=$pid;
                            if($source=="share"){
                                $qa=$this->db->select('tid')->from('techer')->where(array('tid'=>$pid,'icode'=>$icode))->get()->num_rows();
                                if($qa<1){
                                    $this->results['msg']  = '非法注册';
                                    return $this->results;
                                }
                            }
                            $ts=$this->db->insert('techer', $param);
                            $id = $this->db->insert_id();
                            if($id>0){
                                $this->results['status'] = true;
                                $this->results['msg']  = '密码设置成功';
                                $this->results['data'] = array('tid'=>$id,'mobile'=>$param['mobile']);
                            }else{
                                $this->results['msg'] = '密码设置失败';
                            }
                        }


                    }


                }
            
       
        return $this->results;

    }








    /**
     * 手机号码使用情况
     * @param type $mobile
     * @return type
     */
    public function check_mobile($mobile) {
        $where['mobile'] = $mobile;
        $check = $this->db->select('tid')->from('techer')->where($where)->get();
        $check_rs = $check->row_array();
        if ($check_rs) {
            $this->results['status'] = true;
            $this->results['msg'] = '手机号码已被使用';
        } else {
            $this->results['status'] = false;
            $this->results['msg'] = '手机号码尚未使用';
        }
        return $this->results;
    }

    /**
     * 更换手机号码
     * @param type $tid
     * @param type $mobile
     * @param type $new_mobile
     * @return type
     */
    public function change_mobile($tid, $mobile, $new_mobile) {
        $where['tid'] = $tid;
        $where['mobile'] = $mobile;
        $save['mobile'] = $new_mobile;
        $save_rs = $this->db->where($where)->update('techer', $save);
        $save_rs = $this->db->affected_rows();
        if ($save_rs>0) {
            $this->results['status'] = true;
            $this->results['msg'] = '号码更换成功';
        } else {
            $this->results['msg'] = '号码更换失败';
        }
        return $this->results;
    }
    
    /**
     * 修改密码
     * @param type $mobile
     * @param type $pwd
     * @return type
     */
    public function change_pwd($mobile, $pwd) {
        $where['mobile'] = $mobile;
        $check = $this->db->select('tid')->from('techer')->where($where)->get();
        $check_rs = $check->row_array();
        if (empty($check_rs)) {
            $this->results['status'] = false;
            $this->results['msg'] = '尚未注册';
            
        }
        else
        {
            $save['pwd'] = $pwd;
            $save_rs = $this->db->where(array('tid'=>$check_rs['tid']))->update('techer', $save);
            //$save_rs = $this->db->affected_rows();
            if ($save_rs) {
                $this->results['status'] = true;
                $this->results['msg'] = '密码修改成功';
            } else {
                $this->results['status'] = false;
                $this->results['msg'] = '密码修改失败';
            }
        }
        
        return $this->results;
        
    }
    
    /**
     * 修改昵称
     * @param type $tid
     * @param type $nickname
     * @param type $desc
     * @return type
     */
    public function do_editnick($tid, $nickname, $desc) {
        $where['tid'] = $tid;
        $save1['nickname'] = $nickname;
        //$save1['skill'] = $desc;
        $this->db->where($where)->update('techer', $save1);
        //$this->db->where($where)->update('techer_checkinfo', $save2);
        $this->results['status'] = true;
        $this->results['msg'] = '修改成功';
        return $this->results;
    }
    
    /**
     * 修改头像
     * @param type $tid
     * @param type $headurl
     * @return type
     */
    public function do_editimg($tid, $headurl) {
       $where['tid'] = $tid;
        $save['headurl'] = $headurl;
        $save['headurl_status']=0;	
        $rows=$this->db->select('id')->from('techer_checkinfo')->where($where)->get()->num_rows();
        if($rows>0){
            $this->db->where($where)->update('techer_checkinfo', $save);
            $save_rs = $this->db->affected_rows();
            if ($save_rs>0) {
                $this->results['status'] = true;
                $this->results['msg'] = '头像上传成功';
            } else {
                $this->results['msg'] = '头像上传失败';
            }
        }else{
            $array=array('tid'=>$tid,'headurl'=>$headurl);
            $this->db->insert('techer_checkinfo',$array);
            $this->results['status'] = true;
            $this->results['msg'] = '头像上传成功';
        }
        return $this->results;
    }
    
    /**
     * 意见反馈
     * @param type $tid
     * @param type $content
     * @return type
     */
    public function sub_feedback($tid, $content,$type) {
        $data['tid'] = $tid;
        $data['content'] = $content;
        $data['add_time'] = time();
        $data['type'] = $type;
        $this->db->insert('feedback', $data);        
        $id = $this->db->insert_id();
        if($id>0){
            $res['status'] = true;
            $res['msg'] = '提交成功';
        }else{
            $res['status'] = false;
            $res['msg'] = '提交失败';
        }
        return $res;
    }
    
    /**
     * 用户信息
     * @param type $uid
     * @param type $mobile
     * @return type
     */
    public function techer_info($tid, $mobile) {
        $where['tid'] = $tid;
        $where['mobile'] = $mobile;
        $check = $this->db->select('tid')->from('techer')->where($where)->get();
        $check_rs = $check->row_array();
        if ($check_rs) {
            $this->results['status'] = true;
            $this->results['msg'] = '用户存在';
            $this->results['data'] = $check_rs;
        } else {
            $this->results['msg'] = '不存在用户';
        }
        return $this->results;
    }
    
    /**
     * 添加提现账户
     * @param 
     */
    public function do_addaccount($data){

        //添加事务
        $this->db->trans_begin();

        //星期二才能提现
        if(date('w')!=2)
        {
            $res['status'] = false;
            $res['msg'] = '星期二才能申请提现';
        }
        else
        {
        
            if($data['cash_money']<50){
                $res['status'] = false;
                $res['msg'] = '50元以上才能提现';
            }
            else{
               $paypwd=sha1($data['paypwd']);


            $money_query=$this->db->select("money")->from("techer")->where("tid=".$data['tid']." AND paypwd='".$paypwd."'")->get();
            $money_row=$money_query->row_array();

            if($data['cash_money']>$money_row['money'] || empty($money_row['money']))
            {
                $res['status'] = false;
                $res['msg'] = '提现异常';
            }
            else
            {
                unset($data['paypwd']);
                $this->db->insert('techer_cash', $data);
                $id = $this->db->insert_id();

                //插入技师收入明细表
                $ups=array();
                $ups=array(
                    'tid'=>$data['tid'],
                    'goods_name'=>'收入提现',
                    'from'=>2,
                    'add_time'=>time(),
                    'change_money'=>$data['cash_money'],
                    'money'=>$money_row['money']-$data['cash_money'],
                    'cash_id'=>$id
                );

                $this->db->insert('techer_income',$ups);
                
                
                //修改技师余额
                $upt=array();
                $upt['money']=$money_row['money']-$data['cash_money'];
                $this->db->where(array('tid'=>$data['tid']))->update('techer',$upt);

                if($id>0){
                    $res['status'] = true;
                    $res['msg'] = '提现申请已提交，请耐心等待审核';
                }else{
                    $res['status'] = false;
                    $res['msg'] = '提现申请失败';
                } 
            }
            

            }
        }




        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }

        return $res;
    }
    
    
    
    /**
     * 技师个人身份信息
     * @param $tid
     */
    public function techer_infos($tid)
    {
        $query=$this->db->select('c.id,c.tid,c.card,c.sex,FROM_UNIXTIME(c.birthday,\'%Y-%m-%d\') birthday,c.`desc`,t.skill,c.idcard_zheng,c.idcard_fan,t.nickname truename,c.headurl')->from('techer_checkinfo c')->join('techer t','c.tid=t.tid','left')->where('c.tid='.intval($tid))->limit(1)->get();
        $result=$query->row_array();

        (empty($result['skill'])) ? $skill=0 : $skill=$result['skill'];
        $re=$this->db->query('SELECT `name` FROM xjm_goods_option WHERE cate_id IN('.$skill.')')->result_array();
        $skill=array();
        if(!empty($re))
        {
            foreach ($re as $v)
            {
                $skill[]=@implode(',',$v);

            }
            $skill=@implode(',',$skill);
        }
        else
        {
            $skill="";
        }
        unset($result['skill']);
        $result['desc']=$skill;


        if(!empty($result))
        {
            $res['status'] = true;
            $res['msg'] = '返回成功';
            $res['data'] = $result;
        }
        else
        {
            $res['status'] = false;
            $res['msg'] = '暂无数据';
        }
        return $res;
    }
    
    
    /**
     * 获取个人信息
     * @param unknown $tid
     */
    public function get_person_info($tid){
        $where=array('t.tid'=>$tid);

        $result=$this->db->select('t.tid,t.is_online,t.mobile,t.nickname,c.truename,c.desc,c.headurl,t.money,t.skill')
            ->from('techer t')
            ->join('techer_checkinfo c','t.tid=c.tid','left')
            ->where($where)->limit(1)->get()->row_array();

        $where2=array('tid'=>$tid,'pay_status'=>1);
        $my_income=$this->db->select('count(order_id) order_number')->from('order_list')->where($where2)->limit(1)->get()->row_array();


        (empty($result['skill'])) ? $skill=0 : $skill=$result['skill'];
        $re=$this->db->query('SELECT `name` FROM xjm_goods_option WHERE cate_id IN('.$skill.')')->result_array();
        $skill=array();
        if(!empty($re))
        {
            foreach ($re as $v)
            {
                $skill[]=@implode(',',$v);

            }
            $skill=@implode(',',$skill);
        }
        else
        {
            $skill="";
        }

        $data = array();
        $res = array();
        if($result){
            $data['nickname'] = $result['nickname'];
            $data['truename'] = $result['truename'];
            (empty($result['skill'])) ? $data['desc']='' : $data['desc']=$skill;
             (empty($result['headurl'])) ? $data['headurl']='http://oxll5q098.bkt.clouddn.com/wo.png' : $data['headurl']=$result['headurl'];
            // $data['headurl']=$result['headurl'];
            $data['my_income'] = isset($result['money']) ? $result['money'] : '0.00';
            $data['order_number'] = isset($my_income['order_number']) ? $my_income['order_number'] : '0';
            $data['is_online'] = $result['is_online'];
            $data['mobile'] = $result['mobile'];

            $res['status'] = true;
            $res['msg'] = '返回成功';
            $res['data'] = $data;
        }else{
            $res['status'] = false;
            $res['msg'] = '暂无数据';
        }
        return $res;
    }
    
    /**
     * 获取我的收入明细
     * @param unknown $tid
     */
    public function get_income_data($tid,$page){
        $limit=8;
        if($page==0)
        {
            $start=$page;
        }
        else
        {
            $start=$page*$limit-$limit;
        }

        $start=intval($start);

        $all=$this->db->select('i.*,c.status')->from('techer_income i')->join('techer_cash c','i.cash_id=c.id','left')
            ->where('i.tid', $tid)->get()->num_rows();

        $photos = $this->db->select('i.*,c.status')->from('techer_income i')->join('techer_cash c','i.cash_id=c.id','left')
        ->where('i.tid', $tid)
        ->order_by('i.id', 'DESC')
        ->limit($limit,$start)
        ->get();

        $all_list = $photos->result_array();
        $res = array();
        if($all_list){
            $res['status'] = true;
            $res['msg'] = '返回成功';
            foreach ($all_list as $k=>$v)
            {
                if(empty($v['order_id']))
                {
                    $all_list[$k]['order_id']='';
                }
                elseif(empty($v['status']))
                {
                    $all_list[$k]['status']=0;
                }
                unset($all_list[$k]['cash_id']);
                $all_list[$k]['add_time']=date('Y-m-d H:i:s',$v['add_time']);
            }
            $res['data'] = array('count'=>$all,'limit'=>$limit,'list'=>$all_list);
        }else{
            $res['status'] = false;
            $res['msg'] = '暂无数据';
        }
        return $res;
    }
    
    /**
     * 获取我的相册信息
     * @param unknown $tid
     */
    public function getmyphoto($tid){
        //只取审核通过和未审核
        $where=array('tid'=>$tid,'pic_status!='=>2,'video_status!='=>2);
        $photos = $this->db->select('*')->from('techer_photo')
        ->where($where)
        ->order_by('id', 'DESC')
        ->limit(3)
        ->get();
        $all_list = $photos->result_array();
        $res = array();
        if($all_list){
            $res['status'] = true;
            $res['msg'] = '返回成功';
            $res['data'] = $all_list;
        }else{
            $res['status'] = false;
            $res['msg'] = '暂无数据';
        }
        return $res;
    }
    
    /**
     * 设置我的当前在线状态
     * @param unknown $tid
     */
    public function do_isonline($tid, $is_online, $t_lat, $t_lon){
        $where = array();
        $where['tid'] = $tid;

        $check = $this->db->select('tid,status')->from('techer')->where(array('tid'=>$where['tid']))->get();
        $check_row=$check->row_array();
        if($check_row['status']==0)
        {
            $data['status'] = false;
            $data['msg'] = '账号审核中';
        }
        elseif($check_row['status']==2)
        {
            $data['status'] = false;
            $data['msg'] = '账号已封号';
        }
        else
        {
            $save_rs = $this->db->where($where)->update('techer', array('is_online'=>$is_online, 't_lat'=>$t_lat, 't_lon'=>$t_lon));
            $save_rs = $this->db->affected_rows();
            $data = array();
            if ($save_rs>0) {
                $data['status'] = true;
                $data['msg'] = '设置成功';
            } else {
                $data['status'] = false;
                $data['msg'] = '设置失败';
            }
        }


        return $data;
    }
    
    /**
     * 获取用户对技师的评价列表
     */
    public function get_comment_list($tid){
        $comments = $this->db->select('c.*,u.headurl')->from('user_comment c')
        ->join('user u','c.uid=u.uid','left')
        ->where('c.tid', $tid)
        ->order_by('c.add_time desc')
        ->get();
        $comments_list = $comments->result_array();
        $data = array();
        $rss = array();
        $rs = array();
        if ($comments_list) {
            foreach ($comments_list as $k=>$v){
                $rs['add_time']     = date('Y-m-d H:i:s', $v['add_time']);
                $rs['nick_name']    = $v['nick_name'];
                (empty($v['headurl'])) ? $rs['headurl']='http://oxll5q098.bkt.clouddn.com/wo.png' : $rs['headurl']=$v['headurl'];
                
                //$rs['headurl']      = $v['headurl'];
                $rs['content']      = $v['content'];
                $rs['goods_name']   = $v['goods_name'];
                $rs['type']         = $v['type'] == 0 ? '好评' : '差评';
                
                $rss[] = $rs;
            }
            $data['status'] = true;
            $data['msg'] = '返回成功';
            $data['data'] = !empty($rss) ? $rss : null;
        } else {
            $data['status'] = false;
            $data['msg'] = '暂无数据';
        }
        return $data;
    }
    
    public function tech_comment($tid){
        //标注
        $comment_tag = $this->db->select('tag_name,count(tag_id)as all_num')->from('user_comment_tag')
        ->where('tid', $tid)
        ->join('comment_tag_set', 'user_comment_tag.tag_id = comment_tag_set.id', 'left')
        ->group_by('tag_id')
        ->get();
        $comment_tag = $comment_tag->result_array();
        //获取评价
        $appraise = $this->db->select('nick_name,headurl,mobile,type,anonymity,content,add_time')->from('user_comment')->where('tid', $tid)->limit(10)->order_by('add_time desc')->get();
        $appraise_rs = $appraise->result_array();
        $all_appraise = count($appraise_rs);
        $good_appraise = 0;
        foreach ($appraise_rs as $key => $value) {
            if ($value['type'] == 0) {
                $good_appraise ++;
            }
            (empty($value['headurl'])) ? $appraise_rs[$key]['headurl']='http://oxll5q098.bkt.clouddn.com/wo.png' : $appraise_rs[$key]['headurl']=$value['headurl'];
            $appraise_rs[$key]['add_time'] = date('Y-m-d', $value['add_time']);
        }
        $user_msg['tid'] = $tid;
        $user_msg['all_appraise'] = $all_appraise > 0 ? $all_appraise : 0;
        if($all_appraise>0){
            $user_msg['good_appraise'] = ceil(($good_appraise / $all_appraise) * 100);
        }else{
            $user_msg['good_appraise'] = 0;
        }
        $user_msg['appraise_list'] = !empty($appraise_rs) ? $appraise_rs : null;
        $user_msg['comment_tag'] = !empty($comment_tag) ? $comment_tag : null;
        $this->results['status'] = true;
        $this->results['data'] = $user_msg;
        return $this->results;
    }
    
    /**
     * 用户信息
     * @param type $uid
     * @param type $mobile
     * @return type
     */
    public function tech_info($tid, $mobile) {
        $where['tid'] = $tid;
        $where['mobile'] = $mobile;
        $check = $this->db->select('tid')->from('techer')->where($where)->get();
        $check_rs = $check->row_array();
        if ($check_rs) {
            $this->results['status'] = true;
            $this->results['msg'] = '技师存在';
            $this->results['data'] = $check_rs;
        } else {
            $this->results['msg'] = '不存在技师';
        }
        return $this->results;
    }


    //我的评价列表
    public function evaluate_person($tid)
    {
        $sql="SELECT from_unixtime(add_time) addtime,nick_name,headurl,type FROM xjm_user_comment WHERE tid=".intval($tid)." ORDER BY add_time DESC";
        $result=$this->db->query($sql)->result_array();
        if ($result) {
            $this->results['status'] = true;
            $this->results['msg'] = 'ok';
            $this->results['data'] = $result;
        } else {
            $this->results['msg'] = 'error';
        }
        return $this->results;
    }


    //我的账户QA
    public function myperson_qa()
    {
        $where['type']=1;
        $where['status']=0;
        $query=$this->db->select('title,content,description')->from('article')->where($where)->order_by('id')->get();
        $result=$query->result_array();
        $new=array();
        foreach ($result as $k=>$v)
        {
            $new[$v['description']]['title']=$v['title'];
            $new[$v['description']]['content'][]=$v['content'];
        }

        if ($result) {
            $this->results['status'] = true;
            $this->results['msg'] = '获取成功';
            $this->results['data'] = $new;
        } else {
            $this->results['msg'] = '获取失败';
        }
        return $this->results;
    }
}
