<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
    <meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
    <meta name="applicable-device" content="mobile">
    <link rel="stylesheet" type="text/css" href="/app/tech/views/help/money/css/new_file.css" />
    <link rel="stylesheet" type="text/css" href="/app/tech/views/help/money/css/set.css" />
    <title>赚钱攻略</title>
    <style>
        html,
        body {
            height: auto;
            overflow: auto;
        }

        .perfect_hom p.intro,
        .perfect_hom p.intro {
            font-size: 13px;
            color: #999;
            font-weight: 700;
        }

        .fl {
            float: left;
        }

        .fr {
            float: right;
        }

        .img_no {
            text-align: left;
        }

        .shuoming {
            padding-top: 5%;
        }

        .shuoming h3 {
            color: #d5201e;
            font-size: 15px;
            padding: 12px;
        }

        .shuoming span,
        .shuoming3 span {
            color: #999;
            font-size: 13px;
            padding: 0px;
            display: block;
        }

        .shuoming h2.fl {
            width: 50%;
        }

        .shuoming3 h3 {
            color: #d5201e;
            font-size: 15px;
            padding: 6px;
        }

        span.con_title {
            color: #D5201E;
            padding-top: 55px;
            position: relative;
        }

        span.hover:before {
            content: "";
            width: 50px;
            height: 1px;
            background-color: #D5201E;
            position: absolute;
            left: 38%;
            top: 80%;
        }

        span.hover2:after {
            content: "";
            width: 50px;
            height: 1px;
            background-color: #D5201E;
            position: absolute;
            left: 80%;
            top: 80%;
        }

        .wraper_content {
            background: url("/app/tech/views/help/money/img/bg1@2x.png") no-repeat;
            background-size: 100%;
        }

        .perfect_hom {
            background-size: 100%;
            height: 100%;
            text-align: center;
            padding: 15px;
        }

        @media only screen and (min-width: 320px) and (max-width: 360px) {
            .shuoming h3 {
                font-size: 14px !important;
                padding: 8px !important;
            }
            .shuoming span {
                font-size: 12px !important;
            }
            .perfect_home {
                background-size: 104% !important;
                background-position-x: -2px !important;
            }
            span.hover:before {
                content: "";
                width: 43px !important;
            }
            span.hover2:before {
                content: "";
                width: 43px !important;
            }
        }
    </style>
</head>

<body>
<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
    <div class="m_header_bar J_header-bar" style="display: none;">
        <div class="mhb_left">
            <a class="myposition" href=""><img class="icon icon-position3" src="/app/tech/views/help/money/img/fanhui@2x.png"></img>
            </a>

        </div>
        <div class="mhb_center mhb_center_across">
            <h2 class="title">赚钱攻略</h2>
        </div>
    </div>

</header>
<div class="banner">
    <img src="/app/tech/views/help/money/img/guide/banner@2x.png" width="100%" height="100%" />
</div>
<!--主要内容-->
<div class="wraper_content">
    <div class="perfect_hom">
        <h1><img src="/app/tech/views/help/money/img/guide/title@2x.png" width="20%" /></h1>
        <p class="intro">86%的客户，会选择资料完善的技师</p>
        <p style="height: 50px;"></p>
        <p class="img_no"><img src="/app/tech/views/help/money/img/guide/no.1@2x.png" width="90%" /></p>
        <div class="img_no clearfix shuoming">
            <img src="/app/tech/views/help/money/img/guide/pho@2x.png" width="35%" class="fl" style="margin-right: 20px;" />
            <h3>
                上传真人正面真实照片
            </h3>
            <span>
             		在个人中心设置，修改头像。使用真实正面照片作为头像能够展示自己的诚意，获得更多订单机会。
             	</span>
        </div>
    </div>
    <div class="perfect_hom">

        <p class="img_no"><img src="/app/tech/views/help/money/img/guide/no.2@2x.png" width="90%" /></p>
        <div class="img_no clearfix shuoming">
            <img src="/app/tech/views/help/money/img/guide/pho2@2x.png" width="35%" class="fl" style="margin-right: 20px;" />
            <h3>
                认证身份和技能
            </h3>
            <span>
             		在注册页面，进行身份认证和技能认证。客户更喜欢有证书的技师，认证后抢单成功率会提升80%，同时发布多个技能，可以增加更多曝光机会。
             	</span>
        </div>
    </div>
    <div class="perfect_hom">

        <p class="img_no"><img src="/app/tech/views/help/money/img/guide/no.3@2x.png" width="90%" /></p>
        <div class="img_no clearfix shuoming">
            <img src="/app/tech/views/help/money/img/guide/shouji1@2x.png" width="50%" class="fl" style="margin-right: 20px;" />
            <h2>
                <span class="con_title hover">视频：</span>拍摄擅长的技能，或者服务时的场景，上传到“我的视频”中，展现自己专业服务技能流程。
            </h2>

        </div>
        <div class="img_no clearfix shuoming">

            <h2 class="fl">
                <span class="con_title hover2">相册：</span>将服务的场景拍照上传到“我的相册”，多方面展示自己的技能。
            </h2>
            <img src="/app/tech/views/help/money/img/guide/shouji2@2x.png" width="50%" class="fr" />
        </div>
    </div>
    <div class="perfect_hom">
        <h1><img src="/app/tech/views/help/money/img/guide/titlt2@2x.png" width="20%" /></h1>
        <p class="intro">开启抢单模式，即刻显示你身边的订单需求</p>
        <p style="height: 30px;"></p>
        <div class="img_no clearfix shuoming">
            <img src="/app/tech/views/help/money/img/guide/shouji3@2x.png" width="35%" class="fl" style="margin-right: 20px;" />
            <h3>
                开启抢单模式
            </h3>
            <span>
             		在首页点击立即接单，客户便能看到你的相关信息，提高曝光度与成单率。
             	</span>
        </div>
    </div>
    <div class="perfect_hom">
        <h1><img src="/app/tech/views/help/money/img/guide/title3@2x.png" width="20%" /></h1>

        <div class="img_no clearfix shuoming">
            <img src="/app/tech/views/help/money/img/guide/icon1@2x.png" width="20%" class="fl" style="margin-right: 20px;" />
            <h3>
                邀请好友加入喜脚么
            </h3>
            <span>
             		将链接分享给好友，邀请好友一起赚钱。
             	</span>
        </div>
        <div style="height: 20px"></div>
        <div class="img_no clearfix shuoming3">
            <img src="/app/tech/views/help/money/img/guide/icon2@2x.png" width="20%" class="fl" style="margin-right: 20px;" />
            <h3>
                完成交易要评价
            </h3>
            <span>
             		交易结束后，主动要求客户评价，提升自己的好评率。
             	</span>
        </div>
        <div class="img_no clearfix shuoming">
            <img src="/app/tech/views/help/money/img/guide/icon3@2x.png" width="20%" class="fl" style="margin-right: 20px;" />
            <h3>
                喜脚么不定期红包福利
            </h3>
            <span>
             		请随时关注喜脚么APP通知和微信公众号，更多红包与奖励等着你。
             	</span>
        </div>
    </div>
    <div style="height: 30px; background: #fff;"></div>
</div>
</body>

</html>