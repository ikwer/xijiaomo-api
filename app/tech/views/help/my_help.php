<!DOCTYPE html>
<html style="background-color: #fff;">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
    <meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
    <meta name="applicable-device" content="mobile">
    <link rel="stylesheet" type="text/css" href="/app/tech/views/help/css/new_file.css" />
    <link rel="stylesheet" type="text/css" href="/app/tech/views/help/css/set.css" />
    <link rel="stylesheet" type="text/css" href="/app/tech/views/help/css/medie.css" />
    <title>帮助中心</title>
    <style>
        .main {
            overflow: auto;
            min-height: 414px;
        }

        .city_name {
            height: 30px;
            background-color: #f3f3f3;
        }

        .city_name h3 {
            line-height: 30px;
            padding-left: 10px;
            color: #666;
        }

        .FQA {
            padding: 0 10px;
        }

        .FQA_list {
            position: relative;
            border-bottom: 1px solid #e6e6e6;
            font-size: 14px;
            color: #666;
            /*cursor: pointer;*/
        }

        .FQA_list p {
            line-height: 50px;
        }
        .FQA li p {
            height: 50px;
            font-weight: 700;
        }
        .FQA li h3{
            color: #D5201E;
            padding: 5px 0;
            font-size: 14px;
        }
        .main_content{
            padding-bottom: 10px;
        }
        .main_content span{
            line-height: 20px;
            letter-spacing: 1px;
            color: #666;
        }
    </style>
</head>

<body>
<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
    <div class="m_header_bar J_header-bar" style="display:none;">
        <div class="mhb_left">
            

        </div>
        <div class="mhb_center mhb_center_across" style="display:none;">
            <h2 class="title"><?php if($_GET['source']=="techer"){ echo '技师'; }else{ echo '用户'; } ?>帮助中心</h2>
        </div>

    </div>

</header>
<section class="main">
    <div class="city_name">
        <h3>常见问题</h3>

    </div>
    <ul class="clearfix FQA"  id="toggle-view">
        <?php foreach ($result as $k=>$v): ?>
        <li class="clearfix FQA_list  <?php if($k>1){ echo 'list_down'; } ?>  ">
            <p><?php echo $v['title']; ?></p>
            <div class="main_content" style="display: none;">
                <?php echo htmlspecialchars_decode($v['content']); ?>
            </div>
        </li>
        <?php endforeach; ?>


    </ul>
</section>
<script type="text/javascript" src="/app/tech/views/help/js/jquery.min.js"></script>
<script type="text/javascript">

    $(document).ready(function () {

        $('#toggle-view li').click(function () {

            var text = $(this).children('.main_content');

            if (text.is(':hidden')) {
                text.slideDown('200');
                $(this).children('span').html('-');
            } else {
                text.slideUp('200');
                $(this).children('span').html('+');
            }

        });

    });

</script>

</script>
</body>

</html>