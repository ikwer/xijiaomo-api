<!DOCTYPE html>
<html style="background-color: #fff;">

	<head>

		<head>
			<meta charset="utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
			<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
			<meta name="applicable-device" content="mobile">
			<link rel="stylesheet" type="text/css" href="/app/tech/views/help/jinji/css/new_file.css" />
			<link rel="stylesheet" type="text/css" href="/app/tech/views/help/jinji/css/set.css" />
			<link rel="stylesheet" type="text/css" href="/app/tech/views/help/jinji/css/medie.css" />
			<title><?php echo $result['title']; ?></title>
			<style>
				h1 {
					font-size: 15px;
					font-weight: 700;
					padding-bottom: 5px;
				}
				ul li{
					padding-bottom: 10px ;
				}
				ul li p{
					line-height: 25px;
				}
			</style>

			<body>
				<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
					<div class="m_header_bar J_header-bar" style="display:none;">
						<div class="mhb_left">
							<a class="myposition" href="all_project.html"><img class="icon icon-position3" src="/app/tech/views/help/jinji/img/fanhui@2x.png"></img>
							</a>
						</div>
						<div class="mhb_center mhb_center_across">
							<h2 class="title">禁忌说明</h2>
						</div>

					</div>

				</header>
				<section class="main" style="padding: 20px;">

				<?php echo htmlspecialchars_decode($result['content']); ?>
                </section>
			</body>

</html>