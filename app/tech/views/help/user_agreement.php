<!DOCTYPE html>
<html style="background-color: #fff;">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
    <meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
    <meta name="applicable-device" content="mobile">
    <link rel="stylesheet" type="text/css" href="/app/tech/views/help/css/new_file.css" />
    <link rel="stylesheet" type="text/css" href="/app/tech/views/help/css/set.css" />
    <link rel="stylesheet" type="text/css" href="/app/tech/views/help/css/jishizhuye.css" />
    <link rel="stylesheet" type="text/css" href="/app/tech/views/help/css/medie.css" />
    <link rel="stylesheet" href="/app/tech/views/help/css/baguetteBox.min.css">
    <title><?php echo $result['title']; ?></title>
    <style>
        *{
            letter-spacing: 1px;
            line-height: 20px;
        }
        .wenzi{
            padding: 15px;
        }
        h1{
            font-size: 15px;
            text-align: center;
            padding-bottom: 20px;
        }
    </style>
</head>

<body>
<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
    <div class="m_header_bar J_header-bar" style="display:none;">
        <div class="mhb_left">
            <a class="myposition" href="login.html"><img class="icon icon-position3" src="/app/tech/views/help/img/fanhui@2x.png"></img>
            </a>

        </div>
        <div class="mhb_center mhb_center_across">
            <h2 class="title"><?php echo $result['title']; ?></h2>
        </div>

    </div>

</header>
<section class="wenzi">
    <?php echo htmlspecialchars_decode($result['content']); ?>
</section>

</body>

</html>