<!DOCTYPE html>
<?php
/*require_once "php/jssdk.php";
$jssdk = new JSSDK("wx47d4cb38cdc0f293", "4f64efbf42633fd52404ded1c5d1b2ea");
$signPackage = $jssdk->GetSignPackage();*/
?>
<html style="background-color: #fff;">

<head>

    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
        <meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
        <meta name="applicable-device" content="mobile">
        <link rel="stylesheet" type="text/css" href="/data/techer/css/index.css" />
        <link rel="stylesheet" type="text/css" href="/data/techer/css/rest.css" />
        <link rel="stylesheet" type="text/css" href="/data/techer/css/medie.css" />
        <link rel="stylesheet" href="/data/techer/css/center.css" />
        <link rel="stylesheet" type="text/css" href="/data/techer/dist/lib/weui.css" />
        <link rel="stylesheet" type="text/css" href="/data/techer/dist/css/jquery-weui.min.css" />

        <script type="text/javascript" src="/data/assets/js/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="/data/techer/js/jquery.cookie.js"></script>

        <!--layer-->
        <link rel="stylesheet" type="text/css" href="/data/techer/js/layer_mobile/need/layer.css" />
        <script type="text/javascript" src="/data/techer/js/layer_mobile/layer.js"></script>
        <!--layer-->
        <title>喜脚么-技师注册</title>
        <style>
            .weui-cell {
                padding: 16px 30px;}
        </style>
    </head>

<body>
<header class="m_header  sticky_head" id="J_header" style="disposition: sticky; top: 0px; bottom: 0px;border-bottom: 1px solid #e6e6e6;">
    <div class="m_header_bar J_header-bar">
        <div class="mhb_left" style="display: none;">
            <a class="myposition" href="/index.php/tech/main/login"><img class="icon icon-position3" src="/data/techer/img/fanhui@2x.png"></img>
            </a>
        </div>
        <div class="mhb_center mhb_center_across">
            <h2 class="title">技师注册</h2>
        </div>

    </div>

</header>
<div style="height: 20px;"></div>
<div class="weui-cells weui-cells_form">
    <div class="weui-cell">
        <div class="weui-cell__hd"><label class="weui-label"><img src="/data/techer/img/shouji@2x.png" width="16px"/></label></div>
        <div class="weui-cell__bd">
            <input class="weui-input" name="mobile" type="number" pattern="[0-9]*" placeholder="请输入手机号" value="">
        </div>
        <div class="close">
            <i class="weui-icon-cancel"></i>
        </div>
    </div>
    <div class="weui-cell">
        <div class="weui-cell__hd">
            <label class="weui-label"><img src="/data/techer/img/shuru@2x.png" width="16px"/></label>
        </div>
        <div class="weui-cell__bd">
            <input class="weui-input" name="code" type="number" placeholder="请输入验证码">
        </div>
        <div class="weui-cell__ft">
            <button class="weui-vcode-btn weui-but_huoqu">获取验证码</button>
        </div>
    </div>
    <div class="weui-cell">
        <div class="weui-cell__hd"><label class="weui-label"><img src="/data/techer/img/mima-@2x.png" width="16px"/></label></div>
        <div class="weui-cell__bd">
            <input class="weui-input" name="pwd1" type="password" pattern="[0-9]*" placeholder="请输入（6-20位数字或字母）密码">
        </div>
        <div class="weui-cell__bd">
        </div>
    </div>
    <div class="weui-cell">
        <div class="weui-cell__hd"><label class="weui-label"><img src="/data/techer/img/mima-@2x.png" width="16px"/></label></div>
        <div class="weui-cell__bd">
            <input class="weui-input" name="pwd2" type="password" pattern="[0-9]*" placeholder="请再次输入密码">
        </div>
        <div class="weui-cell__bd">
        </div>
    </div>
</div>
<div style="height: 20px;"></div>
<div class="button">
    <a href="javascript:;" id="next_step"><button>下一步</button></a>
    <label for="weuiAgree" class="weui-agree">
        <input id="weuiAgree" type="checkbox" class="weui-agree__checkbox" checked="checked">
        <span class="weui-agree__text"> 注册即表示同意并遵守<a href="javascript:void(0);" id="xieyi">《喜脚么使用协议》</a>
                            </span>
    </label>
</div>

</body>
<script type="text/javascript">
    $
    $(".close").click(function(){
        $("input[name='mobile']").val('');
    });

    


    $(".weui-cell__ft").click(function(){
            var mobile=$("input[name='mobile']").val();
            mobile=$.trim(mobile);

            if(mobile=='' || !(/^1[3|4|5|7|8][0-9]\d{4,8}$/.test(mobile))){
                //提示
                layer.open({
                    content: '手机号格式错误'
                    ,skin: 'msg'
                    ,time: 2
                });
                return false;
            }else{
                var load1=layer.open({type: 2});
                $.post('/tech.php/share/send_code',{account:mobile,type:'1'},function(data){
                    layer.close(load1);
                    if(data.code==0){
                        //提示
                        layer.open({
                            content: data.message
                            ,skin: 'msg'
                            ,time: 2
                        });

                        var validCode=true;
                        var time=60;
                        var $code=$('.weui-but_huoqu');
                        if (validCode) {
                            validCode=false;
                            var t=setInterval(function  () {
                                time--;
                                $code.html(time+"秒后重新获取");
                                if (time==0) {
                                    clearInterval(t);
                                    $code.html("重新获取");
                                    validCode=true;
                                }
                            },1000)
                        }

                    }else{
                        //提示
                        layer.open({
                            content: data.message
                            ,skin: 'msg'
                            ,time: 2
                        });
                        return false;
                    }
                },'json');



            }


    });

    $("#next_step").click(function(){
        if(!$("#weuiAgree").is(':checked')){
            //提示
            layer.open({
                content: '请先勾选注册协议'
                ,skin: 'msg'
                ,time: 2
            });
            return false;
        }else{
            var pwd1=$("input[name='pwd1']").val();
            pwd1 = $.trim(pwd1);
            var pwd2=$("input[name='pwd2']").val();
            pwd2 = $.trim(pwd2);

            var mobile=$("input[name='mobile']").val();
            mobile=$.trim(mobile);

            var code=$("input[name='code']").val();
            code=$.trim(code);

           var pid = "<?php if(!empty($_GET['pid'])){ echo $_GET['pid']; }else{ echo 0; } ?>";
            pid=$.trim(pid);
            
            var icode = "<?php if(!empty($_GET['icode'])){ echo $_GET['icode']; }else{ echo 0; } ?>";
            icode=$.trim(icode);

            if(pwd1=='' || pwd2=='') {
                //提示
                layer.open({
                    content: '密码不能为空'
                    , skin: 'msg'
                    , time: 2
                });
                return false;
            }
           else if(pwd1.length<6 || pwd1.length>20) {
                //提示
                layer.open({
                    content: '请输入（6-20位数字或字母）密码'
                    , skin: 'msg'
                    , time: 2
                });
                return false;
            }
            else if(pwd2.length<6 || pwd2.length>20) {
                //提示
                layer.open({
                    content: '请输入（6-20位数字或字母）密码'
                    , skin: 'msg'
                    , time: 2
                });
                return false;
            }
            else if(pwd1!=pwd2)
            {
                //提示
                layer.open({
                    content: '两次输入的密码不一致'
                    ,skin: 'msg'
                    ,time: 2
                });
                return false;
            }
            else if(mobile=='' || !(/^1[3|4|5|7|8][0-9]\d{4,8}$/.test(mobile))) {
                //提示
                layer.open({
                    content: '手机号格式错误'
                    , skin: 'msg'
                    , time: 2
                });
                return false;
            }else if(code=='' || code.length!=4){
                //提示
                layer.open({
                    content: '验证码格式错误'
                    ,skin: 'msg'
                    ,time: 2
                });
                return false;
            }else{
                var load2 = layer.open({type: 2});
                $.post("/tech.php/share/reg_step1",{account:mobile,code:code,pwd:pwd1,re_pwd:pwd2,pid:pid,icode:icode},function(data){
                    layer.close(load2);
                    if(data.code==0)
                    {
                        layer.open({
                            content: '请下载喜脚么技师端APP完善技师信息'
                            ,skin: 'msg'
                            ,time: 2
                        });
                        var t=setTimeout("window.location.href='http://m.pargo24.com/index.php/Home/appDownload?source=techer'",2000)
                    }
                    else
                    {
                        //提示
                        layer.open({
                            content: data.message
                            ,skin: 'msg'
                            ,time: 2
                        });
                        return false;
                    }
                },'json');
            }

        }
    });

    
</script>

<script src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script>
  /*
   * 注意：
   * 1. 所有的JS接口只能在公众号绑定的域名下调用，公众号开发者需要先登录微信公众平台进入“公众号设置”的“功能设置”里填写“JS接口安全域名”。
   * 2. 如果发现在 Android 不能分享自定义内容，请到官网下载最新的包覆盖安装，Android 自定义分享接口需升级至 6.0.2.58 版本及以上。
   * 3. 常见问题及完整 JS-SDK 文档地址：http://mp.weixin.qq.com/wiki/7/aaa137b55fb2e0456bf8dd9148dd613f.html
   *
   * 开发中遇到问题详见文档“附录5-常见错误及解决办法”解决，如仍未能解决可通过以下渠道反馈：
   * 邮箱地址：weixin-open@qq.com
   * 邮件主题：【微信JS-SDK反馈】具体问题
   * 邮件内容说明：用简明的语言描述问题所在，并交代清楚遇到该问题的场景，可附上截屏图片，微信团队会尽快处理你的反馈。
   */
  wx.config({
    // debug: '',
    appId: '<?php echo $signPackage["appId"];?>',
    timestamp: <?php echo $signPackage["timestamp"];?>,
    nonceStr: '<?php echo $signPackage["nonceStr"];?>',
    signature: '<?php echo $signPackage["signature"];?>',
    jsApiList: [
        // 'checkJsApi',
        'onMenuShareTimeline',
        'onMenuShareAppMessage',
        'onMenuShareQQ',
        'onMenuShareWeibo',
        'onMenuShareQZone',
        'hideMenuItems',
        'showMenuItems',
        'hideAllNonBaseMenuItem',
        'showAllNonBaseMenuItem',
        'translateVoice',
        'startRecord',
        'stopRecord',
        'onVoiceRecordEnd',
        'playVoice',
        'onVoicePlayEnd',
        'pauseVoice',
        'stopVoice',
        'uploadVoice',
        'downloadVoice',
        'chooseImage',
        'previewImage',
        'uploadImage',
        'downloadImage',
        'getNetworkType',
        'openLocation',
        'getLocation',
        'hideOptionMenu',
        'showOptionMenu',
        'closeWindow',
        'scanQRCode',
        'chooseWXPay',
        'openProductSpecificView',
        'addCard',
        'chooseCard',
        'openCard'
      // 所有要调用的 API 都要加到这个列表中
    ]
  });
  

    //     wx.checkJsApi({
    
    //     jsApiList: ['onMenuShareTimeline','onMenuShareAppMessage'], // 需要检测的JS接口列表，所有JS接口列表见附录2,
    
    //     success: function(res) {
    
    //     //alert('{"checkResult":{"chooseImage":true},"errMsg":"checkJsApi:ok"}');
    //         // 以键值对的形式返回，可用的api值true，不可用为false
    
    //         // 如：{"checkResult":{"chooseImage":true},"errMsg":"checkJsApi:ok"}
    
    //     }
    
    // });
 
  
  var urls="https://api.xijiaomo.com/tech.php/share/reg_step1?pid=<?php echo $_GET['pid']; ?>";
  
  wx.ready(function () {
    wx.onMenuShareTimeline({
      title: '喜脚么-技师注册',
      link: urls,
      imgUrl: 'http://i3.res.meizu.com/fileserver/app_icon/8582/1259713c03474d0ab36971bf8f000253.png',
      trigger: function (res) {
        // 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
        //alert('用户点击分享到朋友圈');
      },
      success: function (res) {
        //alert('已分享');
      },
      cancel: function (res) {
        //alert('已取消');
      },
      fail: function (res) {
        //alert(JSON.stringify(res));
      }
    });
    
  });
</script>
</html>