<!DOCTYPE html>
<?php
/*require_once "php/jssdk.php";
$jssdk = new JSSDK("wx47d4cb38cdc0f293", "4f64efbf42633fd52404ded1c5d1b2ea");
$signPackage = $jssdk->GetSignPackage();*/
?>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no"/>
    <meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
    <meta name="applicable-device" content="mobile">
    <link rel="stylesheet" type="text/css" href="/app/tech/views/share/css/new_file.css"/>
    <link rel="stylesheet" type="text/css" href="/app/tech/views/share/css/set.css"/>
    <link rel="stylesheet" type="text/css" href="/app/tech/views/share/css/medie.css"/>



    <!-- ace styles -->
    <script src="/data/assets/js/jquery-1.10.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/app/tech/views/share/layer_mobile/need/layer.css"/>
    <script src="/app/tech/views/share/layer_mobile/layer.js"></script>

    <style>

        .swiper-container {
            width: 100%;
            height: auto;
            margin: 20px auto;
        }
        .swiper-slide {
            text-align: center;
            font-size: 18px;
            background: #fff;

            /* Center slide text vertically */
            display: -webkit-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            -webkit-justify-content: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            -webkit-align-items: center;
            align-items: center;
        }
    </style>
    <title><?php echo $techer_row['nickname']; ?>个人主页-喜脚么</title>
</head>

<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
    <div class="m_header_bar J_header-bar" >
        <div class="mhb_left">
                    <a class="myposition" href="javascript:history.back(1);"><img class="icon icon-position3" src="/app/tech/views/share/img/fanhui@2x.png"></img></a>
                    
        </div>

        <div class="mhb_center mhb_center_across">
            <h2 class="title">个人主页</h2>
        </div>
        
        <div class="mhb_right">
                    <a class="share" href="" m=""><img class="icon icon-share" src="/app/tech/views/share/img/fenxiang@2x.png"></img></a>
        </div>


    </div>

</header>

<div id="mengban" style="display: none;width: 100%;height: 100%;background: #000;position: fixed;z-index:100;margin-top: 0;opacity:0.7;" ></div>
<!-- Swiper -->

<div class="swiper-container" style="position: absolute;z-index:999;">
    <div class="swiper-wrapper">
        <?php foreach($photo_video as $k=>$v): ?>
            <?php if($v['type']==1){ ?>
                <div class="swiper-slide"><img src="<?php echo $v['pic_url']; ?>" style="width:100%;"></div>
            <?php }else{ ?>
                <div class="swiper-slide"><video src="<?php echo $v['video_url']; ?>" controls="controls" width="414"></div>
            <?php } ?>
        <?php endforeach; ?>

    </div>
</div>
<div id="banner">
    <div class="touxiang">
        <p style="text-align: center;"><img class="touxiang_pho" src="<?php echo $techer_row['headurl']; ?>" style="border-radius: 50%;"/></p>
    </div>
    <div class="jishi_name">
        <p><?php echo $techer_row['nickname']; ?></p>
    </div>
    <div class="my_label">
        <p style="text-align: center;">
            <span class="comment">共<?php echo $commemt_num; ?>条评论</span>
            <span class="goodwill">好感度<?php echo $great_bili; ?>%</span></p>

    </div>

</div>
<div class="my_album">
    <div class="album_left">
        <p>个人相册</p>

    </div>
    <div class="album_center">
        <ul>
            <?php foreach ($techer_photo_result as $k=>$v): ?>
                <?php if($v['type']==1){ ?>
                    <li><img src="<?php echo $v['pic_url']; ?>" height="70"/></li>
                <?php }else{ ?>
                    <li><video src="<?php echo $v['video_url']; ?>" width="57" height="70"/></li>
                <?php } ?>
            <?php endforeach; ?>
        </ul>
    </div>
    <?php if($techer_photo_nums>4){ ?>
        <div class="album_right">
            <p><img src="/app/tech/views/share/img/jiantou@2x.png"/></p>
        </div>
    <?php } ?>
</div>







<div class="my_information">
    <div class="information_list">
        <ul>
            <li>
                        <span class="detailed_title"><img src="/app/tech/views/share/img/jianjie@2x.png" />
                        个人技能</span>
                <span class="detailed_mess"><?php echo $techer_row['desc']; ?></span>

            </li>
            <li>
                        <span class="detailed_title"><img src="/app/tech/views/share/img/renzheng@2x.png" />
                        认证信息</span>
                <span class="detailed_mess"><?php echo substr($techer_row['mobile'],0,3); ?>****<?php echo substr($techer_row['mobile'],7,4); ?></span>
            </li>
            <li>
                        <span class="detailed_title"><img src="/app/tech/views/share/img/pingjia@2x.png" />
                        用户评论<span>（<?php echo $commemt_num; ?>条）</span></span>
            </li>
        </ul>
    </div>
    <div class="detailed_comment">

        <div class="all_label">
            <?php if(empty($tag)){ ?>


            <?php }else{ ?>

                <?php foreach($tag as $key=>$val): ?>
                    <?php foreach($val as $k=>$v):  ?>
                        <?php $arr[]=$v['tag_name'];  ?>
                    <?php endforeach; ?>
                <?php endforeach; ?>
                <?php $arr=array_count_values($arr); foreach($arr as $ks=>$vs): ?>
                    <p><?php echo $ks; ?>(<?php echo $vs; ?>)</p>
                <?php endforeach; ?>
            <?php } ?>
        </div>


        <div class="detailed_content">
            <ul>

                <?php foreach($comment_result as $k=>$v): ?>
                    <li>
                        <div class="up_top">
                            <img src="<?php if(empty($v['headurl'])){ echo 'https://www.pargo24.com/data/assets/images/logo.jpg'; }else{ echo $v['headurl'].'?imageView2/1/w/200/h/200'; }  ?>" style="border-radius: 50%;"/><p class="user_name"><?php echo $v['nick_name']; ?></p>
                            <p class="time_carry"><?php echo $v['dates']; ?></p>
                        </div>
                        <div class="bottom">
                            <p> <?php echo $v['content']; ?></p>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>



    </div>

</div>

<div class="footer" style="display:none;"><p>~没有更多了~</p></div>

<link rel="stylesheet" href="/app/tech/views/share/dist/css/swiper.min.css">
<!-- Swiper JS -->
<script src="/app/tech/views/share/dist/js/swiper.min.js"></script>

<!-- Initialize Swiper -->
<script>
    //横向滑动相册
    var swiper = new Swiper('.swiper-container', {
        autoHeight: true, //高度随内容变化
    });
    $(".swiper-container").hide();

    $(".album_center ul li").click(function(){
        $("#mengban").show();
        $(".swiper-container").show();
        $("body").css("overflow-y","hidden");
    });

    $(".album_right").click(function(){
        $("#mengban").show();
        $(".swiper-container").show();
        $("body").css("overflow-y","hidden");
    });

    $("#mengban").click(function(){
        $("#mengban").hide();
        $(".swiper-container").hide();
        $("body").css("overflow-y","");
    });


    //滑屏
    var i=1;
    $(document).on('scroll', function () {
        var wScrollY = window.scrollY; // 当前滚动条位置
        var wInnerH = window.innerHeight; // 设备窗口的高度（不会变）
        var bScrollH = document.body.scrollHeight; // 滚动条总高度
        if (wScrollY + wInnerH >= bScrollH) {
            var page=i++;
            var count=<?php echo $commemt_num; ?>;
            $(".footer").show();
            $(".footer").html('<p>~正在加载中~</p>');


            var arr=[];
            arr.length = 0;
            if(page*4<count)
            {
                $.get('/tech.php/share/nextpage',{page:page,tid:<?php echo $_GET['tid']; ?>},function(data){

                    if(data.code==0)
                    {
                        var length = $(".detailed_content").find('li').length;
                        length = length-1;

                        for(a=0;a<data.count;a++)
                        {
                            var html='<li><div class="up_top"><img src="'+data.data[a]['headurl']+'?imageView2/1/w/200/h/200" style="border-radius: 50%;box-shadow: 0px 0px 5px #fff;"><p class="user_name">'+data.data[a]['nick_name']+
                                '</p><p class="time_carry">'+data.data[a]['dates']+'</div><div class="bottom"><p>'+data.data[a]['content']+'</p></div></li>';
                            arr.push(html);

                        }

                        $(".detailed_content").find('li').eq(length).after(arr);
                    }
                    else
                    {
                        $(".footer").html('<p>~没有更多了~</p>');
                        return false;
                    }
                },'json');
            }
            else
            {
                $(".footer").html('<p>~没有更多了~</p>');
                return false;
            }


        }
    });


</script>
<script src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script>
  /*
   * 注意：
   * 1. 所有的JS接口只能在公众号绑定的域名下调用，公众号开发者需要先登录微信公众平台进入“公众号设置”的“功能设置”里填写“JS接口安全域名”。
   * 2. 如果发现在 Android 不能分享自定义内容，请到官网下载最新的包覆盖安装，Android 自定义分享接口需升级至 6.0.2.58 版本及以上。
   * 3. 常见问题及完整 JS-SDK 文档地址：http://mp.weixin.qq.com/wiki/7/aaa137b55fb2e0456bf8dd9148dd613f.html
   *
   * 开发中遇到问题详见文档“附录5-常见错误及解决办法”解决，如仍未能解决可通过以下渠道反馈：
   * 邮箱地址：weixin-open@qq.com
   * 邮件主题：【微信JS-SDK反馈】具体问题
   * 邮件内容说明：用简明的语言描述问题所在，并交代清楚遇到该问题的场景，可附上截屏图片，微信团队会尽快处理你的反馈。
   */
  wx.config({
    // debug: '',
    appId: '<?php echo $signPackage["appId"];?>',
    timestamp: <?php echo $signPackage["timestamp"];?>,
    nonceStr: '<?php echo $signPackage["nonceStr"];?>',
    signature: '<?php echo $signPackage["signature"];?>',
    jsApiList: [
        // 'checkJsApi',
        'onMenuShareTimeline',
        'onMenuShareAppMessage',
        'onMenuShareQQ',
        'onMenuShareWeibo',
        'onMenuShareQZone',
        'hideMenuItems',
        'showMenuItems',
        'hideAllNonBaseMenuItem',
        'showAllNonBaseMenuItem',
        'translateVoice',
        'startRecord',
        'stopRecord',
        'onVoiceRecordEnd',
        'playVoice',
        'onVoicePlayEnd',
        'pauseVoice',
        'stopVoice',
        'uploadVoice',
        'downloadVoice',
        'chooseImage',
        'previewImage',
        'uploadImage',
        'downloadImage',
        'getNetworkType',
        'openLocation',
        'getLocation',
        'hideOptionMenu',
        'showOptionMenu',
        'closeWindow',
        'scanQRCode',
        'chooseWXPay',
        'openProductSpecificView',
        'addCard',
        'chooseCard',
        'openCard'
      // 所有要调用的 API 都要加到这个列表中
    ]
  });
  

    //     wx.checkJsApi({
    
    //     jsApiList: ['onMenuShareTimeline','onMenuShareAppMessage'], // 需要检测的JS接口列表，所有JS接口列表见附录2,
    
    //     success: function(res) {
    
    //     //alert('{"checkResult":{"chooseImage":true},"errMsg":"checkJsApi:ok"}');
    //         // 以键值对的形式返回，可用的api值true，不可用为false
    
    //         // 如：{"checkResult":{"chooseImage":true},"errMsg":"checkJsApi:ok"}
    
    //     }
    
    // });
 
  
  var urls="https://api.xijiaomo.com/tech.php/share/wap?tid=<?php echo $_GET['tid']; ?>";
  
  wx.ready(function () {
    wx.onMenuShareTimeline({
      title: '<?php echo $techer_row['nickname']; ?>个人主页-喜脚么',
      link: urls,
      imgUrl: 'http://i4.res.meizu.com/fileserver/app_icon/8582/b1194d54c01a4ae48a45357feaefb370.png',
      trigger: function (res) {
        // 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
        //alert('用户点击分享到朋友圈');
      },
      success: function (res) {
        //alert('已分享');
      },
      cancel: function (res) {
        //alert('已取消');
      },
      fail: function (res) {
        //alert(JSON.stringify(res));
      }
    });
    
  });
</script>

</html>
