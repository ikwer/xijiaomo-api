<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    //首页基本数据
    function main() {
//         $uid = $this->input->post('user_id');
//         $param = $this->input->post();
//         check_sign($param);
        //重新切割
//         $param=decodeParam($param);
//         $uid = $param['user_id'];
        $this->load->model('public_model_v1');
//        $this->load->model('User_model');
//        $this->User_model->
//         if ($uid) {
//             //获取站内信息 
//             $statistics = $this->public_model_v1->notice($uid);
//             if ($statistics['status']) {
//                 $all_num = $statistics['data'];
//             }
//         } else {
//             $all_num = 0;
//         }
        //获取首页banner
        $banner = $this->public_model_v1->banner();
        ajax_return(0, '获取成功', array('banner' => $banner['data']));
    }

    //所有公告信息
    function msg() {
//         $uid = $this->input->post('user_id');
//         $page = $this->input->post('page');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $uid = $param['user_id'];
        $page = $param['page']?$param['page']:1;
//         $page ? $page : $page = 1;
        if (empty(intval($uid))) {
            ajax_return(-1, '缺少重要数据');
        }
        $this->load->model('public_model_v1');
        $rs = $this->public_model_v1->unread($uid, $page);
        if ($rs['data']) {
            ajax_return(0, '成功', $rs['data']);
        } else {
            echo json_encode(array('code' => 0, 'message' => '成功', 'data' => NULL), JSON_UNESCAPED_UNICODE);
        }
    }

    //更改公告状态
    function read_msg() {
//         $uid = $this->input->post('user_id');
//         $msg_id = $this->input->post('msg_id');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $uid = $param['user_id'];
        $msg_id = $param['msg_id'];
        $this->load->model('public_model_v1');
        $rs = $this->public_model_v1->change_msg($uid, $msg_id);
        if ($rs['status']) {
            ajax_return(0, '成功');
        } else {
            ajax_return(-1, '失败');
        }
    }

    //子项目
    function option_list() {
//         $type = $this->input->post('type_id');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $type = $param['goods_type'];
        if (empty(intval($type))) {
            ajax_return(-1, '缺少重要数据');
        }
        $this->load->model('public_model_v1');
        $rs = $this->public_model_v1->option_info($type);
        ajax_return(0, '成功', $rs['data']);
    }

    //特殊要求/禁忌 
    public function special_message() {
//         $goods_type = $this->input->post('goods_id');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $goods_type = $param['goods_id'];
        $this->load->model('public_model_v1');
        $rs = $this->public_model_v1->special_message($goods_type);
        ajax_return(0, '成功', $rs['data']);
    }


    //特殊要求/禁忌
    public function taboo() {
//         $goods_type = $this->input->post('goods_id');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        if(empty($param['goods_type'])){
            ajax_return(-1,'缺少重要参数');
        }
        $goods_type = $param['goods_type'];
        $this->load->model('public_model_v1');
        $rs = $this->public_model_v1->getTaboo($goods_type);
        if($rs){
            ajax_return(0, '成功', $rs);
        }else{
             ajax_return(1, '暂无禁忌说明');
        }
    }
    
    //技师关于我们
    public function techer_about()
    {
        $query=$this->db->select('option')->from('xjm_option')->where("id=1")->limit(1)->get();
        $result=$query->row_array();

        if(empty($result))
        {
            ajax_return(-1, '失败','');
        }
        else
        {
            $arr=json_decode($result['option'],'true');
            ajax_return(0,'成功',$arr);
        }
    }
    
        public function getCityInfo(){
        $already = $this->db->from('city')->where(['aid' > 0,'status'=>'2'])->get()->result_array();
        $soon =  $this->db->from('city')->where(['aid' > 0,'status'=>'1'])->get()->result_array();
        $cityInfo['already'] = $already?$already:'暂无数据';
        $cityInfo['soon'] = $soon?$soon:'暂无数据';
            ajax_return(0,'获取数据成功',$cityInfo);
    }
    
}
