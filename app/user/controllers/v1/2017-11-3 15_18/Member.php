<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Member extends CI_Controller {
    
   public function __construct() {
        parent::__construct();
        $this->load->model('Member_model_v1');
    }
    
    /*
     * 获取用户收藏列表
     */
    public function favoriteData(){
//         $uid = $this->input->post('user_id');
//         $page = $this->input->post('page');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $uid = $param['user_id'];
        $page = $param['page']?$param['page']:1;
        empty(intval($uid)) && ajax_return(-1,'缺少重要参数');
        $favoriteData = $this->Member_model_v1->getFavoriteDataModel($uid,$page);
        foreach($favoriteData as $k=>$v){
            if($v['birthday']){
                $favoriteData[$k]['birthday'] = getAge($v['birthday']);
            }
            if($v['on_time']){
                $favoriteData[$k]['on_time'] = date('Y-m-d',$v['on_time']);
            }
        }
        if($favoriteData){
            ajax_return(0,'获取数据成功',$favoriteData);
        }else{
            ajax_return(0,'暂无数据');
        }
    }
    /*
     * 取消收藏
     */
    public function cancelFavorite(){
//         $id = $this->input->post('tid');
//         $uid = $this->input->post('user_id');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $id = $param['tid'];
        $uid = $param['user_id'];
        if(empty(intval($id)) || empty(intval($uid))){
            ajax_return(-1,'缺少重要参数');
        }
        if($this->Member_model_v1->cancelFavoriteModel($id,$uid)){
            ajax_return(0,'取消收藏成功');
        }else{
            ajax_return(-1,'取消收藏失败');
        }
    }
    
    /*
     * 添加收藏
     */
    public function addFavorite(){
//         $uid = $this->input->post('user_id');
//         $tech_id = $this->input->post('tid');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $uid = $param['user_id'];
        $tech_id = $param['tid'];
        
        if(empty(intval($uid)) || empty(intval($tech_id))){
            ajax_return(-1,'缺少重要参数');
        }
//         if($this->db->select('*')->from('xjm_favorite')->where(['uid'=>$uid,'obj_id'=>$tech_id])->count_all_results()){
//             ajax_return(-1,'不能重复收藏');
//         }
        $insertData['uid'] = $uid;
        $insertData['obj_id'] = $tech_id;
        $insertData['obj_type'] = 'techer';
        $insertData['on_time'] = time();
        if($this->Member_model_v1->commonAddData('xjm_favorite',$insertData)){
            ajax_return(0,'收藏成功');
        }else{
            ajax_return(-1,'收藏失败');
        }
    }
    /*
     * 积分明细
     */
    
    public function pointList(){
//         $uid = $this->input->post('user_id');
//         $page = $this->input->post('page');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $uid = $param['user_id'];
        $page = $param['page']?$param['page']:1;
//         $page =  $page?$page:1;
        if(empty(intval($uid))){
            ajax_return(-1,'缺少重要参数');
        }
          $data =  $this->Member_model_v1->pointListModel($uid,$page);
          if($data){
              ajax_return(0,'获取数据成功',$data);
          }else{
              ajax_return(0,'暂无数据');
          }
    }
    
    /*
     * 可兑换的所有优惠券
     */
    public function couponList(){
//         $uid = $this->input->post('user_id');
//         $page = $this->input->post('page');
//         $page = $page?$page:1;
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $uid = $param['user_id'];
        $page = $param['page']?$param['page']:1;
        if(empty(intval($uid))){
            ajax_return(-1,'缺少重要参数');
        }
        $userPoint = $this->Member_model_v1->getUserPoint($uid);
        $couponList =  $this->Member_model_v1->couponListModel($page);
        foreach($couponList as $k=>$v){
            $couponList[$k]['status'] = 0;
            if($userPoint['point']>$v['expoint']){
                $couponList[$k]['status'] = 1;
            }
            
        }
        if($couponList){
            ajax_return(0,'获取数据成功',$couponList);
        }else{
            ajax_return(0,'暂无数据');
        }
    }
     /*
      * 兑换优惠券
      */
    public function exchCoupon(){
//         $uid = $this->input->post('user_id');
//         $id = $this->input->post('sid');
         $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $uid = $param['user_id'];
        $id = $param['sid'];
        if(empty(intval($uid)) || empty(intval($id))){
            ajax_return(-1,'缺少重要参数');
        }
        $couponInfo = $this->Member_model_v1->getCouponModel($id);
        $userInfo = $this->Member_model_v1->getUserPoint($uid);
        if($couponInfo && $userInfo){
            if($couponInfo['expoint']>$userInfo['point']){
                ajax_return(-1,'积分不够');
            }
            $data['uid'] = $uid;
            $data['cid'] = $id;
            $data['name'] = $couponInfo['name'];
            $data['type'] = 1;
            $data['from'] = 4;
            $data['money'] = $couponInfo['money'];
            $data['send_time'] = time();
            $data['end_time'] = strtotime("+".$couponInfo['validity_time']." day");
            $data['status'] = 0;
            if($this->Member_model_v1->commonAddData('xjm_user_coupon',$data)){
                $point = $userInfo['point'] - $couponInfo['expoint'];
                $this->db->where(['uid'=>$uid])->update('xjm_user',array('point'=>$point));
                if($this->db->affected_rows()){
                    ajax_return(0,'兑换成功');
                }else{
                    ajax_return(-1,'兑换失败');
                }
            }
        }else{
            ajax_return(-1,'缺少重要参数');
        }
        
    }
    /*
     * 我的积分
     */
    public function totalPoint(){
//         $uid = $this->input->post('user_id');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $uid = $param['user_id'];
        if(empty(intval($uid))){
            ajax_return(-1,'缺少重要参数');
        }
        $userInfo = $this->Member_model_v1->getUserPoint($uid);
        if($userInfo){
            ajax_return(0,'获取数据成功',$userInfo);
        }else{
            ajax_return(0,'暂无数据');
        }
    }
    /*
     * 我的订单
     */
    public function orderList(){
        $uid = $this->input->post('user_id');
        $status = $this->input->post('order_status');
        $page = $this->input->post('page');
        $page = $page?$page:1;
        --$page;
        $page_num = 10;
        if(empty(intval($uid)) || empty(intval($status))){
          ajax_return(-1,'缺少重要参数');
        }
        $where = ' uid = '.$uid;
        switch ($status) {
            case 1:$where .=" and order_status in(0,1,5,6,7,8,9)";break;
            case 2:$where .=" and order_status in(2,4)";break;
            case 3:$where .=" and order_status = 3";break;
        }
        
        
        $rs = $this->db->select(array('a.order_sn','pay_status','a.goods_name','a.goods_price','a.tid','a.add_time','a.order_status','b.goods_id','b.goods_image','is_comment'))
                    ->from('order_list a')
                    ->join('goods b','a.goods_id=b.goods_id')
                    ->where($where)
                    ->limit($page_num,$page_num * $page)
                    ->get();
        $order_info = $rs->result_array();
                    //->limit($page_num)
                   // ->offset($page*$page_num)
                    //->get('order_list');
                    
        if($order_info){
            foreach($order_info as $k=>$v){
                //获取技师信息
                if($v['tid']){
                      $techInfo = $this->Member_model_v1->getTechInfoModel($v['tid']);           
                      $order_info[$k]['tech_name'] = $techInfo['nickname'];
                      $order_info[$k]['mobile'] = $techInfo['mobile'];
                      $order_info[$k]['goods_image'] = $techInfo['headurl'];
                }
                //转换时间
                if($v['add_time']){
                    $order_info[$k]['add_time'] = date('Y-m-d',$v['add_time']);
                }
                //进行中的订单
                 if($status==1){               
                    if($v['pay_status']){
                        if($v['order_status']==0){
                            $order_status_id = 1;//支付成功
                        }elseif(in_array($v['order_status'],array('7','8','9'))){
                            $order_status_id = 2;//退款中
                        }elseif($v['order_status']==5 || $v['order_status'] == 6){
                            $order_status_id = 3;//进行中
                        }
                    }else{
                        $order_status_id = 4;//处理中待支付
                    }
                    $order_status = 1;
                 }
                 //已完成的订单
                 if($status == 2){
                     //if($v['pay_status']){
                           if($v['order_status']==2){
                               $order_status_id = 5;//订单已完成；
                           }
                           if($v['order_status']==4){
                               $order_status_id = 6;//退款已完成；
                           }
                     //}
                     $order_status = 2;
                 }
                 //已取消
               //  $status == 3 && $order_status_id = 7;//订单已失效
                 if($status == 3){
                     $order_status_id = 7;
                     $order_status = 3;
                 }
                $order_info[$k]['order_status_id'] = $order_status_id;
                $order_info[$k]['order_status'] = $order_status;
            }
        }
        if($order_info){
            ajax_return(0,'数据获取成功',$order_info);
        }else{
            ajax_return(0,'暂无数据');
        }
    }
    /*
     * 我的优惠券
     */
    public function allCoupon(){
//         $uid = $this->input->post('user_id');
//         $page = $this->input->post('page');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $uid = $param['user_id'];
        $page = $param['page']?$param['page']:1;
        if(empty(intval($uid))){
            ajax_return(-1,'缺少重要参数');
        }
        $couponList = $this->Member_model_v1->getCouponByUid($uid,$page);
        if($couponList){
            ajax_return(0,'获取数据成功',$couponList);
        }else{
            ajax_return(0,'暂无数据');
        }
    }
    /*
     * 意见反馈
     */
    public function addFeedback(){
//         $uid = $this->input->post('user_id');
//         $content = $this->input->post('content');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $uid = $param['user_id'];
        $content = $param['content'];
        if(empty(intval($uid)) || empty($content)){
           ajax_return(-1,'缺少重要数据');             
        }
        $data['uid'] = $uid;
        $data['content'] = $content;
        $data['add_time'] = time();
        if($this->Member_model_v1->commonAddData('xjm_feedback',$data)){
            ajax_return(0,'提交成功');
        }else{
            ajax_return(-1,'提交失败');
        }
    }
    /*
     * 上传/修改头像
     */
    public function saveHeadUrl(){
//         $uid = $this->input->post('user_id');
//         $headUrl = $this->input->post('headurl');
        
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param = decodeParam($param);
        $uid = $param['user_id'];
        $headUrl = $param['headurl'];
        if(empty(intval($uid)) || empty($headUrl)){
            ajax_return(-1,'缺少重要数据');
        }
        $data['headurl'] = $headUrl;
       if($this->Member_model_v1->saveHeadUrlModel($uid,$data)){
           ajax_return(0,'数据处理成功');
       }else{
           ajax_return(-1,'数据处理失败');
       }
        
    }
    /*
     * 获取评价标签
     */
    public function getCommentTag(){
//         $uid = $this->input->post('user_id');
//         $goods_type = $this->input->post('goods_type');
//         $type = intval($this->input->post('type'));
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param = decodeParam($param);
        $uid = $param['user_id'];
        $goods_type = $param['goods_type'];
        $type = isset($param['type'])?$param['type']:0;
        if(empty(intval($uid)) || empty($goods_type)){
            ajax_return(-1,'缺少重要数据');
        }
        $rs = $this->db->select("good_tag,bad_tag")->from('xjm_goods_option')
            ->where(['cate_id'=>$goods_type])
            ->get();
        $commentTag =  $rs->row_array(); 
        $type = $type?'bad_tag':'good_tag';
        if($commentTag[$type]){
            $tagArr = explode(',',$commentTag[$type]);
            foreach($tagArr as $k=>$v){
                if($v){
                   $tagList =  $this->Member_model_v1->getTagnameById($v);
                    $tagArr[$k] = $tagList;
                }
            }
            if($tagArr){
                ajax_return(0,'数据获取成功',$tagArr);
            }else{
                ajax_return(-1,'暂无数据');
            }
        }
    }
    
    /*
     * 评价
     */
    public function addComment(){
//       $uid = $this->input->post('user_id');
//       $comment_tag = $this->input->post('comment_tag');
//       $order_id= $this->input->post('order_sn');
//       //$tid= $this->input->post('tid');
//       $type= $this->input->post('type');
//       $content= $this->input->post('content');
      $param = $this->input->post();
      check_sign($param);
      //重新切割
      $param = decodeParam($param);
      $uid = (!empty($param['user_id']))?$param['user_id']:'';
     $comment_tag = '';
     $content = '';
     if(isset($param['comment_tag'])){
          $comment_tag = $param['comment_tag'];
         
     }
      $order_id = $param['order_sn'];
      $type = isset($param['type'])?$param['type']:0;
      if(isset($param['content'])){
          $content = $param['content'];
      }
       $rs = $this->db->select('tid')->from('order_list')->where(['order_sn'=>$order_id])->get();
       $tInfo = $rs->row_array();
      if(empty(intval($uid))  ||  empty($order_id)){
          ajax_return(-1,'缺少重要数据');
      }
      $rs = $this->db->select('*')->from('xjm_user_comment')->where(['uid'=>$uid,'order_id'=>$order_id])->get();
      $res = $rs->row_array();
      if(!$this->db->select('*')->from('xjm_user_comment')->where(['uid'=>$uid,'order_id'=>$order_id])->count_all_results()){
          if($uid){
              $userInfo = $this->Member_model_v1->getUserInfoModel($uid);
              if($userInfo){
                  $data['uid'] = $uid;
                  $data['tid'] = $tInfo['tid'];
                  $data['nick_name'] = $userInfo['nickname'];
                  $data['headurl'] = $userInfo['headurl'];
                  $data['type'] = intval($type);
                  $data['content'] = $content;
                  $data['add_time'] = time();
                  $data['comment_tag'] = $comment_tag;
                  $data['order_id'] = $order_id;
                  if($this->Member_model_v1->commonAddData('xjm_user_comment',$data)){
                      $update['is_comment'] = 1;
                      $this->db->where(['order_sn'=>$order_id])->update('order_list',$update);
                      ajax_return(0,'提交评价成功');
                  }else{
                      ajax_return(0,'提交评价失败');
                  }
              }
          }
      }else{
          ajax_return(-1,'不能重复提交评价');
      }
    }
    /*
     * 修改昵称和手机号
     */
    public function saveUserInfo(){
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
//         ajax_return($param);
//         $uid = $this->input->post('user_id');
//         $nickname = $this->input->post('nickname');
        if(empty($param['user_id']) || empty($param['nickname'])){
            ajax_return(-1,'缺少重要数据');
        }
        $data['nickname'] = $param['nickname'];
        if($this->Member_model_v1->saveHeadUrlModel($param['user_id'],$data)){
            ajax_return(0,'修改成功');
        }else{
            ajax_return(-1,'修改失败');
        }
    }
    
    
    /*
     * 附近的技师
     */
    public function nearTecher() {
//         $longitude = $this->input->post('longitude');
//         $latitude = $this->input->post('latitude');
//         $page = $this->input->post('page');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $longitude = $param['longitude'];
        $latitude = $param['latitude'];
        $page = $param['page']?$param['page']:1;
        if(empty($longitude) || empty($latitude)){
            ajax_return(-1,'缺少重要参数');
        }
        $distance = 5;
        $rows = $this->Member_model_v1->getLocation($longitude,$latitude,$distance,$page);
        if($rows){
            ajax_return(0,'获取数据成功',$rows);
        }else{
            ajax_return(-1,'暂无数据');
        }
    
    }
    function notify()
    {
        $filename = '/usr/share/nginx/html/api/app/notify.txt';
        $filename1 = '/usr/share/nginx/html/api/app/notify1.txt';
        
        require_once('/usr/share/nginx/html/api/system/libraries/Pay/Alipay/aop/AopClient.php');
        $aop = new AopClient;
        $aop->alipayrsaPublicKey = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2JxZD6CSTYkGWjUC54BA6t9VTLvTaTQHngPoEwV/sHZDT65r3GtMGNL5H/QkwSYhb7MzaGVECC6FdeQKDaxNxguSjZpC3NU8mHLLx1L4WLptjbbJeWbTE4ZhZOD7jSJ5C9UKvTTiKpbzSGRr0m3D2Ad04yUPBA2vHihGKozjrISFkDzzOplNQU2F7W/ZL2NPczD2hCJ6oCwjtIUAQJNfM2CAZR7U2j6v3eN5O4WD4cuvDlc8PkUlo+lFvqHMr1jrjsqd4eRIZg97cD/Vr/3WxmBwo0AH8BIoDTYB3N4JhIB7IONNrAp4NssRGPse1cT4hTVUV11SxgejEPryjGA7KQIDAQAB';
        
        //$aop->alipayrsaPublicKey = '请填写支付宝公钥，一行字符串';
        //此处验签方式必须与下单时的签名方式一致
        $flag = $aop->rsaCheckV1($_POST, NULL, "RSA2");
        // file_put_contents($filename, $_POST);
        file_put_contents($filename, $flag);
        // if($_REQUEST['trade_status'])
        if($flag && $_REQUEST['trade_status']){
            $data['pay_status'] = 1;
            $data['order_status'] = 0;
            // $this->db->where('order_sn',$_REQUEST['out_trade_no'])->update('xjm_order_list',$data);
             if($this->db->where('order_sn',$_REQUEST['out_trade_no'])->update('xjm_order_list',$data)){
                $orderInfo = $this->db->select('order_id,order_amount,uid,tid')->from('order_list')->where(['order_sn'=>$_REQUEST['out_trade_no']])->get()->row_array();
                $insert['type'] = 'in';
                $insert['money'] = $orderInfo['order_amount'];
                $insert['uid'] = $orderInfo['uid'];
                $insert['tid'] = $orderInfo['tid'];
                $insert['order_id'] = $_REQUEST['out_trade_no'];
                $insert['pay_time'] = time();
                $this->db->insert('finance',$insert);
                $this->send_ws($orderInfo['order_id']);
            }
        }
    }
    
    //推送到技师ws
    public function send_ws($order_id)
    {
        // 建立socket连接到内部推送端口
        $client = stream_socket_client('tcp://172.16.97.18:56789', $errno, $errmsg, 1);
        // 推送的数据，包含uid字段，表示是给这个uid推送
        $query=$this->db->select('o.tid,o.order_id,o.order_sn,o.address,o.mobile,u.nickname,o.house_number,o.mobile,o.user_note,o.u_lon,o.u_lat')->from('xjm_order_list o')->join('xjm_user u','o.uid=u.uid')->where('o.order_id='.intval($order_id))->limit(1)->get();
        $result=$query->row_array();
        if(!empty($result))
        {
            $data = array('uid'=>base64_encode('tid='.$result['tid']),'code'=>0,'message'=>'恭喜你接单成功','data'=>$result);
            // 发送数据，注意5678端口是Text协议的端口，Text协议需要在数据末尾加上换行符
            fwrite($client, json_encode($data)."\n");
            // 读取推送结果
            return fread($client, 8192);
        }
    
    }
    
    function server_start(){
//         $order_sn = $this->input->post('order_sn');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $order_sn = $param['order_sn'];
        $order_status = (!empty($param['order_status']))?$param['order_status']:'';
        if(empty($order_sn)){
            ajax_return(-1,'缺少重要参数');
        }
        if($order_status == 6){
            $this->db->where(['order_sn'=>$order_sn])->update('order_list',array('order_status'=>$order_status));
        }
        $orderInfoRs = $this->db->select('uid,tid,order_sn,order_status,address,goods_name,add_time,date_type,date_time,goods_id')
                      ->from('order_list')->where(['order_sn'=>$order_sn])->get();
        $orderInfo = $orderInfoRs->result_array();
        foreach($orderInfo as $k=>$v){
            if($v['tid']){
                $tRs = $this->db->select('nickname,mobile')->from('techer')->where(['tid'=>$v['tid']])->get();
                $tInfo = $tRs->row_array();
                $orderInfo[$k]['tnickname'] = $tInfo['nickname'];
                $orderInfo[$k]['mobile'] = $tInfo['mobile'];
            }
            if($v['goods_id']){
                $tRs = $this->db->select('server_time')->from('goods')->where(['goods_id'=>$v['goods_id']])->get();
                $tInfo = $tRs->row_array();
                $orderInfo[$k]['server_time'] = $tInfo['server_time'];
            }
                $estimateDate = date("Y-m-d");
            if(!$v['date_type']){
                $estimateTime = date('H:i',$v['add_time']+30*60);
            }elseif($v['date_type'] == 1){
                $estimateTime = date('H:i',$v['date_time']);
            }elseif($v['date_type'] == 2){
                $estimateTime = date('H:i',$v['date_time']);
                $estimateDate = date("Y-m-d",strtotime("+1 day"));
            }
            $orderInfo[$k]['estimateTime'] = $estimateTime;
            $orderInfo[$k]['estimateDate'] = $estimateDate;
            $orderInfo[$k]['kfmobile'] = $this->config->item('kfmoblie');
        }
        ajax_return(0,'获取数据成功',$orderInfo[0]);       
    }

    function newNotice(){
//      $user_id = $this->input->post('user_id');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $user_id = $param['user_id'];
        if(empty($user_id)){
            ajax_return(-1,'缺少重要参数');
        }
        $noticeRs= $this->db->select('*')->from('xjm_notice')->where(['uid'=>$user_id,'is_read'=>0])->get();
        $noticeNum = count($noticeRs->result_array());
        if($noticeNum){
            ajax_return(0,'获取数据成功',$noticeNum);     
        }else{
            ajax_return(1,'暂无数据');      
        }
    }
   //获取系统服务时间
    function server_time(){
//         $date_type = $this->input->post('date_type');
//         $goods_id = $this->input->post('goods_id');
//         $user_id = $this->input->post('user_id');
        $param = $this->input->post();
        check_sign($param);
        $param=decodeParam($param);
        if(empty($param['goods_id']) || empty($param['user_id']) || empty($param['tid'])){
            ajax_return(-1,'缺少重要参数');
        }
        $date_type = isset($param['date_type'])?$param['date_type']:1;
        $user_id = $param['user_id'];
        $goods_id = $param['goods_id'];
        $tid = $param['tid'];
        $TodayEnd_time = strtotime(date('Y-m-d',strtotime('+1 day')));
        $TodayStart_time = time();
        $TomorrowEnd_time = $TodayEnd_time+86400;
//         $date_type = $date_type?$date_type:1;
        if($date_type==2){
            $now_time = $TodayEnd_time;
            $end_time = $TomorrowEnd_time;
            $date = date("Y-m-d",strtotime("+1 day"));
        }else{
            $now_time = $TodayStart_time;
            $end_time = $TodayEnd_time;
            $date = date("Y-m-d");
        }
            $tomorrow = date("m-d",strtotime("+1 day"));
            $today  = date("m-d");
        if($date_type){
            $serverTimeRs = $this->db->from('server_time')->get();
            $serverTimeArr = $serverTimeRs->result_array();
            $allOrderRs = $this->db->select('date_id')->from('order_list')->where("add_time>".$now_time." and add_time<".$end_time." and tid = $tid")->get();
            $allOrder = $allOrderRs->result_array();
           //已经过期的时间不可选
            foreach($serverTimeArr as $k=>$v){
                $serverTimeArr[$k]['status'] = 1;
                if(strtotime($date.$v['server_time'])<$now_time){
                    // $serverTimeArr[$k]['status'] = 0;
                    unset($serverTimeArr[$k]);
                }
            }
                sort($serverTimeArr);
            //被预约过的时间不可选
            if($allOrder){
                foreach($serverTimeArr as $k=>$v){
                    if($v['status'] == 1){
                        foreach($allOrder as $key =>$val){
                            if($val['date_id'] == $v['id']){
                                $serverTimeArr[$k]['status'] = 0;
                            }
                        }
                    }
                }
            }
            //根据服务时长判断剩下的时间是否可选。
            $rs = $this->db->select('server_time')->from('goods')->where(['goods_id'=>$goods_id])->get();
            $goodsInfo = $rs->row_array();
            $goods_time = $goodsInfo['server_time']*60;
            $goods_time = $goods_time + 30*60;
            $endArr = end($serverTimeArr);
            foreach($serverTimeArr as $k=>$v){
                $k1 = $k+1;
                if($v['id']<$endArr['id']){
                    if($v['status'] == 1 ){
                        if($serverTimeArr[$k1]['status'] ==1){
                            $serverTimeArr[$k]['status'] = 1;
                        }else{
                            $ktime = strtotime($v['server_time'])+$goods_time;
                            $k1_time = $serverTimeArr[$k+1]['server_time'];
                            $k1_time = strtotime($k1_time);
                            if($ktime > $k1_time){
                                $serverTimeArr[$k]['status'] = 0;
                                
                            }
                        }
                    } 
                }
            }
            $data['today'] = $today;
            $data['tomorrow'] = $tomorrow;
            $data['server_time'] = $serverTimeArr;
            if($serverTimeArr){
                ajax_return(0,'数据处理成功',$data);
            }
        }
    }
    
    //结束服务
    function order_over(){
//         $order_id = $this->input->post('order_sn');
        $param = $this->input->post();
        check_sign($param);
        $param=decodeParam($param);
        $order_id = $param['order_sn'];
        if(empty($order_id)){
            ajax_return(-1,'缺少重要参数');
        }
        $orderInfoRs = $this->db->where(['order_sn'=>$order_id])->select('order_sn,order_status,tid,goods_name,goods_totalprice')->from('order_list')->get();
        $orderInfo = $orderInfoRs->row_array();
        $this->db->where(['order_sn'=>$order_id])->update('xjm_order_list',array('order_status'=>2));
        if($this->db->affected_rows()){
            $tInfoRs = $this->db->select('money')->from('techer')->where(['tid'=>$orderInfo['tid']])->get();
            $tMoney = $tInfoRs->row_array();
            $totalMoney = $tMoney['money']+$orderInfo['goods_totalprice'];
            $this->db->where(['tid'=>$orderInfo['tid']])->update('techer',array('money'=>$totalMoney));
            $insert['tid'] = $orderInfo['tid'];
            $insert['order_id'] = $orderInfo['order_sn'];
            $insert['goods_name'] = $orderInfo['goods_name'];
            $insert['change_money'] = $orderInfo['goods_totalprice'];
            $insert['money'] = $totalMoney;
            $insert['from'] = '收入';
            $insert['add_time'] = time();
            $this->db->insert('techer_income',$insert);
            ajax_return(0,'处理数据成功');
        }else{
            ajax_return(-1,'处理数据失败');
        }
    }
    function getTechInfo(){
//         $order_id = $this->input->post('order_sn');
        $param = $this->input->post();
        check_sign($param);
        $param=decodeParam($param);
        $order_id = $param['order_sn'];
        $orderInfoRs = $this->db->select('a.tid,b.headurl')->from('order_list a')->join('techer_checkinfo b','a.tid=b.tid')->where(['order_sn'=>$order_id])->get();
        $orderInfo = $orderInfoRs->row_array();
        $tNicknameRs = $this->db->select('nickname')->from('techer')->where(['tid'=>$orderInfo['tid']])->get();
        $tNickname = $tNicknameRs->row_array();
        $orderInfo['nicknamer'] = $tNickname['nickname'];
        if($orderInfo){
         ajax_return(0,'获取数据成功',$orderInfo);
        }
    }
    function test(){
        var_dump(date('Y-m-d H:i:s','1509461400'));
        
    }
}