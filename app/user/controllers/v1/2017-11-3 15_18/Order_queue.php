<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Order_queue extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('redis');
    }

    /**
     * 开始处理redis队列
     */
    function order_handling() {
        $file_name = FCPATH . 'app\common\rsakey\order_lock';
        $fp = fopen($file_name, "r");
        //锁定文件   独占锁    开始处理数据  ，若当前队列在处理中直接退出
        if (flock($fp, LOCK_EX | LOCK_NB)) {
            $last_order = $this->redis->lget('order_list', -1);
            $data = explode('_', $last_order);
            //判断订单是否过期
            $time = time() - $data[1];
            if ($time > 300) {
                //订单改为已失效  修改状态 直接继续
                $this->load->library('mongodb');
                $mongo_id = new MongoId($data[0]);
                $order_info = $this->mongodb->select('*')
                        ->where(array('_id' => $mongo_id))
                        ->get('order_list');
                print_r($order_info);
                if (!$order_info) {
                    //不存在订单直接删除
                    $this->redis->rPop('order_list');
                }
                $order_info = $order_info[0];
                //未支付/未失效   订单直接改为取消状态，移除队列 移除失败 进行三次
                if ($order_info['order_status'] >= -10 && $order_info['order_status'] <= 10) {
                    $update['order_status'] = -10;
                    $change_order = $this->mongodb->where(array('_id' => $mongo_id))->update('order_list', $update);
                    if ($change_order) {
                        $this->redis->rPop('order_list');
                    }
                    flock($fp, LOCK_UN);
                    fclose($fp);
                    ob_clean();
                    $this->order_handling();
                } else {
                    //已支付订单 加入数据库循环刷新
                    $add['order_id'] = $mongo_id;
                    $add['end_time'] = 0;
                    $this->db->insert('order_auto_finish', $add);
                    $inset_id = $this->db->insert_id();
                    if ($inset_id) {
                        $this->redis->rPop('order_list');
                    }
                }
            } else {
                //睡眠一秒继续
                sleep(2);
                flock($fp, LOCK_UN);
                fclose($fp);
                $this->order_handling();
            }
        } else {
            fclose($fp);
            exit;
        }
    }

    function order_end() {
        
    }

}
