<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('authtoken');
    }

    //颁发token令牌
    public function issue_token() {
//         $uid = $this->input->post('user_id');
//         $mobile = $this->input->post('account');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $uid = $param['user_id'];
        $mobile = $param['account'];
        if (!preg_match('/^1\d{10}/', $mobile) || empty(intval($uid))) {
            ajax_return(-1, '参数格式有误');
        }
        //检测请求用户是否存在
        $this->load->model('User_model_v1');
        $user_info = $this->User_model_v1->user_info($uid, $mobile);
        if (!$user_info['status']) {
            ajax_return(-1, '令牌发放失败,不存在的用户');
        }
        $rs = $this->authtoken->auth_encode($uid);
        if ($rs['status']) {
            ajax_return(0, '请求成功', $rs['data']);
        } else {
            ajax_return(-1, '请求失败,令牌生成失败');
        }
    }

    //验证令牌
    public function check_token() {
//         $token = $this->input->get('token');
//         $uid = $this->input->post('user_id');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $token = $param['token'];
        $uid = $param['user_id'];
        if (empty($uid) || empty($token)) {
            ajax_return(-1, '缺少验证数据');
        }
        $rs = $this->authtoken->autn_decode($uid, $token);
        if ($rs['status']) {
            ajax_return(0, '令牌验证成功');
        } else {
            ajax_return(-1, '令牌验证失败');
        }
    }

}
