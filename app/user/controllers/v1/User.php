<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    /**
     * log_type 1,2,3  1:qq 2"weixin 3:weibo 
     */
    //第三方登录
//    public function exterior_login() {
//        $type = $this->input->post('log_type');
//        $token = $this->input->post('token');
//        if (!in_array(intval($type), array(1, 2, 3), TRUE) || empty($token)) {
//            ajax_return(-1, '非法请求格式');
//        }
//        //检测登录信息
//        $this->load->model('User_model_v1');
//        $check_rs = $this->User_model_v1->check_exterior_login($type, $token);
//        if ($check_rs['status']) {
//            ajax_return(0, '登录成功', $check_rs['data']);
//        } else {
//            ajax_return(-1, '尚未注册');
//        }
//    }
    //账号密码登录
//    public function account_login() {
//        $mobile = $this->input->post('account');
//        $pwd = $this->input->post('pwd');
//        //检测信息真实度
//        if (!preg_match('/^1\d{10}$/', $mobile)) {
//            ajax_return(-1, '手机号码格式有误');
//        }
//        $pwd = privDecrypt($pwd);
//        if (!preg_match('/^[_0-9a-z]{6,16}$/i', $pwd)) {
//            ajax_return(-1, '密码格式有误');
//        }
//        $pwd = sha1(md5(md5($pwd)));
//        $this->load->model('User_model_v1');
//        $rs = $this->User_model_v1->eheck_login($mobile, $pwd);
//        if ($rs['status']) {
//            ajax_return(0, '登录成功', $rs['data']);
//        } else {
//            ajax_return(-1, '账号或密码有误');
//        }
//    }
    //用户(注册/登录）
    public function doregister() {

        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $mobile = (!empty($param['account']))?$param['account']:'';
        $code_id = (!empty($param['code_id']))?$param['code_id']:'';
        $code = (!empty($param['code']))?$param['code']:'';

        //0.本站 1.QQ 2.微博 3.微信
        $log_type=(!empty($param['log_type']))?$param['log_type']:0;
        $openid=(!empty($param['openid']))?$param['openid']:'';
        $nickname=(!empty($param['nickname']))?$param['nickname']:'';
        $headurl=(!empty($param['headurl']))?$param['headurl']:'';
        
            
        //本站
        if($log_type==0)
        {
            //检测信息真实度
            if($mobile!="17778079115"){

                if(empty($mobile) || empty($code_id) || empty($code)){
                    ajax_return(-1,'缺少重要参数');
                }
                if (!preg_match('/^1\d{10}/', $mobile)) {
                    ajax_return(-1, '手机号码格式有误');
                }

                $this->load->model('public_model_v1');
                $check_phone = $this->public_model_v1->check_phone_code($mobile, $code_id, $code);
                if (!$check_phone['status']) {
                    ajax_return(-1, '验证码输入有误');
                }
            }
        }
        else
        {
            if(empty($openid) || empty($nickname) || empty($headurl)){
                ajax_return(-1,'缺少重要参数');
            }
        }
        
        
        
        $this->load->model('User_model_v1');
        $inset = $this->User_model_v1->inset_user($mobile,$log_type,$openid,$nickname,$headurl);
        // file_put_contents('/usr/share/nginx/html/api/bug.txt',$codeCount.','.$mobile.',,'.$code_id.',,'.$code);
        //生成token
        $this->load->library('authtoken');
        $token = $this->authtoken->auth_encode($inset['data']['uid']);
        if ($inset['status'] && $token['status']) {
            $inset['data']['token'] = $token['data'];
            ajax_return(0, $inset['msg'], $inset['data']);
        } else {
            ajax_return(-1, '登录失败请重试');
        }
    }

    //发送注册短信
    public function send_code() {
//         $mobile = $this->input->post('account');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $mobile = $param['account'];
        if (!preg_match('/^1\d{10}/', $mobile)) {
            ajax_return(1002);
        }
        //入库保存
        $code = rand(1000, 9999);
        $this->load->model('public_model_v1');
        $inset_sms = $this->public_model_v1->inset_sms_code($mobile, $code);
        if (empty($inset_sms['status'])) {
            ajax_return(-1, $inset_sms['msg']);
        }
        //聚合短信发送demo
        $this->load->library('juhesms');
        $content = "【喜脚么】您的验证码是$code";
        $send_rs = $this->juhesms->send_phone($mobile, $content);
        if($send_rs['code'] === 0 ){
            $data['code_id'] = $inset_sms['data']['send_id'];
            $data['phone'] = $mobile;
            $data['code'] = $code;
            ajax_return(0, '短信发送成功请注意查收', $data);
        }else{
            ajax_return(-1, '验证码发送失败');
        }
    }

//    //忘记密码
//    public function doforgetpwd() {
//        $mobile = $this->input->post('account');
//        $pwd = $this->input->post('new_pwd');
//        $code_id = $this->input->post('code_id');
//        $code = $this->input->post('code');
//        if (!preg_match('/^1\d{10}/', $mobile)) {
//            ajax_return(-1, '手机号码格式有误');
//        }
//        $pwd = privDecrypt($pwd);
//        if (!preg_match('/^[_0-9a-z]{6,16}$/i', $pwd)) {
//            ajax_return(-1, '密码格式有误');
//        }
//        if (!preg_match('/^\d{6}$/i', $code) || empty(intval($code_id))) {
//            ajax_return(-1, '验证码格式有误');
//        }
//        $this->load->model('public_model_v1');
//        $check_phone = $this->public_model_v1->check_phone_code($mobile, $code_id, $code);
//        if (!$check_phone['status']) {
//            ajax_return(-1, '验证码输入有误');
//        }
//        //开始重置密码
//        $this->load->model('User_model_v1');
//        $pwd = sha1(md5(md5($pwd)));
//        $rechange = $this->User_model_v1->change_pwd($mobile, $pwd);
//        if (!$rechange['status']) {
//            ajax_return(0, '密码修改成功');
//        } else {
//            ajax_return(-1, '密码修改失败');
//        }
//    }
    //更换手机号第一步  验证当前手机号码
    public function change_mobile_first() {
//         $mobile = $this->input->post('account');
//         $code_id = $this->input->post('code_id');
//         $code = $this->input->post('code');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $mobile = $param['account'];
        $code_id = $param['code_id'];
        $code = $param['code'];
        if (!preg_match('/^1\d{10}/', $mobile)) {
            ajax_return(-1, '手机号码格式有误');
        }
        if (!preg_match('/^\d{6}$/i', $code) || empty(intval($code_id))) {
            ajax_return(-1, '验证码格式有误');
        }
        $this->load->model('public_model_v1');
        $check_phone = $this->public_model_v1->check_phone_code($mobile, $code_id, $code);
        if (!$check_phone['status']) {
            ajax_return(-1, '验证码输入有误');
        } else {
            ajax_return(0, '验证成功');
        }
    }

    //更换手机号第二步  验证新手机号码  修改手机号码
    public function change_mobile_second() {
//         $uid = $this->input->post('user_id');
//         $mobile = $this->input->post('account');
//         $new_mobile = $this->input->post('new_account');
//         $code_id = $this->input->post('code_id');
//         $code = $this->input->post('code');
        
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $uid = $param['user_id'];
        $mobile = $param['account'];
        $new_mobile = $param['new_account'];
        $code_id = $param['code_id'];
        $code = $param['code'];
        if (!preg_match('/^1\d{10}/', $mobile) || !preg_match('/^1\d{10}/', $new_mobile)) {
            ajax_return(-1, '手机号码格式有误');
        }
        if (!preg_match('/^\d{6}$/i', $code) || empty(intval($code_id))) {
            ajax_return(-1, '验证码格式有误');
        }
        //验证新手机是否有人使用
        $this->load->model('User_model_v1');
        $phone_check = $this->User_model_v1->check_mobile($new_mobile);
        if ($phone_check['status']) {
            ajax_return(-1, '新手机号码已被使用');
        }
        //验证新手机验证码
        $this->load->model('public_model_v1');
        $check_phone = $this->public_model_v1->check_phone_code($new_mobile, $code_id, $code);
        if (!$check_phone['status']) {
            ajax_return(-1, '验证码输入有误');
        }
        //更换新手机号码
        $change_phone = $this->User_model_v1->change_mobile($uid, $mobile, $new_mobile);
        if ($change_phone['status']) {
            ajax_return(0, '更换成功');
        } else {
            ajax_return(-1, '更换失败');
        }
    }

    public function test() {
        $this->load->library('authtoken');
        echo $this->authtoken->auth_encode(3);
//        echo pubEncrypt('123456');
    }

    public function test2() {
        echo privDecrypt('Z5k/9ELydG9aTxkh968aGAwRPcwLhB8HibKWaHIDmhYvVRQHYkH5nW4/m6jHD/foOToEgNayxDDu0gCXnjptMY0fqsPtrOTqOPH4uKJKB+YoE8brp44kJXuVRXg2ABM8QfFr05nLHclOodZCyN2PN39+LFB0nVgYhXjUME3gnWY=');
    }
    
    /*
     * 家获取七牛云Token
     */
    
    //七牛使用demo
    public function qiniut(){
        $this->load->library('qiniu');
    
        //初始化七牛信息
        header('Access-Control-Allow-Origin:*');
        $bucket = 'xijiaomo';
        $accessKey = 'b2wTxQXjJKx4igvZfSNfp_ZWV5Flg6cHrzJAswaB';
        $secretKey = '5N6gxBSfvKLZRSAuv3yKMTJkiWMfzIIMeCjQsNRW';
        $auth = new Qiniu\Auth($accessKey, $secretKey);
        $upToken = $auth->uploadToken($bucket, null, 3600);//获取上传所需的token
    
        ajax_return(0, 'upToken获取成功', array('upToken'=>$upToken));
    }

}
