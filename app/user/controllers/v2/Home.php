<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    //首页基本数据
    function main() {
//         $uid = $this->input->post('user_id');
//         $param = $this->input->post();
//         check_sign($param);
        //重新切割
//         $param=decodeParam($param);
//         $uid = $param['user_id'];
        $this->load->model('public_model_v2');
//        $this->load->model('User_model');
//        $this->User_model->
//         if ($uid) {
//             //获取站内信息 
//             $statistics = $this->public_model_v2->notice($uid);
//             if ($statistics['status']) {
//                 $all_num = $statistics['data'];
//             }
//         } else {
//             $all_num = 0;
//         }
        //获取首页banner
        $banner = $this->public_model_v2->banner();
        ajax_return(0, '获取成功', array('banner' => $banner['data']));
    }

    //所有公告信息
    function msg() {
//         $uid = $this->input->post('user_id');
//         $page = $this->input->post('page');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $uid = $param['user_id'];
        $page = $param['page']?$param['page']:1;
//         $page ? $page : $page = 1;
        if (empty(intval($uid))) {
            ajax_return(-1, '缺少重要数据');
        }
        $this->load->model('public_model_v2');
        $rs = $this->public_model_v2->unread($uid, $page);
        if ($rs['data']) {
            ajax_return(0, '成功', $rs['data']);
        } else {
            ajax_return(-2, '暂无数据');
            
        }
    }


    //用户免责条款
    public function user_mianze()
    {
        $result=$this->db->select('title,content')->from('xjm_article')->where('id=29')->get()->row_array();
        if(!empty($result)){
            ajax_return(0,'数据获取成功',$result);
        }else{
            ajax_return(-1,'数据获取失败',$result);
        }
    }

    

    //更改公告状态
    function read_msg() {
//         $uid = $this->input->post('user_id');
//         $msg_id = $this->input->post('msg_id');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $uid = $param['user_id'];
        $msg_id = $param['msg_id'];
        $this->load->model('public_model_v2');
        $rs = $this->public_model_v2->change_msg($uid, $msg_id);
        if ($rs['status']) {
            ajax_return(0, '成功');
        } else {
            ajax_return(-1, '失败');
        }
    }

    //子项目
    function option_list() {
//         $type = $this->input->post('type_id');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $type = $param['goods_type'];
        if (empty(intval($type))) {
            ajax_return(-1, '缺少重要数据');
        }
        $this->load->model('public_model_v2');
        $rs = $this->public_model_v2->option_info($type);
        ajax_return(0, '成功', $rs['data']);
    }

    //特殊要求/禁忌 
    public function special_message() {
//         $goods_type = $this->input->post('goods_id');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $goods_type = $param['goods_id'];
        $this->load->model('public_model_v2');
        $rs = $this->public_model_v2->special_message($goods_type);
        ajax_return(0, '成功', $rs['data']);
    }


    //特殊要求/禁忌
    public function taboo() {
//         $goods_type = $this->input->post('goods_id');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        if(empty($param['goods_type'])){
            ajax_return(-1,'缺少重要参数');
        }
        $goods_type = $param['goods_type'];
        $this->load->model('public_model_v2');
        $rs = $this->public_model_v2->getTaboo($goods_type);
        if($rs){
            ajax_return(0, '成功', $rs);
        }else{
             ajax_return(1, '暂无禁忌说明');
        }
    }
    
    //技师关于我们
    public function techer_about()
    {
        $query=$this->db->select('option')->from('xjm_option')->where("id=1")->limit(1)->get();
        $result=$query->row_array();

        if(empty($result))
        {
            ajax_return(-1, '失败','');
        }
        else
        {
            $arr=json_decode($result['option'],'true');
            unset($arr['au_version']);
            unset($arr['at_version']);
            unset($arr['iu_version']);
            unset($arr['it_version']);
            $arr['version']='1.1.0';
            ajax_return(0,'成功',$arr);
        }
    }
    
        public function getCityInfo(){
        $already = $this->db->from('city')->where(['aid' > 0,'status'=>'2'])->get()->result_array();
        $soon =  $this->db->from('city')->where(['aid' > 0,'status'=>'1'])->get()->result_array();
        $cityInfo['already'] = $already?$already:'暂无数据';
        $cityInfo['soon'] = $soon?$soon:'暂无数据';
            ajax_return(0,'获取数据成功',$cityInfo);
    }
    
    
    public function check_update()
    {

        $query=$this->db->select('option')->from('xjm_option')->where("id=1")->limit(1)->get();
        $result=$query->row_array();

        if(empty($result))
        {
            ajax_return(-1, '失败','');
        }
        else
        {
            $arr=json_decode($result['option'],'true');
            $ar=array(
                'android_new_version'=>$arr['au_version'],
                'android_download_url'=>'http://download.pargo24.com/android/beta/app-release.apk',
                'android_is_update'=>0,
            );
            ajax_return(1, '获取成功',$ar);

        }
    }
    
    
    public function check_ios_update()
    {
        $query=$this->db->select('option')->from('xjm_option')->where("id=1")->limit(1)->get();
        $result=$query->row_array();

        if(empty($result))
        {
            ajax_return(-1, '失败','');
        }
        else
        {
            $arr=json_decode($result['option'],'true');
            $ar=array(
                'ios_new_version'=>$arr['it_version'],
                'ios_download_url'=>'xxx',
                'ios_is_update'=>1,
            );
            ajax_return(1, '获取成功',$ar);

        }
        
    }


    public function get_location()
    {
        $param=$this->input->post();
        if(empty($param['order_id']))
        {
           ajax_return(-2,'参数错误'); 
        }

        $order=$this->db->select('tid')->from('xjm_order_list')->where(array('order_sn'=>$param['order_id']))->get()->row_array();
        $param['tid']=intval($order['tid']);
        $redis = new Redis();
        $redis->connect('127.0.0.1', 6379);
        $r=$redis->get('location_'.$param['tid']);
        $cache=unserialize($r);
        
        
        if(!empty($cache)){
            $result=$this->db->select('t.nickname,c.headurl,o.u_lon,o.u_lat')->
            from('xjm_techer t')->
            join('xjm_techer_checkinfo c','t.tid=c.tid','left')->
            join('xjm_order_list o','t.tid=o.tid','left')->
            where(array('t.tid'=>$param['tid'],'o.order_sn'=>$param['order_id']))->
            get()
            ->row_array();
            if(!empty($result)){
                
                $result['distance'] = sphere_distance($result['u_lat'], $result['u_lon'], $cache['lat'], $cache['lon']);
                if($result['distance']<1){
                    $d=$result['distance']*1000;
                    $result['distance']=$d.'米';
                }else{
                    $result['distance']=$result['distance'].'公里';
                }
                (empty($result['headurl']))? $result['headurl']='http://oxll5q098.bkt.clouddn.com/wo.png' : $result['headurl']=$result['headurl'];
                //unset($result['u_lon']);
                //unset($result['u_lat']);
                $result['lat']=$cache['lat'];
                $result['lon']=$cache['lon'];
            }else{
                ajax_return(-1,'暂无数据');
            }    
            // var_dump($result);die;
            ajax_return(0,'数据获取成功',$result);
        }else{
            $result=$this->db->select('t.tid,t.t_lat lat,t.t_lon lon,t.nickname,c.headurl,o.u_lon,o.u_lat')->
            from('xjm_techer t')->
            join('xjm_techer_checkinfo c','t.tid=c.tid','left')->
            join('xjm_order_list o','t.tid=o.tid','left')->
            where(array('t.tid'=>$param['tid'],'o.order_sn'=>$param['order_id']))->
            get()->
            row_array();
            if(!empty($result)){
                
                $result['distance'] = sphere_distance($result['u_lat'], $result['u_lon'], $result['lat'], $result['lon']);
                if($result['distance']<1){
                    $d=$result['distance']*1000;
                    $result['distance']=$d.'米';
                }else{
                    $result['distance']=$result['distance'].'公里';
                }
                (empty($result['headurl']))? $result['headurl']='http://oxll5q098.bkt.clouddn.com/wo.png' : $result['headurl']=$result['headurl'];
                //unset($result['u_lon']);
                //unset($result['u_lat']);
                
                ajax_return(0,'数据获取成功',$result);
            }else{
                ajax_return(-1,'暂无数据');
            }
            
        }
    }
    
}
