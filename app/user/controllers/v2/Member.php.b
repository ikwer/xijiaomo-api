<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Member extends CI_Controller {
    
   public function __construct() {
        parent::__construct();
        $this->load->model('member_model_v2');
    }



    //点击时间段金额返回
    public function time_returns()
    {
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $goods_id=$param['goods_id'];
        $time_id=$param['time_id'];
        $date_type=$param['date_type'];

        $goods=$this->db->select('goods_price,server_time')->from('xjm_goods')->where(array('goods_id'=>$goods_id))->get()->row_array();

        if(!empty($goods))
        {
            //即刻上门
            if($date_type==0){
                $time=time()+$goods['server_time']*60;
                $h=date('G',$time);
                //22点到6点返回补贴后的金额
                if($h>=22 || $h<=6){
                    $money=$goods['goods_price']+20;
                }else{
                    $money=$goods['goods_price'];
                }
                ajax_return(0,'金额获取成功',array('money'=>$money));

                //预约时间
            }else{
                $server_time=$this->db->select('server_time')->from('xjm_server_time')->where(array('id'=>$time_id))->get()->row_array();
                if(!empty($server_time))
                {
                    if($date_type==1){
                        $yuyue=strtotime(date('Y-m-d').$server_time['server_time'].':00');

                    }else{
                        $yuyue=strtotime(date('Y-m-d +1 day').$server_time['server_time'].':00');

                    }
                    $times=$yuyue+$goods['server_time']*60;
                    $h=date('G',$times);

                    if($h>=22 || $h<=6){
                        $money=$goods['goods_price']+20;
                    }else{
                        $money=$goods['goods_price'];
                    }
                }
                else
                {
                    $money=$goods['goods_price'];
                }
                ajax_return(0,'金额获取成功',array('money'=>$money));
            }
        }
        else
        {
            ajax_return(-1,'请求错误');
        }


    }



    
    /*
     * 获取用户收藏列表
     */
    public function favoriteData(){
//         $uid = $this->input->post('user_id');
//         $page = $this->input->post('page');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $uid = (!empty($param['user_id']))?$param['user_id']:'';
        $page = (!empty($param['page']))?$param['page']:1;
        empty(intval($uid)) && ajax_return(-1,'缺少重要参数');
        $favoriteData = $this->member_model_v2->getFavoriteDataModel($uid,$page);
        foreach($favoriteData as $k=>$v){
            if($v['birthday']){
                $favoriteData[$k]['birthday'] = getAge($v['birthday']);
            }
            if($v['on_time']){
                $favoriteData[$k]['on_time'] = date('Y-m-d',$v['on_time']);
            }
             if($v['sid']){
                $commentArr1 = $this->db->from('user_comment')->where(['tid'=>$v['sid']])->get()->result_array();
                $commentArr2 = $this->db->from('user_comment')->where(['tid'=>$v['sid'],'type'=>'0'])->get()->result_array();
                if(count($commentArr1) > 0 && count($commentArr2) > 0){
                    $favoriteData[$k]['probability'] = intval(count($commentArr2)/count($commentArr1)*100).'%';
                }else{
                    $favoriteData[$k]['probability'] = '100%';
                }
                
            }
            
        }
        if($favoriteData){
            ajax_return(0,'获取数据成功',$favoriteData);
        }else{
            ajax_return(1,'暂无数据');
        }
    }
    /*
     * 取消收藏
     */
    public function cancelFavorite(){
//         $id = $this->input->post('tid');
//         $uid = $this->input->post('user_id');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $id = $param['tid'];
        $uid = $param['user_id'];
        if(empty(intval($id)) || empty(intval($uid))){
            ajax_return(-1,'缺少重要参数');
        }
        if($this->member_model_v2->cancelFavoriteModel($id,$uid)){
            ajax_return(0,'取消收藏成功');
        }else{
            ajax_return(-1,'取消收藏失败');
        }
    }
    
    /*
     * 添加收藏
     */
    public function addFavorite(){
//         $uid = $this->input->post('user_id');
//         $tech_id = $this->input->post('tid');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $uid = $param['user_id'];
        $tech_id = $param['tid'];
        
        if(empty(intval($uid)) || empty(intval($tech_id))){
            ajax_return(-1,'缺少重要参数');
        }
//         if($this->db->select('*')->from('xjm_favorite')->where(['uid'=>$uid,'obj_id'=>$tech_id])->count_all_results()){
//             ajax_return(-1,'不能重复收藏');
//         }
        $insertData['uid'] = $uid;
        $insertData['obj_id'] = $tech_id;
        $insertData['obj_type'] = 'techer';
        $insertData['on_time'] = time();
        if($this->member_model_v2->commonAddData('xjm_favorite',$insertData)){
            ajax_return(0,'收藏成功');
        }else{
            ajax_return(-1,'收藏失败');
        }
    }
    /*
     * 积分明细
     */
    
    public function pointList(){
//         $uid = $this->input->post('user_id');
//         $page = $this->input->post('page');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $uid = $param['user_id'];
        $page = $param['page']?$param['page']:1;
//         $page =  $page?$page:1;
        if(empty(intval($uid))){
            ajax_return(-1,'缺少重要参数');
        }
          $data =  $this->member_model_v2->pointListModel($uid,$page);
          if($data){
              ajax_return(0,'获取数据成功',$data);
          }else{
              ajax_return(1,'暂无数据');
          }
    }
    
    /*
     * 可兑换的所有优惠券
     */
    public function couponList(){
//         $uid = $this->input->post('user_id');
//         $page = $this->input->post('page');
//         $page = $page?$page:1;
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $uid = $param['user_id'];
        $page = $param['page']?$param['page']:1;
        if(empty(intval($uid))){
            ajax_return(-1,'缺少重要参数');
        }
        $userPoint = $this->member_model_v2->getUserPoint($uid);
        $couponList =  $this->member_model_v2->couponListModel($page);
        foreach($couponList as $k=>$v){
            $couponList[$k]['status'] = 0;
            if($userPoint['point']>$v['expoint']){
                $couponList[$k]['status'] = 1;
            }
            
        }
        if($couponList){
            ajax_return(0,'获取数据成功',$couponList);
        }else{
            ajax_return(1,'暂无数据');
        }
    }
     /*
      * 兑换优惠券
      */
    public function exchCoupon(){
//         $uid = $this->input->post('user_id');
//         $id = $this->input->post('sid');
         $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $uid = $param['user_id'];
        $id = $param['sid'];
        if(empty(intval($uid)) || empty(intval($id))){
            ajax_return(-1,'缺少重要参数');
        }
        $couponInfo = $this->member_model_v2->getCouponModel($id);
        $userInfo = $this->member_model_v2->getUserPoint($uid);
        if($couponInfo && $userInfo){
             $type = $couponInfo['type'] == 1?2:1;
            if($couponInfo['expoint']>$userInfo['point']){
                ajax_return(-1,'积分不够');
            }
            $data['uid'] = $uid;
            $data['cid'] = $id;
            $data['name'] = $couponInfo['name'];
            $data['type'] = $type;
            $data['from'] = 4;
            $data['condition'] = $couponInfo['condition'];
            $data['money'] = $couponInfo['money'];
            $data['send_time'] = time();
            $data['end_time'] = strtotime("+".$couponInfo['validity_time']." day");
            $data['status'] = 0;
            if($this->member_model_v2->commonAddData('xjm_user_coupon',$data)){
                $pointArr['uid'] = $uid;
                $pointArr['number'] = $couponInfo['expoint'];
                $pointArr['type'] = 1;
                $pointArr['addtime'] = time();
                $pointArr['origin'] = 3;
                $pointArr['origin_name'] = '积分兑换优惠券';
                $this->db->insert('user_point',$pointArr);
                $point = $userInfo['point'] - $couponInfo['expoint'];
                $this->db->where(['uid'=>$uid])->update('xjm_user',array('point'=>$point));
                if($this->db->affected_rows()){
                    ajax_return(0,'兑换成功');
                }else{
                    ajax_return(-1,'兑换失败');
                }
            }
        }else{
            ajax_return(-1,'缺少重要参数');
        }
        
    }
    /*
     * 我的积分
     */
    public function totalPoint(){
//         $uid = $this->input->post('user_id');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $uid = $param['user_id'];
        if(empty(intval($uid))){
            ajax_return(-1,'缺少重要参数');
        }
        $userInfo = $this->member_model_v2->getUserPoint($uid);
        if($userInfo){
            ajax_return(0,'获取数据成功',$userInfo);
        }else{
            ajax_return(1,'暂无数据');
        }
    }
    /*
     * 我的订单
     */
    public function orderList(){
        $uid = $this->input->post('user_id');
        $status = $this->input->post('order_status');
        $page = $this->input->post('page');
        $page = $page?$page:1;
        --$page;
        $page_num = 10;
        if(empty(intval($uid)) || empty(intval($status))){
          ajax_return(-1,'缺少重要参数');
        }
        $where = ' uid = '.$uid;
        switch ($status) {
            case 1:$where .=" and order_status in(0,1,5,6,7,8,9)";break;
            case 2:$where .=" and order_status in(2,4)";break;
            case 3:$where .=" and order_status = 3";break;
        }
        
        
        $rs = $this->db->select(array('a.order_sn','a.is_comment','b.goods_type','pay_status','a.goods_name','a.order_amount goods_price','a.tid','a.add_time','a.order_status','b.goods_id','b.goods_image','a.xiaofei'))
                    ->from('order_list a')
                    ->join('goods b','a.goods_id=b.goods_id')
                    ->where($where)
                    ->order_by('a.add_time','desc')
                    ->limit($page_num,$page_num * $page)
                    ->get();
        $order_info = $rs->result_array();
                    //->limit($page_num)
                   // ->offset($page*$page_num)
                    //->get('order_list');
                    
        if($order_info){
            foreach($order_info as $k=>$v){
                //获取技师信息
                if($v['tid']){
                      $techInfo = $this->member_model_v2->getTechInfoModel($v['tid']);           
                      $order_info[$k]['tech_name'] = $techInfo['nickname'];
                      $order_info[$k]['mobile'] = $techInfo['mobile'];
                      $order_info[$k]['goods_image'] = $techInfo['headurl'];
                }
                //转换时间
                if($v['add_time']){
                    $order_info[$k]['add_time'] = date('Y-m-d',$v['add_time']);
                }
                //进行中的订单
                 if($status==1){               
                    if($v['pay_status']){
                        if($v['order_status']==0){
                            $order_status_id = 1;//支付成功
                        }elseif(in_array($v['order_status'],array('7','8','9'))){
                            $order_status_id = 2;//退款中
                        }elseif($v['order_status']==5 || $v['order_status'] == 6){
                            $order_status_id = 3;//进行中
                        }
                    }else{
                        $order_status_id = 4;//处理中待支付
                    }
                    $order_status = 1;
                 }
                 //已完成的订单
                 if($status == 2){
                     //if($v['pay_status']){
                           if($v['order_status']==2){
                               $order_status_id = 5;//订单已完成；
                           }
                           if($v['order_status']==4){
                               $order_status_id = 6;//退款已完成；
                           }
                     //}
                     $order_status = 2;
                 }
                 //已取消
               //  $status == 3 && $order_status_id = 7;//订单已失效
                 if($status == 3){
                     $order_status_id = 7;
                     $order_status = 3;
                 }
                $order_info[$k]['order_status_id'] = $order_status_id;
                $order_info[$k]['order_status'] = $order_status;
                $order_info[$k]['order_amount'] =$v['order_amount']+$v['xiaofei'];
                unset($order_info[$k]['xiaofei']);
            }
        }
        if($order_info){
            ajax_return(0,'数据获取成功',$order_info);
        }else{
            ajax_return(1,'暂无数据');
        }
    }
    /*
     * 我的优惠券
     */
    public function allCoupon(){
//         $uid = $this->input->post('user_id');
//         $page = $this->input->post('page');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $uid = $param['user_id'];
        $page = $param['page']?$param['page']:1;
        if(empty(intval($uid))){
            ajax_return(-1,'缺少重要参数');
        }
        $couponList = $this->member_model_v2->getCouponByUid($uid,$page);
        if($couponList){
            ajax_return(0,'获取数据成功',$couponList);
        }else{
            ajax_return(1,'暂无数据');
        }
    }
    /*
     * 意见反馈
     */
    public function addFeedback(){
//         $uid = $this->input->post('user_id');
//         $content = $this->input->post('content');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $uid = $param['user_id'];
        $title = $param['title'];
        $content = $param['content'];
        if(empty(intval($uid)) || empty($content) || empty($title)){
           ajax_return(-1,'缺少重要数据');             
        }
        $data['uid'] = $uid;
        $data['content'] = $content;
        $data['add_time'] = time();
        $data['type']=$title;
        if($this->member_model_v2->commonAddData('xjm_feedback',$data)){
            ajax_return(0,'提交成功');
        }else{
            ajax_return(-1,'提交失败');
        }
    }
    /*
     * 上传/修改头像
     */
    public function saveHeadUrl(){
//         $uid = $this->input->post('user_id');
//         $headUrl = $this->input->post('headurl');
        
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param = decodeParam($param);
        $uid = $param['user_id'];
        $headUrl = $param['headurl'];
        if(empty(intval($uid)) || empty($headUrl)){
            ajax_return(-1,'缺少重要数据');
        }
        $data['headurl'] = $headUrl;
       if($this->member_model_v2->saveHeadUrlModel($uid,$data)){
           ajax_return(0,'数据处理成功');
       }else{
           ajax_return(-1,'数据处理失败');
       }
        
    }
    /*
     * 获取评价标签
     */
    public function getCommentTag(){
//         $uid = $this->input->post('user_id');
//         $goods_type = $this->input->post('goods_type');
//         $type = intval($this->input->post('type'));
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param = decodeParam($param);
        $uid = $param['user_id'];
        $goods_type = $param['goods_type'];
        $type = isset($param['type'])?$param['type']:0;
        if(empty(intval($uid)) || empty($goods_type)){
            ajax_return(-1,'缺少重要数据');
        }
        $rs = $this->db->select("good_tag,bad_tag")->from('xjm_goods_option')
            ->where(['cate_id'=>$goods_type])
            ->get();
        $commentTag =  $rs->row_array(); 
        $type = $type?'bad_tag':'good_tag';
        if($commentTag[$type]){
            $tagArr = explode(',',$commentTag[$type]);
            foreach($tagArr as $k=>$v){
                if($v){
                   $tagList =  $this->member_model_v2->getTagnameById($v);
                    $tagArr[$k] = $tagList;
                }
            }
            if($tagArr){
                ajax_return(0,'数据获取成功',$tagArr);
            }else{
                ajax_return(-1,'暂无数据');
            }
        }
    }
    
    /*
     * 评价
     */
    public function addComment(){
//       $uid = $this->input->post('user_id');
//       $comment_tag = $this->input->post('comment_tag');
//       $order_id= $this->input->post('order_sn');
//       //$tid= $this->input->post('tid');
//       $type= $this->input->post('type');
//       $content= $this->input->post('content');
      $param = $this->input->post();
      check_sign($param);
      //重新切割
      $param = decodeParam($param);
      $uid = (!empty($param['user_id']))?$param['user_id']:'';
     $comment_tag = '';
    //  $content = '';
     if(isset($param['comment_tag'])){
          $comment_tag = $param['comment_tag'];
         
     }
      $order_id = $param['order_sn'];
      $type = isset($param['type'])?$param['type']:0;
    //   if(isset($param['content'])){
    //       $content = $param['content'];
    //   }
      $content = (!empty($param['content']))?$param['content']:'默认好评';
       $rs = $this->db->select('tid')->from('order_list')->where(['order_sn'=>$order_id])->get();
       $tInfo = $rs->row_array();
      if(empty(intval($uid))  ||  empty($order_id)){
          ajax_return(-1,'缺少重要数据');
      }
      $rs = $this->db->select('*')->from('xjm_user_comment')->where(['uid'=>$uid,'order_id'=>$order_id])->get();
      $res = $rs->row_array();
      if(!$this->db->select('*')->from('xjm_user_comment')->where(['uid'=>$uid,'order_id'=>$order_id])->count_all_results()){
          if($uid){
              $userInfo = $this->member_model_v2->getUserInfoModel($uid);
              if($userInfo){
                  $data['uid'] = $uid;
                  $data['tid'] = $tInfo['tid'];
                  $data['nick_name'] = $userInfo['nickname'];
                  $data['headurl'] = $userInfo['headurl'];
                  $data['type'] = intval($type);
                  $data['content'] = $content;
                  $data['add_time'] = time();
                  $data['comment_tag'] = $comment_tag;
                  $data['order_id'] = $order_id;
                  if($this->member_model_v2->commonAddData('xjm_user_comment',$data)){
                      $update['is_comment'] = 1;
                      $this->db->where(['order_sn'=>$order_id])->update('order_list',$update);
                      ajax_return(0,'提交评价成功');
                  }else{
                      ajax_return(-1,'提交评价失败');
                  }
              }
          }
      }else{
          ajax_return(-1,'不能重复提交评价');
      }
    }
    /*
     * 修改昵称和手机号
     */
    public function saveUserInfo(){
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
//         ajax_return($param);
//         $uid = $this->input->post('user_id');
//         $nickname = $this->input->post('nickname');
        if(empty($param['user_id']) || empty($param['nickname'])){
            ajax_return(-1,'缺少重要数据');
        }
        $data['nickname'] = $param['nickname'];
        if($this->member_model_v2->saveHeadUrlModel($param['user_id'],$data)){
            ajax_return(0,'修改成功');
        }else{
            ajax_return(-1,'未做任何修改');
        }
    }
    
    
    /*
     * 附近的技师
     */
    public function nearTecher() {
//         $longitude = $this->input->post('longitude');
//         $latitude = $this->input->post('latitude');
//         $page = $this->input->post('page');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $longitude = $param['longitude'];
        $latitude = $param['latitude'];
        $page = $param['page']?$param['page']:1;
        (!empty($param['goods_type'])) ? $goods_type=$param['goods_type'] : $goods_type=1;
        if(empty($longitude) || empty($latitude)){
            ajax_return(-1,'缺少重要参数');
        }
        //距离范围km
        $distance = 100;
        $rows = $this->member_model_v2->getLocation($longitude,$latitude,$distance,$page,$goods_type);
        if($rows){
            ajax_return(0,'获取数据成功',$rows);
        }else{
            ajax_return(1,'暂无数据');
        }
    
    }
    function notify()
    {
        // $filename = '/usr/share/nginx/html/api/app/notify.txt';
        // $filename1 = '/usr/share/nginx/html/api/app/notify1.txt';
        
        require_once('/usr/share/nginx/html/api/system/libraries/Pay/Alipay/aop/AopClient.php');
        $aop = new AopClient;
        $aop->alipayrsaPublicKey = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2JxZD6CSTYkGWjUC54BA6t9VTLvTaTQHngPoEwV/sHZDT65r3GtMGNL5H/QkwSYhb7MzaGVECC6FdeQKDaxNxguSjZpC3NU8mHLLx1L4WLptjbbJeWbTE4ZhZOD7jSJ5C9UKvTTiKpbzSGRr0m3D2Ad04yUPBA2vHihGKozjrISFkDzzOplNQU2F7W/ZL2NPczD2hCJ6oCwjtIUAQJNfM2CAZR7U2j6v3eN5O4WD4cuvDlc8PkUlo+lFvqHMr1jrjsqd4eRIZg97cD/Vr/3WxmBwo0AH8BIoDTYB3N4JhIB7IONNrAp4NssRGPse1cT4hTVUV11SxgejEPryjGA7KQIDAQAB';
        
        //$aop->alipayrsaPublicKey = '请填写支付宝公钥，一行字符串';
        //此处验签方式必须与下单时的签名方式一致
        $flag = $aop->rsaCheckV1($_POST, NULL, "RSA2");
        // file_put_contents($filename, $_POST);
        // if($_REQUEST['trade_status'])
        if($flag && $_REQUEST['trade_status']=="TRADE_SUCCESS"){
            
            
            
         file_put_contents('/usr/share/nginx/html/api/bug.txt', json_encode($_REQUEST));
            if(!empty($_REQUEST['gmt_refund'])){
                echo 'success';
                
            }else{
            
                $data['pay_status'] = 1;
                $data['order_status'] = 0;
                // $this->db->where('order_sn',$_REQUEST['out_trade_no'])->update('xjm_order_list',$data);
                 if($this->db->where('order_sn',$_REQUEST['out_trade_no'])->update('xjm_order_list',$data)){
                    $orderInfo = $this->db->select('mobile,order_id,order_amount,uid,tid,add_time,date_type,date_time')->from('order_list')->where(['order_sn'=>$_REQUEST['out_trade_no']])->get()->row_array();
                    $techInfo = $this->db->select('nickname')->from('techer')->where(['tid'=>$orderInfo['tid']])->get()->row_array();
                    if($orderInfo['date_type']){
                        $arrive_time = date('Y-m-d H:i',$orderInfo['date_time']);
                    }else{
                        $arrive_time = date('Y-m-d H:i',$orderInfo['add_time']+30*60);
                    }
                    if(empty($orderInfo['date_type'])){
                        $this->db->where(['tid'=>$orderInfo['tid']])->update('techer',array('is_lock'=>1));
                    }
                    $insert['type'] = 'out';
                    $insert['money'] = $orderInfo['order_amount'];
                    $insert['uid'] = $orderInfo['uid'];
                    $insert['tid'] = $orderInfo['tid'];
                    $insert['order_id'] = $_REQUEST['out_trade_no'];
                    $insert['pay_time'] = time();
                    $this->db->insert('finance',$insert);
                    $this->load->library('juhesms');
                    $content = "【喜脚么】预约成功！专业技师".$techInfo['nickname']."将于".$arrive_time."上门为您服务，如有疑问请联系17778079115。";
                    $send_rs = $this->juhesms->send_phone($orderInfo['mobile'], $content);
                    if($orderInfo['date_type']==0){
                      $this->send_ws($orderInfo['order_id']);
                      $this->jpush($orderInfo['tid'],'您有一笔新的预约订单');
                    }else{
                      $this->jpush($orderInfo['tid'],'您有一笔新的预约订单');  
                    }
                    
                }
            }
        }
    }
    
    
    
    public function jpush($id,$msg)
    {
      ob_start();
      $url="https://api.xijiaomo.com/jpush/examples/push_example.php?tid=".intval($id).'&msg='.$msg;
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_HEADER, false);
      curl_setopt($ch, CURLOPT_BINARYTRANSFER, false);
      curl_setopt($ch, CURLOPT_TIMEOUT, 60);
      curl_exec($ch);
      curl_close($ch);
      $stream = ob_get_contents();
      ob_end_clean();
      echo "success";
    }
    


    
    //推送到技师ws
    public function send_ws($order_id)
    {
        // 建立socket连接到内部推送端口
       /* $client = stream_socket_client('tcp://172.16.97.18:56789', $errno, $errmsg, 1);
        // 推送的数据，包含uid字段，表示是给这个uid推送
        $query=$this->db->select('o.tid,o.order_id,o.order_sn,o.address,o.mobile,u.nickname,o.house_number,o.mobile,o.user_note,o.u_lon,o.u_lat')->from('xjm_order_list o')->join('xjm_user u','o.uid=u.uid')->where('o.order_id='.intval($order_id))->limit(1)->get();
        $result=$query->row_array();
        if(!empty($result))
        {
            $data = array('uid'=>base64_encode('tid='.$result['tid']),'code'=>0,'message'=>'恭喜你接单成功','data'=>$result);
            // 发送数据，注意5678端口是Text协议的端口，Text协议需要在数据末尾加上换行符
            fwrite($client, json_encode($data)."\n");
            // 读取推送结果
            return fread($client, 8192);
        }*/
        $query=$this->db->select('o.tid,o.order_id,o.order_sn,o.address,o.mobile,u.nickname,o.house_number,o.mobile,o.user_note,o.u_lon,o.u_lat')->from('xjm_order_list o')->join('xjm_user u','o.uid=u.uid','left')->where('o.order_id='.intval($order_id))->limit(1)->get();
         $result=$query->row_array();
        //  echo $this->db->last_query();die;
        // var_dump($result);die;
        //不为空的时候推送消息给技师
        if(!empty($result)){
            //$this->send_sms($result);
            
            $push_api_url="http://api.xijiaomo.com:9595";
            $arr=array("code"=>0,"message"=>"恭喜你接单成功","data"=>$result);
            $post_data = array(
               "type" => "publish",
               "msg" => json_encode($arr),
               "uid" => $result['tid'], 
            );
            ob_start();
            $ch = curl_init ();
            curl_setopt ( $ch, CURLOPT_URL, $push_api_url );
            curl_setopt ( $ch, CURLOPT_POST, 1 );
            curl_setopt ( $ch, CURLOPT_HEADER, 0 );
            curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
            curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_data );
            curl_setopt ($ch, CURLOPT_HTTPHEADER, array("Expect:"));
            $return = curl_exec ( $ch );
            curl_close ( $ch );
            ob_end_clean();
            
            
            return true;
            
        }
    }
    
    
   function send_sms($result)
   {
       //聚合短信发送demo
        $this->load->library('juhesms');
        $content = "【喜脚么】您有新的即时上门订单，订单号：".$result['order_sn']." ，地址：".$result['address']."，客户电话：".$result['mobile'];
        $send_rs = $this->juhesms->send_phone($mobile, $content);
        return true;
   }
    
    function server_start(){
//         $order_sn = $this->input->post('order_sn');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $order_sn = $param['order_sn'];
        $order_status = (!empty($param['order_status']))?$param['order_status']:'';
        if(empty($order_sn)){
            ajax_return(-1,'缺少重要参数');
        }
        if($order_status == 6){
            $this->db->where(['order_sn'=>$order_sn])->update('order_list',array('order_status'=>$order_status));
        }
        $orderInfoRs = $this->db->select('uid,tid,order_sn,order_status,address,goods_name,add_time,date_type,date_time,goods_id')
                      ->from('order_list')->where(['order_sn'=>$order_sn])->get();
        $orderInfo = $orderInfoRs->result_array();
        if(!empty($orderInfo)){
            foreach($orderInfo as $k=>$v){
                if($v['tid']){
                    $tRs = $this->db->select('nickname,mobile')->from('techer')->where(['tid'=>$v['tid']])->get();
                    $tInfo = $tRs->row_array();
                    $orderInfo[$k]['tnickname'] = $tInfo['nickname'];
                    $orderInfo[$k]['mobile'] = $tInfo['mobile'];
                }
                if($v['goods_id']){
                    $tRs = $this->db->select('server_time')->from('goods')->where(['goods_id'=>$v['goods_id']])->get();
                    $tInfo = $tRs->row_array();
                    $orderInfo[$k]['server_time'] = $tInfo['server_time'];
                }
                    $estimateDate = date("Y-m-d");
                if(!$v['date_type']){
                    $estimateTime = date('H:i',$v['add_time']+30*60);
                }elseif($v['date_type'] == 1){
                    $estimateTime = date('H:i',$v['date_time']);
                }elseif($v['date_type'] == 2){
                    $estimateTime = date('H:i',$v['date_time']);
                    $estimateDate = date("Y-m-d",strtotime("+1 day"));
                }
                $orderInfo[$k]['estimateTime'] = $estimateTime;
                $orderInfo[$k]['estimateDate'] = $estimateDate;
                // $orderInfo[$k]['kfmobile'] = $this->config->item('kfmoblie');
            }
             $result=$this->db->select('option')->from('xjm_option')->where("id=1")->limit(1)->get()->row_array();
            $arr = json_decode($result['option'],'true');
            $orderInfo[0]['kfmobile'] = $arr['company_phone'];
            ajax_return(0,'获取数据成功',$orderInfo[0]);     
        }
    }

    function newNotice(){
//      $user_id = $this->input->post('user_id');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $user_id = $param['user_id'];
        if(empty($user_id)){
            ajax_return(-1,'缺少重要参数');
        }
        $noticeRs= $this->db->select('*')->from('xjm_notice')->where(['uid'=>$user_id,'is_read'=>0])->get();
        $noticeNum = count($noticeRs->result_array());
        if($noticeNum){
            ajax_return(0,'获取数据成功',$noticeNum);     
        }else{
            ajax_return(1,'暂无数据');      
        }
    }
    

    /**
     * 非正常时间上门服务+20
     */
    function orderamount(){
        $param = $this->input->post();
        check_sign($param);
       $param=decodeParam($param);
       $goods_id = (!empty($param['goods_id']))?$param['goods_id']:'';
       if(empty($goods_id)){
           ajax_return(-1,'缺少重要参数');
       }
       $goodInfo = $this->db->select('server_time,goods_price')->from('goods')->where(['goods_id'=>$goods_id])->get()->row_array();
       $strp = $goodInfo['server_time']*60 + time() >= strtotime(date('Y-m-d').' 22:00');
       if(time() >= strtotime(date('Y-m-d').' 22:00') || time() <= strtotime(date('Y-m-d').' 06:00') || $strp){
           $data['money'] = $goodInfo['goods_price']+20;
           ajax_return(0,'超过了正常服务时间段+20',$data);
       }else{
           ajax_return(-1,'在正常服务时间内');
       }
    }

    
    
     //获取系统服务时间
    function server_time(){
        $param = $this->input->post();
       check_sign($param);
       $param=decodeParam($param);
        if(empty($param['goods_id']) || empty($param['user_id']) || empty($param['tid'])){
            ajax_return(-1,'缺少重要参数');
        }
        $date_type = isset($param['date_type'])?$param['date_type']:1;
        $user_id = $param['user_id'];
        $goods_id = $param['goods_id'];
        $tid = $param['tid'];
        $TodayEnd_time = strtotime(date('Y-m-d',strtotime('+1 day')));
        $TodayStart_time = time();
        $TomorrowEnd_time = $TodayEnd_time+86400;
//         $date_type = $date_type?$date_type:1;
        $tomorrow = date("m-d",strtotime("+1 day"));
        $today  = date("m-d");
        if($date_type==2){
            $now_time = $TodayEnd_time;
            $end_time = $TomorrowEnd_time;
            $dateFn = date("Y-m-d",strtotime("+1 day"));
            $date = date("Y-m-d",strtotime("+1 day"));
        }else{
            $dateFn = date("Y-m-d");
            $now_time = $TodayStart_time;
            $end_time = $TodayEnd_time;
            $date = date("Y-m-d");
        }
        if($date_type){
            //根据性别展示不同的时间段
            $goodsInfo = $this->db->select('server_time')->from('goods')->where('goods_id',$goods_id)->get()->row_array();
            $sex_arr=$this->db->select('sex')->from('techer_checkinfo')->where(array('tid'=>$tid))->get()->row_array();
            if(!empty($sex_arr) && $sex_arr['sex']==2){
                $serverTimeRs = $this->db->from('server_time')->where(' id>7 AND id<21 ')->get();
            }else{
                $serverTimeRs = $this->db->from('server_time')->get();
            }

            $serverTimeArr = $serverTimeRs->result_array();
            $allOrderRs = $this->db->select('date_id,server_time')->from('order_list')->where("date_type=".$date_type." and tid=".$tid." and order_status in(0,5,6)")->get();
            $allOrder = $allOrderRs->result_array();
            
           //已经过期的时间不可选
            foreach($serverTimeArr as $k=>$v){
                $serverTimeArr[$k]['status'] = 1;
                if(strtotime($date.$v['server_time'])<$now_time){
                    // $serverTimeArr[$k]['status'] = 0;
                    unset($serverTimeArr[$k]);
                }
            }
            sort($serverTimeArr);
            //被预约过的时间不可选
            if($allOrder){
                foreach($serverTimeArr as $k=>$v){
                    if($v['status'] == 1){
                        foreach($allOrder as $key =>$val){
                            if($val['date_id'] == $v['id']){
                                $serverTimeArr[$k]['status'] = 0;
                                $serverTimeArr[$k]['origin'] = 1;
                                $serverTimeArr[$k]['sTime'] = $val['server_time'];
                            }
                        }
                    }
                }
            }
            
            foreach($serverTimeArr as $k=>$v){
                if($k>0){
                    $k1 = $k-1;
                    if(isset($serverTimeArr[$k1]['sTime']) && $serverTimeArr[$k1]['sTime']){
                        if($serverTimeArr[$k1]['sTime'] > 30){
                            $serverTimeArr[$k]['sTime'] = $serverTimeArr[$k1]['sTime']-60;
                            $serverTimeArr[$k]['status'] = 0;
                        }
                    }
                }                
                
            }
            foreach($serverTimeArr as $k=>$v){
              $serverTimeArr[$k]['additional'] =  0;
                if($k>0){
                    $k1 = $k-1;
                    if(isset($v['origin']) && $serverTimeArr[$k1]){
                        $serverTimeArr[$k1]['status'] = 0;
                    }
                }
                if(strtotime($date.$v['server_time']) >= strtotime($date.' 22:00') || strtotime($date.$v['server_time']) <= strtotime($date.' 06:00')){
                    $serverTimeArr[$k]['additional'] =  20;
                    
                }
                ///当前时间距离整点小于30分钟不可选。
                if($date_type == 1){
                    if((time()+30*60) > strtotime($date.$v['server_time'])){
                        unset($serverTimeArr[$k]);
                    }
                    if(strtotime($date.$v['server_time'])+$goodsInfo['server_time']*60 >= strtotime($date.' 22:00')){
                        $serverTimeArr[$k]['additional'] =  20;
                    }
                }
                
            
            }
            
            $is_lock=$this->db->select('is_lock')->from('techer')->where(array('tid'=>$tid))->get()->row_array();
//            var_dump($date_type);die;

            if($is_lock['is_lock']==0){
                if($date_type==1){
                    $is_arr[]=array('id'=>'25', 'server_time'=>'即刻上门', 'status'=>1,'additional'=>0);
                }
                else{
                    $is_arr='';
                }
            }else{
                if($date_type==1){
                    $is_arr[]=array('id'=>'25', 'server_time'=>'即刻上门', 'status'=>0,'additional'=>0);
                }else{
                    $is_arr='';
                }
            }


            (!empty($is_arr)) ? $rea=array_merge($is_arr,$serverTimeArr) : $rea=$serverTimeArr;

            
            
            $data['today'] = $today;
            $data['tomorrow'] = $tomorrow;
            $data['server_time'] = $rea;
            if($serverTimeArr){
                ajax_return(0,'数据处理成功',$data);
            }
        }
    }
    
    
//   //获取系统服务时间
//     function server_time(){
// //         $date_type = $this->input->post('date_type');
// //         $goods_id = $this->input->post('goods_id');
// //         $user_id = $this->input->post('user_id');
//         $param = $this->input->post();
//         check_sign($param);
//         $param=decodeParam($param);
//         if(empty($param['goods_id']) || empty($param['user_id']) || empty($param['tid'])){
//             ajax_return(-1,'缺少重要参数');
//         }
//         $date_type = isset($param['date_type'])?$param['date_type']:1;
//         $user_id = $param['user_id'];
//         $goods_id = $param['goods_id'];
//         $tid = $param['tid'];
//         $TodayEnd_time = strtotime(date('Y-m-d',strtotime('+1 day')));
//         $TodayStart_time = time();
//         $TomorrowEnd_time = $TodayEnd_time+86400;
// //         $date_type = $date_type?$date_type:1;
//         if($date_type==2){
//             $now_time = $TodayEnd_time;
//             $end_time = $TomorrowEnd_time;
//             $date = date("Y-m-d",strtotime("+1 day"));
//         }else{
//             $now_time = $TodayStart_time;
//             $end_time = $TodayEnd_time;
//             $date = date("Y-m-d");
//         }
//             $tomorrow = date("m-d",strtotime("+1 day"));
//             $today  = date("m-d");
//         if($date_type){
//             $serverTimeRs = $this->db->from('server_time')->get();
//             $serverTimeArr = $serverTimeRs->result_array();
//             $allOrderRs = $this->db->select('date_id')->from('order_list')->where("add_time>".$now_time." and add_time<".$end_time." and tid = $tid")->get();
//             $allOrder = $allOrderRs->result_array();
//           //已经过期的时间不可选
//             foreach($serverTimeArr as $k=>$v){
//                 $serverTimeArr[$k]['status'] = 1;
//                 if(strtotime($date.$v['server_time'])<$now_time){
//                     // $serverTimeArr[$k]['status'] = 0;
//                     unset($serverTimeArr[$k]);
//                 }
//             }
//                 sort($serverTimeArr);
//             //被预约过的时间不可选
//             if($allOrder){
//                 foreach($serverTimeArr as $k=>$v){
//                     if($v['status'] == 1){
//                         foreach($allOrder as $key =>$val){
//                             if($val['date_id'] == $v['id']){
//                                 $serverTimeArr[$k]['status'] = 0;
//                             }
//                         }
//                     }
//                 }
//             }
//             //根据服务时长判断剩下的时间是否可选。
//             $rs = $this->db->select('server_time')->from('goods')->where(['goods_id'=>$goods_id])->get();
//             $goodsInfo = $rs->row_array();
//             $goods_time = $goodsInfo['server_time']*60;
//             $goods_time = $goods_time + 30*60;
//             $endArr = end($serverTimeArr);
//             foreach($serverTimeArr as $k=>$v){
//                 $k1 = $k+1;
//                 if($v['id']<$endArr['id']){
//                     if($v['status'] == 1 ){
//                         if($serverTimeArr[$k1]['status'] ==1){
//                             $serverTimeArr[$k]['status'] = 1;
//                         }else{
//                             $ktime = strtotime($v['server_time'])+$goods_time;
//                             $k1_time = $serverTimeArr[$k+1]['server_time'];
//                             $k1_time = strtotime($k1_time);
//                             if($ktime > $k1_time){
//                                 $serverTimeArr[$k]['status'] = 0;
                                
//                             }
//                         }
//                     } 
//                 }
//             }
            
//             $is_lock=$this->db->select('is_lock')->from('techer')->where(array('tid'=>$tid))->get()->row_array();
            
//             if($is_lock['is_lock']==0){
//                 if($date_type==1){
//                     $is_arr[]=array('id'=>'25', 'server_time'=>'即刻上门', 'status'=>1);
//                 }
//                 else{
//                     $is_arr='';
//                 }
//             }else{
//                 if($date_type==1){
//                     $is_arr[]=array('id'=>'25', 'server_time'=>'即刻上门', 'status'=>0);
//                 }else{
//                     $is_arr='';
//                 }
//             }
            

//             (!empty($is_arr)) ? $rea=array_merge($is_arr,$serverTimeArr) : $rea=$serverTimeArr;
            
//             $data['today'] = $today;
//             $data['tomorrow'] = $tomorrow;
//             $data['server_time'] = $rea;
//             if($serverTimeArr){
//                 ajax_return(0,'数据处理成功',$data);
//             }
//         }
//     }
    
    //结束服务
    function order_over(){
//         $order_id = $this->input->post('order_sn');
        $param = $this->input->post();
        check_sign($param);
        $param=decodeParam($param);
        $order_id = $param['order_sn'];
        if(empty($order_id)){
            ajax_return(-1,'缺少重要参数');
        }
        $orderInfoRs = $this->db->where(['order_sn'=>$order_id])->select('uid,order_sn,order_status,tid,goods_name,order_amount,date_type,date_time,add_time')->from('order_list')->get();
        $orderInfo = $orderInfoRs->row_array();
        $this->db->where(['order_sn'=>$order_id])->update('xjm_order_list',array('order_status'=>2));
        if($this->db->affected_rows()){
            $tMoney = $this->db->select('money')->from('techer')->where(['tid'=>$orderInfo['tid']])->get()->row_array();
            $uInfo = $this->db->select('point')->from('user')->where(['uid'=>$orderInfo['uid']])->get()->row_array();
            

           //根据服务结束时间进行判断补贴的钱不进行80%
            if($orderInfo['date_type']==0)
            {
                $time=$orderInfo['add_time']+$orderInfo['server_time']*60;
                $h=date('G',$time);
                if($h>=22 || $h<=6){
                    $amount=$orderInfo['order_amount']-20;
                    $totalMoney = $tMoney['money']+20+$amount*0.8;
                    $insert['change_money'] = $amount*0.8+20;

                }else{
                    $totalMoney = $tMoney['money']+$orderInfo['order_amount']*0.8;
                    $insert['change_money'] = $orderInfo['order_amount']*0.8;
                }
            }else{
                $time=$orderInfo['date_time']+$orderInfo['server_time']*60;
                $h=date('G',$time);
                if($h>=22 || $h<=6){
                    $amount=$orderInfo['order_amount']-20;
                    $totalMoney = $tMoney['money']+20+$amount*0.8;
                    $insert['change_money'] = $amount*0.8+20;
                }else{
                    $totalMoney = $tMoney['money']+$orderInfo['order_amount']*0.8;
                    $insert['change_money'] = $orderInfo['order_amount']*0.8;
                }
            }   


            $insert['tid'] = $orderInfo['tid'];
            $insert['order_id'] = $orderInfo['order_sn'];
            $insert['goods_name'] = $orderInfo['goods_name'];
            $insert['money'] = $totalMoney;
            $insert['from'] = '1';
            $insert['add_time'] = time();
            //给用户新增积分记录
            $insertPoint['uid'] = $orderInfo['uid'];
            $number = $orderInfo['order_amount'] < 1?1:$orderInfo['order_amount'];
            $insertPoint['number'] = $number;
            $insertPoint['type'] = 0;
            $insertPoint['addtime'] = time();
            $insertPoint['origin'] = 2;
            $insertPoint['origin_name'] = '支付订单';
            $uPoint = empty($uInfo['point'])?0:$uInfo['point'];
            $this->db->insert('user_point',$insertPoint);
            $this->db->insert('techer_income',$insert);
            $this->db->where(['uid'=>$orderInfo['uid']])->update('user',array('point'=>$uPoint+$number));
            $this->db->where(['tid'=>$orderInfo['tid']])->update('techer',array('is_lock'=>0,'money'=>$totalMoney));
            ajax_return(0,'处理数据成功');
            $this->jpush($orderInfo['tid'],$orderInfo['order_sn'].'该订单已经完成');
        }else{
            ajax_return(-1,'处理数据失败');
        }
    }
    function getTechInfo(){
//         $order_id = $this->input->post('order_sn');
        $param = $this->input->post();
        check_sign($param);
        $param=decodeParam($param);
        $order_id = $param['order_sn'];
        $orderInfoRs = $this->db->select('a.tid,b.headurl')->from('order_list a')->join('techer_checkinfo b','a.tid=b.tid')->where(['order_sn'=>$order_id])->get();
        $orderInfo = $orderInfoRs->row_array();
        $tNicknameRs = $this->db->select('nickname')->from('techer')->where(['tid'=>$orderInfo['tid']])->get();
        $tNickname = $tNicknameRs->row_array();
        $orderInfo['nicknamer'] = $tNickname['nickname'];
        if($orderInfo){
         ajax_return(0,'获取数据成功',$orderInfo);
        }
    }
      function delHistory(){
        $param = $this->input->post();
        check_sign($param);
        $param=decodeParam($param);
        $address_id = (!empty($param['address_id']))?$param['address_id']:'';
        $uid = (!empty($param['user_id']))?$param['user_id']:'';
        if(empty($address_id) || empty($uid)){
            ajax_return(-1,'缺少重要参数');
        }
        $this->db->where(['address_id'=>$address_id])->delete('user_address');
        if($this->db->affected_rows()){
            ajax_return(0,'删除成功');
        }else{
            ajax_return(-1,'删除失败');
        }
        
    }
    
    //邀请好友
    function getCouponInvitation(){
        $param = $this->input->post();
        check_sign($param);
        $param=decodeParam($param);
        $uid = (!empty($param['user_id']))?$param['user_id']:'';
        $urlToEncode="http://m.pargo24.com/index.php/Home/login?uid=$uid";
        $url = "http://api.xijiaomo.com/tech.php/autosc/qr_code?url=$urlToEncode";
        $couponArr = $this->db->select('name,money')->from('coupon_set')->where('s_type',1)->get()->row_array();
        $couponArr['qr_code'] = $url;
        $couponArr['registerUrl'] = $urlToEncode;
        $couponArr['couponImg'] = 'http://'.$_SERVER['SERVER_NAME'].'/data/assets/images/quan.png';
        $couponArr['money'] &&  $couponArr['money'] = intval($couponArr['money']);
        ajax_return(0,'获取数据成功',$couponArr);
    }
    //我的邀请列表
    function invitationList(){
        $param = $this->input->post();
        check_sign($param);
        $param=decodeParam($param);
        $uid = (!empty($param['user_id']))?$param['user_id']:'';
        $userArr = $this->db->select('s_uid')->from('user')->where('uid='.$uid)->get()->row_array();
        (!empty($userArr['s_uid'])) ? $s_uid = $userArr['s_uid'] : $s_uid =0;
        $userInfo = $this->db->select("REPLACE(mobile, SUBSTR(mobile,4,4), '****') mobile,from_unixtime(add_time,'%Y-%m-%d') add_time")->from('user')->where("uid in($s_uid)")->get()->result_array();
        $userInfos['couponNum'] = count(explode(',',$s_uid));
        $userInfos['list'] = $userInfo;
        if(!empty($userInfo)){
            ajax_return(0,'获取数据成功',$userInfos);
        }else{
            ajax_return(1,'暂无数据');
        }
    }
    
    
}