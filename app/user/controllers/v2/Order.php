<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

    //过期时间 秒
    private $time = 30000;
    //有效距离 米
    private $distance = 3000;

    function __construct() {
        parent::__construct();
//         $token = $this->input->post('token');
//         $uid = $this->input->post('user_id');
        $param = $this->input->post();
        check_sign($param);
        $param = decodeParam($param);
        if (empty($param['token']) || empty($param['user_id'])) {
            ajax_return(-1, '无效的请求,缺少token或者user_id');
        }
        $token = $param['token'];
        $uid = $param['user_id'];
        $this->load->library('authtoken');
        $check_auth = $this->authtoken->autn_decode($uid, $token);
        if (!$check_auth['status']) {
            ajax_return(-5, '令牌验证失败，请重新登录');
        }
    }

    //生成订单
    function place_order() {
        $uid = $this->input->post('user_id');
        $goods_id = $this->input->post('goods_id');
        $goods_type = $this->input->post('goods_type');
        //经纬度
        $u_lon = $this->input->post('u_lon');
        $u_lat = $this->input->post('u_lat');
        //下单地区
        $area_name = $this->input->post('area_name');
        if (empty(intval($uid)) || empty(intval($goods_type)) || empty(intval($goods_id)) || empty($u_lon) || empty($u_lat) || empty($area_name)) {
            ajax_return(-1, '缺少重要参数');
        }
//        $uid = 22;
//        $goods_type = 1;
//        $u_lon = '114.2923164368';
//        $u_lat = '30.5696513533';
//        $area_name = '武汉市';
        //检测mongo里当是否有5分钟内当前项目的单
        $this->load->library('mongodb');
        $time = time() - 300;
        $check_order = $this->mongodb->select('*')
                ->where(array('uid' => $uid, 'option_type' => $goods_id))
                ->where_gt('add_time', $time)
                ->get('order_list');
        $order_id = '';
        if ($check_order) {
            $order_id = json_decode(json_encode($check_order[0]['_id']), TRUE);
        }
        if ($order_id) {
            //返回单号
            $data['order_id'] = $order_id['$id'];
            ajax_return(1, '获取订单信息成功', $data);
        } else {
            //下单
            $this->load->model('order_model_v2');
            //当前地址是否可下单
            $check_area = $this->order_model_v2->open_address($area_name);
            if (!$check_area['status']) {
                ajax_return(-1, '当前城市未开通');
            }
            //获取产品信息
            $pro_info = $this->order_model_v2->product_info($goods_type);
            if (!$pro_info['status']) {
                ajax_return(-1, '当前产品已下架');
            }
            $pro_info = $pro_info['data'];
            //添加mongo 订单状态 0正常状态  1待支付  2已完成  3已失效  4退款
            $inset_time = time();
            $add_mongo['uid'] = $uid;  //用户id
            $add_mongo['tid'] = 0;  //技师id
            $add_mongo['goods_id'] = $goods_id;  //产品id
            $add_mongo['goods_type'] = $goods_type;  //产品类型
            $add_mongo['order_sn'] = date('YmdHis', $inset_time) . $uid;  //订单编号
            $add_mongo['order_status'] = 0;  //订单状态 0正常状态  10待支付  20已支付（待服务）   30服务中（技师以上门）  40服务完成   -10取消   -20 退款
            $add_mongo['pay_status'] = 0;  //支付状态 0未支付  1已支付
            $add_mongo['area_id'] = $check_area['data']['id'];   //订单所属城市
            $add_mongo['address'] = ''; //服务地址
            $add_mongo['house_number'] = '';  //门牌号
            $add_mongo['user_note'] = '';    //用户留言
            $add_mongo['user_notice'] = '';    //用户定义
            $add_mongo['u_lon'] = $u_lon;
            $add_mongo['u_lat'] = $u_lat;
            $add_mongo['mobile'] = 0;                  //手机号
            $add_mongo['goods_name'] = $pro_info['goods_name'];     //名称
            $add_mongo['goods_price'] = $pro_info['goods_price'];   //价格
            $add_mongo['goods_image'] = $pro_info['goods_image'];   //图片
            $add_mongo['server_time'] = $pro_info['server_time'];  //服务器时间
            //  $add_mongo['goods_totalprice'] = 0;   
            $add_mongo['coupon_price'] = 0;    //优惠券抵扣
            $add_mongo['total_amount'] = 0;   //订单总价
            $add_mongo['order_amount'] = 0;   //应付款金额
            $add_mongo['pay_type'] = 0;     //支付类型
            $add_mongo['pay_time'] = 0;     //支付类型
            $add_mongo['add_time'] = $inset_time;   //下单时间
            //下单
            $mongon_rs = $this->mongodb->insert("order_list", $add_mongo);
            if (empty($mongon_rs)) {
                ajax_return(-1, '下单失败,请稍后再试');
            }
            $mongon_rs = json_decode(json_encode($mongon_rs), TRUE);
            //添加redis队列
            $r_inset_id = $mongon_rs['$id'] . '_' . $inset_time;
            $this->load->library('redis');
            $inset_list = $this->redis->lPush('order_list', $r_inset_id);
            //插入失败,连续重试5次，均不成功添加错误日志,订单继续运行
            if ($inset_list != 1) {
                $inset_true = 0;
                for ($index = 0; $index < 5; $index++) {
                    $repetition = $this->redis->lPush('order_list', $r_inset_id);
                    if ($repetition == 1) {
                        $inset_true = 1;
                        break;
                    }
                }
                if ($inset_true !== 1) {
                    $error_msg = date('Ymd:His', $inset_time) . '__添加redis队列失败__用户id__' . $uid . '__mongo插入id__' . $m_inset_id;
                    $error_name = APPPATH . 'logs\error_order_' . date('Y-m-d');
                    file_put_contents($error_name, $error_msg . PHP_EOL, FILE_APPEND);
                }
            }
            $data['order_id'] = $mongon_rs['$id'];
            ajax_return(0, '下单成功', $data);
        }
    }

    //刷新订单
    function refresh_order() {
        $user_id = $this->input->post('user_id');
        $order_id = $this->input->post('order_id');
        $this->load->library('mongodb');
        //获取订单信息
        try {
            $mongo_id = new MongoId($order_id);
        } catch (Exception $exc) {
            ajax_return(-1, '非法请求,订单号格式有误');
        }
        $order_info = $this->mongodb->select('*')
                ->where(array('_id' => $mongo_id))
                ->get('order_list');
        if (empty($order_info[0])) {
            ajax_return(-1, '不存在的订单');
        }
        $order_info = $order_info[0];
        if ($order_info['uid'] != $user_id) {
            ajax_return(-1, '非法请求');
        }
        $time = time() - $order_info['add_time'];
        if ($time > $this->time) {
//            ajax_return(-2, '订单已过期,请重新下单');
        }
        $res = $this->mongodb->select(array('tid', 'headurl', 'truename'))
                ->where(array('udid' => $order_id))
                ->order_by(array('add_time' => -1))
                ->get('tech_grab_order');
        if (empty($res)) {
            $res = NULL;
        }
        $data['banner'] = $order_info['goods_image'];
        $data['tech_list'] = $res;
        ajax_return(0, '成功', $data);
    }

    //技师信息
    public function tech_info() {
//         $user_id = $this->input->post('user_id');
//         $tid = $this->input->post('tid');

        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $user_id = $param['user_id'];
        $tid = $param['tid'];
        if (empty(intval($user_id) || empty(intval($tid)))) {
            ajax_return(-1, '非法请求');
        }
        $this->load->model('tech_model_v2');
        $rs = $this->tech_model_v2->tech_msg($user_id, $tid);
        if ($rs['status']) {
            ajax_return(0, '成功', $rs['data']);
        } else {
            ajax_return(-1, '非法请求');
        }
    }
    
    //技师评价
    public function tech_comment(){
//         $tid = $this->input->post('tid');
//         $page = $this->input->post('page');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $tid = $param['tid'];
        $page = $param['page'];
        if (empty(intval($tid))){
            ajax_return(-1, '缺少重要参数');
        }
        $this->load->model('tech_model_v2');
        $rs = $this->tech_model_v2->getTechComment($tid,$page);
       if($rs['appraise_list']){
           ajax_return(0,'获取数据成功',$rs);
       }else{
           ajax_return(1,'暂无数据');
       }
    }


    //获取系统服务时间段
    public function server_time() {
        $rs = $this->db->select('*')->from('server_time')->get();
        $server_time = $rs->result_array();
        if($server_time){
            ajax_return(0,'获取数据成功',$server_time);
        }else{
            ajax_return(-1,'暂无数据');
        }
    }
    

    //检测技师是否能接单
    public function check_tech() {
//         $t_id = $this->input->post('t_id');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $t_id = $param['t_id'];
        if (empty(intval($t_id))) {
            ajax_return(-1, '非法请求');
        }
        //获取技师信息
        $this->load->model('tech_model_v2');
        $tech_info = $this->tech_model_v2->tech_states($t_id);
        if (!$tech_info['status']) {
            ajax_return(-1, '不存在的技师');
        }
        if ($tech_info['data']['is_online'] != 0 || empty($tech_info['data']['t_lon']) || empty($tech_info['data']['t_lat'])) {
            ajax_return(-1, '技师未上线');
        }
        ajax_return(0, '可以接单');
    }

    //选择地址
    public function select_address() {
//         $user_id = $this->input->post('user_id');
        
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $user_id = $param['user_id'];
        if (empty(intval($user_id))) {
            ajax_return(-1, '非法请求');
        }
        $this->load->model('order_model_v2');
        $rs = $this->order_model_v2->select_address($user_id);
        if ($rs['status']) {
            ajax_return(0, '成功', $rs['data']);
        } else {
            ajax_return(-1, '非法请求');
        }
    }

    //添加下单地址
    public function add_address() {
//         $user_id = $this->input->post('user_id');
//         $address = $this->input->post('address');
//         $house_number = $this->input->post('house_number');
//         $tid = $this->input->post('tid');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $user_id = $param['user_id'];
        $address = $param['address'];
        $house_number = $param['house_number'];
        $tid = $param['tid'];
        if (empty(intval($user_id)) || empty($address) || empty($house_number) || empty($tid)) {
            ajax_return(-1, '非法请求');
        }
        $data['address'] = $address.$house_number;
        $this->load->model('order_model_v2');
        $location = $this->order_model_v2->getLocationByTid($tid);
        $jsonStr =  file_get_contents('http://restapi.amap.com/v3/geocode/geo?&output=json&key=211b7971d38e8df2728fda85fd519e9a&address='.$address.$house_number.'&city=武汉');
        $locationArr = json_decode($jsonStr,true);
        if($locationArr['status'] == '1' && $locationArr['geocodes']){
            $locationDara = explode(',',$locationArr['geocodes'][0]['location']);
            $data['t_lat'] = $locationDara[1];
            $data['t_lon'] = $locationDara[0];
            $res = $this->getDistance($location['t_lat'],$location['t_lon'],$locationDara[1],$locationDara[0]);
            //$res = getDistance($location['t_lat'],$location['t_lon'],$locationDara[1],$locationDara[0]);
            //if($res/1000 > 5){
            if($res/1000 > 20){
                ajax_return(-1, '不在该技师的服务范围内');
            }
        }else{
            ajax_return(-1, '请输入有效地址');
        }
        if($this->order_model_v2->check_address($user_id,$address,$house_number) == true){
            ajax_return(0, '获取地址成功',$data);
        }
        $rs = $this->order_model_v2->add_address($user_id, $address, $house_number);
        if ($rs['status']) {
            ajax_return(0, '添加地址成功', $data);
        } else {
            ajax_return(-1, '添加失败');
        }
    }

    function getDistance($lat1, $lng1, $lat2, $lng2)
    {
        $earthRadius = 6367000; //approximate radius of earth in meters
        $lat1 = ($lat1 * pi() ) / 180;
        $lng1 = ($lng1 * pi() ) / 180;
    
        $lat2 = ($lat2 * pi() ) / 180;
        $lng2 = ($lng2 * pi() ) / 180;
        $calcLongitude = $lng2 - $lng1;
        $calcLatitude = $lat2 - $lat1;
        $stepOne = pow(sin($calcLatitude / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($calcLongitude / 2), 2);
        $stepTwo = 2 * asin(min(1, sqrt($stepOne)));
        $calculatedDistance = $earthRadius * $stepTwo;
        return round($calculatedDistance);
    }
    
    
    //可用优惠卷数量
    public function usable_num() {
//         $user_id = $this->input->post('user_id');
//         $pro_id = $this->input->post('pro_id');
//        $pro_num = $this->input->post('pro_num');
//         $address = $this->input->post('address');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        
        $user_id = $param['user_id'];
         $pro_id = $param['pro_id'];
         $address = $param['address'];
        if (empty(intval($user_id)) || empty(intval($pro_id)) || empty($address)) {
            ajax_return(-1, '请求参数有误');
        }
        //获取商品信息
        $this->load->model('order_model_v2');
        $pro_info = $this->order_model_v2->product_info($pro_id);
        if (!$pro_info['status']) {
            ajax_return(-1, '不存在的项目');
        }
        //查看当前用户下单地区id 判断优惠卷是否可使用
        $area = $this->order_model_v2->open_address($address);
        $area_id = 0;
        if ($area['status']) {
            $area_id = $area['data']['id'];
        }
        
        $order_price = $pro_info['data']['goods_price'];
        $coupon_rs = $this->order_model_v2->coupon_num($user_id, $order_price, $area_id);
        // var_dump($coupon_rs['data']);
        ajax_return(0, '成功', $coupon_rs['data']);
    }

    //有效优惠卷
    public function coupon() {
//         $user_id = $this->input->post('user_id');
//         $pro_id = $this->input->post('pro_id');
//         $address = $this->input->post('address');
        
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        
//         $page = $this->input->post('page');
        
        $user_id = $param['user_id'];
        $pro_id = $param['pro_id'];
        $address = $param['address'];
        $page = $param['page']?$param['page']:1;
        if (empty(intval($user_id)) || empty(intval($pro_id)) || empty($address)) {
            ajax_return(-1, '请求参数有误');
        }
        //获取商品信息
        $this->load->model('order_model_v2');
        $pro_info = $this->order_model_v2->product_info($pro_id);
        if (!$pro_info['status']) {
            ajax_return(-1, '不存在的项目');
        }
        //查看当前用户下单地区id 判断优惠卷是否可使用
        $area = $this->order_model_v2->open_address($address);
        $area_id = 0;
        if ($area['status']) {
            $area_id = $area['data']['id'];
        }else{
            ajax_return(-1, '请填写正确的下单地址');
        }
        
        //获取优惠卷列表
        $coupon_rs = $this->order_model_v2->coupon_list($user_id, $page, $area_id);
        if ($coupon_rs['data']) {
            $order_price = $pro_info['data']['goods_price'];
            $use_coupon = $no_coupon = array();
            foreach ($coupon_rs['data'] as $key => $value) {
                $value['send_time'] = date('Y-m-d', $value['send_time']);
                $value['end_time'] = date('Y-m-d', $value['end_time']);
                if (!$value['area_name']) {
                    $value['area_name'] = '不限地区使用';
                } else {
                    $value['area_name'] = '限' . $value['area_name'] . '地区使用';
                }
                //可以使用  无门槛  总价大于起始价  在当前下单区  0可以使用  0不能使用
                if (($value['condition'] <= $order_price) && ($area_id == $value['area_id'] || $value['area_id'] == 0)) {
                    $value['is_use'] = '0';
                    $use_coupon[] = $value;
                } else {
                    $value['is_use'] = '1';
                    $no_coupon[] = $value;
                }
            }
            
            $all_coupon = array_merge($use_coupon, $no_coupon);
            $data['coupon_list'] = $all_coupon ? $all_coupon : NULL;
            $data['can_use_num'] = count($use_coupon);
            ajax_return(0, '成功', $data);
        } else {
            ajax_return(-1, '暂无优惠券',null);
        }
    }

    //确认预约
    /*public function confirm_order() {
        $user_id = $this->input->post('user_id');
        $t_id = $this->input->post('t_id');
        $goods_id = $this->input->post('goods_id');
        $order_id = $this->input->post('order_id');
        //基本信息
        $mobile = $this->input->post('mobile');
        $area = $this->input->post('area');
        $address = $this->input->post('address');
        $user_note = $this->input->post('user_note');
        $user_notice = $this->input->post('user_notice');
        if (!preg_match('/^1\d{10}$/', $mobile)) {
            ajax_return(-1, '手机号码格式有误');
        }
        //优惠卷
        $coupon_id = $this->input->post('coupon_id');
        if (empty(intval($user_id)) || empty(intval($goods_id)) || empty($order_id) || empty($address)) {
            ajax_return(-1, '请求参数有误');
        }
        $this->load->model('order_model');
        $this->load->library('mongodb');
        //获取订单信息  5分钟有效
        $time = time() - $this->time;
        try {
            $mongo_id = new MongoId($order_id);
            $order_info = $this->mongodb->select('*')
                    ->where(array('_id' => $mongo_id, 'order_status' => 0, 'pay_status' => 0))
                    ->where_gt('add_time', $time)
                    ->get('order_list');
            if (empty($order_info[0])) {
                ajax_return(-1, '订单已过期失效,请重新下单');
            }
            $order_info = $order_info[0];
        } catch (Exception $exc) {
            ajax_return(-1, '订单参数有误');
        }
        //参数检测，避免违法数据
        if ($order_info['uid'] != $user_id || $order_info['goods_id'] != $goods_id) {
            ajax_return(-1, '非法操作订单1');
        }
        //获取下单地区id 检测地址是否有误
        $address_id = $this->order_model->open_address($area);
        if (!$address_id['status']) {
            ajax_return(-1, '下单地区不存在');
        }
        //检测优惠卷是否可用  计算价钱
        $money = $order_info['goods_price'];
        $coupon_price = 0; //优惠券抵扣
        if ($coupon_id) {
            $coupon_rs = $this->order_model->check_coupon($user_id, $coupon_id, $order_info['goods_price']);
            if (!$coupon_rs['status']) {
                ajax_return(-1, '非法操作订单,没有优惠卷');
            }
            if ($coupon_rs['data']['end_time'] < time()) {
                ajax_return(-1, '已过期优惠卷,无法使用');
            }
            $coupon_price = $coupon_rs['data']['money'];
            $money = $order_info['goods_price'] - $coupon_price;
        }
        //开始下单，预订订单,修改状态 //订单状态 0正常状态  10待支付  20已支付（待服务）   30服务中（技师以上门）  40服务完成   -10取消   -20 退款
        $this->db->trans_begin();
        //锁定优惠卷
        if ($coupon_id) {
            $lock_rs = $this->order_model->lock_coupon($coupon_id);
            if (!$lock_rs) {
                ajax_return(-1, '预订失败,请重试');
            }
        }
        $update['tid'] = $t_id;
        $update['order_status'] = 10;
        $update['address'] = $address;
        $update['user_note'] = $user_note;   //用户备注
        $update['user_notice'] = trim($user_notice, ',');  //用户定义
        $update['mobile'] = $mobile;
        $update['coupon_price'] = $coupon_price;   //优惠券抵扣
        $update['order_amount'] = $money;
        $change_order = $this->mongodb->where(array('_id' => $mongo_id))->update('order_list', $update);
        if ($change_order) {
            $this->db->trans_commit();
            ajax_return(0, '预订成功');
        } else {
            $this->db->trans_rollback();
            ajax_return(-1, '预订失败');
        }
    }*/

    public function confirm_order() {
         $param = $this->input->post();
         check_sign($param);
         $param=decodeParam($param);
         if (empty(intval($param['user_id'])) || empty(intval($param['goods_id'])) || empty($param['address'])) {
            ajax_return(-1, '请求参数有误');
         }
         $user_id =  (!empty($param['user_id']))?$param['user_id']:'';
         $u_lon = (!empty($param['t_lon']))?$param['t_lon']:'';
         $u_lat = (!empty($param['t_lat']))?$param['t_lat']:'';
         $t_id = (!empty($param['t_id']))?$param['t_id']:'';
         $goods_id = (!empty($param['goods_id']))?$param['goods_id']:'';
         $mobile = (!empty($param['mobile']))?$param['mobile']:'';
         $area = (!empty($param['area']))?$param['area']:'';
         $address = (!empty($param['address']))?$param['address']:'';
         $user_note = (!empty($param['user_note']))?$param['user_note']:'';
         $date_type = (!empty($param['date_type']))?$param['date_type']:'';
         $date_time = (!empty($param['date_time']))?$param['date_time']:'';
         $coupon_id = (!empty($param['coupon_id']))?$param['coupon_id']:'';
         $tidStatus = $this->db->select('is_lock')->from('techer')->where(['tid'=>$t_id])->get()->row_array();
         //file_put_contents('/usr/share/nginx/html/api/bug.txt',$param['date_time']);
         $date_time =='即刻上门' && $date_type=0;
         
         if(empty($date_type) && $tidStatus['is_lock']){
             ajax_return(-1, '该技师已被预约');
         }
        if(!empty($param['date_time']) && $date_type){
          $date_type==1?$rs = date('Y-m-d'):$rs =  date("Y-m-d",strtotime("+1 day"));
             $date_time = strtotime($rs.' '.$param['date_time']);
             $dateArr = $this->db->from('server_time')->where(['server_time'=>$param['date_time']])->get()->row_array();
             $date_id = $dateArr['id'];
             
         }else{
             $date_id = '';
             $date_time = '';
         }
         if (!preg_match('/^1\d{10}$/', $mobile)) {
            ajax_return(-1, '手机号码格式有误');
         }
         //优惠卷
        //  $coupon_id = $this->input->post('coupon_id');
         $this->load->model('order_model_v2');
         $goodsInfoRs = $this->db->select("goods_id,goods_name,goods_price,server_time")->from('goods')->where('goods_id',$goods_id)->get();
         $goodsInfo = $goodsInfoRs->row_array();
         if(empty($date_type)){
            $time =  time();
         }else{
              $time =  $date_time;
         }
             $alreadlyOrder = $this->db->select('date_time')->from('order_list')->where('date_time > '.time().' and date_type > 0 and tid='.$t_id.' and order_status in(0,5,6)')->order_by('date_time','ASC')->get()->row_array();
            //   var_dump(time()+($goodsInfo['server_time']+30*60));
            //  var_dump($alreadlyOrder);
            //  file_put_contents('/usr/share/nginx/html/api/bug.txt',$alreadlyOrder['date_time']);
             if($alreadlyOrder && (($time+$goodsInfo['server_time']*60+1800) > $alreadlyOrder['date_time'])){
                 ajax_return(-1, '当前时间不能预约');
             }
        //  if($date_type){
             
             
        //  }
         
        // $this->load->library('mongodb');
         //获取订单信息  5分钟有效
         $time = time() - $this->time;
    //      try {
    //          $mongo_id = new MongoId($order_id);
    //          $order_info = $this->mongodb->select('*')
    //          ->where(array('_id' => $mongo_id, 'order_status' => 0, 'pay_status' => 0))
    //          ->where_gt('add_time', $time)
    //          ->get('order_list');
    //          if (empty($order_info[0])) {
    //             ajax_return(-1, '订单已过期失效,请重新下单');
    //          }
    //         $order_info = $order_info[0];
    //      } catch (Exception $exc) {
    //         ajax_return(-1, '订单参数有误');
    //      }
    //      //参数检测，避免违法数据
    //      if ($order_info['uid'] != $user_id || $order_info['goods_id'] != $goods_id) {
    //      ajax_return(-1, '非法操作订单1');
    //      }
         //获取下单地区id 检测地址是否有误
         /**
          * vivo上线通行证
          */
//          $address_id = $this->order_model_v2->open_address($area);
//          if (!$address_id['status']) {
//          ajax_return(-1, '下单地区不存在');
//          }
         /**
          * vivo上线通行证
          */
         //检测优惠卷是否可用  计算价钱
         $money = $goodsInfo['goods_price'];
         $coupon_price = 0; //优惠券抵扣
         $nowtime = time();
         if ($coupon_id) {
         $coupon_rs = $this->order_model_v2->check_coupon($user_id, $coupon_id, $goodsInfo['goods_price']);
         if (!$coupon_rs['status']) {
         ajax_return(-1, '非法操作订单,没有优惠卷');
         }
         if ($coupon_rs['data']['end_time'] < $nowtime) {
            ajax_return(-1, '已过期优惠卷,无法使用');
         }
         $coupon_price = $coupon_rs['data']['money'];
         $money = $goodsInfo['goods_price'] - $coupon_price;
          $money < 0 && $money = 0;
         }
         //开始下单，预订订单,修改状态 //订单状态 0正常状态  10待支付  20已支付（待服务）   30服务中（技师以上门）  40服务完成   -10取消   -20 退款
    //      $this->db->trans_begin();
         //锁定优惠卷
         if ($coupon_id) {
            $lock_rs = $this->order_model_v2->lock_coupon($coupon_id);
         if (!$lock_rs) {
            ajax_return(-1, '预订失败,请重试');
         }
         }
         $orderId = date('Ymd') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
         $update['uid'] = $user_id;
         $update['tid'] = $t_id;
         $update['order_status'] = 1;
         $update['address'] = $address;
         $update['user_note'] = $user_note;   //用户备注
         $update['order_sn'] = $orderId;  //用户定义
         $update['mobile'] = $mobile;
         $update['goods_name'] = $goodsInfo['goods_name'];
         $update['goods_price'] = $goodsInfo['goods_price'];
         $update['goods_totalprice'] = $goodsInfo['goods_price'];
        // $update['area_id'] = $address_id['data']['id'];
         $update['area_id'] = 0;
         $update['coupon_price'] = $coupon_price;   //优惠券抵扣
         $update['order_amount'] = $money;
         $update['add_time'] = time();
         $update['goods_id'] = $goods_id;
         $update['server_time'] = $goodsInfo['server_time'];
         $update['u_lon'] = $u_lon;
         $update['u_lat'] = $u_lat;
         $update['server_time'] = $goodsInfo['server_time'];
         $update['date_type'] = $date_type;
         $update['date_id'] = $date_id;
         $update['date_time'] = $date_time;


         //根据服务结束时间进行判断补贴的钱不进行80%
        if($update['date_type']==0)
        {
            $time=$update['add_time']+$update['server_time']*60;
            $h=date('G',$time);
            if($h>=22 || $h<=6){

                $update['xiaofei']=20;

            }else{
                $update['xiaofei']=0;
            }
        }else{
            $time=$update['date_time']+$update['server_time']*60;
            $h=date('G',$time);
            if($h>=22 || $h<=6){
                $update['xiaofei']=20;
            }else{
                $update['xiaofei']=0;
            }
        }
         
         $change_order = $this->db->insert('order_list', $update);
         //$order_id = $this->db->insert_id();
         if ($change_order) {
            // $this->db->where(['tid'=>$t_id])->update('techer',array('is_lock'=>1));
             $this->db->trans_commit();
             ajax_return(0, '预订成功',$orderId);
         } else {
             $this->db->trans_rollback();
             ajax_return(-1, '预订失败');
         }
     }
     
     
     //支付宝预支付
    function order_pay(){
        $param = $this->input->post();
        check_sign($param);
        $param = decodeParam($param);
        $uid = $param['user_id'];
        $order_sn = $param['order_sn'];
        $pay_type = $param['pay_type'];
        if(empty($uid) || empty($order_sn) || empty($pay_type)){
            ajax_return(-1,'缺少重要参数');
        }
        $orderRs = $this->db->select('order_sn,order_status,goods_name,goods_price,order_amount,xiaofei')
                       ->where('order_sn',$order_sn)
                       ->from('order_list')
                       ->get();
        $orderInfo = $orderRs->row_array();
        $orderInfo['order_amount']=$orderInfo['order_amount']+$orderInfo['xiaofei'];
        if(intval($pay_type && $orderInfo) == 1){
            //支付宝支付
            $this->alipay_order($orderInfo);
        }else{
            //微信支付
           // $this->alipay_order();
        }
        
    }
     
     
    
     function alipay_order($orderInfo){
         require_once('/usr/share/nginx/html/api/system/libraries/Pay/Alipay/aop/AopClient.php');
         require_once('/usr/share/nginx/html/api/system/libraries/Pay/Alipay/aop/request/AlipayTradeAppPayRequest.php');
         $aop = new AopClient;
         $aop->gatewayUrl = "https://openapi.alipay.com/gateway.do";
         $aop->appId = "2017101809366484";
         //$aop->rsaPrivateKey = '请填写开发者私钥去头去尾去回车，一行字符串';
//          $aop->rsaPrivateKey = 'MIIEpAIBAAKCAQEAxGwu4MJfnLlmuAtcwfenVC+xWdA+Z18jzrVMkMCHZjJCT6mi526J71QmEa5Tc40R8II5Z9nuLtWIebvpphAobsQMZzEXhoYNU1x1rOkTjIzwfj3z7b6AaPvgMrMpmJobNYJAgkyj6aKuYunsJWIK+hbkJDWXJPvdVQcfz/nUzWTEBAUygSDbfYMEKARUhGldtPS4HxrTEWneb+TqHpQNi8NYM+sF1HkAqq9GBtXcE9Q7Tp/n8sJdmva+KX5E7Qc7RnPHwaL2j0CMZj4PMfPQWy6Cix2OZ9yS63siLMzkVFfXzZhNsx4RcgPw5n74hA+Qay0ioWpiaRLPawWA1jv5bQIDAQABAoIBAQCRKOYoAl3IpBeuce82BcDqbWii2EwV9vCuhbu0yiQGOaRZ3SLf6AgWD9kmfe98WMfmN0cXYa4tfG8kj+Pnbs+CtG36IQSizRSyA4WPqJxi5ZGhqkVH8N5TN7mDnnZiKZlsHPtxkBQeyLQZs4hqRB+anSozPEub8GqsuNwgxiAnPUG/Px3W9ZL24kOLSb3U5y8KKj9soNrBP0BPpVDtiVyAyIHOj8xJVpKC9f/8sykMUwXIKWpb3h54siw1mmyzTJd0zT9m9eiTjJ4HoBKIaYmQSWI4SgEnZB3HzqBg5KEv0/NSBGSJVo88+rDgnvtZFENyeWy+WWMqNeLyWnUkLMshAoGBAO3UwFgUciK6a2o9JdOPTKZhN/h5CT8VOE+dpf3RTUofggx8d0c05g5SyXfI6yFkADrGkPJ2Jrswd8dqN3pB/p5WWVEDRV4gJ2irfIjCEFsLV0rqHSNlXBqCFxUtWN5ndExWXzRRiemI3wc2juM0JrQPkimbuCs+ydr4CLYLZbkFAoGBANNtm9FOb5G0N5fQ7lK0YPLrxbRmyDYQV6t2+vvIyOQAu/j+6+jRZxDi5aW35f6gREzsjbIPvKGHa8GcmBFKiYgJTpLyXpdyzBEATHqUKzoaWaS0kr5g1rcnHKQAPEZjAbAl992xFGjB7ijWrVdrYvDR6jAKs3MZ76ncsBQw2wtJAoGAYRfA6nR2YA0g+v85qRPQZAA+fFbVWoaMNZyTfnprBj60teo+O7ixztknXJqOu1P68APOet2XVEYmMCgBvAyjKPrxfaxLLnEMBaQ0tBQd3z1TBsdq299uaXgAemnH5QvxI5UDSvWaytdQzb4NOV46CrpsnkXvOuBoQdeOntsqTQ0CgYEAmTag5/izGR9D+hxJ2F0mtGLvDmA9ThBht7us/bDvA2sUzMfPJRQ6YB5M1N6DKRXy6NuzgwmsZFHCYLed60nciFxWOF6y9YUXJGrXAS1GdgCKXbUNcjCy6F7Lfn09MLSmkIOzL1AXviJq9+U6EFOdi25TCyV2DT8KUkm+v6L32SECgYAUvs/41G9/fJIMUuuhk5ML2PjMip944Zxco45k/CIZEuhs921pPQ53ZuwwqdXjOp/G2LZVr5XnTqiCFupDdznh07V3Nbe6oibcXEh/AFVO2Wb2jAipQcNznxyxkOj7kmeejPhJ0mAqH0FCcqICtz9u8UzTnFB++o97p1BE2HJ0yQ==';
         $aop->rsaPrivateKey = 'MIIEpQIBAAKCAQEA09fNeZc74rE7s+LG07YvO1KzisDPAQLG3oJhYstviXhe3UU8egRNnc+XFwrmQozlXffO2dOnXh/TQWJy2HaCO71tndK32rBH2qzA6+Hvv6xiQaDw8xv6E3GYj/6awYFhyciRd0fmv5oQUfNvvCUIqkbycK5dT4fPyAGLIGpfZCylP7qIG/6duhZ+/8LV4eoTfqxSaGa/KMwDqe5N7cFw3ftO769mK3APO+F37v+hKWPaQ4eYksPANfiN7bPy8n+kB6d+FbPqyojdyWodD8hYRg8TR7jcUPkWsC64iTQL5Lcw/pe7ojR9eNR/DADUzQAwWXnv3zhe2xfRfRQQnm4rkwIDAQABAoIBADt+HoloLHxEc9TX+iiYwHjzh9KOxVOuWPVCPmFFaqR7toCDojFOJhIMq4zgFbxY301de6z24MjxPR3erQwXdAmc8DzjYZi76uIBpAhaoj79BXcS4LgVgkyVmVfPE90GHdVSS2/pNGdYt/6b1CRNbjqLxCha/3+HT5YGwPCwjuuNj2mX3zLCWtLgedf1lnas/lu2RDxElkuRhOsIIo5YXJdZhWIY/2zwqIBndgTDwptfWVSLEnvYo4hyDvQFkFnZGfvwtI2c12lmY+ExM400uDXFJIN5kIIGBgcaPVQ/n8wJykBkVyeyqNm5Ffhn5zjilx5ZdFGB9zvldq4iVckpU7ECgYEA6Y9jX1Ndyh9gClXGqCp4P9g0D8XMluZjZEeCmtwksUjvaLNCAB/yqB09habUNKjxgo1TBQuLEZPvEmUGGNwYN3BQ9aJGbznmI+ILoq2GR2jZXM0PDMW5ER5qZSVob/d6FLZbVZRm4EqASfFoCsgcHi9BzhlEQ1H1nzvghFLBFpkCgYEA6DJDUE4YBVc09uBg0fQYuToNYdqhk7TQoRn+krkb2iOWSU2ZmFBW7HP5abe2CdPcclXn7AUj6Qz6JiTRO1z5KvPoksuCcY+U0CL4GHT55hmnkfHg59JDbcaeTkOxg8HR2iQehyEubcJL6/nevnakumX3KpeeKnLFjXac2y6aqwsCgYEAzT4DDkbdOXw+0nY0H51KlJgyj9W7BrQojXvFyr4/xEcak4BLNH7ep1sisCs9eZUovhfg56MQL383bIu4QupOoZEio+hZSu6vTcMbhHZGdMQvlrxgSFIMYn2+82lfEF2CO2dQdbD2go0VlWT9j9Pv2ZqfkjRj52DDno1oq/9ozKkCgYEA1S5gkvhAIZchb5AuFFUx5c2gv7jFJCGccmy1R+xf3/VQY9i1LhyE0e7gjOk4Xul+uhKZLh7CC5P4jtC6sO/5bDAn1a63AqA6lqWkdn/feB0RtnMGdJCdi8oRSfXoovluPANxa8tRH0CGCA+PK/st3l0Dgr1VX8+kBO9jr/Cn3GUCgYEAzlgaLfaaopFXyPbKmnzMA0R7eTGMIgTJwhWwVlr5OBYAI2z3MQu8kj0ZFCFqJWavTlxbxGOPC3uwJZxXhjf1GviJYTzlsvXlyeZeQzy3bdi+1an2QsBHQZZRPU4u+A7tdy1Rg91MONZmTkthZdVR+Q2KHLZkzhcYNn178kghVGk=';
         $aop->format = "json";
         $aop->charset = "UTF-8";
         $aop->signType = "RSA2";
         $aop->alipayrsaPublicKey = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2JxZD6CSTYkGWjUC54BA6t9VTLvTaTQHngPoEwV/sHZDT65r3GtMGNL5H/QkwSYhb7MzaGVECC6FdeQKDaxNxguSjZpC3NU8mHLLx1L4WLptjbbJeWbTE4ZhZOD7jSJ5C9UKvTTiKpbzSGRr0m3D2Ad04yUPBA2vHihGKozjrISFkDzzOplNQU2F7W/ZL2NPczD2hCJ6oCwjtIUAQJNfM2CAZR7U2j6v3eN5O4WD4cuvDlc8PkUlo+lFvqHMr1jrjsqd4eRIZg97cD/Vr/3WxmBwo0AH8BIoDTYB3N4JhIB7IONNrAp4NssRGPse1cT4hTVUV11SxgejEPryjGA7KQIDAQAB';
        //实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay
         $request = new AlipayTradeAppPayRequest();
         //SDK已经封装掉了公共参数，这里只需要传入业务参数
         $bizcontent = json_encode([
                       'body'=>$orderInfo['goods_name'],
                       'subject'=>$orderInfo['goods_name'],
                       'out_trade_no'=>$orderInfo['order_sn'],//此订单号为商户唯一订单号
                       'total_amount'=> $orderInfo['order_amount'],//保留两位小数
                       'timeout_express'=>'15m',
                       'product_code'=>'QUICK_MSECURITY_PAY'
                  ]);
         $request->setNotifyUrl("https://api.xijiaomo.com/user.php/v1/Member/notify");
         $request->setBizContent($bizcontent);
         //这里和普通的接口调用不同，使用的是sdkExecute
         $response = $aop->sdkExecute($request);
         //htmlspecialchars是为了输出到页面时防止被浏览器将关键参数html转义，实际打印到日志以及http传输不会有这个问题
         ajax_return(0,'获取数据成功',$response);//就是orderString 可以直接给客户端请求，无需再做处理。
        
        
        
        
        
        
     }
     //退款
     function refund(){
         //$order_sn = $this->input->post('order_sn');
         $param = $this->input->post();
         check_sign($param);
         $param = decodeParam($param);
         $order_sn = $param['order_sn'];
         if(empty($order_sn)){
             ajax_return(0,'缺少重要参数');
         }
         $uid = (!empty($param['user_id']))?$param['user_id']:'';
         if($this->db->from('refund')->where(['uid'=>$uid,'order_sn'=>$order_sn])->count_all_results() >= 3){
             ajax_return(2,'您已经提交过三次了');
         }
         $order_sn = $this->db->select('a.goods_name,a.goods_price,a.order_amount,a.goods_id,b.server_time')
                     ->from('order_list a')->join('goods b','a.goods_id=b.goods_id')->where(['order_sn'=>$order_sn])->get();
         $orderInfo = $order_sn->row_array();
         if($orderInfo){
             ajax_return(0,'获取数据成功',$orderInfo);
         }else{
             ajax_return(1,'暂无数据');
         }
     }
     
    //获取退款原因
    function refundCause(){
//         $order_sn = $this->input->post('order_sn');
        $param = $this->input->post();
        check_sign($param);
        $param = decodeParam($param);
        $order_sn = $param['order_sn'];
        $orderInfoRs = $this->db->select('order_amount,goods_name,date_type,add_time,date_time')->from('order_list')->where(['order_sn'=>$order_sn])->get();
        $orderInfo = $orderInfoRs->row_array();
        $serverTime = $orderInfo['date_type']?$orderInfo['date_time']:$orderInfo['add_time'];
        $startedTime = $this->config->item('startedTime');
        $refundProportion = $this->config->item('refundProportion');
        $proportion = $orderInfo['add_time']+$startedTime*60>time()?100:$refundProportion;
        $refundMoney = $orderInfo['order_amount'] * $proportion/100;
        $format_num = sprintf("%.2f",$refundMoney);
        $confRefund = $this->db->from('refund_desc')->get()->result_array();
        foreach($confRefund as $k=>$v){
            if($v['all_payment']){
                $confRefund[$k]['refund_money'] = $format_num;
            }else{
                $confRefund[$k]['refund_money'] = $orderInfo['order_amount'];
            }
        }
        if($confRefund){
            ajax_return(0,'获取数据成功',$confRefund);
        }else{
            ajax_return(1,'暂无数据');
        }
    }
    
    //取消支付
    function ordercCancel(){
//         $order_id = $this->input->post('order_sn');
        $param = $this->input->post();
        check_sign($param);
        $param = decodeParam($param);
        $order_id = $param['order_sn'];
        if(empty($order_id)){
            ajax_return(0,'缺少重要参数');
        }
        $data['order_status'] = 3;
        $this->db->where(['order_sn'=>$order_id])->update('order_list',$data);
        if($this->db->affected_rows()){
            ajax_return(0,'订单取消成功');
        }else{
            ajax_return(-1,'订单取消失败');
        }
    }
    //删除订单
    function orderDel(){
//         $order_id = $this->input->post('order_sn');
        $param = $this->input->post();
        check_sign($param);
        $param = decodeParam($param);
        $order_id = $param['order_sn'];
        if(empty($order_id)){
            ajax_return(0,'缺少重要参数');
        }
        $this->db->where(['order_sn'=>$order_id])->delete('order_list');
        if($this->db->affected_rows()){
            ajax_return(0,'订单删除成功');
        }else{
            ajax_return(-1,'订单删除失败');
        }
    
    }
    //继续支付
    function getOrderInfo(){
//         $order_id = $this->input->post('order_sn');
        $param = $this->input->post();
        check_sign($param);
        $param = decodeParam($param);
        if(empty($param['order_sn'])){
            ajax_return(-1,'缺少重要参数');
        }
        $order_id = $param['order_sn'];
        
        $orderInfoRs = $this->db->select('server_time,goods_name,goods_price,coupon_price,order_amount,xiaofei,add_time')->from('order_list')->where(['order_sn'=>$order_id])->get();
        $orderInfo = $orderInfoRs->row_array();
        $orderInfo['order_amount']=$orderInfo['order_amount']+$orderInfo['xiaofei'];
            //大于30分钟不允许支付
            $timediff = time()-$orderInfo['add_time'];
            
            if($timediff>1800)
            {
                $save['order_status']=3;
                $this->db->update('order_list', $save, array('order_sn' => $order_id));
                //$this->db->where(['order_sn'=>$order_id])->update('order_list',$save);
                ajax_return(-2,'该笔订单已经超过三十分钟,不允许继续支付');
            }
        
            
        
        
        
        if($orderInfoRs->row_array()){
            ajax_return(0,'获取数据成功',$orderInfo);
        }else{
            ajax_return(1,'暂无数据');
        }
    }
    
     //提交退款
    function refundAdd(){
        //         $order_id = $this->input->post('order_sn');
        //         $uid = $this->input->post('user_id');
        $param = $this->input->post();
        check_sign($param);
        $param=decodeParam($param);
        if(empty($param['order_sn']) || empty($param['refund_money']) || empty($param['refund_desc'])){
            ajax_return(0,'缺少重要参数');
        }
        $order_id = $param['order_sn'];
        $uid = $param['user_id'];
        $refund_money = $param['refund_money'];
        $refund_desc =  $param['refund_desc'];
        $data['refund_note'] =  isset($param['refund_note'])?$param['refund_note']:'好评';
        //$data['refund_note'] =  $this->input->post('refund_note');
        $rs = $this->db->select('order_id,goods_name,goods_price,tid,goods_id,server_time,mobile')->from('order_list')->where(['order_sn'=>$order_id])->get();
        $orderInfo = $rs->row_array();
        $data['refund_money'] =  $refund_money;
        $data['goods_name'] =  $orderInfo['goods_name'];
        $data['order_id'] =  $orderInfo['order_id'];
        $data['order_sn'] =  $order_id;
        $data['tid'] =  $orderInfo['tid'];
        $data['uid'] =  $uid;
        $data['goods_id'] =  $orderInfo['goods_id'];
        $data['goods_price'] =  $orderInfo['goods_price'];
        $data['server_time'] =  $orderInfo['server_time'];
        $data['refund_reason'] = $refund_desc;
        $data['add_time'] = time();
        $update['order_status'] = 7;
          $this->db->where(['order_sn'=>$order_id])->update('order_list',$update);
        if($this->db->from('refund')->where(['uid'=>$uid,'order_id'=>$orderInfo['order_id']])->count_all_results() <3){
            $data['status'] =  1;
            if($this->db->insert('refund',$data)){
               $this->load->library('juhesms');
                $content = "【喜脚么】您好您有一笔订单用户已经申请退款，您可以在我的待处理订单中查看。";
                $send_rs = $this->juhesms->send_phone($orderInfo['mobile'], $content);
                ajax_return(0,'提交成功');
            }else{
                ajax_return(-1,'提交失败');
            }
        }else{
            ajax_return(2,'您已经提交过三次了');
        }
    }
    //再来一单
    function payAgain(){
        $param = $this->input->post();
        check_sign($param);
        $param = decodeParam($param);
        if(empty($param['goods_id'])){
            ajax_return(-1,'缺少重要参数');
        }
        $tid = (!empty($param['tid']))?$param['tid']:'';
        $tInfors = $this->db->select('is_online')->from('techer')->where(['tid'=>$tid])->get();
        $tInfo =$tInfors->row_array();
        if($tInfo){
            if($tInfo['is_online']){
                ajax_return(-1,'技师不在线');
            }
        }else{
            ajax_return(-1,'不存在的技师');
        }
        $rs = $this->db->select('goods_id,goods_type,goods_price,goods_name,server_time')->from('goods')->where(array('goods_id'=>$param['goods_id']))->get();
        $goodsInfo = $rs->row_array();
        if($goodsInfo){
            ajax_return(0,'获取数据成功',$goodsInfo);
        }else{
            ajax_return(1,'商品已下架');
        }
    }
    
 function refundStep(){
        $param = $this->input->post();
        check_sign($param);
        $param=decodeParam($param);
        if(empty($param['order_sn'])){
            ajax_return(0,'缺少重要参数');
        }
        $rs = $this->db->select('goods_name,goods_price,status,refund_money,server_time,add_time,from_unixtime(refund_time,"%Y-%m-%d %H:%i:%s") refund_time,refund_reason')->from('refund')->where(array('order_sn'=>$param['order_sn']))->get();
        $refundArr = $rs->row_array();
        if($refundArr){
            $time = $refundArr['add_time']+172800;
            $time = $time-time();
            if($time>0){
                $time = $this->Sec2Time($time);
            }else{
                $time = '0天0小时0分';
            }
             $refundArr['refund_num'] = 0;
            if($this->db->from('refund')->where(['uid'=>$param['user_id'],'order_sn'=>$param['order_sn']])->count_all_results() >= 3){
                $refundArr['refund_num'] = 1;
            }
            $refundArr['stime'] = $time;
            $refundArr['add_time'] = date('Y-m-d H:i', $refundArr['add_time']);
            ajax_return(0,'获取数据成功',$refundArr);
        }else{
            ajax_return(-1,'不存在的订单');
        }
    }
    
    function Sec2Time($time){
        if(is_numeric($time)){
        $value = array(
          "years" => 0, "days" => 0, "hours" => 0,
          "minutes" => 0, "seconds" => 0,
        );
        if($time >= 86400){
          $value["days"] = floor($time/86400);
          $time = ($time%86400);
        }
        if($time >= 3600){
          $value["hours"] = floor($time/3600);
          $time = ($time%3600);
        }
        if($time >= 60){
          $value["minutes"] = floor($time/60);
          $time = ($time%60);
        }
        $value["seconds"] = floor($time);
        $t=$value["days"] ."天"." ". $value["hours"] ."小时". $value["minutes"] ."分";
        Return $t;
         }else{
         return (bool) FALSE;
        }
     }
    
    
}
