<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use JPush\Client;
class Welcome extends CI_Controller {
    public function __construct() {
        parent::__construct();
    }
    
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}
	
	//获取地图范围内的所有技师
	public function getareadata(){
	    $this->load->library('mongodb');
	    $result = GetRange(110.325945,20.031541,2000);
	    //根据地图经纬度信息获取圆周内的所有技师
	    //$where = " (`weidu` between ".$result['minLat']." and ".$result['maxLat'].") and ( `jingdu` between ".$result['maxLon']." and ".$result['minLon']." ) and `uid`=2 ";
	    $result['minLat'] = (string) $result['minLat'];
	    $result['maxLat'] = (string) $result['maxLat'];
	    $result['maxLon'] = (string) $result['maxLon'];
	    $result['minLon'] = (string) $result['minLon'];
	    $res = $this->mongodb->where_between('weidu', $result['minLat'], $result['maxLat'])->where_between('jingdu', $result['maxLon'], $result['minLon'])->get("techer");
	    $r = array();
	    $data = array();
	    foreach ($res as $k=>$v){
	        $r['uid']      = $v['uid'];
	        $r['jingdu']   = $v['jingdu'];
	        $r['weidu']    = $v['weidu'];
	        $data[] = $r;
	    }
	    if(!empty($data)){
	        ajax_return(0, $data);
	    }else{
	        ajax_return(0);
	    }
	}
	
	//redis使用demo
	public function redist(){
	    $this->load->library('redis');
	    
	    $token = settoken();
	    $this->redis->set($token, 1);
	    $this->redis->get($token);
	    
	    $data['name'] = '张三';
	    $data['pwd'] = '111111';
	    
	    $key = 'xijiaomo2017';
	    
	    
	    echo 'sign：'.makeSignature($data, $key);
	    exit;
	}
	
	//mongo使用demo
	public function mongot(){
	    $this->load->library('mongodb');
	    
	    $order_by = ['id' => -1];
	    $where    = ['status' => 1];
	    
	    $MongoId = new MongoId('59bf82de0847b62015000029');
	    $this->mongodb->insert("xijiaomo", array("_id"=>$MongoId, "title"=>"asdqw"));
	    $res = $this->mongodb->select("*")->order_by($order_by)->limit(21)->get('xijiaomo');
	    
	    print_r($res);
	    exit;
	}
	
	//聚合短信发送demo
	public function juhet(){
	    $this->load->library('juhesms');
	    
	    $mobile = '18868108303';
	    $tpl_id = '35216';
	    $code   = rand(100000, 999999);
	    $content = 'zhangsan';
	    $r = $this->juhesms->send($mobile, $tpl_id, $code, $content);
	    if($r !== false){
	        echo '发送成功';
	    }else{
	        echo '发送失败';
	    }
	    exit;
	}
	
	//七牛使用demo
	public function qiniut(){
	    $this->load->library('qiniu');
	    
	    //初始化七牛信息
	    header('Access-Control-Allow-Origin:*');
	    $bucket = 'slys2017';
	    $accessKey = 'ofogHIzoeYZbU_HEhd6THyA3ZJDCgB_nONhLNmZS';
	    $secretKey = 'w-bAU5rWT46C8heMe-wmWaedurY_9r1J0ZEaoOvx';
	    $auth = new Qiniu\Auth($accessKey, $secretKey);
	    $upToken = $auth->uploadToken($bucket, null, 3600);//获取上传所需的token
	    print_r($upToken);
	    exit;
	    $qiniu_url = 'http://up-z2.qiniu.com/putb64/-1';
	    $read_url = 'http://omqtlkwj8.bkt.clouddn.com/';
	    $data['photo'] = 'iVBORw0KGgoAAAANSUhEUgAAAJwAAABxCAYAAAAgYsoRAAABUklEQVR4nO3SMRHDMADAQLeUzJ9Hp/BI7uyUhTLkH4EGfc7zvMcLrbXH3mNc1xrH8RtzzqeTXuH7dADvYjhShiNlOFKGI2U4UoYjZThShiNlOFKGI2U4UoYjZThShiNlOFKGI2U4UoYjZThShiNlOFKGI2U4UoYjZThShiNlOFKGI2U4UoYjZThShiNlOFKGI2U4UoYjZThShiNlOFKGI2U4UoYjZThShiNlOFKGI2U4UoYjZThShiNlOFKGI2U4UoYjZThShiNlOFKGI2U4UoYjZThShiNlOFKGI2U4UoYjZThShiNlOFKGI2U4UoYjZThShiNlOFKGI2U4UoYjZThShiNlOFKGI2U4UoYjZThShiNlOFKGI2U4UoYjZThShiNlOFKGI2U4UoYjZThShiNlOFKGI2U4UoYjZThShiNlOFKGI2U4UoYjZThShiNlOFJ/khoP3ljCuesAAAAASUVORK5CYII=';
	    //使用千牛进行图片上传
	    if($data['photo']){
	        $r  = $this->qiniu->request_by_curl($qiniu_url,$data['photo'],$upToken);
	        $rd = json_decode($r, true);
	        //将当前更新的图片插入到图片表
	        $res = $read_url.$rd['key'];
	        print_r($res);
	        exit;
	    }
	}
	
	//极光推送
	public function jpusht(){
	    $this->load->library('jpush');
	    
	    $app_key = '7b9e561e81b6ac9d450eca3b';
	    $master_secret = '5f5ef96a715879a8729b211c';
	    
	    $client = new Client($app_key, $master_secret);
	    $pusher = $client->push();
	    $pusher->setPlatform('all');
	    $pusher->addAllAudience();
	    $pusher->setNotificationAlert('Hello, JPush');
	    try {
	        $pusher->send();
	    } catch (\JPush\Exceptions\JPushException $e) {
	        print $e;
	    }
	}
	
	//微信支付获取sign
	public function weixinpayt(){
	    $this->load->library('pay');
	    
	    $w = new Pay\Weixin\Weixinpay();
	    $w->wxpay('201709040001001', 100);
	}
	
	//支付宝支付获取sign
	public function alipayt(){
	    $this->load->library('pay');
	    
	    $a = new Pay\alipay\Zfbpay('201709040001001', 100);
	}
}
