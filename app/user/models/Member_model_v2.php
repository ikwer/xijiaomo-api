<?php 
class Member_model_v2 extends CI_Model {
    function getFavoriteDataModel($uid,$now_page = 1){
        --$now_page;
        $page_num = 10;
        $rs =  $this->db->select("a.obj_id sid,a.on_time,c.truename,c.headurl,c.keyong_headurl,c.birthday,c.sex,t.nickname truename")
        ->from('xjm_favorite a')
        //->join('xjm_techer b','a.obj_id=b.tid')
        ->join('xjm_techer_checkinfo c','a.obj_id=c.tid')
        ->join('xjm_techer t','a.obj_id=t.tid')
        ->where(['uid'=>$uid])
         ->order_by('a.on_time','DESC')
        ->limit($page_num,$now_page * $page_num)
        ->get();
        return $rs->result_array();
    }
    
    /*
     * 取消收藏
     */
    function cancelFavoriteModel($id,$uid){
       $this->db->where(array('obj_id'=>$id,'uid'=>$uid))->delete('xjm_favorite');
       return $this->db->affected_rows();
    }
   
    /*
     * 积分明细
     */
    function pointListModel($uid,$now_page = 1){
        --$now_page;
        $page_num = 10;
         $rs = $this->db->select("*,from_unixtime(addtime,'%Y-%m-%d') addtime")
                ->from('xjm_user_point')
                ->where(['uid'=>$uid])
                ->limit($page_num,$now_page * $page_num)
                ->order_by('id desc')
                ->get();
         return $rs->result_array();
    }
    function couponListModel($now_page){
        --$now_page;
        $page_num = 10;
//        $time=1511425952;
        $time=time();
        $where = array('end_time>='=>$time,'s_type'=>0);
        $rs = $this->db->select("id sid,name,type,money,condition,from_unixtime(start_time,'%Y-%m-%d') send_time,from_unixtime(end_time,'%Y-%m-%d') end_time,expoint")
        ->from('xjm_coupon_set')
        ->where($where)
        ->limit($page_num,$now_page*$page_num)
        ->order_by('id desc')
        ->get();
//        echo $this->db->last_query();die;
        return $rs->result_array();
    }
    
    function getCouponModel($id){
            $rs =  $this->db->select('*')
                ->from('xjm_coupon_set')
                ->where(['id'=>$id])
                ->get();
                return $rs->row_array();
    }
    /*
     * 获取用户总积分
     */
    function getUserPoint($uid){
       $rs =  $this->db->select('uid user_id,point')
        ->from('xjm_user')
        ->where(['uid'=>$uid])
        ->get();
        return $rs->row_array();
    }
   
    /*
     * 获取技师信息；
     */
    function getTechInfoModel($tid){
        $rs = $this->db->select('a.tid,a.nickname,a.mobile,b.headurl,b.keyong_headurl')
            ->from('xjm_techer a')
            ->join('xjm_techer_checkinfo b','a.tid = b.tid')
            ->where(['a.tid'=>$tid])
            ->get();
            return $rs->row_array();
        
    }
    /*
     * 获取用户所有的优惠券
     */
    function getCouponByUid($uid,$now_page = 1){
        --$now_page;
        $page_num = 10;
        $rs = $this->db->select("*,from_unixtime(end_time,'%Y-%m-%d') end_time,from_unixtime(send_time,'%Y-%m-%d') send_time")
            ->from('xjm_user_coupon')
            ->where(['uid'=>$uid])
            ->limit($page_num,$now_page * $page_num)
            ->get();
        return $rs->result_array();
    }
  
    /*
     * 上传头像/修改头像
     */
    function saveHeadUrlModel($uid,$data){
       return  $this->db->where(['uid'=>$uid])->update('xjm_user',$data);
        //return $this->db->affected_rows();
    }
    
    /*
     * 获取用户信息；
     */
    function getUserInfoModel($uid){
        $rs = $this->db->select('nickname,headurl')
        ->from('xjm_user')
        ->where(['uid'=>$uid])
        ->get();
        return $rs->row_array();
    
    }
    /*
     * 公共添加数据函数
     */
    function commonAddData($table,$data){
        return  $this->db->insert($table,$data);
    }
    /*
     * 获取tagname
     */
    function getTagnameById($id){
        $rs = $this->db->select("id sid,tag_name")
            ->from('xjm_comment_tag_set')
            ->where(['id'=>$id])
            ->get();
            return $rs->row_array();
    }
    /*
     * 选择项目后获取附近的技师
     */
   /* function getLocation($longitude,$latitude,$distance,$now_page,$goods_type){
        --$now_page;
        $pageNum = 9;
        $offset = $now_page * $pageNum;
        $sql = "select a.tid,a.t_lat,a.t_lon,a.nickname,b.headurl from  xjm_techer a left join xjm_techer_checkinfo b 
            on a.tid=b.tid where sqrt( ( ((".$longitude."-t_lon)*PI()*12656*cos(((".$latitude."+t_lat)/2)*PI()/180)/180)
                * ((".$longitude."-t_lon)*PI()*12656*cos (((".$latitude."+t_lat)/2)*PI()/180)/180) ) + ( ((".$latitude."-t_lat)*PI()*
                    12656/180) * ((".$latitude."-t_lat)*PI()*12656/180) ) )/2 < ".$distance." and a.is_online=0 and a.status=1 and INSTR(a.skill,".$goods_type.")  limit ".$offset.",".$pageNum;
        $result=$this->db->query($sql)->result_array();
        foreach ($result as $k=>$v){
            (empty($v['headurl'])) ? $result[$k]['headurl']='http://oxll5q098.bkt.clouddn.com/wo.png' : $result[$k]['headurl']=$v['headurl'];
        }

        return $result;
    }*/
    /*
     * 选择项目后获取附近的技师
     */
  
    function getLocation($longitude,$latitude,$distance,$now_page,$goods_type){
        --$now_page;
        $pageNum = 9;
        $offset = $now_page * $pageNum;
        $sql = "select a.tid,a.t_lat,a.t_lon,a.nickname,b.headurl,b.headurl_status,b.sex,b.keyong_headurl from  xjm_techer a left join xjm_techer_checkinfo b 
            on a.tid=b.tid where  a.is_online=0 and a.status=1 and INSTR(a.skill,".$goods_type.") ORDER BY a.tuidan_nums  limit ".$offset.",".$pageNum;
        $result=$this->db->query($sql)->result_array();
        foreach ($result as $k=>$v){
            if($v['sex']==2){
                $h=date('G');
                if($h<8 || $h>20){
                    unset($result[$k]);
                }
            }
            (empty($v['keyong_headurl'])) ? $result[$k]['headurl']='http://pic.xijiaomo.com/wo1.png' : $result[$k]['headurl']=$v['keyong_headurl'];
        
        }

        return $result;
    }
    
}
