<?php

/**
 * Description of m_user
 *
 * @author acer
 */
class Order_model_v2 extends CI_Model {

    //true成功  false 失败
    private $results = array(
        'status' => false,
        'msg' => '',
        'data' => array()
    );

    /**
     * 获取用户历史地址
     * @param type $uid
     * @return type
     */
    function select_address($uid) {
        $address = $this->db->select('address_id,address,house_number')->from('user_address')
                ->where('uid', $uid)
                ->order_by('add_time DESC')
                ->limit(10)
                ->get();
        $user_ddress = $address->result_array();
        $this->results['status'] = true;
        $this->results['data'] = $user_ddress;
        return $this->results;
    }

   //可使用优惠卷数量
    function coupon_num($uid, $order_price, $area_id) {
        $time = time();
        $all_numRs = $this->db->select('type,condition')->from('user_coupon')
                ->where('uid', $uid)
                ->where('end_time >', $time)
                ->where('status', 0)
                //->where('condition <=', $order_price)
                ->where_in('area_id', array('0', $area_id))
                ->get();
        $user_ddress = $all_numRs->result_array();
        if(!empty($user_ddress)){
            foreach($user_ddress as $k=>$v){
                if($v['type'] == 2){
                    if($v['condition'] > $order_price){
                        unset($user_ddress[$k]);
                    }
                }
            }
            $all_num = count($user_ddress);
            
        }
        $all_numArr['all_num'] = (!empty($all_num))?$all_num:0;
        $this->results['status'] = true;
        //以后删除
        //$user_ddress['all_num'] = '1';
        $this->results['data'] = $all_numArr;
        return $this->results;
    }

    /**
     * 锁定优惠卷
     * @param type $coupon_id
     */
    function lock_coupon($coupon_id) {
        $up['status'] = 1;
        $this->db->where('id', $coupon_id);
        $this->db->update('user_coupon', $up);
        return $this->db->affected_rows();
    }

    /**
     * 获取优惠卷信息
     * @param type $coupon_id
     */
    function check_coupon($user_id, $coupon_id, $price) {
        $all_num = $this->db->select('id,uid,area_id,type,money,condition,end_time,status')->from('user_coupon')
                ->where('id', $coupon_id)
                ->where('uid', $user_id)
                ->where('condition <=', $price)
                ->get();
        $rs = $all_num->row_array();
        if ($rs) {
            $this->results['status'] = true;
            $this->results['data'] = $rs;
        }
        return $this->results;
    }

    //优惠卷列表
    function coupon_list($uid, $now_page = 1) {
        $time = time();
        --$now_page;
        $page_num = 20;
        $address = $this->db->select('user_coupon.id,type,name,area_id,area_name,money,condition,send_time,end_time')->from('user_coupon')
                ->join('area_list', 'area_list.id = user_coupon.area_id', 'left')
                ->where('uid', $uid)
                ->where('end_time >', $time)
                ->where('status', 0)
                ->limit($page_num, $now_page * $page_num)
                ->order_by('money desc')
                ->get();
        $user_ddress = $address->result_array();
        $this->results['status'] = true;
        $this->results['data'] = $user_ddress;
        return $this->results;
    }

    //添加下单地址
    function add_address($uid, $address, $house_number) {
        $add['uid'] = $uid;
        $add['address'] = $address;
        $add['house_number'] = $house_number;
        $add['add_time'] = time();
        $this->db->insert('user_address', $add);
        $inset_id = $this->db->insert_id();
        if ($inset_id) {
            $this->results['status'] = true;
            $this->results['data'] = $inset_id;
        }
        return $this->results;
    }
    //
    function check_address($uid,$address,$house_number){
         $where['address'] = $address;
        $where['house_number'] = $house_number;
        $where['uid'] = $uid;
        $rs = $this->db->select('*')->from('user_address')->where($where)->get();
        $addressArr = $rs->row_array();
        if($addressArr){
            return true;
        }else{
            return false;
        }
    }
    
    //可下单地区id
    function open_address($area_name) {
        $check = $this->db->select('id,area_name')->from('area_list')
                ->where('area_name', $area_name)
                ->where('open', 1)
                ->get();
        $order_states = $check->row_array();
        if(!$order_states){
            $data['status'] = false;
        }else{
           $data['status'] = true;
           $data['data'] = $order_states;
        }
        return $data;
    }

    //获取产品信息
    function product_info($id) {
        $check = $this->db->select('goods_id,goods_name,goods_price,server_time,goods_image')->from('goods')
                ->where('goods_id', $id)
                ->where('is_show', 0)
                ->get();
        $order_states = $check->row_array();
        if ($order_states) {
            $this->results['status'] = true;
            $this->results['data'] = $order_states;
            return $this->results;
        }
        return $this->results;
    }

    //检测是否有已存在的订单
    function check_orders($uid, $goods_type) {
        //检测有没有5分钟以内的同型号订单  存在直接获取已下单的技师返回   不存在数据库下单添加mongo 添加redis队列 5分钟失效
        $time = time() - 3600;
        $check = $this->db->select('*')->from('order_list')
                ->where('uid', $uid)
                ->where('option_type', $goods_type)
                ->where('add_time >', $time)
                ->where('order_status', 0)
                ->get();
        $order_states = $check->row_array();
        if ($order_states) {
            $this->results['status'] = true;
            return $this->results;
        }
        return $this->results;
    }

    //添加订单
    function add_orders($uid, $goods_type, $u_lon, $u_lat) {
        $add['uid'] = $uid;
        $add['option_type'] = $goods_type;
        $add['order_sn'] = date('YmdHis', time()) . $uid;
        $add['add_time'] = time();
        $add['u_lon'] = $u_lon;
        $add['u_lat'] = $u_lat;
        $this->db->insert('order_list', $add);
        $order_id = $this->db->insert_id();
        if ($order_id) {
            $this->results['status'] = true;
            $this->results['data'] = $order_id;
            return $this->results;
        }
        return $this->results;
    }

    /*
     * 获取技师的经纬度
     */
    
    public function getLocationByTid($tid){
        $rs = $this->db->select('t_lat,t_lon')->from('techer')->where(['tid'=>$tid])->get();
        $techLocation = $rs->row_array();
        return $techLocation;
//         var_dump($techLocation);
    }
    
}
