<?php

/**
 * Description of m_user
 *
 * @author acer
 */
class Tech_model_v2 extends CI_Model {

    //true成功  false 失败
    private $results = array(
        'status' => false,
        'msg' => '',
        'data' => array()
    );

    //技师信息
    public function tech_msg($user_id, $tid) {
        $user = $this->db->select('techer.tid,mobile,truename,nickname,headurl,desc,skill,headurl_status,keyong_headurl')->from('techer_checkinfo')
                ->join('techer', 'techer.tid = techer_checkinfo.tid', 'left')
                ->where('xjm_techer.tid', $tid)
                ->get();
        $user_info = $user->row_array();
        
         (empty($user_info['skill'])) ? $skill=0 : $skill=$user_info['skill'];
        $re=$this->db->query('SELECT `name` FROM xjm_goods_option WHERE cate_id IN('.$skill.')')->result_array();
        $skill=array();
        if(!empty($re))
        {
            foreach ($re as $v)
            {
                $skill[]=@implode(',',$v);

            }
            $skill=@implode(',',$skill);
        }
        else
        {
            $skill="";
        }
        unset($user_info['skill']);
        $user_info['desc']=$skill;
        
        if (!$user_info) {
            return $this->results;
        }
        //照片视频
        $photo = $this->db->select('type,pic_url,video_url,pic_status,video_status')->from('techer_photo')
                ->where('tid', $tid)
                ->get();
        $photo_info = $photo->result_array();
        $hear_data = array();
        $video = '';
        foreach ($photo_info as $key => $value) {
            if ($value['type'] == 1) {
                //$hear_data[] = $value['pic_url'];
                if($value['pic_status']!=2){
                    $hear_data[] = array(
                        'thumb_pic'=>$value['pic_url'].'?imageMogr2/thumbnail/!640x300r/gravity/Center/crop/300x300/format/jpg/blur/1x0/quality/75|imageslim',
                        'pic_url'=>$value['pic_url']
                    );
                }
            }
            if ($value['type'] == 2) {
                // $video = $value['video_url'];

                if($value['video_status']!=2) {
                    $video[] = array('video_pic' => $value['video_url'] . '?vframe/png/offset/2/w/750/h/600', 'video_url' => $value['video_url']);
                }
                
                
            }
        }
        //标注
        $comment_tag = $this->db->select('tag_name,count(tag_id)as all_num')->from('user_comment_tag')
                ->where('tid', $tid)
                ->join('comment_tag_set', 'user_comment_tag.tag_id = comment_tag_set.id', 'left')
                ->group_by('tag_id')
                ->get();
        $comment_tag = $comment_tag->result_array();
//         if (empty($comment_tag)) {
//             $comment_tag = NULL;
//         }
        //是否收藏
        $is_collect = $this->db->select('id')->from('favorite')
                ->where('uid', $user_id)
                ->where('obj_id', $tid)
                ->get();
        $is_collect = $is_collect->row_array();
        if ($is_collect) {
            $user_msg['is_collect'] = '0';
        } else {
            $user_msg['is_collect'] = '1';
        }
        $user_msg['tid'] = $user_info['tid'];
        $user_msg['headurl'] = (empty($user_info['keyong_headurl'])) ? (empty($user_info['headurl']) ? 'http://pic.xijiaomo.com/wo.png' :  $user_info['headurl']) : $user_info['keyong_headurl'];
        $user_msg['truename'] = $user_info['nickname'] ? $user_info['nickname'] : $user_info['truename'];
        $user_msg['mobile'] = substr_replace($user_info['mobile'], '****', 3, 4);
        $user_msg['desc'] = $user_info['desc'];
        //$user_msg['photos_url'] = $hear_data;
        (!empty($hear_data)) ? $user_msg['photos_url'] = $hear_data : $user_msg['photos_url'] = array();
        (!empty($video)) ? $user_msg['video_url'] = $video : $user_msg['video_url'] = array();
        $user_msg['comment_tag'] = $comment_tag;
        $this->results['status'] = TRUE;
        $this->results['data'] = $user_msg;
        return $this->results;
    }
/*
 * 获取技师评论信息
 */
    public function getTechComment($tid,$now_page){
        --$now_page;
        $page_num = 10;
        $appraise = $this->db->select('c.nick_name,c.goods_name,c.headurl,c.mobile,c.type,c.anonymity,c.content,c.comment_img,FROM_UNIXTIME(c.add_time,"%Y-%m-%d") add_time')
            ->from('user_comment c')
            ->join('user u','c.uid=u.uid','left')
            ->where('c.tid', $tid)
            ->limit($page_num,$now_page*$page_num)
            ->order_by('c.add_time desc')
            ->get();
        
        $appraiseList = $appraise->result_array();

        for ($i = 0; $i < count($appraiseList); $i ++) {
            if ($appraiseList[$i]['comment_img']) {
                $appraiseList[$i]['comment_img'] = explode(";", $appraiseList[$i]['comment_img']);
            } else {
                $appraiseList[$i]['comment_img'] = [];
            }
            
        }
        
        $good_praise = 0;
        $all_appraise = 0;
        $rs = $this->db->select('id,type')
        ->from('user_comment')
        ->where('tid', $tid)
        ->get();
        $appraise_rs = $rs->result_array();
        if ($appraise_rs) {
            $all_appraise = count($appraise_rs);
            $good_appraise = 0;
            foreach ($appraise_rs as $key => $value) {
                if ($value['type'] == 0) {
                    $good_appraise ++;
                }
//                 $appraise_rs[$key]['add_time'] = date('Y-m-d', $value['add_time']);
            }
            $good_praise = ceil(($good_appraise / $all_appraise) * 100);
        }
        $data['all_appraise'] = $all_appraise;
        $data['good_praise'] = $good_praise;
        $data['appraise_list'] = $appraiseList;
        return $data;
        
    }
    
    
    
    /**
     * 技师信息
     * @param type $tid
     * @return type
     */
    public function tech_states($tid) {
        $user = $this->db->select('t_lat,t_lon,is_online')->from('techer_checkinfo')
                ->join('techer', 'techer.tid = techer_checkinfo.tid', 'left')
                ->where('xjm_techer.tid', $tid)
                ->get();
        $user_info = $user->row_array();
        if ($user_info) {
            $this->results['status'] = TRUE;
            $this->results['data'] = $user_info;
        }
        return $this->results;
    }

}
