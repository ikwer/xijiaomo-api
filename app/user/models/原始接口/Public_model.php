<?php

/**
 * Description of m_user
 *
 * @author acer
 */
class Public_model extends CI_Model {

    //true成功  false 失败
    private $results = array(
        'status' => false,
        'msg' => '',
        'data' => array()
    );

    //特殊要求/禁忌
    function special_message($goods_type) {
        $notice = $this->db->select('id,name')->from('message_labelling')->where('goods_id', $goods_type)->get();
        $all_notice = $notice->result_array();
        print_r($all_notice);die;
        //禁忌
        $taboo = $this->db->select('goods_id,goods_name,goods_price,server_time,taboo')->from('goods')->where('goods_id', $goods_type)->get();
        $taboo_rs = $taboo->row_array();
        echo $taboo->last_query();die;
        $data['all_notice'] = $all_notice ? $all_notice : NULL;
        $data['taboo_rs'] = $taboo_rs['taboo'] ? $taboo_rs['taboo'] : NULL;
        $data['goods_name'] = $taboo_rs['goods_name'];
        $data['goods_price'] = $taboo_rs['goods_price'];
        $data['server_time'] = $taboo_rs['server_time'];
        $data['goods_id'] = $taboo_rs['goods_id'];
        $this->results['status'] = TRUE;
        $this->results['data'] = $data;
        return $this->results;
    }
    //
    function getTaboo($goods_id){
        $rs = $this->db->select('id,taboo')->from('taboo')->where('goods_id', $goods_id)->get();
        $notice = $rs->result_array();
        return $notice;
    }
    

    //公告分页
    function unread($uid, $now_page) {
        --$now_page;
        $page_num = 12;
        $notice = $this->db->select('id as m_id,title,is_read,content')->from('notice')
                ->where('uid', $uid)
                ->limit($page_num, $now_page * $page_num)
                ->order_by('is_read, add_time DESC')
                ->get();
//        print_r($this->db->last_query());die;
        $all_notice = $notice->result_array();
        $this->results['status'] = TRUE;
        $this->results['data'] = $all_notice;
        return $this->results;
    }

    //读信息
    function change_msg($uid, $msg_id) {
        $where['id'] = $msg_id;
        $where['uid'] = $uid;
        $save['is_read'] = 1;
        $save['read_time'] = time();
        $this->db->where($where);
        $this->db->update('notice', $save);
        $rs = $this->db->affected_rows();
        if ($rs) {
            $this->results['status'] = TRUE;
        } else {
            $this->results['status'] = FALSE;
        }
        return $this->results;
    }

    //插入公共信息
    function notice($uid) {
        //获取1个月内所有以插入公告信息
        $time = time() - 2592000;
        $notice = $this->db->select('id,common_id')->from('notice')
                ->where('uid', $uid)
                ->where('add_time >', $time)
                ->where('type', 1)
                ->get();
        $all_notice = $notice->result_array();
        $where = '';
        $all_id = '';
        if (count($all_notice) > 0) {
            foreach ($all_notice as $key => $value) {
                $all_id .= "'" . $value['common_id'] . "'" . ',';
            }
            $all_id = trim($all_id, ',');
            $where = "AND `id` NOT IN($all_id)";
        }
        //将1个月内所有未插入的公告通告找出
        $sql = "SELECT `id`, `title`, `content`, `add_time` FROM `xjm_notice_pubilc` WHERE `add_time` > $time $where";
        $check_send = $this->db->query($sql);
        $uninset_notice = $check_send->result_array();
        $inset_rs = TRUE;
        if (count($uninset_notice) > 0) {
            //开始插入未插入公告
            $inset_data = array();
            foreach ($uninset_notice as $key => $value) {
                $inset_data[$key]['uid'] = $uid;
                $inset_data[$key]['type'] = 1;
                $inset_data[$key]['common_id'] = $value['id'];
                $inset_data[$key]['title'] = $value['title'];
                $inset_data[$key]['content'] = $value['content'];
                $inset_data[$key]['add_time'] = $value['add_time'];
            }
            $inset_rs = $this->db->insert_batch('notice', $inset_data);
            if (!$inset_rs) {
                //保存错误日志
                $error_sql = $this->db->last_query();
                $time = date('Y-m-d H:i;s');
                log_message('error', '更新公用公告到用户公告表失败时间：' . $time . '用户id为：' . $uid . '执行sql为：' . $error_sql);
                $this->results['status'] = FALSE;
                return $this->results;
            }
        }
        $all_num = $this->db->where("uid = $uid AND is_read = 0")->count_all_results('notice');
        $this->results['status'] = TRUE;
        $this->results['data'] = $all_num;
        return $this->results;
    }

    //首页banne图
    function banner() {
        $banner = $this->db->select('id,img_url')->from('banner')->order_by('sort')->get();
        if ($banner->result_array()) {
            $this->results['status'] = TRUE;
            $this->results['data'] = $banner->result_array();
        } else {
            $this->results['status'] = FALSE;
        }
        return $this->results;
    }

    //分类信息
    function option_info($type) {
        $banner = $this->db->select('cate_id,publicity_img,profile')->from('goods_option')->where('cate_id', $type)->get();
        $option_rs = $banner->row_array();
        $list_sql = $this->db->select('goods_id,goods_name,goods_image,goods_price,server_time')->from('goods')->where('goods_type', $type)->where('is_show', 0)->order_by('sort')->get();
        $list = $list_sql->result_array();
        $data['option'] = $option_rs;
        $data['list'] = $list;
        $this->results['status'] = TRUE;
        $this->results['data'] = $data;
        return $this->results;
    }

    //短信发送存档
    function inset_sms_code($mobile, $code) {
        //120秒发一次
        $check_sql = "SELECT * FROM xjm_sms_code WHERE mobile = '$mobile' ORDER BY sendtime DESC LIMIT 1";
        $check_rs = $this->db->query($check_sql)->row_array();
        $cool_time = time() - $check_rs['sendtime'];
        if ($cool_time < 10) {
            $this->results['msg'] = '短信发送过快,请稍后再试';
            return $this->results;
        }
        $inset['mobile'] = $mobile;
        $inset['code'] = $code;
        $inset['sendtime'] = time();
        $rs = $this->db->insert('sms_code', $inset);
        if ($rs) {
            $this->results['status'] = true;
            $this->results['data'] = array('send_id' => $this->db->insert_id());
        } else {
            $this->results['msg'] = '短信发送失败';
        }
        return $this->results;
    }

    /**
     * 返回短信检验结果
     * @param type $mobile 
     * @param type $id 短信验证id
     * @param type $code  验证码
     */
    function check_phone_code($mobile, $code_id, $code) {
        $where['mobile'] = $mobile;
        $where['id'] = $code_id;
        $where['code'] = $code;
        //2分钟以前的无效
        $send_time = time() - 5000000;
        $check_send = $this->db->select('id')->from('sms_code')->where($where)->where('sendtime >', $send_time)->get();
        if ($check_send->row_array()) {
            $this->results['status'] = TRUE;
        } else {
            $this->results['status'] = FALSE;
        }
        return $this->results;
    }

}
