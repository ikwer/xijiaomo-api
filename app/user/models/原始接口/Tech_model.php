<?php

/**
 * Description of m_user
 *
 * @author acer
 */
class Tech_model extends CI_Model {

    //true成功  false 失败
    private $results = array(
        'status' => false,
        'msg' => '',
        'data' => array()
    );

    //技师信息
    public function tech_msg($user_id, $tid) {
        $user = $this->db->select('techer.tid,mobile,truename,nickname,headurl,desc')->from('techer_checkinfo')
                ->join('techer', 'techer.tid = techer_checkinfo.tid', 'left')
                ->where('xjm_techer.tid', $tid)
                ->get();
        $user_info = $user->row_array();
        if (!$user_info) {
            return $this->results;
        }
        //照片视频
        $photo = $this->db->select('type,pic_url,video_url')->from('techer_photo')
                ->where('tid', $tid)
                ->get();
        $photo_info = $photo->result_array();
        $hear_data = array();
        $video = '';
        foreach ($photo_info as $key => $value) {
            if ($value['type'] == 1) {
                $hear_data[] = $value['pic_url'];
            }
            if ($value['type'] == 2) {
                $video = $value['video_url'];
            }
        }
        //标注
        $comment_tag = $this->db->select('tag_name,count(tag_id)as all_num')->from('user_comment_tag')
                ->where('tid', $tid)
                ->join('comment_tag_set', 'user_comment_tag.tag_id = comment_tag_set.id', 'left')
                ->group_by('tag_id')
                ->get();
        $comment_tag = $comment_tag->result_array();
//         if (empty($comment_tag)) {
//             $comment_tag = NULL;
//         }
        //是否收藏
        $is_collect = $this->db->select('id')->from('favorite')
                ->where('uid', $user_id)
                ->where('obj_id', $tid)
                ->get();
        $is_collect = $is_collect->row_array();
        if ($is_collect) {
            $user_msg['is_collect'] = '0';
        } else {
            $user_msg['is_collect'] = '1';
        }
        $user_msg['tid'] = $user_info['tid'];
        $user_msg['headurl'] = $user_info['headurl'];
        $user_msg['truename'] = $user_info['nickname'] ? $user_info['nickname'] : $user_info['truename'];
        $user_msg['mobile'] = substr_replace($user_info['mobile'], '****', 3, 4);
        $user_msg['desc'] = $user_info['desc'];
        $user_msg['photos_url'] = $hear_data;
        $user_msg['video_url'] = $video;
        $user_msg['comment_tag'] = $comment_tag;
        $this->results['status'] = TRUE;
        $this->results['data'] = $user_msg;
        return $this->results;
    }
/*
 * 获取技师评论信息
 */
    public function getTechComment($tid,$now_page){
        --$now_page;
        $page_num = 5;
        $appraise = $this->db->select('nick_name,headurl,mobile,type,anonymity,content,add_time')
        ->from('user_comment')
        ->where('tid', $tid)
        ->limit($page_num,$now_page*$page_num)
        ->order_by('add_time desc')
        ->get();
        $appraiseList = $appraise->result_array();
        
        $good_praise = 0;
        $all_appraise = 0;
        $rs = $this->db->select('id,type')
        ->from('user_comment')
        ->where('tid', $tid)
        ->get();
        $appraise_rs = $rs->result_array();
        if ($appraise_rs) {
            $all_appraise = count($appraise_rs);
            $good_appraise = 0;
            foreach ($appraise_rs as $key => $value) {
                if ($value['type'] == 0) {
                    $good_appraise ++;
                }
//                 $appraise_rs[$key]['add_time'] = date('Y-m-d', $value['add_time']);
            }
            $good_praise = ceil(($good_appraise / $all_appraise) * 100);
        }
        $data['all_appraise'] = $all_appraise;
        $data['good_praise'] = $good_praise;
        $data['appraise_list'] = $appraiseList;
        return $data;
        
    }
    
    
    
    /**
     * 技师信息
     * @param type $tid
     * @return type
     */
    public function tech_states($tid) {
        $user = $this->db->select('t_lat,t_lon,is_online')->from('techer_checkinfo')
                ->join('techer', 'techer.tid = techer_checkinfo.tid', 'left')
                ->where('xjm_techer.tid', $tid)
                ->get();
        $user_info = $user->row_array();
        if ($user_info) {
            $this->results['status'] = TRUE;
            $this->results['data'] = $user_info;
        }
        return $this->results;
    }

}
