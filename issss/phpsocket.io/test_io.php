<?php
require_once __DIR__ . '/vendor/autoload.php';
use Workerman\Worker;
use PHPSocketIO\SocketIO;
require_once __DIR__ . '/Lib/Timer.php';
use Workerman\Lib\Timer;

// 心跳间隔25秒
define('HEARTBEAT_TIME', 25);


//密钥
define('API_KEYS','78ce7941a4edb427');


$context = array(
    'ssl' => array(
        'local_cert'  => '/etc/nginx/cert/214340111410143.pem',
        'local_pk'    => '/etc/nginx/cert/214340111410143.key',
        'verify_peer' => false,
    )
);

// 创建socket.io服务端，9501
$io = new SocketIO(9501,$context);

// 监听一个http端口，通过http协议访问这个端口可以向所有客户端推送数据(url类似http://ip:9191?msg=xxxx)
$io->on('workerStart', function()use($io) {
    $inner_http_worker = new Worker('http://172.16.97.18:9595');
    $inner_http_worker->onMessage = function($http_connection, $data)use($io){
        if(!isset($_REQUEST['msg'])) {
            return $http_connection->send('fail');
        }
        $ret=$io->emit('push_msg'.$_REQUEST['uid'], $_REQUEST['msg']);
        if($ret){
            $http_connection->send('ok');
        }else{
            $http_connection->send('fail');
        }
        
    };
    $inner_http_worker->listen();
});

// 当有客户端连接时
$io->on('connection', function($socket)use($io){
    
     $socket->on('ios_test', function($msg)use($socket){
         $socket->emit('ios_test', $msg.'_ios_hupin');
     });
    
    
     //自定义locations函数
     $socket->on('locations', function($msg)use($socket){
        //echo $msg;
        parse_str($msg);
        $arrs=array('tid'=>$tid,'lon'=>$lon,'lat'=>$lat);
        $urls="http://api.xijiaomo.com/tech.php/v1/Techer/ws_location";
        $steam=curl_info($urls,$arrs);
        echo $steam;
    
     });


  //自定义login函数
  $socket->on('login', function($msg)use($socket){ 
    $param=oo_param($msg);
    $check=check_sign($param);
    //var_dump($check);
    if($check==false){
        $socket->disconnect();
    }
    $returns=ws_online($param['tid']);
    $json=json_decode($returns,true);
    if($json['code']==0){
        $GLOBALS['time']=time();
        echo $returns.$param['tid'];
    }
    
  });
  //自定义心跳
  $socket->on('pingpong', function($msg)use($socket){
    
    $param=oo_param($msg);
    $check=check_sign($param);
    if(!$check){
        $socket->disconnect();
    }
    
    $returns=ws_online($param['tid']);
    $json=json_decode($returns,true);
    if($json['code']==0){
        $GLOBALS['time']=time();
        $socket->emit('pingpong', "{'type':'pong'}");
        
    }
    
    
  });
  
  
  
  /*//定时器25秒自动下线
    Timer::add(1, function()use($socket){
        $time_now=time();
      if(!empty($GLOBALS['time']) && $time_now-$GLOBALS['time']>HEARTBEAT_TIME)
       {
          $socket->disconnect();
       }
       
       
    });*/
   
  //手动下线 
  $socket->on('close', function($msg)use($socket){
    $param=oo_param($msg);
    echo '下线：'.$msg;
    $check=check_sign($param);
    if(!$check){
        $socket->disconnect();
    }
    
    $returns=ws_offline($param['tid']);
    $json=json_decode($returns,true);
    if($json['code']==0){
        //$socket->disconnect();
        echo $returns;
    }
    
  });
  
});


//发送上线请求
function ws_online($uid)
{
    $url="http://api.xijiaomo.com/tech.php/v1/Techer/ws_online";
    
    $post=array('0-tid'=>$uid,'signature'=>API_KEYS);
    ksort($post);

    $a= http_build_query($post);
    $signature=sha1(md5(urldecode($a)));

    $array=array('0-tid'=>$uid,'signature'=>$signature);
    return curl_info($url,$array);
}



//发送下线请求
function ws_offline($uid)
{
    $url="http://api.xijiaomo.com/tech.php/v1/Techer/ws_offline";
    
    $post=array('0-tid'=>$uid,'signature'=>API_KEYS);
    ksort($post);

    $a= http_build_query($post);
    $signature=sha1(md5(urldecode($a)));

    $array=array('0-tid'=>$uid,'signature'=>$signature);
    return curl_info($url,$array);
}

//curl方法
function curl_info($url,$post)
{
    ob_start();
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    curl_exec($ch);
    curl_close($ch);
    $stream = ob_get_contents();
    ob_end_clean();

    return $stream;
}


//构造参数
function oo_param($str)
{
    parse_str($str);
    if(empty($tid) || empty($signature)){
        return false;
    }else{
        return array('tid'=>$tid,'signature'=>$signature);
    }
    
}



//参数验证
function check_sign($data)
{
        if(empty($data['signature']) || empty($data))
        {
            return false;
        }
        else
        {
            $signature=$data['signature'];
            unset($data['signature']);
            
            $server_sign=sha1(md5("signature=".API_KEYS.'&tid='.$data['tid']));
            // file_put_contents('/usr/share/nginx/html/api/issss/phpsocket.io/log.php','\n'.$server_sign.'\n',FILE_APPEND);  
            // file_put_contents('/usr/share/nginx/html/api/issss/phpsocket.io/log.php','\n'.$signature.'\n',FILE_APPEND);
            //var_dump($server_sign);
            if($server_sign!=$signature)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
}



Worker::runAll();