<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include_once 'src/JWT.php';

use \Firebase\JWT\JWT;

class CI_Authtoken {

    private $auth_key = '大吉大利,今晚吃鸡';

    //加密token
    function auth_encode($uid) {
        $data['status'] = false;
        $data['msg'] = '';
        $data['data'] = '';
        if (empty(intval($uid))) {
            $data['msg'] = '缺少key';
            return $data;
        }
        $key = $this->auth_key;
        $token = array(
            "uid" => $uid, //请求方 用户id
            "iat" => time(), //token创建时间，unix时间戳格式
            "nbf" => time(), //如果当前时间在nbf里的时间之前，则Token不被接受；一般都会留一些余地，比如几分钟。
            "exp" => time() + 7776000  //令牌有效期 3个月
        );
        $data['status'] = TRUE;
        $data['data'] = JWT::encode($token, $key);
        return $data;
    }

    //令牌验证
    function autn_decode($check_id, $token) {
        $data['status'] = false;
        $data['msg'] = '';
        $data['data'] = '';
        if (empty($check_id) || empty($token)) {
            $data['msg'] = '缺少解密参数';
            return $data;
        }
        try {
            $decode = JWT::decode($token, $this->auth_key, array('HS256'));
            if ($decode->uid != $check_id) {
                $data['msg'] = '令牌验证失败';
                return $data;
            }
            $data['status'] = TRUE;
            $data['msg'] = '令牌验证成功';
            $data['data'] = $decode;
            return $data;
        } catch (Exception $exc) {
            $data['msg'] = '令牌验证失败';
//            $data['msg'] = $exc->getMessage();
            return $data;
        }
    }

}
