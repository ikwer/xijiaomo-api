<?php
namespace Pay\alipay;

class Zfbpay{
    
    var $alipay_config = array(
        'appid' =>'2017010604888586',//商户密钥
        'rsaPrivateKey' =>'',//私钥
        'alipayrsaPublicKey'=>'',//公钥(自己的程序里面用不到)
        'partner'=>'2088421540577515',//(商家的参数,新版本的好像用不到)
        'input_charset'=>strtolower('utf-8'),//编码
        'notify_url' =>'www.test.com/api/notify.php',//回调地址(支付宝支付成功后回调修改订单状态的地址)
        'payment_type' =>1,//(固定值)
        'seller_id' =>'',//收款商家账号
        'charset'    => 'utf-8',//编码
        'sign_type' => 'RSA2',//签名方式
        'timestamp' =>date("Y-m-d Hi:i:s"),
        'version'   =>"1.0",//固定值
        'url'       => 'https://openapi.alipay.com/gateway.do',//固定值
        'method'    => 'alipay.trade.app.pay',//固定值
    );
    
    public function __construct($osn,$total_fee){
        //构造业务请求参数的集合(订单信息)
        $content = array();
        $content['body'] = 'ceshi';
        $content['subject'] = 'funbutton';//商品的标题/交易标题/订单标题/订单关键字等
        $content['out_trade_no'] = $osn;//商户网站唯一订单号
        $content['timeout_express'] = '1d';//该笔订单允许的最晚付款时间
        $content['total_amount'] = floatval($total_fee);//订单总金额(必须定义成浮点型)
        $content['seller_id'] = '';//收款人账号
        $content['product_code'] = 'QUICK_MSECURITY_PAY';//销售产品码，商家和支付宝签约的产品码，为固定值QUICK_MSECURITY_PAY
        $content['store_id'] = 'BJ_001';//商户门店编号
        $con = json_encode($content);//$content是biz_content的值,将之转化成字符串
        
        //公共参数
        $param = array();
        $Client = new \AopClient();//实例化支付宝sdk里面的AopClient类,下单时需要的操作,都在这个类里面
        $param['app_id'] = 'appid';//支付宝分配给开发者的应用ID
        $param['method'] = 'method';//接口名称
        $param['charset'] = 'charset';//请求使用的编码格式
        $param['sign_type'] = 'sign_type';//商户生成签名字符串所使用的签名算法类型
        $param['timestamp'] = 'timestamp';//发送请求的时间
        $param['version'] = 'version';//调用的接口版本，固定为：1.0
        $param['notify_url'] = 'notify_url';//支付宝服务器主动通知地址
        $param['biz_content'] = $con;//业务请求参数的集合,长度不限,json格式
        
        //生成签名
        $paramStr = $Client->getSignContent($param);
        $sign = $Client->alonersaSign($paramStr,$this->alipay_config['rsaPrivateKey'],'RSA2');
        
        $param['sign'] = $sign;
        $str = $Client->getSignContentUrlencode($param);
        
        echo json_encode($str);
        die();
    }
    
    //回调数据
    public function notify(){
        $aop = new \AopClient();
        $public_path = "key/rsa_public_key.pem";//公钥路径
        $aop->alipayrsaPublicKey = "支付宝公钥";
        //此处验签方式必须与下单时的签名方式一致
        $flag = $aop->rsaCheckV1($_POST, $public_path, "RSA2");
        //验签通过后再实现业务逻辑，比如修改订单表中的支付状态。
        /**
         *  ①验签通过后核实如下参数out_trade_no、total_amount、seller_id
         *  ②修改订单表
        **/
        
        //商户订单号
        $order_sn = $out_trade_no = $_POST['out_trade_no'];
        //支付宝交易号
        $trade_no = $_POST['trade_no'];
        //交易状态
        $trade_status = $_POST['trade_status'];
    
        if($_POST['trade_status'] == 'TRADE_FINISHED') {
            //判断该笔订单是否在商户网站中已经做过处理
            //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
            //如果有做过处理，不执行商户的业务程序
    
            //注意：
            //该种交易状态只在两种情况下出现
            //1、开通了普通即时到账，买家付款成功后。
            //2、开通了高级即时到账，从该笔交易成功时间算起，过了签约时的可退款时限（如：三个月以内可退款、一年以内可退款等）后。
    
            //调试用，写文本函数记录程序运行情况是否正常
            //logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
    
            // 支付宝解释: 交易成功且结束，即不可再做任何操作。
        }
        else if ($_POST['trade_status'] == 'TRADE_SUCCESS') {
            //判断该笔订单是否在商户网站中已经做过处理
            //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
            //如果有做过处理，不执行商户的业务程序
    
            //注意：
            //该种交易状态只在一种情况下出现——开通了高级即时到账，买家付款成功后。
    
            //调试用，写文本函数记录程序运行情况是否正常
            //logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
    
            //支付宝解释: 交易成功，且可对该交易做操作，如：多级分润、退款等。
        }
        
        //打印success，应答支付宝。必须保证本界面无错误。只打印了success，否则支付宝将重复请求回调地址。
        echo 'success';
    }
}
?>