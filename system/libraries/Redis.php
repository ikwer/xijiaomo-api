<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Redis缓存驱动，适合单机部署、有前端代理实现高可用的场景，性能最好
 * 有需要在业务层实现读写分离、或者使用RedisCluster的需求，请使用Redisd驱动
 *
 * 要求安装phpredis扩展：https://github.com/nicolasff/phpredis
 */
class CI_Redis {

    protected $options = [
        'host' => '127.0.0.1',
        'port' => 6379,
        'password' => '',
        'select' => 0,
        'timeout' => 0,
        'expire' => 0,
        'persistent' => false,
        'prefix' => '',
    ];
    public $handler = '';

    /**
     * 构造函数
     * @param array $options 缓存参数
     * @access public
     */
    public function __construct() {
        if (!class_exists('\Redis', false)) {
            echo 'Redis类不存在，可能是没有安装php_redis扩展';
            return false;
        }
        $this->handler = new \Redis();
        $this->handler->connect($this->options['host'], $this->options['port']);
    }

    /**
     * 读取缓存
     * @access public
     * @param string $name 缓存变量名
     * @param mixed  $default 默认值
     * @return mixed
     */
    public function get($name, $default = false) {
        try {
            $result = $this->handler->get($name);
        } catch (Exception $exc) {
            return FALSE;
        }
        return $result ? true : false;
    }

    /**
     * 写入缓存
     * @access public
     * @param string    $name 缓存变量名
     * @param mixed     $value  存储数据
     * @param integer   $expire  有效时间（秒）
     * @return boolean
     */
    public function set($name, $value, $expire = null) {
        if (is_null($expire)) {
            $expire = $this->options['expire'];
        }
        //对数组/对象数据进行缓存处理，保证数据完整性  byron sampson
        try {
            if (is_int($expire) && $expire) {
                $result = $this->handler->setex($name, $expire, $value);
            } else {
                $result = $this->handler->set($name, $value);
            }
        } catch (Exception $exc) {
            return FALSE;
        }
        return $result ? true : false;
    }

    //加入队列头
    public function lPush($key, $value) {
        if ($key === '' or $value === '') {
            return false;
        }
        try {
            $result = $this->handler->lPush($key, $value);
        } catch (Exception $exc) {
            return FALSE;
        }
        return $result ? true : false;
    }

    //从队列尾部移除队列
    public function rPop($key) {
        try {
            $result = $this->handler->rPop($key);
        } catch (Exception $exc) {
            return FALSE;
        }
        return $result ? true : false;
    }

    function lindex($key, $end = -1) {
        try {
            $result = $this->handler->lindex($key, $end);
        } catch (Exception $exc) {
            return FALSE;
        }
        return $result ? true : false;
    }

    //list长度
//    function lsiez($key) {
//        try {
//            $result = $this->handler->lSize($key);
//        } catch (Exception $exc) {
//            return FALSE;
//        }
//        return $result ? $result : false;
//    }
    //获取列表长度
    function lLen($key) {
        try {
            $result = $this->handler->lLen($key);
        } catch (Exception $exc) {
            return FALSE;
        }
        return $result ? $result : false;
    }

    //返回指定键存储在列表中指定的元素。 0第一个元素，1第二个… -1最后一个元素，-2的倒数第二…错误的索引或键不指向列表则返回FALSE。
    function lget($key, $num) {
        try {
            $result = $this->handler->lget($key, $num);
        } catch (Exception $exc) {
            return FALSE;
        }
        return $result ? $result : false;
    }

    //按索引获取数据
    function lrange($key, $start = 0, $end = -1) {
        try {
            $result = $this->handler->lrange($key, 0, -1);
        } catch (Exception $exc) {
            return FALSE;
        }
        return $result ? $result : false;
    }

    //清除所有
    function flushall() {
        $result = $this->handler->flushAll();
        return $result ? true : false;
    }

}
