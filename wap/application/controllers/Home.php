<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('Member_model_v1');
    }
    
    function index() {
//         var_dump($this->input->cookie());
      $this->load->model('public_model_v1');
        //获取首页banner
        $view = $this->public_model_v1->banner();
        $this->load->view('home/index',$view);
    }

    function goodsList(){
        $this->load->library('session');
        $good_type = $this->input->get('id');
        $this->input->set_cookie('goods_type',$good_type,1200);
        $this->load->model('public_model_v1');
        $view = $this->public_model_v1->option_info($good_type);
        $this->load->view('home/goodslist',$view);
    }
    
    //发送注册短信
    public function send_code() {
           $mobile = $this->input->post('account');
        if (!preg_match('/^1\d{10}/', $mobile)) {
            ajax_return(1002);
        }
        //入库保存
        $code = rand(1000, 9999);
        $this->load->model('public_model_v1');
        $inset_sms = $this->public_model_v1->inset_sms_code($mobile, $code);
        if (empty($inset_sms['status'])) {
            ajax_return(-1, $inset_sms['msg']);
        }
        //云片短信发送demo
        $this->load->library('juhesms');
        $content = "【喜脚么】您的验证码是$code";
        $send_rs = $this->juhesms->send_phone($mobile, $content);
        if($send_rs['code'] === 0 ){
            $this->load->library('session');
            $_SESSION['code'] = $code;
            $data['code_id'] = $inset_sms['data']['send_id'];
            $data['phone'] = $mobile;
            $data['code'] = $code;
            ajax_return(0, '短信发送成功请注意查收', $data);
        }else{
            ajax_return(-1, '验证码发送失败');
        }
    }
    
    
    function login(){
       $this->load->library('session');
       $this->load->helper('url');
       if($this->input->post('account') && $this->input->post('code')){
           $mobile = $this->input->post('account');
           $code = $this->input->post('code');
           $url = $this->input->post('url');
           if (!preg_match('/^1\d{10}/', $mobile)) {
               ajax_return(-1, '请填写正确的手机号码');
           }
           if($_SESSION['code'] != $code){
               ajax_return(-1, '请填写正确的验证码');
           }
           $this->load->model('User_model_v1');
           $inset = $this->User_model_v1->inset_user($mobile);
           if($inset['status']==true){
               ajax_return(1, '登陆成功',$url);
           }
       }else{
           $view['url'] = $this->input->get('f')?$_SERVER['HTTP_REFERER']:'/index.php/Home/index';
           $this->load->view('home/login',$view);
       }
    }
    
     /*
     * 我的订单
     */
    public function orderList(){
        $uid = $this->input->cookie('uid');
        if(empty($uid)){
            $this->showmessage('请登陆','/index.php/Home/login?f=1');
        }
        $status = $this->input->get('order_status');
        $status = $status?$status:1;
        $page = $this->input->get('page');
        //$page = $page?$page:1;
       // --$page;
       // $page_num = 10;
//         if(empty(intval($uid)) || empty(intval($status))){
//           ajax_return(-1,'缺少重要参数');
//         }
        $where = ' uid = '.$uid;
        switch ($status) {
            case 1:$where .=" and order_status in(0,1,5,6,7,8,9)";break;
            case 2:$where .=" and order_status in(2,4)";break;
            case 3:$where .=" and order_status = 3";break;
        }
        --$page;
        $page_num = 10;
        //查询总记录数；
        $countNum = $this->db->select(array('a.order_id','a.order_sn','pay_status','a.goods_name','a.goods_price','a.tid','a.add_time','a.order_status','b.goods_id','b.goods_image','is_comment'))
        ->from('order_list a')
        ->join('goods b','a.goods_id=b.goods_id')
        ->where($where)
        ->order_by('a.order_id','DESC')
        ->count_all_results();
        $rs = $this->db->select(array('a.order_id','a.order_sn','pay_status','a.goods_name','a.goods_price','a.tid','a.add_time','a.order_status','b.goods_id','b.goods_image','is_comment'))
                    ->from('order_list a')
                    ->join('goods b','a.goods_id=b.goods_id')
                    ->where($where)
                    ->order_by('a.order_id','DESC')
                    ->limit($page_num,$page_num * $page)
                    ->get();
        $order_info = $rs->result_array();
                    //->limit($page_num)
                   // ->offset($page*$page_num)
                    //->get('order_list');
               
        if($order_info){
            foreach($order_info as $k=>$v){
                //获取技师信息
                if($v['tid']){
                      $techInfo = $this->Member_model_v1->getTechInfoModel($v['tid']);           
                      $order_info[$k]['tech_name'] = $techInfo['nickname'];
                      $order_info[$k]['mobile'] = $techInfo['mobile'];
                }
                //转换时间
                if($v['add_time']){
                    $order_info[$k]['add_time'] = date('Y-m-d',$v['add_time']);
                }
                //进行中的订单
                 if($status==1){               
                    if($v['pay_status']){
                        if($v['order_status']==0){
                            $order_status_id = 1;//支付成功
                        }elseif(in_array($v['order_status'],array('7','8','9'))){
                            $order_status_id = 2;//退款中
                        }elseif($v['order_status']==5 || $v['order_status'] == 6){
                            $order_status_id = 3;//进行中
                        }
                    }else{
                        $order_status_id = 4;//处理中待支付
                    }
                    $order_status = 1;
                 }
                 //已完成的订单
                 if($status == 2){
                     if($v['pay_status']){
                           if($v['order_status']==2){
                               $order_status_id = 5;//订单已完成；
                           }
                           if($v['order_status']==4){
                               $order_status_id = 6;//退款已完成；
                           }
                     }
                     $order_status = 2;
                 }
                 //已取消
               //  $status == 3 && $order_status_id = 7;//订单已失效
                 if($status == 3){
                     $order_status_id = 7;
                     $order_status = 3;
                 }
                $order_info[$k]['order_status_id'] = $order_status_id;
                $order_info[$k]['order_status'] = $order_status;
            }
        }
        $view['countNum'] = $countNum;
        if($this->input->get('ajax')){
           $html = '';
           foreach($order_info as $k=>$v){
            $html .='<li class="clearfix">
            <div class="pic_list"><span>订单号：'.$v['order_sn'].'</a></div>
			<div class="order_detail">
				<div class="order_up">
					<div class="order_left">
						<img src="'.$v['goods_image'].'"/>
						<h3>深度全身治疗</h3>
						<p>技师：'.$v['tech_name'].'</p>
						<p>电话：'.$v['mobile'].'</p>
					</div>';
					$html .='<div class="order_right">';
					     if($v['order_status_id']=='1'){
						      $html.='<p>支付成功</p>';
						}elseif($v['order_status_id']=='2'){
						      $html .='<p class="order_ding">退款中</p>';
						}else if($v['order_status_id']=='3'){
						      $html .='<p>服务进行中</p>';
						 }else if($v['order_status_id']=='4'){
						      $html .='<p class="order_ding">支付中</p>';
						}else if($v['order_status_id']=='5'){
						      $html .='<p>已完成</p>';
						 }else if($v['order_status_id']=='6'){
						      $html .='<p>退款已完成</p>';
						}else if($v['order_status_id']=='7'){
						      $html .='<p>订单已失效</p>';
						}
						$html .='<p style="padding-top: 30px;font-size: 14px;">总计：'.$v['goods_price'].'元</p>';
					$html .='</div>';
				$html .='</div>';
				if($v['order_status_id']=='1'){
				$html .='<div class="order_down"><a href="/index.php/Order/refundstep?oid='.$v['order_id'].'">';
					$html .='<button class="bat">申请退款</button>';
				$html .='</a></div>';
				 }elseif($v['order_status_id']=='2'){
				$html .='<div class="order_down"><button class="bat">退款进度</button></div>';
				 }else if($v['order_status_id']=='4'){
					$html .='<div class="order_down">
					<a href="/index.php/Order/orderPay?oid='.$v['order_id'].'"><button class="agin">继续付款</button></a>
					<button class="uxiao" id="btn" onclick="ordercCancel('.$v['order_id'].')" >取消订单</button>
		        </div>';
				 }else if($v['order_status_id']=='5'){
					$html .='<div class="order_down">';
					   if($v['is_comment']){
					    $html .='<button class="have">已评论</button>';
					    }else{
					 $html .='<a href="my_conmment.html"><button class="bat">去评论</button></a>';
					}
				$html .='</div>';
					
					}else if($v['order_status_id']=='7'){
					$html .='<div class="order_down">
					<a href="/index.php/Order/placeOrder?goods_id='.$v['goods_id'].'&tid='.$v['tid'].'&payAgain=1"><button class="agin">再来一单</button></a>
					<button class="uxiao" onclick="orderDel('.$v['order_id'].')">删除订单</button>';
				$html .'</div>';
				 }
			$html .='</div>';
		$html .='</li>';
		}
            
          ajax_return(0,'获取数据成功',$html)  ;
//           var_dump($html);
            
            
            
            
            
            
            
            
        }else{
            $view['orderlist'] = $order_info;
            $view['status'] = $status;
            $this->load->view('home/orderlist',$view);
        }
    }
    
        public function nearTecher() {
            $this->load->library('session');
            $longitude = $this->input->cookie('longitude');
            $latitude = $this->input->cookie('latitude');
            $goods_type = $this->input->cookie('goods_type');
            $goods_id = $this->input->get('goods_id');
            $this->input->set_cookie('goods_id',$goods_id,1200);
            if($goods_type){
                $goodsOptionArr = $this->db->select('cate_id,publicity_img')->from('goods_option')->where(['cate_id'=>$goods_type])->get()->row_array();
            }
            $page = 1;
            if(empty($longitude) || empty($latitude)){
             $this->showmessage('定位失败','/index.php/Home/index');   
            }
            $distance = 5;
            $rows['goodsOption'] = $goodsOptionArr;
            $rows['techerList'] = $this->Member_model_v1->getLocation($longitude,$latitude,$distance,$page);
            $this->load->view('home/techerList',$rows);
    
        }
        public function lat(){
            //$this->load->library('session');
            $str = $this->input->post();
            $latArr = object_to_array(json_decode($str['str']));
//             var_dump($latArr);
            //$this->session->set_userdata(array('longitude'=>$latArr['lng'],'latitude'=>$latArr['lat']));
            $this->input->set_cookie('longitude', $latArr['lng'], 20000 );
            $this->input->set_cookie('latitude', $latArr['lat'], 20000 );
            //ajax_return(1,'获取数据成功',$latArr);
        }
//         public function techInfo(){
//             $tid = $this->input->post('tid');
//             $goods_type = $this->input->post('goods_type');
//             $user_id = '30';
//             $param = $this->input->post('tid');
//             $this->load->model('Tech_model_v1');
//             $rs = $this->Tech_model_v1->tech_msg($user_id, $tid);
//             $this->load->view('home/techerInfo',$rs['data']);
//         }
    	public function techInfo()
    	{
//             $param=$this->input->get();
            
             $tid = $this->input->get('tid');
             $this->input->set_cookie('tid',$tid,20000);
//              $goods_type = $this->input->get('goods_type');
            
    //        var_dump($param);die;
//             if(empty($param))
//             {
//                 echo "非法请求";
//             }
//             else
//             {
//                 $tid=intval($param['tid']);
                $techer_query=$this->db->query("SELECT t.mobile,t.nickname,c.headurl,c.`desc` FROM ( SELECT mobile,tid,nickname FROM xjm_techer WHERE tid = $tid ) t JOIN ( SELECT tid,headurl,`desc` FROM xjm_techer_checkinfo WHERE tid = $tid ) c ON t.tid = c.tid");
                $data['techer_row']=$techer_query->row_array();
    
                $techer_photo_query=$this->db->query("SELECT pic_url,video_url,type FROM xjm_techer_photo WHERE tid=$tid AND type=1 ORDER BY add_time DESC LIMIT 4");
                $data['techer_photo_result']=$techer_photo_query->result_array();
//                 var_dump($techer_photo_query->result_array());
    
                $techer_photo=$this->db->query("SELECT pic_url,video_url,type FROM xjm_techer_photo WHERE tid=$tid AND type=1  ORDER BY add_time");
                $data['techer_photo_nums']=$techer_photo->num_rows();
                $data['photo_video']=$techer_photo->result_array();
    
    
                $commemt_query=$this->db->query("SELECT nick_name,FROM_UNIXTIME(add_time,'%Y-%m-%d') dates,headurl,content FROM xjm_user_comment WHERE tid=$tid ORDER BY add_time DESC LIMIT 0,4");
                $data['comment_result']=$commemt_query->result_array();
    
                $commemt_querys=$this->db->query("SELECT nick_name,FROM_UNIXTIME(add_time,'%Y-%m-%d') dates,headurl,content FROM xjm_user_comment WHERE tid=$tid ORDER BY add_time DESC");
                $data['commemt_num']=$commemt_querys->num_rows();
    
                if($data['commemt_num']==0)
                {
                    $data['great_bili']=0;
                }
                else
                {
                    $great_query=$this->db->select('id')->from('xjm_user_comment')->where('tid='.$tid." and type=0")->get();
                    $great_num=$great_query->num_rows();
    
                    $cc=$this->db->select('id,comment_tag')->from('xjm_user_comment')->where('tid='.$tid." and comment_tag IS NOT NULL")->get();
                    $cc_result=$cc->result_array();
                    if(!empty($cc_result))
                    {
                        foreach($cc_result as $k=>$v)
                        {
                            (empty($v['comment_tag'])) ? $tag=0 : $tag=$v['comment_tag'];
                            $tag_query=$this->db->query("SELECT tag_name FROM xjm_comment_tag_set WHERE id IN($tag)");
                            $re=$tag_query->result_array();
                            $tag_result[$k]=$re;
                        }
                    }
                    else
                    {
                        $tag_result='';
                    }
    
    
                    $data['tag']=$tag_result;
                    $data['great_bili']=ceil(($great_num/$data['commemt_num'])*100);
                }
                $this->load->view('home/techerInfo',$data);
//             }
    	}
    	//招商加盟
    	function joined(){
    	    $this->load->view('home/joined');
    	}
    	//app下载引导页
    	function appDownload(){
    	    $this->load->view('home/appDownload');
    	}
    	//我的
    	function mymain(){
    	    $uid = $this->input->cookie('uid');
    	    $userInfo = $this->db->select('nickname,headurl')->where(['uid'=>$uid])->from('user')->get()->row_array();
    	    $view['userInfo'] = $userInfo;
    	    $this->load->view('home/mymain',$view);
    	}
//     	function showmessage(){
    	    
//     	    $this->load->view('home/showmessage');
//     	}
    	
}
