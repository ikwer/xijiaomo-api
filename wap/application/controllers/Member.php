<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {
    public function __construct() {
        parent::__construct();
            $this->load->model('Member_model_v1');
            $this->uid = $this->input->cookie('uid');
             $this->load->helper('url');
            if(empty($this->uid)){
              redirect('/Home/login?f=1','auto');  
            }
    }
    
    //我的收藏
    public function favoriteData(){
        $param = $this->input->cookie();
        $uid = $param['uid'];
//         $page = $param['page']?$param['page']:1;
        $page = 1;
        empty(intval($uid)) && ajax_return(-1,'缺少重要参数');
        $favoriteData = $this->Member_model_v1->getFavoriteDataModel($uid,$page);
        foreach($favoriteData as $k=>$v){
            if($v['birthday']){
                $favoriteData[$k]['birthday'] = getAge($v['birthday']);
            }
            if($v['on_time']){
                $favoriteData[$k]['on_time'] = date('Y-m-d',$v['on_time']);
            }
            if($v['sid']){
                $commentArr1 = $this->db->from('user_comment')->where(['tid'=>$v['sid']])->get()->result_array();
                $commentArr2 = $this->db->from('user_comment')->where(['tid'=>$v['sid'],'type'=>'0'])->get()->result_array();
                if(count($commentArr1) > 0 && count($commentArr2) > 0){
                    $favoriteData[$k]['probability'] = intval(count($commentArr2)/count($commentArr1)*100).'%';
                }else{
                    $favoriteData[$k]['probability'] = '100%';
                }
    
            }
        }
        $view['favorite'] = $favoriteData;
        $this->load->view('member/favorite',$view);
    }   
    //取消收藏
    public function cancelFavorite(){
        $uid = $this->input->cookie('uid');
        $id = $this->input->post('tid');
        if(empty(intval($id)) || empty(intval($uid))){
            ajax_return(-1,'缺少重要参数');
        }
        if($this->Member_model_v1->cancelFavoriteModel($id,$uid)){
            ajax_return(1,'取消收藏成功');
        }else{
            ajax_return(-1,'取消收藏失败');
        }
    }
    
    
    function activity(){
        $this->load->view('member/activity');
    }
    
    
    public function point(){
        $uid = $this->input->cookie('uid');
        $totalPoint = $this->Member_model_v1->getUserPoint($uid);
        $view['totalPoint'] = $totalPoint['point'];
        $this->load->view('member/point');
        
    }
    
    /*
     * 积分明细
     */
    
    public function pointList(){
        $uid = $this->input->cookie('uid');
        $page = 1;
//         $page =  $page?$page:1;
          $data =  $this->Member_model_v1->pointListModel($uid,$page);
          $view['data'] = $data;
        $this->load->view('member/pointlist',$view);
    }
    
    
    
    /*
     * 可兑换的所有优惠券
     */
    public function couponList(){
        if(empty(intval($this->uid))){
            ajax_return(-1,'缺少重要参数');
        }
        $page = 1;
        $userPoint = $this->Member_model_v1->getUserPoint($this->uid);
        $couponList =  $this->Member_model_v1->couponListModel($page);
        foreach($couponList as $k=>$v){
            $couponList[$k]['status'] = 0;
            if($userPoint['point']>$v['expoint']){
                $couponList[$k]['status'] = 1;
            }
    
        }
        $view['couponList'] = $couponList;
        $this->load->view('member/couponlist',$view);
    }
    
    
    
    /*
     * 兑换优惠券
     */
    public function exchCoupon(){
        //         $uid = $this->input->post('user_id');
        //         $id = $this->input->post('sid');
//         $param = $this->input->post();
//         check_sign($param);
//         //重新切割
//         $param=decodeParam($param);
//         $uid = $param['user_id'];
        $id = $this->input->post('sid');
//         $uid = $this->uid;
//         if(empty(intval($this->uid)) || empty(intval($id))){
//             ajax_return(-1,'缺少重要参数');
//         }
        $couponInfo = $this->Member_model_v1->getCouponModel($id);
        $userInfo = $this->Member_model_v1->getUserPoint($this->uid);
        if($couponInfo && $userInfo){
            if($couponInfo['expoint']>$userInfo['point']){
                ajax_return(-1,'积分不够');
            }
            $data['uid'] = $this->uid;
            $data['cid'] = $id;
            $data['name'] = $couponInfo['name'];
            $data['type'] = 1;
            $data['from'] = 4;
            $data['money'] = $couponInfo['money'];
            $data['send_time'] = time();
            $data['end_time'] = strtotime("+".$couponInfo['validity_time']." day");
            $data['status'] = 0;
            if($this->Member_model_v1->commonAddData('xjm_user_coupon',$data)){
                $point = $userInfo['point'] - $couponInfo['expoint'];
                $this->db->where(['uid'=>$this->uid])->update('xjm_user',array('point'=>$point));
                if($this->db->affected_rows()){
                    ajax_return(1,'兑换成功');
                }else{
                    ajax_return(-1,'兑换失败');
                }
            }
        }else{
            ajax_return(-1,'缺少重要参数');
        }
    
    }
    
    /*
     *获取城市 
     */
    
    public function getCity(){
        $already = $this->db->from('city')->where(['aid' > 0,'status'=>'2'])->get()->result_array();
        $soon =  $this->db->from('city')->where(['aid' > 0,'status'=>'1'])->get()->result_array();
        $cityInfo['already'] = $already?$already:'暂无数据';
        $cityInfo['soon'] = $soon?$soon:'暂无数据';
        $view['cityinfo'] = $cityInfo;
        $this->load->view('member/cityinfo',$view);
    } 
    //获取新消息
    function newNotice(){
        //      $user_id = $this->input->post('user_id');
        $param = $this->input->post();
        check_sign($param);
        //重新切割
        $param=decodeParam($param);
        $user_id = $param['user_id'];
        if(empty($user_id)){
            ajax_return(-1,'缺少重要参数');
        }
        $noticeRs= $this->db->select('*')->from('xjm_notice')->where(['uid'=>$user_id,'is_read'=>0])->get();
        $noticeNum = count($noticeRs->result_array());
        
//         if($noticeNum){
//             ajax_return(0,'获取数据成功',$noticeNum);
//         }else{
//             ajax_return(1,'暂无数据');
//         }
        $this->load->view('member/mesage');
    }
    //所有站内信
    function msg() {
        $uid= $this->input->cookie('uid');
        $page = 1;
        //         $page ? $page : $page = 1;
        if (empty(intval($uid))) {
            ajax_return(-1, '缺少重要数据');
        }
        $this->load->model('public_model_v1');
        $rs = $this->public_model_v1->unread($uid, $page);
        $view['notice'] = $rs['data'];
        $this->load->view('member/msg',$view);
    }
    //更改公告状态
    function read_msg() {
        $uid = $this->input->cookie('uid');
        $msg_id = $this->input->post('m_id');
        $this->load->model('public_model_v1');
        $rs = $this->public_model_v1->change_msg($uid, $msg_id);
        if ($rs['status']) {
            ajax_return(1, '成功');
        } else {
            ajax_return(-1, '失败');
        }
    }
    
    //提交评论
    function subcomment(){
        if(!$this->input->post()){
            $order_id = $this->input->get('oid');
            $orderInfoRs = $this->db->select('a.order_id,a.tid,a.goods_id,b.headurl')->from('order_list a')->join('techer_checkinfo b','a.tid=b.tid')->where(['order_id'=>$order_id])->get();
            $orderInfo = $orderInfoRs->row_array();
            $tNicknameRs = $this->db->select('nickname')->from('techer')->where(['tid'=>$orderInfo['tid']])->get();
            $tNickname = $tNicknameRs->row_array();
            $orderInfo['nicknamer'] = $tNickname['nickname'];
            $view['techInfo'] = $orderInfo;
            $tagArr = $this->db->select('a.goods_type,b.good_tag,b.bad_tag')->from('goods a')->join('goods_option b','a.goods_type = b.cate_id')->where(['goods_id'=>$orderInfo['goods_id']])->get()->row_array();
            if($tagArr['good_tag']){
                $tagArr['goodtag'] = explode(',',$tagArr['good_tag']);
                foreach($tagArr['goodtag'] as $k=>$v){
                    $goodtagname[] = $this->db->from('comment_tag_set')->where(['id'=>$v])->get()->row_array();
                }
            }  
            if($tagArr['bad_tag']){
                $tagArr['bad_tag'] = explode(',',$tagArr['bad_tag']);
                foreach($tagArr['bad_tag'] as $k=>$v){
                    $badtagname[] = $this->db->from('comment_tag_set')->where(['id'=>$v])->get()->row_array();
                }
            }
            $view['goodtagname'] = $goodtagname;
            $view['badtagname'] = $badtagname;
            $this->load->view('member/subcomment',$view);
        }else{
            $param = $this->input->post();
            $order_id = $param['order_id'];
            $uid =$this->input->cookie('uid');
            if($order_id){
                $orderInfo = $this->db->select('a.order_sn,a.tid,a.uid,a.mobile,a.goods_name,b.nickname,b.headurl')->where(['a.order_id'=>$order_id])->from('order_list a')->join('user b','a.uid=b.uid')->get()->row_array();
                $content = (!empty($param['wenben']))?$param['wenben']:'默认好评';
                $comment_type = (!empty($param['comment_type']))?$param['comment_type']:'';
                $tag_id = (!empty($param['tag_id']))?$param['tag_id']:'';
                $insertArr['uid'] = $uid;
                $insertArr['tid'] = $orderInfo['tid'];
                $insertArr['goods_name'] = $orderInfo['nickname'];
                $insertArr['headurl'] = $orderInfo['headurl'];
                
                $insertArr['mobile'] = $orderInfo['mobile'];
                $insertArr['type'] = $comment_type;
                $insertArr['content'] = $tag_id;
                $insertArr['add_time'] = time();
                $insertArr['comment_tag'] = $comment_type;
                $insertArr['content'] = $content;
                $insertArr['order_id'] = $orderInfo['order_sn'];
                $insertArr['oid'] = $order_id;
                if($this->db->insert('user_comment',$insertArr)){
                    $this->db->where(['order_id'=>$order_id])->update('order_list',array('is_comment'=>1));
                    $this->load->helper('url');
                    redirect('/Home/orderlist?order_status=2','auto');
                }
            }
        }
    }
    
    //设置
    function setUp(){
        $uid = $this->input->cookie('uid');
        $userInfo = $this->db->select('nickname,headurl,mobile')->from('user')->where(['uid'=>$uid])->get()->row_array();
        $view['userInfo'] = $userInfo;
        $this->load->view('member/set_up',$view);
    
    }
    
    /*
     * 意见反馈
     */
    public function addFeedback(){
//         $param = $this->input->post();
//         check_sign($param);
//         //重新切割
//         $param=decodeParam($param);
        if(!$this->input->post('formhash')){
            $this->load->view('member/feedback');
        }else{
            $uid= $this->input->cookie('uid');
            $param = $this->input->post();
//          if(empty(intval($uid)) || empty($content)){
//                 ajax_return(-1,'缺少重要数据');
//          }
            $data['uid'] = $uid;
            $data['content'] = $param['content'];
            $data['add_time'] = time();
            $data['type'] = $param['feedtype'];
            if($this->Member_model_v1->commonAddData('xjm_feedback',$data)){
                $this->load->helper('url');
                redirect('/Member/setUp');
            }
        }
    }
    
    //技师关于我们
    public function user_about()
    {
        $query=$this->db->select('option')->from('xjm_option')->where("id=1")->limit(1)->get();
        $result=$query->row_array();
        if($result){
            $view['user_about'] = json_decode($result['option'],'true');
        }
        if($this->input->get('type')){
            $this->load->view('member/customer_center',$view);
        }else{
            $this->load->view('member/user_about',$view);
        }
//         if()
    }
    //帮助中心
    public function help()
    {
//         $source=$this->input->get('source');
//         (empty($source)) ? $source='user' : $source=$source;
    
//         if($source=='user')
//         {
            $query=$this->db->select('title,content')->from('xjm_article')->where('type=7 AND status=0')->order_by('sort_order')->get();
            $view['result']=$query->result_array();
//         }
//         else
//         {
//             $query=$this->db->select('title,content')->from('xjm_article')->where('type=8 AND status=0')->order_by('sort_order')->get();
//             $view['result']=$query->result_array();
//         }
//                var_dump($view);die;
        $this->load->view('member/help',$view);
    
    }
    
    /*
     * 我的优惠券
     */
    public function allCoupon(){
        $uid = $this->input->cookie('uid');
        $page = 1;
        if(empty(intval($uid))){
            ajax_return(-1,'缺少重要参数');
        }
        $view['couponList'] = $this->Member_model_v1->getCouponByUid($uid,$page);
        $this->load->view('member/allcoupon',$view);
    }
    
    public function logout(){
        $this->load->helper('cookie');
        $this->load->helper('url');
        delete_cookie('uid');
        redirect('Home/login','auto');
    }
   
    
    
    
    
        
}