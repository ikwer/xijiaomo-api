<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller {
    function placeOrder(){
        $t = $this->input->get('t');
            $this->load->helper('url');
        if(!$this->input->cookie('uid')){
              redirect('Home/login?f=1');  
        }
        if(!$t){
            if($this->input->get('payAgain')){
                //处理再来一单
                $uid = $this->input->cookie('uid');
                $param = $this->input->get();
                $tid = $param['tid'];
                $this->input->set_cookie('tid',$tid,1200);
                if($param['goods_id']){
                    $goodsOptionArr = $this->db->select('goods_type')->from('goods')->where(['goods_id'=>$param['goods_id']])->get()->row_array();
                    $goods_type = $goodsOptionArr['goods_type'];
                }
            }else{
                //正常提交订单
                $param = $this->input->cookie();
                $tid = $this->input->cookie('tid');
                $uid = $param['uid'];
                if(empty($param['uid']) || empty($param['goods_id'])){
                    //ajax_return(-1,'已过期请重新下单');
                    $this->showmessage('已过期请重新下单','/index.php/Home/index');
                }
                $goods_type = $param['goods_type'];
            }
            $goodsArr = $this->db->select('goods_price,server_time,goods_name')
                        ->from('goods')->where(['goods_id'=>$param['goods_id']])
                        ->get()->row_array();
            $userInfo =  $this->db->select('mobile')->from('user')
                        ->where(['uid'=>$uid])->get()->row_array();
            $techInfo = $this->db->select('nickname')->from('techer')
                        ->where(['tid'=>$tid])->get()->row_array();
            //获取商品信息
            $this->load->model('Order_model_v1');
            if (!$goodsArr) {
                //ajax_return(-1, '不存在的项目');
                $this->showmessage('定位失败','/index.php/Home/index');
            }
            $this->load->model('public_model_v1');
            $rs = $this->public_model_v1->getTaboo($goods_type);
            if($rs){
             $orderInfo['taboo'] = $rs[0]['taboo'];
            }
            $coupon_rs = $this->Order_model_v1->coupon_num($uid, $goodsArr['goods_price']);
            $orderInfo['coupon_num'] = $coupon_rs['data']['all_num'];
            $orderInfo['usermobile'] = $userInfo['mobile'];
            $orderInfo['technickanme'] = $techInfo['nickname'];
            $orderInfo['goods_price'] = $goodsArr['goods_price'];
            $orderInfo['server_time'] = $goodsArr['server_time'];
            $orderInfo['goods_name'] = $goodsArr['goods_name'];
            $this->input->set_cookie('orderInfo',json_encode($orderInfo),1200);
            $data['orderInfo'] = $orderInfo;
            //获取该技师服务时间段
        }else{
            $data = $this->input->cookie();
            $data['orderInfo'] = !empty(json_decode($data['orderInfo'],true))?json_decode($data['orderInfo'],true):'';
            $data['coupon'] = !empty($data['coupon'])?json_decode($data['coupon'],true):'';
        }
        $data['todayDate'] = date("m-d");
        $data['TomorrowDate'] = date("m-d",strtotime("+1 day"));
        $this->load->view('order/placeOrder',$data);
    }
    
    //获取系统服务时间
    function server_time(){
//         $param = $this->input->post();
//         check_sign($param);
//         $param=decodeParam($param);
//         if(empty($param['goods_id']) || empty($param['user_id']) || empty($param['tid'])){
//             ajax_return(-1,'缺少重要参数');
//         }\
//         var_dump($this->input->cookie());
        $param = $this->input->cookie();
        $postData = $this->input->post();
        $date_type = (!empty($postData['date_type']))?$postData['date_type']:1;
        $user_id = $param['uid'];
        $goods_id = $param['goods_id'];
        $tid = $param['tid'];
        if(empty($goods_id) || empty($tid)){
            $this->showmessage('订单已过期');            
        }
        $TodayEnd_time = strtotime(date('Y-m-d',strtotime('+1 day')));
        $TodayStart_time = time();
        $TomorrowEnd_time = $TodayEnd_time+86400;
        //$date_type = $date_type?$date_type:1;
        if($date_type==2){
            $now_time = $TodayEnd_time;
            $end_time = $TomorrowEnd_time;
            $date = date("Y-m-d",strtotime("+1 day"));
        }else{
            $now_time = $TodayStart_time;
            $end_time = $TodayEnd_time;
            $date = date("Y-m-d");
        }
        $tomorrow = date("m-d",strtotime("+1 day"));
        $today  = date("m-d");
        if($date_type){
            $serverTimeRs = $this->db->from('server_time')->get();
            $serverTimeArr = $serverTimeRs->result_array();
            $allOrderRs = $this->db->select('date_id')->from('order_list')->where("add_time>".$now_time." and add_time<".$end_time." and tid = $tid")->get();
            $allOrder = $allOrderRs->result_array();
            //已经过期的时间不可选
            foreach($serverTimeArr as $k=>$v){
                $serverTimeArr[$k]['status'] = 1;
                if(strtotime($date.$v['server_time'])<$now_time){
                    // $serverTimeArr[$k]['status'] = 0;
                    unset($serverTimeArr[$k]);
                }
            }
            sort($serverTimeArr);
            //被预约过的时间不可选
            if($allOrder){
                foreach($serverTimeArr as $k=>$v){
                    if($v['status'] == 1){
                        foreach($allOrder as $key =>$val){
                            if($val['date_id'] == $v['id']){
                                $serverTimeArr[$k]['status'] = 0;
                            }
                        }
                    }
                }
            }
            //根据服务时长判断剩下的时间是否可选。
            $rs = $this->db->select('server_time')->from('goods')->where(['goods_id'=>$goods_id])->get();
            $goodsInfo = $rs->row_array();
            $goods_time = $goodsInfo['server_time']*60;
            $goods_time = $goods_time + 30*60;
            $endArr = end($serverTimeArr);
            foreach($serverTimeArr as $k=>$v){
                $k1 = $k+1;
                if($v['id']<$endArr['id']){
                    if($v['status'] == 1 ){
                        if($serverTimeArr[$k1]['status'] ==1){
                            $serverTimeArr[$k]['status'] = 1;
                        }else{
                            $ktime = strtotime($v['server_time'])+$goods_time;
                            $k1_time = $serverTimeArr[$k+1]['server_time'];
                            $k1_time = strtotime($k1_time);
                            if($ktime > $k1_time){
                                $serverTimeArr[$k]['status'] = 0;
    
                            }
                        }
                    }
                }
            }
//             $data['today'] = $today;
//             $data['tomorrow'] = $tomorrow;
//             $data['server_time'] = $serverTimeArr;
               $html = '';
            if($serverTimeArr){
                    $html .='<ul id="serverTime">';
                foreach($serverTimeArr as $k =>$v){
                    if(!$v['status']){
                         $html .= "<li  class='active'><p>".$v['server_time']."</p></li>";
                    }else{
                        $html .= "<li onclick=getDateId('".$v['id']."') value=".$v['id']."><p>".$v['server_time']."</p></li>";
                    }
                }
                     $html .= "<ul>";
            }
            if($serverTimeArr){
                ajax_return(1,'数据处理成功',$html);
            }
        }
    }
    
    
    function confirm_order(){
        $param = $this->input->post();
         if (!preg_match('/^1\d{10}$/', $param['mobile'])) {
            ajax_return(-1, '请填写正确的手机号码');
         }
         if(!$param['address']){
             ajax_return(-1, '请填写地址');
         }
         $area = '武汉';
         $cookieArr = $this->input->cookie();
         $user_id = $cookieArr['uid'];
         $coupon_id = $param['coupon_id'];
         $this->load->model('Order_model_v1');
         if(empty($cookieArr['goods_id'])){
             ajax_return(-1, '订单已过期请重新下单');
         }
         if(!empty($param['date_id']) && $param['date_type']){
             $param['date_type']==1?$rs = date('Y-m-d'):$rs =  date("Y-m-d",strtotime("+1 day"));
//              $date_time = strtotime($rs.' '.$param['date_time']);
             $dateArr = $this->db->from('server_time')->where(['id'=>$param['date_id']])->get()->row_array();
             $date_time = strtotime($rs.$dateArr['server_time']);
         }else{
             $date_time = '';
         }
         $goodsInfoRs = $this->db->select("goods_id,goods_name,goods_price,server_time")->from('goods')->where('goods_id',$cookieArr['goods_id'])->get();
         $goodsInfo = $goodsInfoRs->row_array();
         //获取订单信息  5分钟有效
         $address_id = $this->Order_model_v1->open_address($area);
         if (!$address_id['status']) {
             ajax_return(-1, '下单地区不存在');
         }
         //检测优惠卷是否可用  计算价钱
         $money = $goodsInfo['goods_price'];
         $coupon_price = 0; //优惠券抵扣
         $nowtime = time();
         if ($coupon_id) {
             $coupon_rs = $this->Order_model_v1->check_coupon($user_id, $coupon_id, $goodsInfo['goods_price']);
             if (!$coupon_rs['status']) {
                 ajax_return(-1, '非法操作订单,没有优惠卷');
             }
             if ($coupon_rs['data']['end_time'] < $nowtime) {
                 ajax_return(-1, '已过期优惠卷,无法使用');
             }
             $coupon_price = $coupon_rs['data']['money'];
             $money = $goodsInfo['goods_price'] - $coupon_price;
             $money < 0 && $money = 0;
         }
         //开始下单，预订订单,修改状态 //订单状态 0正常状态  10待支付  20已支付（待服务）   30服务中（技师以上门）  40服务完成   -10取消   -20 退款
         //      $this->db->trans_begin();
         //锁定优惠卷
         if ($coupon_id) {
             $lock_rs = $this->Order_model_v1->lock_coupon($coupon_id);
             if (!$lock_rs) {
                 ajax_return(-1, '预订失败,请重试');
             }
         }
         $orderId = date('Ymd') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
         $update['uid'] = $user_id;
         $update['tid'] = $cookieArr['tid'];
         $update['order_status'] = 1;
         $update['address'] = $param['address'];
         $update['user_note'] = $param['taboo'];   //用户备注
         $update['order_sn'] = $orderId;  //用户定义
         $update['mobile'] = $param['mobile'];
         $update['goods_name'] = $goodsInfo['goods_name'];
         $update['goods_price'] = $goodsInfo['goods_price'];
         $update['goods_totalprice'] = $goodsInfo['goods_price'];
         $update['area_id'] = $address_id['data']['id'];
         $update['coupon_price'] = $coupon_price;   //优惠券抵扣
         $update['order_amount'] = $money;
         $update['add_time'] = time();
         $update['goods_id'] = $cookieArr['goods_id'];
         $update['server_time'] = $goodsInfo['server_time'];
         
         $update['u_lon'] = $cookieArr['longitude'];
         $update['u_lat'] = $cookieArr['latitude'];
         $update['date_type'] = $param['date_type'];
         $update['date_id'] = $param['date_id'];
         $update['date_time'] = $date_time;
         
         $change_order = $this->db->insert('order_list', $update);
         $order_id = $this->db->insert_id();
         if($change_order){
             $this->load->helper('cookie');
             delete_cookie('orderInfo');
             $this->load->helper('url');
             $this->db->where(['tid'=>$cookieArr['tid']])->update('techer',array('is_lock'=>1));
             ajax_return(1,'预定成功','/index.php/Order/orderPay?oid='.$order_id);  
         }else{
             ajax_return(-1,'预定失败');
         }
    }
    
    
    
    
    function address(){
        //处理地址提交
            $user_id =$this->input->cookie('uid');
        if($this->input->post('formhash')){
            $address = $this->input->post('address');
            $house_number = $this->input->post('house_number');
            if(!$address || !$house_number){
                ajax_return(-1,'请填写地址');
            }
            $allOrderInfo = json_decode($this->input->cookie('orderInfo'),true);
            $lng = $allOrderInfo['lng'];
            $lat = $allOrderInfo['lat'];
            $house_number = $this->input->post('house_number');
//             $orderInfo = $this->input->cookie('');
            $this->input->set_cookie('address',$address,12000);
             $this->input->set_cookie('house_number',$house_number,12000);
            $tid = $this->input->cookie('tid');
            if (empty(intval($user_id)) || empty($address) || empty($house_number) || empty($tid)) {
                ajax_return(-1, '非法请求');
            }
            $data['address'] = $address.$house_number;
            $this->load->model('Order_model_v1');
            $location = $this->Order_model_v1->getLocationByTid($tid);
            $jsonStr =  file_get_contents('http://restapi.amap.com/v3/geocode/geo?&output=json&key=211b7971d38e8df2728fda85fd519e9a&address='.$address.'&city=武汉');
            $res = $this->getDistance($location['t_lat'],$location['t_lon'],$lat,$lng);
            if($res/1000 > 5){
                ajax_return(-1, '不在该技师的服务范围内');
            }
            $rs = $this->Order_model_v1->add_address($user_id, $address, $house_number);
            if($rs){
                ajax_return(1,'获取地址成功','/index.php/Order/placeOrder');
            }
        }else{//只用于展示界面
             $orderInfo = json_decode($this->input->cookie('orderInfo'),true);
             $address =  $this->input->get('s');
             $lng = $this->input->get('lng');
             $lat = $this->input->get('lat');
             if($lng && $lat && $address){
                 $orderInfo['lng'] = $lng;
                 $orderInfo['lat'] = $lat;
                 $orderInfo['address'] = $address;
                 $this->input->set_cookie('orderInfo',json_encode($orderInfo),1200);
             }
             $this->load->model('Order_model_v1');
             $rs = $this->Order_model_v1->select_address($user_id);
             $view['address'] = $rs['data'];
             $view['s'] = $address;
            $this->load->view('order/address',$view);
        }
    }
    
    function getDistance($lat1, $lng1, $lat2, $lng2)
    {
        $earthRadius = 6367000; //approximate radius of earth in meters
        $lat1 = ($lat1 * pi() ) / 180;
        $lng1 = ($lng1 * pi() ) / 180;
        $lat2 = ($lat2 * pi() ) / 180;
        $lng2 = ($lng2 * pi() ) / 180;
        $calcLongitude = $lng2 - $lng1;
        $calcLatitude = $lat2 - $lat1;
        $stepOne = pow(sin($calcLatitude / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($calcLongitude / 2), 2);
        $stepTwo = 2 * asin(min(1, sqrt($stepOne)));
        $calculatedDistance = $earthRadius * $stepTwo;
        return round($calculatedDistance);
    }
    //调用腾讯地图
    function add_map(){
        $this->load->view('order/add_map');
    }
    
    function getCoupon(){
        $this->load->helper('url');
        $cid =$this->input->get('cid');
        if(empty($cid)){
        //只用于界面的展示
            $orderInfo = $this->input->cookie();
            $user_id = $orderInfo['uid'];
            $pro_id = $orderInfo['goods_id'];
            $address = '武汉';
    //         $page = $param['page']?$param['page']:1;
            $page  = 1;
            if (empty(intval($user_id)) || empty(intval($pro_id)) || empty($address)) {
                ajax_return(-1, '请求参数有误');
            }
            //获取商品信息
            $this->load->model('Order_model_v1');
            $pro_info = $this->Order_model_v1->product_info($pro_id);
            if (!$pro_info['status']) {
                ajax_return(-1, '不存在的项目');
            }
            //查看当前用户下单地区id 判断优惠卷是否可使用
            $area = $this->Order_model_v1->open_address($address);
            $area_id = 0;
            if ($area['status']) {
                $area_id = $area['data']['id'];
            }else{
                ajax_return(-1, '请填写正确的下单地址');
            }
            
            //获取优惠卷列表
            $coupon_rs = $this->Order_model_v1->coupon_list($user_id, $page, $area_id);
            $order_price = $pro_info['data']['goods_price'];
            $use_coupon = $no_coupon = array();
            foreach ($coupon_rs['data'] as $key => $value) {
                $value['send_time'] = date('Y-m-d', $value['send_time']);
                $value['end_time'] = date('Y-m-d', $value['end_time']);
                if (!$value['area_name']) {
                    $value['area_name'] = '不限地区使用';
                } else {
                    $value['area_name'] = '限' . $value['area_name'] . '地区使用';
                }
                //可以使用  无门槛  总价大于起始价  在当前下单区  0可以使用  0不能使用
                if (($value['type'] == 0 || $value['condition'] <= $order_price) && ($area_id == $value['area_id'] || $value['area_id'] == 0)) {
                    $value['is_use'] = '0';
                    $use_coupon[] = $value;
                } else {
                    $value['is_use'] = '1';
                    $no_coupon[] = $value;
                }
            }
            
            $all_coupon = array_merge($use_coupon, $no_coupon);
            $data['coupon_list'] = $all_coupon ? $all_coupon : NULL;
            $data['can_use_num'] = count($use_coupon);
            $this->load->view('order/coupon',$data);
        }else if($cid == 'delCou'){
            //不使用优惠券
            $this->load->helper('cookie');
            delete_cookie('coupon');
            redirect('/Order/placeOrder?t=1','auto');
        }else{
            //选择优惠券后的处理
             $couponArr = $this->db->select('id,money,name')->from('user_coupon')->where(['id'=>$cid])->get()->row_array(); 
             if($couponArr){
                 if($this->input->cookie('orderInfo')){
                     $oderInfo =json_decode($this->input->cookie('orderInfo'),true);
                     $coupon['coupon_money'] = $couponArr['money'];
                     $coupon['coupon_id'] = $couponArr['id'];
                     $coupon['coupon_name'] = $couponArr['name'];
                     $coupon['order_amount'] = sprintf("%.2f",$oderInfo['goods_price'] - $couponArr['money']);
                    $this->input->set_cookie('coupon',json_encode($coupon),1200);
                    redirect('/Order/placeOrder?t=1','auto');
                 }else{
                     redirect('/Home/index/','auto');
                 }
                 
             }
        }
    }
    function orderPay(){
        $oid = $this->input->get('oid');
        if($oid){
            $orderInfo = $this->db->select('order_sn,goods_price,goods_name,order_amount,coupon_price,server_time')->from('order_list')->where(['order_id'=>$oid])->get()->row_array();
            $view['orderInfo'] = $orderInfo;
            $this->load->view('/order/orderpay',$view);
        }
    }
    
     function pay(){
        header("Content-type: text/html; charset=utf-8");
        $order_sn = $this->input->post('order_id');
        if($order_sn){
           $orderInfo = $this->db->select('order_amount,goods_name')->from('order_list')->where('order_sn',$order_sn)->get()->row_array();
        }
        require_once '/usr/share/nginx/html/api/wap/system/libraries/Pay/wappay/service/AlipayTradeService.php';
        require_once '/usr/share/nginx/html/api/wap/system/libraries/Pay/wappay/buildermodel/AlipayTradeWapPayContentBuilder.php';
        require_once '/usr/share/nginx/html/api/wap/system/libraries/Pay/config.php';
        if (!empty($order_sn)&& trim($order_sn)!=""){
            //商户订单号，商户网站订单系统中唯一订单号，必填
            $out_trade_no = $order_sn;
        
            //订单名称，必填
            $subject = $orderInfo['goods_name'];
        
            //付款金额，必填
            $total_amount = $orderInfo['order_amount'];
        
            //商品描述，可空
            $body = $orderInfo['goods_name'];
        
            //超时时间
            $timeout_express="1m";
        
            $payRequestBuilder = new AlipayTradeWapPayContentBuilder();
            $payRequestBuilder->setBody($body);
            $payRequestBuilder->setSubject($subject);
            $payRequestBuilder->setOutTradeNo($out_trade_no);
            $payRequestBuilder->setTotalAmount($total_amount);
            $payRequestBuilder->setTimeExpress($timeout_express);
        
            $payResponse = new AlipayTradeService($config);
            $result=$payResponse->wapPay($payRequestBuilder,$config['return_url'],$config['notify_url']);
        
            return ;
        }
        
    }
    
    
    function notify()
    {
        require_once("/usr/share/nginx/html/api/wap/system/libraries/Pay/config.php");
        require_once '/usr/share/nginx/html/api/wap/system/libraries/Pay/wappay/service/AlipayTradeService.php';
        $arr=$_GET;
        $alipaySevice = new AlipayTradeService($config);
        $result = $alipaySevice->check($arr);
        if($result){
	       $out_trade_no = htmlspecialchars($_GET['out_trade_no']);
	       $trade_no = htmlspecialchars($_GET['trade_no']);
            $data['pay_status'] = 1;
            $data['order_status'] = 0;
            if($this->db->where('order_sn',$out_trade_no)->update('xjm_order_list',$data)){
                $orderInfo = $this->db->select('order_id,order_amount,uid,tid')->from('order_list')->where(['order_sn'=>$out_trade_no])->get()->row_array();
                $insert['type'] = 'in';
                $insert['money'] = $orderInfo['order_amount'];
                $insert['uid'] = $orderInfo['uid'];
                $insert['tid'] = $orderInfo['tid'];
                $insert['order_id'] = $out_trade_no;
                $insert['pay_time'] = time();
                $this->db->insert('finance',$insert);
                if($this->db->insert('finance',$insert)){
                             $this->load->helper('url');
                    redirect('/Order/serverStart?oid='.$out_trade_no);
                }
            }
        }
    }
    //开始服务
       function serverStart(){
        $param = $this->input->get();
        $oid = $param['oid'];
        $order_status = (!empty($param['order_status']))?$param['order_status']:'';
        $order_id = $order_status=='6'?'order_id':'order_sn';
        $orderInfoRs = $this->db->select('uid,order_id,tid,order_sn,order_status,address,goods_name,add_time,date_type,date_time,goods_id')
        ->from('order_list')->where([$order_id=>$oid])->get();
        $orderInfo = $orderInfoRs->result_array();
        if($order_status && $order_status == 6){
            $view['order_status'] = $order_status;
            $orderInfo[0]['order_status'] = $order_status;
            $this->db->where(['order_id'=>$oid])->update('order_list',array('order_status'=>$order_status));
        }
        foreach($orderInfo as $k=>$v){
            if($v['tid']){
                $tRs = $this->db->select('nickname,mobile')->from('techer')->where(['tid'=>$v['tid']])->get();
                $tInfo = $tRs->row_array();
                $orderInfo[$k]['tnickname'] = $tInfo['nickname'];
                $orderInfo[$k]['mobile'] = $tInfo['mobile'];
            }
            if($v['goods_id']){
                $tRs = $this->db->select('server_time')->from('goods')->where(['goods_id'=>$v['goods_id']])->get();
                $tInfo = $tRs->row_array();
                $orderInfo[$k]['server_time'] = $tInfo['server_time'];
            }
            $estimateDate = date("Y-m-d");
            if(!$v['date_type']){
                $estimateTime = date('H:i',$v['add_time']+30*60);
            }elseif($v['date_type'] == 1){
                $estimateTime = date('H:i',$v['date_time']);
            }elseif($v['date_type'] == 2){
                $estimateTime = date('H:i',$v['date_time']);
                $estimateDate = date("Y-m-d",strtotime("+1 day"));
            }
            $orderInfo[$k]['estimateTime'] = $estimateTime;
            $orderInfo[$k]['estimateDate'] = $estimateDate;
            $orderInfo[$k]['kfmobile'] = $this->config->item('kfmoblie');
        }
        $view['orderInfo'] = $orderInfo[0];
        $this->load->view('/order/server_start',$view);
    }
    //结束服务
    function orderStop(){
        //         $order_id = $this->input->post('order_sn');
        $order_id = $this->input->post('oid');
//         check_sign($param);
//         $param=decodeParam($param);
//         $order_id = $param['order_'];
        if(empty($order_id)){
            ajax_return(-1,'缺少重要参数');
        }
        $orderInfoRs = $this->db->where(['order_id'=>$order_id])->select('order_sn,order_status,tid,goods_name,goods_totalprice')->from('order_list')->get();
        $orderInfo = $orderInfoRs->row_array();
        $this->db->where(['order_id'=>$order_id])->update('xjm_order_list',array('order_status'=>2));
        if($this->db->affected_rows()){
            $tInfoRs = $this->db->select('money')->from('techer')->where(['tid'=>$orderInfo['tid']])->get();
            $tMoney = $tInfoRs->row_array();
            $totalMoney = $tMoney['money']+$orderInfo['goods_totalprice'];
            $this->db->where(['tid'=>$orderInfo['tid']])->update('techer',array('money'=>$totalMoney));
            $insert['tid'] = $orderInfo['tid'];
            $insert['order_id'] = $orderInfo['order_sn'];
            $insert['goods_name'] = $orderInfo['goods_name'];
            $insert['change_money'] = $orderInfo['goods_totalprice'];
            $insert['money'] = $totalMoney;
            $insert['from'] = '收入';
            $insert['add_time'] = time();
            $this->db->insert('techer_income',$insert);
            $this->db->where(['tid'=>$orderInfo['tid']])->update('techer',array('is_lock'=>0));
            ajax_return(1,'处理数据成功');
        }else{
            ajax_return(-1,'处理数据失败');
        }
    }
    
    
    
    
    function checkOrder(){
        $order_sn = $this->input->post('order_id');
        $order_amount = $this->input->post('order_amount');
        if($order_sn){
            $orderInfo = $this->db->select('order_amount,goods_name')->from('order_list')->where('order_sn',$order_sn)->get()->row_array();
        }
        if(!$orderInfo ||  !($order_amount == $orderInfo['order_amount'])){
            ajax_return(-1,'订单信息有误');
        }else{
            ajax_return(1,'订单信息正确');
        }
    } 
    function  refundstep(){
        if(!$this->input->post('formhash')){
             $param = $this->input->get();
             $order_sn = $param['oid'];
             $refundDesc = (!empty($param['desc']))?$param['desc']:'';
             $refundMoney = (!empty($param['money']))?$param['money']:'';
             if(empty($order_sn)){
                 ajax_return(0,'缺少重要参数');
             }
             $order_rs = $this->db->select('a.order_id,a.goods_name,a.goods_price,a.order_amount,a.goods_id,b.server_time')
                         ->from('order_list a')->join('goods b','a.goods_id=b.goods_id')->where(['order_id'=>$order_sn])->get();
             $orderInfo = $order_rs->row_array();
            $refundDesc && $view['refundDesc'] = $refundDesc;
            $refundMoney && $view['refundMoney'] = $refundMoney;
            $view['orderinfo'] = $orderInfo;
        }else{
            $param = $this->input->post();
             if(empty($param['oid']) || empty($param['refundMoney']) || empty($param['refundDesc'])){
            ajax_return(-1,'缺少重要参数');
        }
        $order_id = $param['oid'];
        $uid = $this->input->cookie('uid');
        $refund_money = $param['refundMoney'];
        $refund_desc =  $param['refundDesc'];
        $data['refund_note'] =  isset($param['refund_note'])?$param['refund_note']:'暂无描述';
        //$data['refund_note'] =  $this->input->post('refund_note');
        $rs = $this->db->select('order_id,order_sn,goods_name,goods_price,tid,goods_id,server_time')->from('order_list')->where(['order_id'=>$order_id])->get();
        $orderInfo = $rs->row_array();
        $data['refund_money'] =  $refund_money;
        $data['goods_name'] =  $orderInfo['goods_name'];
        $data['order_id'] =  $orderInfo['order_id'];
        $data['order_sn'] =  $orderInfo['order_sn'];
        $data['tid'] =  $orderInfo['tid'];
        $data['uid'] =  $uid;
        $data['goods_id'] =  $orderInfo['goods_id'];
        $data['goods_price'] =  $orderInfo['goods_price'];
        $data['server_time'] =  $orderInfo['server_time'];
        $data['refund_reason'] = $refund_desc;
        $data['add_time'] = time();
        $update['order_status'] = 7;
          $this->db->where(['order_sn'=>$order_id])->update('order_list',$update);
        if(!$this->db->from('refund')->where(['uid'=>$uid,'order_id'=>$orderInfo['order_id']])->count_all_results()){
            $data['status'] =  1;
            if($this->db->insert('refund',$data)){
              
                ajax_return(1,'提交成功','/index.php/Order/refundstep2?oid='.$order_id.'&status=1');
            }else{
                ajax_return(-1,'提交失败');
            }
        }else{
            $data['status'] = 1;
            //ajax_return(-1,'不能重复提交');
            $this->db->where(['order_id'=>$order_id])->update('refund',$data);
            if($this->db->affected_rows()){
                ajax_return(1,'修改成功','/index.php/Order/refundstep2?oid='.$order_id.'&status=1');
            }else{
                ajax_return(-1,'修改失败');
            }
        }
            
        }
            $view['oid'] = $order_sn;
            $this->load->view('order/refund_step',$view);
    }
    function  refundstep1(){
         $order_sn = $this->input->get('oid');
         if(empty($order_sn)){
             ajax_return(0,'缺少重要参数');
         }
         $order_rs = $this->db->select('a.order_id,a.goods_name,a.goods_price,a.order_amount,a.goods_id,b.server_time,a.date_type,a.add_time,a.date_time')
                     ->from('order_list a')->join('goods b','a.goods_id=b.goods_id')->where(['order_id'=>$order_sn])->get();
         $orderInfo = $order_rs->row_array();
         
          $serverTime = $orderInfo['date_type']?$orderInfo['date_time']:$orderInfo['add_time'];
        $startedTime = $this->config->item('startedTime');
        $refundProportion = $this->config->item('refundProportion');
        $proportion = $orderInfo['add_time']+$startedTime*60>time()?100:$refundProportion;
        $refundMoney = $orderInfo['order_amount'] * $proportion/100;
        $format_num = sprintf("%.2f",$refundMoney);
        $confRefund = $this->db->from('refund_desc')->get()->result_array();
        foreach($confRefund as $k=>$v){
            if($v['all_payment']){
                $confRefund[$k]['refund_money'] = $format_num;
            }else{
                $confRefund[$k]['refund_money'] = $orderInfo['order_amount'];
            }
        }
        $view['oid'] =  $order_sn;
        $view['confRefund'] = $confRefund;
        $this->load->view('order/refund_step1',$view);
        
    }
    function refundstep2(){
        $order_id = $this->input->get('oid');
        $rs = $this->db->select('goods_name,goods_price,status,refund_money,server_time,add_time,from_unixtime(refund_time,"%Y-%m-%d %H:%i:%s") refund_time,refund_reason')->from('refund')->where(array('order_id'=>$order_id))->get();
        $refundArr = $rs->row_array();
        if($refundArr){
            $time = $refundArr['add_time']+172800;
            $time = $time-time();
            if($time>0){
                $time = $this->Sec2Time($time);
            }else{
                $time = '0天0小时0分';
            }
            $refundArr['stime'] = $time;
            $refundArr['add_time'] = date('Y-m-d H:i', $refundArr['add_time']);
            // ajax_return(0,'获取数据成功',$refundArr);
            // $status = $this->input->get('status');
            $view['refundArr'] = $refundArr;
            $view['oid'] = $order_id;
            $this->load->view('order/refund_step2',$view);
        }
    }
    
        function Sec2Time($time){
        if(is_numeric($time)){
        $value = array(
          "years" => 0, "days" => 0, "hours" => 0,
          "minutes" => 0, "seconds" => 0,
        );
        if($time >= 86400){
          $value["days"] = floor($time/86400);
          $time = ($time%86400);
        }
        if($time >= 3600){
          $value["hours"] = floor($time/3600);
          $time = ($time%3600);
        }
        if($time >= 60){
          $value["minutes"] = floor($time/60);
          $time = ($time%60);
        }
        $value["seconds"] = floor($time);
        $t=$value["days"] ."天"." ". $value["hours"] ."小时". $value["minutes"] ."分";
        Return $t;
         }else{
         return (bool) FALSE;
        }
     }
     //取消订单
         function ordercCancel(){
        $order_id = $this->input->post('oid');
        if(empty($order_id)){
            ajax_return(0,'缺少重要参数');
        }
        $data['order_status'] = 3;
        $this->db->where(['order_id'=>$order_id])->update('order_list',$data);
        if($this->db->affected_rows()){
            ajax_return(1,'订单取消成功','/index.php/Home/orderList');
        }else{
            ajax_return(-1,'订单取消失败');
        }
    }
    
        //删除订单
    function orderDel(){
        $order_id = $this->input->post('oid');
        if($order_id){
            if(empty($order_id)){
                ajax_return(0,'缺少重要参数');
            }
            $this->db->where(['order_id'=>$order_id])->delete('order_list');
            if($this->db->affected_rows()){
                ajax_return(1,'订单删除成功');
            }else{
                ajax_return(-1,'订单删除失败');
            }
        }
    }
    //再来一单
    function payAgain(){
        $param = $this->input->get();
//         check_sign($param);
//         $param = decodeParam($param);
        if(empty($param['goods_id'])){
            ajax_return(-1,'缺少重要参数');
        }
        $tid = (!empty($param['tid']))?$param['tid']:'';
        $tInfors = $this->db->select('is_online')->from('techer')->where(['tid'=>$tid])->get();
        $tInfo =$tInfors->row_array();
        if($tInfo){
            if($tInfo['is_online']){
                ajax_return(-1,'技师不在线');
            }
        }else{
            ajax_return(-1,'不存在的技师');
        }
        $rs = $this->db->select('goods_id,goods_type,goods_price,goods_name,server_time')->from('goods')->where(array('goods_id'=>$param['goods_id']))->get();
        $goodsInfo = $rs->row_array();
        if($goodsInfo){
            ajax_return(0,'获取数据成功',$goodsInfo);
        }else{
            ajax_return(1,'商品已下架');
        }
    }
    
    function lat(){
        $address = $this->input->post('address');
        $housenum = $this->input->post('housernun');
        $url = "http://apis.map.qq.com/ws/geocoder/v1/?address=$address&key=CUHBZ-XZFKX-RB74L-7OGGD-NHPZH-B6FFW";
        $res = json_decode(file_get_contents($url),true);
        $location = $res['result']['location'];
        $orderInfo['lng'] = $location['lng'];
        $orderInfo['lat'] = $location['lat'];
        $orderInfo['address'] = $address;
        $this->input->set_cookie('orderInfo',json_encode($orderInfo),1200);
        $view['address'] = $address;
        $view['house_num'] = $housenum;
        ajax_return(1,'获取经纬度成功',$view);
        
    }
//     function get
    
    
}