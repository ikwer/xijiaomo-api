<?php

/**
 * Description of m_user
 *
 * @author acer
 */
class User_model_v1 extends CI_Model {

    //true成功  false 失败
    private $results = array(
        'status' => false,
        'msg' => '',
        'data' => array()
    );

    /**
     * 帐号密码 登录
     * @param type $mobile
     * @param type $pwd
     * @return type
     */
    function eheck_login($mobile, $pwd) {
        $where['mobile'] = $mobile;
        $where['pwd'] = $pwd;
        $check = $this->db->select('uid,nickname,mobile,money,add_time')->from('user')->where($where)->get();
        if ($check->row_array()) {
            $this->results['status'] = TRUE;
            $this->results['msg'] = '登录成功';
            $this->results['data'] = $check->row_array();
        } else {
            $this->results['msg'] = '用户不存在';
        }
        return $this->results;
    }

    /**
     * 第三方登录
     * @param type $type
     * @param type $token
     * @return type
     */
//    function check_exterior_login($type, $log_token) {
//        $this->db->select('uid,nickname,mobile,money,add_time')->from('user');
//        if ($type == 1) {
//            $this->db->where('qq_token', $log_token);
//        } else if ($type == 2) {
//            $this->db->where('weixin_token', $log_token);
//        } else if ($type == 3) {
//            $this->db->where('weibo_token', $log_token);
//        }
//        $this->db->where('status', 0);
//        $check = $this->db->get();
//        $check_rs = $check->row_array();
//        if ($check_rs) {
//            $this->results['status'] = TRUE;
//            $this->results['data'] = $check_rs;
//        } else {
//            $this->results['msg'] = '尚未注册,清先注册';
//        }
//        return $this->results;
//    }

    /**
     * 添加用户信息 注册
     * @param type $mobile
     * @param type $pwd
     * @return type
     */
    function inset_user($mobile) {
         $this->load->library('session');
        //检测手机号 (用户是否存在) 不存在注册
        $check = $this->db->select('uid,nickname,mobile,money,add_time,headurl')->from('user')->where('mobile', $mobile)->get();
        $check_rs = $check->row_array();
//         $_SESSION['uid'] = $check_rs['uid'];
//         $_SESSION['nickname'] = $check_rs['nickname'];
//         $_SESSION['headurl'] = $check_rs['headurl'];
        $this->input->set_cookie('uid',$check_rs['uid'],7*24*3600);
        $this->input->set_cookie('nickname', $check_rs['nickname'],7*24*3600);
        $this->input->set_cookie('headurl', $check_rs['headurl'],7*24*3600);
        if ($check_rs) {
            $this->results['status'] = TRUE;
            $this->results['msg'] = '登录成功';
            $this->results['data'] = $check_rs;
            return $this->results;
        }
        $time = time();
        $inset['mobile'] = $mobile;
        $inset['add_time'] = $time;
        $rs = $this->db->insert('user', $inset);
        if ($rs) {
            $data['uid'] = $this->db->insert_id();
            $data['nickname'] = '';
            $data['mobile'] = $mobile;
            $data['money'] = number_format(0, 2);
            $data['add_time'] = "$time";
            $data['headurl'] =  $this->config->item('headurl');
            $this->results['status'] = TRUE;
            $this->results['msg'] = '注册成功';
            $this->results['data'] = $data;
            $this->input->set_cookie('uid',$data['uid'],7*24*3600);
            $this->input->set_cookie('nickname', $mobile,7*24*3600);
            $this->input->set_cookie('headurl', $data['headurl'],7*24*3600);
            
        } else {
            $this->results['msg'] = '注册失败,请稍后再试';
        }
        return $this->results;
    }

    /**
     * 修改密码
     * @param type $mobile
     * @param type $pwd
     * @return type
     */
//    public function change_pwd($mobile, $pwd) {
//        $where['mobile'] = $mobile;
//        $check = $this->db->select('uid')->from('user')->where($where)->get();
//        $check_rs = $check->row_array();
//        if (!$check_rs) {
//            $this->results['msg'] = '尚未注册';
//            return $this->results;
//        }
//        $save['pwd'] = $pwd;
//        $this->db->where('uid', $check_rs['uid']);
//        $rs = $this->db->update('user', $save);
//        if ($rs) {
//            $this->results['status'] = TRUE;
//            $this->results['msg'] = '密码修改成功';
//        } else {
//            $this->results['msg'] = '密码修改失败';
//        }
//        return $this->results;
//    }

    /**
     * 用户信息
     * @param type $uid
     * @param type $mobile
     * @return type
     */
    public function user_info($uid, $mobile) {
        $where['uid'] = $uid;
        $where['mobile'] = $mobile;
        $check = $this->db->select('uid')->from('user')->where($where)->get();
        $check_rs = $check->row_array();
        if ($check_rs) {
            $this->results['status'] = TRUE;
            $this->results['msg'] = '用户存在';
            $this->results['data'] = $check_rs;
        } else {
            $this->results['msg'] = '不存在用户';
        }
        return $this->results;
    }

    /**
     * 手机号码使用情况
     * @param type $mobile
     * @return type
     */
    public function check_mobile($mobile) {
        $where['mobile'] = $mobile;
        $check = $this->db->select('uid')->from('user')->where($where)->get();
        $check_rs = $check->row_array();
        if ($check_rs) {
            $this->results['status'] = TRUE;
            $this->results['msg'] = '手机号码已被使用';
        } else {
            $this->results['msg'] = '手机号码尚未使用';
        }
        return $this->results;
    }

    /**
     * 更换手机号码
     * @param type $uid
     * @param type $mobile
     * @param type $new_mobile
     * @return type
     */
    public function change_mobile($uid, $mobile, $new_mobile) {
        $where['uid'] = $uid;
        $where['mobile'] = $mobile;
        $save['mobile'] = $new_mobile;
        $this->db->where($where)->update('user', $save);
        $save_rs = $this->db->affected_rows();
        if ($save_rs) {
            $this->results['status'] = TRUE;
            $this->results['msg'] = '号码更换成功';
        } else {
            $this->results['msg'] = '号码更换失败';
        }
        return $this->results;
    }

}
