<!doctype html>
<html class="no-js" lang="zxx">

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
		<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
		<meta name="applicable-device" content="mobile">
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>set.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>appdownload.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>iconfont.css" />
		<meta name="description" content="">
		<title>喜脚么_APP下载</title>

	</head>

	<body background="<?php echo IMG_PATH ?>beijing@2x.png" style="background-size: 100%;">
		<div style="height: 25px;"></div>
		<div id="hcontent" class="">
			<h1>
				<p><img src="<?php echo IMG_PATH ?>logo@2x.png" width="50%"/></p>
				<p style="height: 20px;"></p>
				<p><img src="<?php echo IMG_PATH ?>wenan@2x.png" width="70%"/></p>
			</h1>
			<div id="button">

				<p>
					<a><i class="iconfont icon-pingguo"></i>IOS下载</a>
				</p>
			</div>
			<div class="my_pro" style="padding-top: 20px;">
				<img src="<?php echo IMG_PATH ?>pro@2x.png" width="40%" />
			</div>
		</div>

		<!-- jquery -->
		<script src="<?php echo JS_PATH?>jquery.min.js"></script>
		<script src="<?php echo JS_PATH?>prefixfree.min.js"></script>
		<script type="">
			$(document).ready(function() {
                $(".my_pro img").animate({
					left: '1px',
					opacity: '1',
					width: '93%'
				});

			});
		</script>
	</body>

</html>