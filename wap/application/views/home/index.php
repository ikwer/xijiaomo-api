<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
		<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
		<meta name="applicable-device" content="mobile">
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>new_file.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>set.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>index.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>medie.css" />
		<link type="text/css" href="<?php echo CSS_PATH?>style.css" rel="stylesheet" />
		<title>喜脚么-首页</title>
		<style type="text/css">
			
			.addWrap {
				position: relative;
				width: 100%;
				background: #fff;
				margin: 0;
				padding: 0;
			}
			
			.addWrap .swipe {
				overflow: hidden;
				visibility: hidden;
				position: relative;
			}
			
			.addWrap .swipe-wrap {
				overflow: hidden;
				position: relative;
			}
			
			.addWrap .swipe-wrap>div {
				float: left;
				width: 100%;
				position: relative;
			}
			
			#position {
				position: absolute;
				bottom: 0;
				right: 0;
				padding-right: 8px;
				margin: 0;
				background: #000;
				opacity: 0.4;
				width: 100%;
				filter: alpha(opacity=50);
				text-align: right;
			}
			
			#position li {
				width: 10px;
				height: 10px;
				margin: 0 2px;
				display: inline-block;
				-webkit-border-radius: 5px;
				border-radius: 5px;
				background-color: #AFAFAF;
			}
			
			#position li.cur {
				background-color: #FF0000;
			}
			
			.img-responsive {
				display: block;
				max-width: 100%;
				height: auto;
			}
		</style>
	</head>

	<body>
		<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
			<div class="m_header_bar J_header-bar">
				<div class="mhb_left">
					<a class="myposition" href="/index.php/Member/getCity" ><img class="icon icon-position3" src="<?php echo IMG_PATH ?>home/iconmap@2x.png"></img>
					</a>
					<a class="position" href="/index.php/Member/getCity">武汉</a>
				</div>
				<div class="mhb_center mhb_center_across">
					<h2 class="title">喜脚么</h2>
				</div>
				<div class="mhb_right">
					<a class="share" href="<?php if($this->input->cookie('uid')){?>/index.php/Member/msg<?php }else{?>/index.php/Home/login?f=1<?php }?>" m="">
						<img class="icon icon-share" src="<?php echo IMG_PATH?>home/iconmessgas@2x.png"></img>
					</a>
				</div>

			</div>

		</header>
		<div class="addWrap banner">
			<div class="swipe" id="mySwipe">
				<div class="swipe-wrap">
                                    <?php foreach($data as $k=>$v){?>
					<div>
						<a href="javascript:;"><img class="img-responsive" src="<?php echo $v['img_url']?>" /></a>
					</div>
					<?php } ?>
					
					<!--<div>
						<a href="javascript:;"><img class="img-responsive" src="<?php echo IMG_PATH?>home/banner0@2x.png" /></a>
					</div>-->
				</div>
			</div>

			<ul id="position">
				<!--<li class="cur"></li>
				<li></li>
				<li></li>-->
			</ul>
		</div>
		<section class="index">

			<!--全部项目-->
			<div class="all clearfix">
				<ul class="clearfix">
					<li><a href="/index.php/Home/goodsList?id=1"><img src="<?php echo IMG_PATH?>home/1@2x.png" />
                                                <p>足&nbsp;&nbsp;疗</p></a>
					</li>
					<li><a href="/index.php/Home/goodsList?id=2"><img src="<?php echo IMG_PATH?>home/2@2x.png" />
                                                <p>刮&nbsp;&nbsp;痧</p></a>
					</li>
					<li>
						<a href="/index.php/Home/goodsList?id=3"><img src="<?php echo IMG_PATH?>home/3@2x.png" />
                                                    <p>按&nbsp;&nbsp;摩</p></a>
						</a>
					</li>
					<li><a href="/index.php/Home/goodsList?id=4"><img src="<?php echo IMG_PATH?>home/4@2x.png" />
						<p>洗&nbsp;&nbsp;面</p>
					</li>
					<li><a href="/index.php/Home/goodsList?id=5"><img src="<?php echo IMG_PATH?>home/5@2x.png" />
                                                <p>拔&nbsp;&nbsp;罐</p></a>
					</li>
					<li><a href="/index.php/Home/goodsList?id=6"><img src="<?php echo IMG_PATH?>home/6@2x.png" />
                                                <p>修&nbsp;&nbsp;脚</p></a>
					</li>
					<li><a href="/index.php/Home/goodsList?id=7"><img src="<?php echo IMG_PATH?>home/7@2x.png" />
                                                <p>采&nbsp;&nbsp;耳</p></a>
					</li>
					<li><a href="/index.php/Home/goodsList?id=8"><img src="<?php echo IMG_PATH?>home/8@2x.png" />
                                                <p>艾&nbsp;&nbsp;灸</p></a>
					</li>
					<li><a href="/index.php/Home/goodsList?id=9"><img src="<?php echo IMG_PATH?>home/9@2x.png" />
                                                <p>推&nbsp;&nbsp;拿</p></a>
					</li>
				</ul>

			</div>
			<div class="fooder">
				<ul class="index-bnav">
					<li>
						<a class="select"><img src="<?php echo IMG_PATH?>home/iconhome_lab@2x.png" class="icon-index" />
							<p>首页</p>
						</a>
					</li>
					<li>
						<a href="<?php if($this->input->cookie('uid')){?>/index.php/Home/orderList<?php }else{?>/index.php/Home/login?f=1<?php }?>"><img src="<?php echo IMG_PATH?>home/icondingdan@2x.png" class="icon-ding" />
							<p>订单</p>
						</a>
					</li>
					<li>
						<a href="<?php if($this->input->cookie('uid')){?>/index.php/Home/mymain<?php }else{?>/index.php/Home/login?f=1<?php }?>"><img src="<?php echo IMG_PATH?>home/iconmy@2x.png" class="icon-my" />
							<p>我的</p>
						</a>
					</li>
				</ul>
			</div>
		</section>
		<script type="text/javascript" src="<?php echo JS_PATH ?>jquery.min.js"></script>
		<script src='<?php echo JS_PATH ?>hhSwipe.js' type="text/javascript"></script>
		<script type="text/javascript">
//			var bullets = document.getElementById('position').getElementsByTagName('li');

			var banner = Swipe(document.getElementById('mySwipe'), {
				auto: 4000,
				continuous: true,
				disableScroll: false,
				callback: function(pos) {
//					var i = bullets.length;
//					while(i--) {
//						bullets[i].className = ' ';
//					}
//					bullets[pos].className = 'cur';
				}
			})
		</script>
	</body>

</html>