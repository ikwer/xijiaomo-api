<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
		<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
		<meta name="applicable-device" content="mobile">
		<meta name="description" content="">
		<title>喜脚么招商加盟</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="<?php echo CSS_PATH?>join_style.css" />
		<!-- Link Swiper's CSS -->
		<link rel="stylesheet" href="<?php echo JS_PATH?>dist/css/swiper.min.css" />

		<!-- Demo styles -->
		<style>
			html,
			body {
				position: relative;
				height: 100%;
			}
			
			body {
				background: #eee;
				font-family: "microsoft yahei", Helvetica, Arial, sans-serif;
				font-size: 14px;
				color: #000;
				margin: 0;
				padding: 0;
			}
			
			.swiper-container {
				width: 100%;
				height: 100%;
				margin-left: auto;
				margin-right: auto;
			}
			
			.swiper-slide {
				text-align: center;
				font-size: 18px;
				background: #000;
				/* Center slide text vertically */
				/*display: -webkit-box;
				display: -ms-flexbox;
				display: -webkit-flex;
				display: flex;*/
				-webkit-box-pack: center;
				-ms-flex-pack: center;
				-webkit-justify-content: center;
				justify-content: center;
				-webkit-box-align: center;
				-ms-flex-align: center;
				-webkit-align-items: center;
				align-items: center;
			}
		</style>
	</head>

	<body>
		<!-- Swiper -->
		<div class="swiper-container">
			<a href="tel:18627099297" class="call_us">联系我们</a>
			<div class="swiper-wrapper">
				<div class="swiper-slide slide">

					<div class="one_img" style="padding-top: 8%;"><img src="<?php echo IMG_PATH ?>joinlogo@2x.png" width="30%" /></div>
					<div class="tow_img" style="padding-top: 6%;"><img src="<?php echo IMG_PATH ?>bigtitle@2x.png" width="65%" /></div>
					<p style="margin-top: 5%;">招商热线：18627099297</p>
					<p>[ 喜脚么招商加盟火爆进行中 ]</p>
					<p class="lable">
						<img src="<?php echo IMG_PATH ?>xialaicon@2x.png" />
					</p>
				</div>
				<div class="swiper-slide slide2">
					<img src="<?php echo IMG_PATH ?>dongshi@2x.png" width="85%" style="margin-top: 15%;" />
					<p class="lable2">
						<img src="<?php echo IMG_PATH ?>xialaicon@2x.png" />
					</p>
				</div>
				<div class="swiper-slide slide3" style="text-align: center;">
					<div style="padding-top: 30%;"><img src="<?php echo IMG_PATH ?>about@2x.png" width="45%" /></div>
					<p style="width: 82%;display: inline-block;padding-top: 10%;font-size: 14px;"> 我们为传统足疗门店、 技师人员提供信息化系统入口，技师人员利用碎片化时间接受用户订单， 提高工作效率，更好服务消费者需求。充分利用互联网平台优势提高服务者与消费者需求信息对称，帮助足疗服务行业发展。 </p>
					<p class="lable3">
						<img src="<?php echo IMG_PATH ?>xialaicon@2x.png" />
					</p>
				</div>
				<div class="swiper-slide slide4">
					<div style="padding-top: 15%;"><img src="<?php echo IMG_PATH ?>wepro@2x.png" width="45%" /></div>
					<h2 style="">技术支持 </h2>
					<p>完善产品APP网站后台系统，全面维护稳定</p>
					<h2>运营支持</h2>
					<p>线上微信公众号活动、品牌宣传。</p>
					<h2>培训支持</h2>
					<p>覆盖各地区线下城市、技师人员统一培训、考核</p>
					<h2>产品支持</h2>
					<p>线上线下多渠道推广产品</p>
					<p class="lable4">
						<img src="<?php echo IMG_PATH ?>xialaicon@2x.png" />
					</p>
				</div>
			</div>
			<!-- Add Pagination -->
			<!--<div class="swiper-pagination"></div>-->
		</div>

		<!-- Swiper JS -->
		<script type="text/javascript" src="<?php echo JS_PATH?>jquery.min.js"></script>
		<script src="<?php echo JS_PATH?>swiper-3.4.2.min.js"></script>

		<!-- Initialize Swiper -->
		<script>
			var swiper = new Swiper('.swiper-container', {
				pagination: '.swiper-pagination',
				direction: 'vertical',
//				slidesPerView: 1,
				paginationClickable: true,
				spaceBetween: 5,
				mousewheelControl: true,
				loop: true,
				slidesPerView: 'auto',
				loopedSlides: 8,
			});
		</script>
	</body>

</html>