<!DOCTYPE html>
<html style="background-color: #fff;">

	<head>

		<head>
			<meta charset="utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
			<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
			<meta name="applicable-device" content="mobile">
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>new_file.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>set.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>medie.css" />
					<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>layer.css" />
			<script type="text/javascript" src="<?php echo JS_PATH ?>jquery.min.js "></script>
			 <script type="text/javascript" src="<?php echo JS_PATH ?>layer.js "></script>
			<title>登录</title>
			<style>
				.login {
					padding: 0px 32px;
				}
				
				.login li {
					height: 60px;
					line-height: 60px;
					border-bottom: 1px solid #E6E6E6;
				}
				
				.login li input {
					border: none;
					padding-left: 10px;
				}
				
				.number {
					background: url(<?php echo IMG_PATH ?>/login/iconyonghu@2x.png) no-repeat;
					height: 23px;
					width: 20px;
					background-size: 100%;
					display: inline-block;
					margin-top: 18px;
				}
				
				.mima {
					background: url(<?php echo IMG_PATH ?>/login/iconmima@2x.png) no-repeat;
					height: 23px;
					width: 20px;
					background-size: 100%;
					display: inline-block;
					margin-top: 18px;
				}
				
				.button p a{
					color: #ccc;
				}
				
					.get_yanzheng {
					border: 1px solid #CC5522;
					background: #fbe8e8;
					display: inline-block;
					padding: 0 12px;
					border-radius: 32px;
					line-height: 30px;
					height: 30px;
					float: right;
					color: #D5201E;
					margin-top: 14px;
				}
					
				.button label {
					background: url(<?php echo IMG_PATH ?>/login/icontongyi@2x.png) no-repeat;
					height: 14px;
					width: 16px;
					background-size: 80%;
					display: inline-block;
					padding-right: 5px;
				}
			</style>
		</head>

		<body>
			<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
				<div class="m_header_bar J_header-bar">
					<?php include_once APPPATH . 'views/home/comback.php';?>
					<div class="mhb_center mhb_center_across">
						<h2 class="title">登录</h2>
					</div>

				</div>

			</header>
			<form action="/index.php/Home/login" method="post" name="logonForm" id="logonForm">
			<input type="hidden" name="formhash" id="formhash" value="1"/>
			<input type="hidden" name="url" id="url" value="<?php echo $url?>"/>
    			<section class="main">
    				<div style="height: 8px; background-color: #f3f3f3;"></div>
    				<ul class="login">
    					<li>
    						<label class="number fl"></label>
    						<input type="text" name="account" style="width:40%" id="account" placeholder="请输入电话号码" class="nofastclick">
    						<a class="get_yanzheng" id="phone_code" onclick="sendMessageFn()">获取验证码</a>
    					</li>
    					<li>
    						<label class="mima fl"></label>
    						<input type="text"  name="code" id="code" placeholder="请输入短信验证码" class="nofastclick">
    					</li>
    				</ul>
    				<div class="button" style="padding:0 32px" onclick="butForm()">
    					<p style="height: 44px;line-height:44px; border-radius: 44px;border: 0px;background-color: #D5201E;color: #fff;font-size: 15px;margin: 10px 0 15px 0;">登录</p>
    					<p><label></label><a href="user_agreement.html">注册即表示同意并遵守《喜脚么使用协议》</a></p>
    				</div>
    
    			</section>
			</form>
		</body>
<script>
    function butForm(){
    	var pattern = /^1[34578]\d{9}$/; 
    	if($("#account").val()=='' ||  !pattern.test($("#account").val())){
    		  	layer.open({
    			    content: '请填写正确的手机号'
    			    ,skin: 'msg'
    			    ,time: 2 //2秒后自动关闭
    			  });
    			return false; 
    		}
    	if($("#code").val()==''){
    	  layer.open({
    		    content: '请填写验证码'
    		    ,skin: 'msg'
    		    ,time: 2 //2秒后自动关闭
    		  });
    		return false; 
    	}
    	$.post('/index.php/Home/login',{account:$("#account").val(),code:$("#code").val(),url:$("#url").val()},function(data){
    		if(data.code== -1){
    			 layer.open({
    				    content: data.massage
    				    ,skin: 'msg'
    				    ,time: 2 //2秒后自动关闭
    				  });
    				return false; 
    			}else if(data.code == 1){
       				 layer.open({
      				    content: data.massage
      				    ,skin: 'msg'
      				    ,time: 2 //2秒后自动关闭
      				  });
    				  window.location.href= data.data;
        		}
			
    		},'json');
    }
    var u=false;//用户名
    var InterValObj; //timer变量，控制时间
    var count = 60; //间隔函数，1秒执行
    var curCount;//当前剩余秒数
    function sendMessage(){
    	if(u){
    	  	curCount = count;
    	   	$("#phone_code").replaceWith('<a id="phone_code" class="get_yanzheng" >'+curCount+'s后重新获取</a>');
    	   	InterValObj = window.setInterval(SetRemainTime,1000);
    		var account =  $("#account").val();
    		$.post('/index.php/Home/send_code', {account:account}, function(data){
    			if(data.code == 0){
    				c=true;
    			}
    		},'json');
    	}
    }
    function SetRemainTime() {
        if (curCount == 0) {                
            window.clearInterval(InterValObj);//停止计时器
            $("#phone_code").replaceWith('<a class="get_yanzheng" id="phone_code" onclick="sendMessageFn()">重新获取验证码</a>');
        }
        else {
            curCount--;
            $("#phone_code").replaceWith('<a id="phone_code" class="get_yanzheng" >'+curCount+'s后重新获取</a>');
        }
    }
    function sendMessageFn(){
      var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1})|(17[0-9]{1}))+\d{8})$/;
      if(!myreg.test($('#account').val())){
    		alert('请输入正确的手机号码');
    	  return false;
      }else{
    		u=true;
//     		$.post("/index.php/Home/check_user",{account:$('#account').val(),code:$("#code").val()},function(data){
//         			if(data.code==0){
        				u=true;
        				sendMessage();
//         			}else{
//         				u=false;
//         				if(data.code=='1'){
//         					 layer.open({
//         					    content: '该手机号已经注册'
//         					    ,skin: 'msg'
//         					    ,time: 2 //2秒后自动关闭
//         					 });
//         				}
//         			}
//     			},'json');
    		}
    }
	
	
</script>
</html>