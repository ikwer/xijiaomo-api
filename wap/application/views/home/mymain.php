<!DOCTYPE html>
<html style="background-color: #fff;">
	<head>
		<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
		<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
		<meta name="applicable-device" content="mobile">
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>new_file.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>set.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>my.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>medie.css" />
		<title>喜脚么-个人中心</title>
	
	</head>
	
	<body>
		<header class="m_header sticky_header" id="J_header" style="position: sticky; top: 0px; bottom: 0px;border:none">
			<div class="m_header_bar J_header-bar">
				<div class="mhb_left">
					<a class="myposition" href="/index.php/Member/setUp"><img class="icon icon-position3" src="<?php echo IMG_PATH?>my/iconsetting@2x.png"></img>
					</a>
				</div>
			
				<div class="mhb_center mhb_center_across">
					<h2 class="title">个人中心</h2>
				</div>
				<div class="mhb_right">
					<a class="share" href="/index.php/Member/msg" m=""><img class="icon icon-share" src="<?php echo IMG_PATH?>my/iconmess@2x.png"></img>
					</a>
				</div>

			</div>
            <div class="phto">
            	<p class="touxiang_my"><img style="width:80px;height:80px;border-radius: 50%;" src="<?php if($userInfo['headurl']){echo $userInfo['headurl'];}else{?>/data/img/headurldefault.png<?php }?>"/></p>
            	<p class="name_my"><?php echo $userInfo['nickname']?></p>
            </div>
		</header>
		<section class="my_list clearfix">
			<ul>
				<a href="/index.php/Member/favoriteData"><li>
					
					<img class="list_icon" src="<?php echo IMG_PATH?>my/iconlike@2x.png" />
					<span>我的收藏</span><img class="ivon_go" src="<?php echo IMG_PATH?>my/icongo@2x.png" />
					
				</li></a>
				<a href="/index.php/Member/allCoupon"><li>
					<img class="list_icon" src="<?php echo IMG_PATH?>my/iconzhanghu@2x.png"  style="width: 18px;height: 20px;"/><span>我的账户</span><img class="ivon_go" src="<?php echo IMG_PATH?>my/icongo@2x.png" />
				</li></a>
				<a href="/index.php/Member/point"><li>
					<img class="list_icon" src="<?php echo IMG_PATH?>my/iconjifen@2x.png"  style="width: 20px;height: 20px;"/>
					<span>我的积分</span><img class="ivon_go" src="<?php echo IMG_PATH?>my/icongo@2x.png" />
				</li></a>
			</ul>
			<div style="height: 8px; background-color: #f3f3f3;"></div>
			<ul>
				<a href="/index.php/Member/help"><li>
					<img class="list_icon" src="<?php echo IMG_PATH?>my/iconhelp@2x.png" />
					<span>帮助中心</span><img class="ivon_go" src="<?php echo IMG_PATH?>my/icongo@2x.png" />
					
				</li></a>
				<a href="/index.php/Member/activity"><li>
						<img class="list_icon" src="<?php echo IMG_PATH?>my/iconjiangli@2x.png"  style="width: 18px;height: 20px;"/>
						<span>活动奖励</span><img class="ivon_go" src="<?php echo IMG_PATH?>my/icongo@2x.png" />
				   
				</li> </a>
				<a href="/index.php/Member/user_about?type=1"><li>
					
					<img class="list_icon" src="<?php echo IMG_PATH?>my/iconkefu@2x.png"  style="width: 20px;height: 20px;"/>
					<span>客服中心</span><img class="ivon_go" src="<?php echo IMG_PATH?>my/icongo@2x.png" />
					
				</li></a>
			</ul>
			<p style="height:60px"></p>
			<div class="fooder">
				<ul class="index-bnav">
					<li>
						<a href="/index.php/Home/index" ><img src="<?php echo IMG_PATH?>home/iconhome@2x.png" class="icon-index" />
							<p>首页</p>
						</a>
					</li>
					<li>
						<a href="<?php if($this->input->cookie('uid')){?>/index.php/Home/orderList<?php }else{?>/index.php/Home/login?f=1<?php }?>"><img src="<?php echo IMG_PATH?>home/icondingdan@2x.png" class="icon-ding" />
							<p>订单</p>
						</a>
					</li>
					<li>
						<a class="select" href="<?php if($this->input->cookie('uid')){?>/index.php/Home/mymain<?php }else{?>/index.php/Home/login?f=1<?php }?>"><img src="<?php echo IMG_PATH?>home/iconmy_lab@2x.png" class="icon-my" />
							<p>我的</p>
						</a>
					</li>
				</ul>
			</div>
		</section>
		
		
		
	</body>
	
</html>
