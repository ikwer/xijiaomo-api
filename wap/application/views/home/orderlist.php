<!DOCTYPE html>
<html>

	<head>

		<head>
			<meta charset="utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
			<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
			<meta name="applicable-device" content="mobile">
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>new_file.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>set.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>medie.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>order.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>tankuang.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>layer.css" />
			
			<title>喜脚么-我的订单</title>
		</head>

		<body>
			<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
				<div class="m_header_bar J_header-bar">

					<div class="mhb_center mhb_center_across">
						<h2 class="title">我的订单</h2>
					</div>
					<!--<div class="mhb_right">
						<a class="share" href="" m=""><img class="icon icon-share" src="<?php echo IMG_PATH?>home/iconmessgas@2x.png"></img>
						</a>
					</div>-->

				</div>

			</header>
			<section id="main" class="clearfix">
				<div class="box-163css">
					<ul id="pagenavi" class="page">
						<li>
							<a href="/index.php/Home/orderlist?order_status=1" <?php if($status=='1'){?>class="active"<?php }?>>进行中</a>
						</li>
						<li>
							<a href="/index.php/Home/orderlist?order_status=2" <?php if($status=='2'){?>class="active"<?php }?>>已完成</a>
						</li>
						<li>
							<a href="/index.php/Home/orderlist?order_status=3" <?php if($status=='3'){?>class="active"<?php }?>>已取消</a>
						</li>
					</ul>
					<div id="slider" class="swipe">
						<ul class="box01_list clearfix">
							<li class="li_list">

								<div class="news_list clearfix">
									<ul class="clearfix" id="orderList">
									    <?php foreach($orderlist as $k=>$v){?>
										<li class="clearfix">
											<div class="pic_list"><span>订单号：<?php echo $v['order_sn']; ?></a></div>
												<div class="order_detail">
													<div class="order_up">
														<div class="order_left">
															<img src="<?php echo $v['goods_image']?>"/>
															<h3>深度全身治疗</h3>
															<p>技师：<?php echo $v['tech_name']?></p>
															<p>电话：<?php echo $v['mobile']?></p>
														</div>
														<div class="order_right">
														    <?php if($v['order_status_id']=='1'){?>
															<p>支付成功</p>
															<?php }elseif($v['order_status_id']=='2'){?>
															<p class="order_ding">退款中</p>
															<?php }else if($v['order_status_id']=='3'){?>
															<p>服务进行中</p>
															<?php }else if($v['order_status_id']=='4'){?>
															<p class="order_ding">支付中</p>
															<?php }else if($v['order_status_id']=='5'){?>
															<p>已完成</p>
															<?php }else if($v['order_status_id']=='6'){?>
															<p>退款已完成</p>
															<?php }else if($v['order_status_id']=='7'){?>
															<p>订单已失效</p>
															<?php }?>
															<p style="padding-top: 30px;font-size: 14px;">总计：<?php echo $v['goods_price'] ?>元</p>
														</div>
													</div>
													<?php if($v['order_status_id']=='1'){?>
													<div class="order_down"><a href="/index.php/Order/refundstep?oid=<?php echo $v['order_id']?>">
														<button class="bat">申请退款</button>
													</a></div>
													<?php }elseif($v['order_status_id']=='2'){?>
													<div class="order_down"><button class="bat">退款进度</button></div>
													<?php }else if($v['order_status_id']=='4'){?>
														<div class="order_down">
        												<a href="/index.php/Order/orderPay?oid=<?php echo $v['order_id']?>"><button class="agin">继续付款</button></a>
        												<button class="uxiao " id="btn" onclick="ordercCancel('<?php echo $v['order_id']?>')" >取消订单</button>
											        </div>
													<?php }else if($v['order_status_id']=='5'){?>
														<div class="order_down">
														    <?php if($v['is_comment']){?>
														    <button class="have">已评论</button>
														    <?php }else{?>
														<a href="my_conmment.html"><button class="bat">去评论</button></a>
														<?php }?>
													</div>
														
														<?php }else if($v['order_status_id']=='7'){?>
														<div class="order_down">
														<a href="/index.php/Order/placeOrder?goods_id=<?php echo $v['goods_id']?>&tid=<?php echo $v['tid'];?>&payAgain=1"><button class="agin">再来一单</button></a>
														<button class="uxiao" onclick="orderDel('<?php echo $v['order_id']?>')">删除订单</button>
													</div>
													<?php }?>
												</div>
											</li>
											<?php }?>
							
										</ul>
									</div>
								</li>
					
						
							</ul>
						<div class="footer" style="text-align:center;line-height:50px;"><p>~没有更多了~</p></div>

						</div>
					</div>
				</section>
		<!--脚部样式-->
				<div class="fooder">
				<ul class="index-bnav">
					<li>
						<a href="/index.php/Home/index"><img src="<?php echo IMG_PATH?>home/iconhome@2x.png" class="icon-index" />
							<p>首页</p>
						</a>
					</li>
					<li>
						<a class="select" href="/index.php/Home/orderList?"><img src="<?php echo IMG_PATH?>home/icondingdan_lab@2x.png" class="icon-ding" />
							<p>订单</p>
						</a>
					</li>
					<li>
						<a href="/index.php/Home/mymain"><img src="<?php echo IMG_PATH?>home/iconmy@2x.png" class="icon-my" />
							<p>我的</p>
						</a>
					</li>
				</ul>
			</div>
			<!--<!--          浮层框架开始         -->
			    <!--<div id="ly" style="position: fixed;opacity: 0.5; top: 0px; background-color: rgb(0, 0, 0); z-index: 9999; left: 0px; display: none; width: 100%;height: 100%;"></div>
                <div id="Layer2" align="center" style="z-index: 99999;expression((document.body.offsetWidth-100%)/2);" >
                    <ul class="tamkuang" >
                        <li>
                            
                                <div align="right">
                                 	<a href=JavaScript:; class="STYLE1" onclick="Lock_CheckForm(this);">[关闭]</a>     
                                </div>
                        </li>
                    </ul>
                </div>-->
    <!--  浮层框架结束-->	
				
				
			</section>
			<script type="text/javascript" src="<?php echo JS_PATH?>jquery.min.js"></script>
			<script type="text/javascript" src="<?php echo JS_PATH ?>layer.js "></script>
			<script type="text/javascript" src="<?php echo JS_PATH?>touchslider.js"></script>
			<script type="text/javascript" src="<?php echo JS_PATH?>zepto_min.js"></script>
		<script type="text/javascript">  
    function ordercCancel(oid){
        dialog_msg('#btn', '确定取消订单吗',oid);
        
    }
  function dialog_msg(element, msg,oid){
    var dialogPanel = '<div class="message-dialog" id="message-dialog">'
                +  '<div class="message-dialog-box">'
                +     '<article>'
                +       '<img src="<?php echo IMG_PATH?>tankuang@2x.png" class="tankuan_phto"/>'
                +     '</article>'
                +       '<p class="quxiaotishi">'
                +         '取消就不能享受我们的服务了哦'
                +       '</p>'
                +       '<p style="color:#333">'
                +         msg
                +       '</p>'
                
                +  '<div class="my_button">'
                +     '<button id="dialogCancelBtn">我再想想</button>'
                +     '<button id="dialogOkBtn">确定取消</button>'
                +  '</div>'
                +   '</div>'
                + '</div>';
    $(element).after(dialogPanel);
    $('#dialogCancelBtn').bind('click', function() {
      $('#message-dialog').remove();
    });
    var url = window.location.href;
    $('#dialogOkBtn').bind('click', function() {
         $.post('/index.php/Order/ordercCancel',{oid:oid},function(data){
         if(data.code){
             window.location.href = url;
         }
     },'json');
     $('#message-dialog').remove();
    });
  }

	function orderDel(oid){
		  layer.open({
			    content: '您确定要删除这个订单吗？'
			    ,btn: ['删除', '不删除']
			    ,yes: function(index){
			      layer.close(index);
		     	 var url = window.location.href;
		    	 $.post('/index.php/Order/orderDel',{oid:oid},function(data){
		             if(data.code){
		                 window.location.href = url
		             }
		         },'json');
			    }
			  });
	}


    //滑屏
    var i=1;
    $(document).on('scroll', function () {
        var wScrollY = window.scrollY; // 当前滚动条位置
        var wInnerH = window.innerHeight; // 设备窗口的高度（不会变）
        var bScrollH = document.body.scrollHeight; // 滚动条总高度
        if (wScrollY + wInnerH >= bScrollH) {
            var page=i++;
            var count=<?php echo $countNum; ?>;
            $(".footer").show();
            $(".footer").html('<p>~正在加载中~</p>');


            var arr=[];
            arr.length = 0;
            if(page*4<count)
            {
                $.get('/index.php/Home/orderList',{page:page,ajax:true},function(s){

                    if(s.code==0)
                    {
						$("#orderList").find('li').eq(length).after(s.data);
                    }
                    else
                    {
                        $(".footer").html('<p>~没有更多了~</p>');
                        return false;
                    }
                },'json');
            }
            else
            {
                $(".footer").html('<p>~没有更多了~</p>');
                return false;
            }


        }
    });


	
  

</script>  
		</body>

</html>