<!DOCTYPE html>
<html style="background-color: #fff;">

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
		<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
		<meta name="applicable-device" content="mobile">
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>new_file.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>set.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>medie.css" />
		<script type="text/javascript" src="<?php echo JS_PATH?>jquery.min.js"></script>
		<title>系统提示</title>
		<style>
			.main{
				text-align: center;
				overflow: auto;
			}
			.zanwu{
				text-align: center;
			}
			.zanwu img{
				margin-top: 30%;
			}
	    </style>
	</head>

	<body>
		<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
			<div class="m_header_bar J_header-bar">
				<?php include_once APPPATH . 'views/home/comback.php';?>
				<div class="mhb_center mhb_center_across">
					<h2 class="title">系统提示</h2>
				</div>

			</div>

		</header>
		<section class="main">
			<p class="zanwu"><img src="<?php echo IMG_PATH ?>sys@2x.png"  width="40%"/></p>
			<p style="color: #3c3434;font-size: 16px;"> <?php echo $msg; ?></p>
             <script language="javascript">setTimeout(function(){window.location.href='<?php echo $url_forward?>';},<?php echo $ms?>);</script>
			<p style="color: #333;font-size: 14px;"><lable id="timeOut">2</lable>秒后自动跳转首页</p>
			<a href="/index.php/Home/index"><p style="color: #ccc;font-size: 14px;">如果界面没有跳转您可以点击这里返回到首页</p></a>
		</section>
		<div style="height: 30px;"></div>
		<!-- <div class="button">
			<a href=""><button>返回上一页</button></a>
		</div>-->
	</body>
	<script>
    	$(function(){
            var wait=2;  
        	timeOut(wait);  
    	});
    	function timeOut(wait){  
    	    if(wait >=1){  
    	        setTimeout(function(){  
    	            wait--;  
    	            $("#timeOut").text(wait);
    	            timeOut();  
    	        },1000)  
    	    }  
    	}  
	</script>
</html>
