<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
		<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
		<meta name="applicable-device" content="mobile">
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>new_file.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>set.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>jishizhuye.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>medie.css" />
		<link rel="stylesheet" href="<?php echo CSS_PATH ?>baguetteBox.min.css">
		
				<script src="<?php echo JS_PATH?>baguetteBox.min.js"></script>
		<script src="<?php echo JS_PATH?>highlight.min.js" ></script>
		<title></title>
		<style>
			#tr1 {
				display: none;
			}
		</style>
	</head>

	<body>
		<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
			<div class="m_header_bar J_header-bar">
				
				
				<?php include_once APPPATH . 'views/home/comback.php';?>
				<div class="mhb_center mhb_center_across">
					<h2 class="title">个人主页</h2>
				</div>
				<div class="mhb_right">
					<a class="share" href="" id="collectin">
						<img class="icon icon-share" src="<?php echo IMG_PATH ?>iconget.png" name="mint" onclick="change_img(this)" id="a" />
						<img style="display: none;" class="icon icon-share" src="<?php echo IMG_PATH ?>iconnotget.png" name="baby" onclick="change_img(this)" id="b" />
					</a>
				</div>

			</div>

		</header>
		<div id="banner">
			<div class="touxiang">
				<p style="text-align: center;"><img class="touxiang_pho" src="<?php echo $techer_row['headurl']; ?>" /></p>
			</div>
			<div class="jishi_name">
				<p><?php echo $techer_row['nickname']; ?></p>
			</div>
			<div class="my_label">
			
				    <p style="text-align: center;">
					<span class="comment">共<?php echo $commemt_num; ?>条评论</span>
					<span class="goodwill">好感度<?php echo $great_bili; ?>%</span>
			
			</div>
		</div>
		<div class="my_album">
			<div class="album_left">
				<p>个人相册</p>

			</div>
			<div class="album_center baguetteBoxOne gallery">
				<ul style="height:100px;">
					 <?php foreach ($techer_photo_result as $k=>$v){ ?>
					 <?php if($v['type']=='1'){?>
                   <li style="height:83%;overflow: hidden"><a href="<?php echo $v['pic_url']?>" data-caption="Golden Gate Bridge"><img src="<?php echo $v['pic_url']?>?imageslim" /></a></li>
                   <?php }else{?>
                     <li><a href="<?php echo $v['video_url']?>" data-caption="Golden Gate Bridge"><video src="<?php echo IMG_PATH?>xiangce2@2x.png" /></a></li>
                    <?php }} ?>
				</ul>
				</div>
			 <?php if($techer_photo_nums>4){ ?>
                <div class="album_right">
                    <p><img src="/app/tech/views/share/<?php echo IMG_PATH ?>jiantou@2x.png"/></p>
                </div>
            <?php } ?>
		</div>
		<div class="my_information">
			<div class="information_list">
				<ul>
					<li>
						<span class="detailed_title"><img src="<?php echo IMG_PATH ?>jianjie@2x.png" />
					    个人简介</span>
						<span class="detailed_mess"><?php echo $techer_row['desc']; ?></span>

					</li>
					<li>
						<span class="detailed_title"><img src="<?php echo IMG_PATH ?>renzheng@2x.png" />
					    认证信息</span>
						<span class="detailed_mess"><?php echo substr($techer_row['mobile'],0,3); ?>****<?php echo substr($techer_row['mobile'],7,4); ?></span>
					</li>
					<li>
						<span class="detailed_title"><img src="<?php echo IMG_PATH ?>pingjia@2x.png" />
					    用户评论<span>（<?php echo $commemt_num; ?>条）</span></span>
					</li>
				</ul>
			</div>
			<div class="detailed_comment">
				<div class="all_label">
					   <?php if(empty($tag)){ ?>
                    <?php }else{ ?>
        
                        <?php foreach($tag as $key=>$val): ?>
                            <?php foreach($val as $k=>$v):  ?>
                                <?php $arr[]=$v['tag_name'];  ?>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                        
                        <?php if(!empty($arr)){ $arr=array_count_values($arr); foreach($arr as $ks=>$vs): ?>
                            <p><?php echo $ks; ?>(<?php echo $vs; ?>)</p>
                        <?php  endforeach;} ?>
                    <?php } ?>
				</div>
				<div class="detailed_content">
					<ul>
						  <?php foreach($comment_result as $k=>$v): ?>
                    <li>
                        <div class="up_top">
                            <img src="<?php if(empty($v['headurl'])){ echo 'https://api.xijiaomo.com/data/assets/images/logo.jpg'; }else{ echo $v['headurl'].'?imageView2/1/w/200/h/200'; }  ?>" style="border-radius: 50%;"/><p class="user_name"><?php echo $v['nick_name']; ?></p>
                            <p class="time_carry"><?php echo $v['dates']; ?></p>
                        </div>
                        <div class="bottom">
                            <p> <?php echo $v['content']; ?></p>
                        </div>
                    </li>
                <?php endforeach; ?>
					</ul>
				</div>
			</div>

		</div>
		<div class="footer">
			<p>~没有更多了~</p>
		</div>
		<div style="height:60px"></div>
		<footer class="bat_sure">
			<a href="/index.php/Order/placeOrder"><button>立即预约</button></a>
		</footer>
		<script>
			window.onload = function() {
				baguetteBox.run('.baguetteBoxOne');

				/*if(typeof oldIE === 'undefined' && Object.keys) {
					hljs.initHighlighting();
				}*/


				
			};
		</script>
		<script type="text/javascript">
			function change_img(e) {
				//  alert(e.name);//img的name
				var aaa = e.name;
				if(aaa == "mint") {
					document.getElementById("a").style.display = "none";
					document.getElementById("b").style.display = "block";
				}
				if(aaa == "baby") {
					document.getElementById("a").style.display = "block";
					document.getElementById("b").style.display = "none";
				}
			}
		</script>
	</body>

</html>