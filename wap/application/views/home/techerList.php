<!DOCTYPE html>
<html style="background-color: #fff;">

	<head>

		<head>
			<meta charset="utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
			<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
			<meta name="applicable-device" content="mobile">
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>new_file.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>set.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>medie.css" />
			<title>选择技师</title>
			<style>
				.main {
					overflow: auto;
				}
				
				.banner {
					height: 130px;
				}
				
				.banner img {
					width: 100%;
					height: 130px;
				}
				
				.all {
					background: #FFF;
					clear: both;
				}
				.all h3{
					font-size: 15px;
					padding: 0 5px;
					border-left: 4px solid #d5201e;
					margin-top:15px ;
					margin-left: 10px;
				}
				.all ul {
					margin: auto;
					padding-top: 20px;
				}
				
				.all ul li {
					float: left;
					width: 33%;
					text-align: center;
				}
				
				.all ul li:nth-child(3n+0) {
					margin-right: 0;
				}
				
				.all ul li img {
					width: 80px;
					border-radius: 50%;
				}
				
				.all ul li p {
					text-align: center;
					font-size: 14px;
					padding: 5px 0 22px 0;
				}
			</style>
		</head>

		<body>
			<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
				<div class="m_header_bar J_header-bar">
					<?php include_once APPPATH . 'views/home/comback.php';?>
					<div class="mhb_center mhb_center_across">
						<h2 class="title">选择技师</h2>
					</div>

				</div>

			</header>
			<section class="main">
				<div class="banner">
					<img src="<?php echo $goodsOption['publicity_img']?>" />
				</div>
				<div class="all clearfix">
					<h3>全部技师</h3>
					<ul class="clearfix">
					<?php foreach($techerList as $k=>$v){?>
						<a href="/index.php/Home/techInfo?tid=<?php echo $v['tid']?>&goods_type=<?php echo $goodsOption['cate_id']?>"><li><img src="<?php echo $v['headurl']?>" />
							<p>技师：<?php echo $v['nickname']?></p>
						</li></a>
					<?php }?>	
					</ul>

				</div>
			</section>
		</body>

</html>