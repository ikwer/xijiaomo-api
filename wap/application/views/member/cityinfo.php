<!DOCTYPE html>
<html style="background-color: #fff;">
	<head>
		<head>
			<meta charset="utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
			<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
			<meta name="applicable-device" content="mobile">
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>new_file.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>set.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>medie.css" />
			<title>选择城市</title>
			<style>
				.main {
					overflow: auto;
				}
				
				.city_name {
					height: 30px;
					background-color: #f3f3f3;
				}
				
				.city_name h3 {
					line-height: 30px;
					padding-left: 10px;
					color: #666;
				}
				
				.current_city {
					padding: 0 10px;
					height: 50px;
				}
				
				.current_city img {
					width: 14px;
					float: left;
					padding: 17px 5px 0 0
				}
				
				.current_city .current_map {
					line-height: 50px;
					font-size: 15px;
					color: #d5201e;
				}
				
				.open_city {
					overflow: auto;
					padding: 10px 10px 20px 10px;
				}
				
				.open_city ul li {
					float: left;
					width: 22.2%;
					text-align: center;
					margin: 2.5% 2.8% 0 0;
					padding: 0;
					box-sizing: border-box;
					border: 1px solid #666;
					border-radius: 24px;
					background: #fff;
					line-height: 35px;
					font-size: 12px;
				}
			</style>
		</head>

		<body>
			<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
				<div class="m_header_bar J_header-bar">
					<?php include_once APPPATH . 'views/home/comback.php';?>
					<div class="mhb_center mhb_center_across">
						<h2 class="title">选择城市</h2>
					</div>

				</div>

			</header>
			<section class="main">
				<div class="city_name">
					<h3>当前定位城市</h3>
				</div>
				<div class="current_city">
					<img src="<?php echo IMG_PATH ?>iconweizhi@2x.png" /><span class="current_map">武汉</span>
				</div>
				<div class="city_name">
					<h3>已开通城市</h3>
				</div>
				<div class="open_city">
					<ul>
					<?php if($cityinfo['already']){foreach($cityinfo['already'] as $k=>$v){?>
						<li><?php echo $v['name']?></li>
						<?php }}?>
					</ul>
				</div>

				<div class="city_name">
					<h3>即将开通城市</h3>
				</div>
				<div class="open_city">
					<ul>
						<?php if($cityinfo['soon']){foreach($cityinfo['soon'] as $k=>$v){?>
						<li><?php echo $v['name']?></li>
						<?php }}?>
					</ul>
				</div>

			</section>

		</body>

</html>