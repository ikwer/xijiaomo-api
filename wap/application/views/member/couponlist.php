<!DOCTYPE html>
<html style="background-color: #f3f3f3;">

	<head>

		<head>
			<meta charset="utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
			<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
			<meta name="applicable-device" content="mobile">
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>new_file.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>set.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>medie.css" />
			 <script type="text/javascript" src="<?php echo JS_PATH ?>jquery.min.js"></script>
			 	<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>layer.css" />
			 			<script type="text/javascript" src="<?php echo JS_PATH?>jquery.min.js"></script>
			<script type="text/javascript" src="<?php echo JS_PATH ?>layer.js "></script>
			 
			<title>积分兑换</title>
			<style>
				.all_cupon{
					padding:10px;
				}
				.all_cupon li p:nth-child(n+2){
					color: #999;
				}
				.all_cupon li{
					background: url(<?php echo IMG_PATH ?>cupon/quan1@2x.png) no-repeat;
					background-size: 100%;
					height: auto;
					line-height: 24px;
					padding: 30px 0;
					margin-bottom: 10px;
					position: relative;
				}
				.cupon .cupon_left{
					width: 33%;
				}
				.cupon .cupon_right{
					width: 66%;
				}
				.cupon .cupon_left p{
					text-align: center;
					
				}
				.cupon .cupon_left .money,.cupon_name{
					font-size: 18px;
					color:#D5201E;
					font-weight: 700;
				}
				.cupon .cupon_left .money span{
					font-size: 24px;
					font-weight: 800;
					color: #D5201E;
				}
				.cupon .cupon_right p{
					padding-left: 10px;
				}
				.nont p,.nont span{
					color: #ccc !important;
				}
				.all_cupon li.nont{
					background: url(<?php echo IMG_PATH ?>quan2@2x.png) no-repeat;
					background-size: 100%;
					height: auto;
					line-height: 24px;
					padding: 30px 0;
					margin-bottom: 10px;
					position: relative;
				}
			</style>
	</head>
	<body>
		<body>
		<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
				<div class="m_header_bar J_header-bar">
					<?php include_once APPPATH . 'views/home/comback.php';?>
					<div class="mhb_center mhb_center_across">
						<h2 class="title">积分兑换</h2>
					</div>

				</div>

			</header>
			<section class="main cupon">
				
				<ul class="all_cupon">
				<?php if(!empty($couponList)){foreach($couponList as $k =>$v){?>
					<li <?php if($v['status']){?>onclick="exchCoupon('<?php echo $v['sid']?>')"<?php }?>class="clearfix <?php if(!$v['status']){?>nont<?php }?>">
						<div class="cupon_left fl">
							<p class="money">¥<?php echo $v['money']?></p>
							<p>限武汉下单使用</p>
						</div>
						<div class="cupon_right fr">
							<p class="cupon_name"><?php echo $v['name']?></p>
							<p><?php echo $v['send_time']?>至<?php echo $v['end_time']?></p>
						</div>
					</li>
					<?php }}?>
					
				</ul>
			</section>
	</body>
	<script>
		function exchCoupon(sid){
    		$.post('/index.php/Member/exchCoupon',{sid:sid},function(s){
				    if(s.code == 1){
			    	  layer.open({
			    		    content: '兑换成功'
			    		    ,skin: 'msg'
			    		    ,time: 2 //2秒后自动关闭
			    		});
					 }
    		},'json')
		}

	</script>
</html>
