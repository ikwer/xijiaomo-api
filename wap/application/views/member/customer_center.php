<!DOCTYPE html>
<html style="background-color: #f3f3f3;">

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
		<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
		<meta name="applicable-device" content="mobile">
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>new_file.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>set.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>medie.css" />
		<title>客服中心</title>
		<style>
			.main {
				overflow: auto;
			}
			
			.banner {
				height: 160px;
			}
			
			.banner img {
				width: 100%;
				height: 160px;
			}
			
			.name_kefu {
				margin: 8px 0;
				background-color: #fff;
			}
			
			.name_kefu img {
				float: left;
				padding: 10px;
			}
			
			.sayhello {
				padding: 18px 0 5px 0;
				color: #D5201E;
				font-size: 15px;
				font-weight: 700;
			}
			
			.say_welcom {
				font-size: 12px;
				color: #666;
				line-height: 20px;
			}
			
			.lianxi li {
				background-color: #fff;
				width: 50%;
				float: left;
				text-align: center;
				position: relative;
				font-size: 14px;
				padding: 21px 0;
			}
			.lianxi li span{
				color: #999;
				padding-top: 5px;
				display: block
			}
			.lianxi .lianxi_kefu:after {
				content: "";
				width: 1px;
				height: 60px;
				background: #e6e6e6;
				position: absolute;
				right: 0px;
				top: 13px;
			}
			.pho_kefu{
				text-align: center;
				margin-top: 8px;
				background-color: #D5201E;
				color: #fff;
				padding: 13px 0;
				font-size: 14px;
			}
		</style>
	</head>

	<body>
		<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
			<div class="m_header_bar J_header-bar">
				<?php include_once APPPATH . 'views/home/comback.php';?>
				<div class="mhb_center mhb_center_across">
					<h2 class="title">客服中心</h2>
				</div>

			</div>

		</header>
		<section class="main">
			<div class="banner">
				<img src="<?php echo IMG_PATH ?>custom/banner@2x.png" />
			</div>
			<div class="name_kefu clearfix">
				<img src="<?php echo IMG_PATH ?>custom/touxiang@2x.png" width="60px" />
				<p class="sayhello">小雅客服，您好</p>
				<span class="say_welcom">欢迎来到服务中心，很高兴为您服务</span>
			</div>
			<ul class="clearfix lianxi">
				<li class="lianxi_kefu">
					<p class="kefu">公司邮箱</p>
					<span><?php echo $user_about['kf_email']?></span>
				</li>
				<li>
					<p class="jishi">联系客服</p>
					<span>QQ：<?php echo $user_about['kf_qq']?></span>
				</li>
			</ul>
			<p class="pho_kefu">027-87839337</p>
		</section>
	</body>

</html>