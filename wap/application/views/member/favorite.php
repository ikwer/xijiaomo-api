<!DOCTYPE html>
<html style="">

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
		<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
		<meta name="applicable-device" content="mobile">
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>new_file.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>set.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>medie.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>tankuang.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>layer.css" />
		<title>我的收藏</title>
		<style>
			.main {
				overflow: auto;
			}
			
			ul {
				padding: 10px;
			}
			
			ul li {
				background-color: #fff;
				border-radius: 6px;
				box-shadow: 1px 1px 2px #CCCCCC;
				margin-bottom: 10px;
			}
			
			.collection_up {
				height: 100px;
				border-bottom: 1px solid #e6e6e6;
			}
			
			.collection_up img {
				float: left;
				padding: 10px;
			}
			
			.detailed_jishi {
				padding-top: 10px;
			}
			
			.detailed_jishi p {
				padding-bottom: 5px;
				color: #666;
			}
			
			p.name {
				font-size: 14px;
				color: #333;
			}
			
			.name span {
				font-size: 12px;
				color: #666;
			}
			
			.collection_down button {
				float: right;
				background-color: #fff;
				padding: 5px 15px;
				border-radius: 20px;
				margin: 10px;
				font-size: 14px;
			}
			
			.yuyue_but {
				border: 1px solid #D5201E;
				color: #D5201E;
			}
			
			.notcollection_but {
				border: 1px solid #999;
				color: #999;
			}
		</style>
	</head>

	<body>
		<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
			<div class="m_header_bar J_header-bar">
				<?php include_once APPPATH . 'views/home/comback.php';?>
				<div class="mhb_center mhb_center_across">
					<h2 class="title">我的收藏</h2>
				</div>

			</div>

		</header>
		<section class="main">
			<ul class="clearfix">
				
				<?php foreach($favorite as $k=>$v){?>
    				<li class="clearfix">
    					<div class="collection_up">
    						<img src="<?php echo $v['headurl']?>" width="80px" />
    						<div class="detailed_jishi">
    							<p class="name"><?php echo $v['truename']?>&nbsp;&nbsp;<span>女</span>&nbsp;&nbsp;<span><?php echo $v['birthday']?>岁</span></p>
    							<p>3年经验 <?php echo $v['probability']?>好评</p>
    							<p>服务项目：全身按摩 脸部SPA</p>
    							<p>收藏时间：<?php echo $v['on_time']?></p>
    						</div>
    					</div>
    					<div class="collection_down">
    						<button class="yuyue_but">预约</button>
    						<button class="notcollection_but btn"  value="<?php echo $v['sid']?>" >取消收藏</button>
    					</div>
    				</li>
    				<?php }?>
			</ul>
		</section>
		<script type="text/javascript" src="<?php echo JS_PATH?>jquery.min.js"></script>
			<script type="text/javascript" src="<?php echo JS_PATH ?>layer.js "></script>

		<script type="text/javascript">
			$('.btn').click(function() {
				var tid = $(this).attr('value');
				var url = window.location.href;
				 layer.open({
					    content: '您确定要取消收藏吗？'
					    ,btn: ['取消', '暂不取消']
					    ,yes: function(index){
    					    layer.close(index);
        					$.post("/index.php/Member/cancelFavorite",{tid:tid},function(data){
    							if(data.code == 1){
    								window.location.href=url
        						}
        					},'json');
					    }
				 });
			});
		</script>
	</body>

</html>