<!DOCTYPE html>
<html style="background-color: #fff;">

	<head>

		<head>
			<meta charset="utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
			<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
			<meta name="applicable-device" content="mobile">
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>new_file.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>set.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>medie.css" />
			<script type="text/javascript" src="<?php echo JS_PATH?>jquery.min.js"></script>
			
						<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>layer.css" />
			<script type="text/javascript" src="<?php echo JS_PATH ?>layer.js "></script>
			<title>意见反馈</title>
			<style>
				.main {
					overflow: auto;
				}
				
				.city_name {
					height: 30px;
					background-color: #f3f3f3;
				}
				
				.city_name h3 {
					line-height: 30px;
					padding-left: 10px;
					color: #666;
				}
				
				.leixing {
					background-color: #fff;
					padding: 0 10px;
				}
				
				.leixing li {
					height: 44px;
					border-bottom: 1px dashed #e6e6e6;
					position: relative;
					line-height: 44px;
				}
				
				.leixing li:last-child {
					border: none
				}
				
				.current:after {
					content: "";
					background: url(<?php echo IMG_PATH ?>iconchoose@2x.png) no-repeat 0px 4px;
					background-size: 100%;
					height: 20px;
					right: 0px;
					position: absolute;
					width: 16px;
					top: 22%;
				}
				
				.leixing textarea {
					margin: 10px;
					resize: none;
					width: 86%;
					border: 1px solid #ccc;
					border-radius: 6px;
					height: 100px;
					background-color: #e5e5e5;
					padding:10px;
				}
			</style>
		</head>

		<body>
			<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
				<div class="m_header_bar J_header-bar">
					<?php include_once APPPATH . 'views/home/comback.php';?>
					<div class="mhb_center mhb_center_across">
						<h2 class="title">意见反馈</h2>
					</div>

				</div>
			</header>
			<form action="/index.php/Member/addFeedback" name="FeedbackFrom" id="FeedbackFrom" method="post">
    			<input type="hidden" name="formhash" value="1"/>
    			<section id="main">
    				<div class="city_name">
    					<h3>意见类型</h3>
    				</div>
    				<ul class="clearfix leixing">
    					<li class="current">产品问题</li>
    					<li>投诉</li>
    					<li>功能闪退</li>
    					<input type="hidden" name="feedtype" id="feedtype" value="产品问题"/>
    					<textarea placeholder="请在这里输入您的意见，我们都会认真改进哦" name="content" id="content"></textarea>
    				</ul>
    				<div class="button">
    					<button type='button' onclick="subFeed()">提交</button>
    				</div>
    			</section>
			</form>
		</body>
		<script>
		$(function(){
			$(".leixing li").click(function(){
				$(this).addClass('current');
				$(this).siblings().removeClass('current');
				$("#feedtype").val($(this).html());
			});
		});

		function subFeed(){
			if($("#content").val()==''){
				 layer.open({
				    	content: '请填写内容'
				        ,skin: 'msg'
				        ,time: 2 //2秒后自动关闭
				      });
				return false;
			}
			$("#FeedbackFrom").submit();
		}
		</script>

</html>