<!DOCTYPE html>
<html style="background-color: #fff;">
	<head>
		<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
		<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
		<meta name="applicable-device" content="mobile">
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>new_file.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>set.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>medie.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>tankuang.css"/>
		<title>喜脚么-个人中心</title>
	</head>
	<style>
		.main{
			overflow: auto;
		}
		.mes_left{
			width: 20%;
			float: left;
		}
		.main ul li{
			height: 70px;
			border-bottom: 1px solid #e6e6e6;
		}
		.main ul li .icon_mes{
			text-align: center;
			padding-top: 20px;
		}
		.main ul li img{
			width: 50%;
		}
		.mes_right{
			text-align: left;
		}
		.mes_right p{
			font-size: 16px;
		}
		.mes_detail{
			padding-top: 10px;
		}
		.read{
			color: #666;
		}
		.mes_detail_main{
			width: 75%;
			color: #999;
			font-size: 12px !important;
			overflow:hidden; 
			text-overflow:ellipsis;
			white-space:nowrap;
			padding-top: 10px;
		}
	</style>
	<body>
		<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
				<div class="m_header_bar J_header-bar">
					<?php include_once APPPATH . 'views/home/comback.php';?>
					<div class="mhb_center mhb_center_across">
						<h2 class="title">消息</h2>
					</div>

				</div>

		</header>
		<section class="main">
			<div style="background-color: #f3f3f3;height: 8px;"></div>
			<ul>
				<li>
					<div class="mes_left"><p class="icon_mes"><img src="<?php echo IMG_PATH ?>messgas/iconxiaoxi1@2x.png"/></p></div>
					<div class="mes_right"><p class="mes_detail"><a id="btn">全程69折起，下单即享</a></p>
						<p class="mes_detail_main">劲爆惊喜，立即下单可获得69折现金折扣，下单...</p>
					</div>
				</li>
				<li>
					
					<div class="mes_left"><p class="icon_mes"><img src="<?php echo IMG_PATH ?>messgas/iconxiaoxi1@2x.png"/></p></div>
					<div class="mes_right"><p class="mes_detail">全程69折起，下单即享</p>
						<p class="mes_detail_main">劲爆惊喜，立即下单可获得69折现金折扣，下单...</p>
					</div>
					
				</li>
				<li>
					<div class="mes_left"><p class="icon_mes"><img src="<?php echo IMG_PATH ?>messgas/iconxiaoxi2@2x.png"/></p></div>
					<div class="mes_right"><p class="mes_detail read">全程69折起，下单即享</p>
						<p class="mes_detail_main">劲爆惊喜，立即下单可获得69折现金折扣，下单...</p>
					</div>
				</li>
			</ul>
			
		</section>
		<script type="text/javascript" src="js/jquery.min.js" ></script>
				<script type="text/javascript">  

  $('#btn').click(function(){
    dialog_msg('#btn', '      这里是需要显示的消息详情这里是需要,显示的消息详情，这里是需要显示的消息详情，这里是需要显示的消息详情。');
  });
  function dialog_msg(element, msg){
    var dialogPanel = '<div class="message-dialog" id="message-dialog">'
                +  '<div class="message-dialog-box">'
                +     '<a id="guanbi">'
                +      '</a>'
                +       '<h3>'
                +       '<span>'
                +       '全程69折起，下单即享'
                +       '</span>'
                +       '</h3>'
                +       '<p class="main_content" style="">'
                +         msg
                +       '</p>'
                
                +  '<div class="my_button">'
                +     '<button id="next">下一条</button>'
                +  '</div>'
                +   '</div>'
                + '</div>';
    $(element).after(dialogPanel);
    $('#guanbi').bind('click', function() {
      $('#message-dialog').remove();
    });
    $('#next').bind('click', function() {
     
    });
  }

</script>  
	</body>
</html>
