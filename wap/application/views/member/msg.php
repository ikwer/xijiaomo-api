<!DOCTYPE html>
<html style="background-color: #fff;">
	<head>
		<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
		<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
		<meta name="applicable-device" content="mobile">
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>new_file.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>set.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>medie.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>tankuang.css"/>
		<title>喜脚么-个人中心</title>
	</head>
	<style>
		.main{
			overflow: auto;
		}
		.mes_left{
			width: 20%;
			float: left;
		}
		.main ul li{
			height: 70px;
			border-bottom: 1px solid #e6e6e6;
		}
		.main ul li .icon_mes{
			text-align: center;
			padding-top: 20px;
		}
		.main ul li img{
			width: 50%;
		}
		.mes_right{
			text-align: left;
		}
		.mes_right p{
			font-size: 16px;
		}
		.mes_detail{
			padding-top: 10px;
		}
		.read{
			color: #666;
		}
		.mes_detail_main{
			width: 75%;
			color: #999;
			font-size: 12px !important;
			overflow:hidden; 
			text-overflow:ellipsis;
			white-space:nowrap;
			padding-top: 10px;
		}
	</style>
	<body>
		<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
				<div class="m_header_bar J_header-bar">
					<?php include_once APPPATH . 'views/home/comback.php';?>
					<div class="mhb_center mhb_center_across">
						<h2 class="title">消息</h2>
					</div>

				</div>

		</header>
		<section class="main">
			<div style="background-color: #f3f3f3;height: 8px;"></div>
			<ul>
    			<?php if(!empty($notice)){foreach($notice as $k=>$v){?>
    				<li>
    					<div class="mes_left"><p class="icon_mes">
    					<?php if($v['is_read'] == 1){?>
    					   <img src="<?php echo IMG_PATH ?>messgas/iconxiaoxi2@2x.png"/>
    					<?php }else{?>
    					   <img src="<?php echo IMG_PATH ?>messgas/iconxiaoxi1@2x.png"/>
    					<?php }?>
    					</p></div>
    					<div class="mes_right"><p class="mes_detail title"><a id="btn" class="title<?php echo $k?>" onclick="getNotice('<?php echo $k?>',<?php echo $v['m_id']?>)"><?php echo $v['title'];?></a></p>
    						<p class="mes_detail_main content" id="content<?php echo $k?>"><?php echo $v['content'];?></p>
    					</div>
    				</li>
    			<?php }}?>
			</ul>
			
		</section>
		<script type="text/javascript" src="<?php echo JS_PATH?>jquery.min.js" ></script>
				<script type="text/javascript">  

function getNotice(key,id){
    var title = $('.title'+key).html();
    var content = $('#content'+key).html();
    console.log(content);
	dialog_msg('#btn', title,content,id);
}
function dialog_msg(element, title,content,id){
    var dialogPanel = '<div class="message-dialog" id="message-dialog">'
                +  '<div class="message-dialog-box">'
                +     '<a id="guanbi">'
                +      '</a>'
                +       '<h3>'
                +       '<span>'
                +       title
                +       '</span>'
                +       '</h3>'
                +       '<p class="main_content" style="">'
                +         content
                +       '</p>'
                
                +  '<div class="my_button">'
                +     '<button id="next">下一条</button>'
                +  '</div>'
                +   '</div>'
                + '</div>';
    $(element).after(dialogPanel);
    var url = window.location.href;
    $.post('/index.php/Member/read_msg',{m_id:id});
    $('#guanbi').bind('click', function() {
		$('#message-dialog').remove();
		window.location.href = url;
    });
    $('#next').bind('click', function() {
     
    });
}

//     function read_msg(id){
     
//     }

</script>  
	</body>
</html>
