<!DOCTYPE html>
<html style="background-color: #fff;">

	<head>

		<head>
			<meta charset="utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
			<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
			<meta name="applicable-device" content="mobile">
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>new_file.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>set.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>medie.css" />
			<title>积分明细</title>
			<style>
				.main {
					overflow: auto;
				}
				
				.integral_left {
					float: left;
				}
				
				.integral {
					float: right;
				}
				
				.integral_detailed li {
					padding: 20px 10px;
					border-bottom: 1px solid #e6e6e6;
				}
				
				.integral_left h2 {
					font-size: 15px;
					padding-bottom: 5px;
				}
				.integral_left p{
					color: #999;
				}
				.integral{
					font-size: 18px;
					line-height: 40px;
				}
				.add{
					color: #D5201E;
				}
			</style>
		</head>

		<body>
			<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
				<div class="m_header_bar J_header-bar">
					<?php include_once APPPATH . 'views/home/comback.php';?>
					<div class="mhb_center mhb_center_across">
						<h2 class="title">积分明细</h2>
					</div>

				</div>

			</header>
			<section class="main">
				<ul class="integral_detailed">
				<?php if($data){foreach($data as $k=>$v){?>
					<li class="clearfix">
						<div class="integral_left">
							<h2><?php echo $v['origin_name']?></h2>
							<p><?php echo $v['addtime']?></p>
						</div>
						<?php if($v['type']==1){?>
						<p class="integral ">-<?php echo $v['number']?>分</p>
						<?php }else{?>
							<p class="integral add">-<?php echo $v['number']?>分</p>
							<?php }?>
					</li>
					<?php }}?>
				</ul>

			</section>
		</body>

</html>