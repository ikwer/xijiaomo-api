<!DOCTYPE html>
<html>

	<head>

		<head>
			<meta charset="utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
			<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
			<meta name="applicable-device" content="mobile">
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>new_file.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>set.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>medie.css" />
			<title>个人设置</title>
			<style>
				#main {
					overflow: auto;
				}
				
				.touxiang_jishi {
					background-color: #fff;
					margin-top: 8px;
				}
				
				.touxiang_jishi img {
					padding: 15px 10px
				}
				
				.basic li {
					padding: 0 10px;
					height: 50px;
					border-bottom: 1px solid #e6e6e6;
					position: relative;
					line-height: 50px;
					background-color: #fff;
				}
				
				ul.basic {
					margin: 8px 0px;
				}
				
				.huise {
					color: #999;
				}
				
				p.fl {
					font-size: 14px;
				}
				
				.touxiang_jishi p {
					line-height: 92px;
					padding-right: 25px;
				}
				
				.icon_going {
					position: relative;
					height: 92px;
				}
				
				.icon_going:after {
					content: "";
					background: url(<?php echo IMG_PATH ?>jiantou@2x.png) no-repeat 0px 4px;
					background-size: 40%;
					height: 20px;
					right: 0px;
					position: absolute;
					width: 16px;
					top: 40%;
				}
				
				.icon_go:after {
					content: "";
					background: url(<?php echo IMG_PATH ?>jiantou@2x.png) no-repeat;
					background-size: 40%;
					height: 20px;
					right: 0px;
					position: absolute;
					width: 16px;
					top: 36%;
				}
			</style>
		</head>

		<body>
			<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
				<div class="m_header_bar J_header-bar">
					<?php include_once APPPATH . 'views/home/comback.php';?>
					<div class="mhb_center mhb_center_across">
						<h2 class="title">设置</h2>
					</div>
				</div>

			</header>
			<style>
			    .hid{opacity: 0;}
			    
			</style>
			<section id="main">
				<ul class="touxiang_jishi clearfix">
					<li class="icon_going">
						<img class="fl" src="<?php if($userInfo['headurl']){echo $userInfo['headurl'];}else{?>/data/img/headurldefault.png<?php }?>" style="width:65px;height:65px;border-radius: 50%;" />
						    <p class="fr"><input type="file" accept="image/*" /></p>
					</li>
				</ul>
				<ul class="clearfix basic">
					<li>
						<p class=" fl">昵称</p>
						<p class=" fr huise"><?php echo $userInfo['nickname']?></p>
					</li>
					<li>
						<p class="fl">手机</p>
						<p class="fr huise"><?php echo $userInfo['mobile']?></p>
					</li>
				</ul>
				<ul class="clearfix basic">
					<li class="icon_go">
						<a href="/index.php/Member/addFeedback"><p>意见反馈</p></a>
					</li>
					<li class="icon_go">
						<p>清理缓存</p>
					</li>
					<li class="icon_go">
						<a href="/index.php/Member/user_about"><p>关于我们</p></a>
					</li>
				</ul>
				<div class="button" style="background: #f3f3f3;">
					<a href="/index.php/Member/logout"><button>退出登录</button></a>
				</div>
			</section>
		</body>

</html>