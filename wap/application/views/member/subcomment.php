<!DOCTYPE html>
<html style="background-color: #fff;">

	<head>

		<head>
			<meta charset="utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
			<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
			<meta name="applicable-device" content="mobile">
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>new_file.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>set.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>medie.css" />
				<script type="text/javascript" src="<?php echo  JS_PATH?>jquery.min.js"></script>
			<title>评论</title>
			<style>
				.uato_ping {
					width: 50%;
					font-size: 14px;
					margin: auto;
				}
				
				.hao_ping:before {
					content: "";
					background: url(<?php echo IMG_PATH ?>conmmtent/icongood@2x.png) no-repeat;
					width: 32px;
					height: 24px;
					display: inline-block;
					background-size: 60%;
					float: left;
					margin-right: 5px;
				}
				
				.label_active:before {
					content: "";
					background: url(<?php echo IMG_PATH ?>conmmtent/icongood_lab@2x.png) no-repeat;
					width: 32px;
					height: 24px;
					display: inline-block;
					background-size: 60%;
					float: left;
					margin-right: 5px;
				}
				
				.cha_ping:before {
					content: "";
					background: url(<?php echo IMG_PATH ?>conmmtent/iconcha@2x.png) no-repeat 0 2px;
					width: 35px;
					height: 22px;
					display: inline-block;
					background-size: 60%;
					float: left;
				}
				
				.chalabel_active:before {
					content: "";
					background: url(<?php echo IMG_PATH ?>conmmtent/iconcha_lab@2x.png) no-repeat 0 2px;
					width: 35px;
					height: 22px;
					display: inline-block;
					background-size: 60%;
					float: left;
				}
				
				.biao_qian {
					padding: 0 10px;
				}
				
				.biao_qian li {
					float: left;
					margin: 0 10px 10px 0;
				}
				
				.biao_qian li p {
					padding: 5px 12px;
					border-radius: 20px;
					color: #666;
					border: 1px solid #666;
				}
				
				p.active {
					color: #D5201E !important;
					border: 1px solid #D5201E !important;
				}
				
				textarea {
					margin: 0 10px;
					resize: none;
					width: 86%;
					border: 1px solid #ccc;
					border-radius: 6px;
					height: 100px;
					background-color: #e5e5e5;
					padding: 10px;
				}
			</style>
		</head>

		<body>
			<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
				<div class="m_header_bar J_header-bar">
					<?php include_once APPPATH . 'views/home/comback.php';?>
					<div class="mhb_center mhb_center_across">
						<h2 class="title">评论</h2>
					</div>

				</div>

			</header>
			<form action="/index.php/Member/subcomment" name="commentForm" method="post">
			<input type="hidden" name="formhash" value="1"/>
			<input type="hidden" name="order_id" value="<?php echo $techInfo['order_id']?>"/>
    			<section class="main">
    				<div style="height: 8px; background-color: #f3f3f3;"></div>
    				<div style="text-align: center;padding-top: 30px;font-size: 14px;line-height: 30px;">
    					<img src="<?php echo $techInfo['headurl']?>" width="75px" />
    					<p>技师：<?php echo $techInfo['nicknamer']?></p>
    				</div>
    				<div style="padding: 15px;text-align: center;margin: 10px 0 15px 0;">
    					<div class="clearfix uato_ping">
    						<p class="hao_ping fl label_active" value="1">好评</p>
    						<p class="cha_ping fr" value="2">差评</p>
    					</div>
    				</div>
    				<input type="hidden" name="comment_type" id="comment_type" value="1"/>
        				<ul class="biao_qian clearfix" id="comm_1">
        			        <?php if(!empty($goodtagname)){foreach($goodtagname as $k=>$v){?>
            					<li>
            						<p class="biaoqian_list" value="<?php echo $v['id']?>"><?php echo $v['tag_name']?></p>
            					</li>
        					<?php }}?>
        				</ul>
        				<ul class="biao_qian clearfix" id="comm_2" style="display:none">
        				 <?php if(!empty($badtagname)){foreach($badtagname as $k=>$v){?>
        					<li>
        						<p class="biaoqian_list" value="<?php echo $v['id']?>"><?php echo $v['tag_name']?></p>
        					</li>
        					<?php }
        					}?>
        				</ul>
        				<input type="hidden" name="tagid" id="tagid" value=""/>
    				<textarea placeholder="其他想说的(50字以内)" name="wenben"></textarea>
    				<div class="button">
    					<a href="index.html"><button>提交</button></a>
    				</div>
    			</section>
			</form>
			<script type="text/javascript">
				$(function(){
					$(".uato_ping p").click(function(){
    					var tagId = $(this).attr('value');
    					if(tagId == 1){
    						$("#comment_type").val(1);
    						$(this).addClass('label_active');
    						$(this).siblings('p').removeClass('chalabel_active');
    						$("#comm_1").show();
    						$("#comm_2").hide();
    					}else{
    						$("#comment_type").val(2);
    						$(this).addClass('chalabel_active');
    						$(this).siblings('p').removeClass('label_active');
    						$("#comm_1").hide();
    						$("#comm_2").show();
    					}
    					$("#tagid").val('');
					});
				});
				$('.biao_qian p').click(function() { //单独a标签点击添加class
					var thidTagId =  $(this).attr('value');
					var s = $("#tagid").val();
    				if(!$("#tagid").val()){
    					$("#tagid").val(thidTagId);
    					$(this).addClass('active');
        			}else{
        				if(s.indexOf(thidTagId) >= 0){
            				if(s.indexOf(','+thidTagId) >= 0){
    							str=s.replace(','+thidTagId,'');
            				}else{
            					str=s.replace(thidTagId+',','');
                			}
        					$("#tagid").val(str);
							$(this).removeClass('active');
							return false;
            			}
        				$(this).addClass('active');
						$("#tagid").val($("#tagid").val()+','+thidTagId);
            		}
				});

			</script>
			
			

		</body>

</html>