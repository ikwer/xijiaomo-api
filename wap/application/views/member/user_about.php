<!DOCTYPE html>
<html style="background-color: #fff;">

	<head>

		<head>
			<meta charset="utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
			<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
			<meta name="applicable-device" content="mobile">
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>new_file.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>set.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>medie.css" />
			<title>关于我们</title>
			<style>
				#main{
					overflow: auto;
					text-align: center;
				}
				ul{
					margin: 20px 40px;
					border: 1px solid #e6e6e6;
					height: 270px;
					border-radius: 15px;
				}
				ul li{
					margin: 0px 10px;
				}
				ul h3{
					line-height: 50px;
					border-bottom: 1px solid #e6e6e6;
				}
				.detailed_lianxi{
					height: 40px;
					border-bottom: 1px solid #e6e6e6;
					line-height: 40px;
					text-align: left;
				}
				.detailed_lianxi span{
					color: #999;
				}
				.detailed_lianxi_time{
					color: #999;
					text-align: left;
					padding-top: 5px;
				}
	      </style>
		</head>

		<body>
			<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;border-bottom: 1px solid #e6e6e6;">
				<div class="m_header_bar J_header-bar">
					<?php include_once APPPATH . 'views/home/comback.php';?>
					<div class="mhb_center mhb_center_across">
						<h2 class="title">关于我们</h2>
					</div>

				</div>

			</header>
			<section id="main">
				<p style="padding-top: 30px;"><img src="<?php echo IMG_PATH ?>logo@2x.png" width="90px"/></p>
				<p><?php echo $user_about['slogan']?></p>
				<p>版本号：<?php echo $user_about['version']?></p>
				<p style="padding-top: 30px;color: #999;"> www.xijiaomo.com</p>
				<ul>
					<li><h3>联系我们</h3></li>
					<li class="detailed_lianxi">
						<p><span>客服邮箱 : </span><?php echo $user_about['kf_email']?></p>
					</li>
					<li class="detailed_lianxi">
						<p><span>微信服务号 : </span><?php echo $user_about['weixin_code']?></p>
					</li>
					<li class="detailed_lianxi">
						<p><span>客服QQ : </span><?php echo $user_about['kf_qq']?></p>
					</li>
					<li class="detailed_lianxi">
						<p><span>公司电话 : </span><?php echo $user_about['company_phone']?></p>
					</li>
					<li class="detailed_lianxi_time">
						<p><span><?php echo $user_about['ps']?></span></p>
					</li>
				</ul>
			</section>
	</body>
</html>
