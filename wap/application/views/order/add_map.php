<!DOCTYPE html>
<html style="background-color: #fff;">

	<head>

		<head>
			<meta charset="utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
			<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
			<meta name="applicable-device" content="mobile">
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>new_file.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>set.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>medie.css" />
			   <script type="text/javascript" src="<?php echo JS_PATH?>jquery.min.js "></script>
			<title>地址</title>
			<style>
				.main{
					overflow: auto;
				}
				.m_header_bar {
					padding: 0 10px;
					line-height: 45px;
					font-size: 14px;
				}
				
				.position:after {
					content: "";
					background: url(img/map/icon_xiala@2x.png) no-repeat 0 3px;
					display: inline-block;
					width: 20px;
					height: 10px;
					background-size: 60%;
					margin-left: 5px;
				}
				
				.search {
					width: 70%;
				}
				
				.search input {
					text-align: center;
					border-radius: 30px;
					border: 1px solid #ccc;
					background-color: #e9e9e9;
					font-size: 12px;
					height: 30px;
					width: 100%;
					color: #666;
					margin-left: 10px;
				}
				
				.qu_xiao {
					padding-right: 10px;
					color: #D5201E;
				}
				.map{
					width: 100%;
					height: 230px;
				}
				.city_name {
					height: 30px;
					background-color: #f3f3f3;
				}
				
				.city_name h3 {
					line-height: 30px;
					padding-left: 10px;
					color: #666;
				}
				.current_city {
					padding: 0 10px;
					height: 65px;
					border-bottom: 1px solid #e6e6e6;
				}
				
				.current_city img {
					width: 14px;
					float: left;
					padding: 24px 0
				}
				.current_city .current_map {
					font-size: 15px;
					padding-bottom: 5px;
					color: #fa891b;
				}
				.fujin{
					color: #D5201E;
					font-size: 15px;
					padding-bottom: 5px;
				}
				.detail_posi{
					color: #999999;
					width: 100%;
				}
			</style>
		</head>

		<body>
		<input type="hidden" name="url" id="url" value="<?php if(isset($_SERVER['HTTP_REFERER'])){echo $_SERVER['HTTP_REFERER'];}?>"/>
		<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
				<div class="m_header_bar J_header-bar">
					<div class="mhb_left">
						<a class="position">武汉</a>
					</div>
					<div class="mhb_right">
						<a  href="javascript:window.history.go(-1);" class="qu_xiao"  m="">取消</a>
					</div>
				</div>
			</header>
            <iframe id="mapPage" width="100%" height="700px" frameborder=0 
                src="http://apis.map.qq.com/tools/locpicker?search=1&type=1&key=OB4BZ-D4W3U-B7VVO-4PJWW-6TKDJ-WPB77&referer=myapp">
            </iframe> 
     
        	<script>
        	    window.addEventListener('message', function(event) {
        	        // 接收位置信息，用户选择确认位置点后选点组件会触发该事件，回传用户的位置信息
        	        var loc = event.data;
        	        if (loc && loc.module == 'locationPicker') {//防止其他应用也会向该页面post信息，需判断module是否为'locationPicker'
        	          console.log('location', loc);  
        	          var url = encodeURI(loc.poiaddress);
        	          window.location.href='/index.php/Order/address?s='+url+'&lat='+loc.latlng.lat+'&lng='+loc.latlng.lng;
        	        }                                
        	    }, false); 
        	</script>
		</body>

</html>