<!DOCTYPE html>
<html style="background-color: #fff;">

	<head>

		<head>
			<meta charset="utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
			<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
			<meta name="applicable-device" content="mobile">
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>new_file.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>set.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>medie.css" />
			
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>layer.css" />
			<script type="text/javascript" src="<?php echo JS_PATH ?>jquery.min.js "></script>
			<script type="text/javascript" src="<?php echo JS_PATH ?>layer.js "></script>
			
			<script type="text/javascript" src="<?php echo JS_PATH ?>jQuery.Form.js"></script>
			<script type="text/javascript" src="<?php echo JS_PATH ?>jquery.validate.js"></script>
			<title>选择地址</title>
			<style>
				 input {
                	border: none;
                	margin-left: 15px;
                	height: 80%;
                }
				.book_add:after {
					content: "";
					background: url(<?php echo IMG_PATH?>jiantou@2x.png) no-repeat;
					background-size: 40%;
					height: 20px;
					right: 0px;
					position: absolute;
					width: 20px;
					top: 36%;
				}
				 label {
                	line-height: 50px;
                	font-size: 14px;
                	padding: 5px 0;
}
				.main {
					overflow: hidden;
				}
				
				.main .book_mess li {
					padding: 0 10px;
					height: 50px;
					border-bottom: 1px solid #e6e6e6;
					position: relative;
					line-height: 50px;
				}
				
				.main .book_mess li p {
					font-size: 15px;
					line-height: 50px;
				}
				.adress {
					background: url(<?php echo IMG_PATH?>iconweizhi@2x.png) no-repeat 0px 7px;
					background-size: 18%;
					padding-left: 20px;
				}
				
				.book_add .cell_pho {
					background: url(<?php echo IMG_PATH?>iconweizhi@2x.png) no-repeat 0px 7px;
					background-size: 18%;
					padding-left: 20px;
				}
				
				.detail_adress img{
					width: 13px;
					float: left;
					padding-top: 16px;
					padding-right: 8px;
				}
				.detail_adress{
					line-height: 50px;
				}
				.detail_adress li{
					border-bottom: 1px solid #e6e6e6;
					padding: 0 10px;
					font-size: 14px;
				}
				.detail_adress p{
					width: 90%;
					overflow:hidden; 
			text-overflow:ellipsis;
			white-space:nowrap;
				}
			</style>
		</head>

		<body>
			<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
				<div class="m_header_bar J_header-bar">
					<?php include_once APPPATH . 'views/home/comback.php';?>
					<div class="mhb_center mhb_center_across">
						<h2 class="title">选择地址</h2>
					</div>

				</div>

			</header>
			<section class="main">
				<div style="height: 8px; background-color: #f3f3f3;"></div>
				<form action="/index.php/Order/address" method="post" id="addressForm" name="addressForm">
				<input type="hidden" name="formhash" value="1"/>
    				<ul class="book_mess order">
    					<li class="book_add ">
    						<a href="choose_map.html">
    						<label class="cell_pho">服务区域</label>
    						<a href="/index.php/Order/add_map"><input type="text" name="address" placeholder="" class="address" class="nofastclick" value="<?php echo $s?>" readonly="readonly"></a>
    					    </a>
    					</li>
    					<li class=" add_adress">
    						<label class="adress">详细门牌</label>
    						<input type="text" placeholder="请填写您的具体地址" name="house_number" id="house_number"  value="" class="nofastclick housenum">
    						<img class="icon-go" />
    					</li>
    				</ul>
    				<div class="button">
                    <button>确认</button>
                    </div>
				</form>
                <div style="height: 8px; background-color: #f3f3f3;"></div>
                <div>
                	<ul class="detail_adress">
                	<?php if(!empty($address)){foreach($address as $k=>$v){?>
				        <li onclick="getaddress('<?php echo $k?>')"><img src="<?php echo IMG_PATH?>iconweizhi_g@2x.png"/><p><lable id="address<?php echo $k?>"><?php echo $v['address']?></lable><lable id="housenum<?php echo $k?>"><?php echo $v['house_number']?></lable></p></li>
				        <?php }}?>
				    </ul>
                </div>
			</section>
		</body>
				<script>
		//添加发票
    		$("#addressForm").validate({
        			focusInvalid : true, 
        			  rules: {
        			},
        			messages:{
        				
        			},
    			 	errorElement : "span",
    		        errorClass : "error_info",
    		        highlight : function(element, errorClass,
    		            validClass) {
    		          $(element).closest('.form-control').addClass(
    		              'highlight_red');
    		        },
    		        success : function(element) {
    		          $(element).siblings('.form-control')
    		              .removeClass('highlight_red');
    		          $(element).siblings('.form-control').addClass(
    		              'highlight_green');
    		          $(element).remove();
    		        },
    		        submitHandler : function(form) {  //验证通过后的执行方法
    		            //当前的form通过ajax方式提交（用到jQuery.Form文件）
    		            $("#addressForm").ajaxSubmit({
    		                dataType:"json",
    		                success:function( jsondata ){
									if( jsondata.code == -1){
						                 layer.open({
									    	content: jsondata.message
									        ,skin: 'msg'
									        ,time: 2 //2秒后自动关闭
									      });
			                   		}else{
			                	  		window.location.href = jsondata.data;
			                   		}
    			                   		
    		                  }
    		                }); 
    		        }
    		});
        function getaddress(id){
        	var address = $("#address"+id).html();
        	var housernun = $("#housenum"+id).html();

        	$.ajax({
        	    url:'/index.php/Order/lat',
        	    type:'POST', //GET
        	    async:true,    //或false,是否异步
        	    data:{
//         	        name:'yang',age:25
        	    	housernun:housernun,address:address
        	    },
        	    timeout:5000,    //超时时间
        	    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
        	    beforeSend:function(xhr){
        	        console.log(xhr)
        	        console.log('发送前')
        	    },
        	    success:function(data,textStatus,jqXHR){
            	    $(".address").val(data.data.address);
            	    $("#house_number").val(data.data.house_num);
        	    },
        	    error:function(xhr,textStatus){
          	    	  layer.open({
      	    		    content: '网络异常请刷新后在试'
      	    		    ,skin: 'msg'
      	    		    ,time: 2 //2秒后自动关闭
      	    		  });
        	    },
        	});



    		
        }
    		
		</script>

</html>