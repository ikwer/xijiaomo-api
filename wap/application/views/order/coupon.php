<!DOCTYPE html>
<html style="background-color: #f3f3f3;">

	<head>

		<head>
			<meta charset="utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
			<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
			<meta name="applicable-device" content="mobile">
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>new_file.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>set.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>medie.css" />
			<title>我的优惠券</title>
			<style>
				.main{
					background-color: #f3f3f3;
					overflow: auto;
				}
				.notuser{
					margin: 10px;
					height: 44px;
					background: #fff;
					border:1px solid #ccc;
					border-radius: 6px;
					line-height: 44px;
					font-size: 14px;
					padding-left: 15px;
					
				}
				h3{
					padding: 0 0 10px 10px;
					font-size: 14px;
				}
				h3 span{
					color: #D5201E;
					font-size: 16px;
					font-weight: 700;
				}
				.all_cupon{
					padding: 0 10px;
				}
				.all_cupon li p:nth-child(n+2){
					color: #999;
				}
				.all_cupon li{
					background: url(<?php echo IMG_PATH?>cupon/quan1@2x.png) no-repeat;
					background-size: 100%;
					height: auto;
					line-height: 24px;
					padding: 28px 0;
					margin-bottom: 10px;
				}
				.cupon .cupon_left{
					width: 33%;
				}
				.cupon .cupon_right{
					width: 66%;
				}
				.cupon .cupon_left p{
					text-align: center;
					
				}
				.cupon .cupon_left .money,.cupon_name{
					font-size: 18px;
					color:#D5201E;
					font-weight: 700;
				}
				.cupon .cupon_left .money span{
					font-size: 24px;
					font-weight: 800;
					color: #D5201E;
				}
				.cupon .cupon_right p{
					padding-left: 10px;
				}
				.nont p,.nont span{
					color: #ccc !important;
				}
			</style>
	</head>
	<body>
		<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
				<div class="m_header_bar J_header-bar">
					<?php include_once APPPATH . 'views/home/comback.php';?>
					<div class="mhb_center mhb_center_across">
						<h2 class="title">选择优惠券</h2>
					</div>

				</div>
			</header>
			<section class="main cupon">
				<a href="/index.php/Order/getCoupon?cid=delCou"><p class="notuser">不使用优惠券</p></a>
				<h3>有&nbsp;<span><?php echo $can_use_num?></span>&nbsp;个红包可用</h3>
				<ul class="all_cupon">
				<?php foreach($coupon_list as $k=>$v){?>
				    <a href="/index.php/Order/getCoupon?cid=<?php echo $v['id']?>">
    					<li class="clearfix">
    						<div class="cupon_left fl">
    							<p class="money">¥<span><?php echo $v['money']?></span></p>
    							<p><?php echo $v['name']?></p>
    						</div>
    						<div class="cupon_right fr">
    							<p class="cupon_name">上门按摩优惠券</p>
    							<p><?php echo $v['send_time']?>至<?php echo $v['end_time']?></p>
    						</div>
    					</li>
					</a>
					<?php }?>
				</ul>
			</section>
	</body>
</html>
