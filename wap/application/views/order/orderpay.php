<!DOCTYPE html>
<html style="background-color: #fff;">
	<head>

		<head>
			<meta charset="utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
			<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
			<meta name="applicable-device" content="mobile">
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>new_file.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>set.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>medie.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>layer.css" />
			
			<script type="text/javascript" src="<?php echo JS_PATH ?>jquery.min.js "></script>
			<script type="text/javascript" src="<?php echo JS_PATH ?>layer.js "></script>
			
			
			<title>支付订单</title>
<style>
	.project_detail{
		height: 50px;
		line-height: 50px;
		padding: 0 10px;
		font-size: 14px;
	}
	.fr{
		color: #999;
	}
	.pay_way{
		padding: 10px;
	}
	.pay_way h3{
		font-size: 15px;
	}
	.pay_way li{
		height: 80px;
		line-height: 80px;
		font-size: 15px;
		border-bottom: 1px solid #e6e6e6;
	}
	.pay_way li img{
		padding-top: 22px;
		padding-right: 10px;
		width: 38px;
	}
	.pay_way li {
					position: relative;
				}
				
				.pay_way li.active:after {
					content: "";
					position: absolute;
					background: url(/data/img/icon_lab@2x.png) no-repeat;
					background-size: 90%;
					height: 23px;
					width: 23px;
					right: 0px;
					top: 38%;
				}
</style>
		</head>

		<body>
			<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
				<div class="m_header_bar J_header-bar">
					<?php include_once APPPATH . 'views/home/comback.php';?>
					<div class="mhb_center mhb_center_across">
						<h2 class="title">支付订单</h2>
					</div>

				</div>
			</header>
			<form action="/index.php/Order/pay" method="post" name="payForm" id="payForm" />
    			<input type="hidden" name="order_id" id="order_id" value="<?php echo $orderInfo['order_sn']?>"/>
    			<input type="hidden" name="order_amount" id="order_amount" value="<?php echo $orderInfo['order_amount']?>"/>
    			<section class="main">
    				<div class="project_detail">
    					<p class="fl">深度全身治疗</p>
    					<p class="fr"><?php echo $orderInfo['goods_price']?>元/<?php echo $orderInfo['server_time']?>分钟</p>
    				</div>
    				<div class="project_detail">
    					<p class="fl">优惠</p>
    					<p class="fr" style="color:#D5201E">-<?php echo $orderInfo['coupon_price']?></p>
    				</div>
    				<div class="project_detail">
    					<p class="fl">实际支付</p>
    					<p class="fr"><?php echo $orderInfo['order_amount']?></p>
    				</div>
    				<div style="height: 8px; background-color: #f3f3f3;"></div>
    				<ul class="pay_way">
    					<h3>请选择支付方式</h3>
    					<li class="active">
    						<img class="fl" src="<?php echo IMG_PATH ?>iconzhifubao@2x.png"/><p>支付宝</p>
    					</li>
    					<li>
    						<img class="fl" src="<?php echo IMG_PATH ?>iconweixin@2x.png"/><p>支付宝</p>
    					</li>
    				</ul>
    				<div class="button" onclick="submitOrder()">
    				<button type="button">确认支付</button>
                    </div>
    			</section>
			</form>
	</body>
			<script>
				function submitOrder(){
        			$.post('/index.php/Order/checkOrder',{order_id:$("#order_id").val(),order_amount:$("#order_amount").val()},function(data){
						if(data.code == 1){
							$("#payForm").submit();
						}else{
							 layer.open({
						    	content: data.message
						        ,skin: 'msg'
						        ,time: 2 //2秒后自动关闭
						      });
						}
            		},'json');
        			
				}
		</script>
</html>
