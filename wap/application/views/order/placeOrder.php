<!DOCTYPE html>
<html style="background-color: #fff;">

	<head>

		<head>
			<meta charset="utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
			<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
			<meta name="applicable-device" content="mobile">
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>new_file.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>set.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>medie.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>book_order.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>tankuang.css" />
			
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>layer.css" />
			<script type="text/javascript" src="<?php echo JS_PATH ?>jquery.min.js "></script>
			<script type="text/javascript" src="<?php echo JS_PATH ?>layer.js "></script>
			
			<script type="text/javascript" src="<?php echo JS_PATH ?>jQuery.Form.js"></script>
			<script type="text/javascript" src="<?php echo JS_PATH ?>jquery.validate.js"></script>
			
			<title>预约下单</title>

		</head>

		<body>
			<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
				<div class="m_header_bar J_header-bar">
					<?php include_once APPPATH . 'views/home/comback.php';?>
					<div class="mhb_center mhb_center_across">
						<h2 class="title">预约下单</h2>
					</div>

				</div>

			</header>
			<form action="/index.php/Order/confirm_order" method="post" name="orderForm" id="orderForm">
			<input type="hidden" name="order_id" value="1"/>
    			<section class="main">
    				<div style="height: 8px; background-color: #f3f3f3;"></div>
    				<ul class="clearfix book_detail order">
    					<li>
    						<p class="book_name"><?php echo $orderInfo['goods_name'] ?></p>
    						<p class="book_time"><?php echo $orderInfo['goods_price'] ?>元/<?php echo $orderInfo['server_time']?>分钟</p>
    					</li>
    					<li>
    						<p class="book_name">技师</p>
    						<p class="book_time"><?php echo $orderInfo['technickanme']?></p>
    					</li>
    				</ul>
    				<div style="height: 8px; background-color: #f3f3f3;"></div>
    				<div class="order-mian clearfix">
    					<ul class="book_mess order">
    						<li class="book_add">
    							<label class="cell_pho">手机号码</label>
    							<input type="text" name="mobile" id="mobile" value="<?php echo $orderInfo['usermobile']?>" class="nofastclick">
    						</li>
    						<li class="book_add add_adress">
    							<label class="adress">服务地址</label>
    							<a href="/index.php/Order/address"><input type="text" value="<?php echo $this->input->cookie('address').$this->input->cookie('house_number')?>" name="address" id="address"  placeholder="请填写您的具体地址" class="nofastclick" readonly="readonly"></a>
    						</li>
    						<li class="book_add add_adress clearfix">
    							<label class="door_time">上门时间</label>
    							<span id="botBtn" class="time fr">即时上门</span>
    						</li>
    
    					</ul>
    					<div class="order_leaving clearfix">
    						<textarea placeholder="如有特殊要求请留言" name="taboo"></textarea>
    						<!-- <ul>
    							<li>
    								<p>时间提前</p>
    								</a>
    							</li>
    							<li class="select">
    								<p>颈椎病</p>
    							</li>
    							<li>
    								<p>腰肌劳损</p>
    							</li>
    							<li>
    								<p>跌打损伤</p>
    							</li>
    						</ul>-->
    					</div>
    				</div>
    				<div style="height: 8px; background-color: #f3f3f3;"></div>
    				<ul class="clearfix order order_tadboo">
    					<li class="book_add add_adress">
    						<label class="youhuiquan">优惠券&nbsp;</label>
    						<input type="hidden" name="coupon_id" value="<?php if(!empty($coupon)){echo $coupon['coupon_id'];}?>"/>
    						<?php if($orderInfo['coupon_num']){?>
    						<a href="/index.php/Order/getCoupon">
    						<input type="text" placeholder="" class="nofastclick" readonly="readonly" <?php if(!empty($coupon)){?>value="<?php echo $coupon['coupon_name']?>"<?php }else{?>value="<?php echo '您有'.$orderInfo['coupon_num'].'张可使用的优惠券'?>"<?php }?>></a>
    					   <?php }else{?>
    						<input type="text" placeholder="" class="nofastclick" readonly="readonly" value="<?php echo '暂无优惠券可用'?>">
    					   <?php }?>
    					</li>
    					<li class="book_add">
    						<label class="pay">实际支付</label>
    						<input type="text" placeholder="" class="nofastclick" readonly="readonly" value="<?php if(!empty($coupon)){echo $coupon['order_amount'];}else {echo $orderInfo['goods_price'];}?>元">
    
    					</li>
    					<li>
    						<label class="taboo">禁忌提示</label>
    						<p> <?php if(isset($orderInfo['taboo'])){echo $orderInfo['taboo'];}else{echo '暂无禁忌说明';}?>
    						</p>
    					</li>
    				</ul>
    				<footer class="bat_sure">
    					<a href="pay_order.html"><button>确认预约</button></a>
    				</footer>
    				<!-- 弹出层 -->
    				<aside id="aside" class="aside">
    					<i class="aside-overlay hideAside"></i>
    
    					<div class="botContent">
    						<div class="mian_conten">
    							<h3>选择时间</h3>
    						</div>
    						<div class="left_time fl">
    							<ul>
    								<li onclick="getDateByAjax('1')" value="1" class="select">
    									<p><?php echo $todayDate?>（今天）</p>
    								</li>
    								<li onclick="getDateByAjax('2')" value="2">
    									<p><?php echo $TomorrowDate?>（明天）</p>
    								</li>
    							</ul>
    						</div>
    						<p  style="line-height:45px" class="shiji_daoda">实际到达时间可能有20分钟误差</p>
    						<div class="right_time fr" style="height:70%;overflow-y:auto; overflow-x:hidden;">
    							<ul id="serverTime">
    							</ul>
    						</div>
    					</div>
                       <input type="hidden" name="date_type" value="1" id="date_type"/>
                        <input type="hidden" name="date_id" value="" id="date_id"/>
    				</aside>
    				<script type="text/javascript">
    				/*	$('.order_leaving,li').click(function() { //单独a标签点击添加class
    						if($(this).hasClass('select')) {
    							$(this).removeClass('select');
    						} else {
    							$(this).addClass('select');
    						}
    					});*/
    
    						var botBtn = $('#botBtn');
    						var oAside = $('#aside');
    					$(function() {
//     						$("#serverTime li").click(function(){
//             					alert(123);
//     							//$("#date_id").val($(this).attr('value'));
//             				});
    
    						botBtn.on("click", function() {
    							$('.botContent').show();
    							$("body").css('overflow-y','hidden');
    							oAside.addClass('active');
    							$('body').addClass('fiexd');
    							getDateByAjax(1);
    						});
    						$('.hideAside').on("click", function() {
    							oAside.removeClass('active');
    							$("body").css('overflow-y','auto');
    						});
    						$(".left_time li").click(function(){
								$(this).addClass('select');
								$("#date_type").val($(this).attr('value'));
								$(this).siblings().removeClass('select');
        					});
    					
    					});
    						function getDateId(id){
								$("#date_id").val(id);
								oAside.removeClass('active');
    							$("body").css('overflow-y','auto');
//     							alert($(".left_time"))
    							//alert(show());
//     							alert($(this).attr('value'));
    							var time = $("#serverTime li[value='"+id+"']").children().html();
    							$("#botBtn").html(show()+time);
    							
        					}

    						function show(){  
 							   var mydate = new Date();  
 							   str = (mydate.getMonth()+1) + "月"; 
 							    if($("#date_type").val()==1){
 							    	str += mydate.getDate() + "日";
 							    }else{
 							   		str += (mydate.getDate()+1) + "日";  
 							    }
 							   return str;  
 							  }  

						//发送请求获取所有的服务时间段
    					function getDateByAjax(date_type){
        					if($(".left_time li[value='2']").hasClass('select')){
            					$(".left_time li[value='2']").removeClass('select');
    							$(".left_time li[value='1']").addClass('select');
            				}else{
            				}
    						$.ajax({
				        	    url:'/index.php/Order/server_time',
				        	    type:'POST', //GET
				        	    async:true,    //或false,是否异步
				        	    data:{
				        	    	date_type:date_type
				        	    },
				        	    timeout:5000,    //超时时间
				        	    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
				        	    beforeSend:function(xhr){
				        	        console.log(xhr)
				        	        console.log('发送前')
				        	    },
				        	    success:function(data,textStatus,jqXHR){
				        	    	if(data.code == 1){
										$("#serverTime").replaceWith(data.data);
					        	    }
				        	    }
				        	});
        					}
    				</script>
    
    			</section>
			</form>
		</body>
		<script>
		//添加发票
    		$("#orderForm").validate({
        			focusInvalid : true, 
        			  rules: {
        			},
        			messages:{
        				
        			},
    			 	errorElement : "span",
    		        errorClass : "error_info",
    		        highlight : function(element, errorClass,
    		            validClass) {
    		          $(element).closest('.form-control').addClass(
    		              'highlight_red');
    		        },
    		        success : function(element) {
    		          $(element).siblings('.form-control')
    		              .removeClass('highlight_red');
    		          $(element).siblings('.form-control').addClass(
    		              'highlight_green');
    		          $(element).remove();
    		        },
    		        submitHandler : function(form) {  //验证通过后的执行方法
    		            //当前的form通过ajax方式提交（用到jQuery.Form文件）
    		            $("#orderForm").ajaxSubmit({
    		                dataType:"json",
    		                success:function( jsondata ){
    									if( jsondata.code == -1){
    						                 layer.open({
    									    	content: jsondata.message
    									        ,skin: 'msg'
    									        ,time: 2 //2秒后自动关闭
    									      });
    			                   		}else{
    			                	  		window.location.href = jsondata.data;
    			                   		}
    			                   		
    		                  }
    		                }); 
    		        }
    		});
		</script>
</html>