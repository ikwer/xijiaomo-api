<!DOCTYPE html>
<html style="background-color: #f3f3f3;">

	<head>

		<head>
			<meta charset="utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
			<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
			<meta name="applicable-device" content="mobile">
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>new_file.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>set.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>medie.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>tankuang.css" />
			
					<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>layer.css" />
			<script type="text/javascript" src="<?php echo JS_PATH ?>jquery.min.js "></script>
			<script type="text/javascript" src="<?php echo JS_PATH ?>layer.js "></script>
			<script type="text/javascript" src="<?php echo JS_PATH ?>jQuery.Form.js"></script>
			<script type="text/javascript" src="<?php echo JS_PATH ?>jquery.validate.js"></script>
			<title>申请退款</title>
			<style>
				.main {
					overflow: auto;
				}
				
				.project {
					margin: 8px 0;
					padding: 0 10px;
					line-height: 50px;
					background-color: #fff;
					font-size: 14px;
				}
				
				.choose_list li {
					background-color: #fff;
					margin-bottom: 8px;
					line-height: 60px;
					padding: 0 10px;
				}
				
				.type_refund {
					font-size: 14px;
				}
				
				.introduce {
					position: relative;
					height: 60px;
				}
				
				.introduce:after {
					content: "";
					background: url(img/jiantou@2x.png) no-repeat;
					background-size: 40%;
					height: 20px;
					right: 0px;
					position: absolute;
					width: 20px;
					top: 36%;
				}
				
				input {
					border: none;
					margin-left: 10px;
					width: 75%;
				}
				
				.jine {
					padding-left: 10px;
					color: #D5201E;
					font-weight: 700;
					font-size: 14px;
				}
				
				.mian_conten {
					font-size: 14px;
					border-bottom: 1px solid #e6e6e6;
				}
				
				.mian_conten h3 {
					line-height: 50px;
				}
				
				.botContent li {
					text-align: left;
					border-bottom: 1px solid #e6e6e6;
					padding: 12px 10px;
				}
				
				.botContent li p:nth-child(odd) {
					font-size: 14px;
					padding-bottom: 5px;
				}
				
				.botContent li p:nth-child(n+2) {
					color: #D5201E;
				}
				
				.nont_on button {
					height: 44px;
					position: fixed;
					bottom: 0px;
					width: 100%;
					border: none;
					background-color: #D5201E;
					color: #fff;
					font-size: 15px;
				}
				.botContent li{
					position: relative;
				}
				li.active:after {
					content: "";
					background: url(<?php echo IMG_PATH?>book_order/icon_lab@2x.png) no-repeat;
					display: inline-block;
					width: 30px;
					height: 30px;
					background-size: 50%;
					position: absolute;
					right: 0;
    top: 25px;
				}
			</style>
		</head>

		<body>
			<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
				<div class="m_header_bar J_header-bar">
					<?php include_once APPPATH . 'views/home/comback.php';?>
					<div class="mhb_center mhb_center_across">
						<h2 class="title">申请退款</h2>
					</div>

				</div>

			</header>
			<form action="/index.php/Order/refundstep" name="refundForm" method="post" id="refundForm">
			    
			    <input type="hidden" name="formhash" value="1"/>
			     
			<section class="main">
				<div class="project clearfix">
					<p class="fl"><?php echo $orderinfo['goods_name']?></p>
					<p class="fr"><?php echo $orderinfo['goods_price']?>元/<?php echo $orderinfo['server_time'];?>分钟</p>
				</div>
				<input type="hidden" name="oid" value="<?php echo $oid;?>" />
				<ul class="choose_list">
					<li class="clearfix introduce">
						<p class="type_refund fl">退款原因</p>
						<a href="/index.php/Order/refundstep1?oid=<?php echo $orderinfo['order_id'];?>"><p id="botBtn" class=" fr" style="padding-right: 25px;font-size: 14px;"><?php if(isset($refundDesc)){echo $refundDesc ;}else{
						echo '请选择退款原因';}?></p></a>
						<input type="hidden" name="refundDesc" value="<?php if(isset($refundDesc)){echo $refundDesc ;}?>"/>
					</li>
					<li class="clearfix">
						<p class="type_refund fl">退款金额</p>
						<span class="jine"> ¥<?php if(isset($refundMoney)){echo $refundMoney;}else{ echo $orderinfo['goods_price'];}?></span>
						<input type="hidden" name="refundMoney" value="<?php if(isset($refundMoney)){echo $refundMoney ;}else{ echo $orderinfo['goods_price'];}?>"/>
						
					</li>
					<li class="clearfix">
						<p class="type_refund fl">退款说明</p>
						<input type="text" name="refund_note"  placeholder="选填" class="nofastclick">
					</li>
				</ul>
				<div class="button" style="background: #f3f3f3;">
					<a href=""><button>提交</button></a>
				</div>
			</section>
			</form>
		
			<script type="text/javascript" src="js/jquery.min.js"></script>
			<script type="text/javascript">
				$(function() {
					var botBtn = $('#botBtn');
					var oAside = $('#aside');

					botBtn.on("click", function() {

						$('.botContent').show();
						oAside.addClass('active');
					});
					$('.hideAside').on("click", function() {
						oAside.removeClass('active');
					});
				})
			</script>
		</body>
				<script>
		//添加发票
    		$("#refundForm").validate({
        			focusInvalid : true, 
        			  rules: {
        			},
        			messages:{
        				
        			},
    			 	errorElement : "span",
    		        errorClass : "error_info",
    		        highlight : function(element, errorClass,
    		            validClass) {
    		          $(element).closest('.form-control').addClass(
    		              'highlight_red');
    		        },
    		        success : function(element) {
    		          $(element).siblings('.form-control')
    		              .removeClass('highlight_red');
    		          $(element).siblings('.form-control').addClass(
    		              'highlight_green');
    		          $(element).remove();
    		        },
    		        submitHandler : function(form) {  //验证通过后的执行方法
    		            //当前的form通过ajax方式提交（用到jQuery.Form文件）
    		            $("#refundForm").ajaxSubmit({
    		                dataType:"json",
    		                success:function( jsondata ){
    									if( jsondata.code == -1){
    						                 layer.open({
    									    	content: jsondata.message
    									        ,skin: 'msg'
    									        ,time: 2 //2秒后自动关闭
    									      });
    			                   		}else{
    			                	  		window.location.href = jsondata.data;
    			                   		}
    			                   		
    		                  }
    		                }); 
    		        }
    		});
		</script>

</html>