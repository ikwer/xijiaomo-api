<!DOCTYPE html>
<html style="background-color: #f3f3f3;">

	<head>

		<head>
			<meta charset="utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
			<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
			<meta name="applicable-device" content="mobile">
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>new_file.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>set.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>medie.css" />
			<title>退款类型</title>
			<style>
				.main{
					overflow: auto;
				}
				.project{
					margin: 8px 0;
					padding: 0 10px;
					line-height: 50px;
					background-color: #fff;
					font-size: 14px;
				}
				li{
					background-color: #fff;
					margin-bottom: 8px;
				}
				li img{
					padding: 20px 20px;
				}
				.type_refund{
					font-size: 14px;
					padding-top: 10px;
				}
				.intro{
					padding-top: 5px;
					color: #999;
				}
			</style>
	</head>
	<body>
		<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
				<div class="m_header_bar J_header-bar">
					<?php include_once APPPATH . 'views/home/comback.php';?>
					<div class="mhb_center mhb_center_across">
						<h2 class="title">选择退款类型</h2>
					</div>

				</div>

			</header>
			<section class="main">
				<div class="project clearfix">
					<p class="fl">深度全身理疗</p>
					<p class="fr">168元/90分钟</p>
				</div>
				<ul>
				    <?php foreach($confRefund as $k=>$v){?>
				    
				<a href="/index.php/Order/refundstep?desc=<?php echo $v['desc']?>&oid=<?php echo $oid;?>&money=<?php echo $v['refund_money'];?>">	<li class="clearfix">
						<img class="fl" src="<?php echo IMG_PATH?>iconkuan@2x.png" width="20px"/>
						<p class="type_refund" ><?php echo $v['desc'];?></p>
						<p class="intro">技师已上门，联系不到用户或用户临时有事</p>
						</a>
					</li>
				<?php }?>
				</ul>
			</section>
	</body>
</html>

