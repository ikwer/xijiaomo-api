<!DOCTYPE html>
<html style="background-color: #fff;">

	<head>

		<head>
			<meta charset="utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
			<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
			<meta name="applicable-device" content="mobile">
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>new_file.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>set.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>medie.css" />
			<title>退款状态</title>
			<style>
				.banner {
					height: 100px;
					background: url(<?php echo IMG_PATH?>service/bg@2x.png) no-repeat;
					color: #fff;
					padding: 0 10px;
				}
				.pleas{
					padding: 30px 0 5px 0;
					font-size: 15px;
				}
				.tishi {
					line-height: 40px;
					padding: 0 10px;
					font-size: 14px;
				}
				
				.set_tishi {
					padding: 10px 10px;
				}
				.project_detail {
					background-color: #f3f3f3;
					height: 50px;
					line-height: 50px;
					font-size: 14px;
					padding: 0 10px;
				}
			    li i{
			    	height: 10px;
			    	width: 10px;
			    	border-radius: 10px;
			    	background-color: #e6e6e6;
			    	display: inline-block;
			    	margin-right: 10px;
			    }
			    /*.set_tishi{
			    	margin-bottom: 8px;
			    }*/
			    .set_tishi p{
			    	color: #999;
			    	line-height: 20px;
			    }
				.xiangqing {
					padding: 10px 10px;
					color: #999;
					line-height: 20px;
				}
				.but_an{
					float: right;
					padding: 5px 0px;
				}
				.but_an a{
					padding: 5px 12px;
					border: 1px solid #e6e6e6;
					border-radius: 6px;
				}
				.but_an a:nth-child(1){
					border: 1px solid #D5201E;
					color: #D5201E;
					margin-right: 10px;
				}
				.but_an a:nth-child(2){
					color: #666;
					border: 1px solid #666;
				}
			</style>
		</head>

		<body>
			<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
				<div class="m_header_bar J_header-bar">
					<?php include_once APPPATH . 'views/home/comback.php';?>
					<div class="mhb_center mhb_center_across">
						<h2 class="title">退款状态</h2>
					</div>

				</div>

			</header>
			<section class="main">
				<div class="banner">
					<p class="pleas">请耐心等待处理</p>
					<p><?php echo $refundArr['stime']?></p>
				</div>
				<p class="tishi">您已成功发起退款申请，请耐心等待处理</p>
				<div style="background-color: #f3f3f3;height: 8px;"></div>
				<ul class="set_tishi clearfix">
					<li><p><i></i>技师同意或请求超时未处理，系统将退款给您</p></li>
					<li><p><i></i>如果技师拒绝，您可以修改申请退款申请后再次发起，技师会重新处理</p></li>
					<li class="but_an">
						<!--<a>取消申请</a>-->
						<a href="/index.php/Order/refundstep?oid=<?php echo $oid?>">修改申请</a></li>
				</ul>
				<div style="background-color: #f3f3f3;height: 8px;"></div>
				<p class="tishi">退款信息</p>
				<div class="project_detail">

					<p class="fl">深度全身理疗</p>
					<p class="fr"><?php echo $refundArr['goods_price']?>元/<?php echo $refundArr['server_time']?>分钟</p>
					

				</div>
				<div class="xiangqing">
					<p>退款原因：其他</p>
					<p>退款金额：<?php echo $refundArr['refund_money']?>元</p>
					<p>申请时间：<?php echo $refundArr['add_time']?></p>
				</div>
			</section>
		</body>

</html>
