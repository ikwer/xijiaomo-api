<!DOCTYPE html>
<html style="background-color: #f3f3f3;">

	<head>

		<head>
			<meta charset="utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
			<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
			<meta name="applicable-device" content="mobile">
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>new_file.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>set.css" />
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>medie.css" />
			<script type="text/javascript" src="<?php echo JS_PATH ?>jquery.min.js"></script>
			<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH ?>layer.css" />
			 <script type="text/javascript" src="<?php echo JS_PATH ?>layer.js "></script>
			
			<title>开始服务</title>
			<style>
				input{
					border:none;
					color: #999;
				}
				.main{
					overflow: auto;
					background-color: #fff;
				}
				.banner{
					height: 120px;
				}
				.banner img{
					width: 100%;
					height: 120px;
				}
				.lianxi li{
					width: 50%;
					float: left;
					text-align: center;
					line-height: 50px;
					position: relative;
					border-bottom: 1px solid #e6e6e6;
					font-size: 14px;
					
				}
				.lianxi .lianxi_kefu:after{
					content: "";
					width: 1px;
					height: 50px;
					background: #e6e6e6;
					position: absolute;
					right: 0px;
				}
				.lianxi li .kefu{
					background: url(<?php echo IMG_PATH?>service/iconkefu2@2x.png) no-repeat;
					background-size: 25%;
					padding-left: 28px;
				}
				.mess_service label{
					padding-right: 10px;
				}
				.lianxi li .jishi{
					background: url(<?php echo IMG_PATH?>service/iconlianxi@2x.png) no-repeat;
					background-size: 24%;
					padding-left: 27px;
				}
				.jishi_mess{
					background: url(<?php echo IMG_PATH?>service/iconzhuanghu@2x.png) no-repeat;
					background-size: 20%;
					padding-left: 27px;
				}
				.service_adress{
					background: url(<?php echo IMG_PATH?>service/icondizhi@2x.png) no-repeat;
					background-size: 21%;
					padding-left: 27px;
				}
				.service_project{
					background: url(<?php echo IMG_PATH?>service/iconanmo@2x.png) no-repeat;
					background-size: 21%;
					padding-left: 27px;
				}
				.service_time{
					background: url(<?php echo IMG_PATH?>service/icontime@2x.png) no-repeat;
					background-size: 21%;
					padding-left: 27px;
				}
				.mess_service li{
					line-height: 50px;
					position: relative;
					border-bottom: 1px solid #e6e6e6;
					font-size: 14px;
					padding: 0 10px;
				}
				.banner{
					position: relative;
					background: url(<?php echo IMG_PATH?>service/bg@2x.png) no-repeat;
					color: #fff;
					text-align: center;
				}
				.countdown_time{
					font-size: 26px;
				}
				.banner span{
					padding: 35px 0;
					display: inline-block;
				}
				.expect_time{
					background: url(<?php echo IMG_PATH?>service/icontime_white@2x.png) no-repeat 0px 2px;
                    background-size: 9%;
					padding-left: 20px;
				}
			</style>
	</head>
	<body>
			<header class="m_header  sticky_head" id="J_header" style="position: sticky; top: 0px; bottom: 0px;">
				<div class="m_header_bar J_header-bar">
					<?php include_once APPPATH . 'views/home/comback.php';?>
					<div class="mhb_center mhb_center_across">
						<h2 class="title">开始服务</h2>
					</div>

				</div>

			</header>
			<section class="main">
				<div class="banner">
					<span>
					   <p class="countdown_time"><?php echo $orderInfo['estimateTime'];?></p>
					   <label class="expect_time">预计到达时间   <?php echo $orderInfo['estimateDate'];?></label>
					</span>
				</div>
				<ul class="clearfix lianxi">
					<li class="lianxi_kefu">
						<a href="tel:<?php echo $orderInfo['kfmobile']?>"<label class="kefu">联系客服</label></a>
					</li>
					<li>
						<a href="tel:<?php echo $orderInfo['mobile']?>">联系技师</a>
					</li>
				</ul>
				<div style="height: 8px ;background-color: #f3f3f3;"></div>
				<ul class="clearfix mess_service">
					<li class="lianxi_kefu">
						<label class="jishi_mess">技师信息</label>
						<input type="text" placeholder="" class="nofastclick" readonly="readonly" value="<?php echo $orderInfo['tnickname'];?>">
					</li>
					<li>
						<label class="service_adress">服务地址</label>
						<input type="text" placeholder="" class="nofastclick" readonly="readonly" value="<?php echo $orderInfo['address'];?>">
					</li>
					<li>
						<label class="service_project">服务项目</label>
						<input type="text" placeholder="" class="nofastclick" readonly="readonly" value="<?php echo $orderInfo['goods_name'];?>">
					</li>
					<li>
						<label class="service_time">服务时长</label>
						<input type="text" placeholder="" class="nofastclick" readonly="readonly" value="<?php echo $orderInfo['server_time'];?>分钟">
					</li>
				</ul>
				<div class="button" style="background: #f3f3f3;">
				<?php if($orderInfo['order_status'] == 5){?>
                    <a href="/index.php/Order/serverStart?order_status=6&oid=<?php echo $orderInfo['order_id']?>"><button>开始服务</button></a>
                    <?php }else if($orderInfo['order_status'] == 6){?>
                     <button onclick="serverStop('<?php echo $orderInfo['order_id']?>')">结束服务</button>
               <?php }else{?>
                    <button type="button" style="background-color:#999">开始服务</button>
                <?php }?>
                </div>
			</section>
	</body>
	<script>
		function serverStop(id){
		  layer.open({
		    content: '您确定要结束服务吗？确定后相应的金额会支付到技师的账户'
		    ,btn: ['确定', '再等等']
		    ,yes: function(index){
		      layer.close(index);
		      $.post('/index.php/Order/orderStop',{oid:id},function(data){
					if(data.code == 1){
            		    window.location.href = "/index.php/Member/subcomment?oid="+id;
						
					}
    		  },'json');
		    }
		  });
		}
	</script>
</html>
