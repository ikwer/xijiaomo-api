<?php
$config = array (	
		//应用ID,您的APPID。
		'app_id' => "2017101809366484",

		//商户私钥，您的原始格式RSA私钥
		'merchant_private_key' => "MIIEpQIBAAKCAQEA09fNeZc74rE7s+LG07YvO1KzisDPAQLG3oJhYstviXhe3UU8egRNnc+XFwrmQozlXffO2dOnXh/TQWJy2HaCO71tndK32rBH2qzA6+Hvv6xiQaDw8xv6E3GYj/6awYFhyciRd0fmv5oQUfNvvCUIqkbycK5dT4fPyAGLIGpfZCylP7qIG/6duhZ+/8LV4eoTfqxSaGa/KMwDqe5N7cFw3ftO769mK3APO+F37v+hKWPaQ4eYksPANfiN7bPy8n+kB6d+FbPqyojdyWodD8hYRg8TR7jcUPkWsC64iTQL5Lcw/pe7ojR9eNR/DADUzQAwWXnv3zhe2xfRfRQQnm4rkwIDAQABAoIBADt+HoloLHxEc9TX+iiYwHjzh9KOxVOuWPVCPmFFaqR7toCDojFOJhIMq4zgFbxY301de6z24MjxPR3erQwXdAmc8DzjYZi76uIBpAhaoj79BXcS4LgVgkyVmVfPE90GHdVSS2/pNGdYt/6b1CRNbjqLxCha/3+HT5YGwPCwjuuNj2mX3zLCWtLgedf1lnas/lu2RDxElkuRhOsIIo5YXJdZhWIY/2zwqIBndgTDwptfWVSLEnvYo4hyDvQFkFnZGfvwtI2c12lmY+ExM400uDXFJIN5kIIGBgcaPVQ/n8wJykBkVyeyqNm5Ffhn5zjilx5ZdFGB9zvldq4iVckpU7ECgYEA6Y9jX1Ndyh9gClXGqCp4P9g0D8XMluZjZEeCmtwksUjvaLNCAB/yqB09habUNKjxgo1TBQuLEZPvEmUGGNwYN3BQ9aJGbznmI+ILoq2GR2jZXM0PDMW5ER5qZSVob/d6FLZbVZRm4EqASfFoCsgcHi9BzhlEQ1H1nzvghFLBFpkCgYEA6DJDUE4YBVc09uBg0fQYuToNYdqhk7TQoRn+krkb2iOWSU2ZmFBW7HP5abe2CdPcclXn7AUj6Qz6JiTRO1z5KvPoksuCcY+U0CL4GHT55hmnkfHg59JDbcaeTkOxg8HR2iQehyEubcJL6/nevnakumX3KpeeKnLFjXac2y6aqwsCgYEAzT4DDkbdOXw+0nY0H51KlJgyj9W7BrQojXvFyr4/xEcak4BLNH7ep1sisCs9eZUovhfg56MQL383bIu4QupOoZEio+hZSu6vTcMbhHZGdMQvlrxgSFIMYn2+82lfEF2CO2dQdbD2go0VlWT9j9Pv2ZqfkjRj52DDno1oq/9ozKkCgYEA1S5gkvhAIZchb5AuFFUx5c2gv7jFJCGccmy1R+xf3/VQY9i1LhyE0e7gjOk4Xul+uhKZLh7CC5P4jtC6sO/5bDAn1a63AqA6lqWkdn/feB0RtnMGdJCdi8oRSfXoovluPANxa8tRH0CGCA+PK/st3l0Dgr1VX8+kBO9jr/Cn3GUCgYEAzlgaLfaaopFXyPbKmnzMA0R7eTGMIgTJwhWwVlr5OBYAI2z3MQu8kj0ZFCFqJWavTlxbxGOPC3uwJZxXhjf1GviJYTzlsvXlyeZeQzy3bdi+1an2QsBHQZZRPU4u+A7tdy1Rg91MONZmTkthZdVR+Q2KHLZkzhcYNn178kghVGk=",
		
		//异步通知地址
		'notify_url' => "http://m.pargo24.com/index.php/Order/notify",
		
		//同步跳转
		'return_url' => "http://m.pargo24.com/index.php/Order/notify",

		//编码格式
		'charset' => "UTF-8",

		//签名方式
		'sign_type'=>"RSA2",

		//支付宝网关
		'gatewayUrl' => "https://openapi.alipay.com/gateway.do",

		//支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
		'alipay_public_key' => "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2JxZD6CSTYkGWjUC54BA6t9VTLvTaTQHngPoEwV/sHZDT65r3GtMGNL5H/QkwSYhb7MzaGVECC6FdeQKDaxNxguSjZpC3NU8mHLLx1L4WLptjbbJeWbTE4ZhZOD7jSJ5C9UKvTTiKpbzSGRr0m3D2Ad04yUPBA2vHihGKozjrISFkDzzOplNQU2F7W/ZL2NPczD2hCJ6oCwjtIUAQJNfM2CAZR7U2j6v3eN5O4WD4cuvDlc8PkUlo+lFvqHMr1jrjsqd4eRIZg97cD/Vr/3WxmBwo0AH8BIoDTYB3N4JhIB7IONNrAp4NssRGPse1cT4hTVUV11SxgejEPryjGA7KQIDAQAB",
		
	
);