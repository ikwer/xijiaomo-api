<?php

require_once 'Autoloader.php';
use Workerman\Worker;

require_once 'Lib/Timer.php';
use Workerman\Lib\Timer;

// 心跳间隔25秒
define('HEARTBEAT_TIME', 25);

//密钥
define('API_KEYS','78ce7941a4edb427');

// 初始化一个worker容器，监听1234端口
$worker = new Worker('websocket://172.16.97.18:9501');
// 这里进程数必须设置为1
$worker->count = 1;
// worker进程启动后建立一个内部通讯端口
$worker->onWorkerStart = function($worker)
{
    Timer::add(1, function()use($worker){
        $time_now=time();
        foreach ($worker->connections as $connection)
        {
            // 有可能该connection还没收到过消息，则lastMessageTime设置为当前时间
            if (empty($connection->lastMessageTime)) {
                $connection->lastMessageTime = $time_now;
                continue;
            }
            // 上次通讯时间间隔大于心跳间隔，则认为客户端已经下线，关闭连接
            if ($time_now - $connection->lastMessageTime > HEARTBEAT_TIME) {
                
                //让用户处于停止接单的状态
                $url="http://api.xijiaomo.com/tech.php/v1/Techer/ws_offline";
                $post=array('0-tid'=>$_SESSION['uid'],'signature'=>API_KEYS);
                ksort($post);

                $a= http_build_query($post);
                $signature=sha1(md5(urldecode($a)));

                $array=array('0-tid'=>$_SESSION['uid'],'signature'=>$signature);
                curl_info($url,$array);
                $connection->close();
            }
        }
    });
    
    
    // 开启一个内部端口，方便内部系统推送数据，Text协议格式 文本+换行符
    $inner_text_worker = new Worker('Text://172.16.97.18:56789');
    $inner_text_worker->onMessage = function($connection, $buffer)
    {
        global $worker;
        // $data数组格式，里面有uid，表示向那个uid的页面推送数据
        $data = json_decode($buffer, true);
        $uid = $data['uid'];
        // 通过workerman，向uid的页面推送数据
        $ret = sendMessageByUid($uid, $buffer);
        // 返回推送结果
        $connection->send($ret ? 'ok' : 'fail');
    };
    $inner_text_worker->listen();
};
// 新增加一个属性，用来保存uid到connection的映射
$worker->uidConnections = array();
// 当有客户端发来消息时执行的回调函数
$worker->onMessage = function($connection, $data)use($worker)
{
    // 判断当前客户端是否已经验证,既是否设置了uid
    if(!isset($connection->uid))
    {
        // 没验证的话把第一个包当做uid（这里为了方便演示，没做真正的验证）
        $connection->uid = $data;
        $decode=base64_decode($data);
        if(empty($decode))
        {
            $connection->close("hello\n");
        }
        else
        {
            $explode=@explode('=',$decode);

            if(empty($explode))
            {
                $_SESSION['uid']=0;
            }
            else
            {
                $_SESSION['uid']=$explode[1];
            }
        }
        /* 保存uid到connection的映射，这样可以方便的通过uid查找connection，
         * 实现针对特定uid推送数据
         */
        $worker->uidConnections[$connection->uid] = $connection;
        return;
    }
    
};

// 当有客户端连接断开时
$worker->onClose = function($connection)use($worker)
{
    global $worker;
    if(isset($connection->uid))
    {
        // 连接断开时删除映射
        unset($worker->uidConnections[$connection->uid]);
    }
};

// 向所有验证的用户推送数据
function broadcast($message)
{
    global $worker;
    foreach($worker->uidConnections as $connection)
    {
        $connection->send($message);
    }
}

// 针对uid推送数据
function sendMessageByUid($uid, $message)
{
    global $worker;
    if(isset($worker->uidConnections[$uid]))
    {
        $connection = $worker->uidConnections[$uid];
        $connection->send($message);
        return true;
    }
    return false;
}


function curl_info($url,$post)
{
    ob_start();
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    curl_exec($ch);
    curl_close($ch);
    $stream = ob_get_contents();
    $stream = str_replace("script", "", $stream);
    ob_end_clean();
    $stream = mb_convert_encoding($stream, 'utf-8', 'GBK');

    return $stream;
}

// 运行所有的worker（其实当前只定义了一个）
Worker::runAll();