/*
Navicat MySQL Data Transfer

Source Server         : Demon
Source Server Version : 50558
Source Host           : 47.96.14.234:3306
Source Database       : xijiaomo

Target Server Type    : MYSQL
Target Server Version : 50558
File Encoding         : 65001

Date: 2019-01-21 14:37:48
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin_menu_back
-- ----------------------------
DROP TABLE IF EXISTS `admin_menu_back`;
CREATE TABLE `admin_menu_back` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `cateId` smallint(5) unsigned DEFAULT '0' COMMENT '直接上级菜单',
  `cateIds` char(16) DEFAULT '0' COMMENT '所有上级菜单，不同的ID号之间用英文逗号分隔，最后一个不要逗号',
  `level` tinyint(1) unsigned DEFAULT '1' COMMENT '级别，第几级菜单',
  `name` char(16) DEFAULT NULL COMMENT '菜单名称',
  `class` char(32) DEFAULT NULL COMMENT '类名',
  `function` char(32) DEFAULT NULL COMMENT '所属方法名',
  `sort` tinyint(2) unsigned DEFAULT '1' COMMENT '排序，数值越小，排列越前',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '是否显示，1=显示，0=不显示',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=99 DEFAULT CHARSET=utf8 COMMENT='后台菜单表';

-- ----------------------------
-- Records of admin_menu_back
-- ----------------------------
INSERT INTO `admin_menu_back` VALUES ('1', '0', '0', '1', '系统设置', 'System', 'myInfo', '1', '1');
INSERT INTO `admin_menu_back` VALUES ('2', '0', '0', '1', '全局设置', 'Global', 'adInfo', '2', '1');
INSERT INTO `admin_menu_back` VALUES ('3', '0', '0', '1', '网站管理', 'Www', 'newsCategory', '3', '1');
INSERT INTO `admin_menu_back` VALUES ('4', '1', '0,1', '2', '后台菜单管理', '', null, '1', '1');
INSERT INTO `admin_menu_back` VALUES ('5', '4', '0,1,4', '3', '添加后台菜单', 'System', 'menuInfo', '1', '1');
INSERT INTO `admin_menu_back` VALUES ('6', '4', '0,1,4', '3', '后台菜单列表', 'System', 'menuInfo', '2', '1');
INSERT INTO `admin_menu_back` VALUES ('7', '1', '0,1', '2', '后台菜单分组管理', '', '', '2', '1');
INSERT INTO `admin_menu_back` VALUES ('8', '7', '0,1,7', '3', '添加后台菜单分组', 'System', 'menuLevel', '1', '1');
INSERT INTO `admin_menu_back` VALUES ('9', '7', '0,1,7', '3', '后台菜单分组列表', 'System', 'menuLevel', '2', '1');
INSERT INTO `admin_menu_back` VALUES ('10', '1', '0,1', '2', '特殊权限管理', '', '', '4', '1');
INSERT INTO `admin_menu_back` VALUES ('11', '10', '0,1,10', '3', '添加特殊权限', 'System', 'userLevel', '1', '1');
INSERT INTO `admin_menu_back` VALUES ('12', '10', '0,1,10', '3', '特殊权限列表', 'System', 'userLevel', '2', '1');
INSERT INTO `admin_menu_back` VALUES ('13', '1', '0,1', '2', '账户管理', '', '', '6', '1');
INSERT INTO `admin_menu_back` VALUES ('14', '13', '0,1,13', '3', '添加账户', 'System', 'userInfo', '1', '1');
INSERT INTO `admin_menu_back` VALUES ('15', '13', '0,1,13', '3', '账户列表', 'System', 'userInfo', '2', '1');
INSERT INTO `admin_menu_back` VALUES ('16', '13', '0,1,13', '3', '个人信息维护', 'System', 'myInfo', '5', '1');
INSERT INTO `admin_menu_back` VALUES ('17', '13', '0,1,13', '3', '修改个人密码', 'System', 'myInfo', '6', '1');
INSERT INTO `admin_menu_back` VALUES ('18', '4', '0,1,4', '3', '编辑后台菜单', 'System', 'menuInfo', '3', '0');
INSERT INTO `admin_menu_back` VALUES ('19', '4', '0,1,4', '3', '删除后台菜单', 'System', 'menuInfo', '4', '0');
INSERT INTO `admin_menu_back` VALUES ('20', '7', '0,1,7', '3', '编辑后台菜单分组', 'System', 'menulevel', '3', '0');
INSERT INTO `admin_menu_back` VALUES ('21', '7', '0,1,7', '3', '删除后台菜单分组', 'System', 'menuLevel', '4', '0');
INSERT INTO `admin_menu_back` VALUES ('22', '10', '0,1,10', '3', '编辑特殊权限', 'System', 'userLevel', '3', '0');
INSERT INTO `admin_menu_back` VALUES ('23', '10', '0,1,10', '3', '删除特殊权限', 'System', 'userLevel', '4', '0');
INSERT INTO `admin_menu_back` VALUES ('24', '1', '0,1', '2', '特殊权限类别管理', '', '', '3', '1');
INSERT INTO `admin_menu_back` VALUES ('25', '24', '0,1,24', '3', '添加特殊权限类别', 'System', 'userLevelCategory', '1', '1');
INSERT INTO `admin_menu_back` VALUES ('26', '24', '0,1,24', '3', '特殊权限类别列表', 'System', 'userLevelCategory', '2', '1');
INSERT INTO `admin_menu_back` VALUES ('27', '24', '0,1,24', '3', '编辑特殊权限', 'System', 'userLevelCategory', '3', '0');
INSERT INTO `admin_menu_back` VALUES ('28', '24', '0,1,24', '3', '删除特殊权限类别', 'System', 'userLevelCategory', '4', '0');
INSERT INTO `admin_menu_back` VALUES ('29', '1', '0,1', '2', '特殊权限分组管理', '', '', '5', '1');
INSERT INTO `admin_menu_back` VALUES ('30', '29', '0,1,29', '3', '添加特殊权限分组', 'System', 'userLevelClass', '1', '1');
INSERT INTO `admin_menu_back` VALUES ('31', '29', '0,1,29', '3', '特殊权限分组列表', 'System', 'userLevelClass', '2', '1');
INSERT INTO `admin_menu_back` VALUES ('32', '29', '0,1,29', '3', '编辑特殊权限分组', 'System', 'userLevelClass', '3', '0');
INSERT INTO `admin_menu_back` VALUES ('33', '29', '0,1,29', '3', '删除特殊权限分组', 'System', 'userLevelClass', '4', '0');
INSERT INTO `admin_menu_back` VALUES ('34', '2', '0,2', '2', '广告分类管理', '', '', '1', '1');
INSERT INTO `admin_menu_back` VALUES ('35', '34', '0,2,34', '3', '添加广告分类', 'Global', 'adCategory', '1', '1');
INSERT INTO `admin_menu_back` VALUES ('36', '34', '0,2,34', '3', '广告分类列表', 'Global', 'adCategory', '2', '1');
INSERT INTO `admin_menu_back` VALUES ('37', '34', '0,2,34', '3', '编辑广告分类', 'Global', 'adCategory', '3', '0');
INSERT INTO `admin_menu_back` VALUES ('38', '34', '0,2,34', '3', '删除广告分类', 'Global', 'adCategory', '4', '0');
INSERT INTO `admin_menu_back` VALUES ('39', '2', '0,2', '2', '广告位管理', '', '', '2', '1');
INSERT INTO `admin_menu_back` VALUES ('40', '39', '0,2,39', '3', '添加广告位', 'Global', 'adInfo', '1', '1');
INSERT INTO `admin_menu_back` VALUES ('41', '39', '0,2,39', '3', '广告位列表', 'Global', 'adInfo', '2', '1');
INSERT INTO `admin_menu_back` VALUES ('42', '39', '0,2,39', '3', '编辑广告位', 'global', 'adInfo', '3', '0');
INSERT INTO `admin_menu_back` VALUES ('43', '39', '0,2,39', '3', '删除广告位', 'Global', 'adInfo', '4', '0');
INSERT INTO `admin_menu_back` VALUES ('44', '2', '0,2', '2', '网站SEO优化管理', '', '', '3', '1');
INSERT INTO `admin_menu_back` VALUES ('45', '44', '0,2,44', '3', '添加页面头SEO信息', 'Global', 'seo', '1', '1');
INSERT INTO `admin_menu_back` VALUES ('46', '44', '0,2,44', '3', '页头SEO信息列表', 'Global', 'seo', '2', '1');
INSERT INTO `admin_menu_back` VALUES ('47', '44', '0,2,44', '3', '编辑页头SEO信息', 'Global', 'seo', '3', '0');
INSERT INTO `admin_menu_back` VALUES ('48', '44', '0,2,44', '3', '删除页头SEO信息', 'Global', 'seo', '4', '0');
INSERT INTO `admin_menu_back` VALUES ('49', '2', '0,2', '2', 'SMTP邮件发送设置', '', '', '4', '1');
INSERT INTO `admin_menu_back` VALUES ('50', '49', '0,2,49', '3', '添加SMTP信息', 'Global', 'smtpInfo', '1', '1');
INSERT INTO `admin_menu_back` VALUES ('51', '49', '0,2,49', '3', 'SMTP设置列表', 'Global', 'smtpInfo', '2', '1');
INSERT INTO `admin_menu_back` VALUES ('52', '49', '0,2,49', '3', '编辑SMTP信息', 'Global', 'smtpInfo', '3', '0');
INSERT INTO `admin_menu_back` VALUES ('53', '49', '0,2,49', '3', '删除SMTP信息', 'Global', 'smtpInfo', '4', '0');
INSERT INTO `admin_menu_back` VALUES ('54', '3', '0,3', '2', '网站信息管理', '', '', '7', '1');
INSERT INTO `admin_menu_back` VALUES ('56', '54', '0,3,54', '3', '网站优化管理', 'Global', 'cacheInfo', '2', '1');
INSERT INTO `admin_menu_back` VALUES ('57', '2', '0,2', '2', '网站日志管理', '', '', '7', '1');
INSERT INTO `admin_menu_back` VALUES ('58', '57', '0,2,57', '3', '操作日志列表', 'Global', 'logsInfo', '1', '1');
INSERT INTO `admin_menu_back` VALUES ('59', '57', '0,2,57', '3', '删除日志信息', 'Global', 'logsInfo', '2', '0');
INSERT INTO `admin_menu_back` VALUES ('94', '3', '0,3', '2', '资讯分类管理', '', '', '1', '1');
INSERT INTO `admin_menu_back` VALUES ('95', '94', '0,3,94', '3', '添加资讯分类', 'Www', 'newsCategory', '1', '1');
INSERT INTO `admin_menu_back` VALUES ('96', '94', '0,3,94', '3', '资讯分类列表', 'Www', 'newsCategory', '2', '1');
INSERT INTO `admin_menu_back` VALUES ('97', '94', '0,3,94', '3', '编辑资讯分类', 'Www', 'newsCategory', '3', '0');
INSERT INTO `admin_menu_back` VALUES ('98', '94', '0,3,94', '3', '删除资讯分类', 'Www', 'newsCategory', '4', '0');

-- ----------------------------
-- Table structure for xjm_account_log
-- ----------------------------
DROP TABLE IF EXISTS `xjm_account_log`;
CREATE TABLE `xjm_account_log` (
  `log_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '日志id',
  `uid` int(11) unsigned NOT NULL COMMENT '用户id',
  `user_money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '用户金额',
  `pay_couponid` int(10) NOT NULL COMMENT '支付优惠券',
  `pay_type` tinyint(1) NOT NULL COMMENT '支付类型 1：微信 2：支付宝 3：余额',
  `order_id` int(10) DEFAULT NULL COMMENT '订单id',
  `addtime` int(10) unsigned NOT NULL COMMENT '变动时间',
  PRIMARY KEY (`log_id`),
  KEY `user_id` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单消费明细表';

-- ----------------------------
-- Records of xjm_account_log
-- ----------------------------

-- ----------------------------
-- Table structure for xjm_account_recharge
-- ----------------------------
DROP TABLE IF EXISTS `xjm_account_recharge`;
CREATE TABLE `xjm_account_recharge` (
  `re_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '日志id',
  `re_sn` varchar(100) DEFAULT NULL COMMENT '充值单号',
  `re_type` tinyint(1) NOT NULL COMMENT '充值类型 1：微信 2：支付宝',
  `uid` int(11) unsigned NOT NULL COMMENT '用户id',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '充值金额',
  `addtime` int(10) unsigned NOT NULL COMMENT '变动时间',
  PRIMARY KEY (`re_id`),
  KEY `user_id` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';

-- ----------------------------
-- Records of xjm_account_recharge
-- ----------------------------

-- ----------------------------
-- Table structure for xjm_admin_menu
-- ----------------------------
DROP TABLE IF EXISTS `xjm_admin_menu`;
CREATE TABLE `xjm_admin_menu` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `cate_id` smallint(5) unsigned DEFAULT '0' COMMENT '直接上级菜单',
  `cate_ids` char(16) DEFAULT '0' COMMENT '所有上级菜单，不同的ID号之间用英文逗号分隔，最后一个不要逗号',
  `level` tinyint(1) unsigned DEFAULT '1' COMMENT '级别，第几级菜单',
  `name` char(16) DEFAULT NULL COMMENT '菜单名称',
  `class` char(32) DEFAULT NULL COMMENT '类名',
  `function` char(32) DEFAULT NULL COMMENT '所属方法名',
  `sort` tinyint(2) unsigned DEFAULT '1' COMMENT '排序，数值越小，排列越前',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '是否显示，1=显示，0=不显示',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COMMENT='后台菜单表';

-- ----------------------------
-- Records of xjm_admin_menu
-- ----------------------------
INSERT INTO `xjm_admin_menu` VALUES ('1', '0', '0', '1', '控制台', 'Main', 'index', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('2', '0', '0', '1', '管理员', null, null, '2', '1');
INSERT INTO `xjm_admin_menu` VALUES ('3', '2', '0,2', '2', '管理员列表', 'Admin', 'user_list', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('4', '2', '0,2,3', '3', '修改管理员信息', 'Admin', 'edit_user', '2', '1');
INSERT INTO `xjm_admin_menu` VALUES ('5', '0', '0', '1', '用户管理', null, null, '3', '1');
INSERT INTO `xjm_admin_menu` VALUES ('6', '5', '0,5', '2', '用户列表', 'User', 'index', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('7', '2', '0,2,3', '2', '添加管理员', 'Admin', 'add_user', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('8', '2', '0,2,3', '3', '删除管理员', 'Admin', 'del_user', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('9', '2', '0,2', '2', '管理区域', null, null, '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('10', '0', '0', '1', '产品管理', 'Goods', null, '4', '1');
INSERT INTO `xjm_admin_menu` VALUES ('11', '10', '0,10', '2', '产品分类', 'Cate', 'index', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('12', '10', '0,10', '2', '产品列表', 'Goods', 'index', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('13', '10', '0,10,11', '3', '添加分类', 'Cate', 'add', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('14', '10', '0,10,11', '3', '修改分类', 'Cate', 'edit', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('15', '10', '0,10,12', '3', '添加产品', 'Goods', 'add', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('16', '10', '0,10,12', '3', '修改产品', 'Goods', 'edit', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('17', '0', '0', '1', '订单管理', 'Order', '', '5', '1');
INSERT INTO `xjm_admin_menu` VALUES ('18', '17', '0,17', '2', '订单列表', 'Order', 'index', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('19', '10', '0,10,12', '3', '删除产品', 'Goods', 'del', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('20', '0', '0', '1', '技师管理', 'Techer', 'techer', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('21', '20', '0,20', '2', '技师列表', 'Techer', 'techer_list', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('22', '20', '0,20,21', '3', '查看详情', 'Techer', 'techer_info', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('23', '20', '0,20,21', '3', '添加技师', 'Techer', 'techer_add', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('24', '20', '0,20,21', '3', '技师审核', 'Techer', 'passfn', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('25', '20', '0,20,21', '3', '删除技师', 'Techer', 'techer_del', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('26', '0', '0', '1', '文章管理', '', null, '5', '1');
INSERT INTO `xjm_admin_menu` VALUES ('27', '26', '0,26', '2', '文章列表', 'Article', 'index', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('28', '26', '0,26,27', '2', '文章添加', 'Article', 'add', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('29', '17', '0,17', '2', '提现列表', 'order', 'cash_list', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('30', '17', '0,17,29', '3', '提现详情', 'order', 'cash_info', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('31', '17', '0,17,29,30', '4', '提现通过', 'order', 'cash_success', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('32', '17', '0,17,29,30', '4', '提现驳回', 'order', 'cash_fail', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('33', '26', '0,26,27,28', '3', '清空缓存', 'Article', 'flush_cache', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('34', '5', '0,5', '2', '用户反馈', 'User', 'feedback', '2', '1');
INSERT INTO `xjm_admin_menu` VALUES ('35', '5', '0,5,34', '3', '用户反馈详情', 'User', 'feedback_info', '3', '1');
INSERT INTO `xjm_admin_menu` VALUES ('36', '20', '0,20', '2', '技师反馈', 'Techer', 'feedback', '2', '1');
INSERT INTO `xjm_admin_menu` VALUES ('37', '20', '0,20,36', '3', '技师反馈详情', 'Techer', 'feedback_info', '3', '1');
INSERT INTO `xjm_admin_menu` VALUES ('38', '20', '0,20,21', '3', '技师身份验证', 'Techer', 'check_idcard', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('39', '0', '0', '1', '运营行为', null, null, '7', '1');
INSERT INTO `xjm_admin_menu` VALUES ('40', '39', '0,39', '2', '日新增用户+技师', 'User', 'everyday_user', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('41', '39', '0,39', '2', '用户比例', 'User', 'fenbu_user', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('42', '17', '0,17', '2', '退款列表', 'order', 'refund_list', '3', '1');
INSERT INTO `xjm_admin_menu` VALUES ('43', '1', '0,1,43', '3', '文件上传', 'Main', 'upload', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('44', '39', '0,39', '2', '成交订单走势', 'order', 'order_trend', '3', '1');
INSERT INTO `xjm_admin_menu` VALUES ('45', '2', '0,2', '2', '权限组设置', 'Admin', 'aul_config', '3', '1');
INSERT INTO `xjm_admin_menu` VALUES ('46', '2', '0,2,45', '3', '权限设置', 'Admin', 'aul_option', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('47', '10', '0,10,12', '3', '删除分类', 'Cate', 'del', '4', '1');
INSERT INTO `xjm_admin_menu` VALUES ('48', '2', '0,2', '2', '城市设置', 'Admin', 'city_list', '4', '1');
INSERT INTO `xjm_admin_menu` VALUES ('49', '2', '0,2,48', '4', '城市勾选', 'Admin', 'city_config', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('50', '17', '0,17', '2', '退单列表', 'Order', 'tuidan_list', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('51', '0', '0', '1', '其他设置', null, null, '8', '1');
INSERT INTO `xjm_admin_menu` VALUES ('52', '51', '0,51', '2', '违禁词管理', 'User', 'warning_msg', '1', '1');
INSERT INTO `xjm_admin_menu` VALUES ('53', '20', '0,20', '2', '添加投诉', 'Techer', 'add_tousu', '5', '1');
INSERT INTO `xjm_admin_menu` VALUES ('54', '20', '0,20', '2', '投诉列表', 'Techer', 'tousu_list', '4', '1');

-- ----------------------------
-- Table structure for xjm_admin_menu_level
-- ----------------------------
DROP TABLE IF EXISTS `xjm_admin_menu_level`;
CREATE TABLE `xjm_admin_menu_level` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` char(16) DEFAULT NULL COMMENT '角色名称',
  `menuIds` text COMMENT '菜单ID，用英文逗号分隔，最后一个字符不能是逗号',
  `sort` smallint(5) unsigned DEFAULT '1' COMMENT '排序，数值越小越靠前排列',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='后台菜单权限表';

-- ----------------------------
-- Records of xjm_admin_menu_level
-- ----------------------------
INSERT INTO `xjm_admin_menu_level` VALUES ('1', '系统管理员', '1,2,3,4,5,6,7,8,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54', '1');
INSERT INTO `xjm_admin_menu_level` VALUES ('3', '运营', '1,2', '1');
INSERT INTO `xjm_admin_menu_level` VALUES ('2', '编辑', '1', '1');
INSERT INTO `xjm_admin_menu_level` VALUES ('4', '测试', '1', '1');

-- ----------------------------
-- Table structure for xjm_admin_user
-- ----------------------------
DROP TABLE IF EXISTS `xjm_admin_user`;
CREATE TABLE `xjm_admin_user` (
  `uid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `login_name` varchar(100) NOT NULL DEFAULT '',
  `pwd` varchar(100) NOT NULL DEFAULT '',
  `menu_leval` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '用户权限等级',
  `area` varchar(255) DEFAULT '' COMMENT '辖区城市',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '用户状态，0=无效，不能登录，1=有效',
  `time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `login_name` (`login_name`) USING HASH,
  UNIQUE KEY `uid` (`uid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='后台管理员表';

-- ----------------------------
-- Records of xjm_admin_user
-- ----------------------------
INSERT INTO `xjm_admin_user` VALUES ('1', 'heyiwei', 'ebb027b48e5f7086b4c12a9d3db05a0897126edc', '1', '1,2', '1', '1');
INSERT INTO `xjm_admin_user` VALUES ('2', 'bianji', 'f122db007ed655921f98184e4302bba84990ff68', '3', '1,2', '1', '2');
INSERT INTO `xjm_admin_user` VALUES ('3', 'admin2311', '0f0d74bb0eeb3b80ae3b0f8ee357717a9a84e7e7', '2', '1', '0', '3');
INSERT INTO `xjm_admin_user` VALUES ('4', 'admin234', '0f0d74bb0eeb3b80ae3b0f8ee357717a9a84e7e7', '3', null, '1', '3');
INSERT INTO `xjm_admin_user` VALUES ('5', 'admin2345', '0f0d74bb0eeb3b80ae3b0f8ee357717a9a84e7e7', '3', null, '1', '3');
INSERT INTO `xjm_admin_user` VALUES ('6', 'xym891011', 'ebb027b48e5f7086b4c12a9d3db05a0897126edc', '3', '1', '0', '45');
INSERT INTO `xjm_admin_user` VALUES ('7', 'xym8910', 'ebb027b48e5f7086b4c12a9d3db05a0897126edc', '1', '1', '1', '1504516195');
INSERT INTO `xjm_admin_user` VALUES ('8', 'admin22222', 'f95e66bcea2f77292f8df5f3fde91c574e5f3dc5', '2', null, '1', '1504516265');
INSERT INTO `xjm_admin_user` VALUES ('13', 'xv342323', '31be508787c00b3ef8fb36c38509a4cf9afe8ea1', '1', '', '1', '1504525650');
INSERT INTO `xjm_admin_user` VALUES ('14', 'fdfg32424', '4350ca945a395a7474a45c5d7e11fe4c6ccd723c', '1', null, '1', '1504525778');
INSERT INTO `xjm_admin_user` VALUES ('15', 'sdf342342', '17f98b566bb465d327f1875b1bcb6de7d2d94d1b', '1', '1,2', '1', '1504525794');
INSERT INTO `xjm_admin_user` VALUES ('16', 'huangnan', 'f122db007ed655921f98184e4302bba84990ff68', '1', '1,2', '1', '1');

-- ----------------------------
-- Table structure for xjm_api_log
-- ----------------------------
DROP TABLE IF EXISTS `xjm_api_log`;
CREATE TABLE `xjm_api_log` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `api` varchar(255) DEFAULT NULL,
  `param` text,
  `dateline` int(11) DEFAULT NULL,
  `sign` varchar(255) DEFAULT NULL,
  `rightsign` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xjm_api_log
-- ----------------------------
INSERT INTO `xjm_api_log` VALUES ('6', 'techer/set_pwd', '{\"0-account\":\"18571710546\",\"1-pwd\":\"123456\",\"2-re_pwd\":\"123456\",\"3-code_id\":\"1781\",\"4-code\":\"9197\",\"5-version\":\"1.0.0\",\"6-source\":\"ios\",\"signature\":\"dd105cf1b6e7a438dd73c6b61938ade94da3d5b3\"}', '1546669779', 'dd105cf1b6e7a438dd73c6b61938ade94da3d5b3', 'dd105cf1b6e7a438dd73c6b61938ade94da3d5b3');
INSERT INTO `xjm_api_log` VALUES ('7', 'techer/set_pwd', '{\"0-account\":\"18521598723\",\"signature\":\"5b00bde71496868a98e05dca71bacf2c152c8f77\",\"6-source\":\"android\",\"4-code\":\"8347\",\"3-code_id\":\"1782\",\"2-re_pwd\":\"95962626256f2b76f837ea7dfc4abd39\",\"5-version\":\"1.0.0\",\"1-pwd\":\"95962626256f2b76f837ea7dfc4abd39\"}', '1546669838', '5b00bde71496868a98e05dca71bacf2c152c8f77', '21aa28508ea0858f5c30ccfe353558e45dfe8db5');
INSERT INTO `xjm_api_log` VALUES ('8', 'techer/set_pwd', '{\"0-account\":\"18521598723\",\"signature\":\"5b00bde71496868a98e05dca71bacf2c152c8f77\",\"6-source\":\"android\",\"4-code\":\"9061\",\"3-code_id\":\"1783\",\"2-re_pwd\":\"95962626256f2b76f837ea7dfc4abd39\",\"5-version\":\"1.0.0\",\"1-pwd\":\"95962626256f2b76f837ea7dfc4abd39\"}', '1546670454', '5b00bde71496868a98e05dca71bacf2c152c8f77', '741bd4f45a77f501ae742aa3218bd312060139b7');
INSERT INTO `xjm_api_log` VALUES ('9', 'techer/set_pwd', '{\"0-account\":\"18521598723\",\"signature\":\"5b00bde71496868a98e05dca71bacf2c152c8f77\",\"6-source\":\"android\",\"4-code\":\"9061\",\"3-code_id\":\"1783\",\"2-re_pwd\":\"95962626256f2b76f837ea7dfc4abd39\",\"5-version\":\"1.0.0\",\"1-pwd\":\"95962626256f2b76f837ea7dfc4abd39\"}', '1546670467', '5b00bde71496868a98e05dca71bacf2c152c8f77', '741bd4f45a77f501ae742aa3218bd312060139b7');
INSERT INTO `xjm_api_log` VALUES ('10', 'techer/set_pwd', '{\"0-account\":\"18521598723\",\"signature\":\"5b00bde71496868a98e05dca71bacf2c152c8f77\",\"6-source\":\"android\",\"4-code\":\"9061\",\"3-code_id\":\"1783\",\"2-re_pwd\":\"80c45c05c1502bac6293e00974bd13b5\",\"5-version\":\"1.0.0\",\"1-pwd\":\"80c45c05c1502bac6293e00974bd13b5\"}', '1546670564', '5b00bde71496868a98e05dca71bacf2c152c8f77', 'daf4d6f9852deb1dd49cebe6da5f6fa724f4d347');
INSERT INTO `xjm_api_log` VALUES ('11', 'techer/set_pwd', '{\"0-account\":\"18521598723\",\"signature\":\"5b00bde71496868a98e05dca71bacf2c152c8f77\",\"6-source\":\"android\",\"4-code\":\"4487\",\"3-code_id\":\"1785\",\"2-re_pwd\":\"d6894682e5b40c57c009fa5c4507947f\",\"5-version\":\"1.0.0\",\"1-pwd\":\"d6894682e5b40c57c009fa5c4507947f\"}', '1546670774', '5b00bde71496868a98e05dca71bacf2c152c8f77', 'dc7898de3ee448ef9e421fcd530bfab06dce9257');
INSERT INTO `xjm_api_log` VALUES ('12', 'techer/set_pwd', '{\"0-account\":\"18521598723\",\"signature\":\"5b00bde71496868a98e05dca71bacf2c152c8f77\",\"6-source\":\"android\",\"4-code\":\"4487\",\"3-code_id\":\"1785\",\"2-re_pwd\":\"d6894682e5b40c57c009fa5c4507947f\",\"5-version\":\"1.0.0\",\"1-pwd\":\"d6894682e5b40c57c009fa5c4507947f\"}', '1546670805', '5b00bde71496868a98e05dca71bacf2c152c8f77', 'dc7898de3ee448ef9e421fcd530bfab06dce9257');
INSERT INTO `xjm_api_log` VALUES ('13', 'techer/set_pwd', '{\"0-account\":\"18062394372\",\"1-pwd\":\"123456\",\"2-re_pwd\":\"123456\",\"3-code_id\":\"1786\",\"4-code\":\"8079 \",\"5-version\":\"1.0.0\",\"6-source\":\"ios\",\"signature\":\"84205706eb8114f562b241aacf7047bc96b486e3\"}', '1546670861', '84205706eb8114f562b241aacf7047bc96b486e3', '84205706eb8114f562b241aacf7047bc96b486e3');
INSERT INTO `xjm_api_log` VALUES ('14', 'techer/set_pwd', '{\"0-account\":\"18694065710\",\"1-pwd\":\"hhh123321\",\"2-re_pwd\":\"hhh123321\",\"3-code_id\":\"0\",\"4-code\":\"4338\",\"5-version\":\"1.0.0\",\"6-source\":\"ios\",\"signature\":\"02d0931412fb8de414abffebcba7dad816956f6c\"}', '1546670971', '02d0931412fb8de414abffebcba7dad816956f6c', '44102bea160c88fd477201ccc18696bd9960808d');
INSERT INTO `xjm_api_log` VALUES ('15', 'techer/set_pwd', '{\"0-account\":\"18694065710\",\"1-pwd\":\"hhh123321\",\"2-re_pwd\":\"hhh123321\",\"3-code_id\":\"0\",\"4-code\":\"4338\",\"5-version\":\"1.0.0\",\"6-source\":\"ios\",\"signature\":\"02d0931412fb8de414abffebcba7dad816956f6c\"}', '1546670976', '02d0931412fb8de414abffebcba7dad816956f6c', '44102bea160c88fd477201ccc18696bd9960808d');
INSERT INTO `xjm_api_log` VALUES ('16', 'techer/set_pwd', '{\"0-account\":\"18694065710\",\"1-pwd\":\"hhh123321\",\"2-re_pwd\":\"hhh123321\",\"3-code_id\":\"0\",\"4-code\":\"4338\",\"5-version\":\"1.0.0\",\"6-source\":\"ios\",\"signature\":\"02d0931412fb8de414abffebcba7dad816956f6c\"}', '1546670979', '02d0931412fb8de414abffebcba7dad816956f6c', '44102bea160c88fd477201ccc18696bd9960808d');
INSERT INTO `xjm_api_log` VALUES ('17', 'techer/set_pwd', '{\"0-account\":\"18694065710\",\"1-pwd\":\"hhh123321\",\"2-re_pwd\":\"hhh123321\",\"3-code_id\":\"0\",\"4-code\":\"4338\",\"5-version\":\"1.0.0\",\"6-source\":\"ios\",\"signature\":\"02d0931412fb8de414abffebcba7dad816956f6c\"}', '1546670984', '02d0931412fb8de414abffebcba7dad816956f6c', '44102bea160c88fd477201ccc18696bd9960808d');
INSERT INTO `xjm_api_log` VALUES ('18', 'techer/set_pwd', '{\"0-account\":\"18062394372\",\"1-pwd\":\"123456\",\"2-re_pwd\":\"123456\",\"3-code_id\":\"1787\",\"4-code\":\"1128\",\"5-version\":\"1.0.0\",\"6-source\":\"ios\",\"signature\":\"034b3fe94b1021c436c179e4caa339ef5fdec869\"}', '1546670999', '034b3fe94b1021c436c179e4caa339ef5fdec869', '034b3fe94b1021c436c179e4caa339ef5fdec869');
INSERT INTO `xjm_api_log` VALUES ('19', 'techer/set_pwd', '{\"0-account\":\"18062394372\",\"1-pwd\":\"123456\",\"2-re_pwd\":\"123456\",\"3-code_id\":\"1787\",\"4-code\":\"1128\",\"5-version\":\"1.0.0\",\"6-source\":\"ios\",\"signature\":\"034b3fe94b1021c436c179e4caa339ef5fdec869\"}', '1546671000', '034b3fe94b1021c436c179e4caa339ef5fdec869', '034b3fe94b1021c436c179e4caa339ef5fdec869');
INSERT INTO `xjm_api_log` VALUES ('20', 'techer/set_pwd', '{\"0-account\":\"18062394372\",\"1-pwd\":\"123456\",\"2-re_pwd\":\"123456\",\"3-code_id\":\"1787\",\"4-code\":\"1128\",\"5-version\":\"1.0.0\",\"6-source\":\"ios\",\"signature\":\"034b3fe94b1021c436c179e4caa339ef5fdec869\"}', '1546671001', '034b3fe94b1021c436c179e4caa339ef5fdec869', '034b3fe94b1021c436c179e4caa339ef5fdec869');
INSERT INTO `xjm_api_log` VALUES ('21', 'techer/set_pwd', '{\"0-account\":\"18062394372\",\"1-pwd\":\"123456\",\"2-re_pwd\":\"123456\",\"3-code_id\":\"1787\",\"4-code\":\"1128\",\"5-version\":\"1.0.0\",\"6-source\":\"ios\",\"signature\":\"034b3fe94b1021c436c179e4caa339ef5fdec869\"}', '1546671001', '034b3fe94b1021c436c179e4caa339ef5fdec869', '034b3fe94b1021c436c179e4caa339ef5fdec869');
INSERT INTO `xjm_api_log` VALUES ('22', 'techer/set_pwd', '{\"0-account\":\"18062394372\",\"1-pwd\":\"123456\",\"2-re_pwd\":\"123456\",\"3-code_id\":\"1787\",\"4-code\":\"1128\",\"5-version\":\"1.0.0\",\"6-source\":\"ios\",\"signature\":\"034b3fe94b1021c436c179e4caa339ef5fdec869\"}', '1546671001', '034b3fe94b1021c436c179e4caa339ef5fdec869', '034b3fe94b1021c436c179e4caa339ef5fdec869');
INSERT INTO `xjm_api_log` VALUES ('23', 'techer/set_pwd', '{\"0-account\":\"18062394372\",\"1-pwd\":\"123456\",\"2-re_pwd\":\"123456\",\"3-code_id\":\"1787\",\"4-code\":\"1128\",\"5-version\":\"1.0.0\",\"6-source\":\"ios\",\"signature\":\"034b3fe94b1021c436c179e4caa339ef5fdec869\"}', '1546671001', '034b3fe94b1021c436c179e4caa339ef5fdec869', '034b3fe94b1021c436c179e4caa339ef5fdec869');
INSERT INTO `xjm_api_log` VALUES ('24', 'techer/set_pwd', '{\"0-account\":\"18062394372\",\"1-pwd\":\"123456\",\"2-re_pwd\":\"123456\",\"3-code_id\":\"1787\",\"4-code\":\"1128\",\"5-version\":\"1.0.0\",\"6-source\":\"ios\",\"signature\":\"034b3fe94b1021c436c179e4caa339ef5fdec869\"}', '1546671001', '034b3fe94b1021c436c179e4caa339ef5fdec869', '034b3fe94b1021c436c179e4caa339ef5fdec869');
INSERT INTO `xjm_api_log` VALUES ('25', 'techer/set_pwd', '{\"0-account\":\"18062394372\",\"1-pwd\":\"123456\",\"2-re_pwd\":\"123456\",\"3-code_id\":\"1787\",\"4-code\":\"1128\",\"5-version\":\"1.0.0\",\"6-source\":\"ios\",\"signature\":\"034b3fe94b1021c436c179e4caa339ef5fdec869\"}', '1546671001', '034b3fe94b1021c436c179e4caa339ef5fdec869', '034b3fe94b1021c436c179e4caa339ef5fdec869');
INSERT INTO `xjm_api_log` VALUES ('26', 'techer/set_pwd', '{\"0-account\":\"18694065710\",\"1-pwd\":\"hhh123321\",\"2-re_pwd\":\"hhh123321\",\"3-code_id\":\"1788\",\"4-code\":\"9021\",\"5-version\":\"1.0.0\",\"6-source\":\"ios\",\"signature\":\"6d2bdb45aa23ae7b2da071d8f0865724a17a62ef\"}', '1546671001', '6d2bdb45aa23ae7b2da071d8f0865724a17a62ef', '6d2bdb45aa23ae7b2da071d8f0865724a17a62ef');
INSERT INTO `xjm_api_log` VALUES ('27', 'techer/set_pwd', '{\"0-account\":\"18062394372\",\"1-pwd\":\"123456\",\"2-re_pwd\":\"123456\",\"3-code_id\":\"1787\",\"4-code\":\"1128\",\"5-version\":\"1.0.0\",\"6-source\":\"ios\",\"signature\":\"034b3fe94b1021c436c179e4caa339ef5fdec869\"}', '1546671002', '034b3fe94b1021c436c179e4caa339ef5fdec869', '034b3fe94b1021c436c179e4caa339ef5fdec869');
INSERT INTO `xjm_api_log` VALUES ('28', 'techer/set_pwd', '{\"0-account\":\"18521598723\",\"signature\":\"5b00bde71496868a98e05dca71bacf2c152c8f77\",\"6-source\":\"android\",\"4-code\":\"2320\",\"3-code_id\":\"1789\",\"2-re_pwd\":\"hl123456\",\"5-version\":\"1.0.0\",\"1-pwd\":\"hl123456\"}', '1546671026', '5b00bde71496868a98e05dca71bacf2c152c8f77', '2939072f38ccd442623d59b47101e714fdc32e4e');
INSERT INTO `xjm_api_log` VALUES ('29', 'techer/set_pwd', '{\"0-account\":\"18521598723\",\"signature\":\"5b00bde71496868a98e05dca71bacf2c152c8f77\",\"6-source\":\"android\",\"4-code\":\"7880\",\"3-code_id\":\"1790\",\"2-re_pwd\":\"hl123456\",\"5-version\":\"1.0.0\",\"1-pwd\":\"hl123456\"}', '1546671174', '5b00bde71496868a98e05dca71bacf2c152c8f77', '0570553df4a5fac86d20c4573a38e62afe8fed84');
INSERT INTO `xjm_api_log` VALUES ('30', 'techer/set_pwd', '{\"0-account\":\"18521598723\",\"signature\":\"e6b1b96d82f9a677e42ca29131f86297b54cd96d\",\"6-source\":\"android\",\"4-code\":\"3761\",\"3-code_id\":\"1791\",\"2-re_pwd\":\"95962626256f2b76f837ea7dfc4abd39\",\"5-version\":\"1.0.0\",\"1-pwd\":\"95962626256f2b76f837ea7dfc4abd39\"}', '1546671446', 'e6b1b96d82f9a677e42ca29131f86297b54cd96d', 'e6b1b96d82f9a677e42ca29131f86297b54cd96d');
INSERT INTO `xjm_api_log` VALUES ('31', 'techer/set_pwd', '{\"0-account\":\"18521598723\",\"signature\":\"acc03640d77e0cecd54eb1d781cfabbef2f2f55e\",\"6-source\":\"android\",\"4-code\":\"3716\",\"3-code_id\":\"1791\",\"2-re_pwd\":\"95962626256f2b76f837ea7dfc4abd39\",\"5-version\":\"1.0.0\",\"1-pwd\":\"95962626256f2b76f837ea7dfc4abd39\"}', '1546671454', 'acc03640d77e0cecd54eb1d781cfabbef2f2f55e', 'acc03640d77e0cecd54eb1d781cfabbef2f2f55e');

-- ----------------------------
-- Table structure for xjm_area_list
-- ----------------------------
DROP TABLE IF EXISTS `xjm_area_list`;
CREATE TABLE `xjm_area_list` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `area_name` varchar(50) NOT NULL DEFAULT '' COMMENT '区域名',
  `time` int(11) unsigned NOT NULL COMMENT '添加时间',
  `open` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否开放',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='区域列表';

-- ----------------------------
-- Records of xjm_area_list
-- ----------------------------
INSERT INTO `xjm_area_list` VALUES ('1', '武汉', '5', '1');
INSERT INTO `xjm_area_list` VALUES ('2', '襄阳', '6', '1');
INSERT INTO `xjm_area_list` VALUES ('3', '湖北省武汉市', '0', '0');

-- ----------------------------
-- Table structure for xjm_article
-- ----------------------------
DROP TABLE IF EXISTS `xjm_article`;
CREATE TABLE `xjm_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `type` tinyint(1) DEFAULT NULL COMMENT '类型',
  `status` tinyint(1) DEFAULT '0' COMMENT '状态，0显示，1不显示',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `add_time` int(11) DEFAULT NULL COMMENT '添加时间',
  `content` text COMMENT '正文',
  `set_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `sort_order` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='文章表';

-- ----------------------------
-- Records of xjm_article
-- ----------------------------
INSERT INTO `xjm_article` VALUES ('1', '为什么不能体现', '1', '0', 'Q1', '1507627108', '为什么不能提现，本平台的提现规则是每周二才可以提交', '1508204731', '1');
INSERT INTO `xjm_article` VALUES ('2', '提现后五日没到帐', '1', '0', 'Q2', null, '一般提现后1-3个工作日会到账，如果显示未到账。。', '1509862769', '2');
INSERT INTO `xjm_article` VALUES ('3', '测试标题', '2', '1', '测试简介', '1508203425', '我的天', '1508205486', '3');
INSERT INTO `xjm_article` VALUES ('4', '技师注册协议', '6', '0', '技师注册协议', '1508227740', '<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;\">\r\n	<b>“</b><b>喜脚么</b><b>”</b><b>技师</b><b>服务平台使用协议</b><b></b> \r\n</p>\r\n<p class=\"MsoNormal\">\r\n	<b>提示条款：</b><b></b> \r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	<span>本《喜脚么技师注册协议》是您（下称</span>\"用户\"或\"您\"，是指通过喜脚么（武汉）网络科技有限公司注册使用服务的自然人、法人或其他组织） 与喜脚么（武汉）网络科技有限公司（下称\"喜脚么\"）之间在使用喜脚么出品的互联网产品之前，注册喜脚么帐号时签署的协议。\r\n</p>\r\n<p class=\"MsoNormal\">\r\n	<b>一、重要须知</b><b></b> \r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	1.1 您应认真阅读（未成年人应当在监护人陪同下阅读）、充分理解本《喜脚么技师注册协议》中各条款，包括免除或者限制喜脚么责任的免责条款及对用户的权利限制条款。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	1.2 除非您接受本协议，否则用户无权也无必要继续接受喜脚么的服务，可以退出本次注册。用户点击同意并继续使用喜脚么的服务，视为用户已完全的接受本协议。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	1.3 本协议一经签署，具有法律效力，请您慎重考虑是否接受本协议。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	1.4 在您签署本协议之后，此文本可能因国家政策、产品以及履行本协议的环境发生变化而进行修改，修改后的协议发布在本网站上，若您对修改后的协议有异议的，请立即停止登录、使用喜脚么产品及服务，若您登录或继续使用喜脚么产品，视为对修改后的协议予以认可。\r\n</p>\r\n<p class=\"MsoNormal\">\r\n	二、<b>账户注册与使用</b><b></b> \r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	（一）账户注册\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18.0000pt;\">\r\n	2.1.1您应当具有完全民事能力，能独立承担法律责任。若您属于年龄不满18周岁的未成年人、精神病人或其他无民事行为能力、限制民事行为能力人，您应在监护人的监护下使用喜脚么平台及相关服务&nbsp;。如果您是代表法人及其他合法组织注册账户，您应获得法人或者其他合法组织的授权，并签订遵守本用户协议。<br />\r\n2.1.2您需要先在喜脚么服务平台上注册，注册时，您必须提供个人信息包括不限于移动电话号码，并根据喜脚么服务平台操作提示验证您的手机号码。<br />\r\n2.1.3您应当妥善保管在喜脚么信息服务平台注册所获得时获得的用户名及密码等，不得将其泄露或者提供给任何第三方。由于个人原因造成的信息泄漏所造成的损失及风险由您自行承担。喜脚么服务平台保证你信息的安全，如果您的信息出现泄漏，而您无法提供相反的有效证据，您的账户下的一切使用及操作行为均视为您个人所为，平台不承担任何责任。<br />\r\n（二）使用承诺。<br />\r\n2.2.1您确定您向我们提供的信息真实、准确、完整。我们在任何时间都有权以合法的手段核验您提供信息的真实性，并有权在确定您提供的信息不真实时，拒绝向您提供服务或拒绝您使用喜脚么信息服务平台。<br />\r\n2.2.2如果您使用喜脚么平台，即表示您同意以下事项：<br />\r\n（1）您不会将服务或APP、网站等用于任何非法活动。<br />\r\n（2）您不会利用服务或者APP、网站等骚扰、妨碍他人或给他人造成不便。<br />\r\n（3）您不会影响网络的正常运行。<br />\r\n（4）您不会尝试危害服务或者APP、网站等。<br />\r\n（5）遵守您在使用服务或APP、网站时所处国家/地区的所有适用法律法规，及社会道德公序良俗。<br />\r\n2.2.3如果您违反以上任一规则，我们保留立即终止向您提供服务和拒绝您使用喜脚么的权利。\r\n</p>\r\n<p class=\"MsoNormal\">\r\n	三、<b>实名认证</b><b></b> \r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	3.1.1根据《电信和互联网用户个人信息保护规定》, 《全国人民代表大会常务委员会关于加强网络信息保护的决定》以及相关规定和条款, 为了更有效的让用户正常使用喜脚么（技师端）服务功能, 建议您完成实名认证。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	3.1.2您需要提交的认证信息包括：\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	<span>（</span>1）姓名和性别、出生年月；\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	<span>（</span>2）身份证号码；\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	<span>（</span>3）身份证正面照；\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	<span>（</span>4）身份证反面照。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	3.1.3 如果您要上传头像，以便客户自行选择提供服务的对象，请提供个人真实登记照提交审核。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	3.1.4认证流程\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	<span>（</span>1）提交审核后，审核结果会在1-5个工作日内通知，请前往“我的”中查看审核结果。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	<span>（</span>2）提交实名认证后, 在审核阶段以及认证通过后, 不能修改认证信息, 审核未通过,可重新发起实名认证。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	（3）为保障您服务安全，平台服务技师实名认证年龄小于或等于55周岁。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	（二）银行卡绑定\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	3.2.1绑定的支付宝账号用于技师服务收入提现。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	3.2.2被绑定支付宝的开户人姓名必须与喜脚么（技师端）已实名认证账号的姓名一致。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	3.2.3用户进行提现会收取一定手续费用。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	3.2.4用户通过喜脚么技师端选择增值服务，用户自行选择是否支付相关费用。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	3.2.5支付宝绑定失败原因：\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	<span>（</span>1）支付宝号输入有误，请重新核输入。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	<span>（</span>2）检查网络环境。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	（目前喜脚么技师端提现支持支付宝账号绑定，后续根据版本升级会支持银行卡或微信提现）\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	（三）费用结算\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	3.3.1服务结束后次日起的五个工作日内，客户无差评、无投诉、无退款或者系统默认好评（五个工作日届满后次日起），此后每周二可提现余额范围内的金额一次。有差评、有投诉或者有退款的，喜脚么平台客服介入，待事情处理完毕后五个工作日内结算金额至技师的喜脚么账户，此后每周二可提现余额范围内的金额一次。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	3.3.2技师退单的，将由喜脚么平台客服联系客户重新选择技师或者取消订单。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	（四）安全告知\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	3.4.1预约晚上8点以提供上门服务，只能选择男技师。预约晚上11点以后提供上门服务，应当另行增加夜班费。费用标准以系统金额为准。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	3.4.2对于客户提出的变更服务地点的要求，技师有权拒绝；技师接受的，请在第一时间向客服告知，并与家人保持位置时时共享。喜脚么建议所有技师在提供上门服务之前，都与家人保持位置时时共享。后期确有必要，喜脚么不排除开发主动开启技师的时时定位功能，且认证技师不得拒绝。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	3.4.3喜脚么技师端页面有一键报警功能，请技师知悉并谨慎使用，在遇有危险时可以使用该功能，但不得滥用该功能。\r\n</p>\r\n<p class=\"MsoNormal\">\r\n	<b><span>四、协议的效力</span> </b><b><br />\r\n</b> 4.1在您按照注册页面提示填写信息、阅读并同意本协议并完成全部注册程序后或以其他“喜脚么”允许的方式实际使用“喜脚么”服务平台时，您即受本协议的约束。在注册时，您应当按照法律法规要求，或注册页面的提示，准确填写并及时更新您提供的电子邮件地址、联系电话、联系地址、邮政编码等联系方式，以便“喜脚么”或其他会员与您进行有效联系。如您的资料发生变更，您应及时更新您的资料，以使之真实、及时、完整和准确。如有合理理由怀疑您提供的资料错误、不实、过时或不完整的，“喜脚么”有权向您发出询问及/或要求改正的通知，并有权直接做出删除相应资料的处理，直至中止、终止对您提供部分或全部服务。由此导致您在使用“喜脚么”服务平台过程中产生任何损失或增加费用的，应由您完全独自承担。 <br />\r\n4.2对于您提供的资料及数据信息，您授予“喜脚么”独家的、全球通用的、永久的、免费的许可使用权利。您同意，“喜脚么”有权(全部或部分地)&nbsp;使用、复制、更正、发布、翻译、分发、执行和展示您的资料数据（包括但不限于注册资料、行为数据及全部展示于“喜脚么”平台的各类信息）或制作其派生作品，&nbsp;并以现在已知或日后开发的任何形式、媒体或技术，将上述信息纳入其它作品内。在此，您特别做出授权，允许“喜脚么”依据您提供的信息资料及在“喜脚么”服务平台上的行为、投诉及侵权状况等进行信用评级，您对“喜脚么”的信用评级体系予以认可并同意“喜脚么”将您在平台存储的全部或部分信息及信用评价结果以有偿或无偿方式提供给第三方进行商业或非商业方式的使用，而无需在该提供行为之前再另行取得您的同意。如您不同意做出前述授权，您应立即停止注册程序或停止使用“喜脚么”服务平台。如您继续使用“喜脚么”服务平台的，即表示您同意做出前述授权。<br />\r\n4.3您应当在使用“喜脚么”服务平台之前认真阅读全部协议内容。如您对协议有任何疑问，应向“喜脚么”咨询。但无论您事实上是否在使用“喜脚么”服务平台之前认真阅读了本协议内容，只要您使用“喜脚么”服务平台，则本协议即对您产生约束，届时您不应以未阅读本协议的内容或者未获得“喜脚么”对您问询的解答等理由，主张本协议无效，或要求撤销本协议。 <br />\r\n4.4&nbsp;本协议内容包括协议正文及所有“喜脚么”已经发布的或将来可能发布的各类规则。所有规则为本协议不可分割的组成部分，与协议正文具有同等法律效力。除另行明确声明外，任何“喜脚么”及其关联公司提供的服务（以下称为“喜脚么”服务平台）均受本协议约束。您承诺接受并遵守本协议的约定。如果您不同意本协议的约定，您应立即停止注册程序或停止使用“喜脚么”服务平台。 <br />\r\n4.5“喜脚么”有权根据国家法律法规的更新、产品和服务规则的调整需要，不时地制订、修改本协议及/或各类规则，并以网站公示的方式进行公示。如您继续使用“喜脚么”服务平台的，即表示您接受经修订的协议和规则。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	4.6您确定您向我们提供的信息真实、准确、完整。我们在任何时间都有权以合法的手段核验您提供信息的真实性，并有权在确定您提供的信息不真实时，拒绝向您提供服务或拒绝您使用上门了信息服务平台。\r\n</p>\r\n<p class=\"MsoNormal\">\r\n	<b>五、《责任声明》</b><b></b> \r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	5.1您（以下简称甲方）需要确认并知悉，喜脚么（以下简称乙方）为足疗上门等服务信息提供方，并不实际承担或代理第三方足疗上门（以下简称丙方）承担的预订申请中的足疗等服务责任。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	5.2甲方在足疗服务过程中应遵守下列规定：\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	5.3不得向技师提出违反法律法规的服务要求\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	5.4不得诱导、强迫技师提供非平台中的服务\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	5.5不得对技师人身安全造成损害，不得对技师进行语言攻击、侮辱等\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	5.6乙方应当积极协助服务，并确保服务的及时性和标准化\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	5.7丙方在服务过程中提供的服务应当具有可控性，并通过国家职业资格审核部门的审核认证。对服务期间发生的意外，应当及时提供救治服务。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	5.8鉴于网络服务的特殊性，甲、乙、丙方授权乙方可在以下情形发生时随时变更、中断或终止部分或全部的网络服务。\r\n</p>\r\n<p class=\"MsoNormal\">\r\n	<b>六、禁止的行为</b><b></b> \r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	6.1 利用喜脚么服务产品发表、传送、传播、储存危害国家安全、国家统一、社会稳定的内容，或侮辱诽谤、色情、暴力、引起他人不安及任何违反国家法律法规政策的内容或者设置含有上述内容的网名、角色名。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	6.2 利用喜脚么服务发表、传送、传播、储存侵害他人知识产权、商业机密权、肖像权、隐私权等合法权利的内容。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	6.3 进行任何危害计算机网络安全的行为，包括但不限于：使用未经许可的数据或进入未经许可的服务器/账户；未经允许进入公众计算机网络或者他人计算机系统并删除、修改、增加存储信息；未经许可，企图探查、扫描、测试本“软件”系统或网络的弱点或其它实施破坏网络安全的行为；企图干涉、破坏本“软件”系统或网站的正常运行，故意传播恶意程序或病毒以及其他破坏干扰正常网络信息服务的行为；伪造TCP/IP数据包名称或部分名称。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	6.4 进行任何破坏喜脚么服务公平性或者其他影响应用正常秩序的行为，如主动或被动刷分、合伙作弊、使用外挂或者其他的作弊软件、利用BUG（又叫“漏洞”或者“缺陷”）来获得不正当的非法利益，或者利用互联网或其他方式将外挂、作弊软件、BUG公之于众。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	6.5 进行任何诸如发布广告、销售商品的商业行为，或者进行任何非法的侵害喜脚么利益的行为\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	6.6 进行其他任何违法以及侵犯其他个人、公司、社会团体、组织的合法权益的行为。\r\n</p>\r\n<p class=\"MsoNormal\">\r\n	<b><span>七、</span> 免责条款：</b><b></b> \r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	7.1喜脚么以技术手段向您提供的服务方信息陈列、展示、评价的服务仅供您参考，并不构成建议或者任何推荐，亦不对服务提供方所提供服务的质量、服务满意度承担任何担保或连带性责任。<b><br />\r\n</b> 7.2我们不能保证信息服务平台提供的信息绝对准确性，对于因使用（或无法使用）喜脚么平台导致的任何损害，喜脚么不承担责任。<br />\r\n7.3我们对因第三方服务无法使用（包括不限于服务暂停、通信故障等），而造成的损失不承担责任。<br />\r\n7.4喜脚么服务平台上的服务提供方有权拒绝超过喜脚么服务平台项目服务范围的服务请求，并且有权在接受服务后的服务过程中拒绝您的任何与服务内容无关的请求。喜脚么并不对此类纠纷承担任何担保及协调责任。<br />\r\n7.5在您确保已知悉喜脚么服务平台所有功能提示及愿意为本信息平台各功能进行必要的操作，您根据自身需求自愿选择使用喜脚么平台及其相关服务，因使用本信息平台及其相关服务所存在的风险和一切后果将完全由您自己承担，喜脚么不承担其责任。<br />\r\n7.6喜脚么服务平台APP经过严格的测试，但我们不能保证与所有的软硬件设备系统完全兼容，不能保证APP在任何情况下均可正常提供服务。如果出现不兼容及APP错误的情况，您可将情况反馈给我们，获得技术支持。如果喜脚么仍无法解决兼容性问题，您可以停止使用。<br />\r\n7.7您违反本协议规定，对喜脚么造成损害的，喜脚么有权采取包括但不限于取消您的账户、中断您的信息平台使用许可、停止为您提供服务、限制您使用服务功能、对您采取措施追究您的法律责任、要求您进行损害赔偿等措施。<br />\r\n<b>八、其他条款</b><b></b> \r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	8.1 若用户与喜脚么因履行本协议发生争议的，双方均应本着友好协商的原则加以解决。协商解决未果，任何一方均可以提请有管辖权的法院提起诉讼。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	8.2 本协议由喜脚么公布在网站上，对喜脚么具有法律约束力；用户一经点击接受或者直接注册等行为视为对本协议的接受，对用户具有法律约束力。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	8.3 喜脚么旗下具体的网站、网页应用、APP等的使用由用户和喜脚么的业务平台另行签署相关软件授权及服务协议。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	8.4 本协议条款无论因何种原因部分无效或不可执行，其余条款仍有效，对双方具有约束力。\r\n</p>\r\n<p class=\"MsoNormal\" style=\"text-indent:18pt;\">\r\n	<br />\r\n</p>\r\n<p class=\"p\" align=\"right\" style=\"margin-left:7.5000pt;text-align:right;\">\r\n	<b>喜脚么（武汉）网络科技有限公司</b><b></b> \r\n</p>\r\n<p class=\"p\" align=\"center\" style=\"text-align:right;margin-left:7.5pt;\">\r\n	<b> </b><b>201</b><b>7</b><b>年</b><b></b> \r\n</p>\r\n<p class=\"MsoNormal\">\r\n	<br />\r\n</p>', '1512370952', '0');
INSERT INTO `xjm_article` VALUES ('5', '登录遇到问题', '7', '0', 'Q：收不到短信验证码怎么办', '1509518359', '<h3>\r\n	Q：收不到短信验证码怎么办\r\n</h3>\r\n<span>A：先检查输入的手机号是否正确；如等待超过60秒仍未收到验证码，可重新输入一次；如仍然收不到可拨打客服电话查询验证码 </span>', '1509518783', '0');
INSERT INTO `xjm_article` VALUES ('6', '订单相应问题', '7', '0', 'Q：支付无法正常运行怎么办', '1509518389', '<h3>\r\n	Q：支付无法正常运行怎么办\r\n</h3>\r\n<span>A：一旦出现无法正常付款情况，请及时联系喜脚么官方客服帮您解决，也可以截图发送到客服进行反馈</span>', '1509518796', '0');
INSERT INTO `xjm_article` VALUES ('7', '优惠券相关问题', '7', '0', 'Q：如何领取优惠券', '1509518538', '<h3>\r\n	Q：如何领取优惠券\r\n</h3>\r\n<span>A：到活动专区每日参加活动即可抽取优惠券；或者到我的积分-积分兑换也可以兑换到相应优惠券</span> \r\n<h3>\r\n	Q：优惠券如何使用\r\n</h3>\r\n<span>A：在您下单时可看到优惠券选项，点击后可选择相应优惠券，选择后直接进行抵扣，如暂时无适用或者优惠券是灰色，\r\n							则代表相应优惠券不符合使用条件</span>', '1509518647', '0');
INSERT INTO `xjm_article` VALUES ('8', '订单变更/取消/退款相关问题', '7', '0', 'Q：如何取消订单/申请退款', '1509518565', '<h3>\r\n	Q：如何取消订单/申请退款\r\n</h3>\r\n<span>A：在您未支付前，可直接点击订单下方的【取消订单】,可免费取消订单；如您已经支付，点击订单下方的【申请退款】，\r\n							选择你要退款的类型，后经技师同意即可退款，如果技师不同意退款，将有平台客服进行仲裁处理，保障用户利益 </span> \r\n<h3>\r\n	Q：退款到哪里？\r\n</h3>\r\n<span>A：喜脚么平台退款将在订单取消，或者技师同意退款的情况下立即原路返回到用户付款账户</span> \r\n<h3>\r\n	Q：退款什么时候到账？\r\n</h3>\r\n<span>A：具体到账时间取决于银行到账时间，通常都在一个工作日内到账</span>', '1509518829', '0');
INSERT INTO `xjm_article` VALUES ('9', '售后服务相关问题', '7', '0', 'Q：如何处理服务售后问题', '1509518700', '<h3>\r\n	Q：如何处理服务售后问题\r\n</h3>\r\n<span>A：如在技师服务出现售后问题的，请直接联系技师处理，如未能妥善处理，可到喜脚么平台官方客服投诉，我们将督促相关技师\r\n							解决问题 </span>', '1509518843', '0');
INSERT INTO `xjm_article` VALUES ('10', '技师服务时间与城市', '8', '0', 'Q：上门服务时间是怎样设置的?', '1509518389', '<h3>\r\n	Q：上门服务时间是怎样设置的?\r\n</h3>\r\n<span>A：技师可以根据自己服务时间，在“我的”-“设置”页面中修改自己上门服务时间。</span> \r\n<h3>\r\n	Q：有时候不在武汉，可以接到单吗？\r\n</h3>\r\n<span>A：技师不在武汉地区，将无法接收到订单。目前喜脚么上门服务正积极开拓一、二线城市。已开通上门服务的城市，喜脚么APP将会推送消息告知。</span>', '1510230240', '0');
INSERT INTO `xjm_article` VALUES ('11', '审核与认证', '8', '0', 'Q：上传的身份证信息多久审核通过？', '1509518538', '<h3>\r\n	Q：上传的身份证信息多久审核通过？\r\n</h3>\r\n<span>A：用户上传身份证相片1-2工作日内审核通过，上传身份证相片时请保持证件清晰、无遮挡，有利于系统快速审核通过。如时间2工作日还未收到审核通知，请通过xijiaomo@163.con联系我们或电话联系官方客服人员。</span> \r\n<h3>\r\n	Q：为什么要身份认证？\r\n</h3>\r\n<span>A：技师身份认证有助于增加客户对您的信任感，平台技师统一实名制是保障客户需求与您订单收益。身份认证更好的管理您的账户安全。</span>', '1510230301', '0');
INSERT INTO `xjm_article` VALUES ('12', '订单', '8', '0', 'Q：我怎么从来没接收到订单?', '1509518565', '<h3>\r\n	Q：我怎么从来没接收到订单?\r\n</h3>\r\n<span>A：技师没有接收到订单情况，检查以下情况</span> <br />\r\n<span>1.检查APP是否为最新版</span> <br />\r\n<span>2.检查APP定位权限是否已开启</span><br />\r\n<span>3.检查自己是否在上门服务区域内</span><br />\r\n<span>4.检查APP网络环境</span><br />\r\n<span>（未出现以上问题，请联系官方客服人员）</span> \r\n<h3>\r\n	Q：我的收益在哪看？\r\n</h3>\r\n<span>A：技师每次接单后，上门技能服务客户后，客户选择结束服务并对服务做出评价，系统将本次上门服务费用存入APP平台，用户在“我的”个人中心页面查看收益。</span> \r\n<h3>\r\n	Q：上门服务了，被客户退款了怎办？\r\n</h3>\r\n<span>A：针对技师上门服务后，客户申请退款请求，技师有权利拒绝，三次拒绝退款后。喜脚么平台将介入退款事宜，根据双方服务内容情况协商处理。</span> \r\n<h3>\r\n	Q：接到订单了，临时有事去不了怎么办？\r\n</h3>\r\n<span>A：当您接收到订单后，临时有事无法进行上门服务，可直接联系上门服务客户电话，进行协商沟通，客户可取消本次上门服务订单。平台会根据您拒绝服务订单次数，后台会对您上门服务誉体系进行打分。</span>', '1510230530', '0');
INSERT INTO `xjm_article` VALUES ('13', '评价系统', '8', '0', 'Q：接到订单服务完成后，为什么没有评价显示？', '1509518700', '<h3>\r\n	Q：接到订单服务完成后，为什么没有评价显示？\r\n</h3>\r\n<span>A：评价是客户享受服务完成后，在APP上对技师服务进行评价，客户未对服务做出评价，系统将在7天内自动对未评价订单做默认出好评处理。 </span> \r\n<h3>\r\n	Q：好评、差评对我有什么作用？\r\n</h3>\r\n<span>A：技师每次服务结束后，平台都会记录服务评价，进行综合评分，好评数越多技师将会接受到更多服务订单，更受客户青睐。评价系统将会对每个客户服务评价做出综合判断，对技师\r\n服务做出衡量标准。</span>', '1510230595', '0');
INSERT INTO `xjm_article` VALUES ('14', '用户注册协议', '5', '0', '用户注册协议', '1509608085', '<h1>\r\n	<strong>“喜脚么”服务平台使用协议</strong> \r\n</h1>\r\n<p style=\"font-size:14px;\">\r\n	<strong>提示条款：</strong> \r\n</p>\r\n<p>\r\n	本《喜脚么用户注册协议》是您（下称\"用户\"或\"您\"，是指通过喜脚么（武汉）网络科技有限公司注册使用服务的自然人、法人或其他组织） 与喜脚么（武汉）网络科技有限公司（下称\"喜脚么\"）之间在使用喜脚么出品的互联网产品之前，注册喜脚么帐号时签署的协议。\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p style=\"font-size:14px;\">\r\n	<strong>一、重要须知</strong> \r\n</p>\r\n<p>\r\n	1.1 您应认真阅读（未成年人应当在监护人陪同下阅读）、充分理解本《喜脚么用户注册协议》中各条款，包括免除或者限制喜脚么责任的免责条款及对用户的权利限制条款。\r\n</p>\r\n<p>\r\n	1.2 除非您接受本协议，否则用户无权也无必要继续接受喜脚么的服务，可以退出本次注册。用户点击同意并继续使用喜脚么的服务，视为用户已完全的接受本协议。\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	1.3 本协议一经签署，具有法律效力，请您慎重考虑是否接受本协议。\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	1.4 在您签署本协议之后，此文本可能因国家政策、产品以及履行本协议的环境发生变化而进行修改，修改后的协议发布在本网站上，若您对修改后的协议有异议的，请立即停止登录、使用喜脚么产品及服务，若您登录或继续使用喜脚么产品，视为对修改后的协议予以认可。\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<ul>\r\n	<li style=\"font-size:14px;\">\r\n		<strong>账户注册与使用</strong> \r\n	</li>\r\n</ul>\r\n<p>\r\n	（一）账户注册\r\n</p>\r\n<p>\r\n	2.1.1您应当具有完全民事能力，能独立承担法律责任。若您属于年龄不满18周岁的未成年人、精神病人或其他无民事行为能力、限制民事行为能力人，您应在监护人的监护下使用喜脚么平台及相关服务&nbsp;。如果您是代表法人及其他合法组织注册账户，您应获得法人或者其他合法组织的授权，并签订遵守本用户协议。<br />\r\n2.1.2您需要先在喜脚么服务平台上注册，然后才能使用平台提供的服务。注册时，您必须提供个人信息包括不限于移动电话号码，并根据喜脚么服务平台操作提示验证您的手机号码。<br />\r\n2.1.3您应当妥善保管在喜脚么信息服务平台注册所获得时获得的用户名及密码等，不得将其泄露或者提供给任何第三方。由于个人原因造成的信息泄漏所造成的损失及风险由您自行承担。喜脚么服务平台保证你信息的安全，如果您的信息出现泄漏，而您无法提供相反的有效证据，您的账户下的一切使用及操作行为均视为您个人所为，平台不承担任何责任。<br />\r\n（二）使用承诺。<br />\r\n2.2.1您确定您向我们提供的信息真实、准确、完整。我们在任何时间都有权以合法的手段核验您提供信息的真实性，并有权在确定您提供的信息不真实时，拒绝向您提供服务或拒绝您使用喜脚么信息服务平台。<br />\r\n2.2.2如果您使用喜脚么信息平台，即表示您同意以下事项：<br />\r\n（1）您不会将服务或APP、网站等用于任何非法活动。<br />\r\n（2）您不会利用服务或者APP、网站等骚扰、妨碍他人或给他人造成不便。<br />\r\n（3）您不会影响网络的正常运行。<br />\r\n（4）您不会尝试危害服务或者APP、网站等。<br />\r\n（5）遵守您在使用服务或APP、网站时所处国家/地区的所有适用法律法规，及社会道德公序良俗。<br />\r\n2.2.3如果您违反以上任一规则，我们保留立即终止向您提供服务和拒绝您使用喜脚么的权利。\r\n</p>\r\n<ul>\r\n	<li style=\"font-size:14px;\">\r\n		<strong>服务内容</strong> \r\n	</li>\r\n</ul>\r\n<p>\r\n	（一）服务规则\r\n</p>\r\n<p>\r\n	3.1.1您可以通过喜脚么服务平台向服务提供方发出服务需求，同时，喜脚么服务平台将协助您请求服务提供方提供服务。喜脚么平台会通知您并提供该服务提供方的相关信息。<br />\r\n3.1.2&nbsp;我们不能保证你能联系上服务提供方并获得服务，因为这受限于您发出订单的时间和位置以及此区域服务提供方是否能提供服务。您根据喜脚么平台提示的操作步骤，成功预约到能够上门服务的服务人员并确认所需上门服务的时间、地点等信息之时，您与第三方服务提供方达成的服务合同（以下简称服务合同或者订单）正式成立。<br />\r\n3.1.3如订单因第三方服务提供方的原因无法按约履行，我们将尽快与您联系并在获得您的许可后重新改派其他上门服务提供方或者为您取消/变更订单。<br />\r\n3.1.4如服务合同因您个人的原因无法履行，请您至少提前8个小时取消订单或者拨打我们客服电话取消或更改订单。如多次您无故在派单成功后或服务人员到达服务对点后取消服务，为保障第三方服务提供方的权益及其他用户方的权益，喜脚么有权随时为您暂停或者终止提供喜脚么平台信息服务，且您已经支付的订单费用会扣除相关成本。<br />\r\n3.1.5服务项目的升级与终止。我们始终在不断改进我们所提供的服务，所以我们可能会在未经通知您的情况下升级、更改、暂停或彻底停止某项服务项目。如果出现上述情况，在可能且合理的情况下，我们会向您提前发出通知，并协调第三方服务提供方对您因服务变更受到的影响给予适当补偿。\r\n</p>\r\n<p>\r\n	3.1.7预约晚上8点以提供上门服务，只能选择男技师。预约晚上11点以后提供上门服务，应当另行增加夜班费。费用标准以系统金额为准。\r\n</p>\r\n<p>\r\n	3.1.8客户的退单和取消订单，退款将退在喜脚么平台账户，该款不得提现，只能用于平台消费。享受充值赠费优惠的，充值成功后，该款与赠费也在喜脚么账户，不得提现，只能用于平台消费。\r\n</p>\r\n<br />\r\n（二）服务变更、中断或终止<br />\r\n3.2.1 鉴于网络服务的特殊性，喜脚么有权随时变更服务内容并修改本协议的任何条款，一旦本协议的内容发生变动，喜脚么将会直接在平台网站、APP等渠道上公布修改之后的协议内容，该公布行为视为本公司已经通知了用户修改内容。同时喜脚么也可通过其他适当方式向用户提示修改内容。<br />\r\n3.2.2 若用户不同意喜脚么对本协议相关条款所做的修改，用户应当停止使用平台服务。若用户继续使用平台服务，则视为用户接受喜脚么对本协议相关条款所做的修改。<br />\r\n3.2.3 喜脚么需要定期或不定期地对平台或相关的设备进行检修或者维护，如因此类情况而造成网络服务在合理时间内的中断，喜脚么无需为此承担任何责任，除特殊情况外应当事先进行通告。但已确认的订单仍应在约定时间与地点履行，由客服后台处理。客户评价在系统恢复后的5个工作日内进行。<br />\r\n3.2.4 若用户提供的相关注册资料不真实或严重违反平台的规范，平台有权随时中断或终止向用户提供本协议项下的网络服务，而无需对用户或任何第三方承担责任。<br />\r\n（三）服务的费用及支付<br />\r\n3.3.1您可以通过我们的官方渠道、APP、客服电话、微信平台等）查看、知晓服务提供方所提供服务的价格，这些价格由喜脚么及服务提供方统一制定，价格会进行更新，您需自行留意服务项目的最新价格。<br />\r\n3.3.2我们抵制并请求您和我们一起监督服务提供方的下列不规范的服务及收费行为：\r\n<p>\r\n	<br />\r\n</p>\r\n<ul>\r\n	<li>\r\n		（1）不经由喜脚么平台达成交易服务；\r\n	</li>\r\n	<li>\r\n		（2）不经由喜脚么平台更改服务订单信息；\r\n	</li>\r\n	<li>\r\n		（3）不按喜脚么信息平台公布的标准价格收费；\r\n	</li>\r\n	<li>\r\n		（4）乱设收费项等；\r\n	</li>\r\n	<li>\r\n		（5）收取客户现金；\r\n	</li>\r\n	<li>\r\n		（6）其他损害平台利益、用户利益、其他服务提供方利益的行为。<br />\r\n		<p>\r\n			3.3.3我们支持多种在线支付方式（包括不限于微信支付、支付宝支付等），暂不接受现金支付。在线支付方式是喜脚么经服务提供方的授权，与第三方电子支付服务供应商达成合作，将您的支付宝账户、微信支付账户关联到您在喜脚么信息平台的个人账户。您选择在线支付方式后，除受到本用户协议的约束之外，还要受电子支付服务商及信用卡/借记卡发卡行的条款和政策的约束。喜脚么对于电子支付服务商及银行的行为（包括但不限于系统故障、操作系统错误等）不承担责任。\r\n		</p>\r\n<!--[if IE]>\r\n				<![endif]-->&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.3.4您可以免费下载及使用喜脚么平台，我们保留日后就APP或信息服务收取使用费的权利。如果我们决定收取此类费用，我们将向您发送通知，您有权选择继续履行或者终止本用户协议。<br />\r\n（四）服务的评价。<br />\r\n4.1.1您有权利在信息服务平台上对第三方服务提供方做出相应评价、意见或者建议，以供服务提供方提升服务质量，进而提升喜脚么信息平台的用户体验。<br />\r\n4.1.2在您做出评价、意见&nbsp;或者建议时，应避免下列行为：<br />\r\n（1）发布违反国家相关法律、法规及其他规范性文件的信息或从事其他违法犯罪活动；<br />\r\n（2）发布含有非法、淫秽、威胁、诽谤、侵犯他人隐私、侵犯知识产权等信息，或以其他形式侵犯第三方的合法权益，违反社会公德的图片或者言论；<br />\r\n（3）发布与我们服务无关的言论；<br />\r\n（4）其他损害喜脚么合法权益、技师合法权益及影响其他用户体验的行为。<br />\r\n4.1.3如果您违反上述规则，喜脚么有权不经事先通知，直接采取屏蔽、删除您的评论及采取相关处理措施，包括但不限于暂停、终止您继续使用评论功能或者关闭您的账号。<br />\r\n4.1.4您在喜脚么平台上发布评论信息及使用相关功能，喜脚么即视为您主动将您发表的任何形式的信息的著作权，包括但不限于：复制权、发行权、出租权、展览权、表演权、放映权、广播权、信息网络传播权、摄制权、改编权、翻译权、汇编权等以及应当由著作权人享有的其他可转让权利无偿独家转让给喜脚么所有，喜脚么有权就任何主体侵权单独提起诉讼并获得全部赔偿。本用户协议属于著作权相关法律意义层面上的书面协议，其效力及于您在喜脚么平台发布的任何受著作权法保护的作品内容，无论该内容形成于本协议签订前还是本协议签订后。\r\n	</li>\r\n</ul>\r\n<p>\r\n	<br />\r\n</p>\r\n<p style=\"font-size:14px;\">\r\n	<strong>四、知识产权</strong> \r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	4.1 喜脚么的服务包括喜脚么运营的网站、网页应用、软件以及内涵的文字、图片、视频、音频等元素，喜脚么服务标志、标识以及专利权与商标权，喜脚么对此享有上述知识产权。\r\n</p>\r\n<p>\r\n	4.2 用户不得对喜脚么服务涉及的相关网页、应用、软件等产品进行反向工程、反向汇编、反向编译等。\r\n</p>\r\n<p>\r\n	4.3 用户使用喜脚么服务只能在本《喜脚么用户注册协议》以及相应的授权许可协议授权的范围使用喜脚么知识产权，未经授权超范围使用的构成对喜脚么的侵权。\r\n</p>\r\n<p>\r\n	4.4 用户在使用喜脚么服务时发表上传的文字、图片等信息，此部分信息的知识产权归用户，但用户的发表行为是对喜脚么服务平台的授权，用户确认其发表、上传的信息非独占性、永久性的授权，该授权可转授权。喜脚么可将前述信息在喜脚么旗下的服务平台上使用，可再次编辑后使用，也可以由喜脚么授权给合作方使用。\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p style=\"font-size:14px;\">\r\n	<strong>五、禁止的行为</strong> \r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	5.1 利用喜脚么服务产品发表、传送、传播、储存危害国家安全、国家统一、社会稳定的内容，或侮辱诽谤、色情、暴力、引起他人不安及任何违反国家法律法规政策的内容或者设置含有上述内容的网名、角色名。\r\n</p>\r\n<p>\r\n	5.2 利用喜脚么服务发表、传送、传播、储存侵害他人知识产权、商业机密权、肖像权、隐私权等合法权利的内容。\r\n</p>\r\n<p>\r\n	5.3 进行任何危害计算机网络安全的行为，包括但不限于：使用未经许可的数据或进入未经许可的服务器/账户；未经允许进入公众计算机网络或者他人计算机系统并删除、修改、增加存储信息；未经许可，企图探查、扫描、测试本“软件”系统或网络的弱点或其它实施破坏网络安全的行为；企图干涉、破坏本“软件”系统或网站的正常运行，故意传播恶意程序或病毒以及其他破坏干扰正常网络信息服务的行为；伪造TCP/IP数据包名称或部分名称。\r\n</p>\r\n<p>\r\n	5.4 进行任何破坏喜脚么服务公平性或者其他影响应用正常秩序的行为，如主动或被动刷分、合伙作弊、使用外挂或者其他的作弊软件、利用BUG（又叫“漏洞”或者“缺陷”）来获得不正当的非法利益，或者利用互联网或其他方式将外挂、作弊软件、BUG公之于众。\r\n</p>\r\n<p>\r\n	5.5 进行任何诸如发布广告、销售商品的商业行为，或者进行任何非法的侵害喜脚么利益的行为。\r\n</p>\r\n<p>\r\n	5.6 进行其他任何违法以及侵犯其他个人、公司、社会团体、组织的合法权益的行为。\r\n</p>\r\n<p>\r\n	5.7 恶意差评或恶意退款（差评或退款请先举证，客服核实后会尽快回复客户）。\r\n</p>\r\n<p>\r\n	5.8 侵害技师的人身、财产权利或其他合法权益的（技师举证或报警，客服核实后会协助调查）。\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p style=\"font-size:14px;\">\r\n	<strong> </strong><strong>六.免责条款：</strong> \r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	6.1喜脚么以技术手段向您提供的服务方信息陈列、展示、评价的服务仅供您参考，并不构成建议或者任何推荐，亦不对服务提供方所提供服务的质量、服务满意度承担任何担保或连带性责任。<strong><br />\r\n</strong> 6.2我们不能保证信息服务平台提供的信息绝对准确性，对于因使用（或无法使用）喜脚么平台导致的任何损害，喜脚么不承担责任。<br />\r\n6.3我们对因第三方服务无法使用（包括不限于服务暂停、通信故障等），而造成的损失不承担责任。<br />\r\n6.4喜脚么服务平台上的服务提供方有权拒绝超过喜脚么服务平台项目服务范围的服务请求，并且有权在接受服务后的服务过程中拒绝您的任何与服务内容无关的请求。喜脚么并不对此类纠纷承担任何担保及协调责任。<br />\r\n6.5在您确保已知悉喜脚么服务平台所有功能提示及愿意为本信息平台各功能进行必要的操作，您根据自身需求自愿选择使用喜脚么平台及其相关服务，因使用本信息平台及其相关服务所存在的风险和一切后果将完全由您自己承担，喜脚么不承担其责任。<br />\r\n6.6喜脚么服务平台APP经过严格的测试，但我们不能保证与所有的软硬件设备系统完全兼容，不能保证APP在任何情况下均可正常提供服务。如果出现不兼容及APP错误的情况，您可将情况反馈给我们，获得技术支持。如果喜脚么仍无法解决兼容性问题，您可以停止使用。<br />\r\n6.7您违反本协议规定，对喜脚么造成损害的，喜脚么有权采取包括但不限于取消您的账户、中断您的信息平台使用许可、停止为您提供服务、限制您使用服务功能、对您采取措施追究您的法律责任、要求您进行损害赔偿等措施。\r\n</p>\r\n<p style=\"font-size:14px;\">\r\n	<br />\r\n<strong>七、其他条款</strong> \r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	7.1 若用户与喜脚么因履行本协议发生争议的，双方均应本着友好协商的原则加以解决。协商解决未果，任何一方均可以提请有管辖权的法院提起诉讼。\r\n</p>\r\n<p>\r\n	7.2 本协议由喜脚么公布在网站上，对喜脚么具有法律约束力；用户一经点击接受或者直接注册等行为视为对本协议的接受，对用户具有法律约束力。\r\n</p>\r\n<p>\r\n	7.3 喜脚么旗下具体的网站、网页应用、APP等的使用由用户和喜脚么的业务平台另行签署相关软件授权及服务协议。\r\n</p>\r\n<p>\r\n	7.4 本协议条款无论因何种原因部分无效或不可执行，其余条款仍有效，对双方具有约束力。\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p style=\"font-size:14px;text-align:right;\">\r\n	<strong>喜脚么（武汉）网络科技有限公司</strong> \r\n</p>\r\n<p style=\"font-size:13px;text-align:right;\">\r\n	<strong> 2017</strong><strong>年</strong> \r\n</p>', '1512015316', '0');
INSERT INTO `xjm_article` VALUES ('15', '账户提现流程', '1', '0', 'Q3', '1509862615', '服务器赚取一定佣金', '1509862615', '0');
INSERT INTO `xjm_article` VALUES ('16', '账户提现流程', '1', '0', 'Q3', '1509862652', '点击立即提现按钮（每周二才能提现）', '1509862652', '0');
INSERT INTO `xjm_article` VALUES ('17', '其他', '8', '0', 'Q:导航路线为什么有时候不准?', '1510230612', '<h3>\r\n	Q:导航路线为什么有时候不准?\r\n</h3>\r\n<span>A:APP导航根据GPS系统定位您手机位置，规划您的出发路线。接到订单后请务必授权并打开手机GPS系统，确保导航功能的使用。</span> \r\n<h3>\r\n	Q:一键求助功能有什么作用？\r\n</h3>\r\n<span>A:当您上门服务过程中，遇到紧急情况，可以使用一键求助功能，拨打求救电话。确保您的上门服务安全性。</span>', '1510230653', '0');
INSERT INTO `xjm_article` VALUES ('18', '足疗--上门服务项目详细内容', '4', '0', '足疗--上门服务项目详细内容', '1510567231', '<ul>\r\n	<li style=\"color:#D5201E;\">\r\n		<h1>\r\n			禁忌说明：\r\n		</h1>\r\n		<p>\r\n			孕妇，经期，骨折，高血压，严重心脏病人群不宜。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			服务流程：\r\n		</h1>\r\n		<p>\r\n			1.按前检查辨别健康状况，询问具体症状，制定调理方案。\r\n		</p>\r\n		<p>\r\n			2.服务泡脚10至30分钟。\r\n		</p>\r\n		<p>\r\n			3.进行足底按摩放松、促进血液循环。\r\n		</p>\r\n		<p>\r\n			4.进行小腿按摩，调节腿部神经肌肉。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			适用人群：\r\n		</h1>\r\n		<p>\r\n			工作压力大，缺乏锻炼，腰痛，脚底，腿部酸痛，免疫力下降亚健康人群。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			功能介绍：\r\n		</h1>\r\n		<p>\r\n			通过对足部的刺激，调整人体生理功能，调节神经系统、内分泌系统；改善血液循环，提高免疫系统功能，增强机体免疫力、抵抗力\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			用品提供：\r\n		</h1>\r\n		<p>\r\n			口罩、鞋套、按摩巾、足疗桶。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			订单须知：\r\n		</h1>\r\n		<p>\r\n			1.技师将在您下单后30分钟内确认是否接单。\r\n		</p>\r\n		<p>\r\n			2.订单确认后，您修改订单时间或退款操作请与客服联系。\r\n		</p>\r\n		<p>\r\n			3.如技师接单后爽约将全额退款给您，并向您赠送优惠券。\r\n		</p>\r\n		<p>\r\n			4.为保障您的权益，消费订单请通过喜脚么平台支付。\r\n		</p>\r\n		<p>\r\n			5.喜脚么提供专业、标准服务平台，对于客户有穿着暴露或不雅行为，技师有权利拒绝服务，并保留法律权利。\r\n		</p>\r\n		<p>\r\n			6.因夜间技师往返交通不便，晚22:00至早06:00间服务的订单，需另行向技师支付往返打车费用20元。\r\n		</p>\r\n		<p>\r\n			7.考虑夜间安全性，晚上20:00后客户只能选择男技师上门服务。\r\n		</p>\r\n	</li>\r\n</ul>', '1512529752', '0');
INSERT INTO `xjm_article` VALUES ('19', '刮痧--上门服务项目详细内容', '4', '0', '刮痧--上门服务项目详细内容', '1510568688', '<ul>\r\n	<li style=\"color:#D5201E;\">\r\n		<h1>\r\n			禁忌说明：\r\n		</h1>\r\n		<p>\r\n			皮肤过敏、未愈合伤口等，有出血倾向者或严重血小板减少等患者严禁刮痧，醉酒、过饥、过饱、过渴、过度疲劳者禁刮。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			服务流程：\r\n		</h1>\r\n		<p>\r\n			1.按前检查辨别健康状况，询问具体症状，制定调理方案。\r\n		</p>\r\n		<p>\r\n			2.准备并检查刮痧板用品情况，刮痧用的润滑剂是否备好，以免伤及皮肤。\r\n		</p>\r\n		<p>\r\n			3.根据所需刮拭的部位，选择合适的身体部位。\r\n		</p>\r\n		<p>\r\n			4.在刮拭部位上均匀涂上刮舒润滑剂。\r\n		</p>\r\n		<p>\r\n			5.开始刮痧服务。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			适用人群：\r\n		</h1>\r\n		<p>\r\n			大多数人群。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			功能介绍：\r\n		</h1>\r\n		<p>\r\n			舒筋通络，消除了疼痛病灶，调节肌肉的收缩和舒张，使组织间压力得到调节。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			用品提供：\r\n		</h1>\r\n		<p>\r\n			治疗盘、刮具、治疗碗、浴巾。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			订单须知：\r\n		</h1>\r\n		<p>\r\n			1.技师将在您下单后30分钟内确认是否接单。\r\n		</p>\r\n		<p>\r\n			2.订单确认后，您修改订单时间或退款操作请与客服联系。\r\n		</p>\r\n		<p>\r\n			3.如技师接单后爽约将全额退款给您，并向您赠送优惠券。\r\n		</p>\r\n		<p>\r\n			4.为保障您的权益，消费订单请通过喜脚么平台支付。\r\n		</p>\r\n		<p>\r\n			5.喜脚么提供专业、标准服务平台，对于客户有穿着暴露或不雅行为，技师有权利拒绝服务，并保留法律权利。\r\n		</p>\r\n<p>\r\n			6.因夜间技师往返交通不便，晚22:00至早06:00间服务的订单，需另行向技师支付往返打车费用20元。\r\n		</p>\r\n<p>\r\n			7.考虑夜间安全性，晚上20:00后客户只能选择男技师上门服务。\r\n		</p>\r\n	</li>\r\n</ul>', '1512529763', '0');
INSERT INTO `xjm_article` VALUES ('20', '按摩--上门服务项目详细内容', '4', '0', '按摩--上门服务项目详细内容', '1510570402', '<ul>\r\n	<li style=\"color:#D5201E;\">\r\n		<h1>\r\n			禁忌说明：\r\n		</h1>\r\n		<p>\r\n			有严重心脏病、肝脏病、肾脏病及肺病的人群。骨折、水火烫伤、皮肤病、情绪不稳定、饮酒过量、年老体弱人群。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			服务流程：\r\n		</h1>\r\n		<p>\r\n			1.按前检查辨别健康状况，询问具体症状，制定调理方案。\r\n		</p>\r\n		<p>\r\n			2.选择合适的卧姿或平躺。\r\n		</p>\r\n		<p>\r\n			3.进行身体部位轻度按、捏放松身体肌肉组织。\r\n		</p>\r\n		<p>\r\n			4.全深度对肌肉组织进行推、按、捏、揉，促进血液循环。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			适用人群：\r\n		</h1>\r\n		<p>\r\n			久坐、久站、腰肌劳损、腰间盘突出、坐骨神经痛、梨状肌、各种慢性疾病、长期缺乏运动、工作繁忙、精神紧张等人群。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			功能介绍：\r\n		</h1>\r\n		<p>\r\n			疏通全身经络，促进血液循环，全身肌肉组织按摩。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			用品提供：\r\n		</h1>\r\n		<p>\r\n			一次性床单、一次性毛巾、精油等。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			订单须知：\r\n		</h1>\r\n		<p>\r\n			1.技师将在您下单后30分钟内确认是否接单。\r\n		</p>\r\n		<p>\r\n			2.订单确认后，您修改订单时间或退款操作请与客服联系。\r\n		</p>\r\n		<p>\r\n			3.如技师接单后爽约将全额退款给您，并向您赠送优惠券。\r\n		</p>\r\n		<p>\r\n			4.为保障您的权益，消费订单请通过喜脚么平台支付。\r\n		</p>\r\n		<p>\r\n			5.喜脚么提供专业、标准服务平台，对于客户有穿着暴露或不雅行为，技师有权利拒绝服务，并保留法律权利。\r\n		</p>\r\n		<p>\r\n			6.因夜间技师往返交通不便，晚22:00至早06:00间服务的订单，需另行向技师支付往返打车费用20元。\r\n		</p>\r\n		<p>\r\n			7.考虑夜间安全性，晚上20:00后客户只能选择男技师上门服务。\r\n		</p>\r\n	</li>\r\n</ul>', '1512529778', '0');
INSERT INTO `xjm_article` VALUES ('21', '洗面--上门服务项目详细内容', '4', '0', '洗面--上门服务项目详细内容', '1510570652', '<ul>\r\n	<li style=\"color:#D5201E;\">\r\n		<h1>\r\n			禁忌说明：\r\n		</h1>\r\n		<p>\r\n			皮肤过敏、长痘炎症、传染性不宜洗面。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			服务流程：\r\n		</h1>\r\n		<p>\r\n			1.用温水湿润脸部，保证您脸部毛孔充分张开。\r\n		</p>\r\n		<p>\r\n			2.洁面乳在手心充分打起泡沫，使洁面乳充分起沫。\r\n		</p>\r\n		<p>\r\n			3.泡沫涂在您的脸上轻轻打圈按摩，让泡沫覆盖面部。\r\n		</p>\r\n		<p>\r\n			4.清洗脸部，用蘸了凉水的毛巾轻敷脸部。让您毛孔收紧，同时促进面部血液循环。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			适用人群：\r\n		</h1>\r\n		<p>\r\n			脸部毛孔较大、出油、干燥皮肤人群。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			功能介绍：\r\n		</h1>\r\n		<p>\r\n			脸部去角质，防止毛孔堵塞、对付毛孔黑头，保持皮肤良好的新陈代谢状态。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			用品提供：\r\n		</h1>\r\n		<p>\r\n			一次性毛巾、口罩、洁净洗面用具。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			订单须知：\r\n		</h1>\r\n		<p>\r\n			1.技师将在您下单后30分钟内确认是否接单。\r\n		</p>\r\n		<p>\r\n			2.订单确认后，您修改订单时间或退款操作请与客服联系。\r\n		</p>\r\n		<p>\r\n			3.如技师接单后爽约将全额退款给您，并向您赠送优惠券。\r\n		</p>\r\n		<p>\r\n			4.为保障您的权益，消费订单请通过喜脚么平台支付。\r\n		</p>\r\n		<p>\r\n			5.喜脚么提供专业、标准服务平台，对于客户有穿着暴露或不雅行为，技师有权利拒绝服务，并保留法律权利。\r\n		</p>\r\n		<p>\r\n			6.因夜间技师往返交通不便，晚22:00至早06:00间服务的订单，需另行向技师支付往返打车费用20元。\r\n		</p>\r\n		<p>\r\n			7.考虑夜间安全性，晚上20:00后客户只能选择男技师上门服务。\r\n		</p>\r\n	</li>\r\n</ul>', '1512529806', '0');
INSERT INTO `xjm_article` VALUES ('22', '拔罐--上门服务项目详细内容', '4', '0', '拔罐--上门服务项目详细内容', '1510570823', '<ul>\r\n	<li style=\"color:#D5201E;\">\r\n		<h1>\r\n			禁忌说明：\r\n		</h1>\r\n		<p>\r\n			有心脏病、血液病、皮肤病、皮肤损伤、精神病或神经质的人，肺结核及各种传染病、各种骨折。醉酒等，均应禁用或慎用拔罐疗法。过于瘦弱的人也不宜用火罐。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			服务流程：\r\n		</h1>\r\n		<p>\r\n			1.按前检查辨别健康状况，询问具体症状，制定调理方案。\r\n		</p>\r\n		<p>\r\n			2.洗手，戴口罩。\r\n		</p>\r\n		<p>\r\n			3.准备用品，检查罐口周围是否光滑，有无裂痕。\r\n		</p>\r\n		<p>\r\n			4.手持火罐，将酒精棉球点燃，深入罐内中下端，绕1-2周后迅速抽出，迅速将罐口扣在选定部位上不动，待吸牢后撤手。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			适用人群：\r\n		</h1>\r\n		<p>\r\n			感冒、膈肌痉挛、神经性呕吐、慢性胃炎、偏头痛、面神经麻痹、颈椎病、肩周炎、急性腰扭伤、坐骨神经痛、落枕、网球肘、妇科疾病、更年期综合征、急性扁桃体炎、神经性皮炎、儿童消化不良、小儿腹泻等人群。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			功能介绍：\r\n		</h1>\r\n		<p>\r\n			调节平衡、疏通经络、帮助气血运行及祛湿。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			用品提供：\r\n		</h1>\r\n		<p>\r\n			火罐、清洁纱布、酒精、浴布、口罩。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			订单须知：\r\n		</h1>\r\n		<p>\r\n			1.技师将在您下单后30分钟内确认是否接单。\r\n		</p>\r\n		<p>\r\n			2.订单确认后，您修改订单时间或退款操作请与客服联系。\r\n		</p>\r\n		<p>\r\n			3.如技师接单后爽约将全额退款给您，并向您赠送优惠券。\r\n		</p>\r\n		<p>\r\n			4.为保障您的权益，消费订单请通过喜脚么平台支付。\r\n		</p>\r\n		<p>\r\n			5.喜脚么提供专业、标准服务平台，对于客户有穿着暴露或不雅行为，技师有权利拒绝服务，并保留法律权利。\r\n		</p>\r\n		<p>\r\n			6.因夜间技师往返交通不便，晚22:00至早06:00间服务的订单，需另行向技师支付往返打车费用20元。\r\n		</p>\r\n		<p>\r\n			7.考虑夜间安全性，晚上20:00后客户只能选择男技师上门服务。\r\n		</p>\r\n	</li>\r\n</ul>', '1512529818', '0');
INSERT INTO `xjm_article` VALUES ('23', '修脚--上门服务项目详细内容', '4', '0', '修脚--上门服务项目详细内容', '1510571000', '<ul>\r\n	<li style=\"color:#D5201E;\">\r\n		<h1>\r\n			禁忌说明：\r\n		</h1>\r\n		<p>\r\n			脚部严重发炎、足癣患人群不宜修脚。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			服务流程：\r\n		</h1>\r\n		<p>\r\n			1.以橙木棒沾取软皮去除霜，涂抹于软皮上，用指腹一边按摩，一边涂抹。\r\n		</p>\r\n		<p>\r\n			2.将双脚在温水中浸泡5～10分钟，以使角质充分软化。\r\n		</p>\r\n		<p>\r\n			3.用毛巾拭干水分，将化妆棉包裹于橙木棒顶端，轻轻推压软皮，保持脚指甲的正方形形状。\r\n		</p>\r\n		<p>\r\n			4.将纱布缠绕在拇指或食指指尖，轻轻按摩脚指甲表面，去除老死的软皮屑。\r\n		</p>\r\n		<p>\r\n			5.用软皮剪剪除肉刺、角质的处理。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			适用人群：\r\n		</h1>\r\n		<p>\r\n			长期穿高跟鞋女性，行走脚部有疼痛人群。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			功能介绍：\r\n		</h1>\r\n		<p>\r\n			清洁脚趾，预防疾病，改善体质。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			用品提供：\r\n		</h1>\r\n		<p>\r\n			口罩、修指工具、按摩巾、软皮出除霜、足疗桶。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			订单须知：\r\n		</h1>\r\n		<p>\r\n			1.技师将在您下单后30分钟内确认是否接单。\r\n		</p>\r\n		<p>\r\n			2.订单确认后，您修改订单时间或退款操作请与客服联系。\r\n		</p>\r\n		<p>\r\n			3.如技师接单后爽约将全额退款给您，并向您赠送优惠券。\r\n		</p>\r\n		<p>\r\n			4.为保障您的权益，消费订单请通过喜脚么平台支付。\r\n		</p>\r\n		<p>\r\n			5.喜脚么提供专业、标准服务平台，对于客户有穿着暴露或不雅行为，技师有权利拒绝服务，并保留法律权利。\r\n		</p>\r\n		<p>\r\n			6.因夜间技师往返交通不便，晚22:00至早06:00间服务的订单，需另行向技师支付往返打车费用20元。\r\n		</p>\r\n		<p>\r\n			7.考虑夜间安全性，晚上20:00后客户只能选择男技师上门服务。\r\n		</p>\r\n	</li>\r\n</ul>', '1512529829', '0');
INSERT INTO `xjm_article` VALUES ('24', '采耳--上门服务项目详细内容', '4', '0', '采耳--上门服务项目详细内容', '1510571084', '<ul>\r\n	<li style=\"color:#D5201E;\">\r\n		<h1>\r\n			禁忌说明：\r\n		</h1>\r\n		<p>\r\n			高血压、心脏病不宜。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			服务流程：\r\n		</h1>\r\n		<p>\r\n			耳部深度清洁、耳部内外清洁保养、耳部头部穴位按摩调理。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			适用人群：\r\n		</h1>\r\n		<p>\r\n			中耳炎、耳鸣，以及因工作压力大、熬夜、生活不规律、内分泌失调等引起失眠多梦的人群。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			功能介绍：\r\n		</h1>\r\n		<p>\r\n			舒缓压力、清洁放松耳道、缓解失眠。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			用品提供：\r\n		</h1>\r\n		<p>\r\n			一次性封闭袋、口罩、鞋套、采耳工具。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			订单须知：\r\n		</h1>\r\n		<p>\r\n			1.技师将在您下单后30分钟内确认是否接单。\r\n		</p>\r\n		<p>\r\n			2.订单确认后，您修改订单时间或退款操作请与客服联系。\r\n		</p>\r\n		<p>\r\n			3.如技师接单后爽约将全额退款给您，并向您赠送优惠券。\r\n		</p>\r\n		<p>\r\n			4.为保障您的权益，消费订单请通过喜脚么平台支付。\r\n		</p>\r\n		<p>\r\n			5.喜脚么提供专业、标准服务平台，对于客户有穿着暴露或不雅行为，技师有权利拒绝服务，并保留法律权利。\r\n		</p>\r\n<p>\r\n			6.因夜间技师往返交通不便，晚22:00至早06:00间服务的订单，需另行向技师支付往返打车费用20元。\r\n		</p>\r\n<p>\r\n			7.考虑夜间安全性，晚上20:00后客户只能选择男技师上门服务。\r\n		</p>\r\n	</li>\r\n</ul>', '1512529844', '0');
INSERT INTO `xjm_article` VALUES ('25', '艾灸--上门服务项目详细内容', '4', '0', '艾灸--上门服务项目详细内容', '1510571288', '<ul>\r\n	<li style=\"color:#D5201E;\">\r\n		<h1>\r\n			禁忌说明：\r\n		</h1>\r\n		<p>\r\n			热性体质、女性例假期间不可艾灸。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			服务流程：\r\n		</h1>\r\n		<p>\r\n			1.按前检查辨别健康状况，询问具体症状，制定调理方案。\r\n		</p>\r\n		<p>\r\n			2.选择合适的躺姿。\r\n		</p>\r\n		<p>\r\n			3.根据您体质进行15至20分钟按摩放松调理气血。\r\n		</p>\r\n		<p>\r\n			4.点燃艾条在您穴位或病变部位进行熏烤。\r\n		</p>\r\n		<p>\r\n			5.艾灸结束，结合理疗状况提供健康咨询及建议。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			适用人群：\r\n		</h1>\r\n		<p>\r\n			压力大、抑郁、心烦气躁、易疲劳，寒性体质人群。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			功能介绍：\r\n		</h1>\r\n		<p>\r\n			调和气血、消肿散结、祛湿散寒、改善体质、改善睡眠、增强脾胃。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			用品提供：\r\n		</h1>\r\n		<p>\r\n			鞋套、口罩、床单、按摩布、艾条。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			订单须知：\r\n		</h1>\r\n		<p>\r\n			1.技师将在您下单后30分钟内确认是否接单。\r\n		</p>\r\n		<p>\r\n			2.订单确认后，您修改订单时间或退款操作请与客服联系。\r\n		</p>\r\n		<p>\r\n			3.如技师接单后爽约将全额退款给您，并向您赠送优惠券。\r\n		</p>\r\n		<p>\r\n			4.为保障您的权益，消费订单请通过喜脚么平台支付。\r\n		</p>\r\n		<p>\r\n			5.喜脚么提供专业、标准服务平台，对于客户有穿着暴露或不雅行为，技师有权利拒绝服务，并保留法律权利。\r\n		</p>\r\n<p>\r\n			6.因夜间技师往返交通不便，晚22:00至早06:00间服务的订单，需另行向技师支付往返打车费用20元。\r\n		</p>\r\n<p>\r\n			7.考虑夜间安全性，晚上20:00后客户只能选择男技师上门服务。\r\n		</p>\r\n	</li>\r\n</ul>', '1512529859', '0');
INSERT INTO `xjm_article` VALUES ('26', '推拿--上门服务项目详细内容', '4', '0', '推拿--上门服务项目详细内容', '1510571390', '<ul>\r\n	<li style=\"color:#D5201E;\">\r\n		<h1>\r\n			禁忌说明：\r\n		</h1>\r\n		<p>\r\n			妇女经期、备孕期，有严重心血管病的或高龄体弱人群不宜。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			服务流程：\r\n		</h1>\r\n		<p>\r\n			1.按前检查辨别健康状况，询问具体症状，制定调理方案。\r\n		</p>\r\n		<p>\r\n			2.选择合适卧姿。\r\n		</p>\r\n		<p>\r\n			3.根据您的体质选择合适的穴位、经络进行推拿。\r\n		</p>\r\n		<p>\r\n			4.调理结束，结合身体状况提供健康咨询及建议。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			适用人群：\r\n		</h1>\r\n		<p>\r\n			工作压力大、缺乏锻炼、经常熬夜、腰酸背痛、全身倦怠、长期工作繁忙、精神紧张、腰间盘突出、腰肌劳损等人群。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			功能介绍：\r\n		</h1>\r\n		<p>\r\n			缓解疲劳、疏通经络、调理落枕、改善睡眠，调理脏腑、增强人体免疫力。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			用品提供：\r\n		</h1>\r\n		<p>\r\n			一次性鞋套、口罩、床单、按摩布。\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<h1>\r\n			订单须知：\r\n		</h1>\r\n		<p>\r\n			1.技师将在您下单后30分钟内确认是否接单。\r\n		</p>\r\n		<p>\r\n			2.订单确认后，您修改订单时间或退款操作请与客服联系。\r\n		</p>\r\n		<p>\r\n			3.如技师接单后爽约将全额退款给您，并向您赠送优惠券。\r\n		</p>\r\n		<p>\r\n			4.为保障您的权益，消费订单请通过喜脚么平台支付。\r\n		</p>\r\n		<p>\r\n			5.喜脚么提供专业、标准服务平台，对于客户有穿着暴露或不雅行为，技师有权利拒绝服务，并保留法律权利。\r\n		</p>\r\n<p>\r\n			6.因夜间技师往返交通不便，晚22:00至早06:00间服务的订单，需另行向技师支付往返打车费用20元。\r\n		</p>\r\n<p>\r\n			7.考虑夜间安全性，晚上20:00后客户只能选择男技师上门服务。\r\n		</p>\r\n	</li>\r\n</ul>', '1512529868', '0');
INSERT INTO `xjm_article` VALUES ('27', '技师分享规则说明', '4', '0', '技师分享规则说明', '1511530471', '<p>\r\n	1、邀请好友注册必须为本页面，即推荐人分享链接和二维码完成注册、激活、认证，才可参与活动。\r\n</p>\r\n<p>\r\n	2、技师端分享，每推荐一个人奖励5元优惠券。\r\n</p>\r\n<p>\r\n	3、在分享页面下方我的分享奖励可以看见自己奖励的优惠券和分享成功注册的好友。\r\n</p>\r\n<p>\r\n	4、每个用户的累积奖励上限为100元。\r\n</p>\r\n<p>\r\n	5、被推荐技师使用产品后15天，推荐人可领取金额。\r\n</p>\r\n<p>\r\n	6、如发现推荐双方有作弊等不正当行为，将取消和追回所获奖励\r\n</p>', '1511530471', '0');
INSERT INTO `xjm_article` VALUES ('28', '用户分享协议', '4', '0', '用户分享协议', '1511530912', '<p>\r\n	1、邀请好友注册必须为本页面，即推荐人分享链接和二维码完成注册、激活、认证，才可参与活动。\r\n</p>\r\n<p>\r\n	2、客户端分享给其他用户，每推荐一个人奖励5元优惠券。\r\n</p>\r\n<p>\r\n	3、在分享页面下方我的分享奖励可以看见自己奖励的优惠券和分享成功注册的好友。\r\n</p>\r\n<p>\r\n	4、每个用户的累积奖励上限为100元优惠券。\r\n</p>\r\n<p>\r\n	5、如发现推荐双方有作弊等不正当行为，将取消和追回所获奖励\r\n</p>', '1511530912', '0');
INSERT INTO `xjm_article` VALUES ('29', '用户端公告协议', '4', '0', '用户端公告协议', '1512437417', '请您认真阅读《喜脚么使用公告》中各条款并充分理解，除非您同意本公告，否则您无权也无必要继续接受喜脚么的服务，可以退出应用。您点击同意并继续使用喜脚么的服务，视为您已完全同意本公告。\r\n1.您确定上门服务地址并确认下单支付后，如果服务地址有改动请及时和接单技师联系协商确定新上门服务地址，在此过程中技师有权拒绝上门服务，相关费用由您承担。\r\n2.您选择预约晚上8点以后提供上门服务，只能选择男技师。预约晚上11点以后提供上门服务，应当另行增加20元夜班费。\r\n3.您的退单和取消订单，退款将退在喜脚么平台账户，该款不得提现，只能用于平台消费。享受充值赠费优惠的，充值成功后，该款与赠费也在喜脚么账户，不得提现，只能用于平台消费。\r\n本协议一经同意，具有法律效力，请您慎重考虑是否同意本协议。', '1512630164', '0');
INSERT INTO `xjm_article` VALUES ('30', '技师端公告协议', '4', '0', '技师端公告协议', '1512437434', '请您认真阅读《喜脚么技师使用公告》中各条款并充分理解，除非您同意本公告，否则您无权也无必要继续接受喜脚么的服务，可以退出应用。您点击同意并继续使用喜脚么的服务，视为您已完全同意本公告。\r\n1.您在注册上传头像，请提供个人真实登记照提交审核。\r\n2.上传个人相册与视频，请提供规范服务照与服务视频提交审核。\r\n3.您接到订单后，如果客户服务地址有改动及时协商并确定新上门服务地址，在此过程中您有权拒绝服务，并得到相应费用。\r\n4.晚上8点以后应用只展示男技师提供上门服务，客户预约晚上11点以后提供上门服务，您可以得到20元夜班费补偿。\r\n本协议一经同意，具有法律效力，请您慎重考虑是否同意本协议。', '1512630136', '0');

-- ----------------------------
-- Table structure for xjm_banner
-- ----------------------------
DROP TABLE IF EXISTS `xjm_banner`;
CREATE TABLE `xjm_banner` (
  `id` tinyint(2) unsigned NOT NULL AUTO_INCREMENT,
  `img_url` varchar(100) NOT NULL DEFAULT '' COMMENT '图片地址',
  `sort` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '排序',
  `time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='首页banner';

-- ----------------------------
-- Records of xjm_banner
-- ----------------------------
INSERT INTO `xjm_banner` VALUES ('1', 'http://pic.xijiaomo.com/banner.png', '1', '0');
INSERT INTO `xjm_banner` VALUES ('2', 'http://pic.xijiaomo.com/banner3.png', '2', '0');
INSERT INTO `xjm_banner` VALUES ('3', 'http://pic.xijiaomo.com/banner1.png', '3', '0');
INSERT INTO `xjm_banner` VALUES ('4', 'http://pic.xijiaomo.com/banner5.png', '4', '0');

-- ----------------------------
-- Table structure for xjm_city
-- ----------------------------
DROP TABLE IF EXISTS `xjm_city`;
CREATE TABLE `xjm_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aid` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0' COMMENT '0不显示，1显示',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1086 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xjm_city
-- ----------------------------
INSERT INTO `xjm_city` VALUES ('1', '0', '黑龙江', '0');
INSERT INTO `xjm_city` VALUES ('2', '0', '吉林', '0');
INSERT INTO `xjm_city` VALUES ('3', '0', '辽宁', '0');
INSERT INTO `xjm_city` VALUES ('5', '0', '内蒙古', '0');
INSERT INTO `xjm_city` VALUES ('6', '0', '北京', '0');
INSERT INTO `xjm_city` VALUES ('7', '0', '河北', '0');
INSERT INTO `xjm_city` VALUES ('8', '0', '山东', '0');
INSERT INTO `xjm_city` VALUES ('9', '0', '山西', '0');
INSERT INTO `xjm_city` VALUES ('10', '0', '天津', '0');
INSERT INTO `xjm_city` VALUES ('11', '0', '河南', '0');
INSERT INTO `xjm_city` VALUES ('12', '0', '安徽', '0');
INSERT INTO `xjm_city` VALUES ('13', '0', '湖北', '0');
INSERT INTO `xjm_city` VALUES ('14', '0', '湖南', '0');
INSERT INTO `xjm_city` VALUES ('15', '0', '江西', '0');
INSERT INTO `xjm_city` VALUES ('16', '0', '江苏', '0');
INSERT INTO `xjm_city` VALUES ('17', '0', '上海', '0');
INSERT INTO `xjm_city` VALUES ('18', '0', '浙江', '0');
INSERT INTO `xjm_city` VALUES ('19', '0', '福建', '0');
INSERT INTO `xjm_city` VALUES ('20', '0', '广东', '0');
INSERT INTO `xjm_city` VALUES ('21', '0', '广西', '0');
INSERT INTO `xjm_city` VALUES ('22', '0', '海南', '0');
INSERT INTO `xjm_city` VALUES ('23', '0', '四川', '0');
INSERT INTO `xjm_city` VALUES ('24', '0', '西藏', '0');
INSERT INTO `xjm_city` VALUES ('25', '0', '贵州', '0');
INSERT INTO `xjm_city` VALUES ('26', '0', '云南', '0');
INSERT INTO `xjm_city` VALUES ('27', '0', '重庆', '0');
INSERT INTO `xjm_city` VALUES ('28', '0', '甘肃', '0');
INSERT INTO `xjm_city` VALUES ('29', '0', '陕西', '0');
INSERT INTO `xjm_city` VALUES ('30', '0', '新疆', '0');
INSERT INTO `xjm_city` VALUES ('31', '0', '青海', '0');
INSERT INTO `xjm_city` VALUES ('32', '0', '宁夏', '0');
INSERT INTO `xjm_city` VALUES ('33', '31', '格尔木', '0');
INSERT INTO `xjm_city` VALUES ('34', '1', '绥化', '0');
INSERT INTO `xjm_city` VALUES ('35', '1', '哈尔滨', '0');
INSERT INTO `xjm_city` VALUES ('36', '1', '大庆', '0');
INSERT INTO `xjm_city` VALUES ('37', '1', '牡丹江', '0');
INSERT INTO `xjm_city` VALUES ('38', '1', '齐齐哈尔', '0');
INSERT INTO `xjm_city` VALUES ('39', '1', '佳木斯', '0');
INSERT INTO `xjm_city` VALUES ('40', '1', '鸡西', '0');
INSERT INTO `xjm_city` VALUES ('41', '1', '鹤岗', '0');
INSERT INTO `xjm_city` VALUES ('42', '1', '双鸭山', '0');
INSERT INTO `xjm_city` VALUES ('43', '1', '七台河', '0');
INSERT INTO `xjm_city` VALUES ('44', '1', '加格达奇', '0');
INSERT INTO `xjm_city` VALUES ('45', '1', '黑河', '0');
INSERT INTO `xjm_city` VALUES ('46', '1', '五常', '0');
INSERT INTO `xjm_city` VALUES ('47', '2', '长春', '0');
INSERT INTO `xjm_city` VALUES ('48', '2', '吉林', '0');
INSERT INTO `xjm_city` VALUES ('49', '2', '延吉', '0');
INSERT INTO `xjm_city` VALUES ('50', '2', '四平', '0');
INSERT INTO `xjm_city` VALUES ('51', '2', '松原', '0');
INSERT INTO `xjm_city` VALUES ('52', '2', '通化', '0');
INSERT INTO `xjm_city` VALUES ('53', '2', '辽源', '0');
INSERT INTO `xjm_city` VALUES ('54', '3', '鞍山', '0');
INSERT INTO `xjm_city` VALUES ('55', '3', '朝阳', '0');
INSERT INTO `xjm_city` VALUES ('56', '3', '大连', '0');
INSERT INTO `xjm_city` VALUES ('57', '3', '丹东', '0');
INSERT INTO `xjm_city` VALUES ('58', '3', '阜新', '0');
INSERT INTO `xjm_city` VALUES ('59', '3', '抚顺', '0');
INSERT INTO `xjm_city` VALUES ('60', '3', '葫芦岛', '0');
INSERT INTO `xjm_city` VALUES ('61', '3', '锦州', '0');
INSERT INTO `xjm_city` VALUES ('62', '3', '铁岭', '0');
INSERT INTO `xjm_city` VALUES ('63', '3', '辽阳', '0');
INSERT INTO `xjm_city` VALUES ('64', '3', '本溪', '0');
INSERT INTO `xjm_city` VALUES ('65', '3', '盘锦', '0');
INSERT INTO `xjm_city` VALUES ('66', '3', '沈阳', '0');
INSERT INTO `xjm_city` VALUES ('67', '3', '营口', '0');
INSERT INTO `xjm_city` VALUES ('68', '5', '包头', '0');
INSERT INTO `xjm_city` VALUES ('69', '5', '呼和浩特', '0');
INSERT INTO `xjm_city` VALUES ('70', '5', '通辽', '0');
INSERT INTO `xjm_city` VALUES ('71', '5', '乌海', '0');
INSERT INTO `xjm_city` VALUES ('72', '5', '伊克昭盟', '0');
INSERT INTO `xjm_city` VALUES ('73', '5', '赤峰', '0');
INSERT INTO `xjm_city` VALUES ('74', '5', '锡林浩特', '0');
INSERT INTO `xjm_city` VALUES ('75', '5', '乌兰浩特', '0');
INSERT INTO `xjm_city` VALUES ('76', '5', '呼伦贝尔', '0');
INSERT INTO `xjm_city` VALUES ('77', '5', '霍林郭勒', '0');
INSERT INTO `xjm_city` VALUES ('78', '5', '巴彦淖尔', '0');
INSERT INTO `xjm_city` VALUES ('79', '6', '北京', '1');
INSERT INTO `xjm_city` VALUES ('80', '7', '保定', '0');
INSERT INTO `xjm_city` VALUES ('82', '7', '沧州', '0');
INSERT INTO `xjm_city` VALUES ('83', '7', '承德', '0');
INSERT INTO `xjm_city` VALUES ('84', '7', '邯郸', '0');
INSERT INTO `xjm_city` VALUES ('85', '7', '衡水', '0');
INSERT INTO `xjm_city` VALUES ('86', '7', '廊坊', '0');
INSERT INTO `xjm_city` VALUES ('87', '7', '秦皇岛', '0');
INSERT INTO `xjm_city` VALUES ('88', '7', '石家庄', '0');
INSERT INTO `xjm_city` VALUES ('89', '7', '唐山', '0');
INSERT INTO `xjm_city` VALUES ('90', '7', '邢台', '0');
INSERT INTO `xjm_city` VALUES ('91', '7', '张家口', '0');
INSERT INTO `xjm_city` VALUES ('92', '7', '遵化', '0');
INSERT INTO `xjm_city` VALUES ('93', '8', '菏泽', '0');
INSERT INTO `xjm_city` VALUES ('94', '8', '济南', '0');
INSERT INTO `xjm_city` VALUES ('95', '8', '济宁', '0');
INSERT INTO `xjm_city` VALUES ('96', '8', '聊城', '0');
INSERT INTO `xjm_city` VALUES ('97', '8', '临沂', '0');
INSERT INTO `xjm_city` VALUES ('98', '8', '鲁东', '0');
INSERT INTO `xjm_city` VALUES ('99', '8', '枣庄', '0');
INSERT INTO `xjm_city` VALUES ('100', '8', '青岛', '0');
INSERT INTO `xjm_city` VALUES ('101', '8', '泰安', '0');
INSERT INTO `xjm_city` VALUES ('102', '8', '威海', '0');
INSERT INTO `xjm_city` VALUES ('103', '8', '潍坊', '0');
INSERT INTO `xjm_city` VALUES ('104', '8', '烟台', '0');
INSERT INTO `xjm_city` VALUES ('105', '8', '淄博', '0');
INSERT INTO `xjm_city` VALUES ('106', '8', '东营', '0');
INSERT INTO `xjm_city` VALUES ('107', '8', '日照', '0');
INSERT INTO `xjm_city` VALUES ('108', '8', '滨州', '0');
INSERT INTO `xjm_city` VALUES ('109', '8', '德州', '0');
INSERT INTO `xjm_city` VALUES ('110', '9', '长治', '0');
INSERT INTO `xjm_city` VALUES ('111', '9', '大同', '0');
INSERT INTO `xjm_city` VALUES ('112', '9', '晋城', '0');
INSERT INTO `xjm_city` VALUES ('113', '9', '晋中', '0');
INSERT INTO `xjm_city` VALUES ('114', '9', '临汾', '0');
INSERT INTO `xjm_city` VALUES ('115', '9', '吕梁', '0');
INSERT INTO `xjm_city` VALUES ('116', '9', '太原', '0');
INSERT INTO `xjm_city` VALUES ('117', '9', '忻州', '0');
INSERT INTO `xjm_city` VALUES ('118', '9', '阳泉', '0');
INSERT INTO `xjm_city` VALUES ('119', '9', '运城', '0');
INSERT INTO `xjm_city` VALUES ('120', '9', '朔州', '0');
INSERT INTO `xjm_city` VALUES ('121', '9', '河津', '0');
INSERT INTO `xjm_city` VALUES ('122', '10', '天津', '0');
INSERT INTO `xjm_city` VALUES ('123', '11', '信阳', '0');
INSERT INTO `xjm_city` VALUES ('124', '11', '郑州', '0');
INSERT INTO `xjm_city` VALUES ('125', '11', '洛阳', '0');
INSERT INTO `xjm_city` VALUES ('126', '11', '平顶山', '0');
INSERT INTO `xjm_city` VALUES ('127', '11', '安阳', '0');
INSERT INTO `xjm_city` VALUES ('128', '11', '许昌', '0');
INSERT INTO `xjm_city` VALUES ('129', '11', '南阳', '0');
INSERT INTO `xjm_city` VALUES ('130', '11', '新乡', '0');
INSERT INTO `xjm_city` VALUES ('131', '11', '焦作', '0');
INSERT INTO `xjm_city` VALUES ('132', '11', '商丘', '0');
INSERT INTO `xjm_city` VALUES ('133', '11', '周口', '0');
INSERT INTO `xjm_city` VALUES ('134', '11', '驻马店', '0');
INSERT INTO `xjm_city` VALUES ('135', '11', '开封', '0');
INSERT INTO `xjm_city` VALUES ('136', '11', '濮阳', '0');
INSERT INTO `xjm_city` VALUES ('137', '11', '三门峡', '0');
INSERT INTO `xjm_city` VALUES ('138', '11', '漯河', '0');
INSERT INTO `xjm_city` VALUES ('139', '11', '鹤壁', '0');
INSERT INTO `xjm_city` VALUES ('140', '12', '安庆', '0');
INSERT INTO `xjm_city` VALUES ('141', '12', '蚌埠', '0');
INSERT INTO `xjm_city` VALUES ('142', '12', '阜阳', '0');
INSERT INTO `xjm_city` VALUES ('143', '12', '合肥', '0');
INSERT INTO `xjm_city` VALUES ('144', '12', '六安', '0');
INSERT INTO `xjm_city` VALUES ('145', '12', '黄山', '0');
INSERT INTO `xjm_city` VALUES ('146', '12', '芜湖', '0');
INSERT INTO `xjm_city` VALUES ('147', '12', '宿州', '0');
INSERT INTO `xjm_city` VALUES ('148', '12', '铜陵', '0');
INSERT INTO `xjm_city` VALUES ('149', '12', '池州', '0');
INSERT INTO `xjm_city` VALUES ('150', '13', '黄石', '0');
INSERT INTO `xjm_city` VALUES ('151', '13', '荆州', '0');
INSERT INTO `xjm_city` VALUES ('152', '13', '荆门', '0');
INSERT INTO `xjm_city` VALUES ('153', '13', '武汉', '2');
INSERT INTO `xjm_city` VALUES ('154', '13', '襄阳', '1');
INSERT INTO `xjm_city` VALUES ('155', '13', '孝感', '0');
INSERT INTO `xjm_city` VALUES ('156', '13', '宜昌', '0');
INSERT INTO `xjm_city` VALUES ('157', '13', '咸宁', '0');
INSERT INTO `xjm_city` VALUES ('158', '13', '黄冈', '0');
INSERT INTO `xjm_city` VALUES ('159', '13', '恩施', '1');
INSERT INTO `xjm_city` VALUES ('160', '13', '十堰', '0');
INSERT INTO `xjm_city` VALUES ('161', '13', '仙桃', '0');
INSERT INTO `xjm_city` VALUES ('162', '13', '随州', '0');
INSERT INTO `xjm_city` VALUES ('163', '13', '潜江', '0');
INSERT INTO `xjm_city` VALUES ('164', '14', '长沙', '0');
INSERT INTO `xjm_city` VALUES ('165', '14', '岳阳', '0');
INSERT INTO `xjm_city` VALUES ('166', '14', '衡阳', '0');
INSERT INTO `xjm_city` VALUES ('167', '14', '郴州', '0');
INSERT INTO `xjm_city` VALUES ('168', '14', '常德', '0');
INSERT INTO `xjm_city` VALUES ('169', '14', '湘潭', '0');
INSERT INTO `xjm_city` VALUES ('170', '14', '株洲', '0');
INSERT INTO `xjm_city` VALUES ('171', '14', '邵阳', '0');
INSERT INTO `xjm_city` VALUES ('172', '14', '娄底', '0');
INSERT INTO `xjm_city` VALUES ('173', '14', '永州', '0');
INSERT INTO `xjm_city` VALUES ('174', '14', '怀化', '0');
INSERT INTO `xjm_city` VALUES ('175', '14', '益阳', '0');
INSERT INTO `xjm_city` VALUES ('176', '14', '张家界', '0');
INSERT INTO `xjm_city` VALUES ('177', '14', '攸县', '0');
INSERT INTO `xjm_city` VALUES ('178', '14', '耒阳', '0');
INSERT INTO `xjm_city` VALUES ('179', '15', '赣州', '0');
INSERT INTO `xjm_city` VALUES ('181', '15', '吉安', '0');
INSERT INTO `xjm_city` VALUES ('182', '15', '南昌', '0');
INSERT INTO `xjm_city` VALUES ('183', '15', '宜春', '0');
INSERT INTO `xjm_city` VALUES ('184', '15', '抚州', '0');
INSERT INTO `xjm_city` VALUES ('185', '15', '上饶', '0');
INSERT INTO `xjm_city` VALUES ('186', '15', '萍乡', '0');
INSERT INTO `xjm_city` VALUES ('187', '15', '九江', '0');
INSERT INTO `xjm_city` VALUES ('188', '15', '新余', '0');
INSERT INTO `xjm_city` VALUES ('189', '15', '鹰潭', '0');
INSERT INTO `xjm_city` VALUES ('190', '15', '景德镇', '0');
INSERT INTO `xjm_city` VALUES ('191', '16', '常州', '0');
INSERT INTO `xjm_city` VALUES ('192', '16', '连云港', '0');
INSERT INTO `xjm_city` VALUES ('193', '16', '淮安', '0');
INSERT INTO `xjm_city` VALUES ('194', '16', '南京', '0');
INSERT INTO `xjm_city` VALUES ('195', '16', '南通', '0');
INSERT INTO `xjm_city` VALUES ('196', '16', '苏州', '0');
INSERT INTO `xjm_city` VALUES ('197', '16', '宿迁', '0');
INSERT INTO `xjm_city` VALUES ('198', '16', '靖江', '0');
INSERT INTO `xjm_city` VALUES ('199', '16', '泰州', '0');
INSERT INTO `xjm_city` VALUES ('200', '16', '镇江', '0');
INSERT INTO `xjm_city` VALUES ('201', '16', '无锡', '0');
INSERT INTO `xjm_city` VALUES ('202', '16', '徐州', '0');
INSERT INTO `xjm_city` VALUES ('203', '16', '盐城', '0');
INSERT INTO `xjm_city` VALUES ('204', '16', '扬州', '0');
INSERT INTO `xjm_city` VALUES ('205', '16', '江阴', '0');
INSERT INTO `xjm_city` VALUES ('206', '16', '海安', '0');
INSERT INTO `xjm_city` VALUES ('207', '17', '上海', '1');
INSERT INTO `xjm_city` VALUES ('208', '18', '杭州', '0');
INSERT INTO `xjm_city` VALUES ('209', '18', '湖州', '0');
INSERT INTO `xjm_city` VALUES ('210', '18', '嘉兴', '0');
INSERT INTO `xjm_city` VALUES ('211', '18', '金华', '0');
INSERT INTO `xjm_city` VALUES ('212', '18', '宁波', '0');
INSERT INTO `xjm_city` VALUES ('213', '18', '衢州', '0');
INSERT INTO `xjm_city` VALUES ('214', '18', '绍兴', '0');
INSERT INTO `xjm_city` VALUES ('215', '18', '台州', '0');
INSERT INTO `xjm_city` VALUES ('216', '18', '温州', '0');
INSERT INTO `xjm_city` VALUES ('217', '18', '舟山', '0');
INSERT INTO `xjm_city` VALUES ('218', '18', '丽水', '0');
INSERT INTO `xjm_city` VALUES ('219', '18', '慈溪', '0');
INSERT INTO `xjm_city` VALUES ('220', '18', '临安', '0');
INSERT INTO `xjm_city` VALUES ('221', '19', '福州', '0');
INSERT INTO `xjm_city` VALUES ('222', '19', '泉州', '0');
INSERT INTO `xjm_city` VALUES ('223', '19', '三明', '0');
INSERT INTO `xjm_city` VALUES ('224', '19', '莆田', '0');
INSERT INTO `xjm_city` VALUES ('225', '19', '南平', '0');
INSERT INTO `xjm_city` VALUES ('226', '19', '宁德', '0');
INSERT INTO `xjm_city` VALUES ('227', '19', '龙岩', '0');
INSERT INTO `xjm_city` VALUES ('228', '19', '厦门', '0');
INSERT INTO `xjm_city` VALUES ('229', '19', '漳州', '0');
INSERT INTO `xjm_city` VALUES ('230', '19', '福安', '0');
INSERT INTO `xjm_city` VALUES ('231', '19', '南安', '0');
INSERT INTO `xjm_city` VALUES ('232', '20', '佛山', '0');
INSERT INTO `xjm_city` VALUES ('233', '20', '东莞', '0');
INSERT INTO `xjm_city` VALUES ('234', '20', '广州', '1');
INSERT INTO `xjm_city` VALUES ('235', '20', '河源', '0');
INSERT INTO `xjm_city` VALUES ('236', '20', '惠州', '0');
INSERT INTO `xjm_city` VALUES ('237', '20', '揭阳', '0');
INSERT INTO `xjm_city` VALUES ('238', '20', '梅州', '0');
INSERT INTO `xjm_city` VALUES ('239', '20', '清远', '0');
INSERT INTO `xjm_city` VALUES ('240', '20', '汕头', '0');
INSERT INTO `xjm_city` VALUES ('241', '20', '韶关', '0');
INSERT INTO `xjm_city` VALUES ('242', '20', '潮州', '0');
INSERT INTO `xjm_city` VALUES ('243', '20', '深圳', '1');
INSERT INTO `xjm_city` VALUES ('244', '20', '粤西', '0');
INSERT INTO `xjm_city` VALUES ('245', '20', '云浮', '0');
INSERT INTO `xjm_city` VALUES ('246', '20', '阳江', '0');
INSERT INTO `xjm_city` VALUES ('247', '20', '湛江', '0');
INSERT INTO `xjm_city` VALUES ('248', '20', '肇庆', '0');
INSERT INTO `xjm_city` VALUES ('249', '20', '江门', '0');
INSERT INTO `xjm_city` VALUES ('250', '20', '中山', '0');
INSERT INTO `xjm_city` VALUES ('251', '20', '茂名', '0');
INSERT INTO `xjm_city` VALUES ('252', '20', '珠海', '0');
INSERT INTO `xjm_city` VALUES ('253', '21', '北海', '0');
INSERT INTO `xjm_city` VALUES ('254', '21', '贵港', '0');
INSERT INTO `xjm_city` VALUES ('255', '21', '桂林', '0');
INSERT INTO `xjm_city` VALUES ('257', '21', '柳州', '0');
INSERT INTO `xjm_city` VALUES ('259', '21', '南宁', '0');
INSERT INTO `xjm_city` VALUES ('263', '21', '钦州', '0');
INSERT INTO `xjm_city` VALUES ('264', '21', '玉林', '0');
INSERT INTO `xjm_city` VALUES ('265', '21', '梧州', '0');
INSERT INTO `xjm_city` VALUES ('266', '21', '百色市', '0');
INSERT INTO `xjm_city` VALUES ('267', '21', '防城港', '0');
INSERT INTO `xjm_city` VALUES ('268', '21', '贺州', '0');
INSERT INTO `xjm_city` VALUES ('269', '21', '河池', '0');
INSERT INTO `xjm_city` VALUES ('270', '21', '崇左', '0');
INSERT INTO `xjm_city` VALUES ('271', '21', '南宁', '0');
INSERT INTO `xjm_city` VALUES ('272', '21', '广西壮族自治区', '0');
INSERT INTO `xjm_city` VALUES ('273', '22', '海口', '0');
INSERT INTO `xjm_city` VALUES ('274', '22', '三亚', '0');
INSERT INTO `xjm_city` VALUES ('275', '23', '达州', '0');
INSERT INTO `xjm_city` VALUES ('276', '23', '宜宾', '0');
INSERT INTO `xjm_city` VALUES ('277', '23', '攀枝花', '0');
INSERT INTO `xjm_city` VALUES ('278', '23', '广元', '0');
INSERT INTO `xjm_city` VALUES ('279', '23', '成都', '0');
INSERT INTO `xjm_city` VALUES ('280', '23', '德阳', '0');
INSERT INTO `xjm_city` VALUES ('281', '23', '乐山', '0');
INSERT INTO `xjm_city` VALUES ('282', '23', '绵阳', '0');
INSERT INTO `xjm_city` VALUES ('283', '23', '南充', '0');
INSERT INTO `xjm_city` VALUES ('284', '23', '泸州', '0');
INSERT INTO `xjm_city` VALUES ('285', '23', '自贡', '0');
INSERT INTO `xjm_city` VALUES ('286', '23', '眉山', '0');
INSERT INTO `xjm_city` VALUES ('287', '24', '拉萨', '0');
INSERT INTO `xjm_city` VALUES ('288', '24', '日喀则', '0');
INSERT INTO `xjm_city` VALUES ('289', '25', '贵阳', '0');
INSERT INTO `xjm_city` VALUES ('290', '25', '六盘水', '0');
INSERT INTO `xjm_city` VALUES ('291', '25', '遵义', '0');
INSERT INTO `xjm_city` VALUES ('292', '25', '都匀', '0');
INSERT INTO `xjm_city` VALUES ('293', '25', '兴义', '0');
INSERT INTO `xjm_city` VALUES ('294', '25', '毕节', '0');
INSERT INTO `xjm_city` VALUES ('295', '26', '大理州', '0');
INSERT INTO `xjm_city` VALUES ('296', '26', '红河州', '0');
INSERT INTO `xjm_city` VALUES ('297', '26', '昆明市', '0');
INSERT INTO `xjm_city` VALUES ('299', '26', '曲靖', '0');
INSERT INTO `xjm_city` VALUES ('300', '26', '玉溪', '0');
INSERT INTO `xjm_city` VALUES ('301', '26', '文山', '0');
INSERT INTO `xjm_city` VALUES ('302', '26', '楚雄', '0');
INSERT INTO `xjm_city` VALUES ('303', '26', '保山', '0');
INSERT INTO `xjm_city` VALUES ('304', '26', '临沧', '0');
INSERT INTO `xjm_city` VALUES ('305', '26', '丽江', '0');
INSERT INTO `xjm_city` VALUES ('306', '26', '宏州', '0');
INSERT INTO `xjm_city` VALUES ('307', '27', '重庆', '1');
INSERT INTO `xjm_city` VALUES ('308', '27', '黔江', '0');
INSERT INTO `xjm_city` VALUES ('309', '28', '兰州', '0');
INSERT INTO `xjm_city` VALUES ('310', '28', '武威', '0');
INSERT INTO `xjm_city` VALUES ('311', '28', '酒泉', '0');
INSERT INTO `xjm_city` VALUES ('312', '28', '庆阳', '0');
INSERT INTO `xjm_city` VALUES ('313', '28', '白银', '0');
INSERT INTO `xjm_city` VALUES ('314', '28', '平凉', '0');
INSERT INTO `xjm_city` VALUES ('315', '28', '定西', '0');
INSERT INTO `xjm_city` VALUES ('316', '28', '张掖', '0');
INSERT INTO `xjm_city` VALUES ('317', '28', '嘉峪关', '0');
INSERT INTO `xjm_city` VALUES ('318', '29', '汉中', '0');
INSERT INTO `xjm_city` VALUES ('319', '29', '西安', '0');
INSERT INTO `xjm_city` VALUES ('320', '29', '航天基地', '0');
INSERT INTO `xjm_city` VALUES ('321', '29', '榆林', '0');
INSERT INTO `xjm_city` VALUES ('322', '29', '渭南', '0');
INSERT INTO `xjm_city` VALUES ('323', '29', '延安', '0');
INSERT INTO `xjm_city` VALUES ('324', '29', '宝鸡', '0');
INSERT INTO `xjm_city` VALUES ('325', '29', '安康', '0');
INSERT INTO `xjm_city` VALUES ('326', '29', '商洛', '0');
INSERT INTO `xjm_city` VALUES ('327', '29', '铜川', '0');
INSERT INTO `xjm_city` VALUES ('328', '30', '乌鲁木齐', '0');
INSERT INTO `xjm_city` VALUES ('329', '30', '库尔勒', '0');
INSERT INTO `xjm_city` VALUES ('330', '30', '克拉玛依', '0');
INSERT INTO `xjm_city` VALUES ('331', '30', '哈密', '0');
INSERT INTO `xjm_city` VALUES ('332', '30', '昌吉', '0');
INSERT INTO `xjm_city` VALUES ('333', '30', '阿克苏', '0');
INSERT INTO `xjm_city` VALUES ('334', '30', '维吾尔自治区', '0');
INSERT INTO `xjm_city` VALUES ('335', '30', '伊宁', '0');
INSERT INTO `xjm_city` VALUES ('336', '31', '西宁', '0');
INSERT INTO `xjm_city` VALUES ('337', '32', '银川', '0');
INSERT INTO `xjm_city` VALUES ('338', '32', '回族自治区', '0');

-- ----------------------------
-- Table structure for xjm_comment_tag_set
-- ----------------------------
DROP TABLE IF EXISTS `xjm_comment_tag_set`;
CREATE TABLE `xjm_comment_tag_set` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(50) DEFAULT '' COMMENT '标签名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='后台评论标签设置表';

-- ----------------------------
-- Records of xjm_comment_tag_set
-- ----------------------------
INSERT INTO `xjm_comment_tag_set` VALUES ('1', '按摩手法专业');
INSERT INTO `xjm_comment_tag_set` VALUES ('2', '穴位找的准');
INSERT INTO `xjm_comment_tag_set` VALUES ('3', '服务热情周到');
INSERT INTO `xjm_comment_tag_set` VALUES ('4', '穿着得体');
INSERT INTO `xjm_comment_tag_set` VALUES ('5', '非常细心');
INSERT INTO `xjm_comment_tag_set` VALUES ('6', '轻重适中');
INSERT INTO `xjm_comment_tag_set` VALUES ('7', '非常热情');
INSERT INTO `xjm_comment_tag_set` VALUES ('8', '服务态度不好');
INSERT INTO `xjm_comment_tag_set` VALUES ('9', '力度太重了');
INSERT INTO `xjm_comment_tag_set` VALUES ('10', '服务不到位');
INSERT INTO `xjm_comment_tag_set` VALUES ('11', '价格有点贵');
INSERT INTO `xjm_comment_tag_set` VALUES ('12', '环境不好');
INSERT INTO `xjm_comment_tag_set` VALUES ('13', '不热情');

-- ----------------------------
-- Table structure for xjm_coupon_set
-- ----------------------------
DROP TABLE IF EXISTS `xjm_coupon_set`;
CREATE TABLE `xjm_coupon_set` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT '表id',
  `name` varchar(50) NOT NULL COMMENT '优惠券名字',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '使用类型：0无门槛 1满减',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '优惠券金额',
  `condition` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '使用条件',
  `createnum` int(11) DEFAULT '0' COMMENT '发放数量',
  `start_time` int(11) DEFAULT NULL COMMENT '添加时间',
  `end_time` int(11) DEFAULT '0' COMMENT '使用时间按秒计算',
  `expoint` int(50) NOT NULL DEFAULT '0' COMMENT '积分兑换优惠券',
  `validity_time` int(11) NOT NULL COMMENT '有效期以天为单位',
  `s_type` int(1) NOT NULL DEFAULT '0' COMMENT '0可兑换优惠券  1邀请注册送优惠券',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='优惠券设置表';

-- ----------------------------
-- Records of xjm_coupon_set
-- ----------------------------
INSERT INTO `xjm_coupon_set` VALUES ('1', '通用10元优惠券', '0', '10.00', '0.00', '500', '1511366400', '1514131200', '1000', '2', '0');
INSERT INTO `xjm_coupon_set` VALUES ('2', '满150减20元优惠券', '1', '20.00', '150.00', '200', '1511366400', '1514131200', '1000', '3', '0');
INSERT INTO `xjm_coupon_set` VALUES ('3', '上门按摩优惠券', '0', '5.00', '0.00', '0', '1511366400', '1514131200', '0', '0', '1');

-- ----------------------------
-- Table structure for xjm_favorite
-- ----------------------------
DROP TABLE IF EXISTS `xjm_favorite`;
CREATE TABLE `xjm_favorite` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned DEFAULT '0' COMMENT '用户id',
  `obj_id` int(11) unsigned DEFAULT '0' COMMENT '被收藏id',
  `obj_type` varchar(20) DEFAULT '',
  `nickname` varchar(50) DEFAULT NULL,
  `on_time` int(12) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=353 DEFAULT CHARSET=utf8 COMMENT='收藏表';

-- ----------------------------
-- Records of xjm_favorite
-- ----------------------------
INSERT INTO `xjm_favorite` VALUES ('33', '22', '19', 'techer', null, '1505726704');
INSERT INTO `xjm_favorite` VALUES ('40', '22', '20', 'techer', null, '1505727121');
INSERT INTO `xjm_favorite` VALUES ('43', '22', '22', 'techer', null, '1505727464');
INSERT INTO `xjm_favorite` VALUES ('107', '22', '5', 'techer', null, '1505903758');
INSERT INTO `xjm_favorite` VALUES ('108', '22', '5', 'techer', null, '1505903760');
INSERT INTO `xjm_favorite` VALUES ('109', '22', '5', 'techer', null, '1505903779');
INSERT INTO `xjm_favorite` VALUES ('161', '34', '3', 'techer', null, '1509517681');
INSERT INTO `xjm_favorite` VALUES ('164', '36', '3', 'techer', null, '1509535859');
INSERT INTO `xjm_favorite` VALUES ('180', '37', '22', 'techer', null, '1509679213');
INSERT INTO `xjm_favorite` VALUES ('181', '37', '20', 'techer', null, '1509679230');
INSERT INTO `xjm_favorite` VALUES ('210', '31', '27', 'techer', null, '1510218529');
INSERT INTO `xjm_favorite` VALUES ('216', '46', '22', 'techer', null, '1510218562');
INSERT INTO `xjm_favorite` VALUES ('230', '31', '35', 'techer', null, '1510812285');
INSERT INTO `xjm_favorite` VALUES ('234', '32', '27', 'techer', null, '1511092811');
INSERT INTO `xjm_favorite` VALUES ('239', '38', '27', 'techer', null, '1511234701');
INSERT INTO `xjm_favorite` VALUES ('240', '38', '3', 'techer', null, '1511239687');
INSERT INTO `xjm_favorite` VALUES ('246', '35', '31', 'techer', null, '1511332326');
INSERT INTO `xjm_favorite` VALUES ('247', '46', '38', 'techer', null, '1511343893');
INSERT INTO `xjm_favorite` VALUES ('248', '40', '27', 'techer', null, '1511344997');
INSERT INTO `xjm_favorite` VALUES ('249', '38', '29', 'techer', null, '1511345499');
INSERT INTO `xjm_favorite` VALUES ('252', '68', '36', 'techer', null, '1511411285');
INSERT INTO `xjm_favorite` VALUES ('265', '40', '43', 'techer', null, '1511620022');
INSERT INTO `xjm_favorite` VALUES ('266', '32', '43', 'techer', null, '1511622746');
INSERT INTO `xjm_favorite` VALUES ('270', '36', '43', 'techer', null, '1511701250');
INSERT INTO `xjm_favorite` VALUES ('271', '77', '3', 'techer', null, '1511702229');
INSERT INTO `xjm_favorite` VALUES ('274', '37', '29', 'techer', null, '1511791588');
INSERT INTO `xjm_favorite` VALUES ('275', '36', '26', 'techer', null, '1511845179');
INSERT INTO `xjm_favorite` VALUES ('277', '32', '48', 'techer', null, '1511942988');
INSERT INTO `xjm_favorite` VALUES ('278', '32', '3', 'techer', null, '1512043107');
INSERT INTO `xjm_favorite` VALUES ('279', '32', '2', 'techer', null, '1512049484');
INSERT INTO `xjm_favorite` VALUES ('280', '31', '2', 'techer', null, '1512118245');
INSERT INTO `xjm_favorite` VALUES ('281', '31', '48', 'techer', null, '1512120104');
INSERT INTO `xjm_favorite` VALUES ('283', '36', '58', 'techer', null, '1512548740');
INSERT INTO `xjm_favorite` VALUES ('288', '181', '2', 'techer', null, '1514615195');
INSERT INTO `xjm_favorite` VALUES ('289', '186', '2', 'techer', null, '1514904475');
INSERT INTO `xjm_favorite` VALUES ('290', '96', '2', 'techer', null, '1514981066');
INSERT INTO `xjm_favorite` VALUES ('291', '187', '39', 'techer', null, '1515082462');
INSERT INTO `xjm_favorite` VALUES ('297', '105', '69', 'techer', null, '1519710520');
INSERT INTO `xjm_favorite` VALUES ('298', '105', '39', 'techer', null, '1519710523');
INSERT INTO `xjm_favorite` VALUES ('299', '105', '40', 'techer', null, '1519710526');
INSERT INTO `xjm_favorite` VALUES ('300', '105', '26', 'techer', null, '1519710529');
INSERT INTO `xjm_favorite` VALUES ('304', '271', '39', 'techer', null, '1521915956');
INSERT INTO `xjm_favorite` VALUES ('307', '288', '2', 'techer', null, '1522991607');
INSERT INTO `xjm_favorite` VALUES ('308', '345', '40', 'techer', null, '1527949177');
INSERT INTO `xjm_favorite` VALUES ('309', '381', '2', 'techer', null, '1530608740');
INSERT INTO `xjm_favorite` VALUES ('311', '383', '2', 'techer', null, '1530778801');
INSERT INTO `xjm_favorite` VALUES ('312', '383', '26', 'techer', null, '1530778815');
INSERT INTO `xjm_favorite` VALUES ('313', '383', '34', 'techer', null, '1530778827');
INSERT INTO `xjm_favorite` VALUES ('314', '383', '40', 'techer', null, '1530778875');
INSERT INTO `xjm_favorite` VALUES ('315', '437', '40', 'techer', null, '1533968572');
INSERT INTO `xjm_favorite` VALUES ('316', '422', '69', 'techer', null, '1534155980');
INSERT INTO `xjm_favorite` VALUES ('317', '466', '40', 'techer', null, '1535002636');
INSERT INTO `xjm_favorite` VALUES ('320', '567', '34', 'techer', null, '1538659421');
INSERT INTO `xjm_favorite` VALUES ('321', '89', '3', 'techer', null, '1542971732');
INSERT INTO `xjm_favorite` VALUES ('322', '89', '2', 'techer', null, '1543050788');
INSERT INTO `xjm_favorite` VALUES ('323', '587', '40', 'techer', null, '1543144224');
INSERT INTO `xjm_favorite` VALUES ('328', '681', '3', 'techer', null, '1545264523');
INSERT INTO `xjm_favorite` VALUES ('342', '658', '40', 'techer', null, '1546914515');
INSERT INTO `xjm_favorite` VALUES ('343', '698', '1', 'techer', null, '1546915926');
INSERT INTO `xjm_favorite` VALUES ('347', '727', '34', 'techer', null, '1547100661');
INSERT INTO `xjm_favorite` VALUES ('348', '727', '69', 'techer', null, '1547100664');
INSERT INTO `xjm_favorite` VALUES ('349', '727', '40', 'techer', null, '1547101194');
INSERT INTO `xjm_favorite` VALUES ('351', '730', '1', 'techer', null, '1547189207');
INSERT INTO `xjm_favorite` VALUES ('352', '730', '34', 'techer', null, '1547190505');

-- ----------------------------
-- Table structure for xjm_feedback
-- ----------------------------
DROP TABLE IF EXISTS `xjm_feedback`;
CREATE TABLE `xjm_feedback` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT '0' COMMENT '用户id',
  `tid` int(11) DEFAULT '0',
  `content` mediumtext COMMENT '反馈意见',
  `add_time` int(10) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COMMENT='用户反馈';

-- ----------------------------
-- Records of xjm_feedback
-- ----------------------------
INSERT INTO `xjm_feedback` VALUES ('52', '0', '37', 'lz娱乐哦www', '1511171028', '产品问题');
INSERT INTO `xjm_feedback` VALUES ('53', '0', '37', '测试', '1511939511', '产品问题');
INSERT INTO `xjm_feedback` VALUES ('54', '46', '0', '测试测试测试测试测试', '1512007487', '产品问题');
INSERT INTO `xjm_feedback` VALUES ('55', '114', '0', '测试测试测试测试', '1512554993', '产品问题');
INSERT INTO `xjm_feedback` VALUES ('56', '0', '48', 'thug', '1512611595', '功能闪退');
INSERT INTO `xjm_feedback` VALUES ('57', '134', '0', '脱欧诺克', '1513237330', '产品问题');
INSERT INTO `xjm_feedback` VALUES ('58', '212', '0', '测试测试测试测试测试测试', '1516333402', '产品问题');
INSERT INTO `xjm_feedback` VALUES ('59', '655', '0', '不错', '1544695698', '产品问题');
INSERT INTO `xjm_feedback` VALUES ('60', '655', '0', '很好', '1544695803', '产品问题');
INSERT INTO `xjm_feedback` VALUES ('61', '695', '0', '照片发不上', '1545488953', '产品问题');
INSERT INTO `xjm_feedback` VALUES ('62', '695', '0', '照片怎么传不上啊', '1545565079', '产品问题');
INSERT INTO `xjm_feedback` VALUES ('63', '658', '0', '手机输入框', '1546913845', '功能闪退');
INSERT INTO `xjm_feedback` VALUES ('64', '0', '232', 'ruanjianenaoyongo', '1547004488', '产品问题');
INSERT INTO `xjm_feedback` VALUES ('65', '730', '0', '明敏', '1547529775', '投诉');

-- ----------------------------
-- Table structure for xjm_finance
-- ----------------------------
DROP TABLE IF EXISTS `xjm_finance`;
CREATE TABLE `xjm_finance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(10) NOT NULL COMMENT 'in收入',
  `uid` int(11) DEFAULT NULL,
  `tid` int(11) DEFAULT NULL,
  `money` decimal(10,2) NOT NULL,
  `order_id` varchar(50) DEFAULT NULL,
  `pay_time` int(11) NOT NULL,
  `add_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=709 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xjm_finance
-- ----------------------------
INSERT INTO `xjm_finance` VALUES ('657', 'out', '31', '29', '0.01', '2017112394436', '1511400548', null);
INSERT INTO `xjm_finance` VALUES ('658', 'out', '35', '39', '0.01', '2017112325427', '1511402139', null);
INSERT INTO `xjm_finance` VALUES ('659', 'out', '35', '39', '0.01', '2017112318013', '1511402286', null);
INSERT INTO `xjm_finance` VALUES ('660', 'out', '35', '39', '0.01', '2017112318603', '1511402994', null);
INSERT INTO `xjm_finance` VALUES ('661', 'out', '35', '39', '0.01', '2017112365195', '1511403240', null);
INSERT INTO `xjm_finance` VALUES ('662', 'out', '35', '39', '0.01', '2017112307742', '1511403458', null);
INSERT INTO `xjm_finance` VALUES ('663', 'out', '35', '29', '0.01', '2017112300326', '1511403532', null);
INSERT INTO `xjm_finance` VALUES ('664', 'out', '35', '39', '0.01', '2017112318844', '1511404795', null);
INSERT INTO `xjm_finance` VALUES ('665', 'out', '35', '39', '0.01', '2017112347480', '1511404932', null);
INSERT INTO `xjm_finance` VALUES ('666', 'out', '40', '40', '50.00', '2017112305104', '1511432880', null);
INSERT INTO `xjm_finance` VALUES ('667', 'out', '40', '40', '50.00', '2017112328246', '1511436436', null);
INSERT INTO `xjm_finance` VALUES ('668', 'out', '40', '40', '50.00', '2017112432003', '1511488035', null);
INSERT INTO `xjm_finance` VALUES ('669', 'out', '40', '40', '50.00', '2017112427329', '1511505622', null);
INSERT INTO `xjm_finance` VALUES ('670', 'out', '40', '40', '50.00', '2017112441969', '1511514579', null);
INSERT INTO `xjm_finance` VALUES ('671', 'out', '40', '40', '50.00', '2017112445139', '1511515743', null);
INSERT INTO `xjm_finance` VALUES ('672', 'out', '40', '40', '50.00', '2017112466118', '1511515966', null);
INSERT INTO `xjm_finance` VALUES ('673', 'out', '46', '40', '50.00', '2017112423720', '1511516249', null);
INSERT INTO `xjm_finance` VALUES ('674', 'out', '32', '43', '60.00', '2017112778170', '1511753607', null);
INSERT INTO `xjm_finance` VALUES ('675', 'out', '32', '43', '60.00', '2017112757193', '1511754375', null);
INSERT INTO `xjm_finance` VALUES ('676', 'out', '34', '29', '50.00', '2017112815057', '1511837118', null);
INSERT INTO `xjm_finance` VALUES ('677', 'out', '34', '29', '60.00', '2017112855596', '1511837618', null);
INSERT INTO `xjm_finance` VALUES ('678', 'out', '77', '37', '60.00', '2017112866232', '1511851800', null);
INSERT INTO `xjm_finance` VALUES ('679', 'out', '77', '37', '60.00', '2017112860333', '1511851895', null);
INSERT INTO `xjm_finance` VALUES ('680', 'out', '31', '29', '50.00', '2017112861594', '1511852119', null);
INSERT INTO `xjm_finance` VALUES ('681', 'out', '31', '29', '50.00', '2017112844189', '1511852241', null);
INSERT INTO `xjm_finance` VALUES ('682', 'out', '81', '3', '65.00', '2017112850989', '1511857899', null);
INSERT INTO `xjm_finance` VALUES ('683', 'out', '81', '3', '65.00', '2017112809625', '1511858530', null);
INSERT INTO `xjm_finance` VALUES ('684', 'out', '40', '40', '50.00', '2017112881737', '1511859147', null);
INSERT INTO `xjm_finance` VALUES ('685', 'out', '32', '48', '88.00', '2017112967000', '1511943035', null);
INSERT INTO `xjm_finance` VALUES ('686', 'out', '31', '29', '88.00', '2017112940263', '1511943095', null);
INSERT INTO `xjm_finance` VALUES ('687', 'out', '81', '29', '88.00', '2017112945625', '1511949494', null);
INSERT INTO `xjm_finance` VALUES ('688', 'out', '81', '29', '88.00', '2017112963957', '1511949718', null);
INSERT INTO `xjm_finance` VALUES ('689', 'out', '32', '48', '88.00', '2017112900498', '1511950413', null);
INSERT INTO `xjm_finance` VALUES ('690', 'out', '89', '52', '118.00', '2017113069642', '1512037250', null);
INSERT INTO `xjm_finance` VALUES ('691', 'out', '32', '48', '88.00', '2017113014408', '1512038114', null);
INSERT INTO `xjm_finance` VALUES ('692', 'out', '32', '48', '118.00', '2017120197665', '1512092043', null);
INSERT INTO `xjm_finance` VALUES ('693', 'out', '32', '48', '113.00', '2017120161938', '1512109121', null);
INSERT INTO `xjm_finance` VALUES ('694', 'out', '31', '2', '118.00', '2017120109426', '1512119969', null);
INSERT INTO `xjm_finance` VALUES ('695', 'out', '32', '48', '118.00', '2017120113721', '1512129610', null);
INSERT INTO `xjm_finance` VALUES ('696', 'out', '108', '3', '88.00', '2017120599993', '1512464021', null);
INSERT INTO `xjm_finance` VALUES ('697', 'out', '96', '3', '118.00', '2017120718830', '1512626692', null);
INSERT INTO `xjm_finance` VALUES ('698', 'out', '46', '37', '118.00', '2017120855296', '1512699830', null);
INSERT INTO `xjm_finance` VALUES ('699', 'out', '141', '39', '118.00', '2017121641623', '1513436328', null);
INSERT INTO `xjm_finance` VALUES ('700', 'out', '182', '39', '128.00', '2017123072630', '1514646745', null);
INSERT INTO `xjm_finance` VALUES ('701', 'out', '182', '26', '158.00', '2017123007374', '1514646980', null);
INSERT INTO `xjm_finance` VALUES ('702', 'out', '230', '2', '88.00', '2018021751744', '1518849886', null);
INSERT INTO `xjm_finance` VALUES ('703', 'out', '278', '40', '218.00', '2018032847364', '1522249174', null);
INSERT INTO `xjm_finance` VALUES ('704', 'out', '278', '40', '218.00', '2018032847364', '1522249393', null);
INSERT INTO `xjm_finance` VALUES ('705', 'out', '287', '2', '118.00', '2018040629342', '1522976166', null);
INSERT INTO `xjm_finance` VALUES ('706', 'out', '291', '3', '158.00', '2018051258009', '1526055623', null);
INSERT INTO `xjm_finance` VALUES ('707', 'out', '439', '40', '88.00', '2018081180697', '1533986636', null);
INSERT INTO `xjm_finance` VALUES ('708', 'out', '616', '2', '118.00', '2018120315508', '1543836876', null);

-- ----------------------------
-- Table structure for xjm_goods
-- ----------------------------
DROP TABLE IF EXISTS `xjm_goods`;
CREATE TABLE `xjm_goods` (
  `goods_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `area_id` tinyint(4) DEFAULT '0' COMMENT '地区id',
  `goods_type` tinyint(5) DEFAULT '0' COMMENT '商品所属分类',
  `goods_name` varchar(50) DEFAULT '' COMMENT '产品名称',
  `goods_image` varchar(200) DEFAULT '' COMMENT '商品图片',
  `goods_price` decimal(10,2) DEFAULT '0.00' COMMENT '商品价格',
  `server_time` tinyint(3) DEFAULT '0' COMMENT '服务时间',
  `goods_content` mediumtext COMMENT '商品介绍',
  `taboo` text COMMENT '禁忌',
  `sale_num` int(10) DEFAULT '0' COMMENT '销量',
  `is_show` tinyint(1) DEFAULT '0' COMMENT '是否下架 0：上架  1：下架',
  `sort` tinyint(2) unsigned DEFAULT '0' COMMENT '排序',
  `add_time` int(10) DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`goods_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COMMENT='产品表';

-- ----------------------------
-- Records of xjm_goods
-- ----------------------------
INSERT INTO `xjm_goods` VALUES ('1', '0', '10', '足疗', 'http://pic.xijiaomo.com/1dr99uhe9y2.png', '0.01', '50', '99999', '忌在空腹或饱食候进行按摩，人体在饱食后血流加速，胃蠕动增强，此时按摩容易引起不适', '0', '0', '2', '1505380750');
INSERT INTO `xjm_goods` VALUES ('3', '0', '2', '精油刮痧+全身推拿', 'http://pic.xijiaomo.com/z0olhgjebsg.png', '108.00', '60', '', '忌在空腹或饱食候进行按摩，人体在饱食后血流加速，胃蠕动增强，此时按摩容易引起不适', '0', '0', '1', '1511924360');
INSERT INTO `xjm_goods` VALUES ('4', '0', '1', '泡脚+足底按摩', 'http://pic.xijiaomo.com/l4ft5hheg3k.jpg', '118.00', '60', '', '忌在空腹或饱食候进行按摩，人体在饱食后血流加速，胃蠕动增强，此时按摩容易引起不适', '0', '0', '1', '1511924369');
INSERT INTO `xjm_goods` VALUES ('8', '0', '5', '拔罐+肩颈经络按摩+开背', 'http://pic.xijiaomo.com/wdydo3fewpi.png', '88.00', '60', '主要治疗关节炎，肩周炎等', '忌在空腹或饱食候进行按摩，人体在饱食后血流加速，胃蠕动增强，此时按摩容易引起不适', '0', '0', '0', '1511924375');
INSERT INTO `xjm_goods` VALUES ('9', '0', '3', '头部颈椎按摩', 'http://pic.xijiaomo.com/nykz4noev9.png', '158.00', '60', '足底按摩', '忌在空腹或饱食候进行按摩，人体在饱食后血流加速，胃蠕动增强，此时按摩容易引起不适', '0', '0', '0', '1511924381');
INSERT INTO `xjm_goods` VALUES ('11', '0', '9', '肩颈推拿', 'http://pic.xijiaomo.com/w0ec7kohkhi.jpg', '128.00', '60', '舒筋活血', '忌在空腹或饱食候进行按摩，人体在饱食后血流加速，胃蠕动增强，此时按摩容易引起不适', '0', '0', '0', '1511924391');
INSERT INTO `xjm_goods` VALUES ('12', '0', '1', '中药泡脚+足疗+足底刮痧', 'http://pic.xijiaomo.com/q8m7pjp0hsa.jpg', '158.00', '70', '足浴', '忌在空腹或饱食候进行按摩，人体在饱食后血流加速，胃蠕动增强，此时按摩容易引起不适', '0', '0', '0', '1511924318');
INSERT INTO `xjm_goods` VALUES ('22', '0', '1', '中医足疗+反射区调理', 'http://pic.xijiaomo.com/CC8J@6X@7T5_WEOO%29I6T%7BVI.png', '188.00', '70', '', '忌在空腹或饱食候进行按摩，人体在饱食后血流加速，胃蠕动增强，此时按摩容易引起不适', '0', '0', '0', '1511924338');
INSERT INTO `xjm_goods` VALUES ('25', '0', '9', '专业全身推拿', 'http://pic.xijiaomo.com/09gv5yqdia8v.png', '158.00', '60', '舒筋活血', '', '0', '0', '0', '1511924399');
INSERT INTO `xjm_goods` VALUES ('26', '0', '9', '全身经络调理', 'http://pic.xijiaomo.com/b49qdzm8cogrh2y3dvpctyb9.png', '188.00', '90', '舒筋活血，去湿健脾', '', '0', '0', '0', '1511924788');
INSERT INTO `xjm_goods` VALUES ('27', '0', '8', '头颈肩理疗+艾灸', 'http://pic.xijiaomo.com/bj3fu84p8a6.png', '118.00', '60', '去湿健脾', '', '0', '0', '0', '1511924423');
INSERT INTO `xjm_goods` VALUES ('28', '0', '8', '神经经络调理+艾灸', 'http://pic.xijiaomo.com/gqf1qqol1ht.png', '158.00', '90', '美容养颜', '', '0', '0', '0', '1511924430');
INSERT INTO `xjm_goods` VALUES ('30', '0', '2', '刮痧+肩颈精油按摩+拔罐', 'http://pic.xijiaomo.com/h632sd6xkx.png', '128.00', '60', '刮痧', '', '0', '0', '0', '1511924437');
INSERT INTO `xjm_goods` VALUES ('31', '0', '4', '面部精致补水', 'http://pic.xijiaomo.com/qlmfgrejblk.png', '88.00', '60', '洗面', '', '0', '0', '0', '1511924444');
INSERT INTO `xjm_goods` VALUES ('32', '0', '4', '深层洁面', 'http://pic.xijiaomo.com/hkw2gcqda1.png', '158.00', '60', '洗面', '', '0', '0', '0', '1511924450');
INSERT INTO `xjm_goods` VALUES ('33', '0', '6', '泡脚+修脚', 'http://pic.xijiaomo.com/ezwqlqg8s880snwep16whwu3di.png', '88.00', '60', '修脚', '', '0', '0', '0', '1511924458');
INSERT INTO `xjm_goods` VALUES ('34', '0', '7', '专业采耳+温灸', 'http://pic.xijiaomo.com/7ixqxa013fokb5sq913dlk57b9.png', '88.00', '60', '采耳', '', '0', '0', '0', '1511924466');
INSERT INTO `xjm_goods` VALUES ('36', '0', '2', '刮痧+精油开背+拔罐养生', 'http://pic.xijiaomo.com/1gx8t0q9bo8o1wza48d7ioogvi.png', '158.00', '90', '', null, '0', '0', '0', '1511924473');
INSERT INTO `xjm_goods` VALUES ('37', '0', '3', '肩颈腰按摩', 'http://pic.xijiaomo.com/qen9j6esq2eyvvzdoszjpds4i.png', '198.00', '90', '', null, '0', '0', '0', '1511924480');
INSERT INTO `xjm_goods` VALUES ('38', '0', '3', '全身经络按摩', 'http://pic.xijiaomo.com/2l9arf9ud0afek4ap914r96bt9.png', '218.00', '90', '', null, '0', '0', '0', '1511924593');
INSERT INTO `xjm_goods` VALUES ('39', '0', '4', '水氧面部护理', 'http://pic.xijiaomo.com/lolt7o1bunztygcd2iyzdj9k9.png', '188.00', '90', '', null, '0', '0', '0', '1511924616');
INSERT INTO `xjm_goods` VALUES ('40', '0', '5', '拔罐+精油按摩+开背', 'http://pic.xijiaomo.com/y7nwueat6fkrmxwcxu0x80k9.png', '108.00', '60', '', null, '0', '0', '0', '1511924624');
INSERT INTO `xjm_goods` VALUES ('41', '0', '5', '火罐+全身推拿+精油开背', 'http://pic.xijiaomo.com/piwzl9im9m1no0byvam3lq5mi.png', '128.00', '90', '', null, '0', '0', '0', '1511924633');
INSERT INTO `xjm_goods` VALUES ('42', '0', '6', '舒筋修脚', 'http://pic.xijiaomo.com/a5zpv3lfnfzilqlb281al9pb9.png', '128.00', '60', '', null, '0', '0', '0', '1511924642');
INSERT INTO `xjm_goods` VALUES ('43', '0', '6', '足浴修脚+足底按摩', 'http://pic.xijiaomo.com/2hubjmbl01ebsva0p5qry919k9.png', '158.00', '70', '', null, '0', '0', '0', '1511924649');
INSERT INTO `xjm_goods` VALUES ('44', '0', '7', '专业采耳+香薰耳烛', 'http://pic.xijiaomo.com/4d4k6zhmjx486dsgrtn0xav2t9.png', '128.00', '60', '', null, '0', '0', '0', '1511924668');
INSERT INTO `xjm_goods` VALUES ('45', '0', '7', '专业采耳+头部调理', 'http://pic.xijiaomo.com/u4palxkrjc5mglmvgegv3rf6r.png', '148.00', '70', '', null, '0', '0', '0', '1511924674');
INSERT INTO `xjm_goods` VALUES ('46', '0', '8', '深度艾灸调理', 'http://pic.xijiaomo.com/yxrkfgpyzbta1llvwnpfd2t9.png', '188.00', '90', '', null, '0', '0', '0', '1511924681');

-- ----------------------------
-- Table structure for xjm_goods_option
-- ----------------------------
DROP TABLE IF EXISTS `xjm_goods_option`;
CREATE TABLE `xjm_goods_option` (
  `cate_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL DEFAULT '' COMMENT '类型名称',
  `parent_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '父id',
  `level` tinyint(1) DEFAULT '0' COMMENT '等级',
  `sort_order` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '顺序排序',
  `is_show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否显示',
  `image` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '' COMMENT '分类图片',
  `publicity_img` varchar(100) DEFAULT '' COMMENT '宣传图片',
  `good_tag` varchar(50) NOT NULL COMMENT '评价标签 多个标签用英文逗号隔开对应comment_tag_set主键id',
  `bad_tag` varchar(50) NOT NULL COMMENT '差评标签',
  `profile` text COMMENT '服务介绍',
  PRIMARY KEY (`cate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='商品分类表';

-- ----------------------------
-- Records of xjm_goods_option
-- ----------------------------
INSERT INTO `xjm_goods_option` VALUES ('1', '足疗', '0', '0', '50', '1', '', 'http://pic.xijiaomo.com/%E6%9C%AA%E6%A0%87%E9%A2%98-1.png', '1,2,3', '8,9,10', '通过足部的刺激，调整人体生理功能，促进血液循环，调节升级系统，提高免疫系统功能，预防疾病，改善体质。');
INSERT INTO `xjm_goods_option` VALUES ('2', '刮痧', '0', '0', '50', '1', '', 'http://pic.xijiaomo.com/xrbfhocr4z.png', '2,3,4', '9,10,11', '具有调气行血、活血化瘀、舒筋通络、消肿止痛，以增强机体自身潜在的抗病能力和免疫机能，改善和调整脏腑功能，使脏腑阴阳得到平衡。');
INSERT INTO `xjm_goods_option` VALUES ('3', '按摩', '0', '0', '50', '1', '', 'http://pic.xijiaomo.com/urzwsmiyt7p.png', '3,4,5', '11,12,13', '运用手、指的技巧在人体皮肤、肌肉组织上推、按、捏、揉，以促进血液循环，消除疲劳，调节体内信息，增强体质，健美防衰。');
INSERT INTO `xjm_goods_option` VALUES ('4', '洗面', '0', '0', '50', '1', '', 'http://pic.xijiaomo.com/m294z37f9em.png', '1,3,5', '9,10,11', '通过清洁使得皮肤处于尽可能无污染和无侵害的状态中，让毛孔充分发挥皮肤健康和正常的吸收、呼吸、排泄功能，保持皮肤良好的新陈代谢状态。');
INSERT INTO `xjm_goods_option` VALUES ('5', '拔罐', '0', '0', '50', '1', '', 'http://pic.xijiaomo.com/pa3a59d2kw.png', '1,3,5,7', '10,11,12', '拔罐利用燃火、抽气等方法产生负压，使之吸附于体表，造成局部瘀血，以达到通经活络、行气活血、消肿止痛、祛风散寒等作用的疗法。');
INSERT INTO `xjm_goods_option` VALUES ('6', '修脚', '0', '0', '50', '1', '', 'http://pic.xijiaomo.com/cnwgobn78o.png', '5,6,7', '10,12,13', '修治脚上的脚部皮肤和趾甲病痛，包括修脚趾甲、刮脚、捏脚、去死皮等。是保护人体健康的一项重要措施。');
INSERT INTO `xjm_goods_option` VALUES ('7', '采耳', '0', '0', '50', '1', '', 'http://pic.xijiaomo.com/9u4ix2mze1.png', '1,4,6', '8,10,13', '采耳师傅利用各种采耳工具对耳洞内外进行刺激，它除了有清洁耳洞的功能外，还能起到减压和享乐作用，手法应当做到轻、稳、准。');
INSERT INTO `xjm_goods_option` VALUES ('8', '艾灸', '0', '0', '50', '1', '', 'http://pic.xijiaomo.com/onvkjzqds6a.png', '1,3,6', '9,11,13', '熏烤人体的穴位以达到保健治病的一种自然疗法，通过激发经气的活动来调整人体紊乱的生理生化功能，从而达到防病治病目的的一种治疗方法。');
INSERT INTO `xjm_goods_option` VALUES ('9', '推拿', '0', '0', '50', '1', '', 'http://pic.xijiaomo.com/epzlmprmzon.png', '1,5,6', '9,11,13', '具体运用推、拿、按、摩、揉、捏、点等形式多样的手法，以期达到疏通经络、推行气血、调节机体生理，降低周围血管阻力，减轻心脏负担，故可防治心血管疾病。');

-- ----------------------------
-- Table structure for xjm_message_labelling
-- ----------------------------
DROP TABLE IF EXISTS `xjm_message_labelling`;
CREATE TABLE `xjm_message_labelling` (
  `id` tinyint(2) unsigned NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '产品id',
  `name` char(25) NOT NULL DEFAULT '' COMMENT '标签名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='特殊要求留言';

-- ----------------------------
-- Records of xjm_message_labelling
-- ----------------------------
INSERT INTO `xjm_message_labelling` VALUES ('1', '1', '时间提前');
INSERT INTO `xjm_message_labelling` VALUES ('2', '1', '颈椎病');
INSERT INTO `xjm_message_labelling` VALUES ('3', '1', '扭伤');
INSERT INTO `xjm_message_labelling` VALUES ('4', '2', '时间提前');
INSERT INTO `xjm_message_labelling` VALUES ('5', '2', '颈椎病');
INSERT INTO `xjm_message_labelling` VALUES ('6', '2', '腿疼');
INSERT INTO `xjm_message_labelling` VALUES ('7', '9', '时间提前');
INSERT INTO `xjm_message_labelling` VALUES ('8', '9', '行动不便');

-- ----------------------------
-- Table structure for xjm_notice
-- ----------------------------
DROP TABLE IF EXISTS `xjm_notice`;
CREATE TABLE `xjm_notice` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户id',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0：私有  1：公共',
  `common_id` int(11) unsigned DEFAULT '0' COMMENT '公共信息',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '标题',
  `content` varchar(200) NOT NULL DEFAULT '' COMMENT '内容',
  `add_time` int(10) unsigned NOT NULL DEFAULT '0',
  `is_read` tinyint(1) unsigned DEFAULT '0' COMMENT '0 未读  1已读',
  `read_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '阅读时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=utf8 COMMENT='信息表';

-- ----------------------------
-- Records of xjm_notice
-- ----------------------------
INSERT INTO `xjm_notice` VALUES ('19', '23', '1', '1', '注册就送代金卷', '充值就送', '1504858748', '1', '1507874199');
INSERT INTO `xjm_notice` VALUES ('20', '23', '1', '3', '注册就送代金卷3', '333333', '1504858848', '1', '1507874210');
INSERT INTO `xjm_notice` VALUES ('21', '23', '0', '0', '欢迎注册', '你好', '1504859848', '1', '1507874209');
INSERT INTO `xjm_notice` VALUES ('22', '31', '1', '1', '注册就送代金卷', '充值就送', '1504858748', '1', '1511157143');
INSERT INTO `xjm_notice` VALUES ('23', '31', '1', '3', '注册就送代金卷3', '333333', '1504858748', '1', '1511157139');
INSERT INTO `xjm_notice` VALUES ('24', '31', '1', '1', '注册就送代金卷', '充值就送', '1504858748', '1', '1510130935');
INSERT INTO `xjm_notice` VALUES ('25', '31', '1', '3', '注册就送代金卷3', '333333', '1504858748', '1', '1510130898');
INSERT INTO `xjm_notice` VALUES ('26', '31', '1', '1', '注册就送代金卷', '充值就送', '1504858748', '1', '1510130899');
INSERT INTO `xjm_notice` VALUES ('27', '31', '1', '3', '注册就送代金卷3', '333333', '1504858748', '1', '1510130902');
INSERT INTO `xjm_notice` VALUES ('28', '31', '1', '1', '注册就送代金卷', '充值就送', '1504858748', '1', '1510130903');
INSERT INTO `xjm_notice` VALUES ('29', '30', '1', '3', '注册就送代金卷3', '333333', '1504858748', '1', '1509624395');
INSERT INTO `xjm_notice` VALUES ('30', '31', '1', '1', '注册就送代金卷', '充值就送', '1504858748', '1', '1510130904');
INSERT INTO `xjm_notice` VALUES ('31', '31', '1', '3', '注册就送代金卷3', '333333', '1504858748', '1', '1511166497');
INSERT INTO `xjm_notice` VALUES ('32', '32', '1', '1', '注册就送代金卷', '充值就送', '1504858748', '1', '1510301276');
INSERT INTO `xjm_notice` VALUES ('33', '32', '1', '3', '注册就送代金卷3', '333333', '1504858748', '1', '1510301276');
INSERT INTO `xjm_notice` VALUES ('34', '46', '1', '1', '注册就送代金卷', '充值就送', '1504858748', '1', '1511166506');
INSERT INTO `xjm_notice` VALUES ('35', '46', '1', '3', '注册就送代金卷3', '333333', '1504858748', '1', '1511166512');
INSERT INTO `xjm_notice` VALUES ('36', '38', '0', '3', '退款成功', '已经将订单：2017112046263退款0.01元，打入你的付款账户', '1511162572', '1', '1511345720');
INSERT INTO `xjm_notice` VALUES ('37', '40', '0', '3', '退款成功', '已经将订单：2017110367954退款0.01元，打入你的付款账户', '1511162782', '1', '1511510020');
INSERT INTO `xjm_notice` VALUES ('38', '32', '0', '3', '退款成功', '已经将订单：2017103067599退款0.01元，打入您的付款账户', '1511165701', '1', '1511234385');
INSERT INTO `xjm_notice` VALUES ('39', '31', '0', '3', '退款成功', '已经将订单：2017103084960退款0.01元，打入您的付款账户', '1511165705', '1', '1512027127');
INSERT INTO `xjm_notice` VALUES ('40', '32', '0', '3', '退款成功', '已经将订单：2017103141915退款0.01元，打入您的付款账户', '1511165711', '1', '1511234384');
INSERT INTO `xjm_notice` VALUES ('41', '35', '0', '3', '退款成功', '已经将订单：2017110369328退款0.01元，打入您的付款账户', '1511165714', '1', '1511313323');
INSERT INTO `xjm_notice` VALUES ('42', '31', '0', '3', '退款成功', '已经将订单：2017110390292退款0.01元，打入您的付款账户', '1511165716', '1', '1512027126');
INSERT INTO `xjm_notice` VALUES ('43', '38', '0', '3', '退款成功', '已经将订单：2017111045431退款0.01元，打入您的付款账户', '1511165724', '1', '1511345720');
INSERT INTO `xjm_notice` VALUES ('44', '46', '0', '3', '退款成功', '已经将订单：2017110900107退款0.01元，打入您的付款账户', '1511165726', '1', '1511166087');
INSERT INTO `xjm_notice` VALUES ('45', '35', '0', '3', '退款成功', '已经将订单：2017111748267退款0.01元，打入您的付款账户', '1511165729', '1', '1511313323');
INSERT INTO `xjm_notice` VALUES ('46', '46', '0', '3', '退款成功', '已经将订单：2017111925412退款0.01元，打入您的付款账户', '1511165731', '1', '1511261818');
INSERT INTO `xjm_notice` VALUES ('47', '46', '0', '3', '退款成功', '已经将订单：2017111936603退款0.01元，打入您的付款账户', '1511165734', '1', '1511788205');
INSERT INTO `xjm_notice` VALUES ('48', '38', '0', '3', '退款成功', '已经将订单：2017111937738退款0.01元，打入您的付款账户', '1511165736', '1', '1511345719');
INSERT INTO `xjm_notice` VALUES ('49', '31', '0', '3', '退款成功', '已经将订单：2017111853302退款0.01元，打入您的付款账户', '1511165742', '1', '1511345286');
INSERT INTO `xjm_notice` VALUES ('50', '46', '0', '3', '退款成功', '已经将订单：2017111754148退款0.01元，打入您的付款账户', '1511165745', '1', '1511788201');
INSERT INTO `xjm_notice` VALUES ('51', '46', '0', '3', '退款成功', '已经将订单：2017111572599退款0.01元，打入您的付款账户', '1511165752', '1', '1511788198');
INSERT INTO `xjm_notice` VALUES ('52', '46', '0', '3', '退款成功', '已经将订单：2017111542548退款0.01元，打入您的付款账户', '1511165755', '1', '1511166234');
INSERT INTO `xjm_notice` VALUES ('53', '38', '0', '3', '退款成功', '已经将订单：2017111509013退款0.01元，打入您的付款账户', '1511165757', '1', '1511345719');
INSERT INTO `xjm_notice` VALUES ('54', '38', '0', '3', '退款成功', '已经将订单：2017111547405退款0.01元，打入您的付款账户', '1511165763', '1', '1511345719');
INSERT INTO `xjm_notice` VALUES ('55', '38', '0', '3', '退款成功', '已经将订单：2017111524828退款0.01元，打入您的付款账户', '1511165765', '1', '1511345718');
INSERT INTO `xjm_notice` VALUES ('56', '38', '0', '3', '退款成功', '已经将订单：2017111418703退款0.01元，打入您的付款账户', '1511165767', '1', '1511178303');
INSERT INTO `xjm_notice` VALUES ('57', '38', '0', '3', '退款成功', '已经将订单：2017111423345退款0.01元，打入您的付款账户', '1511165769', '1', '1511178303');
INSERT INTO `xjm_notice` VALUES ('58', '38', '0', '3', '退款成功', '已经将订单：2017111406070退款0.01元，打入您的付款账户', '1511165771', '1', '1511178302');
INSERT INTO `xjm_notice` VALUES ('59', '38', '0', '3', '退款成功', '已经将订单：2017111436708退款0.01元，打入您的付款账户', '1511165773', '1', '1511178301');
INSERT INTO `xjm_notice` VALUES ('60', '38', '0', '3', '退款成功', '已经将订单：2017111444924退款0.01元，打入您的付款账户', '1511165775', '1', '1511180991');
INSERT INTO `xjm_notice` VALUES ('61', '38', '0', '3', '退款成功', '已经将订单：2017111491879退款0.01元，打入您的付款账户', '1511165777', '1', '1511180991');
INSERT INTO `xjm_notice` VALUES ('62', '38', '0', '3', '退款成功', '已经将订单：2017111444111退款0.01元，打入您的付款账户', '1511165779', '1', '1511180991');
INSERT INTO `xjm_notice` VALUES ('63', '38', '0', '3', '退款成功', '已经将订单：2017111470709退款0.01元，打入您的付款账户', '1511165781', '1', '1511180990');
INSERT INTO `xjm_notice` VALUES ('64', '38', '0', '3', '退款成功', '已经将订单：2017111433558退款0.01元，打入您的付款账户', '1511165783', '1', '1511343846');
INSERT INTO `xjm_notice` VALUES ('65', '38', '0', '3', '退款成功', '已经将订单：2017111321382退款0.01元，打入您的付款账户', '1511165787', '1', '1511180990');
INSERT INTO `xjm_notice` VALUES ('66', '63', '0', '3', '退款成功', '已经将订单：2017111107873退款88.00元，打入您的付款账户', '1511165792', '0', '0');
INSERT INTO `xjm_notice` VALUES ('67', '38', '0', '3', '退款成功', '已经将订单：2017111083764退款0.01元，打入您的付款账户', '1511165814', '1', '1511180990');
INSERT INTO `xjm_notice` VALUES ('68', '35', '0', '3', '退款成功', '已经将订单：2017111036932退款0.01元，打入您的付款账户', '1511165817', '1', '1511313318');
INSERT INTO `xjm_notice` VALUES ('69', '38', '0', '3', '退款成功', '已经将订单：2017110961376退款0.01元，打入您的付款账户', '1511165822', '1', '1512026937');
INSERT INTO `xjm_notice` VALUES ('70', '55', '0', '3', '退款成功', '已经将订单：2017110996139退款0.01元，打入您的付款账户', '1511165824', '0', '0');
INSERT INTO `xjm_notice` VALUES ('71', '38', '0', '3', '退款成功', '已经将订单：2017110957460退款0.01元，打入您的付款账户', '1511165827', '1', '1511235269');
INSERT INTO `xjm_notice` VALUES ('72', '32', '0', '3', '退款成功', '已经将订单：2017110679493退款0.01元，打入您的付款账户', '1511165829', '1', '1511234381');
INSERT INTO `xjm_notice` VALUES ('73', '32', '0', '3', '退款成功', '已经将订单：2017110627692退款0.01元，打入您的付款账户', '1511165831', '1', '1511234381');
INSERT INTO `xjm_notice` VALUES ('74', '32', '0', '3', '退款成功', '已经将订单：2017110644048退款0.01元，打入您的付款账户', '1511165833', '1', '1511234381');
INSERT INTO `xjm_notice` VALUES ('75', '32', '0', '3', '退款成功', '已经将订单：2017110641461退款0.01元，打入您的付款账户', '1511165836', '1', '1511234381');
INSERT INTO `xjm_notice` VALUES ('76', '32', '0', '3', '退款成功', '已经将订单：2017110657084退款0.01元，打入您的付款账户', '1511165838', '1', '1511234376');
INSERT INTO `xjm_notice` VALUES ('77', '38', '0', '3', '退款成功', '已经将订单：2017110383266退款0.01元，打入您的付款账户', '1511165841', '1', '1511345721');
INSERT INTO `xjm_notice` VALUES ('78', '32', '0', '3', '退款成功', '已经将订单：2017110337549退款0.01元，打入您的付款账户', '1511165843', '1', '1511234376');
INSERT INTO `xjm_notice` VALUES ('79', '32', '0', '3', '退款成功', '已经将订单：2017110334863退款0.01元，打入您的付款账户', '1511165845', '1', '1511234375');
INSERT INTO `xjm_notice` VALUES ('80', '32', '0', '3', '退款成功', '已经将订单：2017110343216退款0.01元，打入您的付款账户', '1511165847', '1', '1511234375');
INSERT INTO `xjm_notice` VALUES ('81', '32', '0', '3', '退款成功', '已经将订单：2017110223330退款0.01元，打入您的付款账户', '1511165850', '1', '1511262450');
INSERT INTO `xjm_notice` VALUES ('82', '32', '0', '3', '退款成功', '已经将订单：2017110166675退款0.01元，打入您的付款账户', '1511165854', '1', '1511169750');
INSERT INTO `xjm_notice` VALUES ('83', '32', '0', '3', '退款成功', '已经将订单：2017110144093退款0.01元，打入您的付款账户', '1511165856', '1', '1511169750');
INSERT INTO `xjm_notice` VALUES ('84', '32', '0', '3', '退款成功', '已经将订单：2017110106461退款0.01元，打入您的付款账户', '1511165858', '1', '1511169750');
INSERT INTO `xjm_notice` VALUES ('85', '32', '0', '3', '退款成功', '已经将订单：2017110111922退款0.01元，打入您的付款账户', '1511165868', '1', '1511169750');
INSERT INTO `xjm_notice` VALUES ('86', '32', '0', '3', '退款成功', '已经将订单：2017110117375退款0.01元，打入您的付款账户', '1511165870', '1', '1511169750');
INSERT INTO `xjm_notice` VALUES ('87', '32', '0', '3', '退款成功', '已经将订单：2017103182083退款0.01元，打入您的付款账户', '1511165873', '1', '1511169750');
INSERT INTO `xjm_notice` VALUES ('88', '32', '0', '3', '退款成功', '已经将订单：2017103125598退款0.01元，打入您的付款账户', '1511165875', '1', '1511169753');
INSERT INTO `xjm_notice` VALUES ('89', '32', '0', '3', '退款成功', '已经将订单：2017103164490退款0.01元，打入您的付款账户', '1511165877', '1', '1511169748');
INSERT INTO `xjm_notice` VALUES ('90', '32', '0', '3', '退款成功', '已经将订单：2017103192445退款0.01元，打入您的付款账户', '1511165880', '1', '1511169747');
INSERT INTO `xjm_notice` VALUES ('91', '32', '0', '3', '退款成功', '已经将订单：2017103109591退款0.01元，打入您的付款账户', '1511165883', '1', '1511169747');
INSERT INTO `xjm_notice` VALUES ('92', '32', '0', '3', '退款成功', '已经将订单：2017103146494退款0.01元，打入您的付款账户', '1511165885', '1', '1511169747');
INSERT INTO `xjm_notice` VALUES ('93', '32', '0', '3', '退款成功', '已经将订单：2017103117001退款0.01元，打入您的付款账户', '1511165888', '1', '1511169750');
INSERT INTO `xjm_notice` VALUES ('94', '32', '0', '3', '退款成功', '已经将订单：2017103184172退款0.01元，打入您的付款账户', '1511165890', '1', '1511317544');
INSERT INTO `xjm_notice` VALUES ('95', '32', '0', '3', '退款成功', '已经将订单：2017103156050退款0.01元，打入您的付款账户', '1511165892', '1', '1511317543');
INSERT INTO `xjm_notice` VALUES ('96', '32', '0', '3', '退款成功', '已经将订单：2017103149444退款0.01元，打入您的付款账户', '1511165895', '1', '1511317543');
INSERT INTO `xjm_notice` VALUES ('97', '32', '0', '3', '退款成功', '已经将订单：2017103118040退款0.01元，打入您的付款账户', '1511165898', '1', '1511317552');
INSERT INTO `xjm_notice` VALUES ('98', '32', '0', '3', '退款成功', '已经将订单：2017103127018退款0.01元，打入您的付款账户', '1511165900', '1', '1511317542');
INSERT INTO `xjm_notice` VALUES ('99', '32', '0', '3', '退款成功', '已经将订单：2017103109428退款0.01元，打入您的付款账户', '1511165902', '1', '1511262449');
INSERT INTO `xjm_notice` VALUES ('100', '32', '0', '3', '退款成功', '已经将订单：2017103111351退款0.01元，打入您的付款账户', '1511165904', '1', '1511262449');
INSERT INTO `xjm_notice` VALUES ('101', '32', '0', '3', '退款成功', '已经将订单：2017103140399退款0.01元，打入您的付款账户', '1511165906', '1', '1511262449');
INSERT INTO `xjm_notice` VALUES ('102', '32', '0', '3', '退款成功', '已经将订单：2017111799965退款0.01元，打入您的付款账户', '1511165910', '1', '1511262449');
INSERT INTO `xjm_notice` VALUES ('103', '32', '0', '3', '退款成功', '已经将订单：2017111699256退款0.01元，打入您的付款账户', '1511165912', '1', '1511262448');
INSERT INTO `xjm_notice` VALUES ('104', '46', '0', '3', '退款成功', '已经将订单：2017111656006退款0.01元，打入您的付款账户', '1511165914', '1', '1511788192');
INSERT INTO `xjm_notice` VALUES ('105', '46', '0', '3', '退款成功', '已经将订单：2017111689773退款0.01元，打入您的付款账户', '1511165917', '1', '1511261819');
INSERT INTO `xjm_notice` VALUES ('106', '31', '0', '3', '退款成功', '已经将订单：2017111679045退款0.01元，打入您的付款账户', '1511165919', '1', '1512027137');
INSERT INTO `xjm_notice` VALUES ('107', '32', '0', '3', '退款成功', '已经将订单：2017111614965退款0.01元，打入您的付款账户', '1511165922', '1', '1511346823');
INSERT INTO `xjm_notice` VALUES ('108', '32', '0', '3', '退款成功', '已经将订单：2017111624112退款0.01元，打入您的付款账户', '1511165925', '1', '1511410971');
INSERT INTO `xjm_notice` VALUES ('109', '38', '0', '3', '退款成功', '已经将订单：2017111695723退款0.01元，打入您的付款账户', '1511165927', '1', '1511343849');
INSERT INTO `xjm_notice` VALUES ('110', '31', '0', '3', '退款成功', '已经将订单：2017111518324退款0.01元，打入您的付款账户', '1511165946', '1', '1512027139');
INSERT INTO `xjm_notice` VALUES ('111', '55', '0', '3', '退款成功', '已经将订单：2017111421499退款0.01元，打入您的付款账户', '1511165949', '0', '0');
INSERT INTO `xjm_notice` VALUES ('112', '35', '0', '3', '退款成功', '已经将订单：2017111716274退款0.01元，打入您的付款账户', '1511165952', '1', '1511165984');
INSERT INTO `xjm_notice` VALUES ('113', '38', '0', '3', '退款成功', '已经将订单：2017112029562退款0.01元，打入您的付款账户', '1511172381', '1', '1512026914');
INSERT INTO `xjm_notice` VALUES ('114', '35', '0', '3', '退款成功', '已经将订单：2017112263738退款0.01元，打入您的付款账户', '1511332568', '1', '1511334126');
INSERT INTO `xjm_notice` VALUES ('115', '35', '0', '3', '退款成功', '已经将订单：2017112209149退款0.01元，打入您的付款账户', '1511333979', '1', '1511401740');
INSERT INTO `xjm_notice` VALUES ('116', '35', '0', '3', '退款成功', '已经将订单：2017112272294退款80.00元，打入您的付款账户', '1511344178', '1', '1511401739');
INSERT INTO `xjm_notice` VALUES ('117', '38', '0', '3', '退款成功', '已经将订单：2017112128746退款0.01元，打入您的付款账户', '1511345925', '1', '1512032974');
INSERT INTO `xjm_notice` VALUES ('118', '32', '0', '3', '退款成功', '已经将订单：2017112072669退款0.01元，打入您的付款账户', '1511346418', '1', '1511410971');
INSERT INTO `xjm_notice` VALUES ('119', '40', '0', '3', '退款成功', '已经将订单：2017112199005退款0.01元，打入您的付款账户', '1511354008', '1', '1511510020');
INSERT INTO `xjm_notice` VALUES ('120', '31', '0', '3', '退款成功', '已经将订单：2017112111663退款0.01元，打入您的付款账户', '1511354016', '1', '1512543524');
INSERT INTO `xjm_notice` VALUES ('121', '40', '0', '3', '退款成功', '已经将订单：2017112141557退款0.01元，打入您的付款账户', '1511354018', '1', '1511510019');
INSERT INTO `xjm_notice` VALUES ('122', '46', '0', '3', '退款成功', '已经将订单：2017112127407退款0.01元，打入您的付款账户', '1511354020', '1', '1511788175');
INSERT INTO `xjm_notice` VALUES ('123', '46', '0', '3', '退款成功', '已经将订单：2017112105653退款0.01元，打入您的付款账户', '1511354022', '1', '1511788195');
INSERT INTO `xjm_notice` VALUES ('124', '32', '0', '3', '退款成功', '已经将订单：2017112119208退款0.01元，打入您的付款账户', '1511354024', '1', '1511410971');
INSERT INTO `xjm_notice` VALUES ('125', '46', '0', '3', '退款成功', '已经将订单：2017112137020退款0.01元，打入您的付款账户', '1511354026', '1', '1511767460');
INSERT INTO `xjm_notice` VALUES ('126', '46', '0', '3', '退款成功', '已经将订单：2017112173449退款0.01元，打入您的付款账户', '1511354034', '1', '1511788189');
INSERT INTO `xjm_notice` VALUES ('127', '32', '0', '3', '退款成功', '已经将订单：2017112171427退款0.01元，打入您的付款账户', '1511354036', '1', '1516262463');
INSERT INTO `xjm_notice` VALUES ('128', '32', '0', '3', '退款成功', '已经将订单：2017112112185退款0.01元，打入您的付款账户', '1511354038', '1', '1516262461');
INSERT INTO `xjm_notice` VALUES ('129', '32', '0', '3', '退款成功', '已经将订单：2017112130475退款0.01元，打入您的付款账户', '1511354040', '1', '1516262459');
INSERT INTO `xjm_notice` VALUES ('130', '32', '0', '3', '退款成功', '已经将订单：2017112163945退款60.00元，打入您的付款账户', '1511354042', '1', '1516262467');
INSERT INTO `xjm_notice` VALUES ('131', '46', '0', '3', '退款成功', '已经将订单：2017112178226退款0.01元，打入您的付款账户', '1511354044', '1', '1511788186');
INSERT INTO `xjm_notice` VALUES ('132', '35', '0', '3', '退款成功', '已经将订单：2017112134115退款0.01元，打入您的付款账户', '1511354046', '1', '1511401739');
INSERT INTO `xjm_notice` VALUES ('133', '32', '0', '3', '退款成功', '已经将订单：2017112244038退款80.00元，打入您的付款账户', '1511354048', '1', '1516262465');
INSERT INTO `xjm_notice` VALUES ('134', '35', '0', '3', '退款成功', '已经将订单：2017112263738退款0.01元，打入您的付款账户', '1511354051', '1', '1511401739');
INSERT INTO `xjm_notice` VALUES ('135', '35', '0', '3', '退款成功', '已经将订单：2017112255066退款0.01元，打入您的付款账户', '1511354053', '1', '1512730017');
INSERT INTO `xjm_notice` VALUES ('136', '35', '0', '3', '退款成功', '已经将订单：2017112272294退款80.00元，打入您的付款账户', '1511354055', '1', '1512730013');
INSERT INTO `xjm_notice` VALUES ('137', '46', '0', '3', '退款成功', '已经将订单：2017112263347退款0.01元，打入您的付款账户', '1511354057', '1', '1511788174');
INSERT INTO `xjm_notice` VALUES ('138', '46', '0', '3', '退款成功', '已经将订单：2017112281376退款55.00元，打入您的付款账户', '1511354059', '1', '1544465669');
INSERT INTO `xjm_notice` VALUES ('139', '35', '0', '3', '退款成功', '已经将订单：2017112272294退款80.00元，打入您的付款账户', '1511354066', '1', '1511401737');
INSERT INTO `xjm_notice` VALUES ('140', '40', '0', '3', '退款成功', '已经将订单：2017112328246退款50.00元，打入您的付款账户', '1511487100', '1', '1511514194');
INSERT INTO `xjm_notice` VALUES ('141', '34', '0', '3', '退款成功', '已经将订单：2017112815057退款50.00元，打入您的付款账户', '1511837374', '1', '1511839231');
INSERT INTO `xjm_notice` VALUES ('142', '32', '0', '3', '退款成功', '已经将订单：2017112757193退款48.00元，打入您的付款账户', '1511838859', '1', '1511839180');
INSERT INTO `xjm_notice` VALUES ('143', '34', '0', '3', '退款成功', '已经将订单：2017112855596退款60.00元，打入您的付款账户', '1511838862', '1', '1511839228');
INSERT INTO `xjm_notice` VALUES ('144', '31', '0', '3', '退款成功', '已经将订单：2017112861594退款50.00元，打入您的付款账户', '1511855293', '1', '1512543519');
INSERT INTO `xjm_notice` VALUES ('145', '31', '0', '3', '退款成功', '已经将订单：2017112844189退款50.00元，打入您的付款账户', '1511855295', '1', '1512027144');
INSERT INTO `xjm_notice` VALUES ('146', '31', '0', '3', '退款成功', '已经将订单：2017112940263退款88.00元，打入您的付款账户', '1511949094', '1', '1512543540');
INSERT INTO `xjm_notice` VALUES ('147', '81', '0', '3', '退款成功', '已经将订单：2017112850989退款65.00元，打入您的付款账户', '1511949434', '1', '1512033117');
INSERT INTO `xjm_notice` VALUES ('148', '32', '0', '3', '退款成功', '已经将订单：2017112967000退款88.00元，打入您的付款账户', '1512376773', '1', '1516261830');
INSERT INTO `xjm_notice` VALUES ('149', '32', '0', '3', '退款成功', '已经将订单：2017112900498退款88.00元，打入您的付款账户', '1512376776', '1', '1516262364');
INSERT INTO `xjm_notice` VALUES ('150', '32', '0', '3', '退款成功', '已经将订单：2017120113721退款118.00元，打入您的付款账户', '1512376782', '1', '1516262361');

-- ----------------------------
-- Table structure for xjm_notice_pubilc
-- ----------------------------
DROP TABLE IF EXISTS `xjm_notice_pubilc`;
CREATE TABLE `xjm_notice_pubilc` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT '0' COMMENT '关联notice表主键id',
  `content` varchar(200) DEFAULT '0' COMMENT '内容',
  `add_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='系统公告表';

-- ----------------------------
-- Records of xjm_notice_pubilc
-- ----------------------------
INSERT INTO `xjm_notice_pubilc` VALUES ('1', '注册就送代金卷', '充值就送', '1504858748');
INSERT INTO `xjm_notice_pubilc` VALUES ('2', '0注册就送代金卷', '22222222', '1');
INSERT INTO `xjm_notice_pubilc` VALUES ('3', '注册就送代金卷3', '333333', '1504858748');

-- ----------------------------
-- Table structure for xjm_option
-- ----------------------------
DROP TABLE IF EXISTS `xjm_option`;
CREATE TABLE `xjm_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `option` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xjm_option
-- ----------------------------
INSERT INTO `xjm_option` VALUES ('1', 'techer_about', '{\"slogan\":\"\\u4e00\\u6280\\u5728\\u624b\\uff0c\\u8db3\\u904d\\u795e\\u5dde\",\"website\":\"www.xijiaomo.com\",\"kf_email\":\"18627099297@163.com\",\"weixin_code\":\"xijiaomo\",\"kf_qq\":\"3060005126\",\"company_phone\":\"18627099297\",\"ps\":\"\\u5ba2\\u670d\\u5de5\\u4f5c\\u65f6\\u95f4\\u4e3a\\u5468\\u4e00\\u5230\\u5468\\u4e9410\\uff1a00-18\\uff1a00\\uff0c\\u8bf7\\u4e8e\\u5de5\\u4f5c\\u65f6\\u95f4\\u8054\\u7cfb\\u5ba2\\u670d\",\"au_version\":\"1.0\",\"at_version\":\"1.0\",\"iu_version\":\"1.0\",\"it_version\":\"1.0\",\"kf_banner\":\"https://api.xijiaomo.com/uploads/1509530846.png\"}');
INSERT INTO `xjm_option` VALUES ('2', 'warning_msg', '操你妈,习近平,fuck,shit,尼玛币');

-- ----------------------------
-- Table structure for xjm_order_auto_finish
-- ----------------------------
DROP TABLE IF EXISTS `xjm_order_auto_finish`;
CREATE TABLE `xjm_order_auto_finish` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` varchar(50) NOT NULL COMMENT '订单id',
  `end_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '过期时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单自动完成';

-- ----------------------------
-- Records of xjm_order_auto_finish
-- ----------------------------

-- ----------------------------
-- Table structure for xjm_order_list
-- ----------------------------
DROP TABLE IF EXISTS `xjm_order_list`;
CREATE TABLE `xjm_order_list` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `option_type` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '订单类型',
  `tid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '关联技师id',
  `order_sn` varchar(20) NOT NULL DEFAULT '' COMMENT '订单编号',
  `order_status` tinyint(1) unsigned NOT NULL COMMENT '订单状态 0正常状态  1待支付  2已完成  3已失效  4退款  5技师开始服务状态中 6客户开始服务状态中 7用户发起退款 8技师同意退款 9技师不同意退款',
  `pay_status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '支付状态 0未支付  1已支付',
  `area_id` int(8) DEFAULT '0' COMMENT '订单所属城市',
  `address` varchar(100) DEFAULT '' COMMENT '服务地址',
  `house_number` varchar(30) DEFAULT '' COMMENT '门牌号',
  `u_lon` char(20) DEFAULT '' COMMENT '经度',
  `u_lat` char(20) DEFAULT '' COMMENT '纬度',
  `mobile` varchar(60) DEFAULT '' COMMENT '手机',
  `goods_name` varchar(120) DEFAULT '' COMMENT '商品名称',
  `goods_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '商品单价',
  `number` int(5) DEFAULT '0' COMMENT '数量',
  `goods_totalprice` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '商品总价',
  `coupon_price` decimal(10,2) DEFAULT '0.00' COMMENT '优惠券抵扣',
  `total_amount` decimal(10,2) DEFAULT '0.00' COMMENT '订单总价',
  `order_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '应付款金额',
  `pay_type` tinyint(1) NOT NULL COMMENT '支付类型 1：微信 2：支付宝 ',
  `pay_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '支付时间',
  `user_note` varchar(255) DEFAULT '' COMMENT '用户备注',
  `add_time` int(10) unsigned DEFAULT '0' COMMENT '下单时间',
  `goods_id` int(11) NOT NULL COMMENT '产品id',
  `date_type` int(1) DEFAULT '0' COMMENT '0即时上门  1预约今天  2预约明天',
  `date_time` varchar(30) NOT NULL COMMENT '被预约的时间戳',
  `date_id` int(3) DEFAULT '0' COMMENT '预约的时间点id',
  `is_comment` int(1) NOT NULL DEFAULT '0' COMMENT '0未评价  1已评价',
  `server_time` varchar(20) DEFAULT '' COMMENT '服务市场',
  `trade_no` varchar(50) DEFAULT NULL COMMENT '支付宝交易号',
  `end_time` int(11) DEFAULT NULL COMMENT '结束时间戳',
  `xiaofei` decimal(10,2) DEFAULT '0.00' COMMENT '晚间补贴金额',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1198 DEFAULT CHARSET=utf8 COMMENT='订单列表';

-- ----------------------------
-- Records of xjm_order_list
-- ----------------------------
INSERT INTO `xjm_order_list` VALUES ('996', '46', '0', '38', '2017112284315', '1', '0', '1', '湖北省武汉市武昌区汉街总部国际(武汉杜莎夫人蜡像馆西南)  11222', '', '114.343292', '30.554939', '17778079115', '测试', '0.01', '0', '0.01', '0.00', '0.00', '0.01', '0', '0', '', '1511357059', '35', '1', '1511359200', '22', '0', '120', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('997', '31', '0', '29', '2017112394436', '2', '1', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  北', '', '114.342437', '30.555744', '15172370138', '测试', '0.01', '0', '0.01', '0.00', '0.00', '0.01', '0', '0', '', '1511400538', '35', '0', '', '0', '1', '120', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('999', '35', '0', '39', '2017112325427', '2', '1', '1', '楚河汉街(地铁站)  7楼', '', '114.341415', '30.553848', '15927219779', '测试', '0.01', '0', '0.01', '0.00', '0.00', '0.01', '0', '0', '', '1511402129', '35', '0', '', '0', '1', '120', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1000', '35', '0', '39', '2017112318013', '2', '1', '1', '楚河汉街(地铁站)  7楼', '', '114.341415', '30.553848', '15927219779', '测试', '0.01', '0', '0.01', '0.00', '0.00', '0.01', '0', '0', '', '1511402275', '35', '0', '', '0', '1', '120', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1001', '35', '0', '39', '2017112365195', '2', '1', '1', '楚河汉街(地铁站)  7楼', '', '114.341415', '30.553848', '15927219779', '测试', '0.01', '0', '0.01', '0.00', '0.00', '0.01', '0', '0', '', '1511402397', '35', '0', '', '0', '1', '120', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1002', '35', '0', '39', '2017112318603', '2', '1', '1', '楚河汉街(地铁站)  7楼', '', '114.341415', '30.553848', '15927219779', '测试', '0.01', '0', '0.01', '0.00', '0.00', '0.01', '0', '0', '', '1511402985', '35', '0', '', '0', '1', '120', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1003', '35', '0', '39', '2017112307742', '4', '1', '1', '楚河汉街(地铁站)  7楼', '', '114.341415', '30.553848', '15927219779', '测试', '0.01', '0', '0.01', '0.00', '0.00', '0.01', '0', '0', '', '1511403450', '35', '0', '', '0', '0', '120', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1004', '35', '0', '29', '2017112300326', '4', '1', '1', '楚河汉街(地铁站)  7楼', '', '114.341415', '30.553848', '15927219779', '测试', '0.01', '0', '0.01', '0.00', '0.00', '0.01', '0', '0', '', '1511403524', '35', '0', '', '0', '0', '120', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1005', '35', '0', '39', '2017112358394', '3', '0', '1', '楚河汉街(地铁站)  7楼', '', '114.341415', '30.553848', '15927219779', '测试', '0.01', '0', '0.01', '0.00', '0.00', '0.01', '0', '0', '', '1511403808', '35', '0', '', '0', '0', '120', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1006', '35', '0', '39', '2017112318844', '2', '1', '1', '楚河汉街(地铁站)  7楼', '', '114.341415', '30.553848', '15927219779', '测试', '0.01', '0', '0.01', '0.00', '0.00', '0.01', '0', '0', '', '1511404785', '35', '0', '', '0', '1', '120', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1007', '35', '0', '39', '2017112347480', '2', '1', '1', '楚河汉街(地铁站)  7楼', '', '114.341415', '30.553848', '15927219779', '测试', '0.01', '0', '0.01', '0.00', '0.00', '0.01', '0', '0', '', '1511404918', '35', '0', '', '0', '1', '120', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1009', '68', '0', '36', '2017112386249', '1', '0', '1', '屈臣氏吃饭的点了个电话都没有123', '', '114.337631', '30.560349', '17320567880', '足底按摩', '60.00', '0', '60.00', '0.00', '0.00', '60.00', '0', '0', '现金想看看星空款地空导弹看看kdkdkdk想看看的快下课想看看的小开心开心想看看星空下看小开心开心开心开心开心开心开心想哭哭成开心开心开心开心开心', '1511411406', '12', '1', '1511434800', '19', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1010', '68', '0', '36', '2017112313290', '1', '0', '1', '屈臣氏吃饭的点了个电话都没有123', '', '114.337631', '30.560349', '17320567880', '足底按摩', '60.00', '0', '60.00', '0.00', '0.00', '60.00', '0', '0', '现金想看看星空款地空导弹看看kdkdkdk想看看的快下课想看看的小开心开心想看看星空下看小开心开心开心开心开心开心开心想哭哭成开心开心开心开心开心', '1511411416', '12', '1', '1511434800', '19', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1011', '68', '0', '37', '2017112386073', '1', '0', '1', '屈臣氏吃饭的点了个电话都没有123', '', '114.337631', '30.560043', '17320567880', '足底按摩', '60.00', '0', '60.00', '0.00', '0.00', '60.00', '0', '0', '', '1511430331', '12', '0', '', '0', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1012', '40', '0', '40', '2017112305104', '2', '1', '1', '汉街总部国际A座701', '', '114.337426', '30.557860', '13397140486', '脚底按摩', '50.00', '0', '50.00', '0.00', '0.00', '50.00', '0', '0', '', '1511432853', '4', '0', '', '0', '1', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1014', '40', '0', '40', '2017112328246', '4', '1', '1', '汉街总部国际A座701', '', '114.337482', '30.557885', '13397140486', '脚底按摩', '50.00', '0', '50.00', '0.00', '0.00', '50.00', '0', '0', '', '1511436423', '4', '1', '1511438400', '20', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1017', '46', '0', '29', '2017112429771', '3', '0', '1', '湖北省武汉市武昌区汉街总部国际(武汉杜莎夫人蜡像馆西南)  11222', '', '114.343292', '30.554939', '17778079115', '脚底按摩', '128.00', '0', '128.00', '0.00', '0.00', '128.00', '0', '0', '', '1511487926', '22', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1018', '40', '0', '40', '2017112432003', '2', '1', '1', '汉街总部国际A座701', '', '114.337483', '30.557861', '13397140486', '脚底按摩', '50.00', '0', '50.00', '0.00', '0.00', '50.00', '0', '0', '', '1511488027', '4', '0', '', '0', '1', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1021', '40', '0', '40', '2017112427329', '2', '1', '1', '汉街总部国际A座701', '', '114.337506', '30.557889', '13397140486', '脚底按摩', '50.00', '0', '50.00', '0.00', '0.00', '50.00', '0', '0', '', '1511505604', '4', '0', '', '0', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1023', '40', '0', '40', '2017112441969', '4', '1', '1', '汉街总部国际A座701', '', '114.337493', '30.557879', '13397140486', '脚底按摩', '50.00', '0', '50.00', '0.00', '0.00', '50.00', '0', '0', '', '1511514571', '4', '0', '', '0', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1024', '40', '0', '40', '2017112445139', '4', '1', '1', '汉街总部国际A座701', '', '114.337535', '30.557936', '13397140486', '脚底按摩', '50.00', '0', '50.00', '0.00', '0.00', '50.00', '0', '0', '', '1511515735', '4', '0', '', '0', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1025', '40', '0', '40', '2017112466118', '2', '1', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥楚河汉街总部国际a座701', '', '114.337488', '30.557915', '13397140486', '脚底按摩', '50.00', '0', '50.00', '0.00', '0.00', '50.00', '0', '0', '', '1511515959', '4', '0', '', '0', '1', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1026', '46', '0', '40', '2017112423720', '4', '1', '1', '湖北省武汉市武昌区汉街总部国际(武汉杜莎夫人蜡像馆西南)  11222', '', '114.343292', '30.554939', '17778079115', '脚底按摩', '50.00', '0', '50.00', '0.00', '0.00', '50.00', '0', '0', '', '1511516237', '4', '1', '1511528400', '21', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1027', '40', '0', '40', '2017112580397', '1', '0', '1', '武汉市武昌区雄楚大街附42号(晒湖小区旁)1栋', '', '114.313966', '30.521106', '13397140486', '脚底按摩', '50.00', '0', '50.00', '0.00', '0.00', '50.00', '0', '0', '', '1511584231', '4', '1', '1511586000', '13', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1032', '36', '0', '36', '2017112699285', '3', '0', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  14', '', '114.342437', '30.555744', '17137214472', '足底按摩', '60.00', '0', '60.00', '0.00', '0.00', '60.00', '0', '0', '', '1511701910', '12', '0', '', '0', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1033', '77', '0', '3', '2017112634110', '1', '0', '1', '武汉市武昌区东湖路142号11', '', '114.357376', '30.559806', '18971193603', '足底按摩', '60.00', '0', '60.00', '0.00', '0.00', '60.00', '0', '0', '', '1511702143', '12', '1', '1511704800', '22', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1034', '77', '0', '3', '2017112684112', '1', '0', '1', '武汉市武昌区东湖路142号11', '', '114.357393', '30.559735', '18971193603', '足底按摩', '60.00', '0', '60.00', '0.00', '0.00', '60.00', '0', '0', '', '1511702261', '12', '0', '', '0', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1035', '36', '0', '36', '2017112646686', '3', '0', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥了解  太寂寞', '', '114.342437', '30.555744', '18008655056', '足底按摩', '60.00', '0', '60.00', '0.00', '0.00', '60.00', '0', '0', '', '1511702478', '12', '0', '', '0', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1037', '36', '0', '36', '2017112687083', '3', '0', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  14', '', '114.342437', '30.555744', '17137214472', '足底按摩', '60.00', '0', '60.00', '0.00', '0.00', '60.00', '0', '0', '', '1511702939', '12', '2', '1511722800', '3', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1039', '36', '0', '36', '2017112678969', '3', '0', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  14', '', '114.342437', '30.555744', '17137214472', '足底按摩', '60.00', '0', '60.00', '0.00', '0.00', '60.00', '0', '0', '', '1511703561', '12', '0', '', '0', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1040', '36', '0', '36', '2017112631090', '3', '0', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  金融服务', '', '114.342437', '30.555744', '17137214472', '足底按摩', '60.00', '0', '60.00', '0.00', '0.00', '60.00', '0', '0', '', '1511703762', '12', '0', '', '0', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1041', '36', '0', '36', '2017112670282', '3', '0', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥了解  太寂寞', '', '114.342437', '30.555744', '17137214472', '足底按摩', '60.00', '0', '60.00', '0.00', '0.00', '60.00', '0', '0', '', '1511704055', '12', '0', '', '0', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1042', '36', '0', '36', '2017112755634', '3', '0', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥了解  太寂寞', '', '114.342437', '30.555744', '17137214472', '足底按摩', '60.00', '0', '60.00', '0.00', '0.00', '60.00', '0', '0', '', '1511747588', '12', '0', '', '0', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1044', '32', '0', '43', '2017112778170', '2', '1', '1', '湖北省武汉市江岸区二曜路1号5564', '', '114.337636', '30.557909', '18206849645', '足底按摩', '60.00', '0', '60.00', '0.00', '0.00', '60.00', '0', '0', '', '1511753587', '12', '1', '1511726400', '4', '1', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1045', '32', '0', '43', '2017112757193', '4', '1', '1', '湖北省武汉市江岸区二曜路1号5564', '', '114.337628', '30.557939', '18206849645', '足底按摩', '60.00', '0', '60.00', '0.00', '0.00', '60.00', '0', '0', '', '1511754359', '12', '1', '1511762400', '14', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1049', '37', '0', '29', '2017112748653', '1', '0', '1', '武昌区汉街总部国际  看', '', '114.343292', '30.554939', '18008655056', '足底按摩', '60.00', '0', '60.00', '0.00', '0.00', '60.00', '0', '0', '', '1511791631', '12', '0', '', '0', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1050', '36', '0', '36', '2017112874205', '3', '0', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥了解  太寂寞', '', '114.342437', '30.555744', '17137214472', '按摩', '518.00', '0', '518.00', '0.00', '0.00', '518.00', '0', '0', '', '1511834547', '9', '0', '', '0', '0', '120', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1051', '34', '0', '29', '2017112815057', '4', '1', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  北', '', '114.342437', '30.555744', '18507151771', '脚底按摩', '50.00', '0', '50.00', '0.00', '0.00', '50.00', '0', '0', '', '1511837097', '4', '0', '', '0', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1052', '34', '0', '29', '2017112855596', '4', '1', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  北', '', '114.342437', '30.555744', '15172370138', '足底按摩', '60.00', '0', '60.00', '0.00', '0.00', '60.00', '0', '0', '', '1511837578', '12', '0', '', '0', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1053', '36', '0', '29', '2017112865185', '3', '0', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥了解  太寂寞', '', '114.342437', '30.555744', '17137214472', '刮痧', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '咯嘛', '1511837633', '30', '1', '1511841600', '12', '0', '120', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1054', '36', '0', '29', '2017112880838', '3', '0', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥了解  太寂寞', '', '114.342437', '30.555744', '17137214472', '刮痧', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '咯嘛', '1511837645', '30', '1', '1511841600', '12', '0', '120', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1055', '36', '0', '29', '2017112879418', '3', '0', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥了解  太寂寞', '', '114.342437', '30.555744', '17137214472', '刮痧', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '咯嘛', '1511837649', '30', '1', '1511841600', '12', '0', '120', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1056', '36', '0', '2', '2017112876177', '3', '0', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥了解  太寂寞', '', '114.342437', '30.555744', '17137214472', '足底按摩', '60.00', '0', '60.00', '0.00', '0.00', '60.00', '0', '0', '', '1511844835', '12', '1', '1511848800', '14', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1057', '77', '0', '37', '2017112866232', '4', '1', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  北', '', '114.342437', '30.555744', '18971193603', '足底按摩', '60.00', '0', '60.00', '0.00', '0.00', '60.00', '0', '0', '', '1511851782', '12', '0', '', '0', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1058', '77', '0', '37', '2017112860333', '4', '1', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  北', '', '114.342437', '30.555744', '18971193603', '足底按摩', '60.00', '0', '60.00', '0.00', '0.00', '60.00', '0', '0', '', '1511851883', '12', '1', '1511856000', '16', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1059', '31', '0', '29', '2017112861594', '4', '1', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  北', '', '114.342437', '30.555744', '15172370138', '脚底按摩', '50.00', '0', '50.00', '0.00', '0.00', '50.00', '0', '0', '', '1511852073', '4', '0', '', '0', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1060', '31', '0', '29', '2017112844189', '4', '1', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  北', '', '114.342437', '30.555744', '15172370138', '脚底按摩', '50.00', '0', '50.00', '0.00', '0.00', '50.00', '0', '0', '', '1511852233', '4', '0', '', '0', '0', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1061', '81', '0', '3', '2017112850989', '4', '1', '1', '武汉市武昌区楚河南路22', '', '114.337541', '30.557900', '18971193603', '刮痧', '65.00', '0', '65.00', '0.00', '0.00', '65.00', '0', '0', '', '1511857826', '3', '1', '1511874000', '21', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1063', '81', '0', '3', '2017112809625', '2', '1', '1', '武汉市武昌区楚河南路22', '', '114.337475', '30.557858', '18971193603', '刮痧', '65.00', '0', '65.00', '0.00', '0.00', '65.00', '0', '0', '吃吃吃 v', '1511858501', '3', '0', '', '0', '1', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1064', '40', '0', '40', '2017112881737', '6', '1', '1', '汉街总部国际A座701', '', '114.337487', '30.557885', '13397140486', '脚底按摩', '50.00', '0', '50.00', '0.00', '0.00', '50.00', '0', '0', '', '1511859134', '4', '0', '', '0', '1', '20', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1067', '81', '0', '1', '2017112960409', '1', '0', '1', '武汉市武昌区楚河南路22', '', '114.343071', '30.5553', '18971193603', '按摩', '518.00', '0', '518.00', '0.00', '0.00', '518.00', '0', '0', '', '1511918629', '9', '1', '1511920800', '10', '0', '120', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1068', '81', '0', '1', '2017112942836', '1', '0', '1', '武汉市武昌区楚河南路22', '', '114.343079', '30.555285', '18971193603', '   泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1511925961', '4', '1', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1069', '36', '0', '2', '2017112938333', '1', '0', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥了解  太寂寞', '', '114.342437', '30.555744', '17137214472', '   泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1511933410', '4', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1071', '32', '0', '48', '2017112967000', '4', '1', '1', '武汉市汉南区振兴街与王家岭路交叉口西50米一号', '', '114.337517', '30.557922', '18206849645', '   专业采耳+温灸', '88.00', '0', '88.00', '0.00', '0.00', '88.00', '0', '0', '', '1511942997', '34', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1073', '31', '0', '29', '2017112940263', '4', '1', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  南', '', '114.342437', '30.555744', '15172370138', '   专业采耳+温灸', '88.00', '0', '88.00', '0.00', '0.00', '88.00', '0', '0', '', '1511943063', '34', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1074', '81', '0', '29', '2017112945625', '4', '1', '1', '向阳佳苑A区  5栋', '', '114.300430', '30.599289', '18971193603', '   专业采耳+温灸', '88.00', '0', '88.00', '0.00', '0.00', '88.00', '0', '0', '', '1511949483', '34', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1075', '81', '0', '29', '2017112909117', '1', '0', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  北', '', '114.342437', '30.555744', '18971193603', '   泡脚+修脚', '88.00', '0', '88.00', '0.00', '0.00', '88.00', '0', '0', '', '1511949669', '33', '1', '1511953200', '19', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1076', '81', '0', '29', '2017112957209', '1', '0', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  北', '', '114.342437', '30.555744', '18971193603', '   泡脚+修脚', '88.00', '0', '88.00', '0.00', '0.00', '88.00', '0', '0', '', '1511949691', '33', '1', '1511953200', '19', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1077', '81', '0', '29', '2017112926507', '3', '0', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  北', '', '114.342437', '30.555744', '18971193603', '   泡脚+修脚', '88.00', '0', '88.00', '0.00', '0.00', '88.00', '0', '0', '', '1511949698', '33', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1078', '81', '0', '29', '2017112944882', '3', '0', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  北', '', '114.342437', '30.555744', '18971193603', '   泡脚+修脚', '88.00', '0', '88.00', '0.00', '0.00', '88.00', '0', '0', '', '1511949700', '33', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1079', '81', '0', '29', '2017112963957', '3', '1', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  北', '', '114.342437', '30.555744', '18971193603', '   泡脚+修脚', '88.00', '0', '88.00', '0.00', '0.00', '88.00', '0', '0', '', '1511949708', '33', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1080', '32', '0', '48', '2017112900498', '4', '1', '1', '武汉市武昌区楚河南路楚河汉街站', '', '114.337525', '30.557893', '18206849645', '   面部精致补水', '88.00', '0', '88.00', '0.00', '0.00', '88.00', '0', '0', '', '1511950371', '31', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1081', '89', '0', '52', '2017113069642', '4', '1', '1', '汉街总部国际A座  111', '', '114.343361', '30.554778', '18971193603', '   泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512037235', '4', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1082', '32', '0', '48', '2017113014408', '4', '1', '1', '武汉市武昌区楚河南路一号', '', '114.337500', '30.557883', '18206849645', '   面部精致补水', '88.00', '0', '88.00', '0.00', '0.00', '88.00', '0', '0', '', '1512038097', '31', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1084', '32', '0', '48', '2017120197665', '4', '1', '1', '武汉市武昌区中北路163号楚河汉街', '', '114.337661', '30.560341', '18206849645', '   泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512092032', '4', '1', '1512108000', '14', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1085', '32', '0', '48', '2017120161938', '4', '1', '1', '武汉市武昌区楚河南路一号', '', '114.336936', '30.558722', '18206849645', '   泡脚+足底按摩', '118.00', '0', '118.00', '5.00', '0.00', '113.00', '0', '0', '希望快点上门服务 电话沟通', '1512109080', '4', '1', '1512115200', '16', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1086', '31', '0', '2', '2017120109426', '4', '1', '1', '武汉市武昌区楚河南路三号', '', '114.336587', '30.558177', '15172370138', '   泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512119906', '4', '1', '1512129600', '20', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1089', '32', '0', '48', '2017120113721', '4', '1', '1', '武汉市武昌区中北路163号楚河汉街', '', '114.337428', '30.557870', '18206849645', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512129591', '4', '1', '1512136800', '22', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1091', '36', '0', '3', '2017120486187', '1', '0', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  14', '', '114.342437', '30.555744', '17137214472', '头颈肩理疗+艾灸', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512367762', '27', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1096', '89', '0', '26', '2017120535967', '3', '0', '1', '天安门西(公交站)  南广场', '', '114.323685', '30.553913', '18971193603', '肩颈推拿', '128.00', '0', '128.00', '0.00', '0.00', '128.00', '0', '0', '', '1512441931', '11', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1097', '46', '0', '2', '2017120529524', '3', '0', '1', '天安门东(地铁站)  南', '', '114.285507', '30.584314', '17778079115', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512442250', '4', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1098', '46', '0', '2', '2017120534441', '3', '0', '0', '湖北省武汉市武昌区停车场(东湖宾馆怡园别墅西)东湖路与兴国南路交叉口东150米161  1', '', '114.362293', '30.557176', '17778079115', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512443107', '4', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1099', '46', '0', '2', '2017120543905', '3', '0', '0', '湖北省武汉市武昌区汉街总部国际(武汉杜莎夫人蜡像馆西南)  11', '', '114.343292', '30.554939', '17778079115', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512443274', '4', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1100', '108', '0', '3', '2017120599993', '4', '1', '0', '武汉市武昌区松竹路28号五号', '', '114.336565', '30.558115', '18107214796', '专业采耳+温灸', '88.00', '0', '88.00', '0.00', '0.00', '88.00', '0', '0', '测试一下', '1512463994', '34', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1101', '36', '0', '2', '2017120578032', '3', '0', '0', '武汉市武昌区中央文化区K6、K7地块一期第K6-11层商7号一号', '', '114.336003', '30.558409', '18206849645', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512464372', '4', '1', '1512486000', '23', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1102', '36', '0', '34', '2017120621203', '3', '0', '0', '武汉市武昌区中央文化区K6、K7地块一期第K6-11层商7号一号', '', '114.336003', '30.558409', '18206849645', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512534047', '4', '1', '1512572400', '23', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1103', '112', '0', '3', '2017120630716', '1', '0', '0', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  肯德基', '', '114.342437', '30.555744', '18107214796', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512554214', '4', '1', '1512565200', '21', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1104', '112', '0', '3', '2017120686438', '1', '0', '0', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  肯德基', '', '114.342437', '30.555744', '18107214796', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512554479', '4', '1', '1512565200', '21', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1105', '112', '0', '3', '2017120643682', '3', '0', '0', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  肯德基', '', '114.342437', '30.555744', '18107214796', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512554494', '4', '1', '1512565200', '22', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1106', '46', '0', '59', '2017120665318', '3', '0', '0', '湖北省武汉市武昌区停车场(东湖宾馆怡园别墅西)东湖路与兴国南路交叉口东150米161  222', '', '114.362293', '30.557176', '17778079115', '精油刮痧+全身推拿', '108.00', '0', '108.00', '0.00', '0.00', '108.00', '0', '0', '', '1512554521', '3', '2', '1512579600', '2', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1107', '112', '0', '3', '2017120626396', '3', '0', '0', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  肯德基', '', '114.342437', '30.555744', '18107214796', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512554578', '4', '1', '1512565200', '22', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1108', '113', '0', '3', '2017120651998', '3', '0', '0', '武汉市武昌区汉街76号一号', '', '114.343006', '30.555815', '18206849645', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512554646', '4', '1', '1512572400', '24', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1109', '46', '0', '59', '2017120651196', '3', '0', '0', '湖北省武汉市武昌区停车场(东湖宾馆怡园别墅西)东湖路与兴国南路交叉口东150米161  222', '', '114.362293', '30.557176', '17778079115', '精油刮痧+全身推拿', '108.00', '0', '108.00', '0.00', '0.00', '108.00', '0', '0', '', '1512554651', '3', '2', '1512579600', '2', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1110', '112', '0', '3', '2017120678924', '3', '0', '0', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  肯德基', '', '114.342437', '30.555744', '18107214796', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512554763', '4', '1', '1512565200', '22', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1111', '112', '0', '3', '2017120684941', '3', '0', '0', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  肯德基', '', '114.342437', '30.555744', '18107214796', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512554773', '4', '1', '1512565200', '22', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1112', '112', '0', '3', '2017120627508', '3', '0', '0', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  肯德基', '', '114.342437', '30.555744', '18107214796', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512554811', '4', '1', '1512565200', '22', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1113', '112', '0', '3', '2017120659322', '3', '0', '0', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  肯德基', '', '114.342437', '30.555744', '18107214796', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512554832', '4', '1', '1512565200', '22', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1114', '112', '0', '3', '2017120688923', '3', '0', '0', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  肯德基', '', '114.342437', '30.555744', '18107214796', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512555038', '4', '1', '1512565200', '22', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1115', '112', '0', '3', '2017120627671', '1', '0', '0', '湖北省武汉市武昌区楚河南路靠近中北路立交桥  肯德基', '', '114.342437', '30.555744', '18107214796', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512558625', '4', '1', '1512565200', '22', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1116', '46', '0', '3', '2017120674791', '3', '0', '0', '湖北省武汉市武昌区停车场(东湖宾馆怡园别墅西)东湖路与兴国南路交叉口东150米161  222', '', '114.362293', '30.557176', '17778079115', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512558755', '4', '1', '1512565200', '22', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1117', '46', '0', '3', '2017120641950', '3', '0', '0', '湖北省武汉市武昌区停车场(东湖宾馆怡园别墅西)东湖路与兴国南路交叉口东150米161  111', '', '114.362293', '30.557176', '17778079115', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512558946', '4', '1', '1512561600', '21', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1118', '36', '0', '40', '2017120600215', '3', '0', '0', '湖北省武汉市武昌区楚河南路靠近中北路立交桥了解  太寂寞', '', '114.342437', '30.555744', '17137214472', '拔罐+肩颈经络按摩+开背', '88.00', '0', '88.00', '0.00', '0.00', '88.00', '0', '0', '', '1512561649', '8', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1119', '36', '0', '3', '2017120667233', '3', '0', '0', '湖北省武汉市武昌区楚河南路靠近中北路立交桥了解  太寂寞', '', '114.342437', '30.555744', '17137214472', '拔罐+精油按摩+开背', '108.00', '0', '108.00', '0.00', '0.00', '108.00', '0', '0', '昨晚', '1512571280', '40', '2', '1512594000', '6', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1120', '46', '0', '39', '2017120738322', '3', '0', '0', '天安门东(地铁站)  南', '', '114.285507', '30.584314', '17778079115', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512609736', '4', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1121', '115', '0', '2', '2017120702594', '1', '0', '0', '南京市玄武区北京东路4号南京广播电视台', '', '118.776259', '32.063890', '13076949806', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512621680', '4', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1124', '36', '0', '34', '2017120755750', '1', '0', '0', '武汉市武昌区中央文化区K6、K7地块一期第K6-11层商7号  一号', '', '114.316223', '30.554235', '17137214472', '头颈肩理疗+艾灸', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512661302', '27', '0', '', '0', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1125', '46', '0', '37', '2017120855296', '4', '1', '0', '湖北省武汉市武昌区楚河南路靠近中北路立交桥宿舍', '', '114.342779', '30.555926', '18107214796', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1512699790', '4', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1127', '89', '0', '23', '2017120851335', '3', '0', '1', '湖北省武汉市武昌区楚河南路靠近中北路立交桥1141', '', '114.362968', '30.557407', '18971193603', '中药泡脚+足疗+足底刮痧', '158.00', '0', '158.00', '0.00', '0.00', '158.00', '0', '0', '', '1512743511', '12', '2', '1512756000', '3', '0', '70', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1130', '36', '0', '58', '2017121028643', '3', '0', '0', '湖北省武汉市武昌区楚河南路靠近中北路立交桥了解  太寂寞', '', '114.342437', '30.555744', '17137214472', '面部精致补水', '88.00', '0', '88.00', '0.00', '0.00', '88.00', '0', '0', '', '1512867566', '31', '1', '1512871200', '11', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1131', '34', '0', '1', '2017121069968', '1', '0', '1', '洪山区徐东馨苑(杨园南路西150米)啊啊啊', '', '114.342865', '30.555374', '18507151771', '头部颈椎按摩', '158.00', '0', '158.00', '0.00', '0.00', '158.00', '0', '0', '', '1512873524', '9', '1', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1132', '46', '0', '69', '2017121363875', '3', '0', '0', '湖北省武汉市武昌区丁字桥路靠近中国邮政储蓄银行(梅苑营业所)  A204', '', '114.332166', '30.526534', '17778079115', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1513177773', '4', '0', '', '0', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1133', '96', '0', '40', '2017121444593', '1', '0', '0', '武汉市武昌区中北路126号一号', '', '114.356946', '30.563709', '18206849645', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1513259804', '4', '1', '1513263600', '24', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1134', '96', '0', '26', '2017121535601', '1', '0', '0', '武汉市洪山区武珞路790号一号', '', '114.349410', '30.525737', '18206849645', '面部精致补水', '88.00', '0', '88.00', '0.00', '0.00', '88.00', '0', '0', '', '1513349050', '31', '0', '', '0', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1135', '140', '0', '40', '2017121620123', '1', '0', '0', '湖北省武汉市洪山区文化路靠近韵湖春晓  韵湖春晓6栋2单元2201室', '', '114.322261', '30.434410', '13296521863', '中药泡脚+足疗+足底刮痧', '158.00', '0', '158.00', '0.00', '0.00', '158.00', '0', '0', '', '1513429782', '12', '0', '', '0', '0', '70', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1136', '141', '0', '39', '2017121648794', '1', '0', '0', '湖北省武汉市蔡甸区东风大道靠近东风公司(地铁站)  58号', '', '114.166201', '30.501004', '18040508444', '中医足疗+反射区调理', '188.00', '0', '188.00', '0.00', '0.00', '188.00', '0', '0', '', '1513436261', '22', '0', '', '0', '0', '70', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1137', '141', '0', '39', '2017121641623', '7', '1', '0', '湖北省武汉市蔡甸区东风大道靠近东风公司(地铁站)  58', '', '114.166201', '30.501004', '18040508444', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1513436310', '4', '0', '', '0', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1138', '149', '0', '34', '2017122011693', '1', '0', '0', '武汉市武昌区小东门民主路739号29-1-201', '', '114.329667', '30.546922', '15927313393', '泡脚+修脚', '88.00', '0', '88.00', '0.00', '0.00', '88.00', '0', '0', '赶紧上门！！！！', '1513763209', '33', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1139', '156', '0', '26', '2017122544930', '1', '0', '0', '河北省保定市定州市兴华东路靠近鼓典架子鼓艺术中心  金地花园212', '', '114.376964', '30.622324', '18632292112', '头部颈椎按摩', '158.00', '0', '158.00', '0.00', '0.00', '158.00', '0', '0', '', '1514204985', '9', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1140', '181', '0', '26', '2017123047551', '3', '0', '0', '吉楚快捷酒店  403', '', '114.288158', '30.582254', '13317225956', '头部颈椎按摩', '158.00', '0', '158.00', '0.00', '0.00', '158.00', '0', '0', '', '1514615149', '9', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1141', '182', '0', '39', '2017123072630', '4', '1', '0', '海南省三亚市天涯区铁路通道靠近三亚市第四小学  紫椰林主题酒店302', '', '114.242355', '30.630121', '18030608882', '舒筋修脚', '128.00', '0', '128.00', '0.00', '0.00', '128.00', '0', '0', '', '1514646711', '42', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1142', '182', '0', '26', '2017123007374', '4', '1', '0', '海南省三亚市天涯区铁路通道靠近三亚市第四小学  紫椰林主题酒店302', '', '114.242355', '30.630121', '18030608882', '头部颈椎按摩', '158.00', '0', '158.00', '0.00', '0.00', '158.00', '0', '0', '', '1514646972', '9', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1143', '198', '0', '2', '2018011062118', '1', '0', '0', '当前位置  丹阳市维多利海休闲会所8012', '', '114.363431', '30.522696', '15952675090', '火罐+全身推拿+精油开背', '128.00', '0', '128.00', '0.00', '0.00', '128.00', '0', '0', '', '1515566905', '41', '0', '', '0', '0', '90', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1144', '198', '0', '34', '2018011068104', '1', '0', '0', '当前位置  丹阳市维多利海休闲会所8012', '', '114.363431', '30.522696', '15952675090', '专业全身推拿', '158.00', '0', '158.00', '0.00', '0.00', '158.00', '0', '0', '', '1515569377', '25', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1145', '96', '0', '26', '2018011000395', '3', '0', '0', '武汉市洪山区武珞路790号一号', '', '114.349410', '30.525737', '18206549644', '精油刮痧+全身推拿', '108.00', '0', '108.00', '0.00', '0.00', '108.00', '0', '0', '', '1515584159', '3', '1', '1515589200', '22', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1149', '204', '0', '2', '2018011713645', '3', '0', '0', '吉林省长春市南关区久长路靠近贝迪堡长春国际早教中心  11111', '', '114.346915', '30.589007', '18500399977', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1516201458', '4', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1150', '223', '0', '34', '2018020393275', '1', '0', '0', '山西省太原市小店区南中环街辅路靠近晋城银行(太原南中环街支行)  山西省太原市解放路永济小区1单元2107', '', '114.298216', '30.540496', '13453180000', '全身经络按摩', '218.00', '0', '218.00', '0.00', '0.00', '218.00', '0', '0', '', '1517668100', '38', '0', '', '0', '0', '90', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1151', '230', '0', '2', '2018021751744', '4', '1', '0', '上海市长宁区天山路靠近招商银行(上海天山支行)  天山路760弄4号405', '', '114.413082', '30.501956', '13918215518', '泡脚+修脚', '88.00', '0', '88.00', '0.00', '0.00', '88.00', '0', '0', '', '1518849844', '33', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1152', '107', '0', '69', '2018031912589', '3', '0', '0', '湖北省武汉市武昌区楚河南路靠近杜莎夫人蜡像馆(汉街)  7楼', '', '114.344520', '30.555134', '15172370138', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1521445937', '4', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1153', '278', '0', '40', '2018032847364', '7', '1', '0', '城市便捷酒店(广州天平架地铁站店)  3055', '', '114.218756', '30.605850', '13410966969', '全身经络按摩', '218.00', '0', '218.00', '0.00', '0.00', '218.00', '0', '0', '', '1522249155', '38', '0', '', '0', '0', '90', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1154', '284', '0', '2', '2018040411868', '1', '0', '0', '广西壮族自治区贺州市钟山县西乐街南路靠近中国人民银行(钟山支行)  左边第一栋2楼', '', '114.335400', '30.542956', '14777786266', '头部颈椎按摩', '158.00', '0', '158.00', '0.00', '0.00', '158.00', '0', '0', '\n', '1522834892', '9', '1', '1522843200', '21', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1155', '287', '0', '2', '2018040652538', '3', '0', '0', '广东省深圳市南山区塘益路靠近塘朗商务公寓写字楼A座  塘朗新村82栋', '', '114.368251', '30.530463', '15889723345', '中医足疗+反射区调理', '188.00', '0', '188.00', '0.00', '0.00', '188.00', '0', '0', '', '1522976043', '22', '1', '1522980000', '11', '0', '70', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1156', '287', '0', '2', '2018040629342', '4', '1', '0', '广东省深圳市南山区塘益路靠近塘朗商务公寓写字楼A座  塘朗新村82栋', '', '114.368251', '30.530463', '15889723345', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1522976153', '4', '1', '1522980000', '11', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1161', '291', '0', '3', '2018041394335', '3', '0', '0', '湖北省武汉市黄陂区汉施路靠近中国建设银行(武汉武湖支行)  东方城一期7栋2902', '', '114.424387', '30.705288', '15327168878', '中药泡脚+足疗+足底刮痧', '158.00', '0', '158.00', '0.00', '0.00', '158.00', '0', '0', '', '1523629199', '12', '1', '1523631600', '24', '0', '70', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1162', '291', '0', '3', '2018042748178', '3', '0', '0', '湖北省武汉市黄陂区汉施路靠近中国建设银行(武汉武湖支行)  东方城一期7栋2902', '', '114.424387', '30.705288', '15327168878', '中药泡脚+足疗+足底刮痧', '158.00', '0', '158.00', '0.00', '0.00', '158.00', '0', '0', '', '1524795709', '12', '1', '1524798000', '12', '0', '70', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1163', '310', '0', '34', '2018043059883', '1', '0', '0', '山东省济南市历下区和平路靠近泉城新时代商业广场  901', '', '114.256796', '30.551083', '15550309955', '头部颈椎按摩', '158.00', '0', '158.00', '0.00', '0.00', '158.00', '0', '0', '', '1525099564', '9', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1164', '291', '0', '3', '2018051155584', '3', '0', '0', '湖北省武汉市江岸区解放大道靠近江岸区食品药品监督管理局丹水池监督所  丹水池友发商务酒店四楼8427房', '', '114.332330', '30.649006', '15327168878', '中药泡脚+足疗+足底刮痧', '158.00', '0', '158.00', '0.00', '0.00', '158.00', '0', '0', '', '1526044107', '12', '1', '1526050800', '24', '0', '70', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1166', '291', '0', '3', '2018051258009', '8', '1', '0', '湖北省武汉市江岸区解放大道靠近江岸区食品药品监督管理局丹水池监督所  丹水池友发商务酒店四楼8427房', '', '114.332330', '30.649006', '15327168878', '中药泡脚+足疗+足底刮痧', '158.00', '0', '158.00', '0.00', '0.00', '158.00', '0', '0', '', '1526055590', '12', '1', '1526058000', '2', '0', '70', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1167', '89', '0', '69', '2018051423712', '3', '0', '0', '湖北省武汉市武昌区松竹路靠近杜莎夫人蜡像馆(汉街)  11', '', '114.344520', '30.555134', '18971193603', '精油刮痧+全身推拿', '108.00', '0', '108.00', '0.00', '0.00', '108.00', '0', '0', '', '1526262170', '3', '1', '1526266800', '12', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1168', '89', '0', '34', '2018061208958', '3', '0', '1', '湖北省武汉市武昌区松竹路靠近杜莎夫人蜡像馆(汉街)11', '', '114.343117', '30.555269', '18971193603', '头颈肩理疗+艾灸', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1528770005', '27', '2', '1528833600', '5', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1169', '89', '0', '34', '2018061214501', '3', '0', '1', '湖北省武汉市武昌区松竹路靠近杜莎夫人蜡像馆(汉街)11', '', '114.343079', '30.555294', '18971193603', '头颈肩理疗+艾灸', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1528778556', '27', '1', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1170', '89', '0', '2', '2018061267114', '3', '0', '0', '湖北省武汉市武昌区松竹路靠近杜莎夫人蜡像馆(汉街)  11', '', '114.344520', '30.555134', '18971193603', '肩颈推拿', '128.00', '0', '128.00', '0.00', '0.00', '128.00', '0', '0', '', '1528778683', '11', '1', '1528783200', '15', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1171', '127', '0', '69', '2018062969494', '1', '0', '0', '武汉市武昌区楚河汉街1号武汉中央文化旅游区-楚河汉街一街区F1层哟哟', '', '114.343448', '30.554997', '18507151771', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1530262978', '4', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1172', '439', '0', '40', '2018081114742', '3', '0', '0', '江苏省无锡市新吴区南丰路靠近未来星艺术培训中心(南丰路)  无锡市新吴区叙丰家园148号201', '', '114.403289', '30.487395', '13338110086', '舒筋修脚', '128.00', '0', '128.00', '0.00', '0.00', '128.00', '0', '0', '', '1533986582', '42', '1', '1533988800', '21', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1173', '439', '0', '40', '2018081180697', '4', '1', '0', '江苏省无锡市新吴区南丰路靠近未来星艺术培训中心(南丰路)  无锡市新吴区叙丰家园148号201', '', '114.403289', '30.487395', '13338110086', '泡脚+修脚', '88.00', '0', '88.00', '0.00', '0.00', '88.00', '0', '0', '', '1533986624', '33', '1', '1533988800', '21', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1174', '576', '0', '3', '2018100725293', '3', '0', '0', '草埔(地铁站)  比毕丽山庄10栋302', '', '114.301812', '30.599398', '15815538780', '泡脚+修脚', '88.00', '0', '88.00', '0.00', '0.00', '88.00', '0', '0', '', '1538920180', '33', '2', '1539003600', '22', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1175', '576', '0', '3', '2018100702772', '3', '0', '0', '草埔(地铁站)  比毕丽山庄10栋302', '', '114.301812', '30.599398', '15815538780', '泡脚+修脚', '88.00', '0', '88.00', '0.00', '0.00', '88.00', '0', '0', '', '1538920202', '33', '2', '1539003600', '22', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1176', '616', '0', '2', '2018120318145', '1', '0', '0', '武汉市武昌区八一路92号帅府饭店', '', '114.347610', '30.536316', '15989378878', '头颈肩理疗+艾灸', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '颈肩腰腿酸痛', '1543836781', '27', '1', '1543842000', '22', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1177', '616', '0', '2', '2018120315508', '7', '1', '0', '武汉市武昌区八一路92号帅府饭店', '', '114.347610', '30.536316', '15989378878', '头颈肩理疗+艾灸', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '颈肩腰腿酸痛', '1543836804', '27', '1', '1543842000', '22', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1181', '700', '0', '3', '2019010461337', '3', '0', '1', '湖北省武汉市武昌区白鹭街86附近A座7楼', '', '114.31599', '30.55386', '18062394372', '头部颈椎按摩', '158.00', '0', '158.00', '0.00', '0.00', '158.00', '0', '0', '', '1546565530', '9', '1', '1546614000', '24', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1184', '698', '0', '68', '2019010418828', '3', '0', '0', '武汉市武昌区南湖路与白沙洲大道高架交叉口东南200米7楼', '', '114.285000', '30.490470', '18627056710', '精油刮痧+全身推拿', '108.00', '0', '108.00', '0.00', '0.00', '108.00', '0', '0', '', '1546594126', '3', '1', '1546599600', '20', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1186', '698', '0', '34', '2019010575085', '3', '0', '0', '武汉市武昌区南湖路与白沙洲大道高架交叉口东南200米7楼', '', '114.285000', '30.490470', '18627056710', '拔罐+肩颈经络按摩+开背', '88.00', '0', '88.00', '0.00', '0.00', '88.00', '0', '0', '', '1546650956', '8', '1', '1546653600', '11', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1188', '711', '0', '40', '2019010807562', '6', '1', '0', '武汉市武昌区万达SOHO环球国际中心1号楼26131111', '', '114.336977', '30.558008', '18571710546', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1546914964', '4', '1', '1546902000', '8', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1189', '698', '0', '1', '2019011419642', '3', '0', '0', '武汉市武昌区楚河汉街万达中心1号楼(万达广场2号门对面)哈哈哈', '', '114.307539', '30.564083', '18627056710', '面部精致补水', '88.00', '0', '88.00', '0.00', '0.00', '88.00', '0', '0', '', '1547428527', '31', '1', '1547434800', '12', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1190', '727', '0', '1', '2019011457827', '3', '0', '0', '武汉市武昌区楚河汉街万达中心1号楼(万达广场2号门对面)好', '', '114.307539', '30.564083', '18627056710', '拔罐+肩颈经络按摩+开背', '88.00', '0', '88.00', '0.00', '0.00', '88.00', '0', '0', '', '1547434128', '8', '1', '1547449200', '16', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1191', '658', '0', '2', '2019011576406', '3', '0', '0', '武汉市武昌区松竹路3', '', '114.340949', '30.556482', '13554141532', '头颈肩理疗+艾灸', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1547531084', '27', '1', '1547488800', '3', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1192', '46', '0', '1', '2019011580126', '3', '0', '0', '武汉市武昌区白沙洲大道高架与白沙洲大道高架出口交叉口东南150米1234', '', '138.929653', '37.659905', '18627056710', '拔罐+肩颈经络按摩+开背', '88.00', '0', '88.00', '0.00', '0.00', '88.00', '0', '0', '', '1547536905', '8', '1', '1547542800', '18', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1193', '46', '0', '69', '2019011547682', '3', '0', '0', '武汉市武昌区白沙洲大道高架与白沙洲大道高架出口交叉口东南150米1234', '', '114.310878', '30.524179', '18627056710', '拔罐+肩颈经络按摩+开背', '88.00', '0', '88.00', '0.00', '0.00', '88.00', '0', '0', '', '1547536982', '8', '0', '', '0', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1194', '46', '0', '2', '2019011585316', '3', '0', '0', '湖北省武汉市武昌区丁字桥路靠近中国邮政储蓄银行(梅苑营业所)A204', '', '114.331939', '30.526446', '18627058247', '精油刮痧+全身推拿', '108.00', '0', '108.00', '0.00', '0.00', '108.00', '0', '0', '', '1547537549', '3', '1', '1547542800', '18', '0', '60', null, null, '0.00');
INSERT INTO `xjm_order_list` VALUES ('1195', '658', '0', '1', '2019011672413', '2', '0', '0', '武汉市汉阳区平田南村69号3#1302', '', '114.262591', '30.543466', '13554141532', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1547619320', '4', '1', '1547582400', '5', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1196', '730', '0', '2', '2019012128482', '3', '0', '0', '湖北省武汉市武昌区松竹路靠近中北路立交桥  3106', '', '114.342574', '30.555910', '18521598723', '精油刮痧+全身推拿', '108.00', '0', '108.00', '0.00', '0.00', '108.00', '0', '0', '', '1548033935', '3', '2', '1548165600', '23', '0', '60', null, null, '20.00');
INSERT INTO `xjm_order_list` VALUES ('1197', '730', '0', '2', '2019012111050', '1', '0', '0', '湖北省武汉市武昌区松竹路靠近华夏银行(武昌支行)  嘿嘿嘿', '', '114.342426', '30.554756', '18521598723', '泡脚+足底按摩', '118.00', '0', '118.00', '0.00', '0.00', '118.00', '0', '0', '', '1548036469', '4', '1', '1548046800', '14', '0', '60', null, null, '0.00');

-- ----------------------------
-- Table structure for xjm_point
-- ----------------------------
DROP TABLE IF EXISTS `xjm_point`;
CREATE TABLE `xjm_point` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(2) DEFAULT '0',
  `percent` tinyint(3) unsigned DEFAULT '0' COMMENT '积分获得百分比',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='积分设置表';

-- ----------------------------
-- Records of xjm_point
-- ----------------------------

-- ----------------------------
-- Table structure for xjm_rate_record
-- ----------------------------
DROP TABLE IF EXISTS `xjm_rate_record`;
CREATE TABLE `xjm_rate_record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tid` int(11) DEFAULT '0' COMMENT '关联技师表主键id',
  `score` tinyint(1) DEFAULT '0' COMMENT '评分',
  `add_time` int(10) DEFAULT '0' COMMENT '评定时间',
  `desc` varchar(100) DEFAULT '' COMMENT '描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='评分记录表';

-- ----------------------------
-- Records of xjm_rate_record
-- ----------------------------

-- ----------------------------
-- Table structure for xjm_refund
-- ----------------------------
DROP TABLE IF EXISTS `xjm_refund`;
CREATE TABLE `xjm_refund` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL COMMENT '订单ID',
  `order_sn` varchar(100) DEFAULT NULL COMMENT '订单编号',
  `goods_name` varchar(255) DEFAULT NULL COMMENT '商品名称',
  `refund_reason` varchar(255) DEFAULT NULL COMMENT '退款原因',
  `refund_money` decimal(10,2) DEFAULT NULL COMMENT '退款金额',
  `refund_note` varchar(255) DEFAULT NULL COMMENT '退款备注',
  `uid` int(11) DEFAULT NULL COMMENT '用户id',
  `tid` int(11) DEFAULT NULL COMMENT '技师id',
  `add_time` int(11) DEFAULT NULL COMMENT '添加时间',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态，0已确认，1未确认，2已拒绝，3已完成退款',
  `goods_id` int(11) DEFAULT NULL COMMENT '商品id',
  `server_time` int(11) DEFAULT NULL COMMENT '服务时长',
  `refund_time` int(11) DEFAULT NULL COMMENT '退款时间',
  `goods_price` decimal(10,2) DEFAULT '0.00' COMMENT '商品金额',
  PRIMARY KEY (`id`),
  KEY `time_oid` (`add_time`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=211 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xjm_refund
-- ----------------------------
INSERT INTO `xjm_refund` VALUES ('175', '1003', '2017112307742', '测试', '技师未按时到达', '0.01', '', '35', '39', '1511403656', '3', '35', '120', null, '0.01');
INSERT INTO `xjm_refund` VALUES ('176', '1004', '2017112300326', '测试', '技师未按时到达', '0.01', '系统自动申请退款', '35', '29', '1511420461', '3', '35', '120', null, '0.01');
INSERT INTO `xjm_refund` VALUES ('177', '1014', '2017112328246', '脚底按摩', '技师未按时到达', '50.00', '系统自动申请退款', '40', '40', '1511442061', '3', '4', '20', null, '50.00');
INSERT INTO `xjm_refund` VALUES ('178', '1023', '2017112441969', '脚底按摩', '选填', '50.00', '无备注', '40', '40', '1511514592', '3', '4', '20', null, '50.00');
INSERT INTO `xjm_refund` VALUES ('179', '1024', '2017112445139', '脚底按摩', '选填', '50.00', '无备注', '40', '40', '1511515893', '3', '4', '20', null, '50.00');
INSERT INTO `xjm_refund` VALUES ('180', '1026', '2017112423720', '脚底按摩', '技师未按时到达', '50.00', '系统自动申请退款', '46', '40', '1511528461', '3', '4', '20', null, '50.00');
INSERT INTO `xjm_refund` VALUES ('181', '1045', '2017112757193', '足底按摩', '选填', '48.00', '无备注', '32', '43', '1511756320', '3', '12', '20', null, '60.00');
INSERT INTO `xjm_refund` VALUES ('182', '1051', '2017112815057', '脚底按摩', '与商家协商一致退款', '50.00', '', '34', '29', '1511837137', '3', '4', '20', null, '50.00');
INSERT INTO `xjm_refund` VALUES ('183', '1052', '2017112855596', '足底按摩', '与商家协商一致退款', '60.00', '', '34', '29', '1511837666', '3', '12', '20', null, '60.00');
INSERT INTO `xjm_refund` VALUES ('184', '1058', '2017112860333', '足底按摩', '与商家协商一致退款', '60.00', '', '77', '37', '1511851986', '3', '12', '20', null, '60.00');
INSERT INTO `xjm_refund` VALUES ('185', '1057', '2017112866232', '足底按摩', '与商家协商一致退款', '60.00', '', '77', '37', '1511851994', '3', '12', '20', null, '60.00');
INSERT INTO `xjm_refund` VALUES ('186', '1059', '2017112861594', '脚底按摩', '与商家协商一致退款', '50.00', '', '31', '29', '1511852172', '2', '4', '20', null, '50.00');
INSERT INTO `xjm_refund` VALUES ('187', '1059', '2017112861594', '脚底按摩', '与商家协商一致退款', '50.00', '', '31', '29', '1511852201', '3', '4', '20', null, '50.00');
INSERT INTO `xjm_refund` VALUES ('188', '1060', '2017112844189', '脚底按摩', '与商家协商一致退款', '50.00', '', '31', '29', '1511852283', '3', '4', '20', null, '50.00');
INSERT INTO `xjm_refund` VALUES ('189', '1061', '2017112850989', '刮痧', '技师未按时到达', '65.00', '系统自动申请退款', '81', '3', '1511863261', '3', '3', '60', null, '65.00');
INSERT INTO `xjm_refund` VALUES ('190', '1073', '2017112940263', '   专业采耳+温灸', '与商家协商一致退款', '88.00', '', '31', '29', '1511949015', '3', '34', '60', null, '88.00');
INSERT INTO `xjm_refund` VALUES ('191', '1074', '2017112945625', '   专业采耳+温灸', '与商家协商一致退款', '88.00', '', '81', '29', '1511949589', '3', '34', '60', null, '88.00');
INSERT INTO `xjm_refund` VALUES ('192', '1071', '2017112967000', '   专业采耳+温灸', '技师未按时到达', '88.00', '系统自动申请退款', '32', '48', '1511949661', '3', '34', '60', null, '88.00');
INSERT INTO `xjm_refund` VALUES ('193', '1080', '2017112900498', '   面部精致补水', '技师未按时到达', '88.00', '系统自动申请退款', '32', '48', '1511960461', '3', '31', '60', null, '88.00');
INSERT INTO `xjm_refund` VALUES ('194', '1082', '2017113014408', '   面部精致补水', '技师未按时到达', '88.00', '系统自动申请退款', '32', '48', '1512046861', '3', '31', '60', null, '88.00');
INSERT INTO `xjm_refund` VALUES ('195', '1081', '2017113069642', '   泡脚+足底按摩', '技师未按时到达', '118.00', '系统自动申请退款', '89', '52', '1512057661', '3', '4', '60', null, '118.00');
INSERT INTO `xjm_refund` VALUES ('196', '1084', '2017120197665', '   泡脚+足底按摩', '技师未按时到达', '118.00', '系统自动申请退款', '32', '48', '1512100861', '3', '4', '60', null, '118.00');
INSERT INTO `xjm_refund` VALUES ('197', '1085', '2017120161938', '   泡脚+足底按摩', '技师未按时到达', '113.00', '系统自动申请退款', '32', '48', '1512122461', '3', '4', '60', null, '118.00');
INSERT INTO `xjm_refund` VALUES ('198', '1086', '2017120109426', '   泡脚+足底按摩', '技师未按时到达', '118.00', '系统自动申请退款', '31', '2', '1512133261', '3', '4', '60', null, '118.00');
INSERT INTO `xjm_refund` VALUES ('199', '1089', '2017120113721', '泡脚+足底按摩', '技师未按时到达', '118.00', '系统自动申请退款', '32', '48', '1512144061', '3', '4', '60', null, '118.00');
INSERT INTO `xjm_refund` VALUES ('200', '1100', '2017120599993', '专业采耳+温灸', '技师申请退单', '88.00', '技师联系客服申请退单', '108', '3', '1512464550', '3', '34', '60', '1512464550', '88.00');
INSERT INTO `xjm_refund` VALUES ('201', '1125', '2017120855296', '泡脚+足底按摩', '技师申请退单', '118.00', '技师联系客服申请退单', '46', '37', '1512699887', '3', '4', '60', '1512699887', '118.00');
INSERT INTO `xjm_refund` VALUES ('202', '1137', '2017121641623', '泡脚+足底按摩', '技师未按时到达', '118.00', '系统自动申请退款', '141', '39', '1513450861', '1', '4', '60', null, '118.00');
INSERT INTO `xjm_refund` VALUES ('203', '1141', '2017123072630', '舒筋修脚', '技师未按时到达', '128.00', '打电话也不接，我也不知道什么时候来', '182', '39', '1514648544', '3', '42', '60', null, '128.00');
INSERT INTO `xjm_refund` VALUES ('204', '1142', '2017123007374', '头部颈椎按摩', '技师未按时到达', '158.00', '没到达\n', '182', '26', '1514650301', '3', '9', '60', null, '158.00');
INSERT INTO `xjm_refund` VALUES ('205', '1151', '2018021751744', '泡脚+修脚', '与商家协商一致退款', '88.00', '地区不对', '230', '2', '1518850926', '3', '33', '60', null, '88.00');
INSERT INTO `xjm_refund` VALUES ('206', '1153', '2018032847364', '全身经络按摩', '技师未按时到达', '218.00', '我在广州技师在武汉', '278', '40', '1522249642', '1', '38', '90', null, '218.00');
INSERT INTO `xjm_refund` VALUES ('207', '1156', '2018040629342', '泡脚+足底按摩', '临时有急事', '118.00', '', '287', '2', '1522976395', '3', '4', '60', null, '118.00');
INSERT INTO `xjm_refund` VALUES ('208', '1166', '2018051258009', '中药泡脚+足疗+足底刮痧', '技师未按时到达', '158.00', '系统自动申请退款', '291', '3', '1526065261', '0', '12', '70', null, '158.00');
INSERT INTO `xjm_refund` VALUES ('209', '1173', '2018081180697', '泡脚+修脚', '技师未按时到达', '88.00', '', '439', '40', '1533989082', '3', '33', '60', null, '88.00');
INSERT INTO `xjm_refund` VALUES ('210', '1177', '2018120315508', '头颈肩理疗+艾灸', '技师未按时到达', '118.00', '系统自动申请退款', '616', '2', '1543852861', '1', '27', '60', null, '118.00');

-- ----------------------------
-- Table structure for xjm_refund_desc
-- ----------------------------
DROP TABLE IF EXISTS `xjm_refund_desc`;
CREATE TABLE `xjm_refund_desc` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `desc` varchar(50) DEFAULT NULL COMMENT '退款原因选择',
  `all_payment` varchar(1) DEFAULT '0' COMMENT '0 全款退  1根据时间退',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xjm_refund_desc
-- ----------------------------
INSERT INTO `xjm_refund_desc` VALUES ('1', '与商家协商一致退款', '0');
INSERT INTO `xjm_refund_desc` VALUES ('2', '临时有急事', '1');
INSERT INTO `xjm_refund_desc` VALUES ('3', '技师未按时到达', '0');

-- ----------------------------
-- Table structure for xjm_refund_rule
-- ----------------------------
DROP TABLE IF EXISTS `xjm_refund_rule`;
CREATE TABLE `xjm_refund_rule` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `started` varchar(10) DEFAULT '0' COMMENT '项目超时时时长',
  `proportion` varchar(10) DEFAULT '0' COMMENT '按照项目已开始时间扣除相应比例',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xjm_refund_rule
-- ----------------------------
INSERT INTO `xjm_refund_rule` VALUES ('1', '10', '90');
INSERT INTO `xjm_refund_rule` VALUES ('2', '20', '80');
INSERT INTO `xjm_refund_rule` VALUES ('3', '30', '70');
INSERT INTO `xjm_refund_rule` VALUES ('4', '40', '60');
INSERT INTO `xjm_refund_rule` VALUES ('5', '50', '50');

-- ----------------------------
-- Table structure for xjm_server_time
-- ----------------------------
DROP TABLE IF EXISTS `xjm_server_time`;
CREATE TABLE `xjm_server_time` (
  `idss` int(11) NOT NULL AUTO_INCREMENT,
  `server_time` varchar(20) NOT NULL,
  `id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idss`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xjm_server_time
-- ----------------------------
INSERT INTO `xjm_server_time` VALUES ('1', '01:00', '2');
INSERT INTO `xjm_server_time` VALUES ('2', '02:00', '3');
INSERT INTO `xjm_server_time` VALUES ('3', '03:00', '4');
INSERT INTO `xjm_server_time` VALUES ('4', '04:00', '5');
INSERT INTO `xjm_server_time` VALUES ('5', '05:00', '6');
INSERT INTO `xjm_server_time` VALUES ('6', '06:00', '7');
INSERT INTO `xjm_server_time` VALUES ('7', '07:00', '8');
INSERT INTO `xjm_server_time` VALUES ('8', '08:00', '9');
INSERT INTO `xjm_server_time` VALUES ('9', '09:00', '10');
INSERT INTO `xjm_server_time` VALUES ('10', '10:00', '11');
INSERT INTO `xjm_server_time` VALUES ('11', '11:00', '12');
INSERT INTO `xjm_server_time` VALUES ('12', '12:00', '13');
INSERT INTO `xjm_server_time` VALUES ('13', '13:00', '14');
INSERT INTO `xjm_server_time` VALUES ('14', '14:00', '15');
INSERT INTO `xjm_server_time` VALUES ('15', '15:00', '16');
INSERT INTO `xjm_server_time` VALUES ('16', '16:00', '17');
INSERT INTO `xjm_server_time` VALUES ('17', '17:00', '18');
INSERT INTO `xjm_server_time` VALUES ('18', '18:00', '19');
INSERT INTO `xjm_server_time` VALUES ('19', '19:00', '20');
INSERT INTO `xjm_server_time` VALUES ('20', '20:00', '21');
INSERT INTO `xjm_server_time` VALUES ('21', '21:00', '22');
INSERT INTO `xjm_server_time` VALUES ('22', '22:00', '23');
INSERT INTO `xjm_server_time` VALUES ('23', '23:00', '24');
INSERT INTO `xjm_server_time` VALUES ('24', '00:00', '1');

-- ----------------------------
-- Table structure for xjm_sms_code
-- ----------------------------
DROP TABLE IF EXISTS `xjm_sms_code`;
CREATE TABLE `xjm_sms_code` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mobile` char(11) NOT NULL DEFAULT '' COMMENT '手机号码',
  `code` char(6) NOT NULL DEFAULT '' COMMENT '验证码',
  `sendtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发送时间',
  `type` varchar(50) NOT NULL COMMENT '短信类型',
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=1890 DEFAULT CHARSET=utf8 COMMENT='短信验证码表';

-- ----------------------------
-- Records of xjm_sms_code
-- ----------------------------
INSERT INTO `xjm_sms_code` VALUES ('743', '18206849645', '4016', '1511167178', '');
INSERT INTO `xjm_sms_code` VALUES ('794', '17778079115', '8004', '1511356503', '');
INSERT INTO `xjm_sms_code` VALUES ('795', '15927219779', '2038', '1511399575', '1');
INSERT INTO `xjm_sms_code` VALUES ('796', '15927219779', '2380', '1511400690', '');
INSERT INTO `xjm_sms_code` VALUES ('797', '17778079115', '8777', '1511400716', '');
INSERT INTO `xjm_sms_code` VALUES ('798', '17778079115', '1326', '1511400762', '');
INSERT INTO `xjm_sms_code` VALUES ('799', '15172370138', '2283', '1511404841', '');
INSERT INTO `xjm_sms_code` VALUES ('800', '13397140486', '9322', '1511405222', '1');
INSERT INTO `xjm_sms_code` VALUES ('801', '17320567880', '3094', '1511407283', '');
INSERT INTO `xjm_sms_code` VALUES ('802', '17320567880', '3928', '1511407310', '');
INSERT INTO `xjm_sms_code` VALUES ('803', '17320567880', '1623', '1511407321', '');
INSERT INTO `xjm_sms_code` VALUES ('804', '17320567880', '8410', '1511408601', '1');
INSERT INTO `xjm_sms_code` VALUES ('805', '18206849645', '8548', '1511410545', '');
INSERT INTO `xjm_sms_code` VALUES ('806', '17778079115', '8166', '1511410675', '');
INSERT INTO `xjm_sms_code` VALUES ('807', '18107214796', '7647', '1511410697', '');
INSERT INTO `xjm_sms_code` VALUES ('808', '17778079115', '2591', '1511411418', '');
INSERT INTO `xjm_sms_code` VALUES ('809', '17778079115', '5723', '1511411588', '');
INSERT INTO `xjm_sms_code` VALUES ('810', '18107214796', '1654', '1511411607', '');
INSERT INTO `xjm_sms_code` VALUES ('811', '17320567880', '7619', '1511415907', '');
INSERT INTO `xjm_sms_code` VALUES ('812', '15826995634', '9903', '1511418045', '');
INSERT INTO `xjm_sms_code` VALUES ('813', '18206849645', '3838', '1511418865', '1');
INSERT INTO `xjm_sms_code` VALUES ('814', '18206849645', '5122', '1511420222', '1');
INSERT INTO `xjm_sms_code` VALUES ('815', '17368584803', '2025', '1511420632', '');
INSERT INTO `xjm_sms_code` VALUES ('816', '17778079115', '5172', '1511420843', '');
INSERT INTO `xjm_sms_code` VALUES ('817', '15172370138', '9039', '1511422952', '');
INSERT INTO `xjm_sms_code` VALUES ('818', '18206849645', '5530', '1511423660', '1');
INSERT INTO `xjm_sms_code` VALUES ('819', '18206849645', '7092', '1511423819', '1');
INSERT INTO `xjm_sms_code` VALUES ('820', '18206849645', '6251', '1511424335', '1');
INSERT INTO `xjm_sms_code` VALUES ('821', '18206849645', '8816', '1511424383', '1');
INSERT INTO `xjm_sms_code` VALUES ('822', '18206849645', '4719', '1511424623', '1');
INSERT INTO `xjm_sms_code` VALUES ('823', '18206849645', '8499', '1511424731', '1');
INSERT INTO `xjm_sms_code` VALUES ('824', '18206849645', '3044', '1511424899', '1');
INSERT INTO `xjm_sms_code` VALUES ('825', '18206849645', '6199', '1511426397', '1');
INSERT INTO `xjm_sms_code` VALUES ('826', '17320567880', '4185', '1511426415', '1');
INSERT INTO `xjm_sms_code` VALUES ('827', '13397140486', '9128', '1511427353', '');
INSERT INTO `xjm_sms_code` VALUES ('828', '17778079115', '1699', '1511427690', '');
INSERT INTO `xjm_sms_code` VALUES ('829', '13397140486', '6451', '1511428800', '');
INSERT INTO `xjm_sms_code` VALUES ('830', '18206849645', '9948', '1511428916', '');
INSERT INTO `xjm_sms_code` VALUES ('831', '15071432056', '8198', '1511441712', '');
INSERT INTO `xjm_sms_code` VALUES ('832', '13397140486', '1234', '1511485463', '');
INSERT INTO `xjm_sms_code` VALUES ('833', '18206849645', '4800', '1511485922', '2');
INSERT INTO `xjm_sms_code` VALUES ('834', '13397140486', '2893', '1511487163', '');
INSERT INTO `xjm_sms_code` VALUES ('835', '18206849645', '7939', '1511487167', '');
INSERT INTO `xjm_sms_code` VALUES ('836', '15172370138', '9421', '1511487638', '');
INSERT INTO `xjm_sms_code` VALUES ('837', '15172370138', '2477', '1511504510', '');
INSERT INTO `xjm_sms_code` VALUES ('838', '15172370138', '1429', '1511504793', '');
INSERT INTO `xjm_sms_code` VALUES ('839', '18671925557', '5071', '1511510938', '');
INSERT INTO `xjm_sms_code` VALUES ('840', '18206849645', '4351', '1511514710', '1');
INSERT INTO `xjm_sms_code` VALUES ('841', '18206849645', '6850', '1511514897', '1');
INSERT INTO `xjm_sms_code` VALUES ('842', '18062621299', '8266', '1511522873', '');
INSERT INTO `xjm_sms_code` VALUES ('843', '18975661141', '7199', '1511523020', '');
INSERT INTO `xjm_sms_code` VALUES ('844', '13297912209', '8396', '1511523351', '');
INSERT INTO `xjm_sms_code` VALUES ('845', '13216133640', '1628', '1511550236', '');
INSERT INTO `xjm_sms_code` VALUES ('846', '18206849645', '4183', '1511571741', '');
INSERT INTO `xjm_sms_code` VALUES ('847', '18206849645', '5981', '1511571928', '1');
INSERT INTO `xjm_sms_code` VALUES ('848', '13397140486', '4387', '1511583927', '');
INSERT INTO `xjm_sms_code` VALUES ('849', '18971431601', '7177', '1511594085', '');
INSERT INTO `xjm_sms_code` VALUES ('850', '18206849645', '4649', '1511621533', '');
INSERT INTO `xjm_sms_code` VALUES ('851', '17137214472', '4205', '1511701116', '');
INSERT INTO `xjm_sms_code` VALUES ('852', '18971193603', '5269', '1511701786', '');
INSERT INTO `xjm_sms_code` VALUES ('853', '18971193603', '8358', '1511701848', '');
INSERT INTO `xjm_sms_code` VALUES ('854', '18507151771', '2429', '1511747356', '');
INSERT INTO `xjm_sms_code` VALUES ('855', '15826995634', '5057', '1511750461', '1');
INSERT INTO `xjm_sms_code` VALUES ('856', '18206849645', '1584', '1511752777', '1');
INSERT INTO `xjm_sms_code` VALUES ('857', '15172370138', '6975', '1511766433', '');
INSERT INTO `xjm_sms_code` VALUES ('858', '17778079115', '8655', '1511767348', '');
INSERT INTO `xjm_sms_code` VALUES ('859', '13925502374', '9445', '1511770470', '1');
INSERT INTO `xjm_sms_code` VALUES ('860', '18627099297', '1547', '1511770916', '');
INSERT INTO `xjm_sms_code` VALUES ('861', '18627099297', '9010', '1511773489', '');
INSERT INTO `xjm_sms_code` VALUES ('862', '15172370138', '5589', '1511773964', '');
INSERT INTO `xjm_sms_code` VALUES ('863', '13925542153', '4188', '1511774080', '');
INSERT INTO `xjm_sms_code` VALUES ('864', '15172370138', '2470', '1511774661', '');
INSERT INTO `xjm_sms_code` VALUES ('865', '18008655056', '5309', '1511790636', '');
INSERT INTO `xjm_sms_code` VALUES ('866', '18008655056', '2548', '1511791403', '1');
INSERT INTO `xjm_sms_code` VALUES ('867', '17137214472', '9614', '1511830618', '');
INSERT INTO `xjm_sms_code` VALUES ('868', '18507151771', '9250', '1511832782', '');
INSERT INTO `xjm_sms_code` VALUES ('869', '15007150712', '5021', '1511833500', '1');
INSERT INTO `xjm_sms_code` VALUES ('870', '15007150712', '5107', '1511833753', '1');
INSERT INTO `xjm_sms_code` VALUES ('871', '15007150712', '2078', '1511834064', '');
INSERT INTO `xjm_sms_code` VALUES ('872', '15007150712', '8578', '1511834087', '');
INSERT INTO `xjm_sms_code` VALUES ('873', '15007150712', '2286', '1511834104', '');
INSERT INTO `xjm_sms_code` VALUES ('874', '15007150712', '1335', '1511834120', '');
INSERT INTO `xjm_sms_code` VALUES ('875', '15007150712', '5041', '1511834134', '');
INSERT INTO `xjm_sms_code` VALUES ('876', '15007150712', '3793', '1511834149', '');
INSERT INTO `xjm_sms_code` VALUES ('877', '18008655056', '6538', '1511834160', '');
INSERT INTO `xjm_sms_code` VALUES ('878', '18971193603', '7555', '1511834581', '');
INSERT INTO `xjm_sms_code` VALUES ('879', '18627099297', '7535', '1511835485', '');
INSERT INTO `xjm_sms_code` VALUES ('880', '18206849645', '9338', '1511836503', '1');
INSERT INTO `xjm_sms_code` VALUES ('881', '18206849645', '2776', '1511838480', '1');
INSERT INTO `xjm_sms_code` VALUES ('882', '18206849645', '9035', '1511838747', '');
INSERT INTO `xjm_sms_code` VALUES ('883', '18206849645', '8350', '1511839421', '1');
INSERT INTO `xjm_sms_code` VALUES ('884', '13397140486', '4096', '1511840129', '');
INSERT INTO `xjm_sms_code` VALUES ('885', '18206849645', '9187', '1511840154', '1');
INSERT INTO `xjm_sms_code` VALUES ('886', '18971193603', '5971', '1511840164', '');
INSERT INTO `xjm_sms_code` VALUES ('887', '17137214472', '1509', '1511840907', '');
INSERT INTO `xjm_sms_code` VALUES ('888', '18206849645', '7497', '1511840958', '1');
INSERT INTO `xjm_sms_code` VALUES ('889', '17137214472', '7433', '1511841088', '');
INSERT INTO `xjm_sms_code` VALUES ('890', '13607128218', '2502', '1511841162', '');
INSERT INTO `xjm_sms_code` VALUES ('891', '15007150712', '3943', '1511841244', '');
INSERT INTO `xjm_sms_code` VALUES ('892', '15868198636', '2398', '1511844107', '');
INSERT INTO `xjm_sms_code` VALUES ('893', '17386077597', '3238', '1511851697', '1');
INSERT INTO `xjm_sms_code` VALUES ('894', '15826995634', '9179', '1511853269', '');
INSERT INTO `xjm_sms_code` VALUES ('895', '18971193603', '2885', '1511853612', '');
INSERT INTO `xjm_sms_code` VALUES ('896', '15172370138', '2199', '1511855728', '');
INSERT INTO `xjm_sms_code` VALUES ('897', '18971193603', '4264', '1511855819', '');
INSERT INTO `xjm_sms_code` VALUES ('898', '18971193603', '5010', '1511856800', '');
INSERT INTO `xjm_sms_code` VALUES ('899', '13597675220', '3913', '1511856930', '');
INSERT INTO `xjm_sms_code` VALUES ('900', '18971193603', '2881', '1511857426', '');
INSERT INTO `xjm_sms_code` VALUES ('901', '18971193603', '6417', '1511858662', '');
INSERT INTO `xjm_sms_code` VALUES ('902', '18971193606', '7997', '1511860114', '');
INSERT INTO `xjm_sms_code` VALUES ('903', '18971193603', '9492', '1511860140', '');
INSERT INTO `xjm_sms_code` VALUES ('904', '18971193603', '9704', '1511860152', '');
INSERT INTO `xjm_sms_code` VALUES ('905', '18971193603', '3809', '1511860167', '');
INSERT INTO `xjm_sms_code` VALUES ('906', '18971193603', '1427', '1511860181', '');
INSERT INTO `xjm_sms_code` VALUES ('907', '18971193603', '1672', '1511860254', '');
INSERT INTO `xjm_sms_code` VALUES ('908', '18971193603', '3841', '1511860266', '');
INSERT INTO `xjm_sms_code` VALUES ('909', '15172370138', '6176', '1511860291', '');
INSERT INTO `xjm_sms_code` VALUES ('910', '13035162070', '6566', '1511867882', '1');
INSERT INTO `xjm_sms_code` VALUES ('911', '18627921686', '5435', '1511869570', '');
INSERT INTO `xjm_sms_code` VALUES ('912', '15827037353', '5576', '1511870804', '');
INSERT INTO `xjm_sms_code` VALUES ('913', '15807193219', '2811', '1511871891', '');
INSERT INTO `xjm_sms_code` VALUES ('914', '15807193219', '1307', '1511871953', '');
INSERT INTO `xjm_sms_code` VALUES ('915', '15807193219', '9272', '1511872016', '');
INSERT INTO `xjm_sms_code` VALUES ('916', '15807193219', '2422', '1511872071', '');
INSERT INTO `xjm_sms_code` VALUES ('917', '15807193219', '6315', '1511872184', '');
INSERT INTO `xjm_sms_code` VALUES ('918', '15927365686', '4089', '1511874877', '');
INSERT INTO `xjm_sms_code` VALUES ('919', '13816529139', '4731', '1511880614', '');
INSERT INTO `xjm_sms_code` VALUES ('920', '18971193603', '2032', '1511918704', '');
INSERT INTO `xjm_sms_code` VALUES ('921', '18971193603', '5801', '1511918773', '');
INSERT INTO `xjm_sms_code` VALUES ('922', '18971193603', '8730', '1511918827', '');
INSERT INTO `xjm_sms_code` VALUES ('923', '18975661141', '9987', '1511918977', '');
INSERT INTO `xjm_sms_code` VALUES ('924', '15172370138', '9921', '1511919556', '');
INSERT INTO `xjm_sms_code` VALUES ('925', '18971193603', '9986', '1511925712', '');
INSERT INTO `xjm_sms_code` VALUES ('926', '18868108303', '1901', '1511928348', '');
INSERT INTO `xjm_sms_code` VALUES ('927', '18206849645', '5294', '1511937073', '');
INSERT INTO `xjm_sms_code` VALUES ('928', '18551789135', '5502', '1511939448', '1');
INSERT INTO `xjm_sms_code` VALUES ('929', '13886192822', '7465', '1511941013', '');
INSERT INTO `xjm_sms_code` VALUES ('930', '18971193603', '2430', '1511942897', '');
INSERT INTO `xjm_sms_code` VALUES ('931', '18206849645', '8253', '1511966617', '');
INSERT INTO `xjm_sms_code` VALUES ('932', '18507101617', '4472', '1511970957', '');
INSERT INTO `xjm_sms_code` VALUES ('933', '13098801456', '6239', '1512006907', '');
INSERT INTO `xjm_sms_code` VALUES ('934', '13098801456', '2147', '1512007031', '');
INSERT INTO `xjm_sms_code` VALUES ('935', '17778079115', '2012', '1512007457', '');
INSERT INTO `xjm_sms_code` VALUES ('936', '15172370138', '6499', '1512020252', '1');
INSERT INTO `xjm_sms_code` VALUES ('937', '18971193603', '8499', '1512021890', '');
INSERT INTO `xjm_sms_code` VALUES ('938', '18971431601', '8837', '1512024399', '1');
INSERT INTO `xjm_sms_code` VALUES ('939', '18107214796', '4492', '1512026861', '');
INSERT INTO `xjm_sms_code` VALUES ('940', '15172370138', '5342', '1512027950', '');
INSERT INTO `xjm_sms_code` VALUES ('941', '13098801456', '8184', '1512028701', '1');
INSERT INTO `xjm_sms_code` VALUES ('942', '13813548107', '3642', '1512031248', '');
INSERT INTO `xjm_sms_code` VALUES ('943', '15172370138', '2324', '1512033515', '');
INSERT INTO `xjm_sms_code` VALUES ('944', '15167880339', '7655', '1512047587', '');
INSERT INTO `xjm_sms_code` VALUES ('945', '18271405963', '1186', '1512093390', '1');
INSERT INTO `xjm_sms_code` VALUES ('946', '15171186572', '3232', '1512093695', '');
INSERT INTO `xjm_sms_code` VALUES ('947', '18206849645', '5937', '1512102036', '');
INSERT INTO `xjm_sms_code` VALUES ('948', '15172370138', '2068', '1512114750', '');
INSERT INTO `xjm_sms_code` VALUES ('949', '15172370138', '8720', '1512116229', '');
INSERT INTO `xjm_sms_code` VALUES ('950', '15172370138', '8097', '1512116947', '');
INSERT INTO `xjm_sms_code` VALUES ('951', '13085273699', '3410', '1512121997', '');
INSERT INTO `xjm_sms_code` VALUES ('952', '13085271699', '1657', '1512122050', '');
INSERT INTO `xjm_sms_code` VALUES ('953', '15172370138', '5839', '1512122486', '');
INSERT INTO `xjm_sms_code` VALUES ('954', '18206849645', '3710', '1512122940', '');
INSERT INTO `xjm_sms_code` VALUES ('955', '18872233699', '2456', '1512131188', '');
INSERT INTO `xjm_sms_code` VALUES ('956', '15823243624', '9527', '1512202624', '1');
INSERT INTO `xjm_sms_code` VALUES ('957', '13529306620', '9026', '1512319545', '');
INSERT INTO `xjm_sms_code` VALUES ('958', '18206849645', '8198', '1512352787', '');
INSERT INTO `xjm_sms_code` VALUES ('959', '15172370138', '7386', '1512368006', '');
INSERT INTO `xjm_sms_code` VALUES ('960', '15172370138', '6403', '1512378351', '1');
INSERT INTO `xjm_sms_code` VALUES ('961', '18206849645', '4643', '1512380054', '1');
INSERT INTO `xjm_sms_code` VALUES ('962', '18206849645', '7103', '1512382468', '1');
INSERT INTO `xjm_sms_code` VALUES ('963', '18206849645', '4187', '1512382829', '1');
INSERT INTO `xjm_sms_code` VALUES ('964', '18206849645', '2091', '1512382927', '1');
INSERT INTO `xjm_sms_code` VALUES ('965', '17778079115', '1897', '1512383445', '');
INSERT INTO `xjm_sms_code` VALUES ('966', '17778079115', '4980', '1512383694', '');
INSERT INTO `xjm_sms_code` VALUES ('967', '13607128218', '5471', '1512437730', '');
INSERT INTO `xjm_sms_code` VALUES ('968', '17137214472', '7250', '1512439798', '1');
INSERT INTO `xjm_sms_code` VALUES ('969', '17778079115', '1509', '1512441979', '');
INSERT INTO `xjm_sms_code` VALUES ('970', '18206849645', '8912', '1512445268', '');
INSERT INTO `xjm_sms_code` VALUES ('971', '17137214472', '6907', '1512445450', '');
INSERT INTO `xjm_sms_code` VALUES ('972', '15172370138', '4821', '1512460272', '2');
INSERT INTO `xjm_sms_code` VALUES ('973', '15826995634', '6951', '1512460427', '');
INSERT INTO `xjm_sms_code` VALUES ('974', '15007150712', '8731', '1512522599', '');
INSERT INTO `xjm_sms_code` VALUES ('975', '18453040836', '6884', '1512535094', '');
INSERT INTO `xjm_sms_code` VALUES ('976', '18453040836', '1167', '1512535116', '');
INSERT INTO `xjm_sms_code` VALUES ('977', '15005157933', '1309', '1512554898', '');
INSERT INTO `xjm_sms_code` VALUES ('978', '17778079115', '9600', '1512555064', '');
INSERT INTO `xjm_sms_code` VALUES ('979', '17778079115', '3781', '1512558043', '');
INSERT INTO `xjm_sms_code` VALUES ('980', '13076949806', '6946', '1512563088', '');
INSERT INTO `xjm_sms_code` VALUES ('981', '13076949806', '6424', '1512563126', '');
INSERT INTO `xjm_sms_code` VALUES ('982', '18206849645', '7109', '1512563157', '');
INSERT INTO `xjm_sms_code` VALUES ('983', '13982864067', '6252', '1512563415', '1');
INSERT INTO `xjm_sms_code` VALUES ('984', '15271819735', '5430', '1512574524', '');
INSERT INTO `xjm_sms_code` VALUES ('985', '18193863911', '5236', '1512582542', '');
INSERT INTO `xjm_sms_code` VALUES ('986', '18200634347', '7680', '1512613168', '1');
INSERT INTO `xjm_sms_code` VALUES ('987', '18986217203', '2934', '1512636735', '');
INSERT INTO `xjm_sms_code` VALUES ('988', '18986217203', '7149', '1512636772', '');
INSERT INTO `xjm_sms_code` VALUES ('989', '18680434262', '8956', '1512636923', '1');
INSERT INTO `xjm_sms_code` VALUES ('990', '15094314768', '4576', '1512642835', '1');
INSERT INTO `xjm_sms_code` VALUES ('991', '13901708247', '9310', '1512705164', '');
INSERT INTO `xjm_sms_code` VALUES ('992', '18971193603', '1324', '1512708759', '');
INSERT INTO `xjm_sms_code` VALUES ('993', '18971193603', '3755', '1512708844', '');
INSERT INTO `xjm_sms_code` VALUES ('994', '13507136556', '6076', '1512730630', '');
INSERT INTO `xjm_sms_code` VALUES ('995', '15927219779', '8724', '1512730673', '');
INSERT INTO `xjm_sms_code` VALUES ('996', '15927365686', '8083', '1512737395', '');
INSERT INTO `xjm_sms_code` VALUES ('997', '15927219779', '6642', '1512742584', '');
INSERT INTO `xjm_sms_code` VALUES ('998', '13035110308', '6438', '1512742789', '');
INSERT INTO `xjm_sms_code` VALUES ('999', '17778079145', '8812', '1512743379', '');
INSERT INTO `xjm_sms_code` VALUES ('1000', '18971193603', '4947', '1512743394', '');
INSERT INTO `xjm_sms_code` VALUES ('1001', '18971193603', '9829', '1512743592', '');
INSERT INTO `xjm_sms_code` VALUES ('1002', '18627099297', '4303', '1512745262', '');
INSERT INTO `xjm_sms_code` VALUES ('1003', '18738472029', '5966', '1512799846', '1');
INSERT INTO `xjm_sms_code` VALUES ('1004', '18507151771', '8268', '1512872298', '');
INSERT INTO `xjm_sms_code` VALUES ('1005', '18507151771', '6367', '1512873483', '');
INSERT INTO `xjm_sms_code` VALUES ('1006', '13380347170', '8853', '1512890868', '');
INSERT INTO `xjm_sms_code` VALUES ('1007', '18972610481', '1385', '1512891074', '');
INSERT INTO `xjm_sms_code` VALUES ('1008', '18370294187', '3851', '1512899032', '1');
INSERT INTO `xjm_sms_code` VALUES ('1009', '18193147368', '9773', '1512923396', '');
INSERT INTO `xjm_sms_code` VALUES ('1010', '15011347577', '3247', '1512930042', '1');
INSERT INTO `xjm_sms_code` VALUES ('1011', '15640284473', '8892', '1512974579', '1');
INSERT INTO `xjm_sms_code` VALUES ('1012', '15640284473', '3580', '1512974766', '1');
INSERT INTO `xjm_sms_code` VALUES ('1013', '18971193603', '2571', '1513042444', '1');
INSERT INTO `xjm_sms_code` VALUES ('1014', '15195688909', '1885', '1513086639', '1');
INSERT INTO `xjm_sms_code` VALUES ('1015', '18976997301', '1321', '1513110792', '1');
INSERT INTO `xjm_sms_code` VALUES ('1016', '17601550741', '6900', '1513237248', '');
INSERT INTO `xjm_sms_code` VALUES ('1017', '18971193603', '3658', '1513303850', '');
INSERT INTO `xjm_sms_code` VALUES ('1018', '13296521863', '5597', '1513429393', '');
INSERT INTO `xjm_sms_code` VALUES ('1019', '18162519010', '3315', '1513453337', '1');
INSERT INTO `xjm_sms_code` VALUES ('1020', '15927365686', '9612', '1513483324', '');
INSERT INTO `xjm_sms_code` VALUES ('1021', '15826995634', '5306', '1513487260', '');
INSERT INTO `xjm_sms_code` VALUES ('1022', '15034518927', '4584', '1513490226', '1');
INSERT INTO `xjm_sms_code` VALUES ('1023', '18672778800', '3900', '1513490403', '');
INSERT INTO `xjm_sms_code` VALUES ('1024', '17714571801', '3237', '1513591875', '');
INSERT INTO `xjm_sms_code` VALUES ('1025', '15982355804', '7099', '1513612037', '1');
INSERT INTO `xjm_sms_code` VALUES ('1026', '15982355804', '6974', '1513612843', '1');
INSERT INTO `xjm_sms_code` VALUES ('1027', '18505571981', '1322', '1513619478', '');
INSERT INTO `xjm_sms_code` VALUES ('1028', '13970153505', '2698', '1513649285', '');
INSERT INTO `xjm_sms_code` VALUES ('1029', '18627031757', '5225', '1513663225', '');
INSERT INTO `xjm_sms_code` VALUES ('1030', '18419952514', '9670', '1513753129', '1');
INSERT INTO `xjm_sms_code` VALUES ('1031', '15822355865', '2390', '1513783662', '');
INSERT INTO `xjm_sms_code` VALUES ('1032', '13631294998', '9894', '1513784276', '');
INSERT INTO `xjm_sms_code` VALUES ('1033', '15210035193', '8285', '1514041727', '1');
INSERT INTO `xjm_sms_code` VALUES ('1034', '15210035193', '7863', '1514041802', '1');
INSERT INTO `xjm_sms_code` VALUES ('1035', '15164380556', '8601', '1514102081', '1');
INSERT INTO `xjm_sms_code` VALUES ('1036', '13197984282', '1821', '1514108073', '');
INSERT INTO `xjm_sms_code` VALUES ('1037', '15172370138', '4072', '1514165000', '');
INSERT INTO `xjm_sms_code` VALUES ('1038', '13430864796', '9405', '1514193148', '');
INSERT INTO `xjm_sms_code` VALUES ('1039', '18632292112', '3241', '1514204850', '');
INSERT INTO `xjm_sms_code` VALUES ('1040', '13507136556', '9995', '1514211914', '');
INSERT INTO `xjm_sms_code` VALUES ('1041', '15172370138', '7945', '1514253298', '');
INSERT INTO `xjm_sms_code` VALUES ('1042', '13425527650', '9976', '1514273784', '1');
INSERT INTO `xjm_sms_code` VALUES ('1043', '18627921686', '7615', '1514313962', '');
INSERT INTO `xjm_sms_code` VALUES ('1044', '18603957292', '2978', '1514355126', '');
INSERT INTO `xjm_sms_code` VALUES ('1045', '15085373955', '3632', '1514356212', '');
INSERT INTO `xjm_sms_code` VALUES ('1046', '18601959642', '2967', '1514356606', '');
INSERT INTO `xjm_sms_code` VALUES ('1047', '15506802101', '7174', '1514356654', '');
INSERT INTO `xjm_sms_code` VALUES ('1048', '15005373955', '1491', '1514357558', '');
INSERT INTO `xjm_sms_code` VALUES ('1049', '15005373955', '8626', '1514357583', '');
INSERT INTO `xjm_sms_code` VALUES ('1050', '15814012380', '2134', '1514360065', '');
INSERT INTO `xjm_sms_code` VALUES ('1051', '15814012380', '9917', '1514360102', '');
INSERT INTO `xjm_sms_code` VALUES ('1052', '13507136556', '3251', '1514370624', '1');
INSERT INTO `xjm_sms_code` VALUES ('1053', '15899816747', '7050', '1514375058', '');
INSERT INTO `xjm_sms_code` VALUES ('1054', '18603287673', '7233', '1514441236', '');
INSERT INTO `xjm_sms_code` VALUES ('1055', '15172370138', '7637', '1514441388', '');
INSERT INTO `xjm_sms_code` VALUES ('1056', '15957858818', '1352', '1514455232', '');
INSERT INTO `xjm_sms_code` VALUES ('1057', '13050922258', '6030', '1514550325', '');
INSERT INTO `xjm_sms_code` VALUES ('1058', '18778439839', '4675', '1514640910', '1');
INSERT INTO `xjm_sms_code` VALUES ('1059', '18030608882', '7623', '1514646593', '');
INSERT INTO `xjm_sms_code` VALUES ('1060', '18900856797', '5387', '1514668692', '1');
INSERT INTO `xjm_sms_code` VALUES ('1061', '15251827456', '1430', '1514723559', '');
INSERT INTO `xjm_sms_code` VALUES ('1062', '13554307586', '8131', '1514747066', '');
INSERT INTO `xjm_sms_code` VALUES ('1063', '15626112352', '1055', '1514766213', '');
INSERT INTO `xjm_sms_code` VALUES ('1064', '18008655056', '3937', '1514860446', '');
INSERT INTO `xjm_sms_code` VALUES ('1065', '15927365686', '9384', '1514867332', '');
INSERT INTO `xjm_sms_code` VALUES ('1066', '18687870895', '2140', '1514916545', '1');
INSERT INTO `xjm_sms_code` VALUES ('1067', '17600699450', '1793', '1514943420', '');
INSERT INTO `xjm_sms_code` VALUES ('1068', '15178272165', '1081', '1515021561', '2');
INSERT INTO `xjm_sms_code` VALUES ('1069', '13135825001', '7629', '1515073431', '');
INSERT INTO `xjm_sms_code` VALUES ('1070', '18321503886', '4469', '1515077175', '');
INSERT INTO `xjm_sms_code` VALUES ('1071', '17605210525', '1248', '1515149302', '');
INSERT INTO `xjm_sms_code` VALUES ('1072', '18900856797', '5871', '1515188245', '1');
INSERT INTO `xjm_sms_code` VALUES ('1073', '18779817793', '2077', '1515237652', '');
INSERT INTO `xjm_sms_code` VALUES ('1074', '18168578527', '2935', '1515263746', '');
INSERT INTO `xjm_sms_code` VALUES ('1075', '15952675090', '1612', '1515566516', '');
INSERT INTO `xjm_sms_code` VALUES ('1076', '18896218718', '6504', '1515586540', '2');
INSERT INTO `xjm_sms_code` VALUES ('1077', '13511028729', '4695', '1515656864', '1');
INSERT INTO `xjm_sms_code` VALUES ('1078', '13971666324', '6561', '1515765781', '');
INSERT INTO `xjm_sms_code` VALUES ('1079', '13702122632', '2782', '1515850321', '');
INSERT INTO `xjm_sms_code` VALUES ('1080', '13702122632', '5733', '1515850351', '');
INSERT INTO `xjm_sms_code` VALUES ('1081', '13001733030', '8211', '1515866025', '');
INSERT INTO `xjm_sms_code` VALUES ('1082', '13088957366', '4110', '1515900324', '1');
INSERT INTO `xjm_sms_code` VALUES ('1083', '15172370138', '1340', '1515978000', '');
INSERT INTO `xjm_sms_code` VALUES ('1084', '15204002225', '2608', '1516069983', '1');
INSERT INTO `xjm_sms_code` VALUES ('1085', '18111946003', '3185', '1516088494', '1');
INSERT INTO `xjm_sms_code` VALUES ('1086', '18500399977', '1278', '1516201218', '');
INSERT INTO `xjm_sms_code` VALUES ('1087', '18206849645', '8470', '1516261104', '');
INSERT INTO `xjm_sms_code` VALUES ('1088', '18206849645', '2073', '1516265227', '1');
INSERT INTO `xjm_sms_code` VALUES ('1089', '18618416795', '5475', '1516269541', '');
INSERT INTO `xjm_sms_code` VALUES ('1090', '18684349486', '5745', '1516331970', '');
INSERT INTO `xjm_sms_code` VALUES ('1091', '18684379786', '3939', '1516332039', '');
INSERT INTO `xjm_sms_code` VALUES ('1092', '18551780142', '8179', '1516333355', '');
INSERT INTO `xjm_sms_code` VALUES ('1093', '15882145370', '5176', '1516343515', '1');
INSERT INTO `xjm_sms_code` VALUES ('1094', '18206849645', '3449', '1516360736', '1');
INSERT INTO `xjm_sms_code` VALUES ('1095', '15916656447', '5317', '1516380122', '1');
INSERT INTO `xjm_sms_code` VALUES ('1096', '17778079115', '9747', '1516405714', '');
INSERT INTO `xjm_sms_code` VALUES ('1097', '18900856797', '7997', '1516416772', '1');
INSERT INTO `xjm_sms_code` VALUES ('1098', '18900856797', '8493', '1516416849', '2');
INSERT INTO `xjm_sms_code` VALUES ('1099', '18991634357', '7737', '1516505134', '');
INSERT INTO `xjm_sms_code` VALUES ('1100', '15204002225', '7873', '1516588574', '');
INSERT INTO `xjm_sms_code` VALUES ('1101', '15223337005', '5510', '1516624314', '1');
INSERT INTO `xjm_sms_code` VALUES ('1102', '15221117005', '6901', '1516624378', '1');
INSERT INTO `xjm_sms_code` VALUES ('1103', '18516978984', '7209', '1516676703', '1');
INSERT INTO `xjm_sms_code` VALUES ('1104', '18607292768', '3001', '1516704904', '');
INSERT INTO `xjm_sms_code` VALUES ('1105', '15138829654', '3019', '1516706933', '1');
INSERT INTO `xjm_sms_code` VALUES ('1106', '18054199672', '4308', '1516948930', '1');
INSERT INTO `xjm_sms_code` VALUES ('1107', '13305215399', '5659', '1516957628', '1');
INSERT INTO `xjm_sms_code` VALUES ('1108', '15533057813', '8638', '1516981927', '1');
INSERT INTO `xjm_sms_code` VALUES ('1109', '17762826373', '5241', '1517015867', '');
INSERT INTO `xjm_sms_code` VALUES ('1110', '13693448478', '1428', '1517055761', '1');
INSERT INTO `xjm_sms_code` VALUES ('1111', '13596146160', '7443', '1517059857', '1');
INSERT INTO `xjm_sms_code` VALUES ('1112', '18166993953', '6416', '1517108487', '1');
INSERT INTO `xjm_sms_code` VALUES ('1113', '18971431601', '1076', '1517281528', '');
INSERT INTO `xjm_sms_code` VALUES ('1114', '15910684836', '7217', '1517319228', '1');
INSERT INTO `xjm_sms_code` VALUES ('1115', '15910684836', '2191', '1517319302', '1');
INSERT INTO `xjm_sms_code` VALUES ('1116', '13453180000', '7341', '1517667944', '');
INSERT INTO `xjm_sms_code` VALUES ('1117', '17770007860', '7112', '1517672196', '1');
INSERT INTO `xjm_sms_code` VALUES ('1118', '17770008608', '7471', '1517672289', '1');
INSERT INTO `xjm_sms_code` VALUES ('1119', '15070882893', '8819', '1518297195', '1');
INSERT INTO `xjm_sms_code` VALUES ('1120', '15271819738', '6671', '1518336398', '');
INSERT INTO `xjm_sms_code` VALUES ('1121', '15271819735', '8593', '1518336470', '');
INSERT INTO `xjm_sms_code` VALUES ('1122', '15916656447', '9782', '1518369882', '1');
INSERT INTO `xjm_sms_code` VALUES ('1123', '18166993953', '8774', '1518436843', '');
INSERT INTO `xjm_sms_code` VALUES ('1124', '13918215518', '3247', '1518849514', '');
INSERT INTO `xjm_sms_code` VALUES ('1125', '13717715806', '8644', '1518932272', '1');
INSERT INTO `xjm_sms_code` VALUES ('1126', '13717715806', '9834', '1518932407', '1');
INSERT INTO `xjm_sms_code` VALUES ('1127', '15719646392', '5584', '1519116571', '');
INSERT INTO `xjm_sms_code` VALUES ('1128', '13297182539', '2336', '1519310396', '');
INSERT INTO `xjm_sms_code` VALUES ('1129', '18728105694', '2293', '1519348853', '');
INSERT INTO `xjm_sms_code` VALUES ('1130', '17340363858', '8403', '1519425889', '');
INSERT INTO `xjm_sms_code` VALUES ('1131', '18612121312', '4641', '1519579601', '1');
INSERT INTO `xjm_sms_code` VALUES ('1132', '15137768608', '1120', '1519709808', '1');
INSERT INTO `xjm_sms_code` VALUES ('1133', '18614020570', '3910', '1519719643', '');
INSERT INTO `xjm_sms_code` VALUES ('1134', '18971193603', '5597', '1519724554', '');
INSERT INTO `xjm_sms_code` VALUES ('1135', '18942940350', '5820', '1519753085', '');
INSERT INTO `xjm_sms_code` VALUES ('1136', '18201115073', '2052', '1519904832', '');
INSERT INTO `xjm_sms_code` VALUES ('1137', '15927492341', '2146', '1520277727', '1');
INSERT INTO `xjm_sms_code` VALUES ('1138', '18169114731', '4237', '1520346566', '');
INSERT INTO `xjm_sms_code` VALUES ('1139', '18353666913', '3819', '1520477371', '1');
INSERT INTO `xjm_sms_code` VALUES ('1140', '13214378903', '7354', '1520596652', '');
INSERT INTO `xjm_sms_code` VALUES ('1141', '15315398669', '4691', '1520609811', '');
INSERT INTO `xjm_sms_code` VALUES ('1142', '17778411057', '4681', '1520692491', '1');
INSERT INTO `xjm_sms_code` VALUES ('1143', '17778411057', '9166', '1520692756', '');
INSERT INTO `xjm_sms_code` VALUES ('1144', '18972610481', '1347', '1520932028', '');
INSERT INTO `xjm_sms_code` VALUES ('1145', '13297997898', '2553', '1520953795', '');
INSERT INTO `xjm_sms_code` VALUES ('1146', '18500835172', '9199', '1521034862', '1');
INSERT INTO `xjm_sms_code` VALUES ('1147', '15619068108', '3840', '1521200105', '1');
INSERT INTO `xjm_sms_code` VALUES ('1148', '15619068108', '4334', '1521262516', '');
INSERT INTO `xjm_sms_code` VALUES ('1149', '15926300132', '1546', '1521272888', '');
INSERT INTO `xjm_sms_code` VALUES ('1150', '15926300132', '3983', '1521272954', '');
INSERT INTO `xjm_sms_code` VALUES ('1151', '15927365686', '7812', '1521297223', '');
INSERT INTO `xjm_sms_code` VALUES ('1152', '18017279373', '3432', '1521324251', '1');
INSERT INTO `xjm_sms_code` VALUES ('1153', '18017279373', '4221', '1521349305', '1');
INSERT INTO `xjm_sms_code` VALUES ('1154', '18017279373', '8895', '1521349406', '2');
INSERT INTO `xjm_sms_code` VALUES ('1155', '13148126001', '2160', '1521350824', '1');
INSERT INTO `xjm_sms_code` VALUES ('1156', '17560381988', '3236', '1521479921', '1');
INSERT INTO `xjm_sms_code` VALUES ('1157', '13598479564', '3324', '1521706821', '1');
INSERT INTO `xjm_sms_code` VALUES ('1158', '13598479564', '3126', '1521706941', '1');
INSERT INTO `xjm_sms_code` VALUES ('1159', '17081279850', '5955', '1521710643', '1');
INSERT INTO `xjm_sms_code` VALUES ('1160', '13651505567', '3979', '1522042378', '');
INSERT INTO `xjm_sms_code` VALUES ('1161', '18591237096', '7062', '1522046659', '');
INSERT INTO `xjm_sms_code` VALUES ('1162', '13215689262', '3870', '1522087348', '1');
INSERT INTO `xjm_sms_code` VALUES ('1163', '17710594736', '6980', '1522496703', '1');
INSERT INTO `xjm_sms_code` VALUES ('1164', '17710594736', '6948', '1522646198', '');
INSERT INTO `xjm_sms_code` VALUES ('1165', '18344120948', '2626', '1522682710', '1');
INSERT INTO `xjm_sms_code` VALUES ('1166', '13588727593', '2661', '1522684195', '');
INSERT INTO `xjm_sms_code` VALUES ('1167', '18501996874', '3115', '1522810012', '1');
INSERT INTO `xjm_sms_code` VALUES ('1168', '18514709015', '7627', '1522857880', '');
INSERT INTO `xjm_sms_code` VALUES ('1169', '18514709015', '3654', '1522858133', '');
INSERT INTO `xjm_sms_code` VALUES ('1170', '18514709018', '4599', '1522858154', '');
INSERT INTO `xjm_sms_code` VALUES ('1171', '15927247723', '9144', '1522991472', '');
INSERT INTO `xjm_sms_code` VALUES ('1172', '15927247723', '8943', '1522991506', '');
INSERT INTO `xjm_sms_code` VALUES ('1173', '17345831511', '9614', '1523101191', '1');
INSERT INTO `xjm_sms_code` VALUES ('1174', '17345831511', '8763', '1523101554', '1');
INSERT INTO `xjm_sms_code` VALUES ('1175', '13820367571', '1478', '1523159213', '');
INSERT INTO `xjm_sms_code` VALUES ('1176', '13693448475', '7605', '1523181766', '1');
INSERT INTO `xjm_sms_code` VALUES ('1177', '15327168878', '4371', '1523328349', '');
INSERT INTO `xjm_sms_code` VALUES ('1178', '17610291916', '5487', '1523380657', '1');
INSERT INTO `xjm_sms_code` VALUES ('1179', '17633601268', '7593', '1523617991', '1');
INSERT INTO `xjm_sms_code` VALUES ('1180', '15800835261', '5992', '1523741611', '');
INSERT INTO `xjm_sms_code` VALUES ('1181', '15826995634', '8379', '1523963446', '');
INSERT INTO `xjm_sms_code` VALUES ('1182', '18537797996', '7996', '1523977674', '1');
INSERT INTO `xjm_sms_code` VALUES ('1183', '15861403400', '9368', '1524040787', '');
INSERT INTO `xjm_sms_code` VALUES ('1184', '13488776904', '7692', '1524124832', '1');
INSERT INTO `xjm_sms_code` VALUES ('1185', '17633601268', '7901', '1524136933', '1');
INSERT INTO `xjm_sms_code` VALUES ('1186', '13552778312', '3474', '1524290843', '');
INSERT INTO `xjm_sms_code` VALUES ('1187', '15530463210', '6782', '1524477165', '');
INSERT INTO `xjm_sms_code` VALUES ('1188', '15901985152', '3309', '1524478693', '1');
INSERT INTO `xjm_sms_code` VALUES ('1189', '18801360325', '8790', '1524568248', '');
INSERT INTO `xjm_sms_code` VALUES ('1190', '15974645018', '2308', '1524784299', '1');
INSERT INTO `xjm_sms_code` VALUES ('1191', '18040251876', '8559', '1524927491', '1');
INSERT INTO `xjm_sms_code` VALUES ('1192', '15550309955', '2693', '1525099486', '');
INSERT INTO `xjm_sms_code` VALUES ('1193', '18952940971', '6839', '1525116641', '');
INSERT INTO `xjm_sms_code` VALUES ('1194', '15604991125', '1312', '1525228808', '');
INSERT INTO `xjm_sms_code` VALUES ('1195', '18696160056', '9916', '1525340880', '1');
INSERT INTO `xjm_sms_code` VALUES ('1196', '15250960125', '1750', '1525449479', '1');
INSERT INTO `xjm_sms_code` VALUES ('1197', '15250960125', '5077', '1525449915', '');
INSERT INTO `xjm_sms_code` VALUES ('1198', '13650792069', '6151', '1525545614', '1');
INSERT INTO `xjm_sms_code` VALUES ('1199', '13650792069', '5896', '1525545854', '1');
INSERT INTO `xjm_sms_code` VALUES ('1200', '15217615740', '7105', '1525618208', '');
INSERT INTO `xjm_sms_code` VALUES ('1201', '15217615740', '7899', '1525618555', '1');
INSERT INTO `xjm_sms_code` VALUES ('1202', '17633601268', '3089', '1525880364', '1');
INSERT INTO `xjm_sms_code` VALUES ('1203', '17633601268', '9767', '1525881237', '');
INSERT INTO `xjm_sms_code` VALUES ('1204', '13428860814', '6382', '1525962516', '');
INSERT INTO `xjm_sms_code` VALUES ('1205', '15176729923', '6696', '1526024888', '1');
INSERT INTO `xjm_sms_code` VALUES ('1206', '18221953470', '1981', '1526042242', '');
INSERT INTO `xjm_sms_code` VALUES ('1207', '18310794485', '8554', '1526142992', '1');
INSERT INTO `xjm_sms_code` VALUES ('1208', '18971193603', '2524', '1526262055', '');
INSERT INTO `xjm_sms_code` VALUES ('1209', '13693448475', '3067', '1526294759', '1');
INSERT INTO `xjm_sms_code` VALUES ('1210', '18058403671', '4447', '1526596611', '');
INSERT INTO `xjm_sms_code` VALUES ('1211', '18058402671', '4876', '1526596744', '');
INSERT INTO `xjm_sms_code` VALUES ('1212', '13126686833', '1936', '1526643730', '1');
INSERT INTO `xjm_sms_code` VALUES ('1213', '15925779135', '3151', '1527052408', '');
INSERT INTO `xjm_sms_code` VALUES ('1214', '15519921015', '6212', '1527345932', '');
INSERT INTO `xjm_sms_code` VALUES ('1215', '15298764221', '8921', '1527603013', '1');
INSERT INTO `xjm_sms_code` VALUES ('1216', '13311084766', '2521', '1527896889', '1');
INSERT INTO `xjm_sms_code` VALUES ('1217', '18627762790', '7180', '1528051836', '');
INSERT INTO `xjm_sms_code` VALUES ('1218', '15057763774', '1003', '1528059158', '1');
INSERT INTO `xjm_sms_code` VALUES ('1219', '15293485208', '3836', '1528356965', '1');
INSERT INTO `xjm_sms_code` VALUES ('1220', '15217228850', '9019', '1528467492', '');
INSERT INTO `xjm_sms_code` VALUES ('1221', '15923008686', '5941', '1528595255', '1');
INSERT INTO `xjm_sms_code` VALUES ('1222', '15923008686', '2413', '1528603019', '1');
INSERT INTO `xjm_sms_code` VALUES ('1223', '13852252930', '9581', '1528608725', '1');
INSERT INTO `xjm_sms_code` VALUES ('1224', '13852252930', '5190', '1528609459', '');
INSERT INTO `xjm_sms_code` VALUES ('1225', '13588354185', '3459', '1528679970', '1');
INSERT INTO `xjm_sms_code` VALUES ('1226', '18971193603', '3644', '1528769964', '');
INSERT INTO `xjm_sms_code` VALUES ('1227', '13076949806', '2960', '1528780389', '1');
INSERT INTO `xjm_sms_code` VALUES ('1228', '13076949806', '9851', '1528780678', '1');
INSERT INTO `xjm_sms_code` VALUES ('1229', '18679418139', '3677', '1528955471', '1');
INSERT INTO `xjm_sms_code` VALUES ('1230', '18790377970', '8972', '1528988029', '');
INSERT INTO `xjm_sms_code` VALUES ('1231', '15189732555', '1968', '1529137777', '1');
INSERT INTO `xjm_sms_code` VALUES ('1232', '13415533109', '8950', '1529193561', '1');
INSERT INTO `xjm_sms_code` VALUES ('1233', '13723060989', '6507', '1529213643', '');
INSERT INTO `xjm_sms_code` VALUES ('1234', '13723060989', '7892', '1529213761', '2');
INSERT INTO `xjm_sms_code` VALUES ('1235', '18795912447', '7899', '1529342304', '');
INSERT INTO `xjm_sms_code` VALUES ('1236', '15923884498', '8059', '1529360383', '1');
INSERT INTO `xjm_sms_code` VALUES ('1237', '18730571391', '4792', '1529369185', '');
INSERT INTO `xjm_sms_code` VALUES ('1238', '13720153711', '1582', '1529478957', '');
INSERT INTO `xjm_sms_code` VALUES ('1239', '13552292105', '7695', '1529507777', '1');
INSERT INTO `xjm_sms_code` VALUES ('1240', '17601620613', '8981', '1529654225', '');
INSERT INTO `xjm_sms_code` VALUES ('1241', '15927458686', '4080', '1529742288', '');
INSERT INTO `xjm_sms_code` VALUES ('1242', '15927458686', '3868', '1529742313', '');
INSERT INTO `xjm_sms_code` VALUES ('1243', '15393150831', '3188', '1529772848', '');
INSERT INTO `xjm_sms_code` VALUES ('1244', '18993020831', '8782', '1529772892', '');
INSERT INTO `xjm_sms_code` VALUES ('1245', '15522431396', '4844', '1529910881', '');
INSERT INTO `xjm_sms_code` VALUES ('1246', '15036032523', '2319', '1529953916', '1');
INSERT INTO `xjm_sms_code` VALUES ('1247', '13783535772', '9954', '1530123783', '');
INSERT INTO `xjm_sms_code` VALUES ('1248', '18116543307', '8656', '1530130668', '1');
INSERT INTO `xjm_sms_code` VALUES ('1249', '13790496403', '9100', '1530183061', '');
INSERT INTO `xjm_sms_code` VALUES ('1250', '17310329326', '3770', '1530183922', '1');
INSERT INTO `xjm_sms_code` VALUES ('1251', '17691267521', '5099', '1530510478', '');
INSERT INTO `xjm_sms_code` VALUES ('1252', '13040808584', '7653', '1530839523', '1');
INSERT INTO `xjm_sms_code` VALUES ('1253', '15923008686', '9626', '1530844119', '1');
INSERT INTO `xjm_sms_code` VALUES ('1254', '18983928036', '5628', '1530844562', '1');
INSERT INTO `xjm_sms_code` VALUES ('1255', '18098740178', '8225', '1530956222', '1');
INSERT INTO `xjm_sms_code` VALUES ('1256', '15760325044', '2526', '1531072558', '1');
INSERT INTO `xjm_sms_code` VALUES ('1257', '18088379513', '7551', '1531189912', '');
INSERT INTO `xjm_sms_code` VALUES ('1258', '17743421151', '9649', '1531442315', '');
INSERT INTO `xjm_sms_code` VALUES ('1259', '15618167171', '2124', '1531712337', '');
INSERT INTO `xjm_sms_code` VALUES ('1260', '15297962716', '9240', '1531779927', '1');
INSERT INTO `xjm_sms_code` VALUES ('1261', '13111529151', '4165', '1531817834', '');
INSERT INTO `xjm_sms_code` VALUES ('1262', '13101653075', '2392', '1531850175', '1');
INSERT INTO `xjm_sms_code` VALUES ('1263', '17628590059', '4806', '1532079064', '');
INSERT INTO `xjm_sms_code` VALUES ('1264', '17310329326', '1293', '1532094456', '');
INSERT INTO `xjm_sms_code` VALUES ('1265', '18653067990', '9912', '1532102054', '1');
INSERT INTO `xjm_sms_code` VALUES ('1266', '13581007197', '8124', '1532153302', '1');
INSERT INTO `xjm_sms_code` VALUES ('1267', '15110210832', '8762', '1532193833', '1');
INSERT INTO `xjm_sms_code` VALUES ('1268', '16675333383', '2121', '1532267339', '');
INSERT INTO `xjm_sms_code` VALUES ('1269', '18670708115', '9910', '1532280653', '');
INSERT INTO `xjm_sms_code` VALUES ('1270', '13925962501', '3617', '1532334556', '1');
INSERT INTO `xjm_sms_code` VALUES ('1271', '13925962501', '8368', '1532334656', '1');
INSERT INTO `xjm_sms_code` VALUES ('1272', '18038125858', '5804', '1532425769', '');
INSERT INTO `xjm_sms_code` VALUES ('1273', '15220158589', '7138', '1532500087', '');
INSERT INTO `xjm_sms_code` VALUES ('1274', '15220158589', '1391', '1532500296', '');
INSERT INTO `xjm_sms_code` VALUES ('1275', '13315233085', '5055', '1532679194', '');
INSERT INTO `xjm_sms_code` VALUES ('1276', '17612011000', '3510', '1532803616', '');
INSERT INTO `xjm_sms_code` VALUES ('1277', '15001019867', '1732', '1532864254', '');
INSERT INTO `xjm_sms_code` VALUES ('1278', '18817831889', '5605', '1533034240', '');
INSERT INTO `xjm_sms_code` VALUES ('1279', '13852934401', '2588', '1533235806', '1');
INSERT INTO `xjm_sms_code` VALUES ('1280', '13852934401', '1693', '1533235877', '1');
INSERT INTO `xjm_sms_code` VALUES ('1281', '17671605956', '2131', '1533336906', '');
INSERT INTO `xjm_sms_code` VALUES ('1282', '15811290671', '2809', '1533342615', '');
INSERT INTO `xjm_sms_code` VALUES ('1283', '13641111112', '8962', '1533448900', '');
INSERT INTO `xjm_sms_code` VALUES ('1284', '13821955071', '6285', '1533459510', '');
INSERT INTO `xjm_sms_code` VALUES ('1285', '15040916373', '6985', '1533666855', '');
INSERT INTO `xjm_sms_code` VALUES ('1286', '13901359036', '7472', '1533715667', '');
INSERT INTO `xjm_sms_code` VALUES ('1287', '15950553468', '3353', '1533744096', '');
INSERT INTO `xjm_sms_code` VALUES ('1288', '13130732678', '2868', '1533745965', '1');
INSERT INTO `xjm_sms_code` VALUES ('1289', '15816879037', '8026', '1533901758', '');
INSERT INTO `xjm_sms_code` VALUES ('1290', '15383216919', '4372', '1533911049', '1');
INSERT INTO `xjm_sms_code` VALUES ('1291', '13525011012', '7701', '1533976926', '1');
INSERT INTO `xjm_sms_code` VALUES ('1292', '13338110086', '2468', '1533985823', '');
INSERT INTO `xjm_sms_code` VALUES ('1293', '13128707725', '4775', '1534053701', '');
INSERT INTO `xjm_sms_code` VALUES ('1294', '17803812105', '7031', '1534067505', '');
INSERT INTO `xjm_sms_code` VALUES ('1295', '13304019099', '5649', '1534069827', '');
INSERT INTO `xjm_sms_code` VALUES ('1296', '18271515371', '9004', '1534166902', '');
INSERT INTO `xjm_sms_code` VALUES ('1297', '13277279678', '8287', '1534219605', '');
INSERT INTO `xjm_sms_code` VALUES ('1298', '15119008725', '9534', '1534245514', '');
INSERT INTO `xjm_sms_code` VALUES ('1299', '15876370197', '8386', '1534252426', '');
INSERT INTO `xjm_sms_code` VALUES ('1300', '13612805535', '3555', '1534259238', '');
INSERT INTO `xjm_sms_code` VALUES ('1301', '13844549356', '3212', '1534318844', '');
INSERT INTO `xjm_sms_code` VALUES ('1302', '13767783865', '6455', '1534431372', '');
INSERT INTO `xjm_sms_code` VALUES ('1303', '18201159304', '6229', '1534832483', '');
INSERT INTO `xjm_sms_code` VALUES ('1304', '18602740508', '8248', '1535031673', '');
INSERT INTO `xjm_sms_code` VALUES ('1305', '13817442961', '3759', '1535367328', '');
INSERT INTO `xjm_sms_code` VALUES ('1306', '15594515587', '4430', '1535391996', '1');
INSERT INTO `xjm_sms_code` VALUES ('1307', '15594515587', '3205', '1535392273', '1');
INSERT INTO `xjm_sms_code` VALUES ('1308', '15201329720', '6106', '1535481676', '');
INSERT INTO `xjm_sms_code` VALUES ('1309', '18606297924', '9155', '1535577969', '1');
INSERT INTO `xjm_sms_code` VALUES ('1310', '18606297924', '6010', '1535578270', '1');
INSERT INTO `xjm_sms_code` VALUES ('1311', '14781738500', '1984', '1535725897', '1');
INSERT INTO `xjm_sms_code` VALUES ('1312', '15875580960', '8416', '1535870379', '');
INSERT INTO `xjm_sms_code` VALUES ('1313', '18594040513', '8882', '1535902517', '');
INSERT INTO `xjm_sms_code` VALUES ('1314', '15135792695', '9160', '1535976350', '1');
INSERT INTO `xjm_sms_code` VALUES ('1315', '15135792695', '5466', '1535977447', '1');
INSERT INTO `xjm_sms_code` VALUES ('1316', '13641452026', '5221', '1536046934', '1');
INSERT INTO `xjm_sms_code` VALUES ('1317', '17688677379', '5497', '1536077727', '1');
INSERT INTO `xjm_sms_code` VALUES ('1318', '15135792695', '3256', '1536116661', '1');
INSERT INTO `xjm_sms_code` VALUES ('1319', '13863869881', '9202', '1536218924', '1');
INSERT INTO `xjm_sms_code` VALUES ('1320', '15509851000', '6452', '1536276489', '');
INSERT INTO `xjm_sms_code` VALUES ('1321', '15219094938', '8535', '1536292306', '1');
INSERT INTO `xjm_sms_code` VALUES ('1322', '15161874400', '4687', '1536306354', '1');
INSERT INTO `xjm_sms_code` VALUES ('1323', '13870512989', '4705', '1536417441', '');
INSERT INTO `xjm_sms_code` VALUES ('1324', '13876795672', '7155', '1536421150', '');
INSERT INTO `xjm_sms_code` VALUES ('1325', '17820322001', '9363', '1536484257', '1');
INSERT INTO `xjm_sms_code` VALUES ('1326', '15715692352', '7391', '1536564059', '');
INSERT INTO `xjm_sms_code` VALUES ('1327', '15715692352', '9367', '1536564176', '1');
INSERT INTO `xjm_sms_code` VALUES ('1328', '13187066767', '1468', '1536671818', '');
INSERT INTO `xjm_sms_code` VALUES ('1329', '18860197743', '6416', '1536754016', '');
INSERT INTO `xjm_sms_code` VALUES ('1330', '17301073675', '6662', '1536781064', '1');
INSERT INTO `xjm_sms_code` VALUES ('1331', '15014807191', '1113', '1536900555', '');
INSERT INTO `xjm_sms_code` VALUES ('1332', '18829544580', '7401', '1537024667', '1');
INSERT INTO `xjm_sms_code` VALUES ('1333', '18988827021', '5434', '1537071260', '');
INSERT INTO `xjm_sms_code` VALUES ('1334', '18988827021', '8500', '1537085485', '');
INSERT INTO `xjm_sms_code` VALUES ('1335', '13608902377', '4048', '1537170202', '');
INSERT INTO `xjm_sms_code` VALUES ('1336', '13713828074', '1358', '1537195027', '');
INSERT INTO `xjm_sms_code` VALUES ('1337', '15769500213', '8041', '1537200020', '1');
INSERT INTO `xjm_sms_code` VALUES ('1338', '18507968850', '9255', '1537255258', '');
INSERT INTO `xjm_sms_code` VALUES ('1339', '18505203816', '7143', '1537255272', '');
INSERT INTO `xjm_sms_code` VALUES ('1340', '15880051221', '8693', '1537255756', '');
INSERT INTO `xjm_sms_code` VALUES ('1341', '17645099126', '1753', '1537256398', '');
INSERT INTO `xjm_sms_code` VALUES ('1342', '17645099126', '4022', '1537256423', '');
INSERT INTO `xjm_sms_code` VALUES ('1343', '13048089102', '6030', '1537277282', '1');
INSERT INTO `xjm_sms_code` VALUES ('1344', '18578678190', '8092', '1537342029', '');
INSERT INTO `xjm_sms_code` VALUES ('1345', '13760346878', '2844', '1537427877', '');
INSERT INTO `xjm_sms_code` VALUES ('1346', '15952708735', '4207', '1537449152', '1');
INSERT INTO `xjm_sms_code` VALUES ('1347', '18765553781', '6475', '1537455181', '1');
INSERT INTO `xjm_sms_code` VALUES ('1348', '18769766675', '6589', '1537455252', '1');
INSERT INTO `xjm_sms_code` VALUES ('1349', '13183800685', '8792', '1537711802', '1');
INSERT INTO `xjm_sms_code` VALUES ('1350', '18101715198', '3308', '1537712844', '');
INSERT INTO `xjm_sms_code` VALUES ('1351', '13543328468', '4624', '1537785593', '');
INSERT INTO `xjm_sms_code` VALUES ('1352', '13924628345', '6087', '1537798788', '');
INSERT INTO `xjm_sms_code` VALUES ('1353', '13585885769', '3250', '1537829660', '');
INSERT INTO `xjm_sms_code` VALUES ('1354', '18924104499', '1113', '1537859468', '1');
INSERT INTO `xjm_sms_code` VALUES ('1355', '18924104499', '5468', '1537863439', '1');
INSERT INTO `xjm_sms_code` VALUES ('1356', '13853978209', '3950', '1537943073', '');
INSERT INTO `xjm_sms_code` VALUES ('1357', '17313164830', '8205', '1537946016', '');
INSERT INTO `xjm_sms_code` VALUES ('1358', '18924104499', '1132', '1537947406', '');
INSERT INTO `xjm_sms_code` VALUES ('1359', '18998753701', '1109', '1537969845', '');
INSERT INTO `xjm_sms_code` VALUES ('1360', '13291080585', '5854', '1538029195', '');
INSERT INTO `xjm_sms_code` VALUES ('1361', '15589760268', '7521', '1538050153', '');
INSERT INTO `xjm_sms_code` VALUES ('1362', '15589760268', '7502', '1538050170', '');
INSERT INTO `xjm_sms_code` VALUES ('1363', '18263299052', '1374', '1538050255', '');
INSERT INTO `xjm_sms_code` VALUES ('1364', '18972613131', '4376', '1538060580', '');
INSERT INTO `xjm_sms_code` VALUES ('1365', '15937183825', '4836', '1538090663', '');
INSERT INTO `xjm_sms_code` VALUES ('1366', '13291080585', '9147', '1538093921', '');
INSERT INTO `xjm_sms_code` VALUES ('1367', '13825285576', '9074', '1538112443', '');
INSERT INTO `xjm_sms_code` VALUES ('1368', '13825285576', '1967', '1538112482', '');
INSERT INTO `xjm_sms_code` VALUES ('1369', '13825285576', '3619', '1538112492', '');
INSERT INTO `xjm_sms_code` VALUES ('1370', '18086600297', '3090', '1538112570', '');
INSERT INTO `xjm_sms_code` VALUES ('1371', '13825285576', '7585', '1538112579', '');
INSERT INTO `xjm_sms_code` VALUES ('1372', '18086600297', '5187', '1538112583', '');
INSERT INTO `xjm_sms_code` VALUES ('1373', '13692739599', '9474', '1538112674', '');
INSERT INTO `xjm_sms_code` VALUES ('1374', '13797119898', '7598', '1538112679', '');
INSERT INTO `xjm_sms_code` VALUES ('1375', '13797119898', '6708', '1538112719', '');
INSERT INTO `xjm_sms_code` VALUES ('1376', '18617313551', '5671', '1538123222', '');
INSERT INTO `xjm_sms_code` VALUES ('1377', '18617313551', '5612', '1538123241', '');
INSERT INTO `xjm_sms_code` VALUES ('1378', '18617313551', '3884', '1538123301', '');
INSERT INTO `xjm_sms_code` VALUES ('1379', '15338768567', '2076', '1538123313', '');
INSERT INTO `xjm_sms_code` VALUES ('1380', '18617313551', '6221', '1538123315', '');
INSERT INTO `xjm_sms_code` VALUES ('1381', '15338768567', '5082', '1538123328', '');
INSERT INTO `xjm_sms_code` VALUES ('1382', '18617313551', '3253', '1538123338', '');
INSERT INTO `xjm_sms_code` VALUES ('1383', '13893593767', '4428', '1538123340', '');
INSERT INTO `xjm_sms_code` VALUES ('1384', '18119999911', '8797', '1538123344', '');
INSERT INTO `xjm_sms_code` VALUES ('1385', '18119999911', '6198', '1538123364', '');
INSERT INTO `xjm_sms_code` VALUES ('1386', '18119999911', '2831', '1538123374', '');
INSERT INTO `xjm_sms_code` VALUES ('1387', '13473778596', '2593', '1538123453', '');
INSERT INTO `xjm_sms_code` VALUES ('1388', '18119999911', '4108', '1538123460', '');
INSERT INTO `xjm_sms_code` VALUES ('1389', '13473778596', '9616', '1538123469', '');
INSERT INTO `xjm_sms_code` VALUES ('1390', '15527059083', '7955', '1538123482', '');
INSERT INTO `xjm_sms_code` VALUES ('1391', '13473778596', '1570', '1538123501', '');
INSERT INTO `xjm_sms_code` VALUES ('1392', '18617313551', '3498', '1538124692', '');
INSERT INTO `xjm_sms_code` VALUES ('1393', '15872985052', '9207', '1538124847', '');
INSERT INTO `xjm_sms_code` VALUES ('1394', '15872985052', '8676', '1538124877', '');
INSERT INTO `xjm_sms_code` VALUES ('1395', '15815588877', '4164', '1538124878', '');
INSERT INTO `xjm_sms_code` VALUES ('1396', '15815588877', '7306', '1538124910', '');
INSERT INTO `xjm_sms_code` VALUES ('1397', '15872985052', '2679', '1538124912', '');
INSERT INTO `xjm_sms_code` VALUES ('1398', '15872985052', '6703', '1538124935', '');
INSERT INTO `xjm_sms_code` VALUES ('1399', '15815588877', '5898', '1538124947', '');
INSERT INTO `xjm_sms_code` VALUES ('1400', '13834488800', '7757', '1538128640', '');
INSERT INTO `xjm_sms_code` VALUES ('1401', '18972613131', '2990', '1538129937', '');
INSERT INTO `xjm_sms_code` VALUES ('1402', '18972613131', '7028', '1538133429', '');
INSERT INTO `xjm_sms_code` VALUES ('1403', '17307293235', '9409', '1538133471', '');
INSERT INTO `xjm_sms_code` VALUES ('1404', '18972613131', '1645', '1538133575', '');
INSERT INTO `xjm_sms_code` VALUES ('1405', '18972613131', '2560', '1538133803', '');
INSERT INTO `xjm_sms_code` VALUES ('1406', '15586861810', '8626', '1538141616', '1');
INSERT INTO `xjm_sms_code` VALUES ('1407', '15586861810', '2360', '1538141677', '1');
INSERT INTO `xjm_sms_code` VALUES ('1408', '15872985052', '8923', '1538144799', '');
INSERT INTO `xjm_sms_code` VALUES ('1409', '15872985052', '1214', '1538144811', '');
INSERT INTO `xjm_sms_code` VALUES ('1410', '18119999911', '5826', '1538171072', '');
INSERT INTO `xjm_sms_code` VALUES ('1411', '15872985052', '3234', '1538176619', '');
INSERT INTO `xjm_sms_code` VALUES ('1412', '18137867135', '5577', '1538178091', '');
INSERT INTO `xjm_sms_code` VALUES ('1413', '18137867135', '3441', '1538178108', '');
INSERT INTO `xjm_sms_code` VALUES ('1414', '15017480787', '9262', '1538186521', '');
INSERT INTO `xjm_sms_code` VALUES ('1415', '18617313551', '1315', '1538188196', '');
INSERT INTO `xjm_sms_code` VALUES ('1416', '17786431431', '5225', '1538191074', '');
INSERT INTO `xjm_sms_code` VALUES ('1417', '17354018992', '3146', '1538192955', '');
INSERT INTO `xjm_sms_code` VALUES ('1418', '13063533583', '7320', '1538234540', '1');
INSERT INTO `xjm_sms_code` VALUES ('1419', '18971645996', '8705', '1538234805', '');
INSERT INTO `xjm_sms_code` VALUES ('1420', '18971645996', '7674', '1538234840', '');
INSERT INTO `xjm_sms_code` VALUES ('1421', '13763587775', '2199', '1538237228', '');
INSERT INTO `xjm_sms_code` VALUES ('1422', '13763587775', '9928', '1538237251', '');
INSERT INTO `xjm_sms_code` VALUES ('1423', '18621557371', '9876', '1538238164', '');
INSERT INTO `xjm_sms_code` VALUES ('1424', '18679950828', '8797', '1538267899', '1');
INSERT INTO `xjm_sms_code` VALUES ('1425', '18679950828', '6528', '1538267961', '1');
INSERT INTO `xjm_sms_code` VALUES ('1426', '18679950828', '6975', '1538268054', '1');
INSERT INTO `xjm_sms_code` VALUES ('1427', '18679950828', '1256', '1538268164', '');
INSERT INTO `xjm_sms_code` VALUES ('1428', '18679950828', '6372', '1538268178', '');
INSERT INTO `xjm_sms_code` VALUES ('1429', '18170081303', '9016', '1538268191', '');
INSERT INTO `xjm_sms_code` VALUES ('1430', '18679950828', '3193', '1538268309', '1');
INSERT INTO `xjm_sms_code` VALUES ('1431', '15968557724', '4860', '1538272953', '');
INSERT INTO `xjm_sms_code` VALUES ('1432', '15968557724', '9442', '1538272964', '');
INSERT INTO `xjm_sms_code` VALUES ('1433', '15968557724', '7943', '1538272980', '');
INSERT INTO `xjm_sms_code` VALUES ('1434', '13614998644', '6460', '1538274162', '');
INSERT INTO `xjm_sms_code` VALUES ('1435', '13614998644', '1290', '1538274194', '');
INSERT INTO `xjm_sms_code` VALUES ('1436', '13614998644', '8134', '1538274271', '');
INSERT INTO `xjm_sms_code` VALUES ('1437', '15767889155', '6847', '1538275102', '');
INSERT INTO `xjm_sms_code` VALUES ('1438', '15767889155', '1338', '1538275119', '');
INSERT INTO `xjm_sms_code` VALUES ('1439', '13324312799', '7949', '1538275667', '');
INSERT INTO `xjm_sms_code` VALUES ('1440', '15143006929', '8980', '1538276127', '');
INSERT INTO `xjm_sms_code` VALUES ('1441', '15143006929', '7765', '1538276137', '');
INSERT INTO `xjm_sms_code` VALUES ('1442', '15143006929', '3698', '1538276194', '');
INSERT INTO `xjm_sms_code` VALUES ('1443', '18164101933', '5386', '1538276577', '');
INSERT INTO `xjm_sms_code` VALUES ('1444', '18679950828', '6540', '1538276960', '1');
INSERT INTO `xjm_sms_code` VALUES ('1445', '18164101933', '9067', '1538278692', '');
INSERT INTO `xjm_sms_code` VALUES ('1446', '13376688816', '8134', '1538291492', '');
INSERT INTO `xjm_sms_code` VALUES ('1447', '13376688816', '1203', '1538291514', '');
INSERT INTO `xjm_sms_code` VALUES ('1448', '15926311931', '3416', '1538304247', '');
INSERT INTO `xjm_sms_code` VALUES ('1449', '15926311931', '2237', '1538304354', '');
INSERT INTO `xjm_sms_code` VALUES ('1450', '15926311931', '8532', '1538304373', '');
INSERT INTO `xjm_sms_code` VALUES ('1451', '15926311931', '8582', '1538312297', '');
INSERT INTO `xjm_sms_code` VALUES ('1452', '15872985052', '9326', '1538312542', '');
INSERT INTO `xjm_sms_code` VALUES ('1453', '15350386272', '7946', '1538313245', '');
INSERT INTO `xjm_sms_code` VALUES ('1454', '15350386272', '1519', '1538313259', '');
INSERT INTO `xjm_sms_code` VALUES ('1455', '13397986272', '4792', '1538313289', '');
INSERT INTO `xjm_sms_code` VALUES ('1456', '13397986272', '4827', '1538313303', '');
INSERT INTO `xjm_sms_code` VALUES ('1457', '18626006245', '8099', '1538316606', '');
INSERT INTO `xjm_sms_code` VALUES ('1458', '18626006245', '1518', '1538316688', '');
INSERT INTO `xjm_sms_code` VALUES ('1459', '15757193539', '6587', '1538316745', '');
INSERT INTO `xjm_sms_code` VALUES ('1460', '15757193539', '6285', '1538316758', '');
INSERT INTO `xjm_sms_code` VALUES ('1461', '15757193539', '3277', '1538316796', '');
INSERT INTO `xjm_sms_code` VALUES ('1462', '15757193539', '1610', '1538316822', '');
INSERT INTO `xjm_sms_code` VALUES ('1463', '18428382337', '3496', '1538323315', '');
INSERT INTO `xjm_sms_code` VALUES ('1464', '13063533583', '1130', '1538351430', '1');
INSERT INTO `xjm_sms_code` VALUES ('1465', '18986951698', '6031', '1538358819', '');
INSERT INTO `xjm_sms_code` VALUES ('1466', '18626006245', '6642', '1538363049', '');
INSERT INTO `xjm_sms_code` VALUES ('1467', '18627885988', '9881', '1538377552', '');
INSERT INTO `xjm_sms_code` VALUES ('1468', '18627885988', '9337', '1538377569', '');
INSERT INTO `xjm_sms_code` VALUES ('1469', '18627885988', '3571', '1538377593', '');
INSERT INTO `xjm_sms_code` VALUES ('1470', '18516235687', '7422', '1538384759', '');
INSERT INTO `xjm_sms_code` VALUES ('1471', '18216235687', '4143', '1538384777', '');
INSERT INTO `xjm_sms_code` VALUES ('1472', '15997476111', '2833', '1538384801', '');
INSERT INTO `xjm_sms_code` VALUES ('1473', '15997476111', '4107', '1538384887', '');
INSERT INTO `xjm_sms_code` VALUES ('1474', '18536415665', '9124', '1538398076', '');
INSERT INTO `xjm_sms_code` VALUES ('1475', '18536415665', '2737', '1538398089', '');
INSERT INTO `xjm_sms_code` VALUES ('1476', '13593321886', '4741', '1538398137', '');
INSERT INTO `xjm_sms_code` VALUES ('1477', '15997476111', '2503', '1538399191', '');
INSERT INTO `xjm_sms_code` VALUES ('1478', '18679950828', '6871', '1538399866', '1');
INSERT INTO `xjm_sms_code` VALUES ('1479', '18872219842', '4306', '1538438624', '');
INSERT INTO `xjm_sms_code` VALUES ('1480', '18872219842', '4387', '1538438635', '');
INSERT INTO `xjm_sms_code` VALUES ('1481', '15258552880', '8781', '1538455223', '');
INSERT INTO `xjm_sms_code` VALUES ('1482', '15258552880', '7947', '1538455236', '');
INSERT INTO `xjm_sms_code` VALUES ('1483', '15258552880', '1580', '1538455247', '');
INSERT INTO `xjm_sms_code` VALUES ('1484', '15258552880', '1616', '1538455270', '');
INSERT INTO `xjm_sms_code` VALUES ('1485', '15258552880', '9086', '1538455285', '');
INSERT INTO `xjm_sms_code` VALUES ('1486', '13777229036', '5394', '1538464341', '');
INSERT INTO `xjm_sms_code` VALUES ('1487', '15757193539', '6074', '1538480212', '');
INSERT INTO `xjm_sms_code` VALUES ('1488', '13525204157', '1182', '1538501509', '1');
INSERT INTO `xjm_sms_code` VALUES ('1489', '13355531312', '2949', '1538528056', '');
INSERT INTO `xjm_sms_code` VALUES ('1490', '13355531312', '4379', '1538528090', '');
INSERT INTO `xjm_sms_code` VALUES ('1491', '18679950828', '3772', '1538528264', '1');
INSERT INTO `xjm_sms_code` VALUES ('1492', '13545123138', '2922', '1538541130', '');
INSERT INTO `xjm_sms_code` VALUES ('1493', '13545123138', '9224', '1538541141', '');
INSERT INTO `xjm_sms_code` VALUES ('1494', '13047293198', '3243', '1538637990', '');
INSERT INTO `xjm_sms_code` VALUES ('1495', '18537928177', '6852', '1538650956', '');
INSERT INTO `xjm_sms_code` VALUES ('1496', '18537928177', '7079', '1538650967', '');
INSERT INTO `xjm_sms_code` VALUES ('1497', '18537928177', '1254', '1538651012', '');
INSERT INTO `xjm_sms_code` VALUES ('1498', '15767889155', '9623', '1538704903', '');
INSERT INTO `xjm_sms_code` VALUES ('1499', '15803554406', '1338', '1538785236', '');
INSERT INTO `xjm_sms_code` VALUES ('1500', '15803554406', '4792', '1538785249', '');
INSERT INTO `xjm_sms_code` VALUES ('1501', '18679950828', '7251', '1538797581', '1');
INSERT INTO `xjm_sms_code` VALUES ('1502', '18679950828', '7000', '1538797646', '1');
INSERT INTO `xjm_sms_code` VALUES ('1503', '18170081303', '7581', '1538797709', '1');
INSERT INTO `xjm_sms_code` VALUES ('1504', '18679950828', '4919', '1538797760', '');
INSERT INTO `xjm_sms_code` VALUES ('1505', '15926311931', '4749', '1538798835', '');
INSERT INTO `xjm_sms_code` VALUES ('1506', '13856999150', '9025', '1538801739', '');
INSERT INTO `xjm_sms_code` VALUES ('1507', '13856999150', '5793', '1538801764', '');
INSERT INTO `xjm_sms_code` VALUES ('1508', '13856999150', '5084', '1538801782', '');
INSERT INTO `xjm_sms_code` VALUES ('1509', '13856999150', '3261', '1538801814', '');
INSERT INTO `xjm_sms_code` VALUES ('1510', '13507067336', '5010', '1538804260', '');
INSERT INTO `xjm_sms_code` VALUES ('1511', '15326949456', '6294', '1538832334', '');
INSERT INTO `xjm_sms_code` VALUES ('1512', '15326949456', '3613', '1538832349', '');
INSERT INTO `xjm_sms_code` VALUES ('1513', '17313367622', '6608', '1538838011', '');
INSERT INTO `xjm_sms_code` VALUES ('1514', '17313367622', '7976', '1538838030', '');
INSERT INTO `xjm_sms_code` VALUES ('1515', '17313367622', '4394', '1538838165', '');
INSERT INTO `xjm_sms_code` VALUES ('1516', '18954171227', '7233', '1538841454', '');
INSERT INTO `xjm_sms_code` VALUES ('1517', '13659964697', '4951', '1538869336', '1');
INSERT INTO `xjm_sms_code` VALUES ('1518', '13659964697', '7048', '1538869397', '1');
INSERT INTO `xjm_sms_code` VALUES ('1519', '13927434942', '3232', '1538880967', '1');
INSERT INTO `xjm_sms_code` VALUES ('1520', '13927434942', '1186', '1538881030', '1');
INSERT INTO `xjm_sms_code` VALUES ('1521', '15915488842', '2758', '1538881127', '1');
INSERT INTO `xjm_sms_code` VALUES ('1522', '15622986230', '2632', '1538907191', '');
INSERT INTO `xjm_sms_code` VALUES ('1523', '15622986230', '6751', '1538907235', '');
INSERT INTO `xjm_sms_code` VALUES ('1524', '13302994720', '1918', '1538919725', '');
INSERT INTO `xjm_sms_code` VALUES ('1525', '13941325818', '8687', '1538922040', '');
INSERT INTO `xjm_sms_code` VALUES ('1526', '13355531312', '8543', '1538945573', '');
INSERT INTO `xjm_sms_code` VALUES ('1527', '15757193539', '6568', '1538982057', '');
INSERT INTO `xjm_sms_code` VALUES ('1528', '15550453720', '7299', '1538982782', '');
INSERT INTO `xjm_sms_code` VALUES ('1529', '15550453720', '6259', '1538982796', '');
INSERT INTO `xjm_sms_code` VALUES ('1530', '18253487814', '4289', '1538982807', '');
INSERT INTO `xjm_sms_code` VALUES ('1531', '15550453720', '5937', '1538982832', '');
INSERT INTO `xjm_sms_code` VALUES ('1532', '15550453720', '8629', '1538982850', '');
INSERT INTO `xjm_sms_code` VALUES ('1533', '15550453720', '8970', '1538982913', '');
INSERT INTO `xjm_sms_code` VALUES ('1534', '15550453720', '3877', '1538982933', '');
INSERT INTO `xjm_sms_code` VALUES ('1535', '17347408902', '4667', '1539061061', '');
INSERT INTO `xjm_sms_code` VALUES ('1536', '17347408902', '2840', '1539061083', '');
INSERT INTO `xjm_sms_code` VALUES ('1537', '18975647899', '1741', '1539061099', '');
INSERT INTO `xjm_sms_code` VALUES ('1538', '18975647899', '2827', '1539061169', '');
INSERT INTO `xjm_sms_code` VALUES ('1539', '13345964134', '5380', '1539069742', '');
INSERT INTO `xjm_sms_code` VALUES ('1540', '18565564525', '4014', '1539100066', '1');
INSERT INTO `xjm_sms_code` VALUES ('1541', '13539181374', '8864', '1539126242', '');
INSERT INTO `xjm_sms_code` VALUES ('1542', '15865978959', '7496', '1539155621', '');
INSERT INTO `xjm_sms_code` VALUES ('1543', '15865978959', '9042', '1539155649', '');
INSERT INTO `xjm_sms_code` VALUES ('1544', '18807199391', '9178', '1539242099', '');
INSERT INTO `xjm_sms_code` VALUES ('1545', '15928424149', '4349', '1539312311', '1');
INSERT INTO `xjm_sms_code` VALUES ('1546', '13715214786', '5947', '1539329439', '');
INSERT INTO `xjm_sms_code` VALUES ('1547', '13715214786', '6771', '1539329470', '');
INSERT INTO `xjm_sms_code` VALUES ('1548', '18668284388', '2850', '1539352224', '');
INSERT INTO `xjm_sms_code` VALUES ('1549', '18668284388', '9860', '1539352238', '');
INSERT INTO `xjm_sms_code` VALUES ('1550', '15005600978', '8384', '1539412610', '');
INSERT INTO `xjm_sms_code` VALUES ('1551', '15806697088', '9748', '1539413244', '');
INSERT INTO `xjm_sms_code` VALUES ('1552', '13720153711', '3665', '1539857845', '');
INSERT INTO `xjm_sms_code` VALUES ('1553', '13720153711', '6495', '1539857918', '');
INSERT INTO `xjm_sms_code` VALUES ('1554', '18971193603', '9245', '1540863604', '');
INSERT INTO `xjm_sms_code` VALUES ('1555', '13607128218', '9278', '1541064069', '');
INSERT INTO `xjm_sms_code` VALUES ('1556', '13607128218', '2641', '1542936423', '');
INSERT INTO `xjm_sms_code` VALUES ('1557', '18971193603', '9815', '1542971439', '');
INSERT INTO `xjm_sms_code` VALUES ('1558', '13802547394', '8993', '1542981885', '1');
INSERT INTO `xjm_sms_code` VALUES ('1559', '13345964134', '4603', '1543030779', '');
INSERT INTO `xjm_sms_code` VALUES ('1560', '13283725550', '7232', '1543144158', '');
INSERT INTO `xjm_sms_code` VALUES ('1561', '15321520093', '2687', '1543147464', '');
INSERT INTO `xjm_sms_code` VALUES ('1562', '15172370138', '4420', '1543152718', '');
INSERT INTO `xjm_sms_code` VALUES ('1563', '18868929901', '7118', '1543245860', '');
INSERT INTO `xjm_sms_code` VALUES ('1564', '13349931989', '8321', '1543292481', '');
INSERT INTO `xjm_sms_code` VALUES ('1565', '18976424080', '6207', '1543391173', '1');
INSERT INTO `xjm_sms_code` VALUES ('1566', '18507151771', '5556', '1543461096', '');
INSERT INTO `xjm_sms_code` VALUES ('1567', '15172419658', '2980', '1543471323', '');
INSERT INTO `xjm_sms_code` VALUES ('1568', '18627000898', '1813', '1543471470', '');
INSERT INTO `xjm_sms_code` VALUES ('1569', '18627000898', '2413', '1543471532', '');
INSERT INTO `xjm_sms_code` VALUES ('1570', '17683797607', '5100', '1543488759', '');
INSERT INTO `xjm_sms_code` VALUES ('1571', '13712497210', '7540', '1543491871', '');
INSERT INTO `xjm_sms_code` VALUES ('1572', '18971193603', '4184', '1543570990', '');
INSERT INTO `xjm_sms_code` VALUES ('1573', '15968557724', '9504', '1543572198', '');
INSERT INTO `xjm_sms_code` VALUES ('1574', '15068983915', '3759', '1543579319', '');
INSERT INTO `xjm_sms_code` VALUES ('1575', '18216236992', '6315', '1543671182', '');
INSERT INTO `xjm_sms_code` VALUES ('1576', '17624011313', '1209', '1543679598', '');
INSERT INTO `xjm_sms_code` VALUES ('1577', '17624011313', '8884', '1543679679', '');
INSERT INTO `xjm_sms_code` VALUES ('1578', '15736261967', '9457', '1543725884', '');
INSERT INTO `xjm_sms_code` VALUES ('1579', '13317181947', '4108', '1543728149', '');
INSERT INTO `xjm_sms_code` VALUES ('1580', '18832985395', '2252', '1543732276', '');
INSERT INTO `xjm_sms_code` VALUES ('1581', '18055849945', '2339', '1543758228', '2');
INSERT INTO `xjm_sms_code` VALUES ('1582', '18055849945', '9159', '1543758345', '2');
INSERT INTO `xjm_sms_code` VALUES ('1583', '15705686371', '9449', '1543758459', '1');
INSERT INTO `xjm_sms_code` VALUES ('1584', '18986312599', '5734', '1543760371', '');
INSERT INTO `xjm_sms_code` VALUES ('1585', '15327190700', '7366', '1543762765', '');
INSERT INTO `xjm_sms_code` VALUES ('1586', '13545123138', '7000', '1543766316', '');
INSERT INTO `xjm_sms_code` VALUES ('1587', '13545123138', '1034', '1543766354', '');
INSERT INTO `xjm_sms_code` VALUES ('1588', '18986127072', '8422', '1543818821', '');
INSERT INTO `xjm_sms_code` VALUES ('1589', '18351696500', '9350', '1543819208', '');
INSERT INTO `xjm_sms_code` VALUES ('1590', '13554295663', '7257', '1543819721', '');
INSERT INTO `xjm_sms_code` VALUES ('1591', '15549492989', '5172', '1543820503', '');
INSERT INTO `xjm_sms_code` VALUES ('1592', '19945051192', '8596', '1543821827', '');
INSERT INTO `xjm_sms_code` VALUES ('1593', '13371637861', '4922', '1543825148', '');
INSERT INTO `xjm_sms_code` VALUES ('1594', '18986312599', '5694', '1543828130', '');
INSERT INTO `xjm_sms_code` VALUES ('1595', '18986312599', '9728', '1543828478', '');
INSERT INTO `xjm_sms_code` VALUES ('1596', '13291000530', '1634', '1543829787', '1');
INSERT INTO `xjm_sms_code` VALUES ('1597', '13291000530', '9512', '1543829863', '1');
INSERT INTO `xjm_sms_code` VALUES ('1598', '15989378878', '6993', '1543836510', '');
INSERT INTO `xjm_sms_code` VALUES ('1599', '15989378878', '5745', '1543836598', '');
INSERT INTO `xjm_sms_code` VALUES ('1600', '13962984366', '6149', '1543846073', '');
INSERT INTO `xjm_sms_code` VALUES ('1601', '13962984333', '5824', '1543846134', '');
INSERT INTO `xjm_sms_code` VALUES ('1602', '18535853166', '3335', '1543849324', '1');
INSERT INTO `xjm_sms_code` VALUES ('1603', '17762816087', '3054', '1543898819', '');
INSERT INTO `xjm_sms_code` VALUES ('1604', '18675605365', '5827', '1543946960', '1');
INSERT INTO `xjm_sms_code` VALUES ('1605', '18675605365', '1617', '1543947020', '1');
INSERT INTO `xjm_sms_code` VALUES ('1606', '17727491431', '1735', '1543973845', '');
INSERT INTO `xjm_sms_code` VALUES ('1607', '13828830924', '4232', '1543995368', '');
INSERT INTO `xjm_sms_code` VALUES ('1608', '13828830924', '1126', '1543995411', '');
INSERT INTO `xjm_sms_code` VALUES ('1609', '13600783365', '4013', '1544017747', '');
INSERT INTO `xjm_sms_code` VALUES ('1610', '16655013469', '2183', '1544019840', '');
INSERT INTO `xjm_sms_code` VALUES ('1611', '15298806478', '6793', '1544020453', '');
INSERT INTO `xjm_sms_code` VALUES ('1612', '19806586787', '3297', '1544060357', '1');
INSERT INTO `xjm_sms_code` VALUES ('1613', '19806586787', '5124', '1544060429', '');
INSERT INTO `xjm_sms_code` VALUES ('1614', '19806586787', '9785', '1544060470', '');
INSERT INTO `xjm_sms_code` VALUES ('1615', '17610771953', '9562', '1544070969', '1');
INSERT INTO `xjm_sms_code` VALUES ('1616', '17610771953', '6950', '1544071054', '1');
INSERT INTO `xjm_sms_code` VALUES ('1617', '17610771953', '3022', '1544077905', '1');
INSERT INTO `xjm_sms_code` VALUES ('1618', '13860970802', '8941', '1544083739', '');
INSERT INTO `xjm_sms_code` VALUES ('1619', '19991858547', '8286', '1544084996', '');
INSERT INTO `xjm_sms_code` VALUES ('1620', '15737600708', '2549', '1544092584', '');
INSERT INTO `xjm_sms_code` VALUES ('1621', '15959950655', '2864', '1544100282', '');
INSERT INTO `xjm_sms_code` VALUES ('1622', '15959950655', '8821', '1544101308', '1');
INSERT INTO `xjm_sms_code` VALUES ('1623', '13459598625', '5419', '1544104112', '');
INSERT INTO `xjm_sms_code` VALUES ('1624', '15269594236', '4965', '1544104497', '');
INSERT INTO `xjm_sms_code` VALUES ('1625', '19806586787', '2447', '1544114356', '1');
INSERT INTO `xjm_sms_code` VALUES ('1626', '13828830924', '9182', '1544163323', '1');
INSERT INTO `xjm_sms_code` VALUES ('1627', '13543328468', '3399', '1544164804', '1');
INSERT INTO `xjm_sms_code` VALUES ('1628', '13544245291', '7888', '1544164850', '');
INSERT INTO `xjm_sms_code` VALUES ('1629', '13861178264', '1654', '1544195222', '1');
INSERT INTO `xjm_sms_code` VALUES ('1630', '17790532683', '3119', '1544267500', '');
INSERT INTO `xjm_sms_code` VALUES ('1631', '18845106518', '7413', '1544276479', '');
INSERT INTO `xjm_sms_code` VALUES ('1632', '18845016518', '5787', '1544276523', '');
INSERT INTO `xjm_sms_code` VALUES ('1633', '13309169008', '7428', '1544325214', '');
INSERT INTO `xjm_sms_code` VALUES ('1634', '13657260351', '6188', '1544325419', '');
INSERT INTO `xjm_sms_code` VALUES ('1635', '13333299088', '6402', '1544333645', '');
INSERT INTO `xjm_sms_code` VALUES ('1636', '13357594999', '9512', '1544334520', '');
INSERT INTO `xjm_sms_code` VALUES ('1637', '15903972699', '6222', '1544343964', '');
INSERT INTO `xjm_sms_code` VALUES ('1638', '15160477985', '3058', '1544410677', '');
INSERT INTO `xjm_sms_code` VALUES ('1639', '13030836666', '9670', '1544424959', '');
INSERT INTO `xjm_sms_code` VALUES ('1640', '13291000530', '5500', '1544465472', '1');
INSERT INTO `xjm_sms_code` VALUES ('1641', '13291000530', '6236', '1544465537', '1');
INSERT INTO `xjm_sms_code` VALUES ('1642', '13291000530', '6912', '1544465628', '1');
INSERT INTO `xjm_sms_code` VALUES ('1643', '13291000530', '3624', '1544465752', '1');
INSERT INTO `xjm_sms_code` VALUES ('1644', '18924104499', '3290', '1544509484', '1');
INSERT INTO `xjm_sms_code` VALUES ('1645', '15959950655', '9187', '1544607798', '');
INSERT INTO `xjm_sms_code` VALUES ('1646', '18692015873', '4447', '1544617905', '');
INSERT INTO `xjm_sms_code` VALUES ('1647', '15309169099', '6923', '1544623593', '');
INSERT INTO `xjm_sms_code` VALUES ('1648', '13328788888', '6895', '1544666290', '');
INSERT INTO `xjm_sms_code` VALUES ('1649', '13720153711', '8693', '1544693560', '');
INSERT INTO `xjm_sms_code` VALUES ('1650', '17322449897', '1133', '1544694167', '');
INSERT INTO `xjm_sms_code` VALUES ('1651', '18666010600', '6290', '1544694720', '');
INSERT INTO `xjm_sms_code` VALUES ('1652', '13823912365', '1867', '1544695046', '');
INSERT INTO `xjm_sms_code` VALUES ('1653', '18666010600', '1782', '1544695300', '');
INSERT INTO `xjm_sms_code` VALUES ('1654', '13720153711', '3068', '1544695394', '');
INSERT INTO `xjm_sms_code` VALUES ('1655', '13720153711', '8740', '1544695460', '');
INSERT INTO `xjm_sms_code` VALUES ('1656', '18975810377', '6382', '1544695571', '');
INSERT INTO `xjm_sms_code` VALUES ('1657', '15113375199', '3264', '1544752430', '');
INSERT INTO `xjm_sms_code` VALUES ('1658', '13554141532', '6134', '1544755347', '');
INSERT INTO `xjm_sms_code` VALUES ('1659', '13554141532', '4834', '1544755369', '');
INSERT INTO `xjm_sms_code` VALUES ('1660', '13554141532', '9040', '1544755492', '1');
INSERT INTO `xjm_sms_code` VALUES ('1661', '15907150581', '2827', '1544759223', '');
INSERT INTO `xjm_sms_code` VALUES ('1662', '13554141532', '5111', '1544759282', '1');
INSERT INTO `xjm_sms_code` VALUES ('1663', '17671746469', '4182', '1544759343', '');
INSERT INTO `xjm_sms_code` VALUES ('1664', '18521598723', '9855', '1544759802', '');
INSERT INTO `xjm_sms_code` VALUES ('1665', '17671448546', '7585', '1544764277', '1');
INSERT INTO `xjm_sms_code` VALUES ('1666', '15907150581', '8502', '1544765219', '1');
INSERT INTO `xjm_sms_code` VALUES ('1667', '15210970627', '1400', '1544804172', '1');
INSERT INTO `xjm_sms_code` VALUES ('1668', '15210970627', '2805', '1544804317', '1');
INSERT INTO `xjm_sms_code` VALUES ('1669', '15210970627', '7447', '1544804438', '1');
INSERT INTO `xjm_sms_code` VALUES ('1670', '17730415891', '8713', '1544809749', '1');
INSERT INTO `xjm_sms_code` VALUES ('1671', '17730415891', '5549', '1544809812', '1');
INSERT INTO `xjm_sms_code` VALUES ('1672', '13518082677', '9208', '1544849321', '');
INSERT INTO `xjm_sms_code` VALUES ('1673', '18975810377', '2954', '1544849370', '');
INSERT INTO `xjm_sms_code` VALUES ('1674', '15959095329', '5786', '1544852832', '');
INSERT INTO `xjm_sms_code` VALUES ('1675', '15221982868', '4054', '1544855969', '');
INSERT INTO `xjm_sms_code` VALUES ('1676', '15210970627', '6989', '1544874139', '1');
INSERT INTO `xjm_sms_code` VALUES ('1677', '15907150581', '7289', '1544959131', '1');
INSERT INTO `xjm_sms_code` VALUES ('1678', '13554141532', '1525', '1545013130', '');
INSERT INTO `xjm_sms_code` VALUES ('1679', '18954171227', '9254', '1545094582', '');
INSERT INTO `xjm_sms_code` VALUES ('1680', '13554141532', '6158', '1545096257', '1');
INSERT INTO `xjm_sms_code` VALUES ('1681', '13752960939', '2066', '1545153824', '');
INSERT INTO `xjm_sms_code` VALUES ('1682', '18428382337', '8231', '1545264363', '');
INSERT INTO `xjm_sms_code` VALUES ('1683', '18627056710', '1148', '1545284781', '1');
INSERT INTO `xjm_sms_code` VALUES ('1684', '17707267486', '1262', '1545285206', '');
INSERT INTO `xjm_sms_code` VALUES ('1685', '19978534131', '5862', '1545296288', '');
INSERT INTO `xjm_sms_code` VALUES ('1686', '17621237430', '7139', '1545296337', '');
INSERT INTO `xjm_sms_code` VALUES ('1687', '17621237430', '4087', '1545296435', '');
INSERT INTO `xjm_sms_code` VALUES ('1688', '18642163778', '1101', '1545370475', '');
INSERT INTO `xjm_sms_code` VALUES ('1689', '13871220065', '1656', '1545380405', '');
INSERT INTO `xjm_sms_code` VALUES ('1690', '18672214187', '7937', '1545458362', '');
INSERT INTO `xjm_sms_code` VALUES ('1691', '18300468723', '9073', '1545461381', '');
INSERT INTO `xjm_sms_code` VALUES ('1692', '18300468723', '2183', '1545462253', '');
INSERT INTO `xjm_sms_code` VALUES ('1693', '18300468723', '5710', '1545488664', '');
INSERT INTO `xjm_sms_code` VALUES ('1694', '18300468723', '8053', '1545564934', '');
INSERT INTO `xjm_sms_code` VALUES ('1695', '17671448546', '9927', '1545642204', '1');
INSERT INTO `xjm_sms_code` VALUES ('1696', '17671448546', '2145', '1545642299', '1');
INSERT INTO `xjm_sms_code` VALUES ('1697', '18521598723', '4117', '1545642466', '1');
INSERT INTO `xjm_sms_code` VALUES ('1698', '18521598723', '1203', '1545642551', '1');
INSERT INTO `xjm_sms_code` VALUES ('1699', '18521598723', '6503', '1545704255', '');
INSERT INTO `xjm_sms_code` VALUES ('1700', '18627056710', '4791', '1545720735', '');
INSERT INTO `xjm_sms_code` VALUES ('1701', '18627056710', '1264', '1545721388', '');
INSERT INTO `xjm_sms_code` VALUES ('1702', '18627056710', '1331', '1545722419', '');
INSERT INTO `xjm_sms_code` VALUES ('1703', '13554141532', '1769', '1545724341', '');
INSERT INTO `xjm_sms_code` VALUES ('1704', '11862705671', '1394', '1545724566', '');
INSERT INTO `xjm_sms_code` VALUES ('1705', '18627056710', '7935', '1545724580', '');
INSERT INTO `xjm_sms_code` VALUES ('1706', '13554141532', '1103', '1545726470', '');
INSERT INTO `xjm_sms_code` VALUES ('1707', '13626320133', '2523', '1545744071', '1');
INSERT INTO `xjm_sms_code` VALUES ('1708', '13626320133', '4938', '1545744144', '1');
INSERT INTO `xjm_sms_code` VALUES ('1709', '13554141532', '4120', '1545790259', '1');
INSERT INTO `xjm_sms_code` VALUES ('1710', '18627056710', '5032', '1545793148', '1');
INSERT INTO `xjm_sms_code` VALUES ('1711', '18566732867', '8296', '1545859183', '1');
INSERT INTO `xjm_sms_code` VALUES ('1712', '18566732867', '5019', '1545859291', '1');
INSERT INTO `xjm_sms_code` VALUES ('1713', '13554141532', '7748', '1545891016', '');
INSERT INTO `xjm_sms_code` VALUES ('1714', '17671746469', '9480', '1545895285', '');
INSERT INTO `xjm_sms_code` VALUES ('1715', '17671746469', '6204', '1545959877', '1');
INSERT INTO `xjm_sms_code` VALUES ('1716', '15927187854', '3942', '1545960533', '1');
INSERT INTO `xjm_sms_code` VALUES ('1717', '17707267486', '4266', '1545961802', '1');
INSERT INTO `xjm_sms_code` VALUES ('1718', '18571732298', '5270', '1545962003', '1');
INSERT INTO `xjm_sms_code` VALUES ('1719', '18627056710', '6170', '1545962737', '1');
INSERT INTO `xjm_sms_code` VALUES ('1720', '18062394372', '3336', '1545962782', '1');
INSERT INTO `xjm_sms_code` VALUES ('1721', '18521598723', '8402', '1545964132', '1');
INSERT INTO `xjm_sms_code` VALUES ('1722', '17720588195', '1792', '1545965514', '1');
INSERT INTO `xjm_sms_code` VALUES ('1723', '17720588195', '2569', '1545965559', '1');
INSERT INTO `xjm_sms_code` VALUES ('1724', '17671746469', '3344', '1545972366', '1');
INSERT INTO `xjm_sms_code` VALUES ('1725', '18062394372', '5941', '1545976803', '1');
INSERT INTO `xjm_sms_code` VALUES ('1726', '18521598723', '4109', '1545977839', '1');
INSERT INTO `xjm_sms_code` VALUES ('1727', '17671746469', '4698', '1545977998', '1');
INSERT INTO `xjm_sms_code` VALUES ('1728', '17671746469', '5346', '1545978341', '1');
INSERT INTO `xjm_sms_code` VALUES ('1729', '18521598723', '3641', '1545978431', '1');
INSERT INTO `xjm_sms_code` VALUES ('1730', '13164604526', '7009', '1545978593', '1');
INSERT INTO `xjm_sms_code` VALUES ('1731', '18521598723', '4980', '1545978651', '1');
INSERT INTO `xjm_sms_code` VALUES ('1732', '18521598723', '7299', '1545978915', '1');
INSERT INTO `xjm_sms_code` VALUES ('1733', '18062394372', '4777', '1546047182', '');
INSERT INTO `xjm_sms_code` VALUES ('1734', '18162511920', '6987', '1546068769', '');
INSERT INTO `xjm_sms_code` VALUES ('1735', '17671746469', '6664', '1546073625', '');
INSERT INTO `xjm_sms_code` VALUES ('1736', '18339280527', '6378', '1546190576', '1');
INSERT INTO `xjm_sms_code` VALUES ('1737', '17707267486', '9896', '1546400199', '');
INSERT INTO `xjm_sms_code` VALUES ('1738', '15387231319', '2594', '1546413245', '');
INSERT INTO `xjm_sms_code` VALUES ('1739', '18521598723', '1763', '1546488008', '');
INSERT INTO `xjm_sms_code` VALUES ('1740', '13554141532', '1911', '1546502718', '');
INSERT INTO `xjm_sms_code` VALUES ('1741', '18062394372', '6381', '1546506589', '');
INSERT INTO `xjm_sms_code` VALUES ('1742', '18062394372', '5156', '1546506645', '1');
INSERT INTO `xjm_sms_code` VALUES ('1743', '17671746469', '1611', '1546506655', '1');
INSERT INTO `xjm_sms_code` VALUES ('1744', '18571710546', '7422', '1546507677', '1');
INSERT INTO `xjm_sms_code` VALUES ('1745', '18521598723', '7392', '1546564701', '');
INSERT INTO `xjm_sms_code` VALUES ('1746', '17707267486', '4654', '1546566296', '');
INSERT INTO `xjm_sms_code` VALUES ('1747', '18062394372', '2879', '1546566669', '1');
INSERT INTO `xjm_sms_code` VALUES ('1748', '17707267486', '1370', '1546566676', '');
INSERT INTO `xjm_sms_code` VALUES ('1749', '18627056710', '3422', '1546566751', '1');
INSERT INTO `xjm_sms_code` VALUES ('1750', '13554141532', '9131', '1546566757', '1');
INSERT INTO `xjm_sms_code` VALUES ('1751', '17707267486', '3379', '1546566808', '');
INSERT INTO `xjm_sms_code` VALUES ('1752', '18571710546', '2250', '1546569488', '1');
INSERT INTO `xjm_sms_code` VALUES ('1753', '18062394372', '4034', '1546569715', '1');
INSERT INTO `xjm_sms_code` VALUES ('1754', '18893848182', '6629', '1546571158', '1');
INSERT INTO `xjm_sms_code` VALUES ('1755', '18893848102', '8939', '1546571219', '1');
INSERT INTO `xjm_sms_code` VALUES ('1756', '17671448546', '1360', '1546572249', '1');
INSERT INTO `xjm_sms_code` VALUES ('1757', '17625576274', '8593', '1546581416', '1');
INSERT INTO `xjm_sms_code` VALUES ('1758', '18062394372', '3813', '1546581767', '1');
INSERT INTO `xjm_sms_code` VALUES ('1759', '18571710546', '5781', '1546582057', '1');
INSERT INTO `xjm_sms_code` VALUES ('1760', '18571710546', '5780', '1546582355', '1');
INSERT INTO `xjm_sms_code` VALUES ('1761', '18300468723', '4335', '1546591440', '');
INSERT INTO `xjm_sms_code` VALUES ('1762', '13354305788', '3556', '1546611727', '1');
INSERT INTO `xjm_sms_code` VALUES ('1763', '15112635776', '2656', '1546652030', '');
INSERT INTO `xjm_sms_code` VALUES ('1764', '13607128218', '9993', '1546652033', '');
INSERT INTO `xjm_sms_code` VALUES ('1765', '15112635776', '9046', '1546652055', '');
INSERT INTO `xjm_sms_code` VALUES ('1766', '13607128218', '3815', '1546652087', '');
INSERT INTO `xjm_sms_code` VALUES ('1767', '18627056710', '6597', '1546652211', '1');
INSERT INTO `xjm_sms_code` VALUES ('1768', '13554141532', '8774', '1546652956', '1');
INSERT INTO `xjm_sms_code` VALUES ('1769', '18521598723', '1009', '1546653805', '1');
INSERT INTO `xjm_sms_code` VALUES ('1770', '13607128218', '2211', '1546653933', '');
INSERT INTO `xjm_sms_code` VALUES ('1771', '18062394372', '4502', '1546655039', '1');
INSERT INTO `xjm_sms_code` VALUES ('1772', '18062394372', '8015', '1546655333', '1');
INSERT INTO `xjm_sms_code` VALUES ('1773', '18521598723', '4902', '1546658711', '1');
INSERT INTO `xjm_sms_code` VALUES ('1774', '18521598723', '4477', '1546659217', '1');
INSERT INTO `xjm_sms_code` VALUES ('1775', '18521598723', '1678', '1546659807', '1');
INSERT INTO `xjm_sms_code` VALUES ('1776', '18521598723', '2861', '1546659988', '1');
INSERT INTO `xjm_sms_code` VALUES ('1777', '18521598723', '9082', '1546667170', '1');
INSERT INTO `xjm_sms_code` VALUES ('1778', '18521598723', '5396', '1546667260', '1');
INSERT INTO `xjm_sms_code` VALUES ('1779', '18521598723', '8755', '1546668234', '1');
INSERT INTO `xjm_sms_code` VALUES ('1780', '18571710546', '7921', '1546669126', '1');
INSERT INTO `xjm_sms_code` VALUES ('1781', '18571710546', '9197', '1546669762', '1');
INSERT INTO `xjm_sms_code` VALUES ('1782', '18521598723', '8347', '1546669814', '1');
INSERT INTO `xjm_sms_code` VALUES ('1783', '18521598723', '9061', '1546670428', '1');
INSERT INTO `xjm_sms_code` VALUES ('1784', '18694065710', '4338', '1546670595', '1');
INSERT INTO `xjm_sms_code` VALUES ('1785', '18521598723', '4487', '1546670748', '1');
INSERT INTO `xjm_sms_code` VALUES ('1786', '18062394372', '8079', '1546670847', '1');
INSERT INTO `xjm_sms_code` VALUES ('1787', '18062394372', '1128', '1546670987', '1');
INSERT INTO `xjm_sms_code` VALUES ('1788', '18694065710', '9021', '1546670988', '1');
INSERT INTO `xjm_sms_code` VALUES ('1789', '18521598723', '2320', '1546671002', '1');
INSERT INTO `xjm_sms_code` VALUES ('1790', '18521598723', '7880', '1546671151', '1');
INSERT INTO `xjm_sms_code` VALUES ('1791', '18521598723', '3716', '1546671422', '1');
INSERT INTO `xjm_sms_code` VALUES ('1792', '18062394372', '8889', '1546671827', '1');
INSERT INTO `xjm_sms_code` VALUES ('1793', '17671746469', '2177', '1546671846', '1');
INSERT INTO `xjm_sms_code` VALUES ('1794', '18571710546', '1389', '1546671857', '1');
INSERT INTO `xjm_sms_code` VALUES ('1795', '18062394372', '6507', '1546672122', '1');
INSERT INTO `xjm_sms_code` VALUES ('1796', '18694065710', '7343', '1546672428', '1');
INSERT INTO `xjm_sms_code` VALUES ('1797', '15171480993', '6043', '1546674156', '');
INSERT INTO `xjm_sms_code` VALUES ('1798', '13759688137', '5447', '1546674211', '');
INSERT INTO `xjm_sms_code` VALUES ('1799', '18816895699', '4167', '1546674278', '');
INSERT INTO `xjm_sms_code` VALUES ('1800', '13260564119', '5088', '1546674403', '');
INSERT INTO `xjm_sms_code` VALUES ('1801', '18521598723', '7825', '1546674528', '1');
INSERT INTO `xjm_sms_code` VALUES ('1802', '18521598723', '3082', '1546674657', '1');
INSERT INTO `xjm_sms_code` VALUES ('1803', '18062394372', '8586', '1546674889', '1');
INSERT INTO `xjm_sms_code` VALUES ('1804', '18521598723', '2348', '1546674947', '1');
INSERT INTO `xjm_sms_code` VALUES ('1805', '18062394372', '9861', '1546675665', '1');
INSERT INTO `xjm_sms_code` VALUES ('1806', '18521598723', '1119', '1546675835', '1');
INSERT INTO `xjm_sms_code` VALUES ('1807', '17708710125', '9986', '1546739348', '');
INSERT INTO `xjm_sms_code` VALUES ('1808', '15353575182', '3142', '1546772188', '');
INSERT INTO `xjm_sms_code` VALUES ('1809', '13048084220', '1779', '1546777432', '1');
INSERT INTO `xjm_sms_code` VALUES ('1810', '13263378183', '2645', '1546783102', '1');
INSERT INTO `xjm_sms_code` VALUES ('1811', '18694065710', '7660', '1546825045', '1');
INSERT INTO `xjm_sms_code` VALUES ('1812', '17671448546', '7559', '1546825226', '');
INSERT INTO `xjm_sms_code` VALUES ('1813', '17671448546', '7412', '1546825251', '');
INSERT INTO `xjm_sms_code` VALUES ('1814', '18694065710', '9930', '1546825336', '1');
INSERT INTO `xjm_sms_code` VALUES ('1815', '18694065710', '1809', '1546825687', '1');
INSERT INTO `xjm_sms_code` VALUES ('1816', '18627056710', '5378', '1546826087', '1');
INSERT INTO `xjm_sms_code` VALUES ('1817', '18062394372', '2980', '1546826282', '1');
INSERT INTO `xjm_sms_code` VALUES ('1818', '18627056710', '2749', '1546826875', '1');
INSERT INTO `xjm_sms_code` VALUES ('1819', '18694065710', '3865', '1546826899', '1');
INSERT INTO `xjm_sms_code` VALUES ('1820', '15955857953', '1471', '1546828469', '2');
INSERT INTO `xjm_sms_code` VALUES ('1821', '15955857953', '8584', '1546828530', '2');
INSERT INTO `xjm_sms_code` VALUES ('1822', '18627056710', '7755', '1546828834', '1');
INSERT INTO `xjm_sms_code` VALUES ('1823', '13554141532', '4239', '1546839422', '1');
INSERT INTO `xjm_sms_code` VALUES ('1824', '13554141532', '4121', '1546911889', '');
INSERT INTO `xjm_sms_code` VALUES ('1825', '13554141532', '7334', '1546915092', '1');
INSERT INTO `xjm_sms_code` VALUES ('1826', '13554141532', '5309', '1546915108', '1');
INSERT INTO `xjm_sms_code` VALUES ('1827', '18627056710', '2985', '1546925651', '');
INSERT INTO `xjm_sms_code` VALUES ('1828', '18627056710', '4811', '1546925888', '');
INSERT INTO `xjm_sms_code` VALUES ('1829', '18112916819', '4610', '1546961767', '');
INSERT INTO `xjm_sms_code` VALUES ('1830', '18627056710', '9963', '1547087089', '');
INSERT INTO `xjm_sms_code` VALUES ('1831', '18884175413', '3304', '1547089531', '1');
INSERT INTO `xjm_sms_code` VALUES ('1832', '18627056710', '8185', '1547107255', '');
INSERT INTO `xjm_sms_code` VALUES ('1833', '13145547155', '7333', '1547177813', '');
INSERT INTO `xjm_sms_code` VALUES ('1834', '18521598723', '2525', '1547185064', '');
INSERT INTO `xjm_sms_code` VALUES ('1835', '18521598723', '5270', '1547192615', '2');
INSERT INTO `xjm_sms_code` VALUES ('1836', '18521598723', '8340', '1547193167', '2');
INSERT INTO `xjm_sms_code` VALUES ('1837', '18521598723', '4585', '1547306600', '');
INSERT INTO `xjm_sms_code` VALUES ('1838', '18147516810', '2646', '1547343038', '');
INSERT INTO `xjm_sms_code` VALUES ('1839', '18054855123', '6926', '1547350301', '1');
INSERT INTO `xjm_sms_code` VALUES ('1840', '17621064788', '1721', '1547353492', '');
INSERT INTO `xjm_sms_code` VALUES ('1841', '13760184176', '8199', '1547379431', '1');
INSERT INTO `xjm_sms_code` VALUES ('1842', '13760184176', '4005', '1547380036', '2');
INSERT INTO `xjm_sms_code` VALUES ('1843', '13760184176', '8070', '1547380124', '2');
INSERT INTO `xjm_sms_code` VALUES ('1844', '13412332474', '6610', '1547380453', '1');
INSERT INTO `xjm_sms_code` VALUES ('1845', '13412332471', '6448', '1547380514', '1');
INSERT INTO `xjm_sms_code` VALUES ('1846', '18627056710', '4390', '1547434058', '');
INSERT INTO `xjm_sms_code` VALUES ('1847', '18521598723', '7696', '1547436374', '');
INSERT INTO `xjm_sms_code` VALUES ('1848', '18521598723', '1684', '1547442340', '');
INSERT INTO `xjm_sms_code` VALUES ('1849', '18521598723', '3388', '1547447207', '');
INSERT INTO `xjm_sms_code` VALUES ('1850', '18521598723', '8667', '1547447327', '');
INSERT INTO `xjm_sms_code` VALUES ('1851', '18627056710', '8074', '1547517200', '');
INSERT INTO `xjm_sms_code` VALUES ('1852', '18521598723', '4693', '1547529925', '');
INSERT INTO `xjm_sms_code` VALUES ('1853', '13554141532', '9922', '1547530540', '');
INSERT INTO `xjm_sms_code` VALUES ('1854', '18963992077', '3875', '1547538574', '');
INSERT INTO `xjm_sms_code` VALUES ('1855', '14704175446', '7712', '1547543854', '1');
INSERT INTO `xjm_sms_code` VALUES ('1856', '13578999712', '2582', '1547565269', '');
INSERT INTO `xjm_sms_code` VALUES ('1857', '13554141532', '1644', '1547601968', '1');
INSERT INTO `xjm_sms_code` VALUES ('1858', '13554141532', '5529', '1547602026', '1');
INSERT INTO `xjm_sms_code` VALUES ('1859', '13554141532', '2726', '1547602064', '1');
INSERT INTO `xjm_sms_code` VALUES ('1860', '13554141532', '6980', '1547602208', '1');
INSERT INTO `xjm_sms_code` VALUES ('1861', '18627056710', '2344', '1547602230', '1');
INSERT INTO `xjm_sms_code` VALUES ('1862', '18627056710', '4474', '1547602525', '1');
INSERT INTO `xjm_sms_code` VALUES ('1863', '13554141532', '2598', '1547603614', '1');
INSERT INTO `xjm_sms_code` VALUES ('1864', '13554141532', '2697', '1547604014', '');
INSERT INTO `xjm_sms_code` VALUES ('1865', '13554141532', '7790', '1547617743', '');
INSERT INTO `xjm_sms_code` VALUES ('1866', '13554141532', '7336', '1547622380', '1');
INSERT INTO `xjm_sms_code` VALUES ('1867', '18216236992', '1803', '1547653472', '');
INSERT INTO `xjm_sms_code` VALUES ('1868', '13926832619', '4955', '1547702684', '');
INSERT INTO `xjm_sms_code` VALUES ('1869', '15542859409', '1538', '1547780592', '1');
INSERT INTO `xjm_sms_code` VALUES ('1870', '13554141532', '4432', '1547782161', '1');
INSERT INTO `xjm_sms_code` VALUES ('1871', '18521598723', '4606', '1547810175', '');
INSERT INTO `xjm_sms_code` VALUES ('1872', '13163497912', '7858', '1547869843', '1');
INSERT INTO `xjm_sms_code` VALUES ('1873', '13926832619', '7531', '1547883784', '');
INSERT INTO `xjm_sms_code` VALUES ('1874', '18062013177', '8855', '1547883840', '');
INSERT INTO `xjm_sms_code` VALUES ('1875', '13097210953', '3334', '1547883854', '');
INSERT INTO `xjm_sms_code` VALUES ('1876', '18086453933', '2052', '1547883929', '');
INSERT INTO `xjm_sms_code` VALUES ('1877', '15011749988', '7682', '1547883961', '');
INSERT INTO `xjm_sms_code` VALUES ('1878', '13576432833', '5579', '1547884016', '');
INSERT INTO `xjm_sms_code` VALUES ('1879', '18674148713', '7768', '1547884177', '');
INSERT INTO `xjm_sms_code` VALUES ('1880', '13242799955', '3114', '1547884268', '');
INSERT INTO `xjm_sms_code` VALUES ('1881', '18658216818', '2149', '1547884569', '');
INSERT INTO `xjm_sms_code` VALUES ('1882', '18658216819', '4580', '1547884630', '');
INSERT INTO `xjm_sms_code` VALUES ('1883', '13697994616', '8538', '1547942181', '');
INSERT INTO `xjm_sms_code` VALUES ('1884', '18521598723', '8491', '1547971253', '');
INSERT INTO `xjm_sms_code` VALUES ('1885', '13548652553', '1496', '1547988670', '');
INSERT INTO `xjm_sms_code` VALUES ('1886', '18521598723', '8291', '1548035245', '');
INSERT INTO `xjm_sms_code` VALUES ('1887', '18521598723', '7582', '1548035481', '');
INSERT INTO `xjm_sms_code` VALUES ('1888', '18521598723', '9156', '1548035669', '');
INSERT INTO `xjm_sms_code` VALUES ('1889', '18167106601', '5898', '1548039247', '');

-- ----------------------------
-- Table structure for xjm_taboo
-- ----------------------------
DROP TABLE IF EXISTS `xjm_taboo`;
CREATE TABLE `xjm_taboo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) DEFAULT NULL,
  `taboo` varchar(255) DEFAULT NULL COMMENT '禁忌',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xjm_taboo
-- ----------------------------
INSERT INTO `xjm_taboo` VALUES ('1', '1', '禁止空腹按摩');
INSERT INTO `xjm_taboo` VALUES ('2', '2', '按摩后禁止剧烈运动');

-- ----------------------------
-- Table structure for xjm_techer
-- ----------------------------
DROP TABLE IF EXISTS `xjm_techer`;
CREATE TABLE `xjm_techer` (
  `tid` int(11) NOT NULL AUTO_INCREMENT,
  `mobile` varchar(20) NOT NULL,
  `pwd` varchar(255) DEFAULT NULL COMMENT '密码',
  `paypwd` varchar(40) DEFAULT '' COMMENT '支付密码',
  `nickname` varchar(20) NOT NULL COMMENT '用户名',
  `status` tinyint(1) DEFAULT '0' COMMENT '0：审核中 1：审核通过 2：封号',
  `is_online` tinyint(1) DEFAULT '1' COMMENT '0：在线 1：不在线',
  `money` decimal(10,2) DEFAULT '0.00' COMMENT '可提现余额',
  `skill_level` tinyint(1) DEFAULT '0' COMMENT '技能等级',
  `rate` tinyint(1) DEFAULT '0' COMMENT '评级',
  `area_id` int(8) DEFAULT '0' COMMENT '地区id',
  `t_lat` varchar(20) DEFAULT '' COMMENT '纬度',
  `t_lon` varchar(20) DEFAULT '' COMMENT '经度',
  `add_time` int(10) DEFAULT '0' COMMENT '添加时间',
  `cate_id` varchar(50) DEFAULT NULL COMMENT '技能id对应goods_option表的cate_id',
  `version` varchar(10) NOT NULL DEFAULT '1.0' COMMENT '版本号',
  `source` varchar(20) NOT NULL COMMENT '来源：ios，android',
  `last_login_ip` varchar(255) DEFAULT NULL,
  `skill` varchar(255) DEFAULT NULL COMMENT '关联goods表，逗号拼接的格式',
  `last_time` int(11) DEFAULT '0' COMMENT '最后心跳时间',
  `is_lock` tinyint(1) DEFAULT '0' COMMENT '即时上门时0未锁定，1已锁定',
  `pid` int(11) DEFAULT '0' COMMENT '上级ID',
  `icode` varchar(100) DEFAULT NULL COMMENT '邀请码',
  `tuidan_nums` int(11) DEFAULT '0' COMMENT '退单次数',
  PRIMARY KEY (`tid`)
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=utf8 COMMENT='技师表';

-- ----------------------------
-- Records of xjm_techer
-- ----------------------------
INSERT INTO `xjm_techer` VALUES ('1', '18868108303', '14a521ade411fef4afc8173303b17b48234836d1', '7c4a8d09ca3762af61e59520943dc26494f8941b', '露露', '1', '0', '0.00', '0', '0', '0', '30.555234', '114.343039', '0', null, '1.0', 'ios', '', '1,2,3,4,5,6,7,8,9', '0', '1', null, '907c17aa645c43e129fa535bc2ca1b05', '0');
INSERT INTO `xjm_techer` VALUES ('2', '18975661141', '413e56a7f6587a1d5b8540649f0044eed5d9f4a2', null, '李琴', '1', '0', '0.00', '5', '0', '0', '30.555234', '114.343361', '0', null, '1.0', 'android', '', '1,2,3,4,5,6,7,8,9', '0', '1', null, null, '0');
INSERT INTO `xjm_techer` VALUES ('3', '17778079115', '1a38cf898cbd21b8b0007888a8fe550ec0e4f859', '1a38cf898cbd21b8b0007888a8fe550ec0e4f859', '张涛', '1', '0', '65.00', '9', '0', '0', '30.555339', '114.34306', '0', null, '1.0', 'android', '210.21.228.211', '1,2,3,4,5,6,7,8,9', '1513043441', '1', null, '8002ac04f3d75a37a256e4e384fba671', '1');
INSERT INTO `xjm_techer` VALUES ('13', '18871865702', '0f0d74bb0eeb3b80ae3b0f8ee357717a9a84e7e7', null, '熊猫', '1', '1', '0.00', '0', '0', '0', '30.554778', '114.343361', '1505461417', null, '1.0', 'android', '', '1,2,3,4,5,6,7,8,9', '0', '0', null, null, '0');
INSERT INTO `xjm_techer` VALUES ('14', '18871065703', 'f122db007ed655921f98184e4302bba84990ff68', null, '妮娜', '1', '1', '0.00', '0', '0', '0', '30.594778', '114.303361', '1505468538', null, '1.0', 'ios', '', '1,2,3,4,5,6,7,8,9', '0', '0', null, null, '0');
INSERT INTO `xjm_techer` VALUES ('17', '15016740747', 'f122db007ed655921f98184e4302bba84990ff68', null, '海子', '1', '1', '0.00', '0', '0', '0', '30.594778', '114.303361', '1505714671', null, '1.0', 'ios', '', '1,2,3,4,5,6,7,8,9', '0', '0', null, null, '0');
INSERT INTO `xjm_techer` VALUES ('20', '18107214793', '6bd147209f0baaa5547a0f829d16b6ace96ad18b', '', '双子', '1', '1', '0.00', '0', '0', '3', '30.594778', '114.303361', '0', null, '1.0', 'ios', '58.48.208.10', '1,2,3,4,5,6,7,8,9', '0', '0', null, null, '0');
INSERT INTO `xjm_techer` VALUES ('21', '18910128843', '7c4a8d09ca3762af61e59520943dc26494f8941b', '', '阿麦', '0', '1', '0.00', '0', '0', '0', '30.554778', '114.343361', '0', null, '1.0', 'ios', '', '1,2,3,4,5,6,7,8,9', '0', '0', null, null, '0');
INSERT INTO `xjm_techer` VALUES ('22', '18910128844', '7c4a8d09ca3762af61e59520943dc26494f8941b', '', '宋波', '1', '1', '0.00', '0', '0', '0', '30.554778', '114.343361', '0', null, '1.0', 'android', '', '1,2,3,4,5,6,7,8,9', '0', '0', null, null, '0');
INSERT INTO `xjm_techer` VALUES ('23', '18910128845', '7c4a8d09ca3762af61e59520943dc26494f8941b', '', '阿信', '1', '1', '0.00', '0', '0', '0', '30.554778', '114.343361', '0', null, '1.0', 'ios', '', '1,2,3,4,5,6,7,8,9', '0', '1', null, null, '0');
INSERT INTO `xjm_techer` VALUES ('26', '18507101617', 'f122db007ed655921f98184e4302bba84990ff68', 'f122db007ed655921f98184e4302bba84990ff68', '莎莎', '1', '0', '0.00', '0', '0', '0', '30.543952', '114.333305', '1509587570', null, '1.0.0', 'android', '117.154.4.128', '1,2,3,4,5,6,7,8,9', '1512445592', '1', null, '28e4a7e3bcdec6d493b2329891a39a32', '0');
INSERT INTO `xjm_techer` VALUES ('32', '15102784093', '6149422d37635e1d776302c3881fd4abcd1a5146', '', '', '0', '0', '0.00', '0', '0', '0', '', '', '1510058849', null, '1.0.0', 'android', null, '1,2,3,4,5,6,7,8,9', '0', '0', null, null, '0');
INSERT INTO `xjm_techer` VALUES ('34', '18627099297', 'd69e8f794e64fccf72ce010c2888bfc98ac8edf1', '', '胡培哲', '1', '0', '0.00', '0', '0', '0', '30.565475', '114.352428', '1510652078', null, '1.0.0', 'android', '58.48.223.159', '1,2,3,4,5,6,7,8,9', '1512012511', '1', null, null, '0');
INSERT INTO `xjm_techer` VALUES ('37', '18107214796', 'df5377671d68de9b7df8c14107ef5d63d29ced89', 'f122db007ed655921f98184e4302bba84990ff68', '黄楠', '1', '0', '0.00', '0', '0', '0', '0.000000', '0.000000', '1510889887', null, '1.0', 'share', '113.57.227.211', '1,2,3,4,5,6,7,8,9', '1520993735', '1', '48', 'b2c47ef9d8d088496759f08b3e6d37f3', '1');
INSERT INTO `xjm_techer` VALUES ('39', '15927219779', 'f122db007ed655921f98184e4302bba84990ff68', '', '周月', '1', '1', '0.06', '0', '0', '0', '30.5553', '114.343052', '1511399605', null, '1.0.0', 'android', '59.173.121.158', '1,2,3,4,5,6,7,8,9', '1522400883', '1', null, '69ee20b90becb0495360a02c05a27b9a', '0');
INSERT INTO `xjm_techer` VALUES ('40', '13397140486', '756ec2e11519a9500f35a3a3c6e26bd729b305fa', 'c711277b841b9c16374bdba40b4e2a9be81d0b72', '瑶瑶', '1', '0', '0.00', '0', '0', '0', '30.557888', '114.337468', '1511405255', null, '1.0.0', 'android', '118.31.4.56', '1,2,3,4,5,6,7,8,9', '1512610533', '1', null, 'b0ee3bab6e69f466111004027d2da1d7', '0');
INSERT INTO `xjm_techer` VALUES ('41', '17320567880', '0f0d74bb0eeb3b80ae3b0f8ee357717a9a84e7e7', '0f0d74bb0eeb3b80ae3b0f8ee357717a9a84e7e7', '喝啥酒', '0', '1', '0.00', '0', '0', '0', '', '', '1511408624', null, '1.0.0', 'ios', '58.48.208.10', '1,2,3,4,5,6,7,8,9', '0', '0', '29', null, '0');
INSERT INTO `xjm_techer` VALUES ('44', '15826995634', 'f6c232f6449625ada19aa728ee3220deb0e005d8', '', '刘宽', '1', '1', '0.00', '0', '0', '0', '30.555289', '114.343012', '1511750509', null, '1.0.0', 'share', '58.48.208.10', '1,2,3,4,5,6,7,8,9', '1511854407', '0', '29', 'd217e08f3077d46fb4a7051fb8e89ae9', '0');
INSERT INTO `xjm_techer` VALUES ('45', '13925502374', '914055c9ed0e38db690a8f3ed324a295c3de7bb5', '', '', '0', '1', '0.00', '0', '0', '0', '114.34303', '114.34303', '1511770481', null, '1.0.0', 'android', null, '1,2,3,4,5,6,7,8,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('47', '15007150712', 'fefb58a5314bdf4826fd5614357f921f64ff47cf', '', '唐梦', '0', '1', '0.00', '0', '0', '0', '', '', '1511833520', null, '1.0.0', 'ios', null, '1,2,3,4,5,6,7,8,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('49', '17386077597', 'd9a237dd47c30688f5cb892689c8d5e4bd510ab9', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1511851741', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('50', '13035162070', '68f408d2bafa494fbc781e697eba233b55bfced9', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1511867916', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('51', '18551789135', '25d290ea655a6c42ce89c7b1c4b1e572b5b356c0', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1511939462', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('53', '18971431601', 'b63a177228b896cd14ac2bd4b4b4ab8e53df1e22', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1512024436', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('56', '18271405963', '1d9a15c4d59e74b00c0b363f473125a149c1d422', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1512093422', null, '1.0', 'share', null, null, '0', '0', '1', null, '0');
INSERT INTO `xjm_techer` VALUES ('57', '15823243624', 'c74cbeea796de80667cc1de9af9c44c2432a7def', '', '瞿官顺', '0', '1', '0.00', '0', '0', '0', '', '', '1512202656', null, '1.0.0', 'android', '59.109.107.247', '1,2,3,5,6,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('58', '15172370138', '6149422d37635e1d776302c3881fd4abcd1a5146', 'f122db007ed655921f98184e4302bba84990ff68', '阿卷', '1', '1', '0.00', '0', '0', '0', '30.555371', '114.343061', '1512378369', null, '1.0.0', 'android', '59.174.251.80', '1,2,3,4,5,6,7,8,9', '1514276976', '0', '0', 'e3f0faccc50db58dce3e8087fae6b518', '0');
INSERT INTO `xjm_techer` VALUES ('59', '18206849645', 'f122db007ed655921f98184e4302bba84990ff68', '40c2713714902d76c910a32b681b0290fbcaf6f3', '0', '0', '0', '0.00', '0', '0', '0', '0.000000', '0.000000', '1512380072', null, '1.0.0', 'ios', '58.48.208.10', '9,8,7', '1516606466', '0', '0', '79088736067571e6845b7e0f4e375dbc', '0');
INSERT INTO `xjm_techer` VALUES ('60', '17137214472', 'f122db007ed655921f98184e4302bba84990ff68', '', '宋杰', '0', '1', '0.00', '0', '0', '0', '', '', '1512439819', null, '1.0.0', 'android', null, '1,2,4,6,8', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('61', '13982864067', 'e90a116002b419eb08c7f5ec02636d24fe431b1b', '', '杨辛酉', '0', '1', '0.00', '0', '0', '0', '', '', '1512563447', null, '1.0.0', 'android', '117.173.60.226', '1,6', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('62', '18200634347', 'f122db007ed655921f98184e4302bba84990ff68', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1512613235', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('63', '18680434262', 'f122db007ed655921f98184e4302bba84990ff68', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1512636931', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('64', '15094314768', '7dd383fc1db543161f2b962dce611c6fff8b356f', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1512642875', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('65', '18738472029', '5781f98414bfbb048f19576a49649f4161932bd9', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1512799867', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('66', '18370294187', '82bbb47a4f554434677426b77ebce3c25f967b5c', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1512899067', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('67', '15011347577', '829dc3075f8efde552a4aba344070fe7de55293b', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1512930084', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('68', '15640284473', '38695bf84479ca01bb98b6b8c9bc5498a84bd4f7', '9dc0a388a320b109f3a34901d7774882115e72f1', '贾东泽', '1', '0', '0.00', '0', '0', '0', '', '', '1512974593', null, '1.0.0', 'ios', '58.245.30.228', '1,2,3,4,5,6,7,8,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('69', '18971193603', '1a38cf898cbd21b8b0007888a8fe550ec0e4f859', '', '林健', '1', '0', '0.00', '0', '0', '0', '30.555344', '114.343058', '1513042508', null, '1.0.0', 'android', '58.48.223.159', '1,2,3,4,5,6,7,8,9', '1531724153', '0', '0', 'd73c659572b74ee6ea5e40c5d518f50e', '0');
INSERT INTO `xjm_techer` VALUES ('70', '18976997301', '8e1baeee1109eafbe2ae7025757eeed6fdcc7db9', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1513110817', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('71', '18162519010', '38d6311d81b19ddee585bbcd7ff75da42b7d395c', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1513453371', null, '1.0.0', 'ios', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('72', '15034518927', '6e1fcff8e3906dc08f375d2da5e243b826821ee0', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1513490246', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('73', '15982355804', 'a65c17e08166b5d9b55b5baa59427e737c38d1a6', '', '子健行天涯', '0', '1', '0.00', '0', '0', '0', '', '', '1513612091', null, '1.0.0', 'android', '222.209.59.183', '1,2,3,5,6,7,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('74', '18419952514', '908dfa9969aaff8182ac14b05b3d0035d459512a', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1513753155', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('75', '15210035193', '3f3f6bc979b498a8cbd84dfacf2c3a12abbb559b', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1514041746', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('76', '15164380556', 'f6803794b37dabbe3b5b815b9fda22c949690165', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1514102113', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('77', '13425527650', 'fd0604fe202b3fca6154fd462a5edfca3ed7398f', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1514273817', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('78', '13507136556', '3551865534411dd188e4467d1e9fabed23b262c8', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1514370648', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('79', '18778439839', 'db0442f81d0c4c79b83e0208db47d4309a83d7fa', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1514640950', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('80', '18900856797', 'db237279624af5b7360c0426c56c96c0564b971c', '', '詹雄强', '0', '1', '0.00', '0', '0', '0', '', '', '1514668724', null, '1.0.0', 'android', '14.24.0.119', '1,2,3,8,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('81', '18687870895', 'c75c6249fad718c21e9dccf7c8e89164ea9471e7', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1514916566', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('82', '13511028729', 'acdbc13ec68343ef4292a00941d82452b70e2ccb', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1515656900', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('83', '13088957366', '84e97e52981f7773ea568f2205109d69754c33aa', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1515900363', null, '1.0.0', 'android', null, '1,2,3,5,8', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('84', '15204002225', 'b480eb1b6937629c2127cde005aa3e28b1ef57d2', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1516070020', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('85', '18111946003', 'f538dc56afb6900a231ce62adaec49d86d50e7c3', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1516088520', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('86', '15882145370', '24b68a6d3a1cef1d9e51baf170afc2ca7e43aafd', '', '龚正', '0', '1', '0.00', '0', '0', '0', '', '', '1516343572', null, '1.0.0', 'android', '171.221.135.164', '1,2,3,5,8,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('87', '15916656447', '3a8a075cbfd31f4c93668722002f4fcc470dbdce', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1516380152', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('88', '15221117005', '8e9543a1b82f414ef77abc7fe1beba2a5d63d454', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1516624397', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('89', '18516978984', 'e66a2b4bac05d7ea12845fba40847d52c64bcaec', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1516676734', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('90', '15138829654', 'fdd7210f156009eabe171e182a94b1c246707878', '', '王鹏1987', '0', '1', '0.00', '0', '0', '0', '', '', '1516706967', null, '1.0.0', 'android', '222.140.88.235', '1,3,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('91', '13305215399', '7d36353df8b81de4c3297d328def840107a57919', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1516957659', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('92', '15533057813', '068a692f5eb76ea5c0a078caac1ed184d231731d', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1516981954', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('93', '13596146160', '6306a791fdd8e20d08132bd4373131f2a7062867', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1517059874', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('94', '18166993953', 'f84e42d24938011df1543c4afe69d6cf4bd7c36f', '', '顾如意', '0', '1', '0.00', '0', '0', '0', '', '', '1517108534', null, '1.0.0', 'android', '114.135.229.233', '1,3,6,7,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('95', '15910684836', 'a5fca8b947db61446371b4f9d91f2d021198b416', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1517319346', null, '1.0.0', 'android', null, '1,2,3,4,5,7,8,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('96', '17770008608', 'b9c608ef49d82c3f477bff4454b9eb4952e358d1', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1517672302', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('97', '15070882893', '60882b0cd1c1b48adb74c72da024808b28c28b05', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1518297209', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('98', '13717715806', '5821c902a5e72aa6070b047e728c147b240dc903', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1518932616', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('99', '18612121312', 'acb7d609738e76ec7a8ac64a5ef91af6ff521f8a', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1519579640', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('100', '15137768608', '3c14101006389d6c9d621303c65f8e80be566384', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1519709832', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('101', '15927492341', '919c990dc73da0200f516e8236a3817d60a7e897', '', '吴海涛', '0', '1', '0.00', '0', '0', '0', '', '', '1520277747', null, '1.0.0', 'android', '59.174.176.56', '3', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('102', '18353666913', 'ac402d490571cb8e60bf46821065edb642edcf34', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1520477399', null, '1.0.0', 'android', null, '1', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('103', '17778411057', 'c3c200d4f5030803298a533a3f824b21d0ab7de9', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1520692510', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('104', '18500835172', '7c65440bac8c48c6b3fbdbe1e09bc13958d3c10b', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1521034922', null, '1.0.0', 'android', null, '1,2,3,5,6,8,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('105', '15619068108', '6d92cf391f0c0f2df1ea9003bf17cf75b7b803d7', '', '京派特级修脚师', '0', '1', '0.00', '0', '0', '0', '', '', '1521200131', null, '1.0.0', 'android', '113.200.106.22', '1,2,3,6,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('106', '18017279373', 'c6854c84fdf2e1e9e81a63410a19e3fbec086986', '', '杨宏凯', '0', '1', '0.00', '0', '0', '0', '', '', '1521324311', null, '1.0.0', 'android', '101.81.120.83', '3,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('107', '13148126001', '22d538d48f78fdb6c329fec1a4e22f7b53547f7a', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1521350877', null, '1.0.0', 'android', null, '3,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('108', '17560381988', 'db12d002340fa8cfaea27d1fd55d5c830adcd2bc', '', '贾乾强', '0', '1', '0.00', '0', '0', '0', '', '', '1521479939', null, '1.0.0', 'android', '60.10.133.94', '1,2,3,5,6,7,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('109', '13598479564', '1577251e663a78b7ce3cb9f5792ee59d3e39c914', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1521706991', null, '1.0.0', 'android', null, '1,2,6', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('110', '17081279850', '4e5e4e350253a56000dea81d6610c79b56150c00', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1521710663', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('111', '13215689262', '5830261c9c0dabaa0ae8dd1fc543a74d0f4b18ff', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1522087362', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('112', '17710594736', 'ffbd276a488805d2b3bcd560da95a4f232046513', '', '赵鹤祥', '0', '1', '0.00', '0', '0', '0', '', '', '1522496741', null, '1.0.0', 'android', '223.104.3.247', null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('113', '18344120948', 'e726c50b0dface1f093284c4812ec6420f6a9b88', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1522682729', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('114', '18501996874', 'fc076b5dc77515267aaa5dd8d4cd6849da68381f', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1522810041', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('115', '17345831511', '3d4862aafddd4db60abc699f2c7b6bddd69dd5ed', '', '熊世章', '0', '1', '0.00', '0', '0', '0', '', '', '1523101246', null, '1.0.0', 'android', '222.209.244.69', '1,2,3,5,7,8,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('116', '13693448475', '7f0d3d382676d3417f46f33423fc2f43140a18b4', '', '龚连技', '0', '1', '0.00', '0', '0', '0', '', '', '1523181790', null, '1.0.0', 'android', '118.112.106.166', '2,3,4,5,8,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('117', '17610291916', '75082c6c73562f4d748b1dfee24c3797920e9592', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1523380700', null, '1.0.0', 'android', null, '1,2,3,5,6,7,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('118', '17633601268', 'd63cbb6fb31129f3c4218996cc90c2b4cfc3b01d', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1523618013', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('119', '18537797996', 'e10532785b6869f2d09080e2679af09b1212a022', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1523977708', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('120', '13488776904', 'f6dc938d8cf41c8ab5f76586bc4467fc0480845d', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1524124845', null, '1.0.0', 'android', null, '1,3,6', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('121', '15901985152', 'c69d4a40057a04a55f15dfe2e1fa64bc6c8355c5', 'b77befcae3a0fc79269a7fe5d6206298aba62df3', '俞跃新', '0', '1', '0.00', '0', '0', '0', '', '', '1524478739', null, '1.0.0', 'android', '116.224.219.184', '3,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('122', '15974645018', 'de3ee268a9bda1cefaddef5180a10f1de6a8d835', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1524784310', null, '1.0.0', 'android', null, '6,7', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('123', '18696160056', '7fd4f56242aa3c83a1ae9cfa68e2fec49e2124d3', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1525340901', null, '1.0.0', 'ios', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('124', '15250960125', 'ab386c87c4fc87ddfb7e94d0dc7327e3ddc90048', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1525449499', null, '1.0.0', 'ios', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('125', '13650792069', '9e8e2f7e2a458079e55eac058534d575e91f3712', '', '胡贵红', '0', '1', '0.00', '0', '0', '0', '', '', '1525545670', null, '1.0.0', 'android', '222.87.27.146', null, '0', '0', '0', '4c551211b0033102bd4ab59e394399ed', '0');
INSERT INTO `xjm_techer` VALUES ('126', '15217615740', '1e9b1b5bc38023a3ecfa660de6602797638d6c95', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1525618598', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('127', '15176729923', '498c45f2720721bf9ba47a413be64acde413cd4b', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1526024908', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('128', '18310794485', 'a9770cc0839838983ac464cba8149bdf4c2ba0a7', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1526143010', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('129', '13126686833', 'e4fbfa08d7812c7090c49fa4b2f57d8ae9e1882a', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1526643769', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('130', '15298764221', '8047837914b3fe7c43062840a39530baa7a5ec09', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1527603095', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('131', '13311084766', '00a18e3697e67196df45d7d7530e7d905664e9e4', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1527896933', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('132', '15057763774', '878129bdc329955e2c62d864d6376d8ae0620931', '', '郭正洪', '0', '1', '0.00', '0', '0', '0', '', '', '1528059174', null, '1.0.0', 'android', '117.136.72.131', '2,3,5,7,8,9', '0', '0', '0', 'a1a0aee00de433e2c514427f91bdd818', '0');
INSERT INTO `xjm_techer` VALUES ('133', '15293485208', 'd77bad92e7bcf96b534192ec1b99c9e77255add0', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1528356992', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('134', '15923008686', '909e246a5c453619d7960784cd1bc0cd6c3a1050', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1528595275', null, '1.0.0', 'android', null, '1,2', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('135', '13852252930', '10ac7a072172c691fc5b43daf930eb0c7c44eadc', '', '于艾俊', '0', '1', '0.00', '0', '0', '0', '', '', '1528608748', null, '1.0.0', 'android', '118.31.4.56', '6', '0', '0', '0', '084b3f85dda176c81700d1baa577b0cc', '0');
INSERT INTO `xjm_techer` VALUES ('136', '13588354185', 'e88cd57a4a79abaa4a056bc73b4e9543e142e99f', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1528680024', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('138', '13076949806', 'f122db007ed655921f98184e4302bba84990ff68', '', '琳琳', '0', '1', '0.00', '0', '0', '0', '', '', '1528780692', null, '1.0', 'wap', '118.31.4.56', '1', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('139', '18679418139', 'd236e41c0cb4b68f8bf8719574df0da206cdc220', '', '郑敏', '0', '1', '0.00', '0', '0', '0', '', '', '1528955494', null, '1.0.0', 'android', '211.162.237.27', '1,3,6', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('140', '15189732555', 'd8896e87c6addcaf65da1537752c5237c900df10', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1529137799', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('141', '13415533109', 'a763696a339c249a3636371335f6446c25e1c3f1', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1529193589', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('142', '15923884498', 'f122db007ed655921f98184e4302bba84990ff68', '', '李嫩晓', '0', '1', '0.00', '0', '0', '0', '', '', '1529360420', null, '1.0.0', 'android', '183.226.182.213', '2,3,5,8,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('143', '13552292105', '99cda49c58904585cb05759d5b7a095e32407b45', '81e52c5f573f23ba62ec18451ba5d91b7706152b', '赵伍卿', '0', '1', '0.00', '0', '0', '0', '', '', '1529507839', null, '1.0.0', 'android', '115.33.4.70', '1,3,5,9', '0', '0', '0', '9c2dd33f3ed254675174787eda640a2f', '0');
INSERT INTO `xjm_techer` VALUES ('144', '15036032523', '917ba58b8a6ffc093b9fea9553a2bba32e40058d', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1529953938', null, '1.0.0', 'android', null, '1,2,3,4,5,6,7,8,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('145', '18116543307', 'd89911e69b449fa7cc67044d981faaf1c4ab4a5d', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1530130688', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('146', '17310329326', 'd8ee6a9a3409231c15bbc2adf182d49eeb3fb554', '', '李秋姗', '0', '1', '0.00', '0', '0', '0', '', '', '1530183954', null, '1.0.0', 'android', '111.202.66.27', '1,2,3,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('147', '13040808584', 'f32925ae654cfbb1d6fd1591ebcd2ccf3e4efc8d', '', '陈晓龙', '0', '1', '0.00', '0', '0', '0', '', '', '1530839547', null, '1.0.0', 'android', '120.234.27.206', '1,3,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('148', '18983928036', '909e246a5c453619d7960784cd1bc0cd6c3a1050', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1530844580', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('149', '18098740178', '98fcc6c8c725399b1b23e10c410dc9dfda7a7dd5', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1530956242', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('150', '15760325044', 'bc2b300b82e89b0057538e09a125dee2fa12de65', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1531072595', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('151', '15297962716', '7f003f0db756acc6df90e1c9907fbf46b6eee194', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1531779952', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('152', '13101653075', '766b7ac141e5dc613458aeb3b16825ef2aaa41b3', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1531850194', null, '1.0.0', 'android', null, '1', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('153', '18653067990', 'e7699b523003a3194f503d368f52cc6aca834e86', '', '贺付帅', '0', '1', '0.00', '0', '0', '0', '', '', '1532102078', null, '1.0.0', 'android', '144.52.186.112', '1,2,3,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('154', '13581007197', '5b8c3ac2d3b03c5582e883571c04af22af9325bd', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1532153335', null, '1.0.0', 'android', null, '1,2,3,5,6,8,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('155', '15110210832', '4bd21684accf0bf233179b538e680a82e51e83d0', 'cc33322d758a54d945183d1f726d443e1ec2740a', '罗游进', '0', '1', '0.00', '0', '0', '0', '', '', '1532193871', null, '1.0.0', 'android', '223.104.3.199', '2,3,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('156', '13925962501', '430bad924e44e12770d302ca4369c715c62de7a8', '', '龚伟', '0', '1', '0.00', '0', '0', '0', '', '', '1532334611', null, '1.0.0', 'android', '223.104.63.153', '1,2,3,5,6,7,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('157', '13130732678', '620e476db744e59649724666a5bed676479a0738', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1533745989', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('158', '15383216919', '9d40e0b7001591c7be04f89e3cd9f835fb9966ed', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1533911078', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('159', '15594515587', 'bdcecf3fdd666ba8607426293484f2f301e1a06b', '', '周谋', '0', '1', '0.00', '0', '0', '0', '', '', '1535392015', null, '1.0.0', 'android', '113.200.106.2', '1,3,5,6', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('160', '18606297924', '54a7ab52494e2e8c966281d38350af5bc4bba48e', '', '陈兴桥', '0', '1', '0.00', '0', '0', '0', '', '', '1535578008', null, '1.0.0', 'android', '183.194.169.144', '1,2,3,5,6,8,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('161', '14781738500', 'b181f46b0daeb7c94e59516e93c90e9851c5307a', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1535725959', null, '1.0.0', 'android', null, '1,2,3,5', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('162', '15135792695', '3ba137743b121519e2aacc769a85e588a5d46c99', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1535976457', null, '1.0.0', 'android', null, '6,7', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('163', '13641452026', '99204b5a716a16a93bb2fc323eee08c98fb4dc4b', '', '曾婷英', '0', '1', '0.00', '0', '0', '0', '', '', '1536046966', null, '1.0.0', 'android', '113.89.7.23', '1', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('164', '17688677379', '0477b4091dfb73028f987d6c7861c31d75a1c9c4', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1536077748', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('165', '13863869881', 'f3074542098a69710ef1abba1faf67e3daf43b09', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1536218957', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('166', '15219094938', '08a70d22150b8a6b2d01a185ec49f4eb5cbd8817', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1536292337', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('167', '15161874400', '27894f21713108bf6d0764231621a151144fa379', '', '小德子', '0', '1', '0.00', '0', '0', '0', '', '', '1536306378', null, '1.0.0', 'android', '223.104.147.9', '1,2,3,5,6,9', '0', '0', '0', 'cc1488243f3724ab8cd7819a1c4ca4b2', '0');
INSERT INTO `xjm_techer` VALUES ('168', '17820322001', '95836c275497d540e404f8f33e9a850c2e51b733', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1536484270', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('169', '15715692352', '357420825cf0c90099e07677c45c2fa15245f7b9', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1536564219', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('170', '17301073675', 'e71f12341fb77113b102ad16b437142b9a54ed72', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1536781097', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('171', '18829544580', 'f122db007ed655921f98184e4302bba84990ff68', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1537024680', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('172', '15769500213', 'faa2c442a31f141fbc51699996affc54f99287b6', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1537200048', null, '1.0.0', 'android', null, '1,6', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('173', '13048089102', '353168461f64d2345944c6f17c13b90140aba850', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1537277308', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('174', '15952708735', '7ae4a61885762a5cb448ed41c4724015161f85de', '', '戚伟', '0', '1', '0.00', '0', '0', '0', '', '', '1537449182', null, '1.0.0', 'android', '117.91.45.251', '1,3,6', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('175', '18769766675', '35ec80cb7bc4c87754f417185c0bd1c891af61e6', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1537455273', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('176', '13183800685', 'faad343a82d8a76bb8b1ff74fd56aec15be1479a', '', '刘凯', '0', '1', '0.00', '0', '0', '0', '', '', '1537711826', null, '1.0.0', 'android', '118.31.4.56', '1,2,3,5,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('177', '18924104499', 'd1c302ff7facc7b17b2671c568e4e5f0a90be3e9', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1537859488', null, '1.0.0', 'android', null, '2', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('178', '13802547394', '660c48a6f9481c537e5cb402b8b4e65c4417fd8c', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1542981931', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('179', '18976424080', 'e5813fb57c4cf1b3491a67ba4e69dcae35b802e6', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1543391187', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('180', '15705686371', 'ad308c4e15ceebac9c7e1501185de2a58b1e888f', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1543758485', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('181', '13291000530', '7077801fdaaf572b124db41d6720525180c4029a', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1543829815', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('182', '18535853166', '182f1fea865375380a3a81c5031e0020ba18fba8', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1543849374', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('183', '18675605365', '7e546ccf306a83e0b84994f2362889f97427dcf9', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1543946981', null, '1.0.0', 'ios', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('184', '19806586787', 'dea742e166979027ae70b28e0a9006fb1010e760', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1544060384', null, '1.0.0', 'ios', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('185', '17610771953', '37b60254289de030ca54591a59f54a31b640977a', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1544071003', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('186', '15959950655', '5ae0ba9bb0d487af8885541a5e52b4e0c1f51512', '', '丁志忠', '0', '1', '0.00', '0', '0', '0', '', '', '1544101343', null, '1.0.0', 'android', '110.88.204.151', '1,4', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('187', '13828830924', '3d91a13a5240d6f1626026687a126d6097ec89ef', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1544163337', null, '1.0.0', 'ios', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('188', '13543328468', '10f0fd6e89bef161f35cb98a27a1d36082e34af7', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1544164872', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('189', '13861178264', 'ca80e07e3fdd73b2593e0c854d855415b8225de5', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1544195243', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('190', '13554141532', 'b5ec3e2c0b649b19b4ad2461dcc1e1f09bc0d434', '9faae1fa7a62729f90248569c7232d5098d320ad', '爱奔跑的猪', '0', '1', '0.00', '0', '0', '0', '', '', '1544755508', null, '1.0.0', 'ios', '27.18.80.30', '1', '0', '0', '0', 'b59eecfddd41998eca62239beb72f571', '0');
INSERT INTO `xjm_techer` VALUES ('191', '17671448546', 'd47175b2b9d2a39180ee2b362429186c6edabefc', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1544764306', null, '1.0.0', 'ios', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('192', '15907150581', '2da0c1199d0a45db416b56284d4b73f28444c221', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1544765238', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('193', '15210970627', '8e1baeee1109eafbe2ae7025757eeed6fdcc7db9', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1544804197', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('194', '17730415891', '506056c37f960caf7076b4566a3f341a6c193ff3', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1544809766', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('197', '13626320133', '1a4461d09116d04cd291ed60fab060c110821b3c', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1545744094', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('198', '18566732867', '616ccb6547968eea79cc74837b26b797670f6b51', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1545859215', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('200', '15927187854', '7c4a8d09ca3762af61e59520943dc26494f8941b', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1545960555', null, '1.0.0', 'ios', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('201', '17707267486', 'f122db007ed655921f98184e4302bba84990ff68', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1545961820', null, '1.0', 'wap', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('202', '18571732298', 'f122db007ed655921f98184e4302bba84990ff68', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1545962023', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('204', '17720588195', 'f122db007ed655921f98184e4302bba84990ff68', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1545965531', null, '1.0', 'wap', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('205', '13164604526', 'f122db007ed655921f98184e4302bba84990ff68', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1545978623', null, '1.0', 'wap', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('206', '18339280527', '94687d664932f2dcb16393bbf1b5e585c1038eb0', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1546190599', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('208', '18893848102', '8e7f83892904bd3214320c7b20f735f56877ea98', '', '陈兰', '0', '1', '0.00', '0', '0', '0', '', '', '1546571228', null, '1.0.0', 'android', '42.91.138.141', '1,2,3,5,8', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('209', '17625576274', '7c4a8d09ca3762af61e59520943dc26494f8941b', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1546581444', null, '1.0.0', 'ios', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('210', '13354305788', '0a202ba6a10491cbf0fd4f4a830bd070bd5d6037', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1546611758', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('217', '17671746469', '48da8d89b27b6212407191db9061e10c3472c89d', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1546671862', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('218', '18571710546', 'f122db007ed655921f98184e4302bba84990ff68', '4a9248f1f73c0023bfa3b56c887efa5ec30cfa0a', '张伟', '0', '1', '0.00', '0', '0', '0', '', '', '1546671874', null, '1.0.0', 'ios', '183.95.250.171', '9,8,7,6,5,4,3,2,1', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('225', '18521598723', 'cc48ad473c166e078b0f863f7998546a9ed0792a', '', '何某某', '1', '1', '0.00', '0', '0', '0', '30.620397', '114.290585', '1546675854', null, '1.0.0', 'android', '223.209.235.3', '4', '0', '0', '0', '1b1c8a2403a133ebeee64919b8331543', '0');
INSERT INTO `xjm_techer` VALUES ('226', '13048084220', '80bc3331f93a1ef401df978aba5c9079fd74bd81', '', '李然', '0', '1', '0.00', '0', '0', '0', '', '', '1546777451', null, '1.0.0', 'android', null, '2', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('227', '13263378183', '8a607c88e9f5967898089051c73cb8173ba0a2d8', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1546783151', null, '1.0.0', 'android', null, '3', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('232', '18627056710', '8bb5c56dc6f5202ea0d6b34b3874c1b4b1a0765b', '5e00261244142abace52457c6522d1d8455cfa2b', 'gaoxiaobo', '0', '1', '0.00', '0', '0', '0', '', '', '1546828852', null, '1.0.0', 'ios', '59.175.99.5', '3,4,5,6', '0', '0', '0', 'ad51d86a3b3d2e1aab90ed407b03e7eb', '0');
INSERT INTO `xjm_techer` VALUES ('233', '18884175413', 'faad343a82d8a76bb8b1ff74fd56aec15be1479a', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1547089567', null, '1.0', 'wap', null, '1,2,3,5,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('234', '18054855123', 'd950a28dae8fe921df8c9aa8f713904d0c8ab8c9', '', '阮芳荣', '0', '1', '0.00', '0', '0', '0', '', '', '1547350377', null, '1.0.0', 'android', '140.243.136.20', '1,2,3,5,6,7,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('235', '13760184176', '6cd8cc40e7414a5d1b754e9642b675494a3bb0b7', '', '易花女', '0', '1', '0.00', '0', '0', '0', '', '', '1547379444', null, '1.0.0', 'android', null, '2,3,4,9', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('236', '13412332471', '6cd8cc40e7414a5d1b754e9642b675494a3bb0b7', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1547380525', null, '1.0.0', 'android', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('237', '14704175446', 'ba20539881cca8ad07fbde0199e9736b8eaa5949', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1547543889', null, '1.0.0', 'android', null, '', '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('238', '15542859409', '14dac0dd989ce3e18a1d68edf5d39dffc35cb946', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1547780610', null, '1.0.0', 'ios', null, null, '0', '0', '0', null, '0');
INSERT INTO `xjm_techer` VALUES ('239', '13163497912', '9a178aa52704ba47a2a8246e4935c48c3574d832', '', '', '0', '1', '0.00', '0', '0', '0', '', '', '1547869859', null, '1.0.0', 'android', null, '1,2,5,6', '0', '0', '0', null, '0');

-- ----------------------------
-- Table structure for xjm_techer_cash
-- ----------------------------
DROP TABLE IF EXISTS `xjm_techer_cash`;
CREATE TABLE `xjm_techer_cash` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tid` int(11) DEFAULT '0' COMMENT '关联技师表主键id',
  `type` tinyint(1) DEFAULT '0' COMMENT '类型 1：支付宝 2：微信',
  `cash_account` varchar(30) DEFAULT '' COMMENT '提现账号',
  `add_time` int(10) DEFAULT '0' COMMENT '添加时间',
  `cash_money` decimal(10,2) DEFAULT NULL COMMENT '提现金额',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0审核中，1审核通过，2驳回',
  `admin_user` int(11) NOT NULL DEFAULT '0' COMMENT '后台操作员id',
  `set_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `admin_user` (`admin_user`) USING BTREE,
  KEY `tid` (`tid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='技师提现账户表';

-- ----------------------------
-- Records of xjm_techer_cash
-- ----------------------------
INSERT INTO `xjm_techer_cash` VALUES ('1', '31', '1', '15927219779', '1511322363', '60.01', '1', '16', '1511344074');
INSERT INTO `xjm_techer_cash` VALUES ('2', '27', '1', '834229969@qq.com', '1511333747', '50.04', '1', '16', '1511344062');
INSERT INTO `xjm_techer_cash` VALUES ('3', '38', '1', '17778079115', '1511341342', '50.00', '1', '16', '1511341567');
INSERT INTO `xjm_techer_cash` VALUES ('4', '40', '1', '834229969@qq.com', '1511833989', '300.00', '2', '16', '1513822685');
INSERT INTO `xjm_techer_cash` VALUES ('5', '40', '1', '834229969@qq.com', '1512443626', '100.00', '1', '16', '1513822677');

-- ----------------------------
-- Table structure for xjm_techer_checkinfo
-- ----------------------------
DROP TABLE IF EXISTS `xjm_techer_checkinfo`;
CREATE TABLE `xjm_techer_checkinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) unsigned NOT NULL COMMENT '用户id',
  `truename` char(15) DEFAULT '' COMMENT '真实姓名',
  `card` varchar(20) DEFAULT '' COMMENT '身份证号',
  `place` varchar(20) DEFAULT '' COMMENT '籍贯',
  `sex` tinyint(1) DEFAULT '0' COMMENT '1:男 2：女',
  `birthday` int(10) DEFAULT '0' COMMENT '出生年月',
  `jiguan` varchar(20) DEFAULT '' COMMENT '籍贯',
  `desc` mediumtext COMMENT '简介',
  `headurl` varchar(150) DEFAULT '' COMMENT '头像地址',
  `before_cardurl` varchar(150) DEFAULT '' COMMENT '身份证正面',
  `after_cardurl` varchar(150) DEFAULT '' COMMENT '身份证反面',
  `hand_cardurl` varchar(150) DEFAULT '' COMMENT '手持身份证',
  `certurl` varchar(150) DEFAULT '' COMMENT '资格证书',
  `health_url` varchar(150) DEFAULT '' COMMENT '健康证',
  `card_success` tinyint(1) DEFAULT '0' COMMENT '0未验证，1已通过，2未通过',
  `idcard_zheng` varchar(255) DEFAULT NULL COMMENT '身份证正面图',
  `idcard_fan` varchar(255) DEFAULT NULL COMMENT '身份证反面图',
  `headurl_status` tinyint(1) DEFAULT '0' COMMENT '头像审核状态：0未审核，1已通过，2未通过',
  `keyong_headurl` varchar(150) DEFAULT NULL COMMENT '可用头像地址',
  PRIMARY KEY (`id`),
  KEY `card` (`card`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=utf8 COMMENT='技师信息审核表';

-- ----------------------------
-- Records of xjm_techer_checkinfo
-- ----------------------------
INSERT INTO `xjm_techer_checkinfo` VALUES ('1', '1', '谢谢', '42122112', '湖北', '1', '1442460022', '', '个性签名', 'http://pic.xijiaomo.com/avater9.jpg', '', '', '', '', '', '2', null, null, '1', 'http://pic.xijiaomo.com/avater12.jpg');
INSERT INTO `xjm_techer_checkinfo` VALUES ('2', '2', '李琴', '5487542155454', '', '1', '1505617902', '', null, 'http://pic.xijiaomo.com/avater1.jpg', '', '', '', '', '', '0', null, null, '1', 'http://pic.xijiaomo.com/avater13.jpg');
INSERT INTO `xjm_techer_checkinfo` VALUES ('3', '3', '张涛', '4254554545545545454', '', '1', '1442460022', '', null, 'http://pic.xijiaomo.com/avater2.jpg', '', '', '', '', '', '2', null, null, '1', 'http://pic.xijiaomo.com/6.jpg');
INSERT INTO `xjm_techer_checkinfo` VALUES ('5', '13', '双子', '420222199001052071', '', '1', '1505714515', '', '我懂', 'http://pic.xijiaomo.com/avater3.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('6', '14', '黄卫', '420222199001052071', '', '2', '1505714515', '', '', 'http://pic.xijiaomo.com/avater4.jpg', '', '', '', '', '', '0', null, null, '1', 'http://pic.xijiaomo.com/2.jpg');
INSERT INTO `xjm_techer_checkinfo` VALUES ('9', '17', '陈宾鹏', '429004199206092952', '', '1', '1505714515', '', '不知道', 'http://pic.xijiaomo.com/avater5.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('10', '20', '黄楠', '420106199305304417', '', '1', '1505714515', '', '3333', 'http://pic.xijiaomo.com/avater6.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('11', '21', '阿麦', '420106199305304413', '', '2', '1505714515', '', null, 'http://pic.xijiaomo.com/avater7.jpg', '', '', '', '', '', '0', null, null, '1', 'http://pic.xijiaomo.com/9.jpg');
INSERT INTO `xjm_techer_checkinfo` VALUES ('12', '22', '宋波', '420106199305304416', '', '2', '1505714515', '', null, 'http://pic.xijiaomo.com/avater8.jpg', '', '', '', '', '', '2', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('13', '23', '阿信', '', '', '2', '150155222', '', null, 'http://pic.xijiaomo.com/avater9.jpg', '', '', '', '', '', '2', null, null, '1', 'http://pic.xijiaomo.com/3.jpg');
INSERT INTO `xjm_techer_checkinfo` VALUES ('23', '26', '张年春', '429005199102184737', '', '1', '150155222', '', '要不  给你喊个技术  给揉揉吧', 'http://pic.xijiaomo.com/avater1.jpg', '', '', '', '', '', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2620171102095324', 'http://pic.xijiaomo.com/android_xjm_userphoto2620171102095351', '1', 'http://pic.xijiaomo.com/2.jpg');
INSERT INTO `xjm_techer_checkinfo` VALUES ('31', '34', '胡培哲', '42010619900409401X', '', '1', '1416133681', '', '', 'http://pic.xijiaomo.com/avater2.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto3420171116182713.png', 'http://pic.xijiaomo.com/android_xjm_userphoto3420171116182718.png', '1', 'http://pic.xijiaomo.com/wo2.png');
INSERT INTO `xjm_techer_checkinfo` VALUES ('32', '37', '小黄', '420106199305304412', '', '1', '1509465600', '', '', 'http://pic.xijiaomo.com/avater3.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/5e1bquy0kye.png', 'http://pic.xijiaomo.com/Fq_O8nIh09hS-5H4Us0HNEUuhm3s', '1', 'http://pic.xijiaomo.com/wo3.png');
INSERT INTO `xjm_techer_checkinfo` VALUES ('33', '38', '琳琳', '420322199301110662', '', '2', '1258877415', '', '', 'http://pic.xijiaomo.com/avater4.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto3820171122160910.png', 'http://pic.xijiaomo.com/android_xjm_userphoto3820171122160918.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('34', '39', '周大大', '429001199512265162', '', '1', '1479863659', '', '', 'http://pic.xijiaomo.com/avater5.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto3920171123091359.png', 'http://pic.xijiaomo.com/android_xjm_userphoto3920171123091406.png', '1', 'http://pic.xijiaomo.com/2.jpg');
INSERT INTO `xjm_techer_checkinfo` VALUES ('35', '40', '陈瑶瑶', '420822199308155780', '', '1', '745382946', '', '', 'http://pic.xijiaomo.com/avater6.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto4020171123104807.png', 'http://pic.xijiaomo.com/android_xjm_userphoto4020171123104815.png', '1', 'http://pic.xijiaomo.com/1.jpg');
INSERT INTO `xjm_techer_checkinfo` VALUES ('38', '41', '喝啥酒', '429004198707082299', '', '2', '0', '', '6,4,3,2,1', 'http://pic.xijiaomo.com/avater7.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/20171123163936.jpg', 'http://pic.xijiaomo.com/20171123163941.jpg', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('39', '44', '刘宽', '420703199207033376', '', '1', '1511750597', '', '', 'http://pic.xijiaomo.com/avater8.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto4420171127104233.png', 'http://pic.xijiaomo.com/android_xjm_userphoto4420171127104238.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('40', '47', '唐梦', '429006199504150922', '', '2', '1995', '', '2,1,3,4', 'http://pic.xijiaomo.com/avater9.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/20171128095159.jpg', 'http://pic.xijiaomo.com/20171128095208.jpg', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('43', '57', '瞿官顺', '500231198911127951', '', '1', '626861974', '', '', 'http://pic.xijiaomo.com/avater1.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto5720171202161840.png', 'http://pic.xijiaomo.com/android_xjm_userphoto5720171202161859.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('45', '59', '胡品', '421083199005111617', '', '1', '19900511', '', '4,6,5,3,2', 'http://pic.xijiaomo.com/avater2.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/20171204192814.jpg', 'http://pic.xijiaomo.com/20171204192823.jpg', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('46', '58', '阿卷', '429006199403214413', '', '1', '795780666', '', '', 'http://pic.xijiaomo.com/avater3.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto5820171204181025.png', 'http://pic.xijiaomo.com/android_xjm_userphoto5820171204181033.png', '0', 'http://pic.xijiaomo.com/5.jpg');
INSERT INTO `xjm_techer_checkinfo` VALUES ('47', '60', '宋杰', '420115199503033218', '', '1', '744647435', '', '', 'http://pic.xijiaomo.com/avater4.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto6020171206223006.png', 'http://pic.xijiaomo.com/android_xjm_userphoto6020171206223014.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('48', '61', '杨辛酉', '513021197703287754', '', '1', '228400333', '', '', 'http://pic.xijiaomo.com/avater5.jpg', '', '', '', '', '', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto6120171206203457.png', 'http://pic.xijiaomo.com/android_xjm_userphoto6120171206203510.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('49', '68', '贾东泽', '210921199609178812', '', '1', '19960917', '', '3,4,2,1', 'http://pic.xijiaomo.com/avater6.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/20171211144509.jpg', 'http://pic.xijiaomo.com/20171211144516.jpg', '0', 'http://pic.xijiaomo.com/3.jpg');
INSERT INTO `xjm_techer_checkinfo` VALUES ('50', '69', '林健', '4203221992011033265', '', '1', '726629885', '', '', 'http://pic.xijiaomo.com/avater7.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto6920171212093636.png', 'http://pic.xijiaomo.com/android_xjm_userphoto6920171212093645.png', '1', 'http://pic.xijiaomo.com/7.jpg');
INSERT INTO `xjm_techer_checkinfo` VALUES ('51', '73', '桂善彬', '511025197008123452', '', '1', '19271178', '', '', 'http://pic.xijiaomo.com/avater8.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto7320171219090416.png', 'http://pic.xijiaomo.com/android_xjm_userphoto7320171219090445.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('52', '79', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater9.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('53', '82', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater1.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('54', '86', '龚正', '510131197710263414', '', '1', '215159847', '', '', 'http://pic.xijiaomo.com/avater2.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto8620180119143559.png', 'http://pic.xijiaomo.com/android_xjm_userphoto8620180119143613.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('55', '80', '詹雄强', '440923197909110058', '', '1', '274330520', '', '', 'http://pic.xijiaomo.com/avater3.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto8020180120105621.png', 'http://pic.xijiaomo.com/android_xjm_userphoto8020180120105715.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('56', '90', '王程弘', '410881198711065535', '', '1', '531660738', '', '', 'http://pic.xijiaomo.com/avater4.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto9020180123193133.png', 'http://pic.xijiaomo.com/android_xjm_userphoto9020180123193139.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('57', '93', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater5.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('58', '94', '顾如意', '320382199102189438', '', '1', '666846330', '', '', 'http://pic.xijiaomo.com/avater6.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto9420180128110422.png', 'http://pic.xijiaomo.com/android_xjm_userphoto9420180128110445.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('59', '97', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater7.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('60', '101', '吴海涛', '420323199412064116', '', '1', '755119498', '', '', 'http://pic.xijiaomo.com/avater8.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto10120180306032316.png', 'http://pic.xijiaomo.com/android_xjm_userphoto10120180306032334.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('61', '102', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater9.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('62', '104', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater1.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('63', '105', '张金柱', '622824198909171871', '', '1', '622035651', '', '', 'http://pic.xijiaomo.com/avater2.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto10520180316193941.png', 'http://pic.xijiaomo.com/android_xjm_userphoto10520180316193957.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('64', '106', '杨宏凯', '610331197212220010', '', '1', '93849865', '', '', 'http://pic.xijiaomo.com/avater3.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto10620180318132235.png', 'http://pic.xijiaomo.com/android_xjm_userphoto10620180318132326.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('65', '107', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater4.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('66', '108', '贾乾强', '371523198811281735', '', '1', '596654638', '', '', 'http://pic.xijiaomo.com/avater5.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto10820180320012305.png', 'http://pic.xijiaomo.com/android_xjm_userphoto10820180320012325.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('67', '109', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater6.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('68', '110', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater7.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('69', '112', '赵鹤祥', '513101197211075237', '', '1', '124199390', '', '', 'http://pic.xijiaomo.com/avater8.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto11220180331194806.png', 'http://pic.xijiaomo.com/android_xjm_userphoto11220180331194817.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('70', '113', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater9.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('71', '115', '熊世章', '510131197112120034', '', '1', '61386641', '', '', 'http://pic.xijiaomo.com/avater1.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto11520180407194801.png', 'http://pic.xijiaomo.com/android_xjm_userphoto11520180407194849.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('72', '119', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater2.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('73', '121', '俞跃新', '310223196411032919', '', '1', '-162739669', '', '', 'http://pic.xijiaomo.com/avater3.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto12120180423183036.png', 'http://pic.xijiaomo.com/android_xjm_userphoto12120180423183050.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('74', '124', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater4.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('75', '125', '胡贵红', '522221198410074321', '', '2', '465936380', '', '', 'http://pic.xijiaomo.com/avater5.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto12520180506024916.png', 'http://pic.xijiaomo.com/android_xjm_userphoto12520180506024929.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('76', '128', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater6.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('77', '116', '龚连技', '510131197509153424', '', '2', '180010167', '', '', 'http://pic.xijiaomo.com/avater7.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto11620180514184835.png', 'http://pic.xijiaomo.com/android_xjm_userphoto11620180514184842.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('78', '131', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater8.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('79', '132', '郭正洪', '530427199602012137', '', '1', '823121821', '', '', 'http://pic.xijiaomo.com/avater9.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto13220180604045609.png', 'http://pic.xijiaomo.com/android_xjm_userphoto13220180604045630.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('80', '135', '于艾俊', '320882197711052013', '', '1', '247556171', '', '', 'http://pic.xijiaomo.com/avater1.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto13520180610133504.png', 'http://pic.xijiaomo.com/android_xjm_userphoto13520180610133519.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('81', '136', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater2.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('82', '138', '琳琳', '420322199201103326', '', '1', '708278400', '', '', 'http://pic.xijiaomo.com/avater3.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/x6qba9zw65q.png', 'http://pic.xijiaomo.com/FqbcTIFljudBP6sBFWlEsdIliM1q', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('83', '139', '郑敏', '362523199111200811', '', '1', '690652713', '', '', 'http://pic.xijiaomo.com/avater4.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto13920180614235749.png', 'http://pic.xijiaomo.com/android_xjm_userphoto13920180614235800.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('84', '141', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater5.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('85', '142', '李嫩晓', '410381197310196524', '', '2', '119831047', '', '', 'http://pic.xijiaomo.com/avater6.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto14220180619062242.png', 'http://pic.xijiaomo.com/android_xjm_userphoto14220180619062258.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('86', '143', '赵伍卿', '132135197806261719', '', '1', '898874451', '', '', 'http://pic.xijiaomo.com/avater7.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto14320180620231937.png', 'http://pic.xijiaomo.com/android_xjm_userphoto14320180620231953.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('87', '145', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater8.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('88', '146', '李秋姗', '120225198811293946', '', '2', '596804891', '', '', 'http://pic.xijiaomo.com/avater9.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto14620180628190733.png', 'http://pic.xijiaomo.com/android_xjm_userphoto14620180628190747.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('89', '134', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater1.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('90', '148', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater2.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('91', '147', '陈晓龙', '420683199206192136', '', '1', '708929611', '', '', 'http://pic.xijiaomo.com/avater3.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto14720180706125239.png', 'http://pic.xijiaomo.com/android_xjm_userphoto14720180706125254.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('92', '150', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater4.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('93', '151', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater5.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('94', '152', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater6.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('95', '154', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater7.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('96', '153', '贺付帅', '372925198510012553', '', '1', '497001493', '', '', 'http://pic.xijiaomo.com/avater8.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto15320180721155702.png', 'http://pic.xijiaomo.com/android_xjm_userphoto15320180721155713.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('97', '155', '罗游进', '522129198012294017', '', '1', '346872439', '', '', 'http://pic.xijiaomo.com/avater9.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto15520180722012611.png', 'http://pic.xijiaomo.com/android_xjm_userphoto15520180722012623.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('98', '156', '龚伟', '360423198009214537', '', '1', '338373274', '', '', 'http://pic.xijiaomo.com/avater1.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto15620180723163351.png', 'http://pic.xijiaomo.com/android_xjm_userphoto15620180723163406.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('99', '157', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater2.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('100', '158', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater3.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('101', '159', '周谋', '612430199210180019', '', '1', '719344534', '', '', 'http://pic.xijiaomo.com/avater4.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto15920180828015513.png', 'http://pic.xijiaomo.com/android_xjm_userphoto15920180828015457.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('102', '160', '陈兴桥', '612426197704100011', '', '1', '229469678', '', '', 'http://pic.xijiaomo.com/avater5.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto16020180830053302.png', 'http://pic.xijiaomo.com/android_xjm_userphoto16020180830053319.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('103', '161', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater6.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('104', '162', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater7.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('105', '163', '曾婷英', '360731199109038949', '', '2', '683880321', '', '', 'http://pic.xijiaomo.com/avater8.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto16320180904154436.png', 'http://pic.xijiaomo.com/android_xjm_userphoto16320180904154447.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('106', '166', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater9.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('107', '167', '张金德', '321081198908224232', '', '1', '619771827', '', '', 'http://pic.xijiaomo.com/avater1.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto16720180907154937.png', 'http://pic.xijiaomo.com/android_xjm_userphoto16720180907154950.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('108', '172', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater2.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('109', '173', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater3.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('110', '174', '戚伟', '321027198009161516', '', '1', '337958096', '', '', 'http://pic.xijiaomo.com/avater4.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto17420180920211402.png', 'http://pic.xijiaomo.com/android_xjm_userphoto17420180920211412.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('111', '176', '刘凯', '370983198606196934', '', '1', '519570781', '', '', 'http://pic.xijiaomo.com/avater5.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto17620180923221221.png', 'http://pic.xijiaomo.com/android_xjm_userphoto17620180923221232.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('112', '177', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater6.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('113', '186', '丁志忠', '35058219721230051X', '', '1', '94568801', '', '', 'http://pic.xijiaomo.com/avater7.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto18620181206210533.png', 'http://pic.xijiaomo.com/android_xjm_userphoto18620181206210559.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('114', '190', '魏超', '420105198908100071', '', '1', '19890810', '', '1', 'http://pic.xijiaomo.com/20190116150746.jpg', '', '', '', '', '', '0', 'http://oxll5q098.bkt.clouddn.com/20190107133557.jpg', 'http://oxll5q098.bkt.clouddn.com/20190107133607.jpg', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('115', '194', '', '', '', '0', '0', '', null, 'http://pic.xijiaomo.com/avater9.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('119', '208', '陈兰', '622427199501203405', '', '2', '790571440', '', '', '', '', '', '', '', '', '0', 'http://oxll5q098.bkt.clouddn.com/android_xjm_userphoto20820190104111003.png', 'http://oxll5q098.bkt.clouddn.com/android_xjm_userphoto20820190104111016.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('127', '217', '', '', '', '0', '0', '', null, 'http://oxll5q098.bkt.clouddn.com/', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('128', '218', '张伟', '422202198804131357', '', '1', '880413', '', '9,8,7,6,5,4,3,2,1', '', '', '', '', '', '', '0', 'http://oxll5q098.bkt.clouddn.com/20190107151209.jpg', 'http://oxll5q098.bkt.clouddn.com/20190107151213.jpg', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('136', '225', '何洛', '421123199107237631', '', '1', '680253128', '', '', 'http://pic.xijiaomo.com/android_xjm_userphotonull20190114141908.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto22520190105161135.png', 'http://pic.xijiaomo.com/android_xjm_userphoto22520190105161141.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('137', '226', '李然', '210101197509244407', '', '1', '199009', '', '2', '', '', '', '', '', '', '0', 'http://oxll5q098.bkt.clouddn.com/20190106203047.jpg', 'http://oxll5q098.bkt.clouddn.com/20190106203057.jpg', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('138', '228', '高波', '420984198803078057', '', '1', '19880307', '', '2,1', '', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/20190107094708.jpg', 'http://pic.xijiaomo.com/20190107094713.jpg', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('139', '229', '高波', '420984198803071410', '', '1', '19880307', '', '9,8,7,6', '', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/20190107103701.jpg', 'http://pic.xijiaomo.com/20190107103705.jpg', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('140', '230', '', '', '', '0', '0', '', null, 'http://oxll5q098.bkt.clouddn.com/20190107095826.jpg', '', '', '', '', '', '0', null, null, '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('141', '231', '高波', '420984198803076799', '', '1', '19880307', '', '6,5,4', '', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/20190107100929.jpg', 'http://pic.xijiaomo.com/20190107100935.jpg', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('142', '232', '高波', '420984198803079471', '', '1', '19880307', '', '7,6,5', 'http://pic.xijiaomo.com/20190109115707.jpg', '', '', '', '', '', '0', 'http://pic.xijiaomo.com/20190107104113.jpg', 'http://pic.xijiaomo.com/20190107104120.jpg', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('143', '235', '易花女', '360731199601124822', '', '2', '821447131', '', '', '', '', '', '', '', '', '0', 'http://oxll5q098.bkt.clouddn.com/android_xjm_userphoto23520190113194503.png', 'http://oxll5q098.bkt.clouddn.com/android_xjm_userphoto23520190113194511.png', '0', null);
INSERT INTO `xjm_techer_checkinfo` VALUES ('144', '234', '阮芳荣', '352225197603022519', '', '1', '194601514', '', '', '', '', '', '', '', '', '0', 'http://oxll5q098.bkt.clouddn.com/android_xjm_userphoto23420190118160053.png', 'http://oxll5q098.bkt.clouddn.com/android_xjm_userphoto23420190118160111.png', '0', null);

-- ----------------------------
-- Table structure for xjm_techer_income
-- ----------------------------
DROP TABLE IF EXISTS `xjm_techer_income`;
CREATE TABLE `xjm_techer_income` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL DEFAULT '0' COMMENT '技师id',
  `goods_name` varchar(60) DEFAULT '' COMMENT '项目名称',
  `change_money` decimal(10,2) DEFAULT '0.00' COMMENT '改变金额',
  `from` tinyint(1) DEFAULT '0' COMMENT '1:收入 2：提现',
  `add_time` int(10) DEFAULT '0',
  `money` decimal(10,2) DEFAULT '0.00' COMMENT '总余额',
  `order_id` varchar(50) DEFAULT NULL COMMENT '订单id',
  `cash_id` int(11) DEFAULT NULL COMMENT '关联techer_cash',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8 COMMENT='技师收入明细表';

-- ----------------------------
-- Records of xjm_techer_income
-- ----------------------------
INSERT INTO `xjm_techer_income` VALUES ('60', '3', '脚底按摩', null, '1', '1511175134', '0.00', '2017112029775', null);
INSERT INTO `xjm_techer_income` VALUES ('61', '3', '脚底按摩', null, '1', '1511175174', '0.00', '2017112029775', null);
INSERT INTO `xjm_techer_income` VALUES ('62', '3', '脚底按摩', null, '1', '1511175234', '0.00', '2017112029775', null);
INSERT INTO `xjm_techer_income` VALUES ('63', '3', '脚底按摩', null, '1', '1511175281', '0.00', '2017112029775', null);
INSERT INTO `xjm_techer_income` VALUES ('64', '3', '脚底按摩', null, '1', '1511175363', '0.00', '2017112029775', null);
INSERT INTO `xjm_techer_income` VALUES ('65', '3', '脚底按摩', null, '1', '1511175367', '0.00', '2017112029775', null);
INSERT INTO `xjm_techer_income` VALUES ('66', '3', '脚底按摩', null, '1', '1511175443', '0.00', '2017112029775', null);
INSERT INTO `xjm_techer_income` VALUES ('67', '3', '脚底按摩', '0.01', '1', '1511175500', '0.01', '2017112029775', null);
INSERT INTO `xjm_techer_income` VALUES ('68', '3', '脚底按摩', '0.01', '1', '1511175634', '0.02', '2017112029775', null);
INSERT INTO `xjm_techer_income` VALUES ('69', '37', '脚底按摩', '0.01', '1', '1511181397', '0.01', '2017112098644', null);
INSERT INTO `xjm_techer_income` VALUES ('70', '27', '脚底按摩', '0.01', '1', '1511236637', '0.02', '2017112142369', null);
INSERT INTO `xjm_techer_income` VALUES ('71', '27', '脚底按摩', '0.01', '1', '1511319622', '0.03', '2017112295179', null);
INSERT INTO `xjm_techer_income` VALUES ('72', '3', '测试', '0.01', '1', '1511320985', '0.03', '2017112271486', null);
INSERT INTO `xjm_techer_income` VALUES ('73', '31', '足底按摩', '60.00', '1', '1511321597', '60.01', '2017112221580', null);
INSERT INTO `xjm_techer_income` VALUES ('74', '31', '收入提现', '60.01', '2', '1511322363', '0.00', null, '1');
INSERT INTO `xjm_techer_income` VALUES ('75', '31', '测试', '0.01', '1', '1511329407', '0.01', '2017112221728', null);
INSERT INTO `xjm_techer_income` VALUES ('76', '27', '脚底按摩', '50.00', '1', '1511329607', '50.03', '2017112281434', null);
INSERT INTO `xjm_techer_income` VALUES ('77', '36', '测试', '0.01', '1', '1511329974', '0.01', '2017112250787', null);
INSERT INTO `xjm_techer_income` VALUES ('78', '27', '测试', '0.01', '1', '1511330501', '50.04', '2017112272973', null);
INSERT INTO `xjm_techer_income` VALUES ('79', '27', '收入提现', '50.04', '2', '1511333747', '0.00', null, '2');
INSERT INTO `xjm_techer_income` VALUES ('80', '3', '脚底按摩', '50.00', '1', '1511335044', '50.03', '2017112214295', null);
INSERT INTO `xjm_techer_income` VALUES ('81', '3', '脚底按摩', '50.00', '1', '1511335821', '100.03', '2017112214295', null);
INSERT INTO `xjm_techer_income` VALUES ('82', '3', '脚底按摩', '50.00', '1', '1511335864', '100.03', '2017112214295', null);
INSERT INTO `xjm_techer_income` VALUES ('83', '3', '脚底按摩', '50.00', '1', '1511335979', '100.03', '2017112214295', null);
INSERT INTO `xjm_techer_income` VALUES ('84', '38', '洗面', '138.00', '1', '1511340506', '138.00', '2017112269805', null);
INSERT INTO `xjm_techer_income` VALUES ('85', '38', '收入提现', '50.00', '2', '1511341342', '88.00', null, '3');
INSERT INTO `xjm_techer_income` VALUES ('86', '27', '测试', '0.01', '1', '1511343136', '0.01', '2017112217468', null);
INSERT INTO `xjm_techer_income` VALUES ('87', '38', '测试', '0.01', '1', '1511343214', '88.01', '2017112294783', null);
INSERT INTO `xjm_techer_income` VALUES ('88', '27', '测试', '0.01', '1', '1511344830', '0.02', '2017112204277', null);
INSERT INTO `xjm_techer_income` VALUES ('89', '29', '测试', '0.01', '1', '1511400627', '0.01', '2017112394436', null);
INSERT INTO `xjm_techer_income` VALUES ('90', '39', '测试', '0.01', '1', '1511402190', '0.01', '2017112325427', null);
INSERT INTO `xjm_techer_income` VALUES ('91', '39', '测试', '0.01', '1', '1511402312', '0.02', '2017112318013', null);
INSERT INTO `xjm_techer_income` VALUES ('92', '39', '测试', '0.01', '1', '1511403095', '0.03', '2017112318603', null);
INSERT INTO `xjm_techer_income` VALUES ('93', '39', '测试', '0.01', '1', '1511403382', '0.04', '2017112365195', null);
INSERT INTO `xjm_techer_income` VALUES ('94', '39', '测试', '0.01', '1', '1511404856', '0.05', '2017112318844', null);
INSERT INTO `xjm_techer_income` VALUES ('95', '39', '测试', '0.01', '1', '1511404942', '0.06', '2017112347480', null);
INSERT INTO `xjm_techer_income` VALUES ('96', '40', '脚底按摩', '50.00', '1', '1511487198', '50.00', '2017112305104', null);
INSERT INTO `xjm_techer_income` VALUES ('97', '40', '脚底按摩', '50.00', '1', '1511505360', '100.00', '2017112432003', null);
INSERT INTO `xjm_techer_income` VALUES ('98', '40', '脚底按摩', '50.00', '1', '1511507636', '150.00', '2017112427329', null);
INSERT INTO `xjm_techer_income` VALUES ('99', '40', '脚底按摩', '50.00', '1', '1511507762', '200.00', '2017112427329', null);
INSERT INTO `xjm_techer_income` VALUES ('100', '40', '脚底按摩', '50.00', '1', '1511507788', '250.00', '2017112427329', null);
INSERT INTO `xjm_techer_income` VALUES ('101', '40', '脚底按摩', '50.00', '1', '1511518330', '300.00', '2017112466118', null);
INSERT INTO `xjm_techer_income` VALUES ('102', '43', '足底按摩', '60.00', '1', '1511754229', '60.00', '2017112778170', null);
INSERT INTO `xjm_techer_income` VALUES ('103', '43', '足底按摩', '60.00', '1', '1511754326', '120.00', '2017112778170', null);
INSERT INTO `xjm_techer_income` VALUES ('104', '40', '收入提现', '300.00', '2', '1511833989', '0.00', null, '4');
INSERT INTO `xjm_techer_income` VALUES ('105', '43', '足底按摩', '60.00', '1', '1511838770', '180.00', '2017112778170', null);
INSERT INTO `xjm_techer_income` VALUES ('106', '43', '足底按摩', '60.00', '1', '1511838819', '240.00', '2017112778170', null);
INSERT INTO `xjm_techer_income` VALUES ('107', '43', '足底按摩', '60.00', '1', '1511838834', '300.00', '2017112778170', null);
INSERT INTO `xjm_techer_income` VALUES ('108', '43', '足底按摩', '60.00', '1', '1511838847', '360.00', '2017112778170', null);
INSERT INTO `xjm_techer_income` VALUES ('109', '43', '足底按摩', '60.00', '1', '1511838894', '420.00', '2017112778170', null);
INSERT INTO `xjm_techer_income` VALUES ('110', '43', '足底按摩', '60.00', '1', '1511838945', '480.00', '2017112778170', null);
INSERT INTO `xjm_techer_income` VALUES ('111', '43', '足底按摩', '60.00', '1', '1511839008', '540.00', '2017112778170', null);
INSERT INTO `xjm_techer_income` VALUES ('112', '40', '脚底按摩', '50.00', '1', '1511859232', '50.00', '2017112881737', null);
INSERT INTO `xjm_techer_income` VALUES ('113', '3', '刮痧', '65.00', '1', '1511859970', '65.00', '2017112809625', null);
INSERT INTO `xjm_techer_income` VALUES ('114', '40', '脚底按摩', '50.00', '1', '1512359748', '100.00', '2017112881737', null);
INSERT INTO `xjm_techer_income` VALUES ('115', '40', '收入提现', '100.00', '2', '1512443626', '0.00', null, '5');

-- ----------------------------
-- Table structure for xjm_techer_notice
-- ----------------------------
DROP TABLE IF EXISTS `xjm_techer_notice`;
CREATE TABLE `xjm_techer_notice` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL COMMENT '用户id',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0：私有  1：公共',
  `common_id` int(11) unsigned DEFAULT '0' COMMENT '公共信息',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '标题',
  `content` varchar(200) NOT NULL DEFAULT '' COMMENT '内容',
  `add_time` int(10) unsigned NOT NULL DEFAULT '0',
  `is_read` tinyint(1) unsigned DEFAULT '0' COMMENT '0 未读  1已读',
  `read_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '阅读时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COMMENT='技师信息表';

-- ----------------------------
-- Records of xjm_techer_notice
-- ----------------------------
INSERT INTO `xjm_techer_notice` VALUES ('19', '1', '1', '1', '注册就送代金卷', '充值就送', '1504858748', '1', '1505782302');
INSERT INTO `xjm_techer_notice` VALUES ('20', '17', '1', '3', '注册就送代金卷3', '333333', '1504858848', '1', '1505782301');
INSERT INTO `xjm_techer_notice` VALUES ('21', '17', '0', '0', '欢迎注册', '你好', '1504859848', '1', '1505782284');
INSERT INTO `xjm_techer_notice` VALUES ('22', '17', '1', '1', '注册就送代金卷', '充值就送', '1504858748', '0', '0');
INSERT INTO `xjm_techer_notice` VALUES ('23', '17', '1', '3', '注册就送代金卷3', '333333', '1504858748', '0', '0');
INSERT INTO `xjm_techer_notice` VALUES ('24', '1', '1', '1', '注册就送代金卷', '充值就送', '1504858748', '0', '0');
INSERT INTO `xjm_techer_notice` VALUES ('25', '29', '1', '3', '注册就送代金卷3', '333333', '1504858748', '1', '1511344493');
INSERT INTO `xjm_techer_notice` VALUES ('26', '29', '1', '1', '注册就送代金卷', '充值就送', '1504858748', '1', '1511345386');
INSERT INTO `xjm_techer_notice` VALUES ('27', '29', '1', '3', '注册就送代金卷3', '333333', '1504858748', '1', '1511345388');
INSERT INTO `xjm_techer_notice` VALUES ('28', '26', '1', '1', '注册就送代金卷', '充值就送', '1504858748', '1', '1511158798');
INSERT INTO `xjm_techer_notice` VALUES ('29', '26', '1', '3', '注册就送代金卷3', '333333', '1504858748', '1', '1511158800');
INSERT INTO `xjm_techer_notice` VALUES ('30', '26', '1', '1', '注册就送代金卷', '充值就送', '1504858748', '1', '1511158803');
INSERT INTO `xjm_techer_notice` VALUES ('31', '26', '1', '3', '注册就送代金卷3', '333333', '1504858748', '1', '1511158803');
INSERT INTO `xjm_techer_notice` VALUES ('32', '38', '0', '3', '提现成功', '已经将50.00打入您的支付宝账户（17778079115）', '1511341567', '1', '1511343639');
INSERT INTO `xjm_techer_notice` VALUES ('33', '27', '0', '3', '提现成功', '已经将50.04打入您的支付宝账户（834229969@qq.com）', '1511344062', '1', '1511346025');
INSERT INTO `xjm_techer_notice` VALUES ('34', '31', '0', '3', '提现成功', '已经将60.01打入您的支付宝账户（15927219779）', '1511344074', '0', '0');
INSERT INTO `xjm_techer_notice` VALUES ('35', '43', '0', '3', '恭喜您审核成功', '恭喜您审核成功，可以接单了', '1511516476', '0', '0');
INSERT INTO `xjm_techer_notice` VALUES ('36', '44', '0', '3', '恭喜您审核成功', '恭喜您审核成功，可以接单了', '1511750689', '1', '1511750728');
INSERT INTO `xjm_techer_notice` VALUES ('37', '40', '0', '3', '提现成功', '已经将300.00打入您的支付宝账户（834229969@qq.com）', '1511855310', '0', '0');
INSERT INTO `xjm_techer_notice` VALUES ('38', '52', '0', '3', '恭喜您审核成功', '恭喜您审核成功，可以接单了', '1512020876', '1', '1512021488');
INSERT INTO `xjm_techer_notice` VALUES ('39', '1', '0', '3', '您的视频已经审核通过', '您的视频已经审核通过', '1512383537', '0', '0');
INSERT INTO `xjm_techer_notice` VALUES ('40', '2', '0', '3', '您的头像已经审核通过', '您的头像已经审核通过', '1512385260', '0', '0');
INSERT INTO `xjm_techer_notice` VALUES ('41', '3', '0', '3', '您的头像已经审核通过', '您的头像已经审核通过', '1512385316', '1', '1512446653');
INSERT INTO `xjm_techer_notice` VALUES ('42', '26', '0', '3', '您的头像已经审核通过', '您的头像已经审核通过', '1512437159', '0', '0');
INSERT INTO `xjm_techer_notice` VALUES ('43', '59', '0', '3', '恭喜您审核成功', '恭喜您审核成功，可以接单了', '1512459626', '1', '1516360789');
INSERT INTO `xjm_techer_notice` VALUES ('44', '58', '0', '3', '恭喜您审核成功', '恭喜您审核成功，可以接单了', '1512459654', '1', '1543196623');
INSERT INTO `xjm_techer_notice` VALUES ('45', '1', '0', '3', '您的头像已经审核通过', '您的头像已经审核通过', '1512525883', '0', '0');
INSERT INTO `xjm_techer_notice` VALUES ('46', '3', '0', '3', '您的头像已经审核通过', '您的头像已经审核通过', '1512553881', '0', '0');
INSERT INTO `xjm_techer_notice` VALUES ('47', '37', '0', '3', '您的头像已经审核通过', '您的头像已经审核通过', '1512555712', '1', '1512555758');
INSERT INTO `xjm_techer_notice` VALUES ('48', '21', '0', '3', '您的头像已经审核通过', '您的头像已经审核通过', '1512608420', '0', '0');
INSERT INTO `xjm_techer_notice` VALUES ('49', '2', '0', '3', '您的头像已经审核通过', '您的头像已经审核通过', '1512608465', '0', '0');
INSERT INTO `xjm_techer_notice` VALUES ('50', '58', '0', '3', '您的头像已经审核通过', '您的头像已经审核通过', '1512608504', '1', '1543196619');
INSERT INTO `xjm_techer_notice` VALUES ('51', '26', '0', '3', '您的头像已经审核通过', '您的头像已经审核通过', '1512608532', '0', '0');
INSERT INTO `xjm_techer_notice` VALUES ('52', '34', '0', '3', '您的头像已经审核通过', '您的头像已经审核通过', '1512608544', '0', '0');
INSERT INTO `xjm_techer_notice` VALUES ('53', '2', '0', '3', '您的头像已经审核通过', '您的头像已经审核通过', '1512610535', '0', '0');
INSERT INTO `xjm_techer_notice` VALUES ('54', '3', '0', '3', '您的头像已经审核通过', '您的头像已经审核通过', '1512610544', '0', '0');
INSERT INTO `xjm_techer_notice` VALUES ('55', '39', '0', '3', '您的头像已经审核通过', '您的头像已经审核通过', '1512610563', '1', '1512887960');
INSERT INTO `xjm_techer_notice` VALUES ('56', '3', '0', '3', '您的头像已经审核通过', '您的头像已经审核通过', '1512610599', '0', '0');
INSERT INTO `xjm_techer_notice` VALUES ('58', '40', '0', '3', '提现成功', '已经将100.00打入您的支付宝账户（834229969@qq.com）', '1512615548', '0', '0');
INSERT INTO `xjm_techer_notice` VALUES ('59', '69', '0', '3', '恭喜您审核成功', '恭喜您审核成功，可以接单了', '1513042788', '0', '0');
INSERT INTO `xjm_techer_notice` VALUES ('60', '69', '0', '3', '您的头像已经审核通过', '您的头像已经审核通过', '1513042858', '0', '0');
INSERT INTO `xjm_techer_notice` VALUES ('61', '40', '0', '3', '提现成功', '已经将300.00打入您的支付宝账户（834229969@qq.com）', '1513821333', '0', '0');
INSERT INTO `xjm_techer_notice` VALUES ('62', '40', '0', '3', '提现成功', '已经将100.00打入您的支付宝账户（834229969@qq.com）', '1513822677', '0', '0');
INSERT INTO `xjm_techer_notice` VALUES ('63', '13', '0', '3', '恭喜您审核成功', '恭喜您审核成功，可以接单了', '1526259044', '0', '0');

-- ----------------------------
-- Table structure for xjm_techer_photo
-- ----------------------------
DROP TABLE IF EXISTS `xjm_techer_photo`;
CREATE TABLE `xjm_techer_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL COMMENT '用户名',
  `type` tinyint(1) DEFAULT '0' COMMENT '1：照片 2：视频',
  `pic_url` varchar(100) DEFAULT '' COMMENT '图片地址',
  `video_url` varchar(100) DEFAULT '' COMMENT '视频地址',
  `add_time` int(10) DEFAULT '0' COMMENT '添加时间',
  `pic_status` tinyint(1) DEFAULT '0' COMMENT '照片审核状态：0未审核，1已通过，2未通过',
  `video_status` tinyint(1) DEFAULT '0' COMMENT '视频审核状态：0未审核，1已通过，2未通过',
  PRIMARY KEY (`id`),
  KEY `tid` (`tid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=628 DEFAULT CHARSET=utf8 COMMENT='技师照片信息表';

-- ----------------------------
-- Records of xjm_techer_photo
-- ----------------------------
INSERT INTO `xjm_techer_photo` VALUES ('37', '1', '1', 'http://pic.xijiaomo.com/t4.png', '', '1505821312', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('40', '1', '1', 'http://pic.xijiaomo.com/t3.png', '', '1505821312', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('44', '1', '1', 'http://pic.xijiaomo.com/t2.png', '', '1505821346', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('45', '1', '1', 'http://pic.xijiaomo.com/t1.png', '', '1505821346', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('83', '1', '2', '', '', '1505870730', '0', '1');
INSERT INTO `xjm_techer_photo` VALUES ('103', '1', '1', 'http://pic.xijiaomo.com/t1.png', '', '1505896145', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('189', '25', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2520171101100156', '', '1509501729', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('190', '25', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2520171101104822', '', '1509504502', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('191', '25', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2520171101105143', '', '1509504703', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('192', '25', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2520171101105217', '', '1509504736', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('193', '25', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2520171101110152', '', '1509505313', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('194', '25', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2520171101110321', '', '1509505402', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('195', '25', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2520171101110450', '', '1509505489', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('196', '25', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2520171101110658', '', '1509505618', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('197', '25', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2520171101110907', '', '1509505750', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('198', '25', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2520171101110945', '', '1509505786', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('199', '25', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2520171101111027', '', '1509505827', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('200', '25', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2520171101114053', '', '1509507658', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('201', '25', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2520171101114113', '', '1509507677', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('202', '25', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2520171101114437', '', '1509507878', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('203', '25', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2520171101114455', '', '1509507898', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('204', '25', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2520171101115456', '', '1509508496', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('205', '25', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2520171101115504', '', '1509508505', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('206', '25', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2520171101115837', '', '1509508727', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('207', '25', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2520171101115859', '', '1509508740', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('208', '25', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2520171101120317', '', '1509509000', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('209', '25', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2520171101120330', '', '1509509011', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('214', '1', '1', 'http://pic.xijiaomo.com/t1.png', '', '0', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('253', '36', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto3620171116145102.png', '', '1510815063', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('254', '36', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto3620171116145103.png', '', '1510815064', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('255', '36', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto3620171116145104.png', '', '1510815068', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('266', '36', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto3620171117091601.png', '', '1510881363', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('267', '35', '1', 'http://pic.xijiaomo.com/Picture/20171117/1510886694qR95eJoh.jpg', '', '1510886749', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('268', '35', '1', 'http://pic.xijiaomo.com/Picture/20171117/1510886683xrdO6gIa.jpg', '', '1510886749', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('269', '35', '1', 'http://pic.xijiaomo.com/Picture/20171117/1510886685HfVRLNKP.jpg', '', '1510886749', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('270', '35', '1', 'http://pic.xijiaomo.com/Picture/20171117/1510886692g3r2AcmC.jpg', '', '1510886749', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('271', '35', '1', 'http://pic.xijiaomo.com/Picture/20171117/1510886688Wr0Wg7ij.jpg', '', '1510886749', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('272', '35', '1', 'http://pic.xijiaomo.com/Picture/20171117/15108866906fIZVEkX.jpg', '', '1510886749', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('308', '29', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2920171117144756.png', '', '1510901275', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('309', '29', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2920171117144757.png', '', '1510901277', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('310', '29', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2920171117144758.png', '', '1510901277', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('316', '26', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2620171117150032.png', '', '1510902027', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('317', '26', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2620171117150035.png', '', '1510902029', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('318', '26', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2620171117150038.png', '', '1510902030', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('319', '27', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2720171118123936.png', '', '1510979976', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('320', '3', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto320171119212306.png', '', '1511097787', '1', '0');
INSERT INTO `xjm_techer_photo` VALUES ('323', '26', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto2620171120133937.png', '', '1511156358', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('325', '26', '2', '', '', '1511157704', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('326', '27', '1', 'http://pic.xijiaomo.com/Picture/20171120/1511159308ouLpFYxj.jpg', '', '1511159312', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('327', '37', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto3720171120173907.png', '', '1511170729', '1', '0');
INSERT INTO `xjm_techer_photo` VALUES ('328', '37', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto3720171120173908.png', '', '1511170730', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('329', '37', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto3720171120173909.png', '', '1511170730', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('330', '37', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto3720171120173910.png', '', '1511170731', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('331', '37', '2', '', '', '1511170868', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('332', '37', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto3720171120213133.png', '', '1511184675', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('335', '38', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto3820171122173620.png', '', '1511343381', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('336', '38', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto3820171122173621.png', '', '1511343382', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('337', '29', '2', '', '', '1511345510', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('338', '27', '1', 'http://pic.xijiaomo.com/Picture/20171122/1511345630k1tsEflf.jpg', '', '1511345682', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('339', '27', '1', 'http://pic.xijiaomo.com/Picture/20171122/1511345631pqPtu1R5.jpg', '', '1511345682', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('340', '27', '1', 'http://pic.xijiaomo.com/Picture/20171122/1511345634EDUyYkpz.jpg', '', '1511345682', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('341', '27', '1', 'http://pic.xijiaomo.com/Picture/20171122/1511345633zeyx9yTJ.jpg', '', '1511345682', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('342', '38', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto3820171122181757.png', '', '1511345878', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('343', '38', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto3820171122181811.png', '', '1511345892', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('344', '38', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto3820171122181812.png', '', '1511345893', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('345', '35', '1', 'http://pic.xijiaomo.com/Picture/20171122/1511346976SUoVZy2q.jpg', '', '1511347005', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('346', '35', '1', 'http://pic.xijiaomo.com/Picture/20171122/1511346976SUoVZy2q.jpg', '', '1511347005', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('347', '35', '1', '1', '', '1511347005', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('348', '35', '1', '2', '', '1511347005', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('349', '35', '1', '3', '', '1511347005', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('350', '35', '1', '0', '', '1511347005', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('351', '39', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto3920171123091639.png', '', '1511399801', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('352', '39', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto3920171123091641.png', '', '1511399802', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('353', '39', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto3920171123091642.png', '', '1511399802', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('354', '39', '2', '', '', '1511399831', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('355', '39', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto3920171123100454.png', '', '1511402669', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('361', '39', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto3920171123110315.png', '', '1511406170', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('362', '39', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto3920171123110316.png', '', '1511406170', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('366', '40', '1', 'http://pic.xijiaomo.com/Picture/20171123/1511435605mnKhpyNY.jpg', '', '1511435645', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('368', '37', '1', 'http://pic.xijiaomo.com/Picture/20171123/1511443890vwffVuDY.jpg', '', '1511443891', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('369', '37', '1', 'http://pic.xijiaomo.com/Picture/20171123/1511443889q7Td5I79.jpg', '', '1511443891', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('371', '40', '1', 'http://pic.xijiaomo.com/Picture/20171124/1511513321MBKkKkic.jpg', '', '1511513366', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('373', '40', '1', 'http://pic.xijiaomo.com/Picture/20171124/1511513323WosopRkQ.jpg', '', '1511513366', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('376', '40', '1', 'http://pic.xijiaomo.com/Picture/20171124/1511513403ouanOwz4.jpg', '', '1511513438', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('381', '40', '1', 'http://pic.xijiaomo.com/Picture/20171125/1511583593RZYkIhoz.jpg', '', '1511583603', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('382', '40', '1', 'http://pic.xijiaomo.com/Picture/20171126/1511690895ehQUTQFT.jpg', '', '1511690898', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('383', '40', '1', 'http://pic.xijiaomo.com/Picture/20171126/15117056981PRV6XGe.jpg', '', '1511705727', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('384', '40', '1', '0', '', '1511705727', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('385', '43', '1', 'http://pic.xijiaomo.com/Picture/20171127/1511771644f20nY1iH.jpg', '', '1511771645', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('386', '43', '1', '1', '', '1511771645', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('387', '43', '1', 'http://pic.xijiaomo.com/Picture/20171127/1511771644f20nY1iH.jpg', '', '1511771645', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('388', '48', '1', 'http://pic.xijiaomo.com/Picture/20171128/15118476426aCIbNoP.jpg', '', '1511847670', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('389', '48', '1', '1', '', '1511847670', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('390', '48', '1', 'http://pic.xijiaomo.com/Picture/20171128/15118476411LgHl1S0.jpg', '', '1511847670', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('398', '48', '1', 'http://pic.xijiaomo.com/Picture/20171130/1512020980XLVZWNE8.jpg', '', '1512020982', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('399', '52', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto5220171130140743.png', '', '1512022061', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('400', '52', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto5220171130140802.png', '', '1512022081', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('401', '52', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto5220171130140803.png', '', '1512022081', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('402', '52', '2', '', '', '1512022100', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('405', '48', '1', 'http://pic.xijiaomo.com/Picture/20171201/1512133559TG4KE9wy.jpg', '', '1512133573', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('406', '48', '1', '4', '', '1512133573', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('407', '48', '1', '2', '', '1512133573', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('408', '48', '1', 'http://pic.xijiaomo.com/Picture/20171201/1512133560Y5rMtV2n.jpg', '', '1512133573', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('409', '48', '1', '0', '', '1512133573', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('410', '48', '1', 'http://pic.xijiaomo.com/Picture/20171201/15121338477cpUI6WX.jpg', '', '1512133861', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('411', '48', '1', '8', '', '1512133861', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('412', '48', '1', '5', '', '1512133861', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('415', '48', '1', '7', '', '1512133861', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('419', '48', '2', '', '', '1512134599', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('420', '37', '1', 'http://pic.xijiaomo.com/Picture/20171202/1512189135PrFodsJa.jpg', '', '1512189137', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('421', '37', '1', '1', '', '1512189137', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('422', '48', '1', 'http://pic.xijiaomo.com/Picture/20171204/1512360402EmWbNpEa.jpg', '', '1512360419', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('447', '59', '2', '', '', '1512552196', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('448', '59', '2', '', '', '1512559923', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('449', '59', '2', '', '', '1512610717', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('450', '73', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto7320171219122713.png', '', '1513657633', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('451', '73', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto7320171219122714.png', '', '1513657634', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('452', '73', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto7320171219122715.png', '', '1513657634', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('453', '73', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto7320171219122716.png', '', '1513657636', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('455', '73', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto7320171219122752.png', '', '1513657672', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('457', '58', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto5820171227142524.png', '', '1514355923', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('458', '58', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto5820171227142610.png', '', '1514355969', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('459', '58', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto5820171227142611.png', '', '1514355970', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('462', '94', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto9420180128111704.png', '', '1517109424', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('463', '94', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto9420180201172615.png', '', '1517477177', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('464', '94', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto9420180201172616.png', '', '1517477178', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('465', '94', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto9420180201172617.png', '', '1517477179', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('466', '94', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto9420180201172618.png', '', '1517477180', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('467', '94', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto9420180201172619.png', '', '1517477181', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('468', '94', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto9420180201172620.png', '', '1517477182', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('469', '94', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto9420180201172621.png', '', '1517477183', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('470', '94', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto9420180201172623.png', '', '1517477185', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('471', '105', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto10520180316194621.png', '', '1521200782', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('472', '105', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto10520180316194622.png', '', '1521200782', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('473', '105', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto10520180316194623.png', '', '1521200783', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('474', '105', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto10520180316194625.png', '', '1521200786', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('475', '105', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto10520180316194727.png', '', '1521200848', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('476', '105', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto10520180316194729.png', '', '1521200849', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('478', '106', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto10620180318140100.png', '', '1521352861', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('479', '106', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto10620180318140117.png', '', '1521352877', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('480', '106', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto10620180318140303.png', '', '1521352984', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('481', '108', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto10820180320013001.png', '', '1521480601', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('482', '108', '2', '', '', '1521480742', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('483', '108', '2', '', '', '1521480746', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('484', '58', '2', '', '', '1524036884', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('486', '121', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto12120180423190252.png', '', '1524481372', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('487', '121', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto12120180423190917.png', '', '1524481759', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('488', '132', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto13220180608162834.png', '', '1528446516', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('489', '132', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto13220180608162835.png', '', '1528446517', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('490', '132', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto13220180608162836.png', '', '1528446518', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('491', '132', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto13220180608162838.png', '', '1528446519', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('492', '132', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto13220180608162839.png', '', '1528446521', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('493', '146', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto14620180629074743.png', '', '1530229664', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('494', '146', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto14620180629074744.png', '', '1530229665', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('496', '160', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto16020180830180532.png', '', '1535623529', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('497', '176', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto17620180923221441.png', '', '1537712082', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('498', '176', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto17620180923221442.png', '', '1537712083', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('505', '195', '1', 'http://oxll5q098.bkt.clouddn.com/Picture/20190105/1546652591rpGw6CBZ.jpg', '', '1546652595', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('506', '195', '1', '1', '', '1546652595', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('508', '195', '1', 'http://pic.xijiaomo.com/Picture/20190105/1546655709T3m2pxwX.jpg', '', '1546655718', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('509', '195', '1', 'http://pic.xijiaomo.com/Picture/20190105/1546655733PFYDk2TO.jpg', '', '1546655742', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('512', '195', '1', 'http://pic.xijiaomo.com/Picture/20190105/15466558840f9nGpIO.jpg', '', '1546655889', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('513', '195', '1', 'http://pic.xijiaomo.com/Picture/20190105/15466558988RaJicVo.jpg', '', '1546655907', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('514', '195', '1', 'http://pic.xijiaomo.com/Picture/20190105/15466558988RaJicVo.jpg', '', '1546655907', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('515', '195', '1', 'http://pic.xijiaomo.com/Picture/20190105/15466558988RaJicVo.jpg', '', '1546655907', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('516', '195', '1', '3', '', '1546655907', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('517', '195', '1', 'http://pic.xijiaomo.com/Picture/20190105/15466558988RaJicVo.jpg', '', '1546655907', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('519', '195', '1', 'http://pic.xijiaomo.com/Picture/20190105/1546657063589Ho0lJ.jpg', '', '1546657066', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('520', '195', '2', '', 'http://pic.xijiaomo.com/Video/20190105/1546670488KYMrH9bq.mp4', '1546670497', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('522', '199', '2', '', 'http://oxll5q098.bkt.clouddn.com/android_xjm_userphoto19920190105145519.mp4', '1546671362', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('524', '225', '1', 'http://pic.xijiaomo.com/android_xjm_userphoto22520190107105625.png', '', '1546829805', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('537', '225', '2', '', 'http://pic.xijiaomo.com/android_xjm_userphoto22520190107141808.mp4', '1546841891', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('538', '232', '2', '', 'http://pic.xijiaomo.com/5F7D3D92-2C88-4D88-9438-398AE5C3A782154684693264898778132988.mp4', '1546846961', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('539', '232', '2', '', 'http://pic.xijiaomo.com/5F7D3D92-2C88-4D88-9438-398AE5C3A7821546847107337303345332213.mp4', '1546847134', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('540', '232', '2', '', 'http://pic.xijiaomo.com/D0605797-7368-41AE-A5F3-E477BA72A56F1546850441202193560255354.mp4', '1546850508', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('541', '232', '2', '', 'http://pic.xijiaomo.com/D0605797-7368-41AE-A5F3-E477BA72A56F1546850647103286502272325.mp4', '1546850688', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('554', '190', '2', '', 'http://oxll5q098.bkt.clouddn.com/Video/20190108/1546916303vpqwPpVi.mp4', '1546916320', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('555', '190', '2', '', 'http://oxll5q098.bkt.clouddn.com/Video/20190108/1546916328wqo9AGOZ.mp4', '1546916344', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('612', '232', '1', 'http://pic.xijiaomo.com/5F7D3D92-2C88-4D88-9438-398AE5C3A782154700610373944928183978', '', '1547006107', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('613', '232', '1', 'http://pic.xijiaomo.com/5F7D3D92-2C88-4D88-9438-398AE5C3A7821547006112450311150229990', '', '1547006112', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('614', '232', '1', 'http://pic.xijiaomo.com/5F7D3D92-2C88-4D88-9438-398AE5C3A78215470061162703329863407', '', '1547006116', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('615', '232', '1', 'http://pic.xijiaomo.com/5F7D3D92-2C88-4D88-9438-398AE5C3A7821547006123550201938264536', '', '1547006124', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('616', '232', '1', 'http://pic.xijiaomo.com/5F7D3D92-2C88-4D88-9438-398AE5C3A7821547006123601381945147768', '', '1547006124', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('617', '232', '1', 'http://pic.xijiaomo.com/5F7D3D92-2C88-4D88-9438-398AE5C3A7821547006123648219345203047', '', '1547006124', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('618', '232', '1', 'http://pic.xijiaomo.com/5F7D3D92-2C88-4D88-9438-398AE5C3A7821547006123734205072115897', '', '1547006124', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('619', '232', '1', 'http://pic.xijiaomo.com/5F7D3D92-2C88-4D88-9438-398AE5C3A7821547006123695243785103016', '', '1547006124', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('620', '232', '1', 'http://pic.xijiaomo.com/5F7D3D92-2C88-4D88-9438-398AE5C3A782154700612378410297435541', '', '1547006124', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('624', '190', '1', 'http://pic.xijiaomo.com/00000000-0000-0000-0000-00000000000015476225400339046057313', '', '1547622542', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('625', '190', '1', 'http://pic.xijiaomo.com/00000000-0000-0000-0000-000000000000154762254000775517414762', '', '1547622542', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('626', '190', '1', 'http://pic.xijiaomo.com/00000000-0000-0000-0000-000000000000154762253996140930317834', '', '1547622542', '0', '0');
INSERT INTO `xjm_techer_photo` VALUES ('627', '190', '2', '', 'http://pic.xijiaomo.com/00000000-0000-0000-0000-000000000000154762361857973077169799.mp4', '1547623632', '0', '0');

-- ----------------------------
-- Table structure for xjm_techer_time_set
-- ----------------------------
DROP TABLE IF EXISTS `xjm_techer_time_set`;
CREATE TABLE `xjm_techer_time_set` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tid` int(11) DEFAULT '0' COMMENT '技师id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='技师时间设置表';

-- ----------------------------
-- Records of xjm_techer_time_set
-- ----------------------------

-- ----------------------------
-- Table structure for xjm_tousu_log
-- ----------------------------
DROP TABLE IF EXISTS `xjm_tousu_log`;
CREATE TABLE `xjm_tousu_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT '0' COMMENT '订单id',
  `tid` int(11) DEFAULT '0' COMMENT '技师ID',
  `admin_user` int(11) DEFAULT '0' COMMENT '操作员',
  `add_time` int(11) DEFAULT '0' COMMENT '投诉时间',
  `content` text COMMENT '投诉内容',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xjm_tousu_log
-- ----------------------------
INSERT INTO `xjm_tousu_log` VALUES ('4', '997', '29', '16', '1512699992', '测试添加投诉');

-- ----------------------------
-- Table structure for xjm_tuidan_log
-- ----------------------------
DROP TABLE IF EXISTS `xjm_tuidan_log`;
CREATE TABLE `xjm_tuidan_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL COMMENT '订单ID',
  `tid` int(11) DEFAULT NULL,
  `add_time` int(11) DEFAULT NULL,
  `admin_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xjm_tuidan_log
-- ----------------------------
INSERT INTO `xjm_tuidan_log` VALUES ('1', '1100', '3', '1512464550', '16');
INSERT INTO `xjm_tuidan_log` VALUES ('2', '1125', '37', '1512699887', '16');

-- ----------------------------
-- Table structure for xjm_user
-- ----------------------------
DROP TABLE IF EXISTS `xjm_user`;
CREATE TABLE `xjm_user` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户主键id',
  `nickname` varchar(20) DEFAULT '' COMMENT '用户名',
  `mobile` varchar(11) NOT NULL DEFAULT '' COMMENT '手机号',
  `now_address` tinyint(2) unsigned DEFAULT '0' COMMENT '对应地区表经营地区表',
  `money` decimal(11,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '金额',
  `status` int(2) NOT NULL DEFAULT '0' COMMENT '用户状态0正常   1封号',
  `add_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '注册时间',
  `point` int(10) NOT NULL DEFAULT '0' COMMENT '用户总积分',
  `headurl` varchar(200) NOT NULL COMMENT '用户头像',
  `version` varchar(10) DEFAULT '1.0' COMMENT '版本号',
  `source` varchar(10) DEFAULT NULL COMMENT '来源，如ios/android',
  `s_uid` varchar(100) DEFAULT NULL COMMENT '被推荐人的uid',
  `openid` varchar(50) DEFAULT NULL COMMENT 'openid',
  `type` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `mobile` (`mobile`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=756 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of xjm_user
-- ----------------------------
INSERT INTO `xjm_user` VALUES ('23', '111', '15623233245', '0', '0.00', '0', '1505201176', '0', '', '1.0', 'android', null, null, '0');
INSERT INTO `xjm_user` VALUES ('24', '222', '17607185200', '0', '0.00', '0', '1405292124', '0', 'http://omqtlkwj8.bkt.clouddn.com/android_xjm_userphoto2420170921172611', '1.0', 'android', null, null, '0');
INSERT INTO `xjm_user` VALUES ('25', '', '15623233234', '0', '0.00', '0', '1505454990', '0', '', '1.0', 'android', null, null, '0');
INSERT INTO `xjm_user` VALUES ('27', '', '15623765690', '0', '0.00', '0', '1505777105', '0', '', '1.0', 'android', null, null, '0');
INSERT INTO `xjm_user` VALUES ('28', '', '15927365569', '0', '0.00', '0', '1505785727', '0', '', '1.0', 'android', null, null, '0');
INSERT INTO `xjm_user` VALUES ('29', '', '15923765690', '0', '0.00', '0', '1505864038', '0', '', '1.0', 'android', null, null, '0');
INSERT INTO `xjm_user` VALUES ('31', '你猜', '15172370138', '0', '0.00', '0', '1507701355', '16000', 'http://pic.xijiaomo.com/android_xjm_userphoto3120171205162810', '1.0', 'android', '72,73,74', null, '0');
INSERT INTO `xjm_user` VALUES ('32', '火箭一号', '18206849645', '0', '0.00', '0', '1507717036', '540', 'http://pic.xijiaomo.com/20180118160456.', '1.0', 'ios', '92', null, '0');
INSERT INTO `xjm_user` VALUES ('33', '', '18507101617', '0', '0.00', '0', '1509339752', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('34', '233', '18507151771', '0', '0.00', '0', '1509517177', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('35', '123', '15927219779', '0', '0.00', '0', '1509530642', '6', 'http://pic.xijiaomo.com/android_xjm_userphoto3520171115105520', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('36', '天冷了，要运动', '17137214472', '0', '0.00', '0', '1509535522', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto3620171102140110', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('37', '这个产品不太冷', '18008655056', '0', '0.00', '0', '1509622535', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto3720171102193758', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('38', 'yk里', '18107214796', '0', '0.00', '0', '1509672337', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto3820171120194130', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('39', '培培', '18627099297', '0', '0.00', '0', '1509672380', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('40', '大白菜', '13397140486', '0', '0.00', '0', '1509676843', '400', 'http://pic.xijiaomo.com/20171123171332.', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('41', '', '13971630646', '0', '0.00', '0', '1509687343', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('42', '', '13607128218', '0', '0.00', '0', '1509689485', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('46', '枫叶', '17778079115', '0', '0.00', '0', '1509966896', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTL9HOfwHDIVdf5icSwkEUhTY3yd0gribvnsIvH9HriaibvSzgPGupphQqoRKSHzDKpUgkicibxYp9yTpZrQ/132', '1.0', null, '72,73,74', null, '0');
INSERT INTO `xjm_user` VALUES ('47', '', '17778079511', '0', '0.00', '0', '1509969289', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('48', '', '17789541522', '0', '0.00', '0', '1509969586', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('49', '', '17778215644', '0', '0.00', '0', '1509970327', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('50', '', '17780794115', '0', '0.00', '0', '1509970886', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('51', '', '18888584124', '0', '0.00', '0', '1509970959', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('52', '', '17878788565', '0', '0.00', '0', '1509971017', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('53', '', '17780791556', '0', '0.00', '0', '1509971048', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('54', '', '17780791155', '0', '0.00', '0', '1509971091', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('55', '', '17107214796', '0', '0.00', '0', '1510124396', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('56', '', '15071432056', '0', '0.00', '0', '1510154038', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('57', '', '18162519010', '0', '0.00', '0', '1510155198', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('58', '', '13556834691', '0', '0.00', '0', '1510193104', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('60', '', '15337245692', '0', '0.00', '0', '1510220767', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('61', '', '18539976841', '0', '0.00', '0', '1510223500', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('62', '', '15071379321', '0', '0.00', '0', '1510285666', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('63', '', '13477471319', '0', '0.00', '0', '1510399939', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('64', '', '15972039044', '0', '0.00', '0', '1510410426', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('65', '', '13419680692', '0', '0.00', '0', '1510834548', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('66', '', '18702715350', '0', '0.00', '0', '1511089390', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('68', '', '17320567880', '0', '0.00', '0', '1511407297', '0', 'http://pic.xijiaomo.com/20171123120743.', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('69', '阿宽', '15826995634', '0', '0.00', '0', '1511418073', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto6920171221111419', '1.0', null, '81', null, '0');
INSERT INTO `xjm_user` VALUES ('70', 'nxn', '17368584803', '0', '0.00', '0', '1511420642', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto7020171123150548', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('71', '0度', '18671925557', '0', '0.00', '0', '1511510970', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('72', '', '18062621299', '0', '0.00', '0', '1511522883', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('73', '', '18975661141', '0', '0.00', '0', '1511523025', '3000', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('74', '', '13297912209', '0', '0.00', '0', '1511523357', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('75', '', '13216133640', '0', '0.00', '0', '1511550244', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('76', '', '18971431601', '0', '0.00', '0', '1511594106', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('78', '1q51i2ngshui', '13925542153', '0', '0.00', '0', '1511774088', '0', 'http://pic.xijiaomo.com/ctjxgm5fdh.png', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('79', '', '15007150712', '0', '0.00', '0', '1511834072', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('80', '', '15868198636', '0', '0.00', '0', '1511844118', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('82', 'PXR', '13597675220', '0', '0.00', '0', '1511856943', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto8220171128161617', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('83', '', '18627921686', '0', '0.00', '0', '1511869576', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('84', '', '15827037353', '0', '0.00', '0', '1511870828', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('85', '', '13816529139', '0', '0.00', '0', '1511880622', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('86', '', '18868108303', '0', '0.00', '0', '1511928366', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('87', '', '13886192822', '0', '0.00', '0', '1511941024', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('88', '', '13098801456', '0', '0.00', '0', '1512006921', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('89', '18971193603', '18971193603', '0', '0.00', '0', '1512021909', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTL9HOfwHDIVdf5icSwkEUhTY3yd0gribvnsIvH9HriaibvSzgPGupphQqoRKSHzDKpUgkicibxYp9yTpZrQ/132', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('90', '', '13813548107', '0', '0.00', '0', '1512031274', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('91', '', '15167880339', '0', '0.00', '0', '1512047608', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('92', '', '15171186572', '0', '0.00', '0', '1512093708', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('93', '', '13085271699', '0', '0.00', '0', '1512122059', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('94', '', '18872233699', '0', '0.00', '0', '1512131202', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('95', '', '13529306620', '0', '0.00', '0', '1512319562', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('96', '胡品', '', '0', '0.00', '0', '1512354476', '0', 'https://wx.qlogo.cn/mmopen/vi_32/7pib2AEVRKk8LUep3MuGm21RcLPNsaq4H90Ev5yA730aLqWDL8vozJJ2cfSfD06iaCuJB6GyewrCSR4apnAOXpUw/0', '1.0', null, null, 'oS5GbwkpTUOKM_JMtIze7PtPNUoM', '3');
INSERT INTO `xjm_user` VALUES ('104', '亦有', '', '0', '0.00', '0', '1512355127', '0', 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTL9x3uHgQNZ6furZBeSAliaichljSME43vFO6CwqKz9WOmfVzZwRe9L0Mfm781eLkiboNjkCGQsJbqOw/0', '1.0', null, null, 'oS5GbwhYSiN-g_TRskZ3AiHAL1Og', '3');
INSERT INTO `xjm_user` VALUES ('105', 'march', '', '0', '0.00', '0', '1512355132', '0', 'https://q.qlogo.cn/qqapp/1106555152/0AA6AD2A4CC2270E8734F6E2C5A96195/100', '1.0', null, null, '0AA6AD2A4CC2270E8734F6E2C5A96195', '1');
INSERT INTO `xjm_user` VALUES ('106', 'fish', '', '0', '0.00', '0', '1512355195', '0', 'http://q.qlogo.cn/qqapp/1106555152/6D83CE7B4EEFC1038D9A69F80ECCAB8E/100', '1.0', null, null, '6D83CE7B4EEFC1038D9A69F80ECCAB8E', '1');
INSERT INTO `xjm_user` VALUES ('107', '冷眸丶', '', '0', '0.00', '0', '1512355659', '0', 'http://q.qlogo.cn/qqapp/1106555152/F4994FDA83B815541E378216AA5520D7/100', '1.0', null, null, 'F4994FDA83B815541E378216AA5520D7', '1');
INSERT INTO `xjm_user` VALUES ('108', 'Kevin Durant', '', '0', '0.00', '0', '1512447060', '0', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLruKArGrtM9LGIWEyM9h2h9jGICcn5CqTiaD5n29NpIZ5r4A2HosRQ8645l6zJFLJFIhCFre70khg/0', '1.0', null, null, 'oS5GbwmbAwJHetZZqV9cLzkjeU8M', '3');
INSERT INTO `xjm_user` VALUES ('109', '', '18453040836', '0', '0.00', '0', '1512535100', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('111', 'yaoyoo--瑶瑶--', '', '0', '0.00', '0', '1512551411', '0', 'http://tva1.sinaimg.cn/crop.0.13.1536.1536.180/8b2e3573jw8f645jvyu3zj216o17ftcv.jpg', '1.0', null, null, '2335061363', '2');
INSERT INTO `xjm_user` VALUES ('112', '尼快哭了', '', '0', '0.00', '0', '1512553718', '0', 'http://tva3.sinaimg.cn/crop.110.0.414.414.180/a14073a1jw8f9lnub32w8j20fu0ejgom.jpg', '1.0', null, null, '2705355681', '2');
INSERT INTO `xjm_user` VALUES ('113', '测试6171487802', '', '0', '0.00', '0', '1512554320', '0', 'https://tvax3.sinaimg.cn/default/images/default_avatar_male_180.gif', '1.0', null, null, '6171487802', '2');
INSERT INTO `xjm_user` VALUES ('114', '测试测试测试', '15005157933', '0', '0.00', '0', '1512554909', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto11420171206180942', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('115', '', '13076949806', '0', '0.00', '0', '1512563099', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('116', 'Leslie迷妹', '', '0', '0.00', '0', '1512570921', '0', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTL8nR7kBpJIFZkrickRsBxgwSb1ZQ4U8kMamtRlVsSF5EnfnD3rLdXUicZiaMVtcQINUpamRuE4mlU1Q/0', '1.0', null, null, 'oS5GbwqvAKWAtUsIio6gzKQpqk0s', '3');
INSERT INTO `xjm_user` VALUES ('117', '', '15271819735', '0', '0.00', '0', '1512574541', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('118', '', '18193863911', '0', '0.00', '0', '1512582554', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('119', '', '18986217203', '0', '0.00', '0', '1512636753', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('120', '就这样吧', '', '0', '0.00', '0', '1512637150', '0', 'http://wx.qlogo.cn/mmopen/vi_32/JknK3zUQxm6iaHzM6dnHKsT3SHM1nA7Bvf18D2RVyFQrAxQhNtcoVIKOibT3J3Q2ffC3riaHephVl3mvS8aquIHOA/0', '1.0', null, null, 'oS5GbwpidN1SNuoknXQaOhd8kCjs', '3');
INSERT INTO `xjm_user` VALUES ('121', '中毒太深99', '', '0', '0.00', '0', '1512637160', '0', 'http://tva2.sinaimg.cn/crop.0.0.1080.1080.180/006cv6QPjw8ex0w230myhj30u00u0q66.jpg', '1.0', null, null, '5681527531', '2');
INSERT INTO `xjm_user` VALUES ('122', '我造2010', '', '0', '0.00', '0', '1512641339', '0', 'http://tvax2.sinaimg.cn/crop.0.0.480.480.180/006KDHcTly8fi5hnf5z6pj30dc0dc74z.jpg', '1.0', null, null, '6185969331', '2');
INSERT INTO `xjm_user` VALUES ('123', '', '13901708247', '0', '0.00', '0', '1512705195', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('124', '', '13507136556', '0', '0.00', '0', '1512730654', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('125', '', '15927365686', '0', '0.00', '0', '1512737410', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('126', '小灰灰', '13035110308', '0', '0.00', '0', '1512742824', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto12620171208222146', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('127', '祁燕', '', '0', '0.00', '0', '1512871912', '0', '', '1.0', null, null, 'oS5GbwloPhxueB8ufpkEA0Raocbs', '3');
INSERT INTO `xjm_user` VALUES ('128', '我有一个小小的目标。', '', '0', '0.00', '0', '1512872412', '0', 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLkM8K2NHX5icS4WrfuOkU9fM17nXeVFhnwlFZZaQhiaeC9cMicuicTzJoMBe7icqDasiaia8poUD3jIb7ibQ/0', '1.0', null, null, 'oS5GbwjZlIl6XCLNv11OGhHs3Lh0', '3');
INSERT INTO `xjm_user` VALUES ('129', 'Mark先森', '', '0', '0.00', '0', '1512884824', '0', 'http://wx.qlogo.cn/mmopen/vi_32/9VQ7VaNh8M1w2XdrdJaSDicFQX4tFfZ7oa2s6IjMT3ne4jGQzHsPricsRBaiaToRy9fRAfqs2RbQDzjaiaWIhvAh5g/0', '1.0', null, null, 'oS5GbwmESxE257CVYc7_Q969arU0', '3');
INSERT INTO `xjm_user` VALUES ('130', '', '13380347170', '0', '0.00', '0', '1512890882', '0', '', '1.0', null, '131', null, '0');
INSERT INTO `xjm_user` VALUES ('131', '', '18972610481', '0', '0.00', '0', '1512891101', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('132', '', '18193147368', '0', '0.00', '0', '1512923435', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('133', '用户5974880107', '', '0', '0.00', '0', '1513237202', '0', 'http://tvax4.sinaimg.cn/default/images/default_avatar_male_180.gif', '1.0', null, null, '5974880107', '2');
INSERT INTO `xjm_user` VALUES ('134', 'ggbb', '', '0', '0.00', '0', '1513237209', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto13420171214154158', '1.0', null, null, 'oS5Gbwt1VxrcVHHdb0SjJOjb-goA', '3');
INSERT INTO `xjm_user` VALUES ('135', '', '17601550741', '0', '0.00', '0', '1513237255', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('136', '一切安好', '', '0', '0.00', '0', '1513250486', '0', 'http://q.qlogo.cn/qqapp/1106555152/B7660043F44A4CD9DCABC04E4DBA1665/100', '1.0', null, null, 'B7660043F44A4CD9DCABC04E4DBA1665', '1');
INSERT INTO `xjm_user` VALUES ('137', '张', '', '0', '0.00', '0', '1513283457', '0', 'http://wx.qlogo.cn/mmopen/vi_32/7QdogyrATxUDbSnULrvOZfKErolOSgNh7d1asIzb21lH8ical8IO3qq2BvZlujOLUPwJxj9ekRZCibONOapSjb3g/0', '1.0', null, null, 'oS5GbwiN45UNKi4GSmqf9lhs4-xU', '3');
INSERT INTO `xjm_user` VALUES ('138', '凯凯', '', '0', '0.00', '0', '1513288535', '0', 'http://wx.qlogo.cn/mmopen/vi_32/Q3auHgzwzM5iclib2aLxJzkd1ASiamBI4yjG8icChsuYjg6mNFQdaZhOgsJxIxYicMe1ickia23g2Vr1MrHNMBXd7Xxxw/0', '1.0', null, null, 'oS5GbwjE1FpQDJH63hR-KEGJmNVs', '3');
INSERT INTO `xjm_user` VALUES ('139', '向日葵', '', '0', '0.00', '0', '1513349251', '0', 'http://wx.qlogo.cn/mmopen/vi_32/9yxKEm5h5IcSSchatcvQgoJhDapHXyXUHSvGyIOP7ibMDvpCEo611Gic7RHCklib1BE4cjOcDU9zfdINqrymDBtkw/0', '1.0', null, null, 'oS5Gbwm36HUKQycIILOB-rffQ5IM', '3');
INSERT INTO `xjm_user` VALUES ('140', '', '13296521863', '0', '0.00', '0', '1513429406', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('141', 'A 硕利科技 张鹏', '', '0', '0.00', '0', '1513435313', '0', 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIiagG8gyusGAbSibfNcyldkTVicWafvVKa0BahfC6OUpVWa3jU5fvmncHH0VKHwnHazOGgcVYMKF0vA/0', '1.0', null, null, 'oS5GbwsyRLsYEJKum-05-azhzN4o', '3');
INSERT INTO `xjm_user` VALUES ('142', 'y', '', '0', '0.00', '0', '1513453206', '0', 'https://q.qlogo.cn/qqapp/1106555152/C26C93107B3C912A63BFC6C7BFCD9003/100', '1.0', null, null, 'C26C93107B3C912A63BFC6C7BFCD9003', '1');
INSERT INTO `xjm_user` VALUES ('143', '', '18672778800', '0', '0.00', '0', '1513490415', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('144', '', '17714571801', '0', '0.00', '0', '1513591888', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('145', '孙小困困', '', '0', '0.00', '0', '1513591929', '0', 'http://tva3.sinaimg.cn/crop.0.0.100.100.180/006cZ51tjw8evjb4axkyxj302s02sjr7.jpg', '1.0', null, null, '5688670343', '2');
INSERT INTO `xjm_user` VALUES ('146', '', '18505571981', '0', '0.00', '0', '1513619490', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('147', '', '13970153505', '0', '0.00', '0', '1513649295', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('148', '', '18627031757', '0', '0.00', '0', '1513663239', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('149', 'Yyyyy。', '', '0', '0.00', '0', '1513762906', '0', 'https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83eombxQmxstNbKqVyde96ZMibsd1jbKef74y1Ol6X2Bjicsx8gIjBTEOIibSJUblQKZRLtylZWULS2BNw/0', '1.0', null, null, 'oS5GbwjYuS33oolUDeoDA4eSMt8M', '3');
INSERT INTO `xjm_user` VALUES ('150', '', '15822355865', '0', '0.00', '0', '1513783672', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('151', '', '13631294998', '0', '0.00', '0', '1513784283', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('152', 'LUCKY', '', '0', '0.00', '0', '1513791407', '0', 'https://wx.qlogo.cn/mmopen/vi_32/icHtHbgg42hiaIiahoSy0Um6yicJhXQOWiacqwIvhbD6QZN7an3ohByeUo47icniaOEsltpsdIO4flReg46SgRlfLcNOw/0', '1.0', null, null, 'oS5GbwnFCGFF51Wzn4le2SYY3fkI', '3');
INSERT INTO `xjm_user` VALUES ('153', '陈云', '', '0', '0.00', '0', '1514040220', '0', 'https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83epes3ZiaXIVj9x8XhmHvcJj0tkgWkrwVibfMKibVQ5eSfxQc0jBP3tGcuyT3AjDcAVRTPzQQR29PoNhg/0', '1.0', null, null, 'oS5Gbwu_N-7zIdW0AXSApDAgw2eU', '3');
INSERT INTO `xjm_user` VALUES ('154', '', '13197984282', '0', '0.00', '0', '1514108079', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('155', '', '13430864796', '0', '0.00', '0', '1514193156', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('156', '', '18632292112', '0', '0.00', '0', '1514204858', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('157', '黄诚芯', '', '0', '0.00', '0', '1514212179', '0', 'http://wx.qlogo.cn/mmopen/vi_32/w6IxFlVGnib1jtAam6Zgm476FOd76Tmsjou3AyWtL2usdichI0gvyyhVBR43bKkqics1cQYbLXVGcNWicbMrlSH8uQ/0', '1.0', null, null, 'oS5GbwulP9PhenboT6HmbMyhpG5Y', '3');
INSERT INTO `xjm_user` VALUES ('158', '互联网俊明说', '', '0', '0.00', '0', '1514354984', '0', 'https://wx.qlogo.cn/mmopen/vi_32/Q3auHgzwzM4cjD6mH7AR4yAEVB6s5RtygypJhMbQLIF1dLwiauAlcG3x3BTZe3epmJPPBKXxYdrjubGr6OK1qCw/0', '1.0', null, null, 'oS5GbwmPDrB9XTQattfExM6FMJoU', '3');
INSERT INTO `xjm_user` VALUES ('159', '', '18603957292', '0', '0.00', '0', '1514355132', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('160', '大成若为Sutton', '', '0', '0.00', '0', '1514356445', '0', 'http://wx.qlogo.cn/mmopen/vi_32/r4TUQa9wDLZeNNYxdqnh3mO4XMhXM42P0qXA7ibl3wOwu9FRQbmIibWNVv9tFrpbZjEeqfGhY7w1ggodibZHTH1ug/0', '1.0', null, null, 'oS5GbwnZ8GU-jIkkzRqxBf3AaD4Y', '3');
INSERT INTO `xjm_user` VALUES ('161', '', '18601959642', '0', '0.00', '0', '1514356623', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('162', '', '15506802101', '0', '0.00', '0', '1514356691', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('163', '李男', '', '0', '0.00', '0', '1514356866', '0', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIUh1ico3KxNHUIEfFiaB4fEnN0BmH2oUHggkXQg5rZV6ROXNzbJJHOIlMTEH57iaf5QmoHymMwNjmwQ/0', '1.0', null, null, 'oS5GbwvvM9fUMDEnavdIUUuS1Kic', '3');
INSERT INTO `xjm_user` VALUES ('164', 'SALADCOOL-AsaGuan', '', '0', '0.00', '0', '1514356903', '0', 'https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83erv4laTLA2upWS6KiaB3WmMPKmZewItcpXaR8m6F3XFJ9hK4RJFl16sY9Dx9j5icMrXwk91robRh6kA/0', '1.0', null, null, 'oS5GbwuWv_Rqx-L3vZYi5PpgWJdk', '3');
INSERT INTO `xjm_user` VALUES ('165', '', '15005373955', '0', '0.00', '0', '1514357567', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('166', 'Heart!Heart!', '', '0', '0.00', '0', '1514359605', '0', 'https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83eroqJEibh9fzG2MkfMILuDONpom8HhA5Nf32yYdsyuaEd1ELQ3ZArLTWtAbQSqBia6U3yYz2zcCzD9A/0', '1.0', null, null, 'oS5GbwifoOkZXxM1SSqUtEUuewB0', '3');
INSERT INTO `xjm_user` VALUES ('167', '', '15814012380', '0', '0.00', '0', '1514360073', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('168', '彼岸花开', '', '0', '0.00', '0', '1514366081', '0', 'http://wx.qlogo.cn/mmopen/vi_32/xnCXLvFQxhq6aKnn4bFKbadOUQmyVF8IEQxYMic6frmTOq4xwQJ4lrRZ0REcn4Td7g4ruQYnOhkxfHmCLafDxAw/0', '1.0', null, null, 'oS5Gbwv8CQBLyqIXpcx4tG5Y8rzc', '3');
INSERT INTO `xjm_user` VALUES ('169', '阿秀', '', '0', '0.00', '0', '1514369098', '0', 'http://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoeTJtGtIznjmuj1XsMwjlgQnOOo083RsKcDrQ2iajb394OzCsPvDGqqy66laocLYVNcOG8EDOtMsQ/0', '1.0', null, null, 'oS5GbwrawD55ShjqfS6m6BPOzkX4', '3');
INSERT INTO `xjm_user` VALUES ('170', '自由革命', '', '0', '0.00', '0', '1514369323', '0', 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIAP37bk1eVxB7VibESyDEbY3aImvIcNhedHmcXWuxU3XibTlgfE7t10wvGL12N1MicriacgUiapEso4ZQ/0', '1.0', null, null, 'oS5GbwrJWfvrKXwaB1FB6RgWhBdY', '3');
INSERT INTO `xjm_user` VALUES ('171', '王超', '', '0', '0.00', '0', '1514369467', '0', 'http://wx.qlogo.cn/mmopen/vi_32/a0Fdos3Rhge3BhVbY7ETHw9TqXTGCJ5PCFnWgbmbcyicLegoCCr4GyNxibjE38NpJicTL9KBxZibicMjyMHxhcJKiaiaw/0', '1.0', null, null, 'oS5GbwhROETx0tcFEQSniahg1wZ0', '3');
INSERT INTO `xjm_user` VALUES ('172', '就叫我霸霸吧', '', '0', '0.00', '0', '1514370989', '0', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIPBMibXbtZ4ib0gh3S1dpqS321ibrsr47ibja8QmUBR1YnFolOcFD8pibO5o7GibdkyHdoyLIOkIn1f4Ng/0', '1.0', null, null, 'oS5GbwpVBsQrRtk_wVyKeydyQV4Q', '3');
INSERT INTO `xjm_user` VALUES ('173', '师小曼', '', '0', '0.00', '0', '1514372668', '0', 'http://tva2.sinaimg.cn/crop.0.0.664.664.180/ae4d2053jw8f1bt0h8ztmj20ig0igwfu.jpg', '1.0', null, null, '2924290131', '2');
INSERT INTO `xjm_user` VALUES ('174', '', '15899816747', '0', '0.00', '0', '1514375071', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('175', 'Selicover', '', '0', '0.00', '0', '1514387484', '0', 'http://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83ersjDMOiaprMknb0Q8wPzJB57jIXPeVI0dPKSvibAq6gV2uFEQ5dlxOq3wSebyWUrSFq9rHuKicpv11g/0', '1.0', null, null, 'oS5GbwgY3SoTAQeb6sD0OjGEEgVs', '3');
INSERT INTO `xjm_user` VALUES ('176', '三千海世界', '', '0', '0.00', '0', '1514424259', '0', 'https://tva3.sinaimg.cn/crop.0.0.640.640.180/a52fa0fdjw8ea87cfuo5oj20hs0hs3zr.jpg', '1.0', null, null, '2771362045', '2');
INSERT INTO `xjm_user` VALUES ('177', '', '18603287673', '0', '0.00', '0', '1514441252', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('178', '', '15957858818', '0', '0.00', '0', '1514455242', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('179', '太浪゛ོ', '', '0', '0.00', '0', '1514455859', '0', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTK23Dne4QcF4m6eJ5D0DJeT0GG6mNwoOUYuVXLR2R7S0eNHcMhia5WtkgbzfF7fATk5fIX1OZyJ7Gw/0', '1.0', null, null, 'oS5GbwgHVpEpngx_HmM2zC55EyOw', '3');
INSERT INTO `xjm_user` VALUES ('180', '', '13050922258', '0', '0.00', '0', '1514550340', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('181', '静心', '', '0', '0.00', '0', '1514614941', '0', 'http://wx.qlogo.cn/mmopen/vi_32/ehxr61ajoCyv8dwwvapYPsnDiamMn1JBSegoYRV92ibktHyN2z5dDCDJ2Y6N4icI3kxGj2QXlVmn2F2QzkAwvfCHg/0', '1.0', null, null, 'oS5GbwnW7vL92zQlW088EuFvyVY0', '3');
INSERT INTO `xjm_user` VALUES ('182', '', '18030608882', '0', '0.00', '0', '1514646605', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('183', '纯阳', '15251827456', '0', '0.00', '0', '1514723591', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('184', '', '13554307586', '0', '0.00', '0', '1514747076', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('185', '', '15626112352', '0', '0.00', '0', '1514766224', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('186', '鲁卡利欧Luka', '', '0', '0.00', '0', '1514904193', '0', 'http://tvax2.sinaimg.cn/crop.0.0.1328.1328.180/7980786fly8fgwqw2qpq9j210w10wn0c.jpg', '1.0', null, null, '2038462575', '2');
INSERT INTO `xjm_user` VALUES ('187', '一切随风', '', '0', '0.00', '0', '1514916459', '0', 'http://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83epSlfIRkJG9zWjibkHnhMFqhoyztnxyNrrfbibYQnxgkf1bibicibricxMunsDLqkib4w6fdkLFxI0CvWbmw/0', '1.0', null, null, 'oS5GbwuvDoqORDdr0gxLOv58Dr_A', '3');
INSERT INTO `xjm_user` VALUES ('188', '', '17600699450', '0', '0.00', '0', '1514943427', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('189', '郭鹏', '', '0', '0.00', '0', '1514962550', '0', 'http://wx.qlogo.cn/mmopen/vi_32/9CukKchWG4HfIE4PloYDugo5IAozNjibwgfmVAlYjmia15R3QpmaAJ82ib9W5TJTqmjj14y9by5xA27xg4TV9ECjg/0', '1.0', null, null, 'oS5GbwqVDtm_ki4H6_v0k5ccmkgs', '3');
INSERT INTO `xjm_user` VALUES ('190', '', '13135825001', '0', '0.00', '0', '1515073440', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('191', '', '18321503886', '0', '0.00', '0', '1515077188', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('192', 'cirrus', '', '0', '0.00', '0', '1515120693', '0', 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKj6ecfIWrvzzJHrOkUD5saUtqsv2WXj6f24QjTcUgNdyiaDEicGib3WnmaC7vKQY21V9ACF6hVvM6pA/0', '1.0', null, null, 'oS5Gbwn9xXuwcbf8o1G01ngsnijc', '3');
INSERT INTO `xjm_user` VALUES ('193', '再见少年', '', '0', '0.00', '0', '1515137885', '0', 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIiajNicvAaBNu32cSTJjMwJ2wbQOpmCibBDAOOZXibpSvzmOVoROfL4BtQ3OZ8dEnBLD9rRCrkNqENWA/0', '1.0', null, null, 'oS5GbwvtEM50EWunsGBuknXhL1_Y', '3');
INSERT INTO `xjm_user` VALUES ('194', '', '17605210525', '0', '0.00', '0', '1515149319', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('195', '', '18779817793', '0', '0.00', '0', '1515237663', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('196', '', '18168578527', '0', '0.00', '0', '1515263754', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('197', '文浩 招商加盟竞价VIP框架返点户', '', '0', '0.00', '0', '1515330778', '0', 'https://wx.qlogo.cn/mmopen/vi_32/D78r41icgWib3zEFl0GWMM69xA27sKjonbUAVlqiaBDuDI2ksDllRNfWsYscussjEKYTsBsTD18oGQDuDqBK7bSQg/0', '1.0', null, null, 'oS5Gbwlm5h4WWr-bfG0ZRru1omDI', '3');
INSERT INTO `xjm_user` VALUES ('198', '', '15952675090', '0', '0.00', '0', '1515566537', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('199', '合兴（松兴包装）', '', '0', '0.00', '0', '1515739861', '0', 'http://wx.qlogo.cn/mmopen/vi_32/q5ykHyRrke7dodn5kwaSzB1tegWns1KKr9KGROVicgrjX6n6icAMVMJXabFUngtKs8kJO8RdUJo6arAdoHr91c3Q/132', '1.0', null, null, 'oS5GbwsJ3db6eEI2ox1msiW-oo7k', '3');
INSERT INTO `xjm_user` VALUES ('200', '', '13971666324', '0', '0.00', '0', '1515765796', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('201', '', '13702122632', '0', '0.00', '0', '1515850337', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('202', '', '13001733030', '0', '0.00', '0', '1515866034', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('203', '庆', '', '0', '0.00', '0', '1516108803', '0', 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJLDlf0jFT20e5THHfTH9VcKQStxFHMC9OANKxpRm5wcwDib7BCWXl2hcVxFJFukGyfVfEYgYN20Mg/132', '1.0', null, null, 'oS5GbwrCs224YsQtFAjFpXMEP_s4', '3');
INSERT INTO `xjm_user` VALUES ('204', '', '18500399977', '0', '0.00', '0', '1516201265', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('205', 'Aller°', '', '0', '0.00', '0', '1516260095', '0', 'https://q.qlogo.cn/qqapp/1106555152/26F6B18D9FC920959C06827D73EC2EF4/100', '1.0', null, null, '26F6B18D9FC920959C06827D73EC2EF4', '1');
INSERT INTO `xjm_user` VALUES ('206', '  沉寂丶', '', '0', '0.00', '0', '1516261881', '0', 'http://q.qlogo.cn/qqapp/1106555152/8B559073B6D3378FE6FF7AE7D0D46AB1/100', '1.0', null, null, '8B559073B6D3378FE6FF7AE7D0D46AB1', '1');
INSERT INTO `xjm_user` VALUES ('207', '123', '', '0', '0.00', '0', '1516262359', '0', 'http://q.qlogo.cn/qqapp/1106555152/E7AC0F4211B033681EEB2CEEBABB804B/100', '1.0', null, null, 'E7AC0F4211B033681EEB2CEEBABB804B', '1');
INSERT INTO `xjm_user` VALUES ('208', '', '18618416795', '0', '0.00', '0', '1516269555', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('209', '夜鸦castle', '', '0', '0.00', '0', '1516333322', '0', 'http://wx.qlogo.cn/mmopen/vi_32/THmWUHqeVmibCXcbaiay56LPAgAQRCysSFt6IdiaFmJqENF3wtRg79QgrIPqDKPia5FSIgpDjh8OELwfMQ28CNFybg/132', '1.0', null, null, 'oS5GbwgI-coq2rIzrVaK4NLoC0oI', '3');
INSERT INTO `xjm_user` VALUES ('210', '夜鸦castle', '', '0', '0.00', '0', '1516333334', '0', 'http://tvax3.sinaimg.cn/crop.0.0.400.400.180/006DCwcQly8fmpf17u7hoj30b40b4jrc.jpg', '1.0', null, null, '6082254364', '2');
INSERT INTO `xjm_user` VALUES ('211', '夜鸦', '', '0', '0.00', '0', '1516333345', '0', 'http://q.qlogo.cn/qqapp/1106555152/1977E58EBB05346AA9D97D7C120E3F10/100', '1.0', null, null, '1977E58EBB05346AA9D97D7C120E3F10', '1');
INSERT INTO `xjm_user` VALUES ('212', '枫叶丶林湾', '18551780142', '0', '0.00', '0', '1516333364', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto21220180119114256', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('213', '', '18991634357', '0', '0.00', '0', '1516505155', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('214', '高源~北京自然博物馆', '', '0', '0.00', '0', '1516549540', '0', 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIpYoDe6gaZwZtMR9anwxiafOLLhELKjVLDWKmkRSoM4Hk6FocYwmCpr6HHRfFic4swDoN5aaTzeCmA/132', '1.0', null, null, 'oS5GbwiJ_zJe82k_PWghebwAl6L4', '3');
INSERT INTO `xjm_user` VALUES ('215', '', '15204002225', '0', '0.00', '0', '1516588582', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('216', '天降紫峰', '', '0', '0.00', '0', '1516613208', '0', 'http://wx.qlogo.cn/mmopen/vi_32/Xjxmoxk9MzswribVQhNSxOYJ3B8PRoaibm8zWfHKQCE3jv5xmEMRcBZrLibkf5WV9Hpqic15uzxgRiaSjF7Cq95uFeQ/132', '1.0', null, null, 'oS5GbwiKeu5Xrzk73ex4GatRuEn0', '3');
INSERT INTO `xjm_user` VALUES ('217', 'cick聪', '', '0', '0.00', '0', '1516690732', '0', 'https://tva2.sinaimg.cn/crop.0.0.640.640.180/880593e5jw8ebc3h2lp61j20hs0hsdgd.jpg', '1.0', null, null, '2282066917', '2');
INSERT INTO `xjm_user` VALUES ('218', '', '18607292768', '0', '0.00', '0', '1516704916', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('219', '', '17762826373', '0', '0.00', '0', '1517015882', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('220', '浪离格浪', '', '0', '0.00', '0', '1517464702', '0', 'http://q.qlogo.cn/qqapp/1106555152/6DC97E74A7D8159FE90C24AFC3B4EFF4/100', '1.0', null, null, '6DC97E74A7D8159FE90C24AFC3B4EFF4', '1');
INSERT INTO `xjm_user` VALUES ('221', '元仔哥哥。', '', '0', '0.00', '0', '1517523490', '0', 'http://wx.qlogo.cn/mmopen/vi_32/5u9ibEU2pUTFr7EqAqQKoO8W265bMOkNIvLr2MzLErQ5HG70IG7hvfCbovxkpibiakRY4skAlvZJhVt0mSgUchpiaw/132', '1.0', null, null, 'oS5Gbwh73kV4t3BWUG9IM6hTHOhk', '3');
INSERT INTO `xjm_user` VALUES ('222', '一路向北', '', '0', '0.00', '0', '1517652571', '0', 'http://wx.qlogo.cn/mmopen/vi_32/JjUKp1ssYwSvy7QS5TpXS3RibX6hbQoQiacAvEbbkhKKFjiaIwO8o66ysIOPRPYkEUCMypgth9oacb7W56vFzyR2Q/132', '1.0', null, null, 'oS5GbwivK1SOo3zKc55ByeKnPj2E', '3');
INSERT INTO `xjm_user` VALUES ('223', '', '13453180000', '0', '0.00', '0', '1517667952', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('224', '.', '', '0', '0.00', '0', '1518247213', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/vVPpfFH7OnX7GGDtEMpAZqukNQksSxu8xibibSyl1ibxFicq4Uj8BhuoR8dNyAAutBKhExZ0Sva7lKJkwOassrBLSQ/132', '1.0', null, null, 'oS5Gbwi90IpgZIEDzduED44_Ffy8', '3');
INSERT INTO `xjm_user` VALUES ('225', '王大锤 ，', '', '0', '0.00', '0', '1518364699', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoiakMbkJ0RdtLnmjx8qu2BbpqLsWoP1yWMGrthTPK85SnGJYxiamqA4hg4icKt7JX7v8WP54zDMdCFg/132', '1.0', null, null, 'oS5GbwkO6EhjH4N2P7NpbVgpIY3U', '3');
INSERT INTO `xjm_user` VALUES ('226', '', '18166993953', '0', '0.00', '0', '1518436858', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('227', '桂花真香', '', '0', '0.00', '0', '1518512618', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/wrIOw6jasWXEIGBl9YakDs9XthzlDnTbw03k8LFzewroiclFUUrZbVW60orAoIqSibz8z8KoEmM1qtiaPhG83JDtA/132', '1.0', null, null, 'oS5GbwsRrzkaC8IHyq7ohcWxMPDI', '3');
INSERT INTO `xjm_user` VALUES ('228', '', '', '0', '0.00', '0', '1518572228', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTI61B1rsld0jhm0iaQrf5nrIpeoB5x1cSejaGz9QZVqt9oicLnOXW6jI9GYBMgJRA0G4WmbAJfy92eA/132', '1.0', null, null, 'oS5GbwoZr2PMvu3cvBujF5YXJqDk', '3');
INSERT INTO `xjm_user` VALUES ('229', '~', '', '0', '0.00', '0', '1518801958', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJ9qgFwDM9fwcKj7vxXicOeyCIibwtFyXYG1N5NpiaxLpK8CByl0EBgFLBsyZpE7rRJP0iav8flb24CGQ/132', '1.0', null, null, 'oS5GbwuX71ENxyofBo--6G_YLsDE', '3');
INSERT INTO `xjm_user` VALUES ('230', '', '13918215518', '0', '0.00', '0', '1518849529', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('231', '明月几时有？', '', '0', '0.00', '0', '1518955955', '0', 'http://q.qlogo.cn/qqapp/1106555152/9DF7B4C150936D70E31B0011B8136BFF/100', '1.0', null, null, '9DF7B4C150936D70E31B0011B8136BFF', '1');
INSERT INTO `xjm_user` VALUES ('232', '关忆北', '', '0', '0.00', '0', '1518966843', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIAqVYWPkszmOC0hlH4W80nhkrmWvV6BufaJ7KruJfIfMZCxhfqY9jzPWtzW5DRo6CVHS4N3SMCibA/132', '1.0', null, null, 'oS5Gbwt7UlkqF2fvETu-se7qIBvs', '3');
INSERT INTO `xjm_user` VALUES ('233', '葫芦娃', '', '0', '0.00', '0', '1518998566', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTI6RyUV9HjEcKeohEIPeSVTMacVAicOnffwt9jvvhZR7l0rKA1sgrd9IoIgewQt9zWJykgn4fkXd0Q/132', '1.0', null, null, 'oS5Gbwt5PMzRZ7_VZKHPzhzj_IvM', '3');
INSERT INTO `xjm_user` VALUES ('234', '', '15719646392', '0', '0.00', '0', '1519116586', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('235', '橘子巷', '', '0', '0.00', '0', '1519204599', '0', 'http://q.qlogo.cn/qqapp/1106555152/CD9696019409B526A358C3D8FC1203C7/100', '1.0', null, null, 'CD9696019409B526A358C3D8FC1203C7', '1');
INSERT INTO `xjm_user` VALUES ('236', '影子', '', '0', '0.00', '0', '1519275356', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJBy1ncEovhjHXpfYaunBGYslV5uB88XiajYYVTBGDEhe85q4thCMWdEDAe5ibItbsYiaX7Qib4faVU4w/132', '1.0', null, null, 'oS5GbwvHhZDtG80OCIU6OrVkso0Y', '3');
INSERT INTO `xjm_user` VALUES ('237', '', '13297182539', '0', '0.00', '0', '1519310409', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('238', '', '18728105694', '0', '0.00', '0', '1519348873', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('239', '等风奕等你', '', '0', '0.00', '0', '1519395270', '0', 'http://q.qlogo.cn/qqapp/1106555152/6600CE9E317A6EC1B6AC794474FF1B78/100', '1.0', null, null, '6600CE9E317A6EC1B6AC794474FF1B78', '1');
INSERT INTO `xjm_user` VALUES ('240', '大胖', '', '0', '0.00', '0', '1519398634', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTL0TUd3c35KY43hKhRqx3Nr7DS9KsEUJtXhBepBe802N6FW8fMTTWmUc99JZctEpFhHy2oegrcIjQ/132', '1.0', null, null, 'oS5GbwlC-tnebcGmWgYafMuplyVY', '3');
INSERT INTO `xjm_user` VALUES ('241', '柳勤民', '17340363858', '0', '0.00', '0', '1519425903', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('242', '归来~忧伤', '', '0', '0.00', '0', '1519610179', '0', 'http://q.qlogo.cn/qqapp/1106555152/DD7B96B8AEBF408CE6E22707C15485F6/100', '1.0', null, null, 'DD7B96B8AEBF408CE6E22707C15485F6', '1');
INSERT INTO `xjm_user` VALUES ('243', '', '18614020570', '0', '0.00', '0', '1519719673', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('244', '北境南城', '', '0', '0.00', '0', '1519734506', '0', 'http://q.qlogo.cn/qqapp/1106555152/0CB4D08651F7D7369772B938EEC5EEEE/100', '1.0', null, null, '0CB4D08651F7D7369772B938EEC5EEEE', '1');
INSERT INTO `xjm_user` VALUES ('245', '不二臣、', '', '0', '0.00', '0', '1519736528', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKW5o3HEmUL0RIm9kh8adJXibaouk7lCfK9dDo7ricYF9eJuHfLXicCcYWic2qGxy4BkLePI7caEuf5jw/132', '1.0', null, null, 'oS5Gbws58jEMOEF8m7S5S1_JfOic', '3');
INSERT INTO `xjm_user` VALUES ('246', '肖文', '', '0', '0.00', '0', '1519736753', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/RscZklXjiatW5AsCrHkMiak3gRdryWPcKhQUR8DSXmWbbmniaTs2qBhx2qMAujV1C4D9lT7r3kZMCWDK8TKm9iaecQ/132', '1.0', null, null, 'oS5GbwmbNNZ4Ax-rRxADPC2OSXew', '3');
INSERT INTO `xjm_user` VALUES ('247', '', '18942940350', '0', '0.00', '0', '1519753109', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('248', '', '18201115073', '0', '0.00', '0', '1519904841', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('249', 'Freedomᕙ', '', '0', '0.00', '0', '1519916945', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DymibIQk3UicF03CibV2iavxvTdG5DIQtLu4gWZFOEzIhKW7wmzSSpDRQFnZkpFbKO1pbOO7O9UgVic3rQd9t7SkTCg/132', '1.0', null, null, 'oS5Gbwnw_IWLiBtTyPugun5wSZDM', '3');
INSERT INTO `xjm_user` VALUES ('250', '', '', '0', '0.00', '0', '1520176109', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/MM1qhGvrmQibogoib9nlFCSOUMeFOG8ZJ7KIGIvRMQw7Vs7UDtwIckoc4gVd4VSVnQpPF580q4Vndmfgiba9UgbCA/132', '1.0', null, null, 'oS5GbwpEUX82U2gPAba5R3GF5_Hw', '3');
INSERT INTO `xjm_user` VALUES ('251', '华哥仔', '', '0', '0.00', '0', '1520276438', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKxAuEbHF5mcly833IHYf6icpgkfVyrF0jpS6WibuVtdVRjOwGnpBD3Gn8mmuH8ZbhFP6Fdyvm2lrKw/132', '1.0', null, null, 'oS5Gbwi12kXEKD9TiG9mtFbwGDJU', '3');
INSERT INTO `xjm_user` VALUES ('252', '', '18169114731', '0', '0.00', '0', '1520346578', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('253', '凤舞', '', '0', '0.00', '0', '1520477062', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/wmPGM1ySibHsOeaPXfHxZgVgrCc0gdibqzkZ1o76HtuGUibvKSTTpbTVKr3EdibvKBRcNRxC4TFbmdYic50Qar4JSTw/132', '1.0', null, null, 'oS5GbwuEUbepRBnya5Azt3jpxqN4', '3');
INSERT INTO `xjm_user` VALUES ('254', '邱少', '', '0', '0.00', '0', '1520537055', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/057241C7E9B583712AD6B55F0B999C0D/100', '1.0', null, null, '057241C7E9B583712AD6B55F0B999C0D', '1');
INSERT INTO `xjm_user` VALUES ('255', '惜缘', '', '0', '0.00', '0', '1520591620', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/2F6874E5E693593782D6419ACA2462E8/100', '1.0', null, null, '2F6874E5E693593782D6419ACA2462E8', '1');
INSERT INTO `xjm_user` VALUES ('256', '', '13214378903', '0', '0.00', '0', '1520596668', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('257', '', '15315398669', '0', '0.00', '0', '1520609827', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('258', '', '17778411057', '0', '0.00', '0', '1520692767', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('259', '杏儿', '', '0', '0.00', '0', '1520716953', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/E50E7D548B83276DF3C9A17D8E1C83C9/100', '1.0', null, null, 'E50E7D548B83276DF3C9A17D8E1C83C9', '1');
INSERT INTO `xjm_user` VALUES ('260', '', '13297997898', '0', '0.00', '0', '1520953839', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('261', '我在忍耐，一直在忍耐', '', '0', '0.00', '0', '1521008938', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/JsxBFkwGE1icTkg6ibO5cJGOHza69fb8Z7ozPHAzR34d65hflicYDTUzA3RSfoGrzpOtw8mtgKS6UibGmVx9a6xt7A/132', '1.0', null, null, 'oS5GbwvQNIqQgMrHZb-sUvj0pICg', '3');
INSERT INTO `xjm_user` VALUES ('262', '曾福全', '', '0', '0.00', '0', '1521044806', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/C48A5EE2E8CC382ED56865708B4F8808/100', '1.0', null, null, 'C48A5EE2E8CC382ED56865708B4F8808', '1');
INSERT INTO `xjm_user` VALUES ('263', '星耀 客服', '', '0', '0.00', '0', '1521226592', '0', 'https://thirdqq.qlogo.cn/qqapp/1106555152/63FBFF02A623BAE5A39C948A2A5A7A2D/100', '1.0', null, null, '63FBFF02A623BAE5A39C948A2A5A7A2D', '1');
INSERT INTO `xjm_user` VALUES ('264', '', '15619068108', '0', '0.00', '0', '1521262525', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('265', '', '15926300132', '0', '0.00', '0', '1521272895', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('266', '喻豪', '', '0', '0.00', '0', '1521272971', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/J2etic197092g7OULggibxZbAk0ribCxytMaUwyDyN5qcn9vqUfyvpJ0nL3mJ0E66BBBPGlNydah5JsiaicD5sY3jkg/132', '1.0', null, null, 'oS5GbwveRr7J4BXnLa6Vgln-HIkA', '3');
INSERT INTO `xjm_user` VALUES ('267', '天天快乐', '', '0', '0.00', '0', '1521421784', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoXoIWlM0DicpiaWibNDsWq2o2OENw93q1PibykNRW2frreLYbQISXicOBAvoSogVhg1pXicnBIrAbGFgYw/132', '1.0', null, null, 'oS5GbwubQc_na9iYdGzp9yokN5Wk', '3');
INSERT INTO `xjm_user` VALUES ('268', '吹泡泡', '', '0', '0.00', '0', '1521515637', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/rhtCPcnib4zKkibQQgw11zqNJicXxTQPVWbeico2vNNdCLnXRyzn9Jfb3nrEfmRSibUdIaXhKoj7hOwr2AjZL3ZNiadA/132', '1.0', null, null, 'oS5Gbwh9w3bGTATY7hjHAjtChx6I', '3');
INSERT INTO `xjm_user` VALUES ('269', '杨毅飞', '', '0', '0.00', '0', '1521539629', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/rHQLqLYNRXuTibCk4iaFe4LWUBoYGwYxzm7uAw9AGicAZltsP6Bsh4icl8Kx2icD0BzKzSAKZMv2gvhFMocW5jwAeag/132', '1.0', null, null, 'oS5Gbwoggq7fjQqT-8gLPtSS2hO4', '3');
INSERT INTO `xjm_user` VALUES ('270', 'Leonard', '', '0', '0.00', '0', '1521715430', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/AC081CBA9A9B8CEBF13F2E1D56364CB1/100', '1.0', null, null, 'AC081CBA9A9B8CEBF13F2E1D56364CB1', '1');
INSERT INTO `xjm_user` VALUES ('271', 'Toby朱勇平-- 一只鞋，调血压！', '', '0', '0.00', '0', '1521915929', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/UOjSnjVELCkxZib7hibfw1IqXPibJFTIRlmSnqt6KVryibWAictG9lZ3SYUa8RPibTPfZe7Obianw6fcHsXQLt9TWlAoA/132', '1.0', null, null, 'oS5GbwoVi_e0AZMvzS6sbXxGNjCg', '3');
INSERT INTO `xjm_user` VALUES ('272', '云峰', '', '0', '0.00', '0', '1521964591', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/VSX4q0ba0GJdmV2ibia4ia9oTLgCSCDrMJXOTkrLzqwOAiaiaXWLvyLUibDC4yc7dficavVdkYOnBcVwZuU4dFUwyU0kQ/132', '1.0', null, null, 'oS5GbwqD6UOEjClbTG5B4Ve3mg8w', '3');
INSERT INTO `xjm_user` VALUES ('273', '', '13651505567', '0', '0.00', '0', '1522042387', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('274', '', '18591237096', '0', '0.00', '0', '1522046664', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('275', '空心~半岛', '', '0', '0.00', '0', '1522057834', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/22E29E745BF01802F777A9B60BDC8258/100', '1.0', null, null, '22E29E745BF01802F777A9B60BDC8258', '1');
INSERT INTO `xjm_user` VALUES ('276', '利群', '', '0', '0.00', '0', '1522226047', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/986CEBF2BEC78EED26531D6C2739009E/100', '1.0', null, null, '986CEBF2BEC78EED26531D6C2739009E', '1');
INSERT INTO `xjm_user` VALUES ('277', '182998', '', '0', '0.00', '0', '1522243509', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/A73C9334FAE4A6086F86AE8D2F4B1531/100', '1.0', null, null, 'A73C9334FAE4A6086F86AE8D2F4B1531', '1');
INSERT INTO `xjm_user` VALUES ('278', '奇貨大叔', '', '0', '0.00', '0', '1522249031', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/U8PzZP0M4FNHIYMibtt2Zgb2Jict4UQibbDH3H1UoTdH6a20JPgUM4k2LUiaH1qiaHb7ckicvM23ejlNeYCOkZDCicAzw/132', '1.0', null, null, 'oS5Gbwq084WGq0qw_moLSAZiW0PM', '3');
INSERT INTO `xjm_user` VALUES ('279', '把生活过成段子', '', '0', '0.00', '0', '1522400700', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJed1DIXghDab6icEl8CrNFZAaQDLQdNahp6W2ONic5fI8MclOKCibP0tAibQqqy2GbeH7MBfbOwaFmWA/132', '1.0', null, null, 'oS5GbwgH68C5oqj22gdRlSSHNBcI', '3');
INSERT INTO `xjm_user` VALUES ('280', '魔', '', '0', '0.00', '0', '1522471714', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/gln7r8WM7005IN3qkNKuWkFGrHLm991cwAG6sl0VrIpibd7vrpgicKrfLprdFK1bbUmALsotGIa5g3EdNUNVzOcQ/132', '1.0', null, null, 'oS5GbwiSSRXENz5mK_QiVoN_sabQ', '3');
INSERT INTO `xjm_user` VALUES ('281', '罗峰', '', '0', '0.00', '0', '1522476166', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/ajNVdqHZLLA8VLqic2keljDKJPX0BHShcWhkrbo8grsibHbGf8VIqIEP0iavS5Admrx43jqf2iaayRIJW45CvhamWA/132', '1.0', null, null, 'oS5GbwmaDvPmTUWtKghQ4Bl3Nn9k', '3');
INSERT INTO `xjm_user` VALUES ('282', '', '17710594736', '0', '0.00', '0', '1522646212', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('283', '', '13588727593', '0', '0.00', '0', '1522684209', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('284', '鳖鳖', '', '0', '0.00', '0', '1522834766', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/TZ5TomFRGwoMIBCHjicV2b4Zsnq5J5CicuCOfoiaEByKsHBtJeYsY2ialANTg2wGKKJDibayB3ehzDo8eZ5FyfPvB4Q/132', '1.0', null, null, 'oS5GbwqlxZAgPkwfl1Dc6DRj_6Xg', '3');
INSERT INTO `xjm_user` VALUES ('285', '', '18514709018', '0', '0.00', '0', '1522858167', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('286', '', '', '0', '0.00', '0', '1522920494', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/QsIragRd1syGICCia4z3Had2QF9n9iaicEBgCVuQcG5h9cynN4aHwZCzptBxr8vmibeKKPPda1ic3lDAlDjcCgMicsHw/132', '1.0', null, null, 'oS5GbwrcKUCFWOswDvX9mskpmcSA', '3');
INSERT INTO `xjm_user` VALUES ('287', 'coderQ', '', '0', '0.00', '0', '1522974940', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/E9ibqckO0wMb8GXAlF3B7kciapsEFmMJnhHEcHpZf7z5dEXMqel5oVwUaUGZU9QFl4FIZlp5E6XYMLDBODiaO0GLA/132', '1.0', null, null, 'oS5GbwuNjzD4mGUqPNlNA6K4HhTI', '3');
INSERT INTO `xjm_user` VALUES ('288', '', '15927247723', '0', '0.00', '0', '1522991482', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('289', '姜山如此多蕉', '', '0', '0.00', '0', '1523016712', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/4F4A1650F7762CBC9E413F3677719059/100', '1.0', null, null, '4F4A1650F7762CBC9E413F3677719059', '1');
INSERT INTO `xjm_user` VALUES ('290', '', '13820367571', '0', '0.00', '0', '1523159222', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('291', '', '15327168878', '0', '0.00', '0', '1523328367', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('292', 'NUMB.', '', '0', '0.00', '0', '1523365210', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/8D0DA4737684619B532ABBDC8E9A7E75/100', '1.0', null, null, '8D0DA4737684619B532ABBDC8E9A7E75', '1');
INSERT INTO `xjm_user` VALUES ('293', '', '', '0', '0.00', '0', '1523456078', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/gUTeerQsqZz5TMbQnPLht8o3JB5a2fsVLhEDjhUrPX57ZVHAaLQPJKfuXQRjZowHia10ebmXcNX4nicFzkiaWjrGg/132', '1.0', null, null, 'oS5Gbwr4BLQOoCt5XHA1MHqljIyw', '3');
INSERT INTO `xjm_user` VALUES ('294', '孩子他嗲', '', '0', '0.00', '0', '1523631200', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/549C33A83F5EE1FA75726140D88CE481/100', '1.0', null, null, '549C33A83F5EE1FA75726140D88CE481', '1');
INSERT INTO `xjm_user` VALUES ('295', '坟场蹦迪VI', '', '0', '0.00', '0', '1523635589', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/1C75E3B2D850311BF5D63F83E96BBB78/100', '1.0', null, null, '1C75E3B2D850311BF5D63F83E96BBB78', '1');
INSERT INTO `xjm_user` VALUES ('296', '', '15800835261', '0', '0.00', '0', '1523741680', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('297', 'A', '', '0', '0.00', '0', '1523804477', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJnS2uxm9Pl8dTvasVz0vMOHsaaV2UzKaibhcGFrTeZ0dD8vSP14kOWNLgoWdxZFffLGTtplVrg30A/132', '1.0', null, null, 'oS5Gbwp9bOHZzT4FUyjMrE2zi2bY', '3');
INSERT INTO `xjm_user` VALUES ('298', '郭加伦', '', '0', '0.00', '0', '1524017416', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqeNVbnP5hbyh2tPVImuwCulePm5MkfWlVMU2lTqRgXliaLo9qzicbX9lRJzfHIEjjibr8uGF1WAYD8Q/132', '1.0', null, null, 'oS5GbwnReRi-Trvx1zRC3ZOzo-Lc', '3');
INSERT INTO `xjm_user` VALUES ('299', '', '15861403400', '0', '0.00', '0', '1524040798', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('300', '微微笑', '', '0', '0.00', '0', '1524131734', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Rccaya3zw32E6hSn3CCg1takcqD0BZKTW9v6stVjhDAVyo7JiaJyNcvgwQ04QdzyqKne1YXsfG7vl7SUZz4ic1EA/132', '1.0', null, null, 'oS5Gbwh6DDbdQpLUkgOn2g8nST8Q', '3');
INSERT INTO `xjm_user` VALUES ('301', 'Venus', '', '0', '0.00', '0', '1524285045', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIsqsl6Vuk5SXqISvcGRzmG91UxFMupLVczm2KJmZma2Rl65g3jMicjsHQdeoiaYViaheDVP120cO74A/132', '1.0', null, null, 'oS5Gbwhy5kT8BPNfidTEi329GPnA', '3');
INSERT INTO `xjm_user` VALUES ('302', '', '13552778312', '0', '0.00', '0', '1524290865', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('303', 'Mr', '', '0', '0.00', '0', '1524294872', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/cJdxuzY2jJbsGlBNxsvFZyEtmeUXjlowGSyrbVThhIYufSd16Haicdicb8r5aV4g2EjmopvJ6CG1t8P1n7YeWuGw/132', '1.0', null, null, 'oS5Gbwon8RAaIGdL-6r8LrKYKTNM', '3');
INSERT INTO `xjm_user` VALUES ('304', '赵华阳', '', '0', '0.00', '0', '1524423172', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTI6KUs7ibPgzrqTaxsXH30XMSweEHcFAnkjf0jP5UVjhb4OiasJ7716AZDxyrUDk9kLWDlpsciaYibeWQ/132', '1.0', null, null, 'oS5Gbwj5SFUgFaC9jUgZAoIbm768', '3');
INSERT INTO `xjm_user` VALUES ('305', '', '15530463210', '0', '0.00', '0', '1524477173', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('306', '', '18801360325', '0', '0.00', '0', '1524568256', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('307', '黑', '', '0', '0.00', '0', '1524605246', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJHZb0e0LobvYblXbQoWOFjyvnaWiciaNES7VpmwicGGPWxlQsWxDBHWdLia4cVlv82icIJwl25ibFC9kRg/132', '1.0', null, null, 'oS5GbwhVahlW_JBXHLqGCE-qV9nc', '3');
INSERT INTO `xjm_user` VALUES ('308', '离歌', '', '0', '0.00', '0', '1524605955', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/BSYm9NRSt3bL6cslKFUribePAAESdKlsibaqibd6SJqlOyIvolxL2vXTW7WsTZ7j3T2Nic857pznEJNkia26icPuRgzw/132', '1.0', null, null, 'oS5Gbwg6AtRatx8zIf0r6olqQQWI', '3');
INSERT INTO `xjm_user` VALUES ('309', '团结樊耀文', '', '0', '0.00', '0', '1525098369', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/OfUB55ahYiafeBPe4EHUZeHRTH8RFeNNa9ic81cJyEV3WyPrKUfXgiaia8EszDS2IVck42Lhu2UenjtaVzp2aq4BNQ/132', '1.0', null, null, 'oS5GbwrVNHn0kRHTL6R4KmTOp2fQ', '3');
INSERT INTO `xjm_user` VALUES ('310', '', '15550309955', '0', '0.00', '0', '1525099494', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('311', '', '18952940971', '0', '0.00', '0', '1525116651', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('312', 'Dark', '', '0', '0.00', '0', '1525158260', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/913D12D3FA28B0EF7DA2F5F8E1E455EB/100', '1.0', null, null, '913D12D3FA28B0EF7DA2F5F8E1E455EB', '1');
INSERT INTO `xjm_user` VALUES ('313', '', '15604991125', '0', '0.00', '0', '1525228813', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('314', '刘大官人', '', '0', '0.00', '0', '1525267219', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/022C136B6F8B70106F92740FC3C17186/100', '1.0', null, null, '022C136B6F8B70106F92740FC3C17186', '1');
INSERT INTO `xjm_user` VALUES ('315', '刘小洁', '', '0', '0.00', '0', '1525340759', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKiciciaytQLFMGo1hpKIGicibuPh5hiaRLaQoicDyXUT4tCQORTKorCXpeh7EhsNzWWcQaZPPz80Swd0OaA/132', '1.0', null, null, 'oS5Gbwjn60AucGiyVyOucfd5RfoI', '3');
INSERT INTO `xjm_user` VALUES ('316', '', '15250960125', '0', '0.00', '0', '1525449923', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('317', '骑蜗牛追宝马', '', '0', '0.00', '0', '1525596977', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eojjEWzRxsDyzlwpxoHrXP4uJrRxUZh2ibJnaIcjfOAKMEQOu3UUf79FNZeG1m6icLIgeZ3owmyqmhw/132', '1.0', null, null, 'oS5GbwopaB523Y5px72mK8vPqkGM', '3');
INSERT INTO `xjm_user` VALUES ('318', '云', '15217615740', '0', '0.00', '0', '1525618231', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('319', '从未停止', '', '0', '0.00', '0', '1525707497', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/bUk7sljiaVV5BlNMib8m1ZVHpibAC9BYSmia2depvy2UZyZf5uExk8HP2Lt63Oan9WcQ8SzlprX41f790G5DZZMlng/132', '1.0', null, null, 'oS5GbwuxKyUvSPEC5mu-b1L45L18', '3');
INSERT INTO `xjm_user` VALUES ('320', 'Marbid.', '', '0', '0.00', '0', '1525762555', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/5C7D7397CA4F7FB16AF99C64EDCA571F/100', '1.0', null, null, '5C7D7397CA4F7FB16AF99C64EDCA571F', '1');
INSERT INTO `xjm_user` VALUES ('321', '', '17633601268', '0', '0.00', '0', '1525881247', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('322', '路过', '', '0', '0.00', '0', '1525947468', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/676A5EF7E302FB958CEC937955808DBF/100', '1.0', null, null, '676A5EF7E302FB958CEC937955808DBF', '1');
INSERT INTO `xjm_user` VALUES ('323', '', '13428860814', '0', '0.00', '0', '1525962535', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('324', '一只傻林轩', '', '0', '0.00', '0', '1525971336', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/B2D9494C3C495D0C0E69958062D1CDBE/100', '1.0', null, null, 'B2D9494C3C495D0C0E69958062D1CDBE', '1');
INSERT INTO `xjm_user` VALUES ('325', '', '18221953470', '0', '0.00', '0', '1526042253', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('326', 'Tjay', '', '0', '0.00', '0', '1526048729', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/5B088697BC8718894C181A0F8C694045/100', '1.0', null, null, '5B088697BC8718894C181A0F8C694045', '1');
INSERT INTO `xjm_user` VALUES ('327', '生活、', '', '0', '0.00', '0', '1526129840', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/56C3B42936050D9541ABE8434939D840/100', '1.0', null, null, '56C3B42936050D9541ABE8434939D840', '1');
INSERT INTO `xjm_user` VALUES ('328', '不垢不净。', '', '0', '0.00', '0', '1526314961', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/9C8D66D05F5A3754A48A1D172387CF95/100', '1.0', null, null, '9C8D66D05F5A3754A48A1D172387CF95', '1');
INSERT INTO `xjm_user` VALUES ('329', '再，', '', '0', '0.00', '0', '1526550005', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/550F44CB517F226E2E117F11D2842821/100', '1.0', null, null, '550F44CB517F226E2E117F11D2842821', '1');
INSERT INTO `xjm_user` VALUES ('330', '', '18058402671', '0', '0.00', '0', '1526596759', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('331', 'zhuang晶', '', '0', '0.00', '0', '1526630317', '0', 'https://thirdqq.qlogo.cn/qqapp/1106555152/E75D125B8A0E268B27F38CB771B054DA/100', '1.0', null, null, 'E75D125B8A0E268B27F38CB771B054DA', '1');
INSERT INTO `xjm_user` VALUES ('332', '寒冰', '', '0', '0.00', '0', '1526827920', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/zY9pLQG5mAjajHeyLMaBxeJFxEyhrFRBEg89qU62dafFH54hrx5X4RxLCibc2nEOUGHpuqE4uZLMw6goOj6kfZg/132', '1.0', null, null, 'oS5GbwhKR4AswtpDLxVERFEhkZio', '3');
INSERT INTO `xjm_user` VALUES ('333', '一味的逞强', '', '0', '0.00', '0', '1526909752', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIMXcaQchwUTmxrLV9KYgh0O8NahBTeiaPmeZVv2qDYw7wSg0Q1l1hTBib2fhuhNHFJ4tL5TrQnOJjQ/132', '1.0', null, null, 'oS5GbwioUqNNA-p0rn5s-YXCr428', '3');
INSERT INTO `xjm_user` VALUES ('334', '', '15925779135', '0', '0.00', '0', '1527052425', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('335', '　　　　　', '', '0', '0.00', '0', '1527091443', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/C7C5CE0E3F3AB261AF321C23DE0EE025/100', '1.0', null, null, 'C7C5CE0E3F3AB261AF321C23DE0EE025', '1');
INSERT INTO `xjm_user` VALUES ('336', '奔放的胡大毛', '', '0', '0.00', '0', '1527224916', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTI9gYX5tYaqicLUYNiagKcZgB6icxaJ5PQWkp7pLicVtzfiaicE0q1H5lyaOhQAcH6HHB4ySOkrDzoiaibheg/132', '1.0', null, null, 'oS5GbwmWaK8GnpQ9uTDyeJA8ZIo0', '3');
INSERT INTO `xjm_user` VALUES ('337', '', '15519921015', '0', '0.00', '0', '1527345943', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('338', '                 ゛qu', '', '0', '0.00', '0', '1527449109', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/15B6644F9FBB1698BECBA6FC2DB606F9/100', '1.0', null, null, '15B6644F9FBB1698BECBA6FC2DB606F9', '1');
INSERT INTO `xjm_user` VALUES ('339', '混世大魔王', '', '0', '0.00', '0', '1527566878', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/39262B4B9A214A52AFB45183F4831642/100', '1.0', null, null, '39262B4B9A214A52AFB45183F4831642', '1');
INSERT INTO `xjm_user` VALUES ('340', '平平淡淡', '', '0', '0.00', '0', '1527566953', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/oucibjq9N5u9rvP6e0TUD4NnmRfwv8viaWWc3icFsUEYiavWa3KkDsjx4ricPsG4NelR2iaAibrxlKNhVdjQePR8qo0cQ/132', '1.0', null, null, 'oS5Gbwo3R_3C6v_A2Mfh9gyv49ts', '3');
INSERT INTO `xjm_user` VALUES ('341', '猛犸象四百多万年', '', '0', '0.00', '0', '1527826753', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/4ia1evCJgyyfoqfGDPPa0sibAko6gutbMicfvbxuByQQ8x8ZoicfYQbtQTZ7L7YC63y7fLhmIgiaib9rhFZS8Qjb3VzA/132', '1.0', null, null, 'oS5GbwqMTKkZurtl-4t1hnYpu8JE', '3');
INSERT INTO `xjm_user` VALUES ('342', '快乐天使', '', '0', '0.00', '0', '1527911120', '0', 'http://pic.xijiaomo.com/20180602114620.', '1.0', null, null, 'oS5GbwklcAo6nAMNdKmuwPtMTCoM', '3');
INSERT INTO `xjm_user` VALUES ('343', '꧁༺爱谁谁༻꧂', '', '0', '0.00', '0', '1527914151', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/5DD17682344DEB0920199758A452D468/100', '1.0', null, null, '5DD17682344DEB0920199758A452D468', '1');
INSERT INTO `xjm_user` VALUES ('344', '浮夸', '', '0', '0.00', '0', '1527923028', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ept3vGJic7KXeUCicVoHnGoicTCYiaYIibhiaXPjJJgfWTxvibFTUUX9OfUqjytrhalSwWRwYcUgkUE7cdPQ/132', '1.0', null, null, 'oS5GbwkfVpeVKvHDTzP6AgVwDri0', '3');
INSERT INTO `xjm_user` VALUES ('345', '混沌大魔王', '', '0', '0.00', '0', '1527948979', '0', 'https://thirdqq.qlogo.cn/qqapp/1106555152/1EC1599B5C4A4B80F7313373C322B451/100', '1.0', null, null, '1EC1599B5C4A4B80F7313373C322B451', '1');
INSERT INTO `xjm_user` VALUES ('346', '', '18627762790', '0', '0.00', '0', '1528051916', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('347', '༄ཉི།ྀ宠物།ིཉྀ༄', '', '0', '0.00', '0', '1528060128', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ep8ozAv9Oj4EyFkUicicQdSWuCdKMqvyUIfUH3iaUNqZWEYd8WpySib2bR2fr6gM3qD3bpP8ic1NKvy8Rg/132', '1.0', null, null, 'oS5GbwnGFJTM2GfV6qpq2iYqlH8Y', '3');
INSERT INTO `xjm_user` VALUES ('348', '斜阳', '', '0', '0.00', '0', '1528387571', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/goibLBWicHBR0B9pibHFYquQJxuhxxl3rSXbGGkrquxdxy1fOf0pY8z0viaUb4fAA2jibInBdqACw8mCJLF13Dyiaf1g/132', '1.0', null, null, 'oS5GbwqWrXUMy6pOO5632N1ssZWs', '3');
INSERT INTO `xjm_user` VALUES ('349', '', '15217228850', '0', '0.00', '0', '1528467506', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('350', '糯米', '', '0', '0.00', '0', '1528536192', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ep13nA84lrQxS5icwtvglMFm178BsAW8lvqjGFx3ria7pSvFsQl2jnq8EITUKNC34opjBreyep7B5rw/132', '1.0', null, null, 'oS5GbwrzkDgarsvsiLuC7VdjT7PY', '3');
INSERT INTO `xjm_user` VALUES ('351', '', '13852252930', '0', '0.00', '0', '1528609467', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('352', '淡', '', '0', '0.00', '0', '1528906810', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/7B57A0DCC49B0B989459DC5EF11E7E00/100', '1.0', null, null, '7B57A0DCC49B0B989459DC5EF11E7E00', '1');
INSERT INTO `xjm_user` VALUES ('353', '', '18790377970', '0', '0.00', '0', '1528988050', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('354', '方成', '', '0', '0.00', '0', '1529085496', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/zxO5ibaITBb2IHKM3yeHGyDgnmeN7YGnOFCDicZS3rxMFranRQRDRL9VAooxCzbXUiaSFlAiaHIiblJRarzHsD9HGEw/132', '1.0', null, null, 'oS5Gbwo2ACwxKd1tcGzWmasfr708', '3');
INSERT INTO `xjm_user` VALUES ('355', '孙晓安', '', '0', '0.00', '0', '1529126532', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKDIQfqKMt3bUp2icE96e9v59U7YSM2ia7agiap21n256ZRQlzhgdYicyPcph97iaia2DfAXdCtBAib1uVgw/132', '1.0', null, null, 'oS5Gbws2lwWWAwGrNO0MZKhHcPBw', '3');
INSERT INTO `xjm_user` VALUES ('356', '', '13723060989', '0', '0.00', '0', '1529213661', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('357', '', '18795912447', '0', '0.00', '0', '1529342316', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('358', '', '18730571391', '0', '0.00', '0', '1529369200', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('359', '', '13720153711', '0', '0.00', '0', '1529478968', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('360', 'Groot', '', '0', '0.00', '0', '1529518676', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/RC0tB5gyQTianC593aGQVruR0BYliapj79yZdSAzbAO6NujBc0VAGGomtAD0SKo9DK0yIlJaE1fIs3JCArcp2IuQ/132', '1.0', null, null, 'oS5GbwglhzcVc6l1XPbF0LwaWjBE', '3');
INSERT INTO `xjm_user` VALUES ('361', '壹米嘀嗒', '', '0', '0.00', '0', '1529519971', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/eDFEb83hu1VJiaicWyFgV9ahBhRzIiaUmFxboCz3ib3vwY9b0tr3gkusMwt9rb6RQOTBkSbcrwZrSuRHZWI87y4aFQ/132', '1.0', null, null, 'oS5GbwoG_egTgkQ7TMzF43khcyGQ', '3');
INSERT INTO `xjm_user` VALUES ('362', '吹一个球', '', '0', '0.00', '0', '1529523315', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLhquTGBjlqJRfnUdk2icaog1cjUVj9AroZOhBOcc2og8823gicJyGBDjQibUVtGaeaJiblGCwkknQ9og/132', '1.0', null, null, 'oS5Gbwsoy7QzakodgFiJTIaMSxJw', '3');
INSERT INTO `xjm_user` VALUES ('363', '', '17601620613', '0', '0.00', '0', '1529654236', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('364', '张紫石', '', '0', '0.00', '0', '1529654313', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLEbH8DibicJTk1h74jhaWgQIG9WOBGcDWzxmzYVWZLN51H8omxZVh9efwib2527C7bBUD8yjCLnLJ9A/132', '1.0', null, null, 'oS5GbwjZRkm8DsXBFHl9CMtqZEZc', '3');
INSERT INTO `xjm_user` VALUES ('365', '꧁萌小子꧂', '', '0', '0.00', '0', '1529663866', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/4DD7E41FB84B8D58808AF617DBA6AA9B/100', '1.0', null, null, '4DD7E41FB84B8D58808AF617DBA6AA9B', '1');
INSERT INTO `xjm_user` VALUES ('366', '꧁꫞꯭A王华18516523579꫞꧂', '', '0', '0.00', '0', '1529695460', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83erlFV1oNQtkaicibnmgPTgwkh1CdJiaKmTeDMicNe2JV9A3En6oEBGia8laAHHeibXbVVjI2AZWnN4uxv3w/132', '1.0', null, null, 'oS5Gbwn0cEPSFX_2syC7nLVUPdFo', '3');
INSERT INTO `xjm_user` VALUES ('367', '', '15927458686', '0', '0.00', '0', '1529742296', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('368', '', '18993020831', '0', '0.00', '0', '1529772906', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('369', '衰一点先生', '', '0', '0.00', '0', '1529783884', '0', 'http://tvax3.sinaimg.cn/crop.0.0.664.664.180/006Avmm9ly8fppha7llzgj30ig0igmxv.jpg', '1.0', null, null, '6036219197', '2');
INSERT INTO `xjm_user` VALUES ('370', '离火', '', '0', '0.00', '0', '1529839833', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/8AA0ABB6E535950FCADA356EC5BC4125/100', '1.0', null, null, '8AA0ABB6E535950FCADA356EC5BC4125', '1');
INSERT INTO `xjm_user` VALUES ('371', 'God damn it', '', '0', '0.00', '0', '1529845365', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIs1k6sTb9EmALG6SUOsmOlNgofts5YP5esTVmslQ9zY18uxtB17h2ic4L0H0bulkDibJeWP4e4kzwQ/132', '1.0', null, null, 'oS5GbwpOPAOmPexdLJYCBauAyYLc', '3');
INSERT INTO `xjm_user` VALUES ('372', '諷♥刺', '', '0', '0.00', '0', '1529857383', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/CCAE0B29CE1B0A1D6B7FDA309F79EDF2/100', '1.0', null, null, 'CCAE0B29CE1B0A1D6B7FDA309F79EDF2', '1');
INSERT INTO `xjm_user` VALUES ('373', '', '15522431396', '0', '0.00', '0', '1529910885', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('374', '', '', '0', '0.00', '0', '1529921088', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/4C91832EE6C9C7EE78D392FF805303C6/100', '1.0', null, null, '4C91832EE6C9C7EE78D392FF805303C6', '1');
INSERT INTO `xjm_user` VALUES ('375', 'Emmi', '', '0', '0.00', '0', '1530089319', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/hvrzzSniay7gk1a8TttV6FG1icBVEoFicrF1L9KnKPWS4C4bE2cNUTwQVeQoJkpCfU8KXfqfHibWellBS7evZH2KNQ/132', '1.0', null, null, 'oS5GbwlQ5Zy8C8aWniaUDdRr76Ps', '3');
INSERT INTO `xjm_user` VALUES ('376', '', '13783535772', '0', '0.00', '0', '1530123798', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('377', '', '13790496403', '0', '0.00', '0', '1530183069', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('378', '灰', '', '0', '0.00', '0', '1530198616', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/74CC8C716A7428442A46279F5FE10745/100', '1.0', null, null, '74CC8C716A7428442A46279F5FE10745', '1');
INSERT INTO `xjm_user` VALUES ('379', '', '', '0', '0.00', '0', '1530376993', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/PiajxSqBRaEIG8DbvNJyicjV3tY7MjEAoJbia2yEoh5DP865Bia4QUbibwCORVdrXae1NGvB4YDvuo1o4zxS1libyJrA/132', '1.0', null, null, 'oS5GbwmxQW6vTlezO3nxnmwD5PYI', '3');
INSERT INTO `xjm_user` VALUES ('380', '', '17691267521', '0', '0.00', '0', '1530510503', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('381', '牟遥', '', '0', '0.00', '0', '1530608593', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqqnrxNKEawLj97ZEhqd28ZwfF3N3KUAZ6ibq1ZUXUCRMjsZcprGnNqg5lum91xp5MYXMMU80d2s2g/132', '1.0', null, null, 'oS5GbwsTfhxT2OWvWHJ-HMiR7Q3k', '3');
INSERT INTO `xjm_user` VALUES ('382', '阿捷', '', '0', '0.00', '0', '1530767117', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqZd2D5vV0GOJndUlqNdDvgTaRKDM5hLDVhN3AeqEqh6oD6cn2wQAYvCrJwialx7ERFgibI1HMk7PYg/132', '1.0', null, null, 'oS5GbwmW8duzy_pcNimAH4A-oNaQ', '3');
INSERT INTO `xjm_user` VALUES ('383', '十川万平', '', '0', '0.00', '0', '1530778718', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/2pjg57ic8bz3H5BjRvQwOicWcibrib9ibOicyHh7150Qc4KXWPCVoFnBrRG7HVConUlDibCu0CDo97Whj6wuktxib8cibhQ/132', '1.0', null, null, 'oS5GbwkwwpWZplsMq_LJe8r-Mxcg', '3');
INSERT INTO `xjm_user` VALUES ('384', 'Mr.Wang', '', '0', '0.00', '0', '1530795254', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJXec2XeXCK7icVibVEJCTtylToKcwpVXGqWiaq8X9eMQcicKG441IqdsByI6qnoNHELPUUricBOaiawgQg/132', '1.0', null, null, 'oS5GbwlCjb56e2CFSIuUnDsrTX3w', '3');
INSERT INTO `xjm_user` VALUES ('385', '陈小龙', '', '0', '0.00', '0', '1530840375', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/hMo3tIL85Q8tVnbUKF1d0KoFUvxsYfVF5zQxPYYj2NhrQA1YUAgsnWtNNFG191XkZQRtAtCnhwUjUP3pq0fKYg/132', '1.0', null, null, 'oS5Gbwou38xvakmQzryotxHTjBQo', '3');
INSERT INTO `xjm_user` VALUES ('386', '如&影随形', '', '0', '0.00', '0', '1530845205', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/c46tzwdXJzp0uWvgMyI3Cib0GN6BNeCMCvQPRIL2Jt7ThngpuZ5o8zMRJByLQF9E5c8BliboibQXdDuLkKpcWicV2A/132', '1.0', null, null, 'oS5GbwiAhnmvt6R_KSiGc4zNWgGM', '3');
INSERT INTO `xjm_user` VALUES ('387', '彼此相爱', '', '0', '0.00', '0', '1531213038', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/7P7LSWPQEISTic7QtYy4fy7wyPveM4sAvJKFWvdXpHpaaXGiac3vXxFY7Z0mukiaoPG1B4UeoCQI0Wp0AL4qfCdVQ/132', '1.0', null, null, 'oS5Gbwpk8NQteJKAWtl82fyMVcAA', '3');
INSERT INTO `xjm_user` VALUES ('388', '。', '', '0', '0.00', '0', '1531313887', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/hPibTl6Duccp7OvGkfqG2fab2mMJ8q43mxMSdT1FmrtrMheJFgZflibMU7mnlMcfQKEne0RJ4LaYuw6QXkPWspRQ/132', '1.0', null, null, 'oS5Gbwgb45AVv-dC1PcjvaQr6MgU', '3');
INSERT INTO `xjm_user` VALUES ('389', '', '17743421151', '0', '0.00', '0', '1531442335', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('390', '人啊', '', '0', '0.00', '0', '1531455970', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/1005FBAEB52FCAA559E6E7A007877A04/100', '1.0', null, null, '1005FBAEB52FCAA559E6E7A007877A04', '1');
INSERT INTO `xjm_user` VALUES ('391', '/', '', '0', '0.00', '0', '1531474885', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/0D3B2CBCBA4E6CECBAEF7C86B3D4F978/100', '1.0', null, null, '0D3B2CBCBA4E6CECBAEF7C86B3D4F978', '1');
INSERT INTO `xjm_user` VALUES ('392', '不一样的感觉', '', '0', '0.00', '0', '1531518577', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/B03BB4318A7C9928A9C686FE57FB8AFE/100', '1.0', null, null, 'B03BB4318A7C9928A9C686FE57FB8AFE', '1');
INSERT INTO `xjm_user` VALUES ('393', '徒步太平洋', '', '0', '0.00', '0', '1531667399', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIia8vbJEhpFllofSzkd6ltCkdJ1cu3OUDdTicTD6OJgBKfAofWsljztIysOhmqf7JeQQXFyD7RYibTg/132', '1.0', null, null, 'oS5Gbwjqz1F3Toq_SP059A4qdQ-Q', '3');
INSERT INTO `xjm_user` VALUES ('394', 'Jimmy Gemini', '', '0', '0.00', '0', '1531670955', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/jh2d7eZp1MG6VFVaMkY452Y6lDvAwQQSUYETtefksHcrBWnL1QQaMAVnr9y84RAibo30U54ziazaxSK4UdWkJAxA/132', '1.0', null, null, 'oS5GbwttaGyibs3G_kv04Z0Yqu6M', '3');
INSERT INTO `xjm_user` VALUES ('395', '', '15618167171', '0', '0.00', '0', '1531712348', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('396', '1501105590', '', '0', '0.00', '0', '1531808104', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/9A000FE14D52EA1C71A887B0947401CC/100', '1.0', null, null, '9A000FE14D52EA1C71A887B0947401CC', '1');
INSERT INTO `xjm_user` VALUES ('397', '', '13111529151', '0', '0.00', '0', '1531817848', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('398', '中楚信技术部技术总监', '', '0', '0.00', '0', '1531966755', '0', 'https://thirdqq.qlogo.cn/qqapp/1106555152/AE34E54E5FA1929BFB852E903044285E/100', '1.0', null, null, 'AE34E54E5FA1929BFB852E903044285E', '1');
INSERT INTO `xjm_user` VALUES ('399', '因为爱所以爱', '', '0', '0.00', '0', '1531998479', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/30820FA127B8976C5C37CFCC9C304694/100', '1.0', null, null, '30820FA127B8976C5C37CFCC9C304694', '1');
INSERT INTO `xjm_user` VALUES ('400', '', '17310329326', '0', '0.00', '0', '1532094470', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('401', 'Couple', '', '0', '0.00', '0', '1532173061', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/EBN2bwFib7ibWqxOI7GINR0VGxnXsbqTJOzjkgvhic5Qp4SoAPGps5Z5cdFhzhEHHKrPv0CWzhnZhNYhMWzrLWTug/132', '1.0', null, null, 'oS5Gbwjg_cjvZCxXQiFCemwyDvUU', '3');
INSERT INTO `xjm_user` VALUES ('402', '笑看人生（高林）', '', '0', '0.00', '0', '1532194333', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/8hDgEI9ibbXgGV4T33hrSZnQbA27M9ZbG2k72s1ZVHVo8L5PO26icRc0Fd8yMW2CFxPZlMvMzO4hV6dYXOQQZC2Q/132', '1.0', null, null, 'oS5Gbwig8srF4_riyR6KvTtPSxxM', '3');
INSERT INTO `xjm_user` VALUES ('403', '', '16675333383', '0', '0.00', '0', '1532267345', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('404', '', '18670708115', '0', '0.00', '0', '1532280661', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('405', 'A+秋哥', '', '0', '0.00', '0', '1532302368', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/ys3BWHqzL5QzO0o2NHD8KKbbiaNBtibESBxfGygz0O25fttvNkP3SOEQmqJsZ11URWHADPU7PrialtAoZTnyNbf2Q/132', '1.0', null, null, 'oS5GbwuS_eD3ZI5NpgbjCFujaWog', '3');
INSERT INTO `xjm_user` VALUES ('406', '卅卅西', '', '0', '0.00', '0', '1532326198', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/PiajxSqBRaEKgMjVUbWEk2gpF9Iicmx9zR3VB6yZY8z2xQ6ZoCFWmpAhDDoqico0ajJ5QRAZOpeHFZLxhMnzPXhVA/132', '1.0', null, null, 'oS5Gbwvn8ije93R3QLft2kohI15I', '3');
INSERT INTO `xjm_user` VALUES ('407', '', '18038125858', '0', '0.00', '0', '1532425781', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('408', '', '15220158589', '0', '0.00', '0', '1532500100', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('409', 'SUI (18632116757)', '', '0', '0.00', '0', '1532513538', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKvwiaarYdsLn04e7JSOxAAH9fBS0xjBu055UyicY82WBsHYNVTooBq0EZnBI3J9xy1Bz3ibLhfyWV4w/132', '1.0', null, null, 'oS5GbwvnttUf6T97sMduUZhy7ddE', '3');
INSERT INTO `xjm_user` VALUES ('410', '奋斗', '', '0', '0.00', '0', '1532530818', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/YjGkwIFGWPJX2zuFXM0hCnsMWDWMiaTla63A9rRdzaecTH3ibL6Y9kbD6slovlfZ8luVeDH5yDBuXQWx77UklE0Q/132', '1.0', null, null, 'oS5Gbws-WgY2Eyn8xzLHMwEUl64E', '3');
INSERT INTO `xjm_user` VALUES ('411', '昔忆i', '', '0', '0.00', '0', '1532671262', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/VAJkqibsibjyOW4Q2F5P6d5lq5y5tSDiaTBbFMLI5DPgibt6vCibGmaX9rYtYWtsngmgEZsIAxbUdVl2iccYUPYWCiaaQ/132', '1.0', null, null, 'oS5GbwhbRAQDD9_ruLiCqP6OB7o8', '3');
INSERT INTO `xjm_user` VALUES ('412', '', '13315233085', '0', '0.00', '0', '1532679204', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('413', 'Lori Erica', '', '0', '0.00', '0', '1532683275', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/9D61AE616449E6E39E714D64AE79CE92/100', '1.0', null, null, '9D61AE616449E6E39E714D64AE79CE92', '1');
INSERT INTO `xjm_user` VALUES ('414', '', '17612011000', '0', '0.00', '0', '1532803625', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('415', '', '15001019867', '0', '0.00', '0', '1532864263', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('416', 'LSW', '', '0', '0.00', '0', '1532923108', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/vvkRciasibpjfia8svtLY9hp4WwGfsCFAY6EU7SZiamKtUkNzHWqgtIBFMWibAInSq0SfwS6hcYHlItyVXAt5YNsfSA/132', '1.0', null, null, 'oS5GbwvqYK9CC2Za2xl7_9yUPn5k', '3');
INSERT INTO `xjm_user` VALUES ('417', '', '18817831889', '0', '0.00', '0', '1533034249', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('418', '马小亮', '', '0', '0.00', '0', '1533087593', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eod2ZNIY8WNDQny63YUbNSYd5yHjR2gWoLMg1PqU5iaKuOh1OwYISib9G4B10uadow8f0icyiaDtbt2Tg/132', '1.0', null, null, 'oS5GbwgNlRyY8dzFSTwiVxLOst0k', '3');
INSERT INTO `xjm_user` VALUES ('419', '一人一半才是伴', '', '0', '0.00', '0', '1533112970', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/BZUOGUeuLSAFic4rRUOYByaMpsWKibcIPPtptoPSdfNsj3xiaRZBzJ7BCSJdx4XOVf7Dq4TB148vKjibNlrImUEs2Q/132', '1.0', null, null, 'oS5GbwgyXwOlhniUr5Ndt5KbfBGg', '3');
INSERT INTO `xjm_user` VALUES ('420', '诚心为本', '', '0', '0.00', '0', '1533200139', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/EuTSzOwYB79Rry641hD0sUic6vUpu0xC0h7EW1cplE39aJ3J06ibJyI43v1cKEdbvLKRWonR42odiaRdxAFslMD2w/132', '1.0', null, null, 'oS5GbwmEu62mAje-A6gzGyd8_Tyg', '3');
INSERT INTO `xjm_user` VALUES ('421', '逸夫', '', '0', '0.00', '0', '1533280349', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqoibibZCpRw5En2Itlw6W1NDNH2buh1OH5KeXt05fShQKnxCicgOvjBGWcTJuicJFUGblYH85ichbFV7A/132', '1.0', null, null, 'oS5Gbwv1uC1e5YNL5s2WOixOzsPI', '3');
INSERT INTO `xjm_user` VALUES ('422', '自由', '', '0', '0.00', '0', '1533302336', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoJg5G3a6TStLWme9xT1U7RlUbmXAnt2dEf0myZricLKLwrAWWv8icyWKiccEToOicES8dLuOrqnqVXpA/132', '1.0', null, null, 'oS5GbwoxbvBkKaJRTe94H0JgHn30', '3');
INSERT INTO `xjm_user` VALUES ('423', '', '17671605956', '0', '0.00', '0', '1533336916', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('424', '', '15811290671', '0', '0.00', '0', '1533342622', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('425', '张华', '', '0', '0.00', '0', '1533350913', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLfDEjRz7yoo3ibX2lVFqg4x6JicNbiaEBUupV3pibqha1ofHLQkQR4ZInxaAvXWTD4b2MHnjRBEVO3UA/132', '1.0', null, null, 'oS5GbwtiuETQFZ9nNPLbGV3ZTDLk', '3');
INSERT INTO `xjm_user` VALUES ('426', '宁静致远', '', '0', '0.00', '0', '1533366356', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/gXkdul55hQJ5ahqgJkpnESf4PJ1ftryAcKJEzmy08O11nzqicPRveWv5CNOZtYjtOJwdo4JjbzQ03Scj9diajFsA/132', '1.0', null, null, 'oS5GbwkzsbjzjAZYtVd9KkEtKH2A', '3');
INSERT INTO `xjm_user` VALUES ('427', '', '13641111112', '0', '0.00', '0', '1533448912', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('428', '', '13821955071', '0', '0.00', '0', '1533459521', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('429', '', '15040916373', '0', '0.00', '0', '1533666866', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('430', 'A  无人', '', '0', '0.00', '0', '1533707933', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/doMXR1kVhPETy6pu21ID2mn2tuoxjicGlibvobuLZG8ibu9qzeOyCRXuqHTRDXdNzKv1FUFSPRg1nGiaA8TJxjnQuw/132', '1.0', null, null, 'oS5Gbwj8hvJYh3xIr1BnU7tSeI2s', '3');
INSERT INTO `xjm_user` VALUES ('431', '', '13901359036', '0', '0.00', '0', '1533715682', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('432', '', '15950553468', '0', '0.00', '0', '1533744104', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('433', '现实.', '', '0', '0.00', '0', '1533752611', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLsdGPnevLGGpaaoItQvibicPS4w88CvR5ug1M8L1Gr1h5WicTYoWocURZ4LzoF49tUmBBXDDONMKZdQ/132', '1.0', null, null, 'oS5GbwkVVFkIpWiKVLhkFe8-f4aM', '3');
INSERT INTO `xjm_user` VALUES ('434', '尘土', '', '0', '0.00', '0', '1533893396', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLibfIV7n6SDhCh4oMkib0FwQzUgBbJZQ8r5zePuQo0fnMeOR8vuqjgic880hLDIHyHheH79RAt0bHaQ/132', '1.0', null, null, 'oS5GbwtgvU-lK7HCw1u5JUhbnDks', '3');
INSERT INTO `xjm_user` VALUES ('435', '', '15816879037', '0', '0.00', '0', '1533901768', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('436', '当爱以成习惯', '', '0', '0.00', '0', '1533906414', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/RZFa6AWp3NkcuwichaTQo72rZU3WZAGiczibTmXKlAqpljGo4yFJCcNppLibXuLT4iaUP0PNdwHW9CRiaP0SewicUUcMA/132', '1.0', null, null, 'oS5GbwpzH0Ur2ZrBcmI187HTUThk', '3');
INSERT INTO `xjm_user` VALUES ('437', 'he', '', '0', '0.00', '0', '1533966940', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83epxkKrSTaLtcxias1icmeX1LsRMFJAJ6HG73B2smbj948qAvwrpLcUY1Mr2ciaYCJ01PmMEWwlePoITw/132', '1.0', null, null, 'oS5Gbwggc0Qfn2cWh-402fUOLAzo', '3');
INSERT INTO `xjm_user` VALUES ('438', '◆゛╰卡布奇诺', '', '0', '0.00', '0', '1533967462', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/CE45D0971232B03E10B8D81ADD006067/100', '1.0', null, null, 'CE45D0971232B03E10B8D81ADD006067', '1');
INSERT INTO `xjm_user` VALUES ('439', '', '13338110086', '0', '0.00', '0', '1533985833', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('440', '', '13128707725', '0', '0.00', '0', '1534053717', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('441', '', '17803812105', '0', '0.00', '0', '1534067514', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('442', '', '13304019099', '0', '0.00', '0', '1534069840', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('443', '*-我是一只', '', '0', '0.00', '0', '1534150000', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKyYBY0WibhufkFf8AgevxdpI6HDvesD8FpJ8nHDfibIicXqGRRCu1wsCdTJwX93DPmBh8icGWxY56wcg/132', '1.0', null, null, 'oS5GbwvBM8QHMWf9cBQfgu9veMms', '3');
INSERT INTO `xjm_user` VALUES ('444', '余享', '', '0', '0.00', '0', '1534150251', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/5w78iaicLZBCKiaHibePKIGHor4utHJUxAt0gGevELkYYrpHK40jv45RWON12hc1UfqpzIryXsfO0aKn4TCnlmibHZQ/132', '1.0', null, null, 'oS5GbwjpTL4Ib1NB6PFRrYqouuKw', '3');
INSERT INTO `xjm_user` VALUES ('445', '', '18271515371', '0', '0.00', '0', '1534166935', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('446', '洋', '', '0', '0.00', '0', '1534177946', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eo4c1cGaWUiaXSsrFyyuGs1lBPGQicuy2FPPQDavIicE0ia3iaia9GVNv4LsoicvriaXPaIickpUu30mZHvv8g/132', '1.0', null, null, 'oS5Gbwn-_Fi9qlnmPhwa6ex1LQQo', '3');
INSERT INTO `xjm_user` VALUES ('447', '', '13277279678', '0', '0.00', '0', '1534219614', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('448', '崔立强', '', '0', '0.00', '0', '1534240044', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/LZSuvCaGQSdCEO9ictRiaIWuwhsqBdfM2sHt4VqOED3knMJb6XXE7JegfNibunNocH15giaZSb8CngyXiaKibNIkuHwQ/132', '1.0', null, null, 'oS5GbwtwqeUlD7CWcg7d_y1Zqw_0', '3');
INSERT INTO `xjm_user` VALUES ('449', '', '15119008725', '0', '0.00', '0', '1534245525', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('450', '', '15876370197', '0', '0.00', '0', '1534252436', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('451', '靳鑫', '', '0', '0.00', '0', '1534257431', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLcqIgVCW6kktQK4uibzNPU1HOvxbSYylvgiaJSMI5GZKleQpqlU6lficyIicvDoutg4oy9Ud6TAY6MIQ/132', '1.0', null, null, 'oS5Gbwik6OajDulqmyiqCnNL9Ek4', '3');
INSERT INTO `xjm_user` VALUES ('452', '', '13612805535', '0', '0.00', '0', '1534259249', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('453', '珍惜', '', '0', '0.00', '0', '1534306683', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/JYjlHG9g7VbalhHyY0HwICicTjuZmRTnuMdQTwMSZjCdbX1yw4ibrwXfnTRbBYImYUicib3DMxyt4TGKx2DMiacu78Q/132', '1.0', null, null, 'oS5GbwhYiiwNACzvlQ9tDTpSaS0k', '3');
INSERT INTO `xjm_user` VALUES ('454', '', '13844549356', '0', '0.00', '0', '1534318856', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('455', '无敌大鹏哥', '', '0', '0.00', '0', '1534386670', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83epGN1hxk3LdcicnbXJQ9ZFiapicFLtzRVtsfSXOdPwR5z7Tgp5L1fmSXlQ3amW7IRQrqn5ybUL3pOIyA/132', '1.0', null, null, 'oS5GbwgSnzwT7EvBNSXBoL9QB93E', '3');
INSERT INTO `xjm_user` VALUES ('456', '无味.', '', '0', '0.00', '0', '1534390828', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/E4E91A5843571C88A5117A7599E7FE94/100', '1.0', null, null, 'E4E91A5843571C88A5117A7599E7FE94', '1');
INSERT INTO `xjm_user` VALUES ('457', '', '13767783865', '0', '0.00', '0', '1534431381', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('458', '八级大狂风', '', '0', '0.00', '0', '1534586067', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLmZ2EGbwYJFpYGTtXAXGepDqqcHN8fP25l8Bx8QDNeAkysQYjtD4YicN2CPiaHXCGqk6losTLPOYRQ/132', '1.0', null, null, 'oS5GbwnGIjDNDUy989pM7PFE5cfo', '3');
INSERT INTO `xjm_user` VALUES ('459', 'i   ove', '', '0', '0.00', '0', '1534741417', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/4D16B23A22FC5504A6FF6144179F6AC7/100', '1.0', null, null, '4D16B23A22FC5504A6FF6144179F6AC7', '1');
INSERT INTO `xjm_user` VALUES ('460', '篮不住海鹏', '', '0', '0.00', '0', '1534746669', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto46020180820143524', '1.0', null, null, 'oS5Gbwid71Lt_VTS7-fX_CjIkq4E', '3');
INSERT INTO `xjm_user` VALUES ('461', '1.1.1', '', '0', '0.00', '0', '1534753018', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/xPIJbTjCJZias4I7K1rhMSMEJcZciaibxIEzFboeL4JJE5sojQhicXGhYH7aDNZ0jvMYA98Yibl3AeKTdGg788goficg/132', '1.0', null, null, 'oS5GbwkmwVyX_siMBZNzmzZcTsyI', '3');
INSERT INTO `xjm_user` VALUES ('462', '', '18201159304', '0', '0.00', '0', '1534832508', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('463', '只有在梦里来去', '', '0', '0.00', '0', '1534834904', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/StggD5LvINk6G78r4K6a219zVVlB0AriaicpWOZDhC2DCmgHZTlngFq8FX1pWu7KPymH37D1qQbscpDSdn9c9BoA/132', '1.0', null, null, 'oS5Gbwtpd7GJO1ygxXFgr43QahYM', '3');
INSERT INTO `xjm_user` VALUES ('464', '京京', '', '0', '0.00', '0', '1534867388', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/PiajxSqBRaELiazvT2gxM1U85ob2BKfucreBuWGozcj79uutacehJNAUWDQVPavfdPiclzibiaqR9mYGKjppoL9IRrw/132', '1.0', null, null, 'oS5GbwjselnY8xeyFmUfcH-cd8gk', '3');
INSERT INTO `xjm_user` VALUES ('465', 'GNEFIYUIL', '', '0', '0.00', '0', '1534901874', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Eia7qcViauS0JK8bDMJpgMaKxIs94Yq2a7c5CicQicP9TNZ1IOKXJ4FMnWg00sFsxP51vAaicImibHDDwaDwvp9ZXOiaA/132', '1.0', null, null, 'oS5Gbwq5Trw_DAk7KTYP0NXlrkCI', '3');
INSERT INTO `xjm_user` VALUES ('466', '信 ღ 念', '', '0', '0.00', '0', '1535002562', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJP05RJ5icJkUqqzzicicZ87CTiaic9cHt9Piap4ltQgt8cXNf0gE4nNtJIClz76HVo0djdib4veibbY99VIg/132', '1.0', null, null, 'oS5GbwonwWfV2LZpr_45JkKESE3I', '3');
INSERT INTO `xjm_user` VALUES ('467', '沉沦', '', '0', '0.00', '0', '1535027813', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ereTVMicvymbQPNuCLicvPHunicWhSTibbaIl9sw3ACZKj5rk2NpGQkuJiay0mWlzfAcgknyx2pSSibeIicg/132', '1.0', null, null, 'oS5GbwvtAEegdSi6n-3S1CK3Xgdc', '3');
INSERT INTO `xjm_user` VALUES ('468', '', '18602740508', '0', '0.00', '0', '1535031686', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('469', '　　　　　　　　', '', '0', '0.00', '0', '1535039254', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJQ0ePZBw7PoroOicWvElk1pl2GkdDrq6ePa7BoeLVG5yup94yj7LMPxx3ek2zbJX7D19VwdPbZvlw/132', '1.0', null, null, 'oS5GbwhOdw7Y0a5ctyPvYKbp8WDg', '3');
INSERT INTO `xjm_user` VALUES ('470', '', '13817442961', '0', '0.00', '0', '1535367338', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('471', '', '15201329720', '0', '0.00', '0', '1535481704', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('472', '', '', '0', '0.00', '0', '1535546600', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/XzzZibyRqbT1jwfSF5GKCaZmVuCzPHo1Df4QCHbxBXV4PUcauaKcQ5iaIWK38g0fiabhzrUUDuCdbAZh5wU1jpXwA/132', '1.0', null, null, 'oS5Gbwnx0UzyNfPD5qdnt0Crcg-0', '3');
INSERT INTO `xjm_user` VALUES ('473', '、等待', '', '0', '0.00', '0', '1535698358', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJgJd3Lhv25gXsKIxB16MFjEMOZJ1XZZJOvemZSyYbrO6icTYKOAicJMX6rYkCW6MkG7y9Az0dkgoFw/132', '1.0', null, null, 'oS5GbwpRNlSLbpfCwO6ciM8EDP3Y', '3');
INSERT INTO `xjm_user` VALUES ('474', '小角色', '', '0', '0.00', '0', '1535729308', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/0519A9DC62D2146A9ACCC9F3260119F0/100', '1.0', null, null, '0519A9DC62D2146A9ACCC9F3260119F0', '1');
INSERT INTO `xjm_user` VALUES ('475', '斯文败类', '', '0', '0.00', '0', '1535775821', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/E37BDE51EEE6F9525E062E3A6748C094/100', '1.0', null, null, 'E37BDE51EEE6F9525E062E3A6748C094', '1');
INSERT INTO `xjm_user` VALUES ('476', '', '15875580960', '0', '0.00', '0', '1535870393', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('477', 'jared', '', '0', '0.00', '0', '1535870600', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/015AD0D4B79A3BCA8DEE804E630EDCC0/100', '1.0', null, null, '015AD0D4B79A3BCA8DEE804E630EDCC0', '1');
INSERT INTO `xjm_user` VALUES ('478', '', '18594040513', '0', '0.00', '0', '1535902534', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('479', '水是醒来的冰', '', '0', '0.00', '0', '1535959203', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/B0316E04D8E1A3350213A7F990B87150/100', '1.0', null, null, 'B0316E04D8E1A3350213A7F990B87150', '1');
INSERT INTO `xjm_user` VALUES ('480', 'Non  single', '', '0', '0.00', '0', '1535998960', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIaMa2GzPnhAdC9sp6rmJMG5OkTqc7OJzLextJiad2v1qwxgiaqpygFM004jHB2vryfQInWnm8MiaCcA/132', '1.0', null, null, 'oS5GbwphAcyp1DvMDbcXcs1BnhY4', '3');
INSERT INTO `xjm_user` VALUES ('481', '神秘人', '', '0', '0.00', '0', '1536091256', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/EB34B10E287814BACC609E33A8E459B1/100', '1.0', null, null, 'EB34B10E287814BACC609E33A8E459B1', '1');
INSERT INTO `xjm_user` VALUES ('482', '', '15509851000', '0', '0.00', '0', '1536276503', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('483', ' 艾灸石健康 陈莉', '', '0', '0.00', '0', '1536309599', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Rm6Sx3BQT3EzCgic0VBM7XueasCNSxDa4ue7oQQcoNo7rwX0ibgzicpVMFQaxEcC9TLicQ1licy2C9CvfCGbjYpLesQ/132', '1.0', null, null, 'oS5Gbwl0Zgm8WNindbwfjfMnCYF8', '3');
INSERT INTO `xjm_user` VALUES ('484', '小李修脚18380900101', '', '0', '0.00', '0', '1536315590', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/pNibrLWicOKddNDggvQNr6VC8mL89LaSwhuYukADKdDJKGJDaviaBEN1DnWa2XkSlHR6LxmH9D8YhbWMaW7rQjP3w/132', '1.0', null, null, 'oS5GbwjkiBi-c7XHz3FeBAya4aUY', '3');
INSERT INTO `xjm_user` VALUES ('485', '！！！', '', '0', '0.00', '0', '1536319503', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/64D23D7A7FDBF71DFD6D5D2109574CFA/100', '1.0', null, null, '64D23D7A7FDBF71DFD6D5D2109574CFA', '1');
INSERT INTO `xjm_user` VALUES ('486', '帝释天', '', '0', '0.00', '0', '1536356925', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83erQCQ9aDTRVUuMuQQnQLLdjic7EmMjoicp83VLFT24r5h0bkc2qtQQc2FJT3jxywbl03zcYenv6AT4w/132', '1.0', null, null, 'oS5Gbwv2D1zHRS0yDl3hsqwFG36w', '3');
INSERT INTO `xjm_user` VALUES ('487', '', '13870512989', '0', '0.00', '0', '1536417449', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('488', '', '13876795672', '0', '0.00', '0', '1536421159', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('489', '- 李梓元', '', '0', '0.00', '0', '1536514629', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/ZibKPOxCkOxLmzVqr3Q5Ily3pKG1QiauyfwhDQS0yPMHd8QZYSiaPI85jUWIf2Ltmia7Ys6aUjRTMaH0KUlXns5cSg/132', '1.0', null, null, 'oS5GbwhqmHHZYC1SReLE8TL-1vMs', '3');
INSERT INTO `xjm_user` VALUES ('490', '', '15715692352', '0', '0.00', '0', '1536564083', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('491', '李良秋', '', '0', '0.00', '0', '1536589594', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/V2gibbkDXKJvyU9sXwQEWg6r4MJeqFCgf3IwqV9XK2qn2tia2HlthRYkmcfCQYe63wOUlX1SGEkvic38nzq8djO3A/132', '1.0', null, null, 'oS5Gbwo9YghMG7_sehd3KeV__mok', '3');
INSERT INTO `xjm_user` VALUES ('492', '', '13187066767', '0', '0.00', '0', '1536671825', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('493', 'A汤强', '', '0', '0.00', '0', '1536676554', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoz3NRrVsErk9p79ZmC85YA0jExpHNTkPRwpravEGkYWjy4e9hv5Cmpy8Xy1mMHZRDtHl7tJqF7bg/132', '1.0', null, null, 'oS5GbwjCXHONIoybZfkGWF8EyYqI', '3');
INSERT INTO `xjm_user` VALUES ('494', '＠{}$￥#~＆£', '', '0', '0.00', '0', '1536678344', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/68B6AF72B646C0DAC6B04B160980F6F6/100', '1.0', null, null, '68B6AF72B646C0DAC6B04B160980F6F6', '1');
INSERT INTO `xjm_user` VALUES ('495', '旧手机换不锈钢盆', '', '0', '0.00', '0', '1536740261', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/RxOAunXNYSZQtrkSW3oOwbNxSkET1fcibtgXcYvNQOC4x37ibAhxqMnaKvmvWxobt9g8eJeF3wiaK0UGNzww5uYCg/132', '1.0', null, null, 'oS5Gbwu_eT3rwenxx_NWl1zZJKK8', '3');
INSERT INTO `xjm_user` VALUES ('496', '赵杰', '', '0', '0.00', '0', '1536744938', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ercRS8f82YddKnJkICDSoxzMfNq6QQWunrCJBQLLzvNMfZ8PY4PxEWzm3dAmqdWYWUQHZianvNzdzQ/132', '1.0', null, null, 'oS5GbwvfFozr7SZjqXFDhxtYHzgo', '3');
INSERT INTO `xjm_user` VALUES ('497', '', '18860197743', '0', '0.00', '0', '1536754028', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('498', 'poorguy', '', '0', '0.00', '0', '1536816369', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/061A43E34CF1ECEB9D74819F11B84218/100', '1.0', null, null, '061A43E34CF1ECEB9D74819F11B84218', '1');
INSERT INTO `xjm_user` VALUES ('499', '', '15014807191', '0', '0.00', '0', '1536900570', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('500', '杨小涛的日子', '', '0', '0.00', '0', '1536932531', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/ic9AwSicXJNSN7k7XZVDTBEC06AkVkk7qP2tJFsQDdeFwkmwmOIRoZn3tALicAVfQZ1aYrCrW7TPk6zXBO34NxGPQ/132', '1.0', null, null, 'oS5GbwkwTn9vwA48CQ2aOaK14IIA', '3');
INSERT INTO `xjm_user` VALUES ('501', '', '', '0', '0.00', '0', '1536995248', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/y94WhlJ5T46uQLsUZUoic5wmw0DcISG93g56HBPo16EgwMHJ9jGibNgLPD4X7wcbnBqLFm89nIeicax58hcoZj27w/132', '1.0', null, null, 'oS5Gbwo1N-PJT3o_2tG0wu5-SHYg', '3');
INSERT INTO `xjm_user` VALUES ('502', 'Werner', '', '0', '0.00', '0', '1537018895', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJgeCOicFPtbIWgpDriaZh5nFwEZicDntUicYibLfjjvdqMAg9AvvXYtj7Lkdxb1RicibeS0CJFgPFvyya7Q/132', '1.0', null, null, 'oS5GbwlsU0vLtgngQqNx2NIv1G10', '3');
INSERT INTO `xjm_user` VALUES ('503', '衍恒', '', '0', '0.00', '0', '1537066469', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83epvU9qL2YicMRwb4VuHUgu7wlEpD0icbY2ianNJ1unc1sshASFFxPbiaFDnuB6XBDB95GB7mMWxbu64Kw/132', '1.0', null, null, 'oS5GbwrS_PPPFydPOPmYEM43Fr2A', '3');
INSERT INTO `xjm_user` VALUES ('504', '', '18988827021', '0', '0.00', '0', '1537071274', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('505', '石', '', '0', '0.00', '0', '1537072759', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/hNexjiaMbAIk9YUnFHF5AiazAfmtibWIJ1iafRpIWiaJgUJ2EiaicRluViaZhKhSp6Gee7TSkVuJrOJYwTRsIwlLmx8xBA/132', '1.0', null, null, 'oS5GbwqwZvf2cb19x1HED4g3PUOY', '3');
INSERT INTO `xjm_user` VALUES ('506', '抹茶味的叶子', '', '0', '0.00', '0', '1537113680', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/C3E6AG66CgicpHiaBxerWF4xsZhtC1Vr5MgypMcI1LXUtPEHnYylhuLwtcL2VGf3ibQFVwBHve6pDicD9EdOAJvia7g/132', '1.0', null, null, 'oS5GbwvyXPi64oyb5rhcaR6SGJbs', '3');
INSERT INTO `xjm_user` VALUES ('507', 'ZH', '', '0', '0.00', '0', '1537152158', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83erxJYxzce3mZnbrnNkWDDUfdJA3pQ5xzvb2zQp999d0T0md7CWlkGIVVXtc2Y0QEvkPBKqiaWiabwjw/132', '1.0', null, null, 'oS5GbwqQuGv1Hcushk6266NWhnU0', '3');
INSERT INTO `xjm_user` VALUES ('508', '', '13608902377', '0', '0.00', '0', '1537170216', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('509', '', '13713828074', '0', '0.00', '0', '1537195042', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('510', '', '18505203816', '0', '0.00', '0', '1537255277', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('511', '', '15880051221', '0', '0.00', '0', '1537255780', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('512', '健儿奋起步伐', '', '0', '0.00', '0', '1537255787', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/PiajxSqBRaEIuf80VokDtCE1b2hY7fPRnXD3uvoJqadpmgmg0OQQyx30kQ06DPRDWUD45ia01wNHp2qh8hS8iaLwA/132', '1.0', null, null, 'oS5Gbwh4mkbxD1pCyA0X-AQkCGgw', '3');
INSERT INTO `xjm_user` VALUES ('513', '侯某某', '17645099126', '0', '0.00', '0', '1537256404', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('514', 'Deng', '', '0', '0.00', '0', '1537276858', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto51420180918212116', '1.0', null, null, 'oS5Gbwjip6rzFP_k8RlbtxY8n5i4', '3');
INSERT INTO `xjm_user` VALUES ('515', '', '18578678190', '0', '0.00', '0', '1537342039', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('516', '安嘉', '', '0', '0.00', '0', '1537372416', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/8E59FA8E747B2257DDCB69E8EB0CD900/100', '1.0', null, null, '8E59FA8E747B2257DDCB69E8EB0CD900', '1');
INSERT INTO `xjm_user` VALUES ('517', 'Sunny', '', '0', '0.00', '0', '1537403568', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJ2dEsaUjXZxuptMOo4rX5QjvKWSqzcb8bPcapnfgun5758NcCiaIaIJCDXjwxMSmMiaco6H4vnoiafw/132', '1.0', null, null, 'oS5GbwkU6gyYDp1hG1BXu9-wbwPg', '3');
INSERT INTO `xjm_user` VALUES ('518', '', '13760346878', '0', '0.00', '0', '1537427892', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('519', '陈华18907115812', '', '0', '0.00', '0', '1537525102', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/bL7S6qKRia8Jzp4ldOW3NPUXSNh0RNodW1cH2gtPFAkxNR3AVUxUrDcRA4YOwzCc6iaaviavRJ6BrhxUp0LvsQnrw/132', '1.0', null, null, 'oS5Gbwr_YaJZGJdJ0x-kVfyF2hTE', '3');
INSERT INTO `xjm_user` VALUES ('520', '回忆美好的一天', '', '0', '0.00', '0', '1537568804', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eooYgbhbvBkG6MAiaySyN115XXkEuOdGT8aJfxy9A3uy12nyLhwCficBsGpEepO54RJAld1K5Z7BVjg/132', '1.0', null, null, 'oS5GbwpNNd0IZMDx_Pgys1KH6xaQ', '3');
INSERT INTO `xjm_user` VALUES ('521', '❤️', '', '0', '0.00', '0', '1537684995', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/DB4A6F8F8C46C1711E8DE38533A5718F/100', '1.0', null, null, 'DB4A6F8F8C46C1711E8DE38533A5718F', '1');
INSERT INTO `xjm_user` VALUES ('522', '', '18101715198', '0', '0.00', '0', '1537712854', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('523', 'flight2678', '', '0', '0.00', '0', '1537781495', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83erI8YAibZj0hibC5LF62SWRstZnek3G7ic7ul3lTSdkN7rAYYuOMNbMJtiaLEnu2ZJqCBctXqeXTH9lpA/132', '1.0', null, null, 'oS5Gbwg4IeQlxYvH49uhucmUQDSY', '3');
INSERT INTO `xjm_user` VALUES ('524', '', '13543328468', '0', '0.00', '0', '1537785605', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('525', '', '13924628345', '0', '0.00', '0', '1537798800', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('526', '爱上，你', '', '0', '0.00', '0', '1537807207', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/423902CA9E63A0F7FB03F34295CDE3DA/100', '1.0', null, null, '423902CA9E63A0F7FB03F34295CDE3DA', '1');
INSERT INTO `xjm_user` VALUES ('527', '', '13585885769', '0', '0.00', '0', '1537829667', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('528', '龙游天下', '', '0', '0.00', '0', '1537844677', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Nr8o80t7TVzHibicZHyNsDxpCG5C6qTHwWImkASSaX4mquGdMr1jdMIPeHzubiaPTJGKf67RWNaz47ZRoe3oN009w/132', '1.0', null, null, 'oS5GbwrA6Dt3sShZ441MCH33IYPU', '3');
INSERT INTO `xjm_user` VALUES ('529', 'Jimmer', '', '0', '0.00', '0', '1537844708', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/94FF65878426B9B12CC0B0EB7C7C53FB/100', '1.0', null, null, '94FF65878426B9B12CC0B0EB7C7C53FB', '1');
INSERT INTO `xjm_user` VALUES ('530', '农民', '', '0', '0.00', '0', '1537849825', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/712801AE675DD3D65D602040B79FBB2A/100', '1.0', null, null, '712801AE675DD3D65D602040B79FBB2A', '1');
INSERT INTO `xjm_user` VALUES ('531', '', '13853978209', '0', '0.00', '0', '1537943084', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('532', '', '17313164830', '0', '0.00', '0', '1537946028', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('533', '', '18924104499', '0', '0.00', '0', '1537947417', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('534', '', '', '0', '0.00', '0', '1537962616', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/2iagtqw7iaHzXiaLYSxWVRHrCaVFskpDrib0b1AtBUcoDs0HvVVQ4ej3zwCsCLY6F8vYSUZPLASoajwMMCmpEz8d7g/132', '1.0', null, null, 'oS5GbwtnOSjZbpobkkJXF8-qi_ys', '3');
INSERT INTO `xjm_user` VALUES ('535', '', '18998753701', '0', '0.00', '0', '1537969855', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('536', '冰淇淋', '', '0', '0.00', '0', '1537970000', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/icibD2VeTZmibwicTMyyRXeh7fm7Erz9qENiarrLTR1hODkxpEvhB3IHMLFwffQMjKlrbdZLrEGMA6ichRdK5267KZIA/132', '1.0', null, null, 'oS5GbwoDTzFigB7xDu0qIY2FmAOk', '3');
INSERT INTO `xjm_user` VALUES ('537', 'A于 胜@', '', '0', '0.00', '0', '1538090674', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/RjLb7R8bCqicOhqpdgutFeRRPc3h78JttvHwnuJHpBHK0SGwncworjP2qGDpIC9WibGqTNcAPodX0vCCfwEWCR0g/132', '1.0', null, null, 'oS5Gbwv6jpmEY6nfY9nQSXC3BAcc', '3');
INSERT INTO `xjm_user` VALUES ('538', '', '', '0', '0.00', '0', '1538093949', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DGEwGjGX08N96icJiaVT0Y72LlcC4Hlyeo3w0yMvkU5pvS3Bq1NMlIvuz7R39sOwkY1C2LEfo6lTcDnr9QiaQPN0g/132', '1.0', null, null, 'oS5Gbwlwf4cG_Ru2PDmHZsUTU5H4', '3');
INSERT INTO `xjm_user` VALUES ('539', 'tanjw', '', '0', '0.00', '0', '1538094716', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83erhwcH9uJsagQFGz3mPSrYdh1R86xicnSY5H3b8yjSFPicCUNDbaQCZXVmiaM1ial9VGTXFlELeYjfT6A/132', '1.0', null, null, 'oS5Gbwtk3b0Kzt1WFB5X4q-7-Lc0', '3');
INSERT INTO `xjm_user` VALUES ('540', '石材装饰，档次的提升Mr .高', '', '0', '0.00', '0', '1538113774', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTI34A8lRUxHPNJBaAcXaDZ3zy8jZ10za3vFp1KXMegQyLrGbwuJk8YW5xlJicjNIE16SicKeSicPiaO8w/132', '1.0', null, null, 'oS5GbwgRfWqf1-jws9pLWUoAV-HY', '3');
INSERT INTO `xjm_user` VALUES ('541', '龙虾神女跳着走', '', '0', '0.00', '0', '1538123335', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/BagUJIjSicHHrla71IW0pNCop340Wuln0e2up7CazwXUSaXg5EgK4HX2JzUFlEggdEhtD4g5nTgc7KlNk1aAqqw/132', '1.0', null, null, 'oS5Gbwonb8i4tyS1vu0J8sXlQ7ag', '3');
INSERT INTO `xjm_user` VALUES ('542', 'Alex', '', '0', '0.00', '0', '1538123493', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/PiajxSqBRaEKrwuibXjwLYu0ZNfYHvN73bMUEfFFlxibFfkKib6C9Dyz7QoGWH2FSbK2gySn3aCqkKk49BgmDU18xA/132', '1.0', null, null, 'oS5Gbwm-iIVO0uIATgZfRU9nOlFk', '3');
INSERT INTO `xjm_user` VALUES ('543', '南山居士', '', '0', '0.00', '0', '1538123526', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/czv8EQDARl7sjGbNeRUF36hdcXQQNIl0CiaPIZLQNcrEwvN7AmxjnoGFOPibhWojQkJxfwKHhHOAWdswbKIPlxdw/132', '1.0', null, null, 'oS5Gbwujh1rOF36s715hg-UdP5k4', '3');
INSERT INTO `xjm_user` VALUES ('544', '朗月', '', '0', '0.00', '0', '1538123547', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/TGtOj8azRhsEicVToPH65FKCyLs8icwtOzYakzDkBOjKibBWSXibPMd9iafftpGaFGoy3Sl1J8cx9MhTib9Onjc8mjsQ/132', '1.0', null, null, 'oS5GbwoJ-JmWtv8bNMB27_gWU-Qo', '3');
INSERT INTO `xjm_user` VALUES ('545', '张度', '', '0', '0.00', '0', '1538128687', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJNlu1QPdTAxKp2By6icCoFDalhZJZNoibiaBAkAl5Fk36ppfct1fekBuUKyKiaZbWvmzrOp9aDhricafA/132', '1.0', null, null, 'oS5GbwtZVxzcMYGwuGEu5k_iGjbM', '3');
INSERT INTO `xjm_user` VALUES ('546', '꧁꫞꯭不꯭帅꯭气꯭先꯭森꯭꫞꧂', '', '0', '0.00', '0', '1538131723', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83epWDxbdBS3Rqlngd3bIVlqh5LRjfZLBVvCtkibia7lCRXia1CZX1V3oytsxiccxEGMicODLalibHCmiaoxiaw/132', '1.0', null, null, 'oS5GbwuNzxVmALeIOReFPzHv2p7Q', '3');
INSERT INTO `xjm_user` VALUES ('547', '，，', '', '0', '0.00', '0', '1538139302', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJPTgdHr6g18jvvo7VYuarAaSTuRrBBvzNzkKMm95VkKJTEkKbXLn1ugUuEVgw4e7sg92oBLwBiciaQ/132', '1.0', null, null, 'oS5GbwiDvQtN12xhpa0Ff9dkPJi4', '3');
INSERT INTO `xjm_user` VALUES ('548', '吾思', '', '0', '0.00', '0', '1538140201', '0', 'https://thirdqq.qlogo.cn/qqapp/1106555152/12F807BD110464BE7CB9C148FF926A31/100', '1.0', null, null, '12F807BD110464BE7CB9C148FF926A31', '1');
INSERT INTO `xjm_user` VALUES ('549', '会心@爸爸', '', '0', '0.00', '0', '1538145623', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/TDiaaVrllLHy1qhia6f5rBc6bUq2P714c0CXNV5qz8NII5rbUNKy1XK4Ll5syZDe0FGC27QS4yLXSdAtbVucGstw/132', '1.0', null, null, 'oS5GbwqNXhvQ50UE5fc4nYfZ8LxI', '3');
INSERT INTO `xjm_user` VALUES ('550', '杜康18119999911', '', '0', '0.00', '0', '1538171082', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/PiajxSqBRaEISTcGSXlQ61ygsw1gthXntj7eM0eIq6AhIicD4HOGd8F1Py5LrgyyIeZ6QLrFh9GyfT6NsARc4HWw/132', '1.0', null, null, 'oS5GbwpVnn5lM-cFOmO6S0uNHUpA', '3');
INSERT INTO `xjm_user` VALUES ('551', '韩丽阳', '', '0', '0.00', '0', '1538199411', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/5EC54619403F3AEEB64FAC5074782B9A/100', '1.0', null, null, '5EC54619403F3AEEB64FAC5074782B9A', '1');
INSERT INTO `xjm_user` VALUES ('552', '林子', '', '0', '0.00', '0', '1538238176', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/PiajxSqBRaELib9x7MxLwbcR5wLHiawlSmH2OQoyUVcsia8icWBQ4J6zgZ5QPttajdb9Iz3RgzMloGfHy5mJ5wlK6oA/132', '1.0', null, null, 'oS5GbwhTuyveega-Fzp019HVjygA', '3');
INSERT INTO `xjm_user` VALUES ('553', 'DD..', '', '0', '0.00', '0', '1538268626', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqNSicXvCyspkibNYv57sL7l3YbEOp1Lbv6hGrM9xC39Ar6ZweawPe6nrTEhhgGNNeFnNmCs7gCib4CA/132', '1.0', null, null, 'oS5GbwmYRF8xCQY1uqZC2hbdx-8M', '3');
INSERT INTO `xjm_user` VALUES ('554', '勿忘初心', '', '0', '0.00', '0', '1538276204', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/ejH5Kh9NVabhY4stjvUSehpz8rhXCYxkYylZ50MaHx2o9RXiaggArYvjR60VlXdl6icvLR8z5axBYBDibINrr4ylw/132', '1.0', null, null, 'oS5Gbwtk9AS08bRAABQPQQqMMhiA', '3');
INSERT INTO `xjm_user` VALUES ('555', '龙若', '', '0', '0.00', '0', '1538276331', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJiaPd51cbVTwXeUdq08svKjQFMeFw4TnPo6YGA1D4RrV1G5R6UUqoWaibsNTzB4oI9l5kOYPAiaYv3Q/132', '1.0', null, null, 'oS5GbwgJdRQN2VXpoUQNuFCbc0DU', '3');
INSERT INTO `xjm_user` VALUES ('556', '何建聪', '', '0', '0.00', '0', '1538278745', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/bmLc7cPQDH2Hbd9IeXmiaapRRCFh9Re7B4xfOSGYADO6hLtokqqYqvGoGGiadfthwE0Sx0WjVd9OMk3chCR3CrMQ/132', '1.0', null, null, 'oS5Gbwpx4QWUmeDkvc5qVC58D1Po', '3');
INSERT INTO `xjm_user` VALUES ('557', '木头^O^', '', '0', '0.00', '0', '1538284890', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLCD5IurQjk2pmJMEzUADexvBDwy3T5FmxHwXtH4UxtDUSUm1rCo5nfuQbwJq6ZoqwJUSajpJWXfw/132', '1.0', null, null, 'oS5GbwpHJcuVcdgdwNQTbwHHT_L0', '3');
INSERT INTO `xjm_user` VALUES ('558', '', '', '0', '0.00', '0', '1538291811', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/TpFmA9qRwQespDDx5Zeuty65XRBcQsaWD52rRiaMc0PHnSq2T67lssJY9ahdDhOISf5LiaUY7GzBlAGJtGfZzCXw/132', '1.0', null, null, 'oS5Gbwozv9MisijSHBhbHff2U1pY', '3');
INSERT INTO `xjm_user` VALUES ('559', '老柒', '', '0', '0.00', '0', '1538313335', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/ergcvdrviabUImht04bYCJkS64icB7Zkd7LjHWAEdIajZIdLTSiaAwc85mcIkFUY6dycYwZqibsPK8aRvkjeIF7m2Q/132', '1.0', null, null, 'oS5Gbwr0Gkc14TlFMag2TGWBI_Nk', '3');
INSERT INTO `xjm_user` VALUES ('560', '生姜一号', '', '0', '0.00', '0', '1538464357', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/295C38AA8FBB385C7EE954372ABCEF0B/100', '1.0', null, null, '295C38AA8FBB385C7EE954372ABCEF0B', '1');
INSERT INTO `xjm_user` VALUES ('561', '', '', '0', '0.00', '0', '1538473158', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/AB439BED708E856E7174430B02520504/100', '1.0', null, null, 'AB439BED708E856E7174430B02520504', '1');
INSERT INTO `xjm_user` VALUES ('562', '大宋', '', '0', '0.00', '0', '1538475704', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eq0N3O3a5OxyqWJRrtPRaGtS3a8s80LZUZ2QNOBGArNbU2LGlgqiaLMttIa5ibBiacIoryt8DzRcS9GQ/132', '1.0', null, null, 'oS5GbwoFKE74Mces6I6lkHd1MK9g', '3');
INSERT INTO `xjm_user` VALUES ('563', '空海@', '', '0', '0.00', '0', '1538487677', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q5D3g8sNFOrbpiaxEfOLlUxpFmlOh9htA07HQd9gzNkT28zPs23MHN6RRaldiaydU5bWuNSjtM8E6sqSFicViacQdA/132', '1.0', null, null, 'oS5GbwuRFHu6s2XxuR_u5snWENJw', '3');
INSERT INTO `xjm_user` VALUES ('564', '施洪伟', '', '0', '0.00', '0', '1538499828', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJuerIm7hcKHgKJmibyiauziaZKaTsVauh7qLia7nj2xuJ9tk9dhMfVib6PpeTUL9bV70Ik2ES5A65BC7w/132', '1.0', null, null, 'oS5GbwkGAO_pHE55NV39ONrv97jw', '3');
INSERT INTO `xjm_user` VALUES ('565', '夜来香', '', '0', '0.00', '0', '1538541155', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/ENj0lvlcHT1ObheM0erDyRiaorfcibhgicNkibZMZQ2yk1FIBMH4lKBiaRibiaIHQ39O5GtP940zZDibFqyicicUM6WG0tZw/132', '1.0', null, null, 'oS5GbwimG06NyCTKdC24cMkvnKuE', '3');
INSERT INTO `xjm_user` VALUES ('566', '王', '', '0', '0.00', '0', '1538637371', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJLKJk6OZ0FdxgJq9XficW741OQAY1BykjZEbqN19lLYiahvkvjUGCsWtiaLLZLxicxicD5dNNas0QWRUw/132', '1.0', null, null, 'oS5Gbwp7t3Ji_QzYGU53BkBVDVTk', '3');
INSERT INTO `xjm_user` VALUES ('567', '4N', '', '0', '0.00', '0', '1538655800', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/18B7401C521A7AA9E117E839C5E916F9/100', '1.0', null, null, '18B7401C521A7AA9E117E839C5E916F9', '1');
INSERT INTO `xjm_user` VALUES ('568', '嘘，安静', '', '0', '0.00', '0', '1538660451', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/4BD213D65CF6EA6580FFC8F9275FC948/100', '1.0', null, null, '4BD213D65CF6EA6580FFC8F9275FC948', '1');
INSERT INTO `xjm_user` VALUES ('569', '月桂坊', '', '0', '0.00', '0', '1538683560', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/7D24110B547DF2C7BD15B7A16FFE86B8/100', '1.0', null, null, '7D24110B547DF2C7BD15B7A16FFE86B8', '1');
INSERT INTO `xjm_user` VALUES ('570', '邂逅的回眸', '', '0', '0.00', '0', '1538726168', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/AXLO2MHYamhQ2SiaNtNnkOuD7LqKDMRN6IVAJ1uIm2h7UZ03vj1m7jpwE3vf7gXQKibVttoXcwSnicvCNpEzRNZIg/132', '1.0', null, null, 'oS5GbwjCs48f-1Dk1vUkPD0sO3hk', '3');
INSERT INTO `xjm_user` VALUES ('571', '你初中学弟', '', '0', '0.00', '0', '1538753554', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/8icPRK2TpTpiaFOCvicpbibOlFsbQwllI7Y0lv4RN98uBichickQhVbOibFFca8hr6ibOJaibXCZvU3Mec0tj2sBzGj0xRw/132', '1.0', null, null, 'oS5GbwpVKbbTZEaa6DKIO7km_rR0', '3');
INSERT INTO `xjm_user` VALUES ('572', 'A008广利通讯', '', '0', '0.00', '0', '1538815887', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/UOjSnjVELCkxZib7hibfw1IhRZTLqjhpT90zQpgxKWdKJpR1icTygeHyB27n1o3VZcw6YJtsZdj6rIwOcXqBxc2uA/132', '1.0', null, null, 'oS5GbwgNUsveTzgGDxqVdWGEomv4', '3');
INSERT INTO `xjm_user` VALUES ('573', '张文宝  15326949456', '', '0', '0.00', '0', '1538832361', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/9DBUNd9N1QPsP1GIOyg26nB9eiaXdaECsqqiclJHv0sEVaecKFliaqsHQkfPA3XL3m63cWJcFeibhosPicS7bB7GZibQ/132', '1.0', null, null, 'oS5GbwgVaM9Y4U87_r4Psn1TEpR8', '3');
INSERT INTO `xjm_user` VALUES ('574', '朱建龙', '', '0', '0.00', '0', '1538904380', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLQpvhDSVINF0v4oDUgfEKHPSPoXL7juCLh6mgnicmUAQEB4lRn2iaVTrDtGzLlC2a8R85yMbu0HQzA/132', '1.0', null, null, 'oS5Gbwuw-3ZFFeMDvYjK6AB9fNsM', '3');
INSERT INTO `xjm_user` VALUES ('575', '往事随风', '', '0', '0.00', '0', '1538917437', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/7B5C52A6F164D1B2C304D0218BDBAB8E/100', '1.0', null, null, '7B5C52A6F164D1B2C304D0218BDBAB8E', '1');
INSERT INTO `xjm_user` VALUES ('576', '饶烜晔', '', '0', '0.00', '0', '1538919740', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/948yJZy09pKicddOjoh7knp00ibQTvHftRFkSwxen9m4Wibic0ibMAiaeKMicEoquicFt1ZLvgicqu1xYXBQ49CsU5pdBxw/132', '1.0', null, null, 'oS5GbwoDZVpVFAnvZrolLbaT6Ndo', '3');
INSERT INTO `xjm_user` VALUES ('577', 'M T', '', '0', '0.00', '0', '1538922858', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoV4N4riaSGMwVdY0y8Yk8Y7lKpHDZyUMBD9GTaUYiamEn2B8OFL866yicnzSsA2Sk8mop05DVXaEM8w/132', '1.0', null, null, 'oS5GbwiHP8cwEZPJUtSVnq_h5CYE', '3');
INSERT INTO `xjm_user` VALUES ('578', '练兆宗', '', '0', '0.00', '0', '1538987850', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/EfQGsAWQJhn4EAFoBicCem9P2pxYncsoTfH8gZTr6yX19mwJpfBevp76wia2czjDnD59PCPjIDEkOmApo8lQywkw/132', '1.0', null, null, 'oS5Gbwsh_ro4EbgVezwpsS6mji28', '3');
INSERT INTO `xjm_user` VALUES ('579', 'A-昌', '', '0', '0.00', '0', '1539106417', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/icJCIVzDOTmwLg7umWwlQuDnwJXT6c9LMJRSEibfE6dFERL92APjE84jH7T44GJS2G1Nxp79KgUdXE2EtLHMRiavg/132', '1.0', null, null, 'oS5GbwjD6IQ9HkfRY5FwzPls9gfw', '3');
INSERT INTO `xjm_user` VALUES ('580', '金字塔$$¥¥', '', '0', '0.00', '0', '1539134615', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/3C3dticRV4ApLibwib28w1Z1GGlW8wDRIfWGeHxibmnaKbNsudutJ5Zp2N8dgqeTpXa8h4ibkHiav5EacMDH1fm5ZInQ/132', '1.0', null, null, 'oS5Gbwozvs98yR_u-gmVAf5NRqzg', '3');
INSERT INTO `xjm_user` VALUES ('581', '～萍', '', '0', '0.00', '0', '1539241095', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/CRuWo9OGHvibERrTVTumtrfL1dj7HNuRtCpX4NaIJwQPzoWia1fQC0GWqz5CiaD4jZEI3kqCBPFNyc0aeRY0x5ic5w/132', '1.0', null, null, 'oS5Gbwk-aRQq1lg1UhZP4GLKID0U', '3');
INSERT INTO `xjm_user` VALUES ('582', '', '', '0', '0.00', '0', '1539322732', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/oHWfrFczyrLSK3ujvYWcicqDfdAYzQ3PLA5jxC3YeKic9kicjopf78RJzMXf739TCSOZI6CpsUfP1ahkJuwwf0fLA/132', '1.0', null, null, 'oS5GbwtvnFW4OMgT9zDmsoc8AUho', '3');
INSERT INTO `xjm_user` VALUES ('583', '刘秀燕13715214786', '', '0', '0.00', '0', '1539329489', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/NTeSMsntYPL6OecBBUs9STVgDMtaZCjich8d8owmb5JYicNQGgI716kVfd55Gq5KNOBtDrvayfvzjsia5AGDDiaM8A/132', '1.0', null, null, 'oS5GbwmpVj_05hPt9yx-6VZ2D4y0', '3');
INSERT INTO `xjm_user` VALUES ('584', 'Mr. Wang', '', '0', '0.00', '0', '1542973500', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83erFicVQYVlT9K7SqWylibtuicK7myYfrOpia25aQACDEf2mCSy22OabJDXyGkiae3YycXzPCjB39cC4kRQ/132', '1.0', null, null, 'oS5Gbws2ZAi36FpeyhnWyoJOR1oc', '3');
INSERT INTO `xjm_user` VALUES ('585', '黄海龙', '', '0', '0.00', '0', '1542978854', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/DE65572E26CE39A748C5D619687ECFAD/100', '1.0', null, null, 'DE65572E26CE39A748C5D619687ECFAD', '1');
INSERT INTO `xjm_user` VALUES ('586', '', '13345964134', '0', '0.00', '0', '1543030793', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('587', '', '13283725550', '0', '0.00', '0', '1543144171', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('588', '', '15321520093', '0', '0.00', '0', '1543147477', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('589', 'AIIen~艾伦', '18868929901', '0', '0.00', '0', '1543245873', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto58920181126232730', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('590', '', '13349931989', '0', '0.00', '0', '1543292492', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('591', '', '15172419658', '0', '0.00', '0', '1543471330', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('592', '', '18627000898', '0', '0.00', '0', '1543471555', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('593', '', '17683797607', '0', '0.00', '0', '1543488769', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('594', '', '13712497210', '0', '0.00', '0', '1543491895', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('595', '', '15968557724', '0', '0.00', '0', '1543572214', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('596', '', '15068983915', '0', '0.00', '0', '1543579329', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('597', '皮皮鲁', '', '0', '0.00', '0', '1543643305', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIwkDPS058GHYsLJAghKVaMSOcIiavrUiaj3ib3tZTLoXXoMD6UKWu3ly6wibKNgkiaOvbDuiamOlJQM9pw/132', '1.0', null, null, 'oS5GbwoHVhWS55rko6W5S4dpPRcM', '3');
INSERT INTO `xjm_user` VALUES ('598', '', '18216236992', '0', '0.00', '0', '1543671194', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('599', '', '17624011313', '0', '0.00', '0', '1543679607', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('600', '卫国', '', '0', '0.00', '0', '1543721597', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/eY14AWYFwYAdFuZibxiaBFzniaB7OmrCficgicza0wbSwYVwybtibSVHJEm54PdfoK6pNqf0ljddYvPQlKA5CloCT4Gg/132', '1.0', null, null, 'oS5GbwmsyJH6mL1zXEzxLX9ORmbw', '3');
INSERT INTO `xjm_user` VALUES ('601', '仙哥', '15736261967', '0', '0.00', '0', '1543725898', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto60120181202124819', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('602', '', '13317181947', '0', '0.00', '0', '1543728160', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('603', '', '18832985395', '0', '0.00', '0', '1543732285', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('604', '吉祥如意', '18986312599', '0', '0.00', '0', '1543760384', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('605', '', '15327190700', '0', '0.00', '0', '1543762789', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('606', '大道至简', '', '0', '0.00', '0', '1543762818', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/uGOeNuw7ln4BEhraKFAojzUg5gysbmXhfNcVVZusu8DAg3S85alBp7muN2Q2zol4ibXJaNtwrB3kCoPHx6LN2icg/132', '1.0', null, null, 'oS5GbwukoYcbJiVKzPUQO3Wz0SJI', '3');
INSERT INTO `xjm_user` VALUES ('607', '', '13545123138', '0', '0.00', '0', '1543766330', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('608', '杜建冲', '', '0', '0.00', '0', '1543766829', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqZQtVPPaqFOJe9jRmU7MibjTODyFF4ibaO0wdN03Ju1eH2uGBEdbCFGeicsvfvr16Y0VhNAMzCZIZoA/132', '1.0', null, null, 'oS5GbwtqyKZby1Te84CQjHJF8lng', '3');
INSERT INTO `xjm_user` VALUES ('609', '地球村村民', '18986127072', '0', '0.00', '0', '1543818838', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('610', '', '18351696500', '0', '0.00', '0', '1543819217', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('611', '', '13554295663', '0', '0.00', '0', '1543819733', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('612', '', '15549492989', '0', '0.00', '0', '1543820513', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('613', '', '19945051192', '0', '0.00', '0', '1543821842', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('614', '', '13371637861', '0', '0.00', '0', '1543825159', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('615', '峰哥', '', '0', '0.00', '0', '1543827965', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJRk5lBexKhEAElv3LLpiaOfX0wLN1T3goic88l07QzpIkcRUGMQO7Bg212Z05y81wBud8BmMIMqDBg/132', '1.0', null, null, 'oS5Gbws7JNfx-A_4FB0M4wUzWlwk', '3');
INSERT INTO `xjm_user` VALUES ('616', '峰鼎', '15989378878', '0', '0.00', '0', '1543836525', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('617', '', '13962984333', '0', '0.00', '0', '1543846143', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('618', '_L。', '', '0', '0.00', '0', '1543846791', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ericvEsDP3Mkk0g0gBB4JN10nsTFoJNo2SZQib6h9Xo9UIs27sfBFxZaMGeWic2ECXPb0zpHrJxjpZ5A/132', '1.0', null, null, 'oS5GbwrNkkYYsKCHSOXScyNfTpnQ', '3');
INSERT INTO `xjm_user` VALUES ('619', '专业手足护理', '', '0', '0.00', '0', '1543849099', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKVpbzSRxmFSOqRmRGN2w2Nu63Rgb1m7I43jyOEaujGJBt4wfBaibjY98v1yEOznwWylhPykicZc1ibQ/132', '1.0', null, null, 'oS5Gbwk0Q8gQGsLV1Bj1wALc_uk4', '3');
INSERT INTO `xjm_user` VALUES ('620', '宋丹', '', '0', '0.00', '0', '1543890620', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIic3Jkt72JkyevNA7Zy0dDbpWJ8OXIog8zM8jEMBCib5nQogMLv6GY6B2oLzXpT99p1Eo2dmADfxtw/132', '1.0', null, null, 'oS5Gbwup1XXg7tTEPIcjptfqECrA', '3');
INSERT INTO `xjm_user` VALUES ('621', '', '17762816087', '0', '0.00', '0', '1543898833', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('622', '', '17727491431', '0', '0.00', '0', '1543973876', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('623', '', '13828830924', '0', '0.00', '0', '1543995380', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('624', '', '13600783365', '0', '0.00', '0', '1544017759', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('625', '', '16655013469', '0', '0.00', '0', '1544019860', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('626', '', '15298806478', '0', '0.00', '0', '1544020469', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('627', '', '19806586787', '0', '0.00', '0', '1544060437', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('628', '', '13860970802', '0', '0.00', '0', '1544083772', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('629', '', '19991858547', '0', '0.00', '0', '1544085030', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('630', '', '15737600708', '0', '0.00', '0', '1544092592', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('631', '', '15959950655', '0', '0.00', '0', '1544100299', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('632', '', '13459598625', '0', '0.00', '0', '1544104129', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('633', '', '15269594236', '0', '0.00', '0', '1544104519', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('634', '', '13544245291', '0', '0.00', '0', '1544164868', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('635', '陈先生', '', '0', '0.00', '0', '1544164938', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/z3REBgicjSKCHJX9Fb5mfjYbiaT1bKubynAchicbBhEpN3mbibzYVU733nt4KibVdFiaQNFibCx7l1PwZAfazC1SFszgw/132', '1.0', null, null, 'oS5Gbwtj7JhazZPgz3fs8HbsCJfE', '3');
INSERT INTO `xjm_user` VALUES ('636', '', '17790532683', '0', '0.00', '0', '1544267514', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('637', '', '18845016518', '0', '0.00', '0', '1544276537', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('638', '', '13309169008', '0', '0.00', '0', '1544325232', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('639', '', '13657260351', '0', '0.00', '0', '1544325427', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('640', '', '13333299088', '0', '0.00', '0', '1544333675', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('641', '', '13357594999', '0', '0.00', '0', '1544334533', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('642', '奔腾', '', '0', '0.00', '0', '1544343413', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoYeECRnptV54bxQ2GkHxBIDdYQmIib85VtxlmxkqGM4oY8JkG5NsXftZ7icwqVgtiayVAC7sy9EIjpw/132', '1.0', null, null, 'oS5Gbwp457xBuH6tzWzkY1Z8qaVE', '3');
INSERT INTO `xjm_user` VALUES ('643', '', '15903972699', '0', '0.00', '0', '1544343974', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('644', '趋势玩家', '', '0', '0.00', '0', '1544367671', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/PicaCPy59jUaHSobLk84ZNxZmqkiceC9kSeEIODzCm6Wzp87sVXRYX3SQnYwILoicAItl88oCOAxGUaGhA1exiaegA/132', '1.0', null, null, 'oS5GbwsYJSnLsTqANfXsSs-WbP5k', '3');
INSERT INTO `xjm_user` VALUES ('645', '', '15160477985', '0', '0.00', '0', '1544410686', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('646', '', '13030836666', '0', '0.00', '0', '1544424971', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('647', '唐伯虎点蚊香', '', '0', '0.00', '0', '1544579760', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/67A8A3FF1C333590BA8F9A01E8FD0945/100', '1.0', null, null, '67A8A3FF1C333590BA8F9A01E8FD0945', '1');
INSERT INTO `xjm_user` VALUES ('648', '蜜梓琦', '18692015873', '0', '0.00', '0', '1544617919', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('649', '', '15309169099', '0', '0.00', '0', '1544623611', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('650', '榕树，黄', '13328788888', '0', '0.00', '0', '1544666307', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto65020181213100242', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('651', '尐晴迗', '', '0', '0.00', '0', '1544666644', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83epibOMb0f9IeBKXXYWG9RrsEkeFH7tuWOurYmpcjrGBfTgAHHany9ksZxQdwniaU7VoWso8icSe9tHGA/132', '1.0', null, null, 'oS5GbwiVXJBDaG4S3IKgL6fjxnGM', '3');
INSERT INTO `xjm_user` VALUES ('652', '', '17322449897', '0', '0.00', '0', '1544694180', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('653', '', '13823912365', '0', '0.00', '0', '1544695056', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('654', '', '18666010600', '0', '0.00', '0', '1544695325', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('655', '福缘创富系统', '18975810377', '0', '0.00', '0', '1544695585', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto65520181215131752', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('656', '宋成-马到成功', '', '0', '0.00', '0', '1544709051', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/F8TUzx6cFyLrzuxJSicNiaE6aQvM0t0K3ad1BKbR1BVbRopsQUibiablKOWqQVeCQWg3U2Vw2NKCSXeCF77eo1tUmg/132', '1.0', null, null, 'oS5GbwgD5WJe9WYKxNyML0Ws_3yg', '3');
INSERT INTO `xjm_user` VALUES ('657', '', '15113375199', '0', '0.00', '0', '1544752448', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('658', '爱奔跑的猪', '13554141532', '0', '0.00', '0', '1544755354', '0', 'http://pic.xijiaomo.com/20181214104332.', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('659', 'BO', '', '0', '0.00', '0', '1544759233', '0', 'https://thirdqq.qlogo.cn/qqapp/1106555152/CA72EDEFFD0C543022B088E868C7FB50/100', '1.0', null, null, 'CA72EDEFFD0C543022B088E868C7FB50', '1');
INSERT INTO `xjm_user` VALUES ('660', '啊啊啊', '15907150581', '0', '0.00', '0', '1544759233', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto66020181214114748', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('661', '灬灬灬', '', '0', '0.00', '0', '1544759322', '0', 'https://thirdqq.qlogo.cn/qqapp/1106555152/409D946E266BFDEB6E58232AEA306565/100', '1.0', null, null, '409D946E266BFDEB6E58232AEA306565', '1');
INSERT INTO `xjm_user` VALUES ('662', '', '17671746469', '0', '0.00', '0', '1544759351', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('663', '      Aj。', '', '0', '0.00', '0', '1544759449', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/7B4DBE5356530409E0A2F6FE91C0BE73/100', '1.0', null, null, '7B4DBE5356530409E0A2F6FE91C0BE73', '1');
INSERT INTO `xjm_user` VALUES ('665', '庸人自扰～', '', '0', '0.00', '0', '1544763442', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/ic2UJsvsaRIOYe0Axjds10BCI1Z6a1VJm1oB6nxtnp1uBjsicbts5l8xQyd8m80nngbF3zX4YRl4jCbqEOs1r6eQ/132', '1.0', null, null, 'oS5Gbwr1du6Spblww6iww3HGjbMk', '3');
INSERT INTO `xjm_user` VALUES ('666', '朝阳小叶', '', '0', '0.00', '0', '1544764171', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIh35CvxVmuqZzvBylPPfWReQUyXUia971uDXFXibOJNNJiaXbFNp5xt3l98FViaWkceVbpAiavWZmMjRg/132', '1.0', null, null, 'oS5GbwlJ0qAcKBHYfG1mBOYw44N0', '3');
INSERT INTO `xjm_user` VALUES ('667', '　     ', '', '0', '0.00', '0', '1544783702', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/917433B18483D1156819912B24FCC9EA/100', '1.0', null, null, '917433B18483D1156819912B24FCC9EA', '1');
INSERT INTO `xjm_user` VALUES ('668', 'Pluto', '', '0', '0.00', '0', '1544804838', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTI44qtT9fl5kssxw2KmB0R6VyicDSRZyBHarXeibtvFeAEJ01ljTJStIw4yOcXKrFUWuKfHDKsCNC3g/132', '1.0', null, null, 'oS5GbwrlhxjT8WTrVIWVZ2ZIwttQ', '3');
INSERT INTO `xjm_user` VALUES ('669', '甜儿', '', '0', '0.00', '0', '1544810191', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/4A81C425D85F3B791927F8B213891FBA/100', '1.0', null, null, '4A81C425D85F3B791927F8B213891FBA', '1');
INSERT INTO `xjm_user` VALUES ('670', '', '13518082677', '0', '0.00', '0', '1544849332', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('671', '椰雕咖啡', '', '0', '0.00', '0', '1544849357', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eo50M6vKeefpVuRdnAiaYFxOlCf4LqnhdGGMbq302nWic4EmotyoiaCP4YQgHKrzGicytmgqaxiadibXIQg/132', '1.0', null, null, 'oS5GbwuEgpyI5y1oj9fgEZWh94QU', '3');
INSERT INTO `xjm_user` VALUES ('672', '', '15959095329', '0', '0.00', '0', '1544852851', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('673', '', '15221982868', '0', '0.00', '0', '1544855983', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('674', '斌钢小张', '', '0', '0.00', '0', '1544861789', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/7C646CF9E26C4B3870828429D59306CA/100', '1.0', null, null, '7C646CF9E26C4B3870828429D59306CA', '1');
INSERT INTO `xjm_user` VALUES ('675', 'T·S·', '', '0', '0.00', '0', '1544972622', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/afLE5uUUmwzFialPicEhZJOugcaicOXauZM5JImeus7DYqp76Jj87c2icLvibiamkdk8icBYwUibmpnMSdmMoX1XCArB8Q/132', '1.0', null, null, 'oS5Gbwt9zhUpcjsG02DiDaC1-ewE', '3');
INSERT INTO `xjm_user` VALUES ('676', '方圆', '', '0', '0.00', '0', '1544974218', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKo7rkVDvPp7tZEb7QQW6M5PRTRTNgkFI3eVg4f8qMdmxrZNJVIJpzQsmqa0xfnzgLyrg1b7yzpwA/132', '1.0', null, null, 'oS5GbwrXMnctQVxhVE8tRlUf0PJg', '3');
INSERT INTO `xjm_user` VALUES ('677', '三千方|家全套、海利不锈钢、门窗', '', '0', '0.00', '0', '1545006574', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/znU3lFywbjHW8krKu8lD2bGfXPj7TSkrssKDZiaibD2b3h3rY8BnnzpbMHyaSM9Gr8CmibazEg6iaplhlrD0I7JVcw/132', '1.0', null, null, 'oS5Gbwq_GqgmB996llUw1pz9uAg4', '3');
INSERT INTO `xjm_user` VALUES ('678', '', '18954171227', '0', '0.00', '0', '1545094596', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('679', '易靖杰', '', '0', '0.00', '0', '1545115347', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/oxEWlb3CAXdKfR038S2xqRyMZks0ObhkLW6gyictLAcuAWrtIpZMFDMCFqfz0l4FlwHf3iaicm23Q1Rkt3Za48MUg/132', '1.0', null, null, 'oS5GbwlsQdqI3w-Op8zigHa-o4Bw', '3');
INSERT INTO `xjm_user` VALUES ('680', '', '13752960939', '0', '0.00', '0', '1545153831', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('681', '', '18428382337', '0', '0.00', '0', '1545264376', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('682', '', '17707267486', '0', '0.00', '0', '1545285219', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('683', '情绪。', '', '0', '0.00', '0', '1545285228', '0', 'https://thirdqq.qlogo.cn/qqapp/1106555152/C88A5797CB0E7311E87AAE3DD8A64EAC/100', '1.0', null, null, 'C88A5797CB0E7311E87AAE3DD8A64EAC', '1');
INSERT INTO `xjm_user` VALUES ('684', 'Wao.', '', '0', '0.00', '0', '1545285230', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/77ECA9913BCFC0C47E8FEBCF54738F8B/100', '1.0', null, null, '77ECA9913BCFC0C47E8FEBCF54738F8B', '1');
INSERT INTO `xjm_user` VALUES ('685', '。', '', '0', '0.00', '0', '1545285236', '0', 'https://thirdqq.qlogo.cn/qqapp/1106555152/F08D3E9D92697A927DD85DAA0EF72504/100', '1.0', null, null, 'F08D3E9D92697A927DD85DAA0EF72504', '1');
INSERT INTO `xjm_user` VALUES ('686', 'Y.', '', '0', '0.00', '0', '1545285253', '0', 'https://thirdqq.qlogo.cn/qqapp/1106555152/5D9AE9CBB010784CA70E0ABACE2D56B2/100', '1.0', null, null, '5D9AE9CBB010784CA70E0ABACE2D56B2', '1');
INSERT INTO `xjm_user` VALUES ('687', 'crack a smile', '', '0', '0.00', '0', '1545285289', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/DDC54E4246D8DA0BEF07E2E78EA4AC19/100', '1.0', null, null, 'DDC54E4246D8DA0BEF07E2E78EA4AC19', '1');
INSERT INTO `xjm_user` VALUES ('688', '', '19978534131', '0', '0.00', '0', '1545296299', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('689', '', '17621237430', '0', '0.00', '0', '1545296349', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('690', '金城', '18642163778', '0', '0.00', '0', '1545370493', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto69020181221133727', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('691', '', '13871220065', '0', '0.00', '0', '1545380418', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('692', '胡杨', '', '0', '0.00', '0', '1545402677', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/8526FC651F9170A8B7FDB2D91FF124C1/100', '1.0', null, null, '8526FC651F9170A8B7FDB2D91FF124C1', '1');
INSERT INTO `xjm_user` VALUES ('693', '', '18672214187', '0', '0.00', '0', '1545458370', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('694', '嘀嗒', '', '0', '0.00', '0', '1545460664', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTK05p10mAEszQqRTcHrKrmCdcMbEWiaQ8jvyicB6VBn5F9fPzgMacUliaeaKhXn75LnFaTYPYYsnY82A/132', '1.0', null, null, 'oS5Gbwuq7VW6kxtUSyRkKdZwe9Yk', '3');
INSERT INTO `xjm_user` VALUES ('695', '晨曦', '18300468723', '0', '0.00', '0', '1545462268', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto69520181225160226', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('696', '墨染素年', '', '0', '0.00', '0', '1545472923', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJ9YyiapqC1PYn0iaaa3Yb60Y7pQI1zXXexM1oibCX7iba6acS2RzkOiaKSoXXicwgHMfwRfe0bn421HzmQ/132', '1.0', null, null, 'oS5GbwqH_9kTYtGJVLY87H_gKVTY', '3');
INSERT INTO `xjm_user` VALUES ('697', '', '', '0', '0.00', '0', '1545537776', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIwfpclhibqJnFwLCqe5OdpDNu11icjr4dWkCx9j7zgJvcTPCxae5EMDwPzuVRXrzqhmEjNpsjpicSCg/132', '1.0', null, null, 'oS5Gbwn3OFHESceO-r3PXtLc4V4Q', '3');
INSERT INTO `xjm_user` VALUES ('700', '', '18062394372', '0', '0.00', '0', '1546047197', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('701', '', '18162511920', '0', '0.00', '0', '1546068782', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('702', '张小楠_Nigel', '', '0', '0.00', '0', '1546149544', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/PiajxSqBRaEItYjBiaHtSQBlr6TWDuP7cjicHJF9jvBZvQME2gbL4oub8YUuwDzCOGkE8xcbOLxZYMjcYXicyezibqg/132', '1.0', null, null, 'oS5GbwrgysLzMPjy_ulO1foP9GZU', '3');
INSERT INTO `xjm_user` VALUES ('703', '༒狂༒婵༒', '', '0', '0.00', '0', '1546168323', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/r8Pib0ia1aiaOQq3HsYzrNA1dZ8eYKpD8W9lUvLKT4hVbSNnvg1B3c69PnniboP8ETFlnCFPibvJiaeawzkVspWHXejQ/132', '1.0', null, null, 'oS5Gbwi2Be6jiQna0pWe7JWHUiF4', '3');
INSERT INTO `xjm_user` VALUES ('704', '想念', '', '0', '0.00', '0', '1546175687', '0', 'http://thirdqq.qlogo.cn/qqapp/1106555152/28F916CFD688F7215DF68981C81040A3/100', '1.0', null, null, '28F916CFD688F7215DF68981C81040A3', '1');
INSERT INTO `xjm_user` VALUES ('705', '走自己的路让人去说', '', '0', '0.00', '0', '1546178995', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTI4cU0CFj1HQZUQSFvH53NXXicH4BHgGehEg45o6Hu805oHWutjtmibkJwGsPoKKqmEEkcdK1a9Qvtw/132', '1.0', null, null, 'oS5GbwvtSFiqF07xj-kIMUo7-0Y4', '3');
INSERT INTO `xjm_user` VALUES ('706', '', '', '0', '0.00', '0', '1546394453', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIDGVLADVPJibkd8oUIH7T7SpD7TLNIyWXwK08H9wfqPQyXOou7yp7GoTvyPzjKUDcbTibwibphSdDCw/132', '1.0', null, null, 'oS5GbwhbekKLGQxU0mw-ioW85-Vw', '3');
INSERT INTO `xjm_user` VALUES ('707', '椿。', '', '0', '0.00', '0', '1546395459', '0', 'http://oxll5q098.bkt.clouddn.com/android_xjm_userphoto70720190105155820', '1.0', null, null, 'oS5Gbwn3bIf46peQTke2umgCQFyA', '3');
INSERT INTO `xjm_user` VALUES ('708', '', '15387231319', '0', '0.00', '0', '1546413259', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('709', '！', '', '0', '0.00', '0', '1546420797', '0', 'https://thirdqq.qlogo.cn/qqapp/1106555152/37DF8BDC0E3E646C142A095B5EB1901E/100', '1.0', null, null, '37DF8BDC0E3E646C142A095B5EB1901E', '1');
INSERT INTO `xjm_user` VALUES ('710', '深街酒徒', '', '0', '0.00', '0', '1546491823', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eo35cTiaeGgxbsic6y3hhjm1j7lWWepwnRM9MwQelYCJ40PATdmpmwiceaDWz8xDPvs80T4JJ3aPEj1Q/132', '1.0', null, null, 'oS5GbwoEyc3SbDK2Zu9WDYBAktN4', '3');
INSERT INTO `xjm_user` VALUES ('711', 'Demon', '', '0', '0.00', '0', '1546496485', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqoibibZCpRw5Eiaia1JQFgFGsZdiaBlCGlxHBnHyibfMTK1fwxBLGpyxqeqlW0RBqeBicuiaibTIJuibttLxSw/132', '1.0', null, null, 'oS5Gbwo9Sb2-qbWSCQXBxchX6GnQ', '3');
INSERT INTO `xjm_user` VALUES ('712', '日子都喂了猫', '', '0', '0.00', '0', '1546503004', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/YBWr6vCc8vRre1dKribjwoyiarDyJyCcGGiaCPhJU2icXQS7kKaKnSmEfkiaF8o4nRrGz6nAXFw3SwfUicyHXiaAMJRnw/132', '1.0', null, null, 'oS5GbwjOxzhL8msLwgWN3vHD1Qxw', '3');
INSERT INTO `xjm_user` VALUES ('713', '任伟', '', '0', '0.00', '0', '1546632275', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKFRgRIrFg2p9EtcSuDoVtVya5bO8PCNiczqibqWnNus8R9bRMDic5lMnJoaEwiaE2tye5U85xuuKkvbQ/132', '1.0', null, null, 'oS5GbwuVHLisGTLj5jmLuBwKYNIE', '3');
INSERT INTO `xjm_user` VALUES ('714', '', '15112635776', '0', '0.00', '0', '1546652040', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('715', 'Mc.一诺', '', '0', '0.00', '0', '1546653584', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/YNJNprACIrtA6JFFVek9iaszbBXT3FHUEDGC68vqleDIDBjMo0QN5xzoNJILgNZb8pLddoLmLcurwymD4aiatwFg/132', '1.0', null, null, 'oS5GbwsU8iq7DoX25m0YJofE8hcs', '3');
INSERT INTO `xjm_user` VALUES ('716', '杨闻涛', '', '0', '0.00', '0', '1546655619', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83erv46DSkSc4Ev5X3F9leWCibqBU8v6RbNN6bScQH3PT6epJwImbILXxFkULUQFc1PcDT9iafKw5Hmqw/132', '1.0', null, null, 'oS5GbwtRlmd09sEpbQWIOV8b19RU', '3');
INSERT INTO `xjm_user` VALUES ('717', '', '15171480993', '0', '0.00', '0', '1546674169', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('718', '小武', '', '0', '0.00', '0', '1546674170', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/hzcI0l9A7zEyB9Vsg9kHvVUID4IsCTpX57LmDtuguHcuevMibORP4h2af3pauesZ21icKemG6KMSibf6JGicWHiaT9A/132', '1.0', null, null, 'oS5GbwlVpf-bEZYxkZ8wv-8E5iUs', '3');
INSERT INTO `xjm_user` VALUES ('719', '', '13759688137', '0', '0.00', '0', '1546674219', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('720', '', '18816895699', '0', '0.00', '0', '1546674285', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('721', '叶子', '', '0', '0.00', '0', '1546674366', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83er3Ey0Uq2w4wI7HjibUUeicSgZHtduTu49A7SUBIkyuYrcwHiaPpBRibjDjoLVm0ZXLlxgurSC8z11E5g/132', '1.0', null, null, 'oS5GbwnrqCr9OiSHTEooeUKjjqlc', '3');
INSERT INTO `xjm_user` VALUES ('722', '', '13260564119', '0', '0.00', '0', '1546674411', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('723', '金融e家', '', '0', '0.00', '0', '1546674866', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLXnLYqWRUiaEOXAIQXqn5aDOE8iaKj8qmw15kZfiahBLLecPUjC5WJhYItibYt9HvIlgq9j77fIKjvLg/132', '1.0', null, null, 'oS5GbwkyoFpS5x87n2dHkg-Bx4Qk', '3');
INSERT INTO `xjm_user` VALUES ('724', '', '17708710125', '0', '0.00', '0', '1546739359', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('725', '', '15353575182', '0', '0.00', '0', '1546772201', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('726', '', '17671448546', '0', '0.00', '0', '1546825232', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('727', 'gao ', '18627056710', '0', '0.00', '0', '1546925679', '0', 'http://pic.xijiaomo.com/20190110102533.', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('728', '', '18112916819', '0', '0.00', '0', '1546961777', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('729', '', '13145547155', '0', '0.00', '0', '1547177819', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('730', '何某么', '18521598723', '0', '0.00', '0', '1547185072', '0', 'http://pic.xijiaomo.com/android_xjm_userphoto73020190114142702.jpg', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('731', '', '18147516810', '0', '0.00', '0', '1547343053', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('732', '', '17621064788', '0', '0.00', '0', '1547353503', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('733', '高小波_heat', '', '0', '0.00', '0', '1547518989', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/PiajxSqBRaELmGjr2KYlxvAVo7qd3VLIo7NIgleYnYjrFTfkj3ibOu3TPKCtpQzYwlhuCBru5y7PeeDl4x7UsEUw/132', '1.0', null, null, 'oS5GbwjSUyW4QPyZE7E2lbCkFKvg', '3');
INSERT INTO `xjm_user` VALUES ('734', '恒-Heng', '', '0', '0.00', '0', '1547537716', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTK417e56nng3hNQJRM4Y92h5IxMohicNsYnfXvH7FlE13Hc1JvI69cAqaYknVD5bRib3VSpSPo8RKsQ/132', '1.0', null, null, 'oS5GbwmFwSB4zMy74EOfe4_xRaWc', '3');
INSERT INTO `xjm_user` VALUES ('735', '', '18963992077', '0', '0.00', '0', '1547538586', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('736', '', '13578999712', '0', '0.00', '0', '1547565285', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('737', '', '13926832619', '0', '0.00', '0', '1547702695', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('738', 'A[太阳]大野老师[太阳]A', '', '0', '0.00', '0', '1547798286', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIdn5lOPqwfdFeiaIXmTKlvbPVETYPssKmyn8licI66Obrq4aTYJPfvHeK8AwGN9qre3sM3OFD61ENQ/132', '1.0', null, null, 'oS5Gbwpb2NuWN6b2C7lXjHqChICg', '3');
INSERT INTO `xjm_user` VALUES ('739', '佰善缘', '18062013177', '0', '0.00', '0', '1547883849', '0', 'http://oxll5q098.bkt.clouddn.com/android_xjm_userphoto73920190119154533', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('740', '', '13097210953', '0', '0.00', '0', '1547883867', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('741', '卍 妙程卍', '', '0', '0.00', '0', '1547883916', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLmPeZwjxt8o4JhAetlFIePBej71G39VdyiaY8ibCcAt8zyVe0V732mibqAjYyOulmMWCjQcRTol1pyg/132', '1.0', null, null, 'oS5GbwtLs9S46W-JvXhIB1r4Hx9c', '3');
INSERT INTO `xjm_user` VALUES ('742', '简单', '', '0', '0.00', '0', '1547883939', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/qQOgOlecvibBOWu8TtxYmRn0N8jcPtLgzdJLBibTCjkWtAroO41ichAbN1T6GxhmZItU0JbWzkOmDFae9YqdkVFVA/132', '1.0', null, null, 'oS5GbwiaxSP_Ih8Ha5GX9q3pdyhQ', '3');
INSERT INTO `xjm_user` VALUES ('743', '', '15011749988', '0', '0.00', '0', '1547883972', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('744', '杨承泽', '', '0', '0.00', '0', '1547884010', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKricP7yWQ3Fq5Xqcegxo4TGbMIFtPM9F1IE27kbo9E6zhsFCx5TrlibnTpiaCW49h73XhYHrCsWJICg/132', '1.0', null, null, 'oS5Gbwona7nsi0DtcDYXHC4mRBAc', '3');
INSERT INTO `xjm_user` VALUES ('745', '', '13576432833', '0', '0.00', '0', '1547884025', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('746', '财富15014175068', '', '0', '0.00', '0', '1547884063', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJoOia8n2xdlIGT6lvmKyUmZ8icDmicn0gOkFsAaOr39icHL2Qe4lvLvCppEhgAQeQuia7YibiaMP75yKTlQ/132', '1.0', null, null, 'oS5GbwhJb-G7F08Xee9Ud_y3bsm4', '3');
INSERT INTO `xjm_user` VALUES ('747', '百合\n', '18674148713', '0', '0.00', '0', '1547884186', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('748', '', '13242799955', '0', '0.00', '0', '1547884281', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('749', '', '18658216819', '0', '0.00', '0', '1547884640', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('750', '风雨同', '', '0', '0.00', '0', '1547893213', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/ALRBKlAqXp52fr1EDYdPoepFLgRFPLxXicl8GPPSqw1gbMPsTtHicT7F0LUibgLiaqFHFILma4qYIz5IMGp1oziaY0A/132', '1.0', null, null, 'oS5GbwgOTZVAG5JJqR9aWzVfaB6E', '3');
INSERT INTO `xjm_user` VALUES ('751', 'Less', '', '0', '0.00', '0', '1547896846', '0', 'https://thirdwx.qlogo.cn/mmopen/vi_32/BqWNCTsgNZPD6hGIsEMvA3I06BE04ib6tF658dt9f86dFYMaELZnhZicjqMuiaNXMsWEqQScxKO9OSCGiakrasQEXA/132', '1.0', null, null, 'oS5GbwkigGYn0dMgKk2uH3elUb0c', '3');
INSERT INTO `xjm_user` VALUES ('752', '', '13697994616', '0', '0.00', '0', '1547942198', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('753', '不胖到130不改网名', '', '0', '0.00', '0', '1547951356', '0', 'http://thirdwx.qlogo.cn/mmopen/vi_32/TmXRGRVvfTLWX9icbiacenccuoiaDFsGoiaSPB7mPkNIaiaCINLOvWl00EqicGlm2QQBsLhMAiaxxeyQZLk6FUesOvUXg/132', '1.0', null, null, 'oS5Gbwi5BCOkrWR-2WjoluhpIZI8', '3');
INSERT INTO `xjm_user` VALUES ('754', '', '13548652553', '0', '0.00', '0', '1547988682', '0', '', '1.0', null, null, null, '0');
INSERT INTO `xjm_user` VALUES ('755', '', '18167106601', '0', '0.00', '0', '1548039263', '0', '', '1.0', null, null, null, '0');

-- ----------------------------
-- Table structure for xjm_user_address
-- ----------------------------
DROP TABLE IF EXISTS `xjm_user_address`;
CREATE TABLE `xjm_user_address` (
  `address_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `uid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `address` varchar(120) NOT NULL DEFAULT '' COMMENT '上门地址',
  `house_number` varchar(60) NOT NULL DEFAULT '' COMMENT '门牌号',
  `is_default` tinyint(1) DEFAULT '0' COMMENT '1：默认 ',
  `add_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '加入时间',
  PRIMARY KEY (`address_id`),
  KEY `user_id` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=427 DEFAULT CHARSET=utf8 COMMENT='常用地址表';

-- ----------------------------
-- Records of xjm_user_address
-- ----------------------------
INSERT INTO `xjm_user_address` VALUES ('82', '31', '湖北省武汉市武昌区武汉大学医学院职工宿舍24栋东湖路115号武汉大学医学部647', '2508', '0', '1508306075');
INSERT INTO `xjm_user_address` VALUES ('100', '31', '武汉市武昌区楚河汉街汉街', '国际总部a', '0', '1508741114');
INSERT INTO `xjm_user_address` VALUES ('103', '32', '武汉市武昌区徐东大街美林', '123', '0', '1508839514');
INSERT INTO `xjm_user_address` VALUES ('113', '32', '武汉市武昌区徐东大街美林', '3号', '0', '1508901244');
INSERT INTO `xjm_user_address` VALUES ('117', '30', '武汉市楚河汉街', '汉街国际总部', '0', '1509521776');
INSERT INTO `xjm_user_address` VALUES ('119', '36', '湖北省武汉市武昌区楚河南路靠近中北路立交桥', '14', '0', '1509535928');
INSERT INTO `xjm_user_address` VALUES ('122', '36', '湖北省武汉市武昌区楚河南路靠近中北路立交桥了解', '太寂寞', '0', '1509608718');
INSERT INTO `xjm_user_address` VALUES ('123', '30', '武汉市武昌区万达SOHO环球国际中心A座', '123', '0', '1509622813');
INSERT INTO `xjm_user_address` VALUES ('124', '30', '武汉市洪山区楚汉路楚河文化街东1区(汉街歌笛桥旁)', '123456', '0', '1509625532');
INSERT INTO `xjm_user_address` VALUES ('125', '37', '湖北省武汉市武昌区楚河南路靠近武汉杜莎夫人蜡像馆', '藏龙岛', '0', '1509671184');
INSERT INTO `xjm_user_address` VALUES ('126', '37', '湖北省武汉市武昌区楚河南路靠近武汉杜莎夫人蜡像馆', '图咯个', '0', '1509671767');
INSERT INTO `xjm_user_address` VALUES ('127', '40', '武汉市武昌区万达SOHO环球国际中心A座', '101', '0', '1509677370');
INSERT INTO `xjm_user_address` VALUES ('131', '37', '湖北省武汉市武昌区楚河南路靠近武汉杜莎夫人蜡像馆', '啊啦啦啦', '0', '1509677595');
INSERT INTO `xjm_user_address` VALUES ('132', '39', '武汉市武昌区万达SOHO环球国际中心A座', '我', '0', '1509694707');
INSERT INTO `xjm_user_address` VALUES ('133', '39', '武汉市武昌区水果湖街办事处汉街万达环球国际中心A座一楼', '我', '0', '1509695044');
INSERT INTO `xjm_user_address` VALUES ('137', '38', '湖北省武汉市江岸区汉口沿江大道182号粤汉码头(近两江游览及江滩公园)', '515', '0', '1510055809');
INSERT INTO `xjm_user_address` VALUES ('146', '38', '湖北省武汉市江岸区沿江大道188号', '112', '0', '1510056520');
INSERT INTO `xjm_user_address` VALUES ('154', '38', '湖北省武汉市江岸区沿江大道188号', '55', '0', '1510103309');
INSERT INTO `xjm_user_address` VALUES ('155', '38', '湖北省武汉市江岸区沿江大道188号', '65556', '0', '1510103485');
INSERT INTO `xjm_user_address` VALUES ('156', '38', '湖北省武汉市江岸区沿江大道188号', '554', '0', '1510103516');
INSERT INTO `xjm_user_address` VALUES ('157', '38', '湖北省武汉市江岸区沿江大道188号', '455454', '0', '1510103685');
INSERT INTO `xjm_user_address` VALUES ('158', '38', '湖北省武汉市江岸区沿江大道188号', '14', '0', '1510103737');
INSERT INTO `xjm_user_address` VALUES ('159', '38', '湖北省武汉市江岸区汉口沿江大道182号粤汉码头(近两江游览及江滩公园)', '41', '0', '1510103767');
INSERT INTO `xjm_user_address` VALUES ('160', '38', '湖北省武汉市江岸区沿江大道188号', '5251', '0', '1510104359');
INSERT INTO `xjm_user_address` VALUES ('161', '38', '湖北省武汉市江岸区汉口沿江大道182号粤汉码头(近两江游览及江滩公园)', '56455', '0', '1510104498');
INSERT INTO `xjm_user_address` VALUES ('169', '38', '湖北省武汉市洪山区沙湖大道18号', '11', '0', '1510124887');
INSERT INTO `xjm_user_address` VALUES ('170', '38', '湖北省武汉市洪山区沙湖大道18号', '11', '0', '1510124896');
INSERT INTO `xjm_user_address` VALUES ('171', '38', '湖北省武汉市洪山区沙湖大道18号', '11', '0', '1510124926');
INSERT INTO `xjm_user_address` VALUES ('172', '38', '湖北省武汉市洪山区沙湖大道18号', '11', '0', '1510124947');
INSERT INTO `xjm_user_address` VALUES ('173', '38', '湖北省武汉市洪山区沙湖大道18号', '11', '0', '1510124948');
INSERT INTO `xjm_user_address` VALUES ('174', '38', '湖北省武汉市武昌区轨道交通4号线', '1', '0', '1510124965');
INSERT INTO `xjm_user_address` VALUES ('175', '55', '湖北省武汉市江岸区汉口沿江大道182号粤汉码头(近两江游览及江滩公园)', '44', '0', '1510125020');
INSERT INTO `xjm_user_address` VALUES ('176', '55', '湖北省武汉市江岸区汉口沿江大道182号粤汉码头(近两江游览及江滩公园)', '44', '0', '1510125085');
INSERT INTO `xjm_user_address` VALUES ('177', '55', '湖北省武汉市江岸区汉口沿江大道182号粤汉码头(近两江游览及江滩公园)', '4141', '0', '1510125178');
INSERT INTO `xjm_user_address` VALUES ('178', '38', '湖北省武汉市武昌区白鹭街', '11', '0', '1510125221');
INSERT INTO `xjm_user_address` VALUES ('179', '38', '湖北省武汉市武昌区楚河汉街第一街区27号', '11', '0', '1510125305');
INSERT INTO `xjm_user_address` VALUES ('180', '38', '湖北省武汉市洪山区沙湖大道18号', '11', '0', '1510125797');
INSERT INTO `xjm_user_address` VALUES ('181', '38', '湖北省武汉市武昌区汉街总部国际(楚河汉街地铁站东北)', '11', '0', '1510126331');
INSERT INTO `xjm_user_address` VALUES ('185', '38', '武昌区水果湖隧道(武汉大学医学部体育馆)', '浏览量', '0', '1510149364');
INSERT INTO `xjm_user_address` VALUES ('186', '38', '湖北省武汉市武昌区东湖路142号', '11', '0', '1510152935');
INSERT INTO `xjm_user_address` VALUES ('187', '38', '湖北省武汉市武昌区东湖路160号', '啊啊', '0', '1510153497');
INSERT INTO `xjm_user_address` VALUES ('188', '38', '湖北省武汉市武昌区东湖生态旅游风景区湖北省老年大学', '11', '0', '1510153902');
INSERT INTO `xjm_user_address` VALUES ('192', '38', '武汉市武昌区楚河汉街1号武汉中央文化旅游区-楚河汉街一街区F1层', '一记', '0', '1510195235');
INSERT INTO `xjm_user_address` VALUES ('193', '37', '湖北省武汉市武昌区汉街总部国际(武汉杜莎夫人蜡像馆西南)', '1', '0', '1510195472');
INSERT INTO `xjm_user_address` VALUES ('194', '55', '湖北省武汉市江岸区沿江大道188号', '54465', '0', '1510197383');
INSERT INTO `xjm_user_address` VALUES ('196', '38', '湖北省武汉市洪山区汉街', '11', '0', '1510198233');
INSERT INTO `xjm_user_address` VALUES ('197', '39', '汉街总部国际A座', '7楼7号', '0', '1510210142');
INSERT INTO `xjm_user_address` VALUES ('199', '55', '湖北省武汉市江岸区沿江大道226号', '88', '0', '1510213425');
INSERT INTO `xjm_user_address` VALUES ('201', '38', '湖北省武汉市江岸区沿江大道188号', '13213', '0', '1510221652');
INSERT INTO `xjm_user_address` VALUES ('202', '38', '湖北省武汉市武昌区轨道交通4号线', '11', '0', '1510222475');
INSERT INTO `xjm_user_address` VALUES ('203', '38', '湖北省武汉市武昌区轨道交通4号线', '亲亲亲亲亲', '0', '1510222587');
INSERT INTO `xjm_user_address` VALUES ('205', '38', '湖北省武汉市武昌区汉街总部国际(楚河汉街地铁站东北)', '111', '0', '1510227322');
INSERT INTO `xjm_user_address` VALUES ('207', '55', '湖北省武汉市江岸区沿江大道226号', '54879', '0', '1510228097');
INSERT INTO `xjm_user_address` VALUES ('210', '38', '湖北省武汉市武昌区楚河南路(汉街总部国际B座正对面)', '11', '0', '1510231651');
INSERT INTO `xjm_user_address` VALUES ('211', '55', '湖北省武汉市江岸区沿江大道226号', '6656', '0', '1510276186');
INSERT INTO `xjm_user_address` VALUES ('212', '55', '湖北省武汉市江岸区汉口沿江大道182号粤汉码头(近两江游览及江滩公园)', '4545454', '0', '1510276621');
INSERT INTO `xjm_user_address` VALUES ('213', '55', '湖北省武汉市江岸区汉口沿江大道182号粤汉码头(近两江游览及江滩公园)', '122', '0', '1510277760');
INSERT INTO `xjm_user_address` VALUES ('214', '55', '湖北省武汉市江岸区汉口沿江大道182号粤汉码头(近两江游览及江滩公园)', '4554', '0', '1510277878');
INSERT INTO `xjm_user_address` VALUES ('217', '55', '湖北省武汉市江岸区汉口沿江大道182号粤汉码头(近两江游览及江滩公园)', '44', '0', '1510277943');
INSERT INTO `xjm_user_address` VALUES ('218', '55', '湖北省武汉市江岸区汉口沿江大道182号粤汉码头(近两江游览及江滩公园)', '44', '0', '1510277945');
INSERT INTO `xjm_user_address` VALUES ('219', '55', '湖北省武汉市江岸区汉口沿江大道182号粤汉码头(近两江游览及江滩公园)', '4478', '0', '1510277980');
INSERT INTO `xjm_user_address` VALUES ('220', '55', '湖北省武汉市江岸区一元路', '545', '0', '1510278027');
INSERT INTO `xjm_user_address` VALUES ('221', '55', '湖北省武汉市江岸区三阳路和沿江大道交叉口对面', '5454', '0', '1510278942');
INSERT INTO `xjm_user_address` VALUES ('222', '38', '武昌区汉街总部国际', '11', '0', '1510280103');
INSERT INTO `xjm_user_address` VALUES ('225', '38', '湖北省武汉市武昌区水果湖汉街第29号地铺及一楼', '7', '0', '1510280904');
INSERT INTO `xjm_user_address` VALUES ('226', '37', '湖北省武汉市武昌区采花毛尖(白鹭街)水果湖街办事处水果湖街东湖路9号(水果湖消防中队正对面)303', '六楼', '0', '1510285525');
INSERT INTO `xjm_user_address` VALUES ('227', '37', '湖北省武汉市洪山区洪山区机关住宅小区(东南门)徐东大街153号3123', '三号门', '0', '1510285805');
INSERT INTO `xjm_user_address` VALUES ('229', '37', '武昌区汉街总部国际', '看', '0', '1510294776');
INSERT INTO `xjm_user_address` VALUES ('230', '42', '湖北省武汉市武昌区武汉佳野投资有限公司白鹭街楚河汉街国际总部b座6046', '55', '0', '1510297059');
INSERT INTO `xjm_user_address` VALUES ('231', '38', '湖北省武汉市武昌区汉街总部国际(楚河汉街地铁站东北)', '11', '0', '1510299044');
INSERT INTO `xjm_user_address` VALUES ('232', '37', '湖北省武汉市武昌区原麦山丘(汉街店)楚河汉街第一街区29号,地铁4号线楚河汉街B出口(翠华)15', '1', '0', '1510299089');
INSERT INTO `xjm_user_address` VALUES ('233', '38', '湖北省武汉市武昌区轨道交通4号线', '11', '0', '1510304324');
INSERT INTO `xjm_user_address` VALUES ('234', '38', '湖北省武汉市洪山区中北路楚河汉街松竹路8号(近万达、地铁4号线楚河汉街站C出口)', '11', '0', '1510304637');
INSERT INTO `xjm_user_address` VALUES ('235', '63', '湖北省武汉市武昌区丁字桥路', '中南国际汇A区5楼a3-1', '0', '1510400086');
INSERT INTO `xjm_user_address` VALUES ('259', '55', '湖北省武汉市江岸区沿江大道226号', '6656', '0', '1510621942');
INSERT INTO `xjm_user_address` VALUES ('261', '55', '湖北省武汉市江岸区一元路', '545', '0', '1510647502');
INSERT INTO `xjm_user_address` VALUES ('269', '37', '武昌区汉街总部国际', '看', '0', '1510748072');
INSERT INTO `xjm_user_address` VALUES ('271', '32', '武汉市武昌区中央文化区K6、K7地块一期第K6-11层商7号', '一号', '0', '1510751063');
INSERT INTO `xjm_user_address` VALUES ('272', '32', '武汉市武昌区中北路171号', '1号', '0', '1510757379');
INSERT INTO `xjm_user_address` VALUES ('275', '32', '湖北省武汉市江岸区二曜路1号', '5564', '0', '1510814775');
INSERT INTO `xjm_user_address` VALUES ('276', '37', '万科城花璟苑', '太累了', '0', '1510826696');
INSERT INTO `xjm_user_address` VALUES ('277', '34', '洪山区徐东馨苑(杨园南路西150米)', '啊啊啊', '0', '1510837911');
INSERT INTO `xjm_user_address` VALUES ('278', '34', '洪山区徐东馨苑(杨园南路西150米)', '啊啊啊', '0', '1510886185');
INSERT INTO `xjm_user_address` VALUES ('280', '38', '湖北省武汉市洪山区中北路楚河汉街松竹路8号(近万达、地铁4号线楚河汉街站C出口)', '11', '0', '1510906116');
INSERT INTO `xjm_user_address` VALUES ('282', '38', '湖北省武汉市洪山区中北路楚河汉街松竹路8号(近万达、地铁4号线楚河汉街站C出口)', '11', '0', '1510906246');
INSERT INTO `xjm_user_address` VALUES ('287', '35', '湖北省武汉市洪山区荣院路', '3219', '0', '1510925627');
INSERT INTO `xjm_user_address` VALUES ('289', '40', '湖北省武汉市江岸区台北一路靠近江汉规划大楼', '台北一路教师公寓', '0', '1510980313');
INSERT INTO `xjm_user_address` VALUES ('290', '66', '洪山区光谷时间广场', '测试测试', '0', '1511089490');
INSERT INTO `xjm_user_address` VALUES ('294', '39', '水利厅宿舍', '1-1907', '0', '1511108931');
INSERT INTO `xjm_user_address` VALUES ('296', '38', '湖北省武汉市武昌区水果湖汉街第29号地铺及一楼', '405', '0', '1511167128');
INSERT INTO `xjm_user_address` VALUES ('297', '38', '武铁洪山住宅小区(东北门)', '27栋', '0', '1511181301');
INSERT INTO `xjm_user_address` VALUES ('298', '40', '湖北省武汉市武昌区楚河南路靠近中北路立交桥', '楚河汉街总部国际a座701', '0', '1511236371');
INSERT INTO `xjm_user_address` VALUES ('299', '38', '武汉市武昌区白鹭街86附近', '124', '0', '1511237063');
INSERT INTO `xjm_user_address` VALUES ('303', '40', '汉街总部国际A座', '701', '0', '1511319781');
INSERT INTO `xjm_user_address` VALUES ('304', '35', '楚河汉街(地铁站)', '7楼', '0', '1511320216');
INSERT INTO `xjm_user_address` VALUES ('306', '68', '屈臣氏', '吃饭的点了个电话都没有123', '0', '1511409606');
INSERT INTO `xjm_user_address` VALUES ('307', '31', '湖北省武汉市武昌区楚河南路靠近中北路立交桥', '国际总部a座', '0', '1511487741');
INSERT INTO `xjm_user_address` VALUES ('308', '40', '武汉市武昌区雄楚大街附42号(晒湖小区旁)', '1栋', '0', '1511584135');
INSERT INTO `xjm_user_address` VALUES ('309', '32', '武汉市汉南区振兴街与王家岭路交叉口西50米', '一号', '0', '1511585722');
INSERT INTO `xjm_user_address` VALUES ('311', '77', '武汉市武昌区东湖路142号', '11', '0', '1511702140');
INSERT INTO `xjm_user_address` VALUES ('312', '34', '湖北省武汉市武昌区楚河南路靠近中北路立交桥', '北', '0', '1511837089');
INSERT INTO `xjm_user_address` VALUES ('313', '77', '湖北省武汉市武昌区楚河南路靠近中北路立交桥', '基金', '0', '1511852655');
INSERT INTO `xjm_user_address` VALUES ('314', '77', '湖北省武汉市武昌区楚河南路靠近中北路立交桥', '22', '0', '1511852663');
INSERT INTO `xjm_user_address` VALUES ('315', '78', '湖北省武汉市江岸区蔡锷路和沿江大道交叉口对面汉口江滩内', '45', '0', '1511857026');
INSERT INTO `xjm_user_address` VALUES ('316', '81', '武汉市武昌区楚河南路', '22', '0', '1511857730');
INSERT INTO `xjm_user_address` VALUES ('317', '81', '武汉市武昌区楚河南路', '22', '0', '1511918623');
INSERT INTO `xjm_user_address` VALUES ('318', '73', '湖北省武汉市江岸区沿江大道188号', '111', '0', '1511923497');
INSERT INTO `xjm_user_address` VALUES ('319', '81', '湖北省武汉市武昌区楚河南路靠近杜莎夫人蜡像馆(汉街)', '333', '0', '1511942934');
INSERT INTO `xjm_user_address` VALUES ('320', '81', '湖北省武汉市武昌区楚河南路靠近杜莎夫人蜡像馆(汉街)', '336', '0', '1511942944');
INSERT INTO `xjm_user_address` VALUES ('323', '81', '湖北省武汉市武昌区楚河南路靠近杜莎夫人蜡像馆(汉街)', '3366666', '0', '1511943520');
INSERT INTO `xjm_user_address` VALUES ('327', '81', '向阳佳苑A区', '5栋', '0', '1511949480');
INSERT INTO `xjm_user_address` VALUES ('328', '81', '湖北省武汉市武昌区楚河南路靠近中北路立交桥', '北', '0', '1511949646');
INSERT INTO `xjm_user_address` VALUES ('329', '32', '武汉市武昌区楚河南路', '楚河汉街站', '0', '1511950362');
INSERT INTO `xjm_user_address` VALUES ('330', '89', '汉街总部国际A座', '111', '0', '1512037228');
INSERT INTO `xjm_user_address` VALUES ('331', '32', '武汉市武昌区楚河南路', '一号', '0', '1512038094');
INSERT INTO `xjm_user_address` VALUES ('332', '32', '武汉市武昌区中北路163号', '楚河汉街', '0', '1512091792');
INSERT INTO `xjm_user_address` VALUES ('334', '31', '武汉市武昌区楚河南路', '二号', '0', '1512119807');
INSERT INTO `xjm_user_address` VALUES ('335', '31', '武汉市武昌区楚河南路', '三号', '0', '1512119897');
INSERT INTO `xjm_user_address` VALUES ('336', '105', '武汉市武昌区楚河南路', '一号', '0', '1512377911');
INSERT INTO `xjm_user_address` VALUES ('337', '105', '武汉市武昌区楚河南路', '汉街国际总部a座701', '0', '1512378621');
INSERT INTO `xjm_user_address` VALUES ('338', '105', '武汉市武昌区楚河南路', '楚河汉街汉街总部a座', '0', '1512378988');
INSERT INTO `xjm_user_address` VALUES ('342', '89', '天安门西(公交站)', '南广场', '0', '1512441870');
INSERT INTO `xjm_user_address` VALUES ('343', '89', '湖北省武汉市武昌区楚河南路靠近中北路立交桥', '1141', '0', '1512441894');
INSERT INTO `xjm_user_address` VALUES ('345', '108', '武汉市武昌区松竹路28号', '五号', '0', '1512463974');
INSERT INTO `xjm_user_address` VALUES ('346', '36', '武汉市武昌区中央文化区K6、K7地块一期第K6-11层商7号', '一号', '0', '1512464352');
INSERT INTO `xjm_user_address` VALUES ('347', '112', '湖北省武汉市武昌区楚河南路靠近中北路立交桥', '肯德基', '0', '1512554108');
INSERT INTO `xjm_user_address` VALUES ('348', '113', '武汉市武昌区汉街76号', '一号', '0', '1512554629');
INSERT INTO `xjm_user_address` VALUES ('349', '115', '南京市玄武区北京东路4号', '南京广播电视台', '0', '1512563460');
INSERT INTO `xjm_user_address` VALUES ('350', '96', '武汉市江汉区金家墩特1号', '一号', '0', '1512625185');
INSERT INTO `xjm_user_address` VALUES ('351', '96', '武汉市武昌区汉街84号', '一号', '0', '1512626670');
INSERT INTO `xjm_user_address` VALUES ('352', '46', '湖北省武汉市武昌区楚河南路靠近中北路立交桥', '宿舍', '0', '1512631134');
INSERT INTO `xjm_user_address` VALUES ('353', '89', '湖北省武汉市武昌区楚河南路靠近中北路立交桥', '1141', '0', '1512743497');
INSERT INTO `xjm_user_address` VALUES ('354', '127', '武汉市武昌区楚河汉街1号武汉中央文化旅游区-楚河汉街一街区F1层', '哟哟', '0', '1512871998');
INSERT INTO `xjm_user_address` VALUES ('355', '34', '洪山区徐东馨苑(杨园南路西150米)', '啊啊啊', '0', '1512873521');
INSERT INTO `xjm_user_address` VALUES ('356', '46', '湖北省武汉市武昌区丁字桥路靠近中国邮政储蓄银行(梅苑营业所)', 'A204', '0', '1513177770');
INSERT INTO `xjm_user_address` VALUES ('358', '96', '武汉市武昌区中北路126号', '一号', '0', '1513259801');
INSERT INTO `xjm_user_address` VALUES ('359', '96', '武汉市洪山区武珞路790号', '一号', '0', '1513349045');
INSERT INTO `xjm_user_address` VALUES ('360', '140', '湖北省武汉市洪山区文化路靠近韵湖春晓', '韵湖春晓6栋2单元2201室', '0', '1513429764');
INSERT INTO `xjm_user_address` VALUES ('361', '141', '湖北省武汉市蔡甸区东风大道靠近东风公司(地铁站)', '58号', '0', '1513436255');
INSERT INTO `xjm_user_address` VALUES ('362', '141', '湖北省武汉市蔡甸区东风大道靠近东风公司(地铁站)', '58', '0', '1513436297');
INSERT INTO `xjm_user_address` VALUES ('363', '149', '武汉市武昌区小东门民主路739号', '29-1-201', '0', '1513763161');
INSERT INTO `xjm_user_address` VALUES ('364', '156', '欣兰服装修改', '金地花园212', '0', '1514204937');
INSERT INTO `xjm_user_address` VALUES ('365', '156', '河北省保定市定州市兴华东路靠近鼓典架子鼓艺术中心', '金地花园212', '0', '1514204983');
INSERT INTO `xjm_user_address` VALUES ('366', '175', '重庆市綦江区东城大道靠近国能天街', '312', '0', '1514428702');
INSERT INTO `xjm_user_address` VALUES ('367', '181', '吉楚快捷酒店', '403', '0', '1514615125');
INSERT INTO `xjm_user_address` VALUES ('368', '182', '海南省三亚市天涯区铁路通道靠近三亚市第四小学', '紫椰林主题酒店302', '0', '1514646704');
INSERT INTO `xjm_user_address` VALUES ('369', '188', '武汉市', '804室', '0', '1514943794');
INSERT INTO `xjm_user_address` VALUES ('370', '196', '河北省沧州市运河区开元北大道靠近世纪家园', '世纪家园北区14栋1单元1303室', '0', '1515263825');
INSERT INTO `xjm_user_address` VALUES ('371', '198', '当前位置', '丹阳市维多利海休闲会所8012', '0', '1515566864');
INSERT INTO `xjm_user_address` VALUES ('372', '204', '吉林省长春市南关区久长路靠近贝迪堡长春国际早教中心', '11111', '0', '1516201450');
INSERT INTO `xjm_user_address` VALUES ('373', '223', '山西省太原市小店区南中环街辅路靠近晋城银行(太原南中环街支行)', '山西省太原市解放路永济小区1单元2107', '0', '1517668096');
INSERT INTO `xjm_user_address` VALUES ('374', '230', '上海市长宁区天山路靠近招商银行(上海天山支行)', '天山路760弄4号405', '0', '1518849745');
INSERT INTO `xjm_user_address` VALUES ('375', '246', '湖北省武汉市洪山区珞喻路靠近平安银行(光谷支行)', '701', '0', '1519737123');
INSERT INTO `xjm_user_address` VALUES ('376', '246', '湖北省武汉市洪山区珞喻路靠近平安银行(光谷支行)', '教室小区701', '0', '1519737249');
INSERT INTO `xjm_user_address` VALUES ('377', '252', '宁夏回族自治区中卫市沙坡头区平安西路靠近沙坡头水镇', '锦江之星', '0', '1520346614');
INSERT INTO `xjm_user_address` VALUES ('378', '107', '湖北省武汉市武昌区楚河南路靠近杜莎夫人蜡像馆(汉街)', '7楼', '0', '1521445934');
INSERT INTO `xjm_user_address` VALUES ('379', '278', '城市便捷酒店(广州天平架地铁站店)', '3055', '0', '1522249135');
INSERT INTO `xjm_user_address` VALUES ('380', '284', '广西壮族自治区贺州市钟山县西乐街南路靠近中国人民银行(钟山支行)', '左边第一栋2楼', '0', '1522834853');
INSERT INTO `xjm_user_address` VALUES ('381', '285', '陕西省西安市未央区太华北路靠近川渝人家', '团结村现代商务酒店233房间', '0', '1522864693');
INSERT INTO `xjm_user_address` VALUES ('382', '287', '广东省深圳市南山区塘益路靠近塘朗商务公寓写字楼A座', '塘朗新村82栋', '0', '1522976029');
INSERT INTO `xjm_user_address` VALUES ('383', '291', '湖北省武汉市黄陂区汉施路靠近中国建设银行(武汉武湖支行)', '东方城一期7栋2902', '0', '1523596810');
INSERT INTO `xjm_user_address` VALUES ('384', '310', '山东省济南市历下区和平路靠近泉城新时代商业广场', '901', '0', '1525099525');
INSERT INTO `xjm_user_address` VALUES ('385', '319', '吉林省四平市双辽市119乡道靠近中共双辽种羊场纪律检查委员会', '明珠酒店402', '0', '1525707589');
INSERT INTO `xjm_user_address` VALUES ('387', '291', '湖北省武汉市江岸区解放大道靠近武汉农村商业银行(丹水池支行)', '发发商务宾馆', '0', '1526056200');
INSERT INTO `xjm_user_address` VALUES ('388', '291', '湖北省武汉市江岸区解放大道靠近武汉农村商业银行(丹水池支行)', '友发商务宾馆四楼8427', '0', '1526056291');
INSERT INTO `xjm_user_address` VALUES ('389', '89', '湖北省武汉市武昌区松竹路靠近杜莎夫人蜡像馆(汉街)', '11', '0', '1526262166');
INSERT INTO `xjm_user_address` VALUES ('390', '330', '昆仑大厦(东南门)', '3号楼201室', '0', '1526597075');
INSERT INTO `xjm_user_address` VALUES ('391', '89', '湖北省武汉市武昌区松竹路靠近杜莎夫人蜡像馆(汉街)', '11', '0', '1528769998');
INSERT INTO `xjm_user_address` VALUES ('392', '357', '江苏省淮安市淮阴区西安北路靠近金泰家具城', '淮阴区黄河西路泰和家园，金泰公馆1505户', '0', '1529342546');
INSERT INTO `xjm_user_address` VALUES ('393', '373', 'AM.PM影咖主题宾馆', '天津市大港区学府路影咖主题宾馆二楼8205', '0', '1529911146');
INSERT INTO `xjm_user_address` VALUES ('394', '378', '四川省成都市青羊区长顺上街靠近中国建设银行(成都新华支行)', '金河宾馆', '0', '1530200132');
INSERT INTO `xjm_user_address` VALUES ('395', '385', '广东省深圳市龙华区龙观东路靠近龙华汽车站广场', '维也纳', '0', '1530917996');
INSERT INTO `xjm_user_address` VALUES ('396', '435', '广东省深圳市龙岗区发展路靠近坪地十九小区', '26号', '0', '1533901820');
INSERT INTO `xjm_user_address` VALUES ('397', '439', '江苏省无锡市新吴区南丰路靠近未来星艺术培训中心(南丰路)', '无锡市新吴区叙丰家园148号201', '0', '1533986440');
INSERT INTO `xjm_user_address` VALUES ('398', '455', '河南省周口市郸城县育新路靠近美景河畔', '17号楼一单元三楼302', '0', '1537678817');
INSERT INTO `xjm_user_address` VALUES ('399', '455', '河南省周口市郸城县育新路靠近美景河畔', '17号楼三楼302', '0', '1537678895');
INSERT INTO `xjm_user_address` VALUES ('400', '537', '郑州市惠济区北三环与秦岭路交汇处托斯卡纳小区北门', '托斯卡纳25号楼1706', '0', '1538090849');
INSERT INTO `xjm_user_address` VALUES ('401', '556', '石码头既济箱包城春江园', '五号楼1310', '0', '1538745044');
INSERT INTO `xjm_user_address` VALUES ('402', '556', '暨济箱包城', '五号楼1310', '0', '1538745176');
INSERT INTO `xjm_user_address` VALUES ('403', '576', '草埔(地铁站)', '比毕丽山庄10栋302', '0', '1538920173');
INSERT INTO `xjm_user_address` VALUES ('404', '616', '武汉市武昌区八一路92号', '帅府饭店', '0', '1543836713');
INSERT INTO `xjm_user_address` VALUES ('405', '618', '吉林省延边朝鲜族自治州安图县202省道靠近中国邮政储蓄银行(三道营业所)', '三道', '0', '1543846848');
INSERT INTO `xjm_user_address` VALUES ('406', '630', '湖北省武汉市硚口区南泥湾大道靠近正康双语幼儿园(南泥湾大道)', '302', '0', '1544092614');
INSERT INTO `xjm_user_address` VALUES ('407', '656', '湖北省武汉市洪山区水文路靠近武汉湖滨花园福客来酒店(原湖滨花园酒店)', '水神客舍', '0', '1544709184');
INSERT INTO `xjm_user_address` VALUES ('408', '658', '武汉市武昌区松竹路', '3', '0', '1545016069');
INSERT INTO `xjm_user_address` VALUES ('409', '658', '武汉市武昌区松竹路', 'r', '0', '1545016144');
INSERT INTO `xjm_user_address` VALUES ('410', '678', '山东省济南市长清区紫薇路靠近范景家苑', '2号楼1单元', '0', '1546171867');
INSERT INTO `xjm_user_address` VALUES ('411', '698', '武汉市武昌区松竹路', '汉街A座', '0', '1546507918');
INSERT INTO `xjm_user_address` VALUES ('412', '700', '湖北省武汉市武昌区白鹭街86附近', 'A座7楼', '0', '1546565511');
INSERT INTO `xjm_user_address` VALUES ('413', '698', '武汉市武昌区南湖路与白沙洲大道高架交叉口东南200米', '7楼', '0', '1546570012');
INSERT INTO `xjm_user_address` VALUES ('414', '711', '武汉市武昌区万达SOHO环球国际中心1号楼2613', '1111', '0', '1546852393');
INSERT INTO `xjm_user_address` VALUES ('415', '658', '武汉市武昌区松竹路', 'gdhdh', '0', '1546912852');
INSERT INTO `xjm_user_address` VALUES ('416', '698', '武汉市武昌区楚河汉街万达中心1号楼(万达广场2号门对面)', '哈哈哈', '0', '1547428494');
INSERT INTO `xjm_user_address` VALUES ('417', '727', '武汉市武昌区楚河汉街万达中心1号楼(万达广场2号门对面)', '好', '0', '1547434116');
INSERT INTO `xjm_user_address` VALUES ('418', '46', '武汉市武昌区市武昌区棚栏口16-1号武太闸市场口', '1234', '0', '1547437204');
INSERT INTO `xjm_user_address` VALUES ('420', '46', '武汉市武昌区白沙洲大道高架与白沙洲大道高架出口交叉口东南150米', '1234', '0', '1547536639');
INSERT INTO `xjm_user_address` VALUES ('421', '658', '武汉市汉阳区平田南村69号', '3#1302', '0', '1547606911');
INSERT INTO `xjm_user_address` VALUES ('422', '727', '武汉市江夏区高新二路湖北第二师范学院南大门对面', 'qwe', '0', '1547901077');
INSERT INTO `xjm_user_address` VALUES ('423', '754', '湖北省武汉市洪山区华师园路靠近宇辰大厦', '纽宾凯怡国际酒店（1105房间）', '0', '1547988852');
INSERT INTO `xjm_user_address` VALUES ('425', '730', '湖北省武汉市武昌区松竹路靠近中北路立交桥', '3106', '0', '1548033920');
INSERT INTO `xjm_user_address` VALUES ('426', '730', '湖北省武汉市武昌区松竹路靠近华夏银行(武昌支行)', '嘿嘿嘿', '0', '1548036453');

-- ----------------------------
-- Table structure for xjm_user_comment
-- ----------------------------
DROP TABLE IF EXISTS `xjm_user_comment`;
CREATE TABLE `xjm_user_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT '0' COMMENT '用户id',
  `tid` int(11) NOT NULL DEFAULT '0' COMMENT '技师id',
  `goods_name` varchar(100) DEFAULT '' COMMENT '服务项目名称',
  `nick_name` varchar(20) DEFAULT '' COMMENT '昵称',
  `headurl` varchar(255) DEFAULT '' COMMENT '用户头像',
  `mobile` varchar(20) DEFAULT '' COMMENT '手机号',
  `type` tinyint(1) DEFAULT '0' COMMENT '0：好评 1：差评',
  `anonymity` tinyint(1) unsigned DEFAULT '0' COMMENT '0：实名   1匿名',
  `content` mediumtext COMMENT '评论内容',
  `comment_img` text,
  `add_time` int(10) DEFAULT '0' COMMENT '评论时间',
  `comment_tag` varchar(100) DEFAULT NULL COMMENT '评价标语  可多选每个标语用英文逗号隔开',
  `order_id` varchar(50) DEFAULT NULL,
  `oid` int(20) DEFAULT NULL COMMENT '订单表主键id',
  PRIMARY KEY (`id`),
  KEY `tid` (`tid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8 COMMENT='用户评论表';

-- ----------------------------
-- Records of xjm_user_comment
-- ----------------------------
INSERT INTO `xjm_user_comment` VALUES ('1', '1', '1', '足疗', '大白菜', 'http://pic.xijiaomo.com/20171123171332.', '', '0', '0', '一直觉得酸疼或者疼痛，还是需要按摩，还可以起到一个放松的作用。', 'http://pic.xijiaomo.com/android_xjm_userphoto60120181202124819;http://pic.xijiaomo.com/android_xjm_userphoto3720171102193758;http://thirdwx.qlogo.cn/mmopen/vi_32/bmLc7cPQDH2Hbd9IeXmiaapRRCFh9Re7B4xfOSGYADO6hLtokqqYqvGoGGiadfthwE0Sx0WjVd9OMk3chCR3CrMQ/132', '1546746513', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('2', '1', '1', '足疗', '这个产品不太冷', 'http://pic.xijiaomo.com/android_xjm_userphoto3720171102193758', '', '0', '0', '发烧感冒上火，听说按摩可以去火，就试了试 还不错，', null, '1540090000', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('3', '1', '1', '足疗', '火箭一号', 'http://pic.xijiaomo.com/android_xjm_userphoto3120171205162810', '', '0', '0', '服务态度很好，没的说，按的到位，穴位很准确，聊天也很聊得开，很漂亮的一个小女孩。', null, '1544444444', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('4', '1', '1', '足疗', 'Demon', 'http://pic.xijiaomo.com/android_xjm_userphoto3520171115105520', '', '0', '0', '刚刚工作完，浑身难受，也不想出门，就叫了个技师体验一下这个APP的效率，没想到还挺不错，上门也挺快，手法也很赞。', null, '1546666666', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('5', '1', '1', '足疗', '就这样吧', 'http://pic.xijiaomo.com/android_xjm_userphoto21220180119114256', '', '0', '0', '好久没按摩了，特别棒的一次体验，感觉超好。', null, '1537894156', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('6', '1', '1', '足疗', '中毒太深99', 'http://tva2.sinaimg.cn/crop.0.0.1080.1080.180/006cv6QPjw8ex0w230myhj30u00u0q66.jpg', '', '0', '0', '年岁大了，不想动，用软件叫叫人挺好的。', null, '1543333333', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('7', '1', '1', '足疗', '我造2010', 'http://tvax2.sinaimg.cn/crop.0.0.480.480.180/006KDHcTly8fi5hnf5z6pj30dc0dc74z.jpg', '', '0', '0', '小姐姐手法很专业，给我讲了很多养生方面的知识，让我如何正确养生。', null, '1545555555', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('8', '1', '1', '足疗', 'Mark先森', 'http://pic.xijiaomo.com/android_xjm_userphoto13420171214154158', '', '0', '0', '在家里就能养生！！！', null, '1540000000', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('9', '1', '1', '足疗', '我有一个小小的目标。', 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLkM8K2NHX5icS4WrfuOkU9fM17nXeVFhnwlFZZaQhiaeC9cMicuicTzJoMBe7icqDasiaia8poUD3jIb7ibQ/0', '', '0', '0', '心血来潮想按摩，试了一下这个软件，还成。', null, '1531860035', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('10', '1', '1', '足疗', '用户5974880107', 'http://pic.xijiaomo.com/android_xjm_userphoto60120181202124819', '', '0', '0', '很有特色的一个软件', null, '1531867981', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('11', '1', '2', '足疗', '这个产品不太冷', 'http://pic.xijiaomo.com/android_xjm_userphoto3720171102193758', '', '0', '0', '技师很不错~好评~', null, '1546666666', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('12', '1', '2', '足疗', '下里巴人1993', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJLKJk6OZ0FdxgJq9XficW741OQAY1BykjZEbqN19lLYiahvkvjUGCsWtiaLLZLxicxicD5dNNas0QWRUw/132', '', '0', '0', '技师会根据每个人的情况来分配不同的地方的按摩时间，按完觉得自己浑身轻松，大概是不常养生的原因吧。哈哈哈哈。', null, '1531864567', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('13', '1', '2', '足疗', '微笑天使001', 'http://thirdwx.qlogo.cn/mmopen/vi_32/bmLc7cPQDH2Hbd9IeXmiaapRRCFh9Re7B4xfOSGYADO6hLtokqqYqvGoGGiadfthwE0Sx0WjVd9OMk3chCR3CrMQ/132', '', '0', '0', '服务到位~手法娴熟', null, '1543333333', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('14', '1', '2', '足疗', '醉意晨', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLCD5IurQjk2pmJMEzUADexvBDwy3T5FmxHwXtH4UxtDUSUm1rCo5nfuQbwJq6ZoqwJUSajpJWXfw/132', '', '0', '0', '挺不错的，物超所值，值得推荐', null, '1540000000', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('15', '1', '2', '足疗', '温柔大哥2018', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTI44qtT9fl5kssxw2KmB0R6VyicDSRZyBHarXeibtvFeAEJ01ljTJStIw4yOcXKrFUWuKfHDKsCNC3g/132', '', '0', '0', '按摩师非常敬业，看的出来以前培训的很认真。', null, '1543333333', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('16', '1', '2', '足疗', '奇了个怪的二毛', 'http://thirdwx.qlogo.cn/mmopen/vi_32/ergcvdrviabUImht04bYCJkS64icB7Zkd7LjHWAEdIajZIdLTSiaAwc85mcIkFUY6dycYwZqibsPK8aRvkjeIF7m2Q/132', '', '0', '0', '耳目一新的APP 。', null, '1531869871', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('17', '1', '2', '足疗', 'dream六味', 'http://thirdqq.qlogo.cn/qqapp/1106555152/295C38AA8FBB385C7EE954372ABCEF0B/100', '', '0', '0', '给个好评，推荐。', null, '1540000000', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('18', '1', '2', '足疗', '皮皮辉', 'http://thirdqq.qlogo.cn/qqapp/1106555152/AB439BED708E856E7174430B02520504/100', '', '0', '0', '在家里就能享受~', null, '1531860786', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('19', '1', '2', '足疗', '一番一番一番', 'http://thirdqq.qlogo.cn/qqapp/1106555152/4A81C425D85F3B791927F8B213891FBA/100', '', '0', '0', '足疗按摩是中医学的重要组成部分，距今两千多年前的经典医注《黄帝内经》中就详细的介绍了全身的经络和穴位。', null, '1542222222', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('20', '1', '2', '足疗', 'lemon-', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eo50M6vKeefpVuRdnAiaYFxOlCf4LqnhdGGMbq302nWic4EmotyoiaCP4YQgHKrzGicytmgqaxiadibXIQg/132', '', '0', '0', '这里的技师手法很不错，关键的是服务特别到位，每一个细节都为客人着想。', null, '1540000000', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('21', '1', '3', '足疗', '犹豫就是罪', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJuerIm7hcKHgKJmibyiauziaZKaTsVauh7qLia7nj2xuJ9tk9dhMfVib6PpeTUL9bV70Ik2ES5A65BC7w/132', '', '0', '0', '总体来说还不错，按摩的技师非常好，手法到位，人很热情，最主要的是划算。', null, '1531999999', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('22', '1', '3', '足疗', '文字间', 'https://thirdwx.qlogo.cn/mmopen/vi_32/ENj0lvlcHT1ObheM0erDyRiaorfcibhgicNkibZMZQ2yk1FIBMH4lKBiaRibiaIHQ39O5GtP940zZDibFqyicicUM6WG0tZw/132', '', '0', '0', '使用这个APP后 生活质量都高了，在家享受高品质生活。', null, '1531860035', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('23', '1', '3', '足疗', '四月八', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJLKJk6OZ0FdxgJq9XficW741OQAY1BykjZEbqN19lLYiahvkvjUGCsWtiaLLZLxicxicD5dNNas0QWRUw/132', '', '0', '0', '技术力量真是难得的良心品质。', null, '1540000000', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('24', '1', '3', '足疗', '追着风筝的人~', 'http://thirdqq.qlogo.cn/qqapp/1106555152/18B7401C521A7AA9E117E839C5E916F9/100', '', '0', '0', '按摩很有力度，挺舒服的。', null, '1544444444', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('25', '1', '3', '足疗', '可爱的喵呜', 'http://thirdqq.qlogo.cn/qqapp/1106555152/4BD213D65CF6EA6580FFC8F9275FC948/100', '', '0', '0', '手法娴熟，力量适中，很适合现在的生活潮流', null, '1531866666', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('26', '1', '3', '足疗', '飞行员舒克的贝塔', 'http://thirdqq.qlogo.cn/qqapp/1106555152/7D24110B547DF2C7BD15B7A16FFE86B8/100', '', '0', '0', '好评就完事了，', null, '1531860035', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('27', '1', '3', '足疗', '此生相忘于奈何桥', 'http://thirdwx.qlogo.cn/mmopen/vi_32/AXLO2MHYamhQ2SiaNtNnkOuD7LqKDMRN6IVAJ1uIm2h7UZ03vj1m7jpwE3vf7gXQKibVttoXcwSnicvCNpEzRNZIg/132', '', '0', '0', '为民的软件~', null, '1540000000', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('28', '1', '3', '足疗', '天命草', 'http://thirdqq.qlogo.cn/qqapp/1106555152/7C646CF9E26C4B3870828429D59306CA/100', '', '0', '0', '一款很nice的app,一款良心的app', null, '1531877777', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('29', '1', '3', '足疗', '美好时代', 'https://thirdwx.qlogo.cn/mmopen/vi_32/UOjSnjVELCkxZib7hibfw1IhRZTLqjhpT90zQpgxKWdKJpR1icTygeHyB27n1o3VZcw6YJtsZdj6rIwOcXqBxc2uA/132', '', '0', '0', '在家里按摩 感觉自己美美哒。', null, '1544444444', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('30', '1', '3', '足疗', 'viceol', 'http://thirdwx.qlogo.cn/mmopen/vi_32/9DBUNd9N1QPsP1GIOyg26nB9eiaXdaECsqqiclJHv0sEVaecKFliaqsHQkfPA3XL3m63cWJcFeibhosPicS7bB7GZibQ/132', '', '0', '0', '嘻嘻嘻，舒服了。', null, '1531877777', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('31', '1', '26', '足疗', '约旦河禽兽', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLQpvhDSVINF0v4oDUgfEKHPSPoXL7juCLh6mgnicmUAQEB4lRn2iaVTrDtGzLlC2a8R85yMbu0HQzA/132', '', '0', '0', '牛逼克拉斯。', null, '1540000000', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('32', '1', '26', '足疗', '做回真我突破自己', 'http://thirdqq.qlogo.cn/qqapp/1106555152/7B5C52A6F164D1B2C304D0218BDBAB8E/100', '', '0', '0', '经济又实惠，舒服又不贵', null, '1546546513', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('33', '1', '26', '足疗', '小猪02612', 'http://thirdwx.qlogo.cn/mmopen/vi_32/948yJZy09pKicddOjoh7knp00ibQTvHftRFkSwxen9m4Wibic0ibMAiaeKMicEoquicFt1ZLvgicqu1xYXBQ49CsU5pdBxw/132', '', '0', '0', '这款APP 拥有新奇的想法，给上班劳累的人一个令人很舒畅的体验。', null, '1544444444', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('34', '1', '26', '足疗', '悲伤的流氓', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoV4N4riaSGMwVdY0y8Yk8Y7lKpHDZyUMBD9GTaUYiamEn2B8OFL866yicnzSsA2Sk8mop05DVXaEM8w/132', '', '0', '0', '技师服务态度良好，技能娴熟，是一款很良心的app', null, '1546546513', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('35', '1', '26', '足疗', '今天也要想想你', 'https://thirdwx.qlogo.cn/mmopen/vi_32/EfQGsAWQJhn4EAFoBicCem9P2pxYncsoTfH8gZTr6yX19mwJpfBevp76wia2czjDnD59PCPjIDEkOmApo8lQywkw/132', '', '0', '0', '养生的一款很赞的软件', null, '1546546513', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('36', '1', '26', '足疗', '梦不出梦到的样子', 'http://thirdqq.qlogo.cn/qqapp/1106555152/917433B18483D1156819912B24FCC9EA/100', '', '0', '0', '技师挺认真的，按摩的时候也很细致，让人很舒服。', null, '1531860035', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('37', '1', '26', '足疗', '月色血色和绝色', 'http://thirdwx.qlogo.cn/mmopen/vi_32/3C3dticRV4ApLibwib28w1Z1GGlW8wDRIfWGeHxibmnaKbNsudutJ5Zp2N8dgqeTpXa8h4ibkHiav5EacMDH1fm5ZInQ/132', '', '0', '0', '想睡觉哈哈哈、', null, '1540000000', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('38', '1', '26', '足疗', '微笑军字', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIh35CvxVmuqZzvBylPPfWReQUyXUia971uDXFXibOJNNJiaXbFNp5xt3l98FViaWkceVbpAiavWZmMjRg/132', '', '0', '0', '老爷子常用的软件，我也跟着点了几次，很不错推荐！', null, '1531867891', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('39', '1', '26', '足疗', '全力以赴的鱼', 'https://thirdwx.qlogo.cn/mmopen/vi_32/oHWfrFczyrLSK3ujvYWcicqDfdAYzQ3PLA5jxC3YeKic9kicjopf78RJzMXf739TCSOZI6CpsUfP1ahkJuwwf0fLA/132', '', '0', '0', '收费合理，效果见效非常快，人也不错，', null, '1531864546', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('40', '1', '26', '足疗', '嘿我是乱七八糟', 'http://thirdwx.qlogo.cn/mmopen/vi_32/NTeSMsntYPL6OecBBUs9STVgDMtaZCjich8d8owmb5JYicNQGgI716kVfd55Gq5KNOBtDrvayfvzjsia5AGDDiaM8A/132', '', '0', '0', '下次要是继续要叫舒服的话，我还要叫他！！', null, '1544444444', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('41', '1', '34', '足疗', '梦时曾见1998', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83erFicVQYVlT9K7SqWylibtuicK7myYfrOpia25aQACDEf2mCSy22OabJDXyGkiae3YycXzPCjB39cC4kRQ/132', '', '0', '0', '好不错吧，基本的知识和技能都会，按摩也很不错', null, '1531860035', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('42', '1', '34', '足疗', '来日方长111', 'http://thirdqq.qlogo.cn/qqapp/1106555152/DE65572E26CE39A748C5D619687ECFAD/100', '', '0', '0', '这个技师很棒', null, '1531860035', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('43', '1', '34', '足疗', '路边的笑声', 'http://pic.xijiaomo.com/android_xjm_userphoto58920181126232730', '', '0', '0', '理疗师很健谈，效果不错，下次还用，', null, '1531877777', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('44', '1', '34', '足疗', '最远的距离', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIwkDPS058GHYsLJAghKVaMSOcIiavrUiaj3ib3tZTLoXXoMD6UKWu3ly6wibKNgkiaOvbDuiamOlJQM9pw/132', '', '0', '0', '经济又实惠，舒服又不贵', null, '1544444444', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('45', '1', '34', '足疗', '飞雪品人生', 'http://thirdwx.qlogo.cn/mmopen/vi_32/eY14AWYFwYAdFuZibxiaBFzniaB7OmrCficgicza0wbSwYVwybtibSVHJEm54PdfoK6pNqf0ljddYvPQlKA5CloCT4Gg/132', '', '0', '0', '技术真的棒棒哒~~~~', null, '1531877777', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('46', '1', '34', '足疗', '进军司仪', 'http://pic.xijiaomo.com/android_xjm_userphoto60120181202124819', '', '0', '0', '技师挺好的，全程询问力度是否合适，且在整个过程中，服务也很好。', null, '1531899999', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('47', '1', '34', '足疗', '我有个大胆的想法', 'https://thirdwx.qlogo.cn/mmopen/vi_32/uGOeNuw7ln4BEhraKFAojzUg5gysbmXhfNcVVZusu8DAg3S85alBp7muN2Q2zol4ibXJaNtwrB3kCoPHx6LN2icg/132', '', '0', '0', '一直听朋友说做个养生按摩挺不错，决定试试看，没想到想过不错。', null, '1546666666', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('48', '1', '34', '足疗', '不思念不留恋', 'https://thirdwx.qlogo.cn/mmopen/vi_32/uGOeNuw7ln4BEhraKFAojzUg5gysbmXhfNcVVZusu8DAg3S85alBp7muN2Q2zol4ibXJaNtwrB3kCoPHx6LN2icg/132', '', '0', '0', '技师人很热情，技术很好，总体来说，很舒服。', null, '1531877777', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('49', '1', '34', '足疗', '吃鸡蛋', 'https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqZQtVPPaqFOJe9jRmU7MibjTODyFF4ibaO0wdN03Ju1eH2uGBEdbCFGeicsvfvr16Y0VhNAMzCZIZoA/132', '', '0', '0', '66666666666666666666', null, '1531866666', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('50', '1', '34', '足疗', '大人来了', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJRk5lBexKhEAElv3LLpiaOfX0wLN1T3goic88l07QzpIkcRUGMQO7Bg212Z05y81wBud8BmMIMqDBg/132', '', '0', '0', '下次要是继续要叫舒服的话，我还要叫他！！', null, '1544444444', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('51', '1', '37', '足疗', '似若尘埃ゝ', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTL0TUd3c35KY43hKhRqx3Nr7DS9KsEUJtXhBepBe802N6FW8fMTTWmUc99JZctEpFhHy2oegrcIjQ/132', '', '0', '0', '上门时间很准时，技师手法也很好', null, '1531877777', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('52', '1', '37', '足疗', '消失在黎明√', 'http://q.qlogo.cn/qqapp/1106555152/DD7B96B8AEBF408CE6E22707C15485F6/100', '', '0', '0', '经济又实惠，舒服又不贵', null, '1531999999', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('53', '1', '37', '足疗', '鱼的第8秒重生▓', 'http://q.qlogo.cn/qqapp/1106555152/0CB4D08651F7D7369772B938EEC5EEEE/100', '', '0', '0', '性价比很高，这样的价能买到这质量非常不错', null, '1511111111', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('54', '1', '37', '足疗', '幼稚百遍，成熟自现', 'https://thirdwx.qlogo.cn/mmopen/vi_32/rHQLqLYNRXuTibCk4iaFe4LWUBoYGwYxzm7uAw9AGicAZltsP6Bsh4icl8Kx2icD0BzKzSAKZMv2gvhFMocW5jwAeag/132', '', '0', '0', '哈哈，很喜欢', null, '1531866666', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('55', '1', '37', '足疗', '\r\n烏雲後面有陽光', 'http://q.qlogo.cn/qqapp/1106555152/6600CE9E317A6EC1B6AC794474FF1B78/100', '', '0', '0', '牛逼不解释', null, '1531999999', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('56', '1', '37', '足疗', '枫叶', 'http://q.qlogo.cn/qqapp/1106555152/9DF7B4C150936D70E31B0011B8136BFF/100', '', '0', '0', '很好GOOD', null, '1531867891', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('57', '1', '37', '足疗', '向谁诉说曾经', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoiakMbkJ0RdtLnmjx8qu2BbpqLsWoP1yWMGrthTPK85SnGJYxiamqA4hg4icKt7JX7v8WP54zDMdCFg/132', '', '0', '0', '很棒，点个赞', null, '1543333333', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('58', '1', '37', '足疗', '十指紧扣', 'http://thirdwx.qlogo.cn/mmopen/vi_32/vVPpfFH7OnX7GGDtEMpAZqukNQksSxu8xibibSyl1ibxFicq4Uj8BhuoR8dNyAAutBKhExZ0Sva7lKJkwOassrBLSQ/132', '', '0', '0', '良心APP！！！！！！！！！！', null, '1531860035', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('59', '1', '37', '足疗', '爺、傷風敗俗', 'http://q.qlogo.cn/qqapp/1106555152/6DC97E74A7D8159FE90C24AFC3B4EFF4/100', '', '0', '0', '这款APP还不错划算还可以上门服务', null, '1531999999', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('60', '1', '37', '足疗', '万箭穿心习惯就好', 'https://tva2.sinaimg.cn/crop.0.0.640.640.180/880593e5jw8ebc3h2lp61j20hs0hsdgd.jpg', '', '0', '0', '以后还要常来', null, '1543333333', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('61', '1', '40', '足疗', '别人怎么看俄无所谓﹌', 'http://wx.qlogo.cn/mmopen/vi_32/Xjxmoxk9MzswribVQhNSxOYJ3B8PRoaibm8zWfHKQCE3jv5xmEMRcBZrLibkf5WV9Hpqic15uzxgRiaSjF7Cq95uFeQ/132', '', '0', '0', '给1号技师点个赞，按完浑身舒适', null, '1535555555', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('62', '1', '40', '足疗', '独自郊游的鱼丶', 'http://wx.qlogo.cn/mmopen/vi_32/THmWUHqeVmibCXcbaiay56LPAgAQRCysSFt6IdiaFmJqENF3wtRg79QgrIPqDKPia5FSIgpDjh8OELwfMQ28CNFybg/132', '', '0', '0', '手法很到位，很舒服', null, '1543333333', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('63', '1', '40', '足疗', '当凹凸曼操了小怪兽', 'http://q.qlogo.cn/qqapp/1106555152/E7AC0F4211B033681EEB2CEEBABB804B/100', '', '0', '0', '很棒，点个赞', null, '1545555555', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('64', '1', '40', '足疗', '一脸的美人痣', 'http://pic.xijiaomo.com/android_xjm_userphoto6920171221111419', '', '0', '0', '蛮是那回事儿。', null, '1544444444', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('65', '1', '40', '足疗', '日、　久生情', 'http://pic.xijiaomo.com/android_xjm_userphoto3720171102193758', '', '0', '0', '感觉很不错，下次继续点他。', null, '1545555555', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('66', '1', '40', '足疗', '让我一次爱个够べ', 'http://q.qlogo.cn/qqapp/1106555152/8B559073B6D3378FE6FF7AE7D0D46AB1/100', '', '0', '0', '下次要是继续要叫舒服的话，我还要叫他！！', null, '1531866666', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('67', '1', '40', '足疗', '\r\n你是我仅有的骄傲', 'https://q.qlogo.cn/qqapp/1106555152/26F6B18D9FC920959C06827D73EC2EF4/100', '', '0', '0', '去过很多足疗养生店，发现这款APP很实用，技师也很到位，点个赞。', null, '1544444444', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('68', '1', '40', '足疗', '\r\n卑贱的太过分', 'http://wx.qlogo.cn/mmopen/vi_32/q5ykHyRrke7dodn5kwaSzB1tegWns1KKr9KGROVicgrjX6n6icAMVMJXabFUngtKs8kJO8RdUJo6arAdoHr91c3Q/132', '', '0', '0', '技师年轻漂亮，手法娴熟，很享受', null, '1531866666', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('69', '1', '40', '足疗', '微笑打败一切。', 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKj6ecfIWrvzzJHrOkUD5saUtqsv2WXj6f24QjTcUgNdyiaDEicGib3WnmaC7vKQY21V9ACF6hVvM6pA/0', '', '0', '0', '按的蛮板实！', null, '1546546513', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('70', '1', '40', '足疗', '高凡力', 'http://tvax2.sinaimg.cn/crop.0.0.1328.1328.180/7980786fly8fgwqw2qpq9j210w10wn0c.jpg', '', '0', '0', '手法很到位，服务贴心，很推荐。', null, '1544444444', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('71', '1', '68', '足疗', '奇才', 'https://tva3.sinaimg.cn/crop.0.0.640.640.180/a52fa0fdjw8ea87cfuo5oj20hs0hs3zr.jpg', '', '0', '0', '价格合理，按得也不错，下次继续！！！', null, '1541326871', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('72', '1', '68', '足疗', '高熙琪', 'http://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83ersjDMOiaprMknb0Q8wPzJB57jIXPeVI0dPKSvibAq6gV2uFEQ5dlxOq3wSebyWUrSFq9rHuKicpv11g/0', '', '0', '0', '按摩师说话礼貌，并考量客户的需求，手法轻柔。', null, '1531866666', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('73', '1', '68', '足疗', '猛龙', 'http://tva2.sinaimg.cn/crop.0.0.664.664.180/ae4d2053jw8f1bt0h8ztmj20ig0igwfu.jpg', '', '0', '0', '现在这年头哪个人不注意养生呢，困了累了就想找个技师按摩！', null, '1546546513', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('74', '1', '68', '足疗', '易佳乐', 'http://tva3.sinaimg.cn/crop.0.0.100.100.180/006cZ51tjw8evjb4axkyxj302s02sjr7.jpg', '', '0', '0', '技师按摩也算专业，也没有推销办卡什么的，还挺不错', null, '1544444444', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('75', '1', '68', '足疗', '国王', 'https://q.qlogo.cn/qqapp/1106555152/C26C93107B3C912A63BFC6C7BFCD9003/100', '', '0', '0', '技师很专业，一边按摩一边给我建议 挺感谢的。', null, '1546666666', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('76', '1', '68', '足疗', '易雯淅', 'http://wx.qlogo.cn/mmopen/vi_32/7QdogyrATxUDbSnULrvOZfKErolOSgNh7d1asIzb21lH8ical8IO3qq2BvZlujOLUPwJxj9ekRZCibONOapSjb3g/0', '', '0', '0', '服务人员会询问你的一些细节问题。哪里不舒服什么的，会给你针对性的按摩，缓解一下疲劳，促进血液循环，真的很不错！推荐给大家~安排！！', null, '1531860035', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('77', '1', '68', '足疗', '王二狗', 'http://q.qlogo.cn/qqapp/1106555152/B7660043F44A4CD9DCABC04E4DBA1665/100', '', '0', '0', '手法还成，技师也挺会聊天的。', null, '1544444444', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('78', '1', '68', '足疗', '独行侠', 'http://tva1.sinaimg.cn/crop.0.13.1536.1536.180/8b2e3573jw8f645jvyu3zj216o17ftcv.jpg', '', '0', '0', '技师手法专业，性价比高，挺好的。', null, '1531864546', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('79', '1', '68', '足疗', '易锦昕', 'http://q.qlogo.cn/qqapp/1106555152/F4994FDA83B815541E378216AA5520D7/100', '', '0', '0', '这个技师，说话特别好听，颜值也高。', null, '1531999999', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('80', '1', '68', '足疗', '篮网', 'https://q.qlogo.cn/qqapp/1106555152/0AA6AD2A4CC2270E8734F6E2C5A96195/100', '', '0', '0', '最近比较喜欢按摩，然后也不想出门，这个APP挺方便的，技师也很不错，这个软件值得介绍给朋友。', null, '1544444444', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('81', '1', '69', '足疗', '张麟炜', 'http://pic.xijiaomo.com/android_xjm_userphoto8220171128161617', '', '0', '0', '年纪大了，经常会有点不适，当然这也是现代年轻人的常态吗，手机族的不好的后遗症，足不出户享受生活，推荐！', null, '1539999999', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('82', '1', '69', '足疗', '活塞', 'http://pic.xijiaomo.com/ctjxgm5fdh.png', '', '0', '0', '这个技师很棒', null, '1544444444', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('83', '1', '69', '足疗', '张诗涵', 'http://pic.xijiaomo.com/20171123171332.', '', '0', '0', '朋友推荐的APP和技师，第一次使用吗，感觉不错吗，下次有机会继续使用~', null, '1531899999', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('84', '1', '69', '足疗', '开拓者', 'http://pic.xijiaomo.com/android_xjm_userphoto7020171123150548', '', '0', '0', '冬季快到了，又到了养身的时候了，想着做按摩，正好别人推荐了这个APP，挺好使的。', null, '1531866666', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('85', '1', '69', '足疗', '张嘉萱', 'http://pic.xijiaomo.com/android_xjm_userphoto6920171221111419', '', '0', '0', '低价享受生活，折扣卷很舒服。', null, '1544444444', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('86', '1', '69', '足疗', '鹈鹕', 'http://pic.xijiaomo.com/android_xjm_userphoto3120171205162810', '', '0', '0', '年纪大了的缺点就是，不时常按摩下，就总感觉身体不舒服吗，总觉得胫骨都是僵硬的。', null, '1546666666', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('87', '1', '69', '足疗', '李啸林', 'http://pic.xijiaomo.com/20171123120743.', '', '0', '0', '技师手法不错,活也到位', null, '1531899999', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('88', '1', '69', '足疗', '这个产品不太冷', 'http://pic.xijiaomo.com/android_xjm_userphoto3720171102193758', '', '0', '0', '很划算的，上门服务，态度也好，还不贵。', null, '1531899999', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('89', '1', '69', '足疗', '湖人', 'http://pic.xijiaomo.com/android_xjm_userphoto3520171115105520', '', '0', '0', '一次不错的保健体验。', null, '1531866666', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('90', '1', '69', '足疗', '李彦龙', 'http://pic.xijiaomo.com/android_xjm_userphoto3120171205162810', '', '0', '0', '服务周到，态度友好。', null, '1531899999', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('92', '1', '1', '足疗', '王全', 'http://tva3.sinaimg.cn/crop.0.0.100.100.180/006cZ51tjw8evjb4axkyxj302s02sjr7.jpg', '', '0', '0', '手动点赞', null, '1531899999', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('93', '1', '1', '足疗', '哈哈哈', 'http://q.qlogo.cn/qqapp/1106555152/E7AC0F4211B033681EEB2CEEBABB804B/100', '', '0', '0', '妹子很漂亮', null, '1531899999', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('94', '1', '1', '足疗', '给你的哎', 'http://pic.xijiaomo.com/android_xjm_userphoto6920171221111419', '', '0', '0', '服务好，舒服', null, '1531899999', '9,8,7,6,5,4,3,2,1', '2017112809625', null);
INSERT INTO `xjm_user_comment` VALUES ('95', '1', '1', '足疗', '我怕疼', 'http://wx.qlogo.cn/mmopen/vi_32/q5ykHyRrke7dodn5kwaSzB1tegWns1KKr9KGROVicgrjX6n6icAMVMJXabFUngtKs8kJO8RdUJo6arAdoHr91c3Q/132', '', '0', '0', '下班后特别需要洗脚', null, '1531899999', '9,8,7,6,5,4,3,2,1', '2017112809625', null);

-- ----------------------------
-- Table structure for xjm_user_comment_tag
-- ----------------------------
DROP TABLE IF EXISTS `xjm_user_comment_tag`;
CREATE TABLE `xjm_user_comment_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '产品分类ID',
  `tag_id` int(11) NOT NULL DEFAULT '0' COMMENT '标签id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='用户评论标签表';

-- ----------------------------
-- Records of xjm_user_comment_tag
-- ----------------------------
INSERT INTO `xjm_user_comment_tag` VALUES ('1', '13', '1');
INSERT INTO `xjm_user_comment_tag` VALUES ('2', '13', '2');
INSERT INTO `xjm_user_comment_tag` VALUES ('3', '2', '1');
INSERT INTO `xjm_user_comment_tag` VALUES ('4', '2', '2');
INSERT INTO `xjm_user_comment_tag` VALUES ('5', '2', '3');
INSERT INTO `xjm_user_comment_tag` VALUES ('6', '1', '3');
INSERT INTO `xjm_user_comment_tag` VALUES ('7', '1', '4');
INSERT INTO `xjm_user_comment_tag` VALUES ('8', '35', '5');
INSERT INTO `xjm_user_comment_tag` VALUES ('9', '35', '1');
INSERT INTO `xjm_user_comment_tag` VALUES ('10', '26', '1');
INSERT INTO `xjm_user_comment_tag` VALUES ('11', '26', '5');
INSERT INTO `xjm_user_comment_tag` VALUES ('12', '26', '4');

-- ----------------------------
-- Table structure for xjm_user_coupon
-- ----------------------------
DROP TABLE IF EXISTS `xjm_user_coupon`;
CREATE TABLE `xjm_user_coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `cid` int(8) NOT NULL DEFAULT '0' COMMENT '优惠券 对应coupon表id',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '代金卷名字',
  `area_id` mediumint(4) unsigned DEFAULT '0' COMMENT '使用地区：关联地区表id',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '类型 1无门槛  2满减',
  `money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '代金卷价值',
  `condition` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '使用条件',
  `from` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '获得类型 1 按订单发放 2 注册 3 邀请  4兑换',
  `order_id` int(8) DEFAULT '0' COMMENT '订单id',
  `end_time` int(11) NOT NULL DEFAULT '0' COMMENT '结束时间',
  `send_time` int(11) NOT NULL DEFAULT '0' COMMENT '发放时间',
  `status` tinyint(1) DEFAULT '0' COMMENT '0:未使用 1：锁定 2：已使用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=utf8 COMMENT='用户优惠券表';

-- ----------------------------
-- Records of xjm_user_coupon
-- ----------------------------
INSERT INTO `xjm_user_coupon` VALUES ('143', '31', '2', '满150减20元优惠券', '0', '2', '20.00', '150.00', '4', '0', '1511682251', '1511423051', '0');
INSERT INTO `xjm_user_coupon` VALUES ('144', '31', '1', '通用10元优惠券', '0', '1', '10.00', '0.00', '4', '0', '1511595852', '1511423052', '1');
INSERT INTO `xjm_user_coupon` VALUES ('145', '31', '1', '通用10元优惠券', '0', '1', '10.00', '0.00', '4', '0', '1511597474', '1511424674', '0');
INSERT INTO `xjm_user_coupon` VALUES ('146', '46', '3', '上门按摩优惠券', '0', '1', '5.00', '0.00', '3', '0', '1514114883', '1511522883', '1');
INSERT INTO `xjm_user_coupon` VALUES ('147', '46', '3', '上门按摩优惠券', '0', '1', '5.00', '0.00', '3', '0', '1514115025', '1511523025', '1');
INSERT INTO `xjm_user_coupon` VALUES ('148', '69', '3', '上门按摩优惠券', '0', '1', '5.00', '0.00', '3', '0', '1514445634', '1511853634', '0');
INSERT INTO `xjm_user_coupon` VALUES ('149', '73', '2', '满150减20元优惠券', '0', '2', '20.00', '150.00', '4', '0', '1512178748', '1511919548', '0');
INSERT INTO `xjm_user_coupon` VALUES ('150', '73', '2', '满150减20元优惠券', '0', '2', '20.00', '150.00', '4', '0', '1512181571', '1511922371', '0');
INSERT INTO `xjm_user_coupon` VALUES ('151', '73', '2', '满150减20元优惠券', '0', '2', '20.00', '150.00', '4', '0', '1512181855', '1511922655', '0');
INSERT INTO `xjm_user_coupon` VALUES ('152', '73', '1', '通用10元优惠券', '0', '1', '10.00', '0.00', '4', '0', '1512095620', '1511922820', '0');
INSERT INTO `xjm_user_coupon` VALUES ('153', '73', '1', '通用10元优惠券', '0', '1', '10.00', '0.00', '4', '0', '1512095887', '1511923087', '0');
INSERT INTO `xjm_user_coupon` VALUES ('154', '73', '2', '满150减20元优惠券', '0', '2', '20.00', '150.00', '4', '0', '1512182528', '1511923328', '0');
INSERT INTO `xjm_user_coupon` VALUES ('155', '73', '1', '通用10元优惠券', '0', '1', '10.00', '0.00', '4', '0', '1512096143', '1511923343', '0');
INSERT INTO `xjm_user_coupon` VALUES ('156', '32', '3', '上门按摩优惠券', '0', '1', '5.00', '0.00', '3', '0', '1514685708', '1512093708', '1');
INSERT INTO `xjm_user_coupon` VALUES ('157', '31', '2', '满150减20元优惠券', '0', '2', '20.00', '150.00', '4', '0', '1512790670', '1512531470', '0');
INSERT INTO `xjm_user_coupon` VALUES ('158', '130', '3', '上门按摩优惠券', '0', '1', '5.00', '0.00', '3', '0', '1515483101', '1512891101', '0');

-- ----------------------------
-- Table structure for xjm_user_point
-- ----------------------------
DROP TABLE IF EXISTS `xjm_user_point`;
CREATE TABLE `xjm_user_point` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '关联用户id',
  `number` int(8) NOT NULL DEFAULT '0' COMMENT '积分数量',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0：获得 1：消费',
  `addtime` int(10) DEFAULT '0',
  `origin` int(5) NOT NULL COMMENT '积分来源，注册1，支付订单2  兑换优惠券3',
  `origin_name` varchar(50) NOT NULL COMMENT '积分来源名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8 COMMENT='用户积分表';

-- ----------------------------
-- Records of xjm_user_point
-- ----------------------------
INSERT INTO `xjm_user_point` VALUES ('40', '31', '1', '0', '1511400627', '2', '支付订单');
INSERT INTO `xjm_user_point` VALUES ('41', '35', '1', '0', '1511402190', '2', '支付订单');
INSERT INTO `xjm_user_point` VALUES ('42', '35', '1', '0', '1511402312', '2', '支付订单');
INSERT INTO `xjm_user_point` VALUES ('43', '35', '1', '0', '1511403095', '2', '支付订单');
INSERT INTO `xjm_user_point` VALUES ('44', '35', '1', '0', '1511403382', '2', '支付订单');
INSERT INTO `xjm_user_point` VALUES ('45', '35', '1', '0', '1511404856', '2', '支付订单');
INSERT INTO `xjm_user_point` VALUES ('46', '35', '1', '0', '1511404942', '2', '支付订单');
INSERT INTO `xjm_user_point` VALUES ('47', '31', '1000', '1', '1511423051', '3', '积分兑换优惠券');
INSERT INTO `xjm_user_point` VALUES ('48', '31', '1000', '1', '1511423052', '3', '积分兑换优惠券');
INSERT INTO `xjm_user_point` VALUES ('49', '31', '1000', '1', '1511424674', '3', '积分兑换优惠券');
INSERT INTO `xjm_user_point` VALUES ('50', '40', '50', '0', '1511487198', '2', '支付订单');
INSERT INTO `xjm_user_point` VALUES ('51', '40', '50', '0', '1511505360', '2', '支付订单');
INSERT INTO `xjm_user_point` VALUES ('52', '40', '50', '0', '1511507636', '2', '支付订单');
INSERT INTO `xjm_user_point` VALUES ('53', '40', '50', '0', '1511507762', '2', '支付订单');
INSERT INTO `xjm_user_point` VALUES ('54', '40', '50', '0', '1511507788', '2', '支付订单');
INSERT INTO `xjm_user_point` VALUES ('55', '40', '50', '0', '1511518330', '2', '支付订单');
INSERT INTO `xjm_user_point` VALUES ('56', '32', '60', '0', '1511754229', '2', '支付订单');
INSERT INTO `xjm_user_point` VALUES ('57', '32', '60', '0', '1511754326', '2', '支付订单');
INSERT INTO `xjm_user_point` VALUES ('58', '32', '60', '0', '1511838770', '2', '支付订单');
INSERT INTO `xjm_user_point` VALUES ('59', '32', '60', '0', '1511838819', '2', '支付订单');
INSERT INTO `xjm_user_point` VALUES ('60', '32', '60', '0', '1511838834', '2', '支付订单');
INSERT INTO `xjm_user_point` VALUES ('61', '32', '60', '0', '1511838847', '2', '支付订单');
INSERT INTO `xjm_user_point` VALUES ('62', '32', '60', '0', '1511838894', '2', '支付订单');
INSERT INTO `xjm_user_point` VALUES ('63', '32', '60', '0', '1511838945', '2', '支付订单');
INSERT INTO `xjm_user_point` VALUES ('64', '32', '60', '0', '1511839008', '2', '支付订单');
INSERT INTO `xjm_user_point` VALUES ('65', '40', '50', '0', '1511859232', '2', '支付订单');
INSERT INTO `xjm_user_point` VALUES ('66', '81', '65', '0', '1511859970', '2', '支付订单');
INSERT INTO `xjm_user_point` VALUES ('67', '73', '1000', '1', '1511922655', '3', '积分兑换优惠券');
INSERT INTO `xjm_user_point` VALUES ('68', '73', '1000', '1', '1511922820', '3', '积分兑换优惠券');
INSERT INTO `xjm_user_point` VALUES ('69', '73', '1000', '1', '1511923087', '3', '积分兑换优惠券');
INSERT INTO `xjm_user_point` VALUES ('70', '73', '1000', '1', '1511923328', '3', '积分兑换优惠券');
INSERT INTO `xjm_user_point` VALUES ('71', '73', '1000', '1', '1511923343', '3', '积分兑换优惠券');
INSERT INTO `xjm_user_point` VALUES ('72', '40', '50', '0', '1512359748', '2', '支付订单');
INSERT INTO `xjm_user_point` VALUES ('73', '31', '1000', '1', '1512531470', '3', '积分兑换优惠券');
